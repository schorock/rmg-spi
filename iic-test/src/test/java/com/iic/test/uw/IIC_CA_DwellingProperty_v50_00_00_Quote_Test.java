package com.iic.test.uw;

import org.testng.annotations.Test;

import com.iscs.test.runner.TestNGSpreadsheetTestRunnerBase;


/**
 * @author ramaswamy.subramanian
 *
 */
@Test(groups = {"UnderwritingTests"})
public class IIC_CA_DwellingProperty_v50_00_00_Quote_Test extends TestNGSpreadsheetTestRunnerBase {
	
	{
		// Define the source of spreadsheet data for this test
		CLIENT = "IIC";
		SPREADSHEET_URL = this.getClass().getResource("iic-ca-dwellingproperty-v50_00_00.xlsm");
		SPREADSHEET_WORKSHEET = "DPQuote";
	}
	
	// This class is just a thin shell so TestNG has a class to run
	// Most of the work is done in TestRunnerBase

}