-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: iicspi_db_develop
-- ------------------------------------------------------
-- Server version	5.6.28-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accountingstats`
--

DROP TABLE IF EXISTS `accountingstats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountingstats` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `StatSequence` int(11) DEFAULT NULL,
  `StatSequenceReplace` int(11) DEFAULT NULL,
  `PolicyRef` int(11) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `LineCd` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `CoverageCd` varchar(255) DEFAULT NULL,
  `RiskCd` varchar(255) DEFAULT NULL,
  `RiskTypeCd` varchar(255) DEFAULT NULL,
  `SubTypeCd` varchar(255) DEFAULT NULL,
  `CoverageItemCd` varchar(255) DEFAULT NULL,
  `FeeCd` varchar(255) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `CombinedKey` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `TransactionEffectiveDt` date DEFAULT NULL,
  `AccountingDt` date DEFAULT NULL,
  `WrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WrittenCommissionAmt` decimal(28,6) DEFAULT NULL,
  `WrittenPremiumFeeAmt` decimal(28,6) DEFAULT NULL,
  `WrittenCommissionFeeAmt` decimal(28,6) DEFAULT NULL,
  `EarnDays` int(11) DEFAULT NULL,
  `InforceChangeAmt` decimal(28,6) DEFAULT NULL,
  `PolicyYear` varchar(255) DEFAULT NULL,
  `TermDays` int(11) DEFAULT NULL,
  `InsuranceTypeCd` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `NewRenewalCd` varchar(255) DEFAULT NULL,
  `Limit1` varchar(255) DEFAULT NULL,
  `Limit2` varchar(255) DEFAULT NULL,
  `Deductible1` varchar(255) DEFAULT NULL,
  `Deductible2` varchar(255) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `CoinsurancePct` decimal(28,6) DEFAULT NULL,
  `CoveredPerilsCd` varchar(255) DEFAULT NULL,
  `CommissionKey` varchar(255) DEFAULT NULL,
  `CommissionAreaCd` varchar(255) DEFAULT NULL,
  `RateAreaName` varchar(255) DEFAULT NULL,
  `StatData` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `BusinessSourceCd` varchar(255) DEFAULT NULL,
  `ProducerCd` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `PolicyStatusCd` varchar(255) DEFAULT NULL,
  `ProductName` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  `PolicyGroupCd` varchar(255) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `ShortRateStatInd` varchar(255) DEFAULT NULL,
  `PayPlanCd` varchar(255) DEFAULT NULL,
  `ControllingProductVersionIdRef` varchar(255) DEFAULT NULL,
  `ControllingStateCd` varchar(255) DEFAULT NULL,
  `DividendPlanCd` varchar(255) DEFAULT NULL,
  `BillMethod` varchar(255) DEFAULT NULL,
  `StatementAccountNumber` varchar(255) DEFAULT NULL,
  `StatementAccountRef` int(11) DEFAULT NULL,
  `AdmiraltyFELALiabilityLimits` varchar(255) DEFAULT NULL,
  `AdmiraltyFELAMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AlcoholDrugFreeCreditInd` varchar(255) DEFAULT NULL,
  `AlcDrugFreeCreditAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheRate` decimal(28,6) DEFAULT NULL,
  `ContrClassPremAdjCreditFactor` varchar(255) DEFAULT NULL,
  `ContrClassPremAdjCreditAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleDiscountRatePct` decimal(28,6) DEFAULT NULL,
  `DiseaseAbrasiveAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAbrasivePaDeAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAsbestosAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAtomicEnergyAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseCoalmineAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseExperienceAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundryIronAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundryNoFerrAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundrySteelAmt` decimal(28,6) DEFAULT NULL,
  `ELIncreasedLimitPct` decimal(28,6) DEFAULT NULL,
  `ELIncreaseLimitMinPremAmt` decimal(28,6) DEFAULT NULL,
  `ELIncreaseLimitMinPremChrgAmt` decimal(28,6) DEFAULT NULL,
  `EstimatedAnnualPremiumAmt` decimal(28,6) DEFAULT NULL,
  `ExpenseConstantAmt` decimal(28,6) DEFAULT NULL,
  `ExperienceMeritRating` varchar(255) DEFAULT NULL,
  `ExperienceModificationFactor` varchar(255) DEFAULT NULL,
  `ManagedCareCreditInd` varchar(255) DEFAULT NULL,
  `ManagedCareCreditAmt` decimal(28,6) DEFAULT NULL,
  `NAICS` varchar(255) DEFAULT NULL,
  `NCCIRiskIdNumber` varchar(255) DEFAULT NULL,
  `NumberOfClaims` varchar(255) DEFAULT NULL,
  `PremiumDiscountAmt` decimal(28,6) DEFAULT NULL,
  `RatingBureauID` varchar(255) DEFAULT NULL,
  `CertifiedSafetyHealthProgPct` decimal(28,6) DEFAULT NULL,
  `SchedRateClassPeculiarities` varchar(255) DEFAULT NULL,
  `SchedRateEmployee` varchar(255) DEFAULT NULL,
  `SchedRateManagement` varchar(255) DEFAULT NULL,
  `SchedRateMedicalFacilities` varchar(255) DEFAULT NULL,
  `SchedRateMgmtSafetyOrg` varchar(255) DEFAULT NULL,
  `SchedRatePremises` varchar(255) DEFAULT NULL,
  `SchedRateSafetyDevices` varchar(255) DEFAULT NULL,
  `SIC` varchar(255) DEFAULT NULL,
  `StandardPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjust` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjustSign` varchar(255) DEFAULT NULL,
  `TerrorismAmt` decimal(28,6) DEFAULT NULL,
  `TerrorismRate` decimal(28,6) DEFAULT NULL,
  `TotalManualPremimAmt` decimal(28,6) DEFAULT NULL,
  `TotalSubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationInd` varchar(255) DEFAULT NULL,
  `WaiverSubrogationRate` decimal(28,6) DEFAULT NULL,
  `YearsInBusiness` varchar(255) DEFAULT NULL,
  `DrugFreeWorkplace` varchar(255) DEFAULT NULL,
  `StatSourceIdRef` varchar(255) DEFAULT NULL,
  `ConversionTemplateIdRef` varchar(255) DEFAULT NULL,
  `ConversionGroup` varchar(255) DEFAULT NULL,
  `ConversionJobRef` varchar(255) DEFAULT NULL,
  `ConversionFileName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`),
  KEY `AccountingStats_1` (`PolicyNumber`),
  KEY `AccountingStats_2` (`CombinedKey`),
  KEY `AccountingStats_3` (`BookDt`),
  KEY `AccountingStats_4` (`AccountingDt`),
  KEY `AccountingStats_5` (`StatusCd`),
  KEY `AccountingStats_6` (`CommissionKey`),
  KEY `AccountingStats_Idx_1` (`CombinedKey`,`StatusCd`),
  KEY `AccountingStats_Idx_2` (`CombinedKey`,`AccountingDt`),
  KEY `AccountingStats_Idx_3` (`CommissionKey`,`BookDt`),
  KEY `AccountingStats_Idx_4` (`CommissionKey`,`AccountingDt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accountingstats`
--

LOCK TABLES `accountingstats` WRITE;
/*!40000 ALTER TABLE `accountingstats` DISABLE KEYS */;
/*!40000 ALTER TABLE `accountingstats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accountingsummarystats`
--

DROP TABLE IF EXISTS `accountingsummarystats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountingsummarystats` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `StatSequence` int(11) DEFAULT NULL,
  `StatSequenceReplace` int(11) DEFAULT NULL,
  `ReportPeriod` varchar(255) DEFAULT NULL,
  `UpdateDt` date DEFAULT NULL,
  `PolicyRef` int(11) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `LineCd` varchar(255) DEFAULT NULL,
  `RiskCd` varchar(255) DEFAULT NULL,
  `RiskTypeCd` varchar(255) DEFAULT NULL,
  `SubTypeCd` varchar(255) DEFAULT NULL,
  `CoverageCd` varchar(255) DEFAULT NULL,
  `CoverageItemCd` varchar(255) DEFAULT NULL,
  `FeeCd` varchar(255) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `CombinedKey` varchar(255) DEFAULT NULL,
  `AccountingDt` date DEFAULT NULL,
  `PolicyYear` varchar(255) DEFAULT NULL,
  `InsuranceTypeCd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `NewRenewalCd` varchar(255) DEFAULT NULL,
  `Limit1` varchar(255) DEFAULT NULL,
  `Limit2` varchar(255) DEFAULT NULL,
  `Limit3` varchar(255) DEFAULT NULL,
  `Limit4` varchar(255) DEFAULT NULL,
  `Limit5` varchar(255) DEFAULT NULL,
  `Limit6` varchar(255) DEFAULT NULL,
  `Limit7` varchar(255) DEFAULT NULL,
  `Limit8` varchar(255) DEFAULT NULL,
  `Limit9` varchar(255) DEFAULT NULL,
  `Deductible1` varchar(255) DEFAULT NULL,
  `Deductible2` varchar(255) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `CoinsurancePct` decimal(28,6) DEFAULT NULL,
  `CoveredPerilsCd` varchar(255) DEFAULT NULL,
  `CommissionAreaCd` varchar(255) DEFAULT NULL,
  `MTDWrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `TTDWrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `YTDWrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `MTDWrittenCommissionAmt` decimal(28,6) DEFAULT NULL,
  `YTDWrittenCommissionAmt` decimal(28,6) DEFAULT NULL,
  `TTDWrittenCommissionAmt` decimal(28,6) DEFAULT NULL,
  `MTDWrittenPremiumFeeAmt` decimal(28,6) DEFAULT NULL,
  `YTDWrittenPremiumFeeAmt` decimal(28,6) DEFAULT NULL,
  `TTDWrittenPremiumFeeAmt` decimal(28,6) DEFAULT NULL,
  `MTDWrittenCommissionFeeAmt` decimal(28,6) DEFAULT NULL,
  `YTDWrittenCommissionFeeAmt` decimal(28,6) DEFAULT NULL,
  `TTDWrittenCommissionFeeAmt` decimal(28,6) DEFAULT NULL,
  `InforceAmt` decimal(28,6) DEFAULT NULL,
  `LastInforceAmt` decimal(28,6) DEFAULT NULL,
  `MonthEarnedPremiumAmt` decimal(28,6) DEFAULT NULL,
  `MTDEarnedPremiumAmt` decimal(28,6) DEFAULT NULL,
  `YTDEarnedPremiumAmt` decimal(28,6) DEFAULT NULL,
  `TTDEarnedPremiumAmt` decimal(28,6) DEFAULT NULL,
  `PolicyInforceCount` int(11) DEFAULT NULL,
  `LocationInforceCount` int(11) DEFAULT NULL,
  `RiskInforceCount` int(11) DEFAULT NULL,
  `MonthUnearnedAmt` decimal(28,6) DEFAULT NULL,
  `UnearnedAmt` decimal(28,6) DEFAULT NULL,
  `UnearnedM00Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM01Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM02Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM03Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM04Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM05Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM06Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM07Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM08Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM09Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM10Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM11Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM12Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM13Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM14Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM15Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM16Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM17Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM18Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM19Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM20Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM21Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM22Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM23Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM24Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM25Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM26Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM27Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM28Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM29Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM30Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM31Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM32Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM33Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM34Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM35Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM36Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM37Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM38Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM39Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM40Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM41Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM42Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM43Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM44Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM45Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM46Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM47Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM48Amt` decimal(28,6) DEFAULT NULL,
  `RateAreaName` varchar(255) DEFAULT NULL,
  `StatData` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `BusinessSourceCd` varchar(255) DEFAULT NULL,
  `ProducerCd` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `PolicyStatusCd` varchar(255) DEFAULT NULL,
  `ProductName` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  `PolicyGroupCd` varchar(255) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `ControllingProductVersionIdRef` varchar(255) DEFAULT NULL,
  `ControllingStateCd` varchar(255) DEFAULT NULL,
  `DividendPlanCd` varchar(255) DEFAULT NULL,
  `BillMethod` varchar(255) DEFAULT NULL,
  `AdmiraltyFELALiabilityLimits` varchar(255) DEFAULT NULL,
  `AdmiraltyFELAMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AlcoholDrugFreeCreditInd` varchar(255) DEFAULT NULL,
  `AlcDrugFreeCreditAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheRate` decimal(28,6) DEFAULT NULL,
  `ContrClassPremAdjCreditFactor` varchar(255) DEFAULT NULL,
  `ContrClassPremAdjCreditAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleDiscountRatePct` decimal(28,6) DEFAULT NULL,
  `DiseaseAbrasiveAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAbrasivePaDeAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAsbestosAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAtomicEnergyAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseCoalmineAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseExperienceAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundryIronAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundryNoFerrAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundrySteelAmt` decimal(28,6) DEFAULT NULL,
  `ELIncreasedLimitPct` decimal(28,6) DEFAULT NULL,
  `ELIncreaseLimitMinPremAmt` decimal(28,6) DEFAULT NULL,
  `ELIncreaseLimitMinPremChrgAmt` decimal(28,6) DEFAULT NULL,
  `EstimatedAnnualPremiumAmt` decimal(28,6) DEFAULT NULL,
  `ExpenseConstantAmt` decimal(28,6) DEFAULT NULL,
  `ExperienceMeritRating` varchar(255) DEFAULT NULL,
  `ExperienceModificationFactor` varchar(255) DEFAULT NULL,
  `ManagedCareCreditInd` varchar(255) DEFAULT NULL,
  `ManagedCareCreditAmt` decimal(28,6) DEFAULT NULL,
  `NAICS` varchar(255) DEFAULT NULL,
  `NCCIRiskIdNumber` varchar(255) DEFAULT NULL,
  `NumberOfClaims` varchar(255) DEFAULT NULL,
  `PremiumDiscountAmt` decimal(28,6) DEFAULT NULL,
  `RatingBureauID` varchar(255) DEFAULT NULL,
  `CertifiedSafetyHealthProgPct` decimal(28,6) DEFAULT NULL,
  `SchedRateClassPeculiarities` varchar(255) DEFAULT NULL,
  `SchedRateEmployee` varchar(255) DEFAULT NULL,
  `SchedRateManagement` varchar(255) DEFAULT NULL,
  `SchedRateMedicalFacilities` varchar(255) DEFAULT NULL,
  `SchedRateMgmtSafetyOrg` varchar(255) DEFAULT NULL,
  `SchedRatePremises` varchar(255) DEFAULT NULL,
  `SchedRateSafetyDevices` varchar(255) DEFAULT NULL,
  `SIC` varchar(255) DEFAULT NULL,
  `StandardPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjust` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjustSign` varchar(255) DEFAULT NULL,
  `TerrorismAmt` decimal(28,6) DEFAULT NULL,
  `TerrorismRate` decimal(28,6) DEFAULT NULL,
  `TotalSubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationInd` varchar(255) DEFAULT NULL,
  `WaiverSubrogationRate` decimal(28,6) DEFAULT NULL,
  `YearsInBusiness` varchar(255) DEFAULT NULL,
  `DrugFreeWorkplace` varchar(255) DEFAULT NULL,
  `ConversionTemplateIdRef` varchar(255) DEFAULT NULL,
  `ConversionGroup` varchar(255) DEFAULT NULL,
  `ConversionJobRef` varchar(255) DEFAULT NULL,
  `ConversionFileName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`),
  KEY `AccountingSummaryStats_1` (`PolicyNumber`),
  KEY `AccountingSummaryStats_Idx_1` (`ReportPeriod`,`CombinedKey`,`UpdateDt`),
  KEY `AccountingSummaryStats_Idx_2` (`CarrierGroupCd`,`CarrierCd`,`StateCd`,`AnnualStatementLineCd`,`LineCd`,`ProducerCd`),
  KEY `AccountingSummaryStats_Idx_3` (`ReportPeriod`,`PolicyRef`),
  KEY `AccountingSummaryStats_Idx_4` (`ReportPeriod`,`CarrierGroupCd`,`CarrierCd`,`StateCd`,`AnnualStatementLineCd`),
  KEY `AccountingSummaryStats_Idx_5` (`ReportPeriod`,`CarrierGroupCd`,`CarrierCd`,`PolicyNumber`,`PolicyVersion`,`MTDWrittenPremiumAmt`,`MTDWrittenPremiumFeeAmt`,`MonthEarnedPremiumAmt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accountingsummarystats`
--

LOCK TABLES `accountingsummarystats` WRITE;
/*!40000 ALTER TABLE `accountingsummarystats` DISABLE KEYS */;
/*!40000 ALTER TABLE `accountingsummarystats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accountlookup`
--

DROP TABLE IF EXISTS `accountlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accountlookup`
--

LOCK TABLES `accountlookup` WRITE;
/*!40000 ALTER TABLE `accountlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `accountlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accountstats`
--

DROP TABLE IF EXISTS `accountstats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountstats` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `StatSequence` int(11) DEFAULT NULL,
  `StatSequenceReplace` int(11) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `BillingEntitySequenceNumber` varchar(255) DEFAULT NULL,
  `CombinedKey` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  `AccountTypeCd` varchar(255) DEFAULT NULL,
  `PayPlanCd` varchar(255) DEFAULT NULL,
  `TransactionTypeCd` varchar(255) DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `TransactionNumber` varchar(255) DEFAULT NULL,
  `PolicyTransactionCd` varchar(255) DEFAULT NULL,
  `ChargeAmt` decimal(28,6) DEFAULT NULL,
  `CommissionChargeAmt` decimal(28,6) DEFAULT NULL,
  `BalanceAmt` decimal(28,6) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `AdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `CategoryCd` varchar(255) DEFAULT NULL,
  `NetPaidAmt` decimal(28,6) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `ProviderCd` varchar(255) DEFAULT NULL,
  `CheckNumber` varchar(255) DEFAULT NULL,
  `CheckAmt` decimal(28,6) DEFAULT NULL,
  `CheckDt` date DEFAULT NULL,
  `BatchId` varchar(255) DEFAULT NULL,
  `NextDueDt` date DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `PaidBy` varchar(255) DEFAULT NULL,
  `PaidByName` varchar(255) DEFAULT NULL,
  `StatementTypeCd` varchar(255) DEFAULT NULL,
  `ActivityTypeCd` varchar(255) DEFAULT NULL,
  `PolicyTransactionNumber` varchar(255) DEFAULT NULL,
  `CommisionPct` decimal(28,6) DEFAULT NULL,
  `CommissionTypeCd` varchar(255) DEFAULT NULL,
  `AccountRef` varchar(255) DEFAULT NULL,
  `PolicyRef` varchar(255) DEFAULT NULL,
  `PayOffAmt` decimal(28,6) DEFAULT NULL,
  `ReversalStopInd` varchar(255) DEFAULT NULL,
  `CommissionKey` varchar(255) DEFAULT NULL,
  `PolicyTransactionEffectiveDt` date DEFAULT NULL,
  `AccountingDt` date DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `BillMethod` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `DepositoryLocationCd` varchar(255) DEFAULT NULL,
  `NetChargeAmt` decimal(28,6) DEFAULT NULL,
  `NetAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `GrossNetInd` varchar(255) DEFAULT NULL,
  `NetBalanceAmt` decimal(28,6) DEFAULT NULL,
  `CommissionTakenAmt` decimal(28,6) DEFAULT NULL,
  `TransactionDesc` varchar(255) DEFAULT NULL,
  `ReverseReason` varchar(255) DEFAULT NULL,
  `EntryDt` date DEFAULT NULL,
  `SystemCheckNumber` varchar(255) DEFAULT NULL,
  `AdjustmentTypeCd` varchar(255) DEFAULT NULL,
  `ACHAgent` varchar(255) DEFAULT NULL,
  `SettlementDt` date DEFAULT NULL,
  `AuditAccountInd` varchar(255) DEFAULT NULL,
  `PaymentServiceAccountId` varchar(255) DEFAULT NULL,
  `StatementAccountNumber` varchar(255) DEFAULT NULL,
  `StatementAccountRef` int(11) DEFAULT NULL,
  PRIMARY KEY (`SystemId`),
  KEY `AccountStats_1` (`PolicyNumber`),
  KEY `AccountStats_2` (`CombinedKey`),
  KEY `AccountStats_3` (`AddDt`),
  KEY `AccountStats_4` (`BookDt`),
  KEY `AccountStats_5` (`StatusCd`),
  KEY `AccountStats_6` (`AccountNumber`),
  KEY `AccountStats_7` (`PaidAmt`),
  KEY `AccountStats_8` (`AccountingDt`),
  KEY `AccountStats_Idx_1` (`CombinedKey`,`StatusCd`),
  KEY `AccountStats_Idx_2` (`AccountRef`,`AccountingDt`),
  KEY `AccountStats_Idx_3` (`PolicyRef`),
  KEY `AccountStats_Idx_4` (`CommissionKey`,`BookDt`),
  KEY `AccountStats_Idx_5` (`CommissionKey`,`AccountingDt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accountstats`
--

LOCK TABLES `accountstats` WRITE;
/*!40000 ALTER TABLE `accountstats` DISABLE KEYS */;
/*!40000 ALTER TABLE `accountstats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accountsummarystats`
--

DROP TABLE IF EXISTS `accountsummarystats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountsummarystats` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `ReportPeriod` varchar(255) DEFAULT NULL,
  `UpdateDt` date DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `BillingEntitySequenceNumber` varchar(255) DEFAULT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  `PolicyRef` int(11) DEFAULT NULL,
  `AccountRef` varchar(255) DEFAULT NULL,
  `AccountTypeCd` varchar(255) DEFAULT NULL,
  `PayPlanCd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `ProviderCd` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `NextDueDt` date DEFAULT NULL,
  `CommissionPct` decimal(28,6) DEFAULT NULL,
  `CommissionTypeCd` varchar(255) DEFAULT NULL,
  `PremMTDChargeAmt` decimal(28,6) DEFAULT NULL,
  `PremYTDChargeAmt` decimal(28,6) DEFAULT NULL,
  `PremTTDChargeAmt` decimal(28,6) DEFAULT NULL,
  `PremMTDCommissionChargeAmt` decimal(28,6) DEFAULT NULL,
  `PremTTDCommissionChargeAmt` decimal(28,6) DEFAULT NULL,
  `PremYTDCommissionChargeAmt` decimal(28,6) DEFAULT NULL,
  `PremBalanceAmt` decimal(28,6) DEFAULT NULL,
  `PremMTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `PremTTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `PremYTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `PremMTDAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `PremYTDAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `PremTTDAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `C1MTDChargeAmt` decimal(28,6) DEFAULT NULL,
  `C1TTDChargeAmt` decimal(28,6) DEFAULT NULL,
  `C1YTDChargeAmt` decimal(28,6) DEFAULT NULL,
  `C1MTDCommissionChargeAmt` decimal(28,6) DEFAULT NULL,
  `C1YTDCommissionChargeAmt` decimal(28,6) DEFAULT NULL,
  `C1TTDCommissionChargeAmt` decimal(28,6) DEFAULT NULL,
  `C1BalanceAmt` decimal(28,6) DEFAULT NULL,
  `C1MTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `C1YTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `C1TTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `C1MTDAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `C1TTDAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `C1YTDAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `C2MTDChargeAmt` decimal(28,6) DEFAULT NULL,
  `C2YTDChargeAmt` decimal(28,6) DEFAULT NULL,
  `C2MTDCommissionChargeAmt` decimal(28,6) DEFAULT NULL,
  `C2TTDChargeAmt` decimal(28,6) DEFAULT NULL,
  `C2TTDCommissionChargeAmt` decimal(28,6) DEFAULT NULL,
  `C2YTDCommissionChargeAmt` decimal(28,6) DEFAULT NULL,
  `C2BalanceAmt` decimal(28,6) DEFAULT NULL,
  `C2MTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `C2TTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `C2YTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `C2MTDAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `C2YTDAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `C2TTDAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `C3MTDChargeAmt` decimal(28,6) DEFAULT NULL,
  `C3TTDChargeAmt` decimal(28,6) DEFAULT NULL,
  `C3YTDChargeAmt` decimal(28,6) DEFAULT NULL,
  `C3MTDCommissionChargeAmt` decimal(28,6) DEFAULT NULL,
  `C3TTDCommissionChargeAmt` decimal(28,6) DEFAULT NULL,
  `C3BalanceAmt` decimal(28,6) DEFAULT NULL,
  `C3YTDCommissionChargeAmt` decimal(28,6) DEFAULT NULL,
  `C3MTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `C3TTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `C3MTDAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `C3YTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `C3TTDAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `C3YTDAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `C4MTDChargeAmt` decimal(28,6) DEFAULT NULL,
  `C4YTDChargeAmt` decimal(28,6) DEFAULT NULL,
  `C4MTDCommissionChargeAmt` decimal(28,6) DEFAULT NULL,
  `C4TTDChargeAmt` decimal(28,6) DEFAULT NULL,
  `C4TTDCommissionChargeAmt` decimal(28,6) DEFAULT NULL,
  `C4YTDCommissionChargeAmt` decimal(28,6) DEFAULT NULL,
  `C4BalanceAmt` decimal(28,6) DEFAULT NULL,
  `C4MTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `C4TTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `C4YTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `C4MTDAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `C4YTDAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `C4TTDAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `NSFCount` int(11) DEFAULT NULL,
  `LateCount` int(11) DEFAULT NULL,
  `ReinstatementCount` int(11) DEFAULT NULL,
  `CancellationCount` int(11) DEFAULT NULL,
  `CancellationNoticeCount` int(11) DEFAULT NULL,
  `LessOr30AmtDueDt` decimal(28,6) DEFAULT NULL,
  `Over30AmtDueDt` decimal(28,6) DEFAULT NULL,
  `Over60AmtDueDt` decimal(28,6) DEFAULT NULL,
  `Over90AmtDueDt` decimal(28,6) DEFAULT NULL,
  `LessOr30AmtEffectiveDt` decimal(28,6) DEFAULT NULL,
  `Over30AmtEffectiveDt` decimal(28,6) DEFAULT NULL,
  `Over60AmtEffectiveDt` decimal(28,6) DEFAULT NULL,
  `Over90AmtEffectiveDt` decimal(28,6) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `TotalMTDChargeAmt` decimal(28,6) DEFAULT NULL,
  `TotalTTDChargeAmt` decimal(28,6) DEFAULT NULL,
  `TotalYTDChargeAmt` decimal(28,6) DEFAULT NULL,
  `TotalMTDCommissionChargeAmt` decimal(28,6) DEFAULT NULL,
  `TotalTTDCommissionChargeAmt` decimal(28,6) DEFAULT NULL,
  `TotalYTDCommissionChargeAmt` decimal(28,6) DEFAULT NULL,
  `TotalMTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `TotalYTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `TotalTDDPaidAmt` decimal(28,6) DEFAULT NULL,
  `TotalMTDAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `TotalTTDAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `TotalYTDAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `PremPayOffBalanceAmt` decimal(28,6) DEFAULT NULL,
  `TotalBalanceAmt` decimal(28,6) DEFAULT NULL,
  `C1PayOffBalanceAmt` decimal(28,6) DEFAULT NULL,
  `C2PayOffBalanceAmt` decimal(28,6) DEFAULT NULL,
  `C3PayOffBalanceAmt` decimal(28,6) DEFAULT NULL,
  `C4PayOffBalanceAmt` decimal(28,6) DEFAULT NULL,
  `TotalPayOffBalanceAmt` decimal(28,6) DEFAULT NULL,
  `CurrentAmtDueDt` decimal(28,6) DEFAULT NULL,
  `CurrentAmtEffectiveDt` decimal(28,6) DEFAULT NULL,
  `StatementTypeCd` varchar(255) DEFAULT NULL,
  `BillMethod` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `DepositoryLocationCd` varchar(255) DEFAULT NULL,
  `FutureChargeDueAmt` decimal(28,6) DEFAULT NULL,
  `FuturePaidDueAmt` decimal(28,6) DEFAULT NULL,
  `FutureBalanceDueAmt` decimal(28,6) DEFAULT NULL,
  `FutureChargeEffectiveAmt` decimal(28,6) DEFAULT NULL,
  `FuturePaidEffectiveAmt` decimal(28,6) DEFAULT NULL,
  `FutureBalanceEffectiveAmt` decimal(28,6) DEFAULT NULL,
  `CurrentUnbilledAmtDueDt` decimal(28,6) DEFAULT NULL,
  `CurrentUnbilledAmtEffectiveDt` decimal(28,6) DEFAULT NULL,
  PRIMARY KEY (`SystemId`),
  KEY `AccountSummaryStats_Idx_1` (`AccountRef`,`StatusCd`),
  KEY `AccountSummaryStats_Idx_2` (`ReportPeriod`,`AccountRef`,`UpdateDt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accountsummarystats`
--

LOCK TABLES `accountsummarystats` WRITE;
/*!40000 ALTER TABLE `accountsummarystats` DISABLE KEYS */;
/*!40000 ALTER TABLE `accountsummarystats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `application`
--

DROP TABLE IF EXISTS `application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `application`
--

LOCK TABLES `application` WRITE;
/*!40000 ALTER TABLE `application` DISABLE KEYS */;
/*!40000 ALTER TABLE `application` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applicationbaseline`
--

DROP TABLE IF EXISTS `applicationbaseline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicationbaseline` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicationbaseline`
--

LOCK TABLES `applicationbaseline` WRITE;
/*!40000 ALTER TABLE `applicationbaseline` DISABLE KEYS */;
/*!40000 ALTER TABLE `applicationbaseline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applicationbaselinelookup`
--

DROP TABLE IF EXISTS `applicationbaselinelookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicationbaselinelookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicationbaselinelookup`
--

LOCK TABLES `applicationbaselinelookup` WRITE;
/*!40000 ALTER TABLE `applicationbaselinelookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `applicationbaselinelookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applicationlookup`
--

DROP TABLE IF EXISTS `applicationlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicationlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicationlookup`
--

LOCK TABLES `applicationlookup` WRITE;
/*!40000 ALTER TABLE `applicationlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `applicationlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attachment`
--

DROP TABLE IF EXISTS `attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachment` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attachment`
--

LOCK TABLES `attachment` WRITE;
/*!40000 ALTER TABLE `attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attachmentlookup`
--

DROP TABLE IF EXISTS `attachmentlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachmentlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attachmentlookup`
--

LOCK TABLES `attachmentlookup` WRITE;
/*!40000 ALTER TABLE `attachmentlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `attachmentlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bankaccount`
--

DROP TABLE IF EXISTS `bankaccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bankaccount` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bankaccount`
--

LOCK TABLES `bankaccount` WRITE;
/*!40000 ALTER TABLE `bankaccount` DISABLE KEYS */;
/*!40000 ALTER TABLE `bankaccount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bankaccountlookup`
--

DROP TABLE IF EXISTS `bankaccountlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bankaccountlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bankaccountlookup`
--

LOCK TABLES `bankaccountlookup` WRITE;
/*!40000 ALTER TABLE `bankaccountlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `bankaccountlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batchprint`
--

DROP TABLE IF EXISTS `batchprint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batchprint` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batchprint`
--

LOCK TABLES `batchprint` WRITE;
/*!40000 ALTER TABLE `batchprint` DISABLE KEYS */;
/*!40000 ALTER TABLE `batchprint` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batchprintdocument`
--

DROP TABLE IF EXISTS `batchprintdocument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batchprintdocument` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batchprintdocument`
--

LOCK TABLES `batchprintdocument` WRITE;
/*!40000 ALTER TABLE `batchprintdocument` DISABLE KEYS */;
/*!40000 ALTER TABLE `batchprintdocument` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batchprintdocumentlookup`
--

DROP TABLE IF EXISTS `batchprintdocumentlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batchprintdocumentlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batchprintdocumentlookup`
--

LOCK TABLES `batchprintdocumentlookup` WRITE;
/*!40000 ALTER TABLE `batchprintdocumentlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `batchprintdocumentlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batchprintlookup`
--

DROP TABLE IF EXISTS `batchprintlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batchprintlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batchprintlookup`
--

LOCK TABLES `batchprintlookup` WRITE;
/*!40000 ALTER TABLE `batchprintlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `batchprintlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batchreceipt`
--

DROP TABLE IF EXISTS `batchreceipt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batchreceipt` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batchreceipt`
--

LOCK TABLES `batchreceipt` WRITE;
/*!40000 ALTER TABLE `batchreceipt` DISABLE KEYS */;
/*!40000 ALTER TABLE `batchreceipt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batchreceiptlookup`
--

DROP TABLE IF EXISTS `batchreceiptlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batchreceiptlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batchreceiptlookup`
--

LOCK TABLES `batchreceiptlookup` WRITE;
/*!40000 ALTER TABLE `batchreceiptlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `batchreceiptlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batchreturn`
--

DROP TABLE IF EXISTS `batchreturn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batchreturn` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batchreturn`
--

LOCK TABLES `batchreturn` WRITE;
/*!40000 ALTER TABLE `batchreturn` DISABLE KEYS */;
/*!40000 ALTER TABLE `batchreturn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batchreturnlookup`
--

DROP TABLE IF EXISTS `batchreturnlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batchreturnlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batchreturnlookup`
--

LOCK TABLES `batchreturnlookup` WRITE;
/*!40000 ALTER TABLE `batchreturnlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `batchreturnlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batchwriteoff`
--

DROP TABLE IF EXISTS `batchwriteoff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batchwriteoff` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batchwriteoff`
--

LOCK TABLES `batchwriteoff` WRITE;
/*!40000 ALTER TABLE `batchwriteoff` DISABLE KEYS */;
/*!40000 ALTER TABLE `batchwriteoff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batchwriteofflookup`
--

DROP TABLE IF EXISTS `batchwriteofflookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batchwriteofflookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batchwriteofflookup`
--

LOCK TABLES `batchwriteofflookup` WRITE;
/*!40000 ALTER TABLE `batchwriteofflookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `batchwriteofflookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bgprocessorstatus`
--

DROP TABLE IF EXISTS `bgprocessorstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bgprocessorstatus` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bgprocessorstatus`
--

LOCK TABLES `bgprocessorstatus` WRITE;
/*!40000 ALTER TABLE `bgprocessorstatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `bgprocessorstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bgprocessorstatuslookup`
--

DROP TABLE IF EXISTS `bgprocessorstatuslookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bgprocessorstatuslookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bgprocessorstatuslookup`
--

LOCK TABLES `bgprocessorstatuslookup` WRITE;
/*!40000 ALTER TABLE `bgprocessorstatuslookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `bgprocessorstatuslookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cabinet`
--

DROP TABLE IF EXISTS `cabinet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cabinet` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cabinet`
--

LOCK TABLES `cabinet` WRITE;
/*!40000 ALTER TABLE `cabinet` DISABLE KEYS */;
/*!40000 ALTER TABLE `cabinet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cabinetlookup`
--

DROP TABLE IF EXISTS `cabinetlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cabinetlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cabinetlookup`
--

LOCK TABLES `cabinetlookup` WRITE;
/*!40000 ALTER TABLE `cabinetlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `cabinetlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendarexception`
--

DROP TABLE IF EXISTS `calendarexception`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendarexception` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendarexception`
--

LOCK TABLES `calendarexception` WRITE;
/*!40000 ALTER TABLE `calendarexception` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendarexception` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendarexceptionlookup`
--

DROP TABLE IF EXISTS `calendarexceptionlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendarexceptionlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendarexceptionlookup`
--

LOCK TABLES `calendarexceptionlookup` WRITE;
/*!40000 ALTER TABLE `calendarexceptionlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendarexceptionlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catastrophe`
--

DROP TABLE IF EXISTS `catastrophe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catastrophe` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catastrophe`
--

LOCK TABLES `catastrophe` WRITE;
/*!40000 ALTER TABLE `catastrophe` DISABLE KEYS */;
/*!40000 ALTER TABLE `catastrophe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catastrophelookup`
--

DROP TABLE IF EXISTS `catastrophelookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catastrophelookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catastrophelookup`
--

LOCK TABLES `catastrophelookup` WRITE;
/*!40000 ALTER TABLE `catastrophelookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `catastrophelookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `claim`
--

DROP TABLE IF EXISTS `claim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claim` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `claim`
--

LOCK TABLES `claim` WRITE;
/*!40000 ALTER TABLE `claim` DISABLE KEYS */;
/*!40000 ALTER TABLE `claim` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `claimbaseline`
--

DROP TABLE IF EXISTS `claimbaseline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimbaseline` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `claimbaseline`
--

LOCK TABLES `claimbaseline` WRITE;
/*!40000 ALTER TABLE `claimbaseline` DISABLE KEYS */;
/*!40000 ALTER TABLE `claimbaseline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `claimbaselinelookup`
--

DROP TABLE IF EXISTS `claimbaselinelookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimbaselinelookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `claimbaselinelookup`
--

LOCK TABLES `claimbaselinelookup` WRITE;
/*!40000 ALTER TABLE `claimbaselinelookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `claimbaselinelookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `claimlookup`
--

DROP TABLE IF EXISTS `claimlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `claimlookup`
--

LOCK TABLES `claimlookup` WRITE;
/*!40000 ALTER TABLE `claimlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `claimlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `claimreinsurancestats`
--

DROP TABLE IF EXISTS `claimreinsurancestats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimreinsurancestats` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `StatSequence` int(11) DEFAULT NULL,
  `StatSequenceReplace` int(11) DEFAULT NULL,
  `PolicyRef` int(11) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `CombinedKey` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `PolicyYear` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `ReinsuranceName` varchar(255) DEFAULT NULL,
  `ReinsuranceItemName` varchar(255) DEFAULT NULL,
  `ReserveCd` varchar(255) DEFAULT NULL,
  `ReserveTypeCd` varchar(255) DEFAULT NULL,
  `MasterSubInd` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `ClaimNumber` varchar(255) DEFAULT NULL,
  `ClaimRef` int(11) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `LossYear` varchar(255) DEFAULT NULL,
  `ExpectedRecoveryChangeAmt` decimal(28,6) DEFAULT NULL,
  `ReserveChangeAmt` decimal(28,6) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `PostedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `LossDt` date DEFAULT NULL,
  `ReportDt` date DEFAULT NULL,
  `LossCauseCd` varchar(255) DEFAULT NULL,
  `SubLossCauseCd` varchar(255) DEFAULT NULL,
  `ClaimStatusCd` varchar(255) DEFAULT NULL,
  `ProductName` varchar(255) DEFAULT NULL,
  `PolicyProductVersionIdRef` varchar(255) DEFAULT NULL,
  `PolicyProductName` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  `PolicyGroupCd` varchar(255) DEFAULT NULL,
  `ReversalStopInd` varchar(255) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `SummaryKey` varchar(255) DEFAULT NULL,
  `RecoveryTransactionCd` varchar(255) DEFAULT NULL,
  `CatastropheRef` varchar(255) DEFAULT NULL,
  `RecoveryTransactionNumber` int(11) DEFAULT NULL,
  `CatastropheNumber` varchar(255) DEFAULT NULL,
  `HistoricReserveAmt` decimal(28,6) DEFAULT NULL,
  `HistoricExpectedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `HistoricPaidAmt` decimal(28,6) DEFAULT NULL,
  `ReinsuranceCoverageGroupCd` varchar(255) DEFAULT NULL,
  `ReinsuranceReserveCd` varchar(255) DEFAULT NULL,
  `CoverageTriggerCd` varchar(255) DEFAULT NULL,
  `AwarenessDt` date DEFAULT NULL,
  PRIMARY KEY (`SystemId`),
  KEY `ClaimReinsuranceStats_1` (`PolicyNumber`),
  KEY `ClaimReinsuranceStats_2` (`CombinedKey`),
  KEY `ClaimReinsuranceStats_3` (`StatusCd`),
  KEY `ClaimReinsuranceStats_4` (`SummaryKey`),
  KEY `ClaimReinsuranceStatI1` (`CombinedKey`,`StatusCd`),
  KEY `ClaimReinsuranceStatI2` (`CombinedKey`,`BookDt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `claimreinsurancestats`
--

LOCK TABLES `claimreinsurancestats` WRITE;
/*!40000 ALTER TABLE `claimreinsurancestats` DISABLE KEYS */;
/*!40000 ALTER TABLE `claimreinsurancestats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `claimreinsurancesummarystats`
--

DROP TABLE IF EXISTS `claimreinsurancesummarystats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimreinsurancesummarystats` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `ReportPeriod` varchar(255) DEFAULT NULL,
  `UpdateDt` date DEFAULT NULL,
  `PolicyRef` int(11) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `SummaryKey` varchar(255) DEFAULT NULL,
  `PolicyYear` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `ReinsuranceName` varchar(255) DEFAULT NULL,
  `ReinsuranceItemName` varchar(255) DEFAULT NULL,
  `ReserveCd` varchar(255) DEFAULT NULL,
  `ReserveTypeCd` varchar(255) DEFAULT NULL,
  `MasterSubInd` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `ClaimNumber` varchar(255) DEFAULT NULL,
  `ClaimRef` int(11) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `LossYear` varchar(255) DEFAULT NULL,
  `OutstandingAmt` decimal(28,6) DEFAULT NULL,
  `MTDReserveChangeAmt` decimal(28,6) DEFAULT NULL,
  `YTDReserveChangeAmt` decimal(28,6) DEFAULT NULL,
  `OutstandingExpectedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `MTDExpectedRecoveryChangeAmt` decimal(28,6) DEFAULT NULL,
  `YTDExpectedRecoveryChangeAmt` decimal(28,6) DEFAULT NULL,
  `ITDPaidAmt` decimal(28,6) DEFAULT NULL,
  `MTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `YTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `ITDPostedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `MTDPostedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `YTDPostedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `LossDt` date DEFAULT NULL,
  `ReportDt` date DEFAULT NULL,
  `LossCauseCd` varchar(255) DEFAULT NULL,
  `SubLossCauseCd` varchar(255) DEFAULT NULL,
  `ClaimStatusCd` varchar(255) DEFAULT NULL,
  `ProductName` varchar(255) DEFAULT NULL,
  `PolicyProductVersionIdRef` varchar(255) DEFAULT NULL,
  `PolicyProductName` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  `PolicyGroupCd` varchar(255) DEFAULT NULL,
  `MTDIncurredAmt` decimal(28,6) DEFAULT NULL,
  `YTDIncurredAmt` decimal(28,6) DEFAULT NULL,
  `ITDIncurredAmt` decimal(28,6) DEFAULT NULL,
  `MTDIncurredNetRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `YTDIncurredNetRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ITDIncurredNetRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `CatastropheRef` varchar(255) DEFAULT NULL,
  `CatastropheNumber` varchar(255) DEFAULT NULL,
  `HistoricPaidAmt` decimal(28,6) DEFAULT NULL,
  `ReinsuranceCoverageGroupCd` varchar(255) DEFAULT NULL,
  `ReinsuranceReserveCd` varchar(255) DEFAULT NULL,
  `CoverageTriggerCd` varchar(255) DEFAULT NULL,
  `AwarenessDt` date DEFAULT NULL,
  PRIMARY KEY (`SystemId`),
  KEY `ClaimReinsuranceSummaryStats_1` (`PolicyNumber`),
  KEY `ClaimReinsuranceSummaryStats_2` (`SummaryKey`),
  KEY `ClaimReinsuranceSummaryStats_3` (`StatusCd`),
  KEY `ClaimReinSumStatsI1` (`SummaryKey`,`ReportPeriod`,`UpdateDt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `claimreinsurancesummarystats`
--

LOCK TABLES `claimreinsurancesummarystats` WRITE;
/*!40000 ALTER TABLE `claimreinsurancesummarystats` DISABLE KEYS */;
/*!40000 ALTER TABLE `claimreinsurancesummarystats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `claimstats`
--

DROP TABLE IF EXISTS `claimstats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimstats` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `StatSequence` int(11) DEFAULT NULL,
  `StatSequenceReplace` int(11) DEFAULT NULL,
  `CombinedKey` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `PolicyRef` int(11) DEFAULT NULL,
  `LineCd` varchar(255) DEFAULT NULL,
  `RiskCd` varchar(255) DEFAULT NULL,
  `CoverageCd` varchar(255) DEFAULT NULL,
  `CoverageItemCd` varchar(255) DEFAULT NULL,
  `PolicyLimit` varchar(255) DEFAULT NULL,
  `PolicyDeductible` varchar(255) DEFAULT NULL,
  `Limit` varchar(255) DEFAULT NULL,
  `Deductible` varchar(255) DEFAULT NULL,
  `LimitDescription` varchar(255) DEFAULT NULL,
  `DeductibleDescription` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `ClaimNumber` varchar(255) DEFAULT NULL,
  `ClaimRef` int(11) DEFAULT NULL,
  `ClaimantCd` varchar(255) DEFAULT NULL,
  `FeatureCd` varchar(255) DEFAULT NULL,
  `FeatureSubCd` varchar(255) DEFAULT NULL,
  `FeatureTypeCd` varchar(255) DEFAULT NULL,
  `ItemNumber` int(11) DEFAULT NULL,
  `ReserveCd` varchar(255) DEFAULT NULL,
  `ReserveTypeCd` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `ClaimantTransactionCd` varchar(255) DEFAULT NULL,
  `ClaimantTransactionNumber` int(11) DEFAULT NULL,
  `ClaimantTransactionIdRef` varchar(255) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `PolicyYear` varchar(255) DEFAULT NULL,
  `LossYear` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `ProducerProviderCd` varchar(255) DEFAULT NULL,
  `ProducerProviderRef` int(11) DEFAULT NULL,
  `InsuranceTypeCd` varchar(255) DEFAULT NULL,
  `ReserveChangeAmt` decimal(28,6) DEFAULT NULL,
  `ExpectedRecoveryChangeAmt` decimal(28,6) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `PostedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `AdjusterProviderCd` varchar(255) DEFAULT NULL,
  `AdjusterProviderRef` int(11) DEFAULT NULL,
  `ExaminerProviderCd` varchar(255) DEFAULT NULL,
  `ExaminerProviderRef` int(11) DEFAULT NULL,
  `BranchCd` varchar(255) DEFAULT NULL,
  `PayToName` varchar(255) DEFAULT NULL,
  `PaymentAccountCd` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `RateAreaName` varchar(255) DEFAULT NULL,
  `StatData` varchar(255) DEFAULT NULL,
  `ClaimStatusCd` varchar(255) DEFAULT NULL,
  `ClaimantStatusCd` varchar(255) DEFAULT NULL,
  `FeatureStatusCd` varchar(255) DEFAULT NULL,
  `ReserveStatusCd` varchar(255) DEFAULT NULL,
  `LossDt` date DEFAULT NULL,
  `ReportDt` date DEFAULT NULL,
  `ClaimStatusChgInd` varchar(255) DEFAULT NULL,
  `ClaimantStatusChgInd` varchar(255) DEFAULT NULL,
  `FeatureStatusChgInd` varchar(255) DEFAULT NULL,
  `ReserveStatusChgInd` varchar(255) DEFAULT NULL,
  `SummaryKey` varchar(255) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `LossCauseCd` varchar(255) DEFAULT NULL,
  `SubLossCauseCd` varchar(255) DEFAULT NULL,
  `ProductName` varchar(255) DEFAULT NULL,
  `PolicyProductVersionIdRef` varchar(255) DEFAULT NULL,
  `PolicyProductName` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  `PolicyGroupCd` varchar(255) DEFAULT NULL,
  `ReversalStopInd` varchar(255) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `AggregateLimit` varchar(255) DEFAULT NULL,
  `AggregateLimitDescription` varchar(255) DEFAULT NULL,
  `CheckDt` date DEFAULT NULL,
  `CheckNumber` varchar(255) DEFAULT NULL,
  `CheckAmt` varchar(255) DEFAULT NULL,
  `CatastropheRef` varchar(255) DEFAULT NULL,
  `CatastropheNumber` varchar(255) DEFAULT NULL,
  `PropertyDamagedIdRef` varchar(255) DEFAULT NULL,
  `PropertyDamagedNumber` varchar(255) DEFAULT NULL,
  `RecordOnly` varchar(255) DEFAULT NULL,
  `FileReasonCd` varchar(255) DEFAULT NULL,
  `SystemCheckReference` varchar(255) DEFAULT NULL,
  `ClaimantLinkIdRef` varchar(255) DEFAULT NULL,
  `ServicePeriodStartDt` date DEFAULT NULL,
  `ServicePeriodEndDt` date DEFAULT NULL,
  `HistoricReserveAmt` decimal(28,6) DEFAULT NULL,
  `HistoricExpectedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `HistoricPaidAmt` decimal(28,6) DEFAULT NULL,
  `HistoricPostedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ReasonCd` varchar(255) DEFAULT NULL,
  `DenyTypeCd` varchar(255) DEFAULT NULL,
  `DenyReasonCd` varchar(255) DEFAULT NULL,
  `NotToRepairInd` varchar(255) DEFAULT NULL,
  `ClaimantMaritalStatusCd` varchar(255) DEFAULT NULL,
  `ClaimantNumberOfDependents` varchar(255) DEFAULT NULL,
  `ClaimantNatureCd` varchar(255) DEFAULT NULL,
  `ClaimantBodyPartGeneralCd` varchar(255) DEFAULT NULL,
  `ClaimantBodyPartSpecificCd` varchar(255) DEFAULT NULL,
  `ClaimantBodyPartIAIABCCd` varchar(255) DEFAULT NULL,
  `ClaimantInitialTreatment` varchar(255) DEFAULT NULL,
  `ClaimantDisabilityTypeCd` varchar(255) DEFAULT NULL,
  `ClaimantClassCd` varchar(255) DEFAULT NULL,
  `ClaimantEmploymentStatusCd` varchar(255) DEFAULT NULL,
  `ClaimantShiftCd` varchar(255) DEFAULT NULL,
  `ClaimantHourlySalaryCd` varchar(255) DEFAULT NULL,
  `ClaimantAverageWeeklyWagesAmt` varchar(255) DEFAULT NULL,
  `ClaimantHireDt` date DEFAULT NULL,
  `ClaimantLastWorkedDt` date DEFAULT NULL,
  `ShortDesc` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL,
  `State` varchar(255) DEFAULT NULL,
  `FPLiabilityPer` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `ZipCd` varchar(255) DEFAULT NULL,
  `CoverageDesc` varchar(255) DEFAULT NULL,
  `PayToVendor` varchar(255) DEFAULT NULL,
  `ConversionGrp` varchar(255) DEFAULT NULL,
  `CoverageTriggerCd` varchar(255) DEFAULT NULL,
  `AwarenessDt` date DEFAULT NULL,
  PRIMARY KEY (`SystemId`),
  KEY `ClaimStats_1` (`CombinedKey`),
  KEY `ClaimStats_2` (`ClaimNumber`),
  KEY `ClaimStats_3` (`BookDt`),
  KEY `ClaimStats_4` (`StatusCd`),
  KEY `ClaimStats_Idx_1` (`CombinedKey`,`StatusCd`),
  KEY `ClaimStats_Idx_2` (`SummaryKey`,`BookDt`),
  KEY `ClaimStats_Idx_3` (`ClaimRef`,`ClaimantTransactionNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `claimstats`
--

LOCK TABLES `claimstats` WRITE;
/*!40000 ALTER TABLE `claimstats` DISABLE KEYS */;
/*!40000 ALTER TABLE `claimstats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `claimsummarystats`
--

DROP TABLE IF EXISTS `claimsummarystats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimsummarystats` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `ReportPeriod` varchar(255) DEFAULT NULL,
  `UpdateDt` date DEFAULT NULL,
  `SummaryKey` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `PolicyRef` int(11) DEFAULT NULL,
  `LineCd` varchar(255) DEFAULT NULL,
  `RiskCd` varchar(255) DEFAULT NULL,
  `CoverageCd` varchar(255) DEFAULT NULL,
  `CoverageItemCd` varchar(255) DEFAULT NULL,
  `PolicyLimit` varchar(255) DEFAULT NULL,
  `PolicyDeductible` varchar(255) DEFAULT NULL,
  `Limit` varchar(255) DEFAULT NULL,
  `Deductible` varchar(255) DEFAULT NULL,
  `LimitDescription` varchar(255) DEFAULT NULL,
  `DeductibleDescription` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `ClaimNumber` varchar(255) DEFAULT NULL,
  `ClaimRef` int(11) DEFAULT NULL,
  `ClaimantCd` varchar(255) DEFAULT NULL,
  `FeatureCd` varchar(255) DEFAULT NULL,
  `FeatureSubCd` varchar(255) DEFAULT NULL,
  `FeatureTypeCd` varchar(255) DEFAULT NULL,
  `ItemNumber` int(11) DEFAULT NULL,
  `ReserveCd` varchar(255) DEFAULT NULL,
  `SubReserveCd` varchar(255) DEFAULT NULL,
  `ReserveTypeCd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `LossYear` varchar(255) DEFAULT NULL,
  `PolicyYear` varchar(255) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `InsuranceTypeCd` varchar(255) DEFAULT NULL,
  `LitigationInd` varchar(255) DEFAULT NULL,
  `LossDt` date DEFAULT NULL,
  `ReportDt` date DEFAULT NULL,
  `FirstClosedDt` date DEFAULT NULL,
  `OpenedDt` date DEFAULT NULL,
  `ClosedDt` date DEFAULT NULL,
  `DenialDt` date DEFAULT NULL,
  `FirstIndemnityPaymentDt` date DEFAULT NULL,
  `MTDReserveChangeAmt` decimal(28,6) DEFAULT NULL,
  `OutstandingAmt` decimal(28,6) DEFAULT NULL,
  `YTDReserveChangeAmt` decimal(28,6) DEFAULT NULL,
  `IncreaseDecreaseAmt` decimal(28,6) DEFAULT NULL,
  `MTDExpectedRecoveryChangeAmt` decimal(28,6) DEFAULT NULL,
  `OutstandingExpectedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `YTDExpectedRecoveryChangeAmt` decimal(28,6) DEFAULT NULL,
  `MTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `YTDPaidAmt` decimal(28,6) DEFAULT NULL,
  `ITDPaidAmt` decimal(28,6) DEFAULT NULL,
  `MTDPostedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ITDPostedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `YTDPostedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `OpeningReserve` decimal(28,6) DEFAULT NULL,
  `OriginalReserve` decimal(28,6) DEFAULT NULL,
  `OpenedInPeriodCount` int(11) DEFAULT NULL,
  `ReOpenedInPeriodCount` int(11) DEFAULT NULL,
  `ClosedInPeriodCount` int(11) DEFAULT NULL,
  `OpenEndOfPeriodCount` int(11) DEFAULT NULL,
  `ClaimantOpenCount` int(11) DEFAULT NULL,
  `ClaimOpenCount` int(11) DEFAULT NULL,
  `FeatureOpenCount` int(11) DEFAULT NULL,
  `ClaimLastCloseDt` date DEFAULT NULL,
  `ClaimantLastCloseDt` date DEFAULT NULL,
  `FeatureLastCloseDt` date DEFAULT NULL,
  `ReserveLastCloseDt` date DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `LossCauseCd` varchar(255) DEFAULT NULL,
  `SubLossCauseCd` varchar(255) DEFAULT NULL,
  `ProducerProviderCd` varchar(255) DEFAULT NULL,
  `ClaimStatusCd` varchar(255) DEFAULT NULL,
  `ProductName` varchar(255) DEFAULT NULL,
  `PolicyProductVersionIdRef` varchar(255) DEFAULT NULL,
  `PolicyProductName` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  `PolicyGroupCd` varchar(255) DEFAULT NULL,
  `MTDIncurredAmt` decimal(28,6) DEFAULT NULL,
  `YTDIncurredAmt` decimal(28,6) DEFAULT NULL,
  `ITDIncurredAmt` decimal(28,6) DEFAULT NULL,
  `MTDIncurredNetRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `YTDIncurredNetRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ITDIncurredNetRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `AggregateLimit` varchar(255) DEFAULT NULL,
  `AggregateLimitDescription` varchar(255) DEFAULT NULL,
  `CatastropheRef` varchar(255) DEFAULT NULL,
  `CatastropheNumber` varchar(255) DEFAULT NULL,
  `PropertyDamagedIdRef` varchar(255) DEFAULT NULL,
  `PropertyDamagedNumber` varchar(255) DEFAULT NULL,
  `RecordOnly` varchar(255) DEFAULT NULL,
  `FileReasonCd` varchar(255) DEFAULT NULL,
  `ClaimantLinkIdRef` varchar(255) DEFAULT NULL,
  `HistoricPaidAmt` decimal(28,6) DEFAULT NULL,
  `HistoricPostedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `DenyTypeCd` varchar(255) DEFAULT NULL,
  `DenyReasonCd` varchar(255) DEFAULT NULL,
  `ExaminerProviderCd` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL,
  `ShortDesc` varchar(255) DEFAULT NULL,
  `FPLiabilityPer` varchar(255) DEFAULT NULL,
  `State` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `ZipCd` varchar(255) DEFAULT NULL,
  `CoverageDesc` varchar(255) DEFAULT NULL,
  `PayToVendor` varchar(255) DEFAULT NULL,
  `CoverageTriggerCd` varchar(255) DEFAULT NULL,
  `AwarenessDt` date DEFAULT NULL,
  PRIMARY KEY (`SystemId`),
  KEY `ClaimSummaryStats_Idx_1` (`ReportPeriod`,`SummaryKey`,`UpdateDt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `claimsummarystats`
--

LOCK TABLES `claimsummarystats` WRITE;
/*!40000 ALTER TABLE `claimsummarystats` DISABLE KEYS */;
/*!40000 ALTER TABLE `claimsummarystats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commissiondetail`
--

DROP TABLE IF EXISTS `commissiondetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commissiondetail` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `ChargedAmt` decimal(28,6) DEFAULT NULL,
  `CommissionableAmt` decimal(28,6) DEFAULT NULL,
  `CommissionAmt` decimal(28,6) DEFAULT NULL,
  `CommissionArea` varchar(255) DEFAULT NULL,
  `CommissionKey` varchar(255) DEFAULT NULL,
  `CommissionPct` decimal(28,6) DEFAULT NULL,
  `CorrectionInd` varchar(255) DEFAULT NULL,
  `CorrectionReason` varchar(255) DEFAULT NULL,
  `CorrectionReasonCd` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `OriginalPayThroughDt` date DEFAULT NULL,
  `OtherFeeAmt` decimal(28,6) DEFAULT NULL,
  `PaidAccount` varchar(255) DEFAULT NULL,
  `PaidItemDt` date DEFAULT NULL,
  `PaidItemNumber` varchar(255) DEFAULT NULL,
  `PaidMethodCd` varchar(255) DEFAULT NULL,
  `PaidStatusCd` varchar(255) DEFAULT NULL,
  `PaymentRequestedDt` date DEFAULT NULL,
  `PaymentSourceRef` varchar(255) DEFAULT NULL,
  `PayThroughDt` date DEFAULT NULL,
  `PayToCd` varchar(255) DEFAULT NULL,
  `PayToTypeCd` varchar(255) DEFAULT NULL,
  `PolicyFeeAmt` decimal(28,6) DEFAULT NULL,
  `PremiumAmt` decimal(28,6) DEFAULT NULL,
  `ProducerAgency` varchar(255) DEFAULT NULL,
  `ProducerAgencyProviderRef` varchar(255) DEFAULT NULL,
  `ProducerGroup` varchar(255) DEFAULT NULL,
  `ProducerGroupProviderRef` varchar(255) DEFAULT NULL,
  `ProviderCd` varchar(255) DEFAULT NULL,
  `ProviderRef` varchar(255) DEFAULT NULL,
  `ReceivedAmt` decimal(28,6) DEFAULT NULL,
  `SourceEffectiveDt` date DEFAULT NULL,
  `SourceNumber` varchar(255) DEFAULT NULL,
  `SourceRef` varchar(255) DEFAULT NULL,
  `SourceTransactionNumber` int(11) DEFAULT NULL,
  `SourceVersion` varchar(255) DEFAULT NULL,
  `SubTypeCd` varchar(255) DEFAULT NULL,
  `TaxesAmt` decimal(28,6) DEFAULT NULL,
  `TransactionCheckAmt` decimal(28,6) DEFAULT NULL,
  `TransactionCheckNumber` varchar(255) DEFAULT NULL,
  `TransactionEffectiveDt` date DEFAULT NULL,
  `TransactionEntryDt` date DEFAULT NULL,
  `TransactionEntryTm` varchar(255) DEFAULT NULL,
  `TransactionTypeCd` varchar(255) DEFAULT NULL,
  `Type` varchar(255) DEFAULT NULL,
  `WrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `PayToRef` varchar(255) DEFAULT NULL,
  `OriginalCommissionDetailRef` int(11) DEFAULT NULL,
  PRIMARY KEY (`SystemId`),
  KEY `CommissionDetail_1` (`CommissionArea`),
  KEY `CommissionDetail_2` (`CorrectionInd`),
  KEY `CommissionDetail_3` (`PaymentRequestedDt`),
  KEY `CommissionDetail_4` (`PayThroughDt`),
  KEY `CommissionDetail_5` (`ProviderCd`),
  KEY `CommissionDetail_6` (`ProviderRef`),
  KEY `CommissionDetail_7` (`SourceNumber`),
  KEY `CommissionDetail_8` (`SourceRef`),
  KEY `CommissionDetail_9` (`SourceTransactionNumber`),
  KEY `CommissionDetail_10` (`TaxesAmt`),
  KEY `CommissionDetail_11` (`Type`),
  KEY `CommissionDetail_Idx_1` (`ProviderRef`,`SourceNumber`,`SourceTransactionNumber`,`CommissionArea`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commissiondetail`
--

LOCK TABLES `commissiondetail` WRITE;
/*!40000 ALTER TABLE `commissiondetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `commissiondetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commissionplan`
--

DROP TABLE IF EXISTS `commissionplan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commissionplan` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `PlanCd` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `State` varchar(255) DEFAULT NULL,
  `LicenceClass` varchar(255) DEFAULT NULL,
  `CommissionArea` varchar(255) DEFAULT NULL,
  `BusinessSource` varchar(255) DEFAULT NULL,
  `CodeCd` varchar(255) DEFAULT NULL,
  `NewBusiness` varchar(255) DEFAULT NULL,
  `Renewal` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commissionplan`
--

LOCK TABLES `commissionplan` WRITE;
/*!40000 ALTER TABLE `commissionplan` DISABLE KEYS */;
/*!40000 ALTER TABLE `commissionplan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `counters`
--

DROP TABLE IF EXISTS `counters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `counters` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `counters`
--

LOCK TABLES `counters` WRITE;
/*!40000 ALTER TABLE `counters` DISABLE KEYS */;
/*!40000 ALTER TABLE `counters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `counterslookup`
--

DROP TABLE IF EXISTS `counterslookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `counterslookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `counterslookup`
--

LOCK TABLES `counterslookup` WRITE;
/*!40000 ALTER TABLE `counterslookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `counterslookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpcarriercontrib`
--

DROP TABLE IF EXISTS `cpcarriercontrib`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpcarriercontrib` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpcarriercontrib`
--

LOCK TABLES `cpcarriercontrib` WRITE;
/*!40000 ALTER TABLE `cpcarriercontrib` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpcarriercontrib` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpcarriercontriblookup`
--

DROP TABLE IF EXISTS `cpcarriercontriblookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpcarriercontriblookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpcarriercontriblookup`
--

LOCK TABLES `cpcarriercontriblookup` WRITE;
/*!40000 ALTER TABLE `cpcarriercontriblookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpcarriercontriblookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpcluecontrib`
--

DROP TABLE IF EXISTS `cpcluecontrib`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpcluecontrib` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpcluecontrib`
--

LOCK TABLES `cpcluecontrib` WRITE;
/*!40000 ALTER TABLE `cpcluecontrib` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpcluecontrib` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpcluecontriblookup`
--

DROP TABLE IF EXISTS `cpcluecontriblookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpcluecontriblookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpcluecontriblookup`
--

LOCK TABLES `cpcluecontriblookup` WRITE;
/*!40000 ALTER TABLE `cpcluecontriblookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpcluecontriblookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpclupcontrib`
--

DROP TABLE IF EXISTS `cpclupcontrib`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpclupcontrib` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpclupcontrib`
--

LOCK TABLES `cpclupcontrib` WRITE;
/*!40000 ALTER TABLE `cpclupcontrib` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpclupcontrib` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpclupcontriblookup`
--

DROP TABLE IF EXISTS `cpclupcontriblookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpclupcontriblookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpclupcontriblookup`
--

LOCK TABLES `cpclupcontriblookup` WRITE;
/*!40000 ALTER TABLE `cpclupcontriblookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpclupcontriblookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customerlogin`
--

DROP TABLE IF EXISTS `customerlogin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerlogin` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customerlogin`
--

LOCK TABLES `customerlogin` WRITE;
/*!40000 ALTER TABLE `customerlogin` DISABLE KEYS */;
/*!40000 ALTER TABLE `customerlogin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customerloginlookup`
--

DROP TABLE IF EXISTS `customerloginlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerloginlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customerloginlookup`
--

LOCK TABLES `customerloginlookup` WRITE;
/*!40000 ALTER TABLE `customerloginlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `customerloginlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customerlookup`
--

DROP TABLE IF EXISTS `customerlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customerlookup`
--

LOCK TABLES `customerlookup` WRITE;
/*!40000 ALTER TABLE `customerlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `customerlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datareport`
--

DROP TABLE IF EXISTS `datareport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datareport` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datareport`
--

LOCK TABLES `datareport` WRITE;
/*!40000 ALTER TABLE `datareport` DISABLE KEYS */;
/*!40000 ALTER TABLE `datareport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datareportlookup`
--

DROP TABLE IF EXISTS `datareportlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datareportlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datareportlookup`
--

LOCK TABLES `datareportlookup` WRITE;
/*!40000 ALTER TABLE `datareportlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `datareportlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distribution`
--

DROP TABLE IF EXISTS `distribution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distribution` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distribution`
--

LOCK TABLES `distribution` WRITE;
/*!40000 ALTER TABLE `distribution` DISABLE KEYS */;
/*!40000 ALTER TABLE `distribution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distributionlookup`
--

DROP TABLE IF EXISTS `distributionlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distributionlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distributionlookup`
--

LOCK TABLES `distributionlookup` WRITE;
/*!40000 ALTER TABLE `distributionlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `distributionlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dividend`
--

DROP TABLE IF EXISTS `dividend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dividend` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `UpdateDt` varchar(255) DEFAULT NULL,
  `BatchName` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `CancellationDt` date DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `LineBusinessCd` varchar(255) DEFAULT NULL,
  `DividendPlanCd` varchar(255) DEFAULT NULL,
  `DividendFormNumber` varchar(255) DEFAULT NULL,
  `PolicyStatus` varchar(255) DEFAULT NULL,
  `AuditStatus` varchar(255) DEFAULT NULL,
  `NetEarnedPremiumAmt` decimal(28,6) DEFAULT NULL,
  `DividendPlanEligibleRangeInd` varchar(255) DEFAULT NULL,
  `IncurredLossAmt` decimal(28,6) DEFAULT NULL,
  `IncurredLossRatio` decimal(28,6) DEFAULT NULL,
  `EligibleCriteriaInd` varchar(255) DEFAULT NULL,
  `DividendDisqualifyInd` varchar(255) DEFAULT NULL,
  `DividendDisqualifyReason` varchar(255) DEFAULT NULL,
  `DividendDisqualifyComments` varchar(255) DEFAULT NULL,
  `CompanyReview` varchar(255) DEFAULT NULL,
  `EstimatedDividendPaymentAmt` decimal(28,6) DEFAULT NULL,
  `DividendPlanApproveStatus` varchar(255) DEFAULT NULL,
  `DividendPaidAmt` decimal(28,6) DEFAULT NULL,
  `DividendPaidDt` date DEFAULT NULL,
  `NotificationCd` varchar(255) DEFAULT NULL,
  `ProducerCd` varchar(255) DEFAULT NULL,
  `MethodCd` varchar(255) DEFAULT NULL,
  `ACHName` varchar(255) DEFAULT NULL,
  `ACHBankAccountTypeCd` varchar(255) DEFAULT NULL,
  `ACHBankAccountNumber` varchar(255) DEFAULT NULL,
  `ACHRoutingNumber` varchar(255) DEFAULT NULL,
  `ACHBankName` varchar(255) DEFAULT NULL,
  `ACHStandardEntryClassCd` varchar(255) DEFAULT NULL,
  `CommercialName` varchar(255) DEFAULT NULL,
  `Addr1` varchar(255) DEFAULT NULL,
  `Addr2` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL,
  `StateProvCd` varchar(255) DEFAULT NULL,
  `PostalCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dividend`
--

LOCK TABLES `dividend` WRITE;
/*!40000 ALTER TABLE `dividend` DISABLE KEYS */;
/*!40000 ALTER TABLE `dividend` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dividendbatch`
--

DROP TABLE IF EXISTS `dividendbatch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dividendbatch` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `UpdateDt` varchar(255) DEFAULT NULL,
  `BatchName` varchar(255) DEFAULT NULL,
  `DividendPlanCd` varchar(255) DEFAULT NULL,
  `LineBusinessCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `PeriodStartDt` date DEFAULT NULL,
  `PeriodEndDt` date DEFAULT NULL,
  PRIMARY KEY (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dividendbatch`
--

LOCK TABLES `dividendbatch` WRITE;
/*!40000 ALTER TABLE `dividendbatch` DISABLE KEYS */;
/*!40000 ALTER TABLE `dividendbatch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `electronicpayment`
--

DROP TABLE IF EXISTS `electronicpayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `electronicpayment` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `electronicpayment`
--

LOCK TABLES `electronicpayment` WRITE;
/*!40000 ALTER TABLE `electronicpayment` DISABLE KEYS */;
/*!40000 ALTER TABLE `electronicpayment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `electronicpaymentlookup`
--

DROP TABLE IF EXISTS `electronicpaymentlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `electronicpaymentlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `electronicpaymentlookup`
--

LOCK TABLES `electronicpaymentlookup` WRITE;
/*!40000 ALTER TABLE `electronicpaymentlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `electronicpaymentlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emaillistenerprocessor`
--

DROP TABLE IF EXISTS `emaillistenerprocessor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emaillistenerprocessor` (
  `EmailId` varchar(255) NOT NULL,
  `id` varchar(255) DEFAULT NULL,
  `ServerId` varchar(255) DEFAULT NULL,
  `ProcessDt` date DEFAULT NULL,
  `ProcessTm` varchar(255) DEFAULT NULL,
  `FirstServerId` varchar(255) DEFAULT NULL,
  `FirstProcessDt` date DEFAULT NULL,
  `FirstProcessTm` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `AttemptCnt` int(11) DEFAULT NULL,
  PRIMARY KEY (`EmailId`),
  KEY `EmailListenerProcessor_1` (`ServerId`),
  KEY `EmailListenerProcessor_2` (`StatusCd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emaillistenerprocessor`
--

LOCK TABLES `emaillistenerprocessor` WRITE;
/*!40000 ALTER TABLE `emaillistenerprocessor` DISABLE KEYS */;
/*!40000 ALTER TABLE `emaillistenerprocessor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `esignature`
--

DROP TABLE IF EXISTS `esignature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `esignature` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `esignature`
--

LOCK TABLES `esignature` WRITE;
/*!40000 ALTER TABLE `esignature` DISABLE KEYS */;
/*!40000 ALTER TABLE `esignature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `esignaturelookup`
--

DROP TABLE IF EXISTS `esignaturelookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `esignaturelookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `esignaturelookup`
--

LOCK TABLES `esignaturelookup` WRITE;
/*!40000 ALTER TABLE `esignaturelookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `esignaturelookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `esignatureprocessor`
--

DROP TABLE IF EXISTS `esignatureprocessor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `esignatureprocessor` (
  `id` varchar(255) DEFAULT NULL,
  `SystemId` int(11) DEFAULT NULL,
  `ExternalPackageId` varchar(255) DEFAULT NULL,
  `ESignatureRef` varchar(255) DEFAULT NULL,
  `OutputName` varchar(255) DEFAULT NULL,
  `ServerId` varchar(255) DEFAULT NULL,
  `ProcessTm` varchar(255) DEFAULT NULL,
  `ProcessTimestamp` varchar(255) DEFAULT NULL,
  `StatusBeforeProcess` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `esignatureprocessor`
--

LOCK TABLES `esignatureprocessor` WRITE;
/*!40000 ALTER TABLE `esignatureprocessor` DISABLE KEYS */;
/*!40000 ALTER TABLE `esignatureprocessor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `externalpayment`
--

DROP TABLE IF EXISTS `externalpayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `externalpayment` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `externalpayment`
--

LOCK TABLES `externalpayment` WRITE;
/*!40000 ALTER TABLE `externalpayment` DISABLE KEYS */;
/*!40000 ALTER TABLE `externalpayment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `externalpaymentlookup`
--

DROP TABLE IF EXISTS `externalpaymentlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `externalpaymentlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `externalpaymentlookup`
--

LOCK TABLES `externalpaymentlookup` WRITE;
/*!40000 ALTER TABLE `externalpaymentlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `externalpaymentlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filelistenerprocessor`
--

DROP TABLE IF EXISTS `filelistenerprocessor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filelistenerprocessor` (
  `FileId` varchar(255) NOT NULL,
  `id` varchar(255) DEFAULT NULL,
  `ServerId` varchar(255) DEFAULT NULL,
  `ProcessDt` date DEFAULT NULL,
  `ProcessTm` varchar(255) DEFAULT NULL,
  `FirstServerId` varchar(255) DEFAULT NULL,
  `FirstProcessDt` date DEFAULT NULL,
  `FirstProcessTm` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `AttemptCnt` int(11) DEFAULT NULL,
  PRIMARY KEY (`FileId`),
  KEY `FileListenerProcessor_1` (`ServerId`),
  KEY `FileListenerProcessor_2` (`StatusCd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filelistenerprocessor`
--

LOCK TABLES `filelistenerprocessor` WRITE;
/*!40000 ALTER TABLE `filelistenerprocessor` DISABLE KEYS */;
/*!40000 ALTER TABLE `filelistenerprocessor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `froicontrib`
--

DROP TABLE IF EXISTS `froicontrib`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `froicontrib` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `froicontrib`
--

LOCK TABLES `froicontrib` WRITE;
/*!40000 ALTER TABLE `froicontrib` DISABLE KEYS */;
/*!40000 ALTER TABLE `froicontrib` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `froicontriblookup`
--

DROP TABLE IF EXISTS `froicontriblookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `froicontriblookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `froicontriblookup`
--

LOCK TABLES `froicontriblookup` WRITE;
/*!40000 ALTER TABLE `froicontriblookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `froicontriblookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glstats`
--

DROP TABLE IF EXISTS `glstats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glstats` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `StatSourceId` varchar(255) DEFAULT NULL,
  `StatSource` varchar(255) DEFAULT NULL,
  `TransactionName` varchar(255) DEFAULT NULL,
  `Amount` decimal(28,6) DEFAULT NULL,
  `TransactionPeriod` varchar(255) DEFAULT NULL,
  `TransactionDate` date DEFAULT NULL,
  `ARReceiptTypeCd` varchar(255) DEFAULT NULL,
  `InsuranceTypeCd` varchar(255) DEFAULT NULL,
  `TransferCarrier` varchar(255) DEFAULT NULL,
  `AccountingDt` date DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `ControllingStateCd` varchar(255) DEFAULT NULL,
  `CategoryCd` varchar(255) DEFAULT NULL,
  `FeeCd` varchar(255) DEFAULT NULL,
  `PolicyYear` varchar(255) DEFAULT NULL,
  `LossYear` varchar(255) DEFAULT NULL,
  `ProductName` varchar(255) DEFAULT NULL,
  `LineCd` varchar(255) DEFAULT NULL,
  `PayPlanCd` varchar(255) DEFAULT NULL,
  `DepositoryLocationCd` varchar(255) DEFAULT NULL,
  `ReserveTypeCd` varchar(255) DEFAULT NULL,
  `ReserveCd` varchar(255) DEFAULT NULL,
  `ProducerCd` varchar(255) DEFAULT NULL,
  `RateAreaName` varchar(255) DEFAULT NULL,
  `CoverageCd` varchar(255) DEFAULT NULL,
  `SubTypeCd` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `ChargeAmt` decimal(28,6) DEFAULT NULL,
  `ClaimantNumber` varchar(255) DEFAULT NULL,
  `ItemDt` date DEFAULT NULL,
  `DividendPlanCd` varchar(255) DEFAULT NULL,
  `SystemCheckNumber` varchar(255) DEFAULT NULL,
  `ReinsuranceItemName` varchar(255) DEFAULT NULL,
  `ActivityTypeCd` varchar(255) DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `ReinsuranceName` varchar(255) DEFAULT NULL,
  `CommissionAreaCd` varchar(255) DEFAULT NULL,
  `NewRenewalCd` varchar(255) DEFAULT NULL,
  `ServicePeriodStartDt` date DEFAULT NULL,
  `ContractReinsuranceInd` varchar(255) DEFAULT NULL,
  `BillMethod` varchar(255) DEFAULT NULL,
  `ReinsuranceCoverageGroupCd` varchar(255) DEFAULT NULL,
  `PaymentAccountCd` varchar(255) DEFAULT NULL,
  `PayToCd` varchar(255) DEFAULT NULL,
  `ACHAgent` varchar(255) DEFAULT NULL,
  `PayToTypeCd` varchar(255) DEFAULT NULL,
  `ReverseReason` varchar(255) DEFAULT NULL,
  `FeatureCd` varchar(255) DEFAULT NULL,
  `TransactionDesc` varchar(255) DEFAULT NULL,
  `ReinsuranceGroupId` varchar(255) DEFAULT NULL,
  `TransactionTypeCd` varchar(255) DEFAULT NULL,
  `PaymentRequestedDt` date DEFAULT NULL,
  `BatchId` varchar(255) DEFAULT NULL,
  `ReinsuranceReserveCd` varchar(255) DEFAULT NULL,
  `ItemAmt` decimal(28,6) DEFAULT NULL,
  `PayToName` varchar(255) DEFAULT NULL,
  `CommissionTypeCd` varchar(255) DEFAULT NULL,
  `ItemNumber` varchar(255) DEFAULT NULL,
  `CatastropheNumber` varchar(255) DEFAULT NULL,
  `ServicePeriodEndDt` date DEFAULT NULL,
  `ClassificationCd` varchar(255) DEFAULT NULL,
  `AdjustmentTypeCd` varchar(255) DEFAULT NULL,
  `ProviderCd` varchar(255) DEFAULT NULL,
  `ClaimNumber` varchar(255) DEFAULT NULL,
  `ReportPeriod` varchar(255) DEFAULT NULL,
  `StatementTypeCd` varchar(255) DEFAULT NULL,
  `PayThroughDt` date DEFAULT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  `StatementAccountNumber` varchar(255) DEFAULT NULL,
  `ProducerAgency` varchar(255) DEFAULT NULL,
  `PaidMethodCd` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`),
  KEY `GLStats_1` (`TransactionDate`),
  KEY `GLStats_2` (`AccountingDt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glstats`
--

LOCK TABLES `glstats` WRITE;
/*!40000 ALTER TABLE `glstats` DISABLE KEYS */;
/*!40000 ALTER TABLE `glstats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inboxview`
--

DROP TABLE IF EXISTS `inboxview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inboxview` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inboxview`
--

LOCK TABLES `inboxview` WRITE;
/*!40000 ALTER TABLE `inboxview` DISABLE KEYS */;
/*!40000 ALTER TABLE `inboxview` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inboxviewlookup`
--

DROP TABLE IF EXISTS `inboxviewlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inboxviewlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inboxviewlookup`
--

LOCK TABLES `inboxviewlookup` WRITE;
/*!40000 ALTER TABLE `inboxviewlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `inboxviewlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job`
--

DROP TABLE IF EXISTS `job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job`
--

LOCK TABLES `job` WRITE;
/*!40000 ALTER TABLE `job` DISABLE KEYS */;
/*!40000 ALTER TABLE `job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `joblookup`
--

DROP TABLE IF EXISTS `joblookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `joblookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `joblookup`
--

LOCK TABLES `joblookup` WRITE;
/*!40000 ALTER TABLE `joblookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `joblookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobschedule`
--

DROP TABLE IF EXISTS `jobschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobschedule` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobschedule`
--

LOCK TABLES `jobschedule` WRITE;
/*!40000 ALTER TABLE `jobschedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobschedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobschedulelookup`
--

DROP TABLE IF EXISTS `jobschedulelookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobschedulelookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobschedulelookup`
--

LOCK TABLES `jobschedulelookup` WRITE;
/*!40000 ALTER TABLE `jobschedulelookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobschedulelookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logentry`
--

DROP TABLE IF EXISTS `logentry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logentry` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logentry`
--

LOCK TABLES `logentry` WRITE;
/*!40000 ALTER TABLE `logentry` DISABLE KEYS */;
/*!40000 ALTER TABLE `logentry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logentrylookup`
--

DROP TABLE IF EXISTS `logentrylookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logentrylookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logentrylookup`
--

LOCK TABLES `logentrylookup` WRITE;
/*!40000 ALTER TABLE `logentrylookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `logentrylookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logintoken`
--

DROP TABLE IF EXISTS `logintoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logintoken` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logintoken`
--

LOCK TABLES `logintoken` WRITE;
/*!40000 ALTER TABLE `logintoken` DISABLE KEYS */;
/*!40000 ALTER TABLE `logintoken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logintokenlookup`
--

DROP TABLE IF EXISTS `logintokenlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logintokenlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logintokenlookup`
--

LOCK TABLES `logintokenlookup` WRITE;
/*!40000 ALTER TABLE `logintokenlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `logintokenlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moratorium`
--

DROP TABLE IF EXISTS `moratorium`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moratorium` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moratorium`
--

LOCK TABLES `moratorium` WRITE;
/*!40000 ALTER TABLE `moratorium` DISABLE KEYS */;
/*!40000 ALTER TABLE `moratorium` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moratoriumlookup`
--

DROP TABLE IF EXISTS `moratoriumlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moratoriumlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moratoriumlookup`
--

LOCK TABLES `moratoriumlookup` WRITE;
/*!40000 ALTER TABLE `moratoriumlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `moratoriumlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsbanner`
--

DROP TABLE IF EXISTS `newsbanner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsbanner` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsbanner`
--

LOCK TABLES `newsbanner` WRITE;
/*!40000 ALTER TABLE `newsbanner` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsbanner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsbannerlookup`
--

DROP TABLE IF EXISTS `newsbannerlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsbannerlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsbannerlookup`
--

LOCK TABLES `newsbannerlookup` WRITE;
/*!40000 ALTER TABLE `newsbannerlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsbannerlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `note`
--

DROP TABLE IF EXISTS `note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `note` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `note`
--

LOCK TABLES `note` WRITE;
/*!40000 ALTER TABLE `note` DISABLE KEYS */;
/*!40000 ALTER TABLE `note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notelookup`
--

DROP TABLE IF EXISTS `notelookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notelookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notelookup`
--

LOCK TABLES `notelookup` WRITE;
/*!40000 ALTER TABLE `notelookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `notelookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notificationrequest`
--

DROP TABLE IF EXISTS `notificationrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notificationrequest` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notificationrequest`
--

LOCK TABLES `notificationrequest` WRITE;
/*!40000 ALTER TABLE `notificationrequest` DISABLE KEYS */;
/*!40000 ALTER TABLE `notificationrequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notificationrequestlookup`
--

DROP TABLE IF EXISTS `notificationrequestlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notificationrequestlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notificationrequestlookup`
--

LOCK TABLES `notificationrequestlookup` WRITE;
/*!40000 ALTER TABLE `notificationrequestlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `notificationrequestlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `openaccount`
--

DROP TABLE IF EXISTS `openaccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `openaccount` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `ProviderNumber` varchar(255) DEFAULT NULL,
  `ProviderRef` int(11) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `GrossPrem` decimal(28,6) DEFAULT NULL,
  `CommissionAmt` decimal(28,6) DEFAULT NULL,
  `CommissionPct` decimal(28,6) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `TransactionEffectiveDt` date DEFAULT NULL,
  `TransactionEntryDt` date DEFAULT NULL,
  `DueAmt1` decimal(28,6) DEFAULT NULL,
  `DueAmt2` decimal(28,6) DEFAULT NULL,
  `DueAmt3` decimal(28,6) DEFAULT NULL,
  `DueAmt4` decimal(28,6) DEFAULT NULL,
  `BillMethod` varchar(255) DEFAULT NULL,
  `RunMonth` varchar(255) DEFAULT NULL,
  `RunYear` varchar(255) DEFAULT NULL,
  `FeeAmt` decimal(28,6) DEFAULT NULL,
  PRIMARY KEY (`SystemId`),
  KEY `OpenAccount_1` (`PolicyNumber`),
  KEY `OpenAccount_2` (`EffectiveDt`),
  KEY `OpenAccount_3` (`GrossPrem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `openaccount`
--

LOCK TABLES `openaccount` WRITE;
/*!40000 ALTER TABLE `openaccount` DISABLE KEYS */;
/*!40000 ALTER TABLE `openaccount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payablebatchrequest`
--

DROP TABLE IF EXISTS `payablebatchrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payablebatchrequest` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payablebatchrequest`
--

LOCK TABLES `payablebatchrequest` WRITE;
/*!40000 ALTER TABLE `payablebatchrequest` DISABLE KEYS */;
/*!40000 ALTER TABLE `payablebatchrequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payablebatchrequestlookup`
--

DROP TABLE IF EXISTS `payablebatchrequestlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payablebatchrequestlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payablebatchrequestlookup`
--

LOCK TABLES `payablebatchrequestlookup` WRITE;
/*!40000 ALTER TABLE `payablebatchrequestlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `payablebatchrequestlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payablerequest`
--

DROP TABLE IF EXISTS `payablerequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payablerequest` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payablerequest`
--

LOCK TABLES `payablerequest` WRITE;
/*!40000 ALTER TABLE `payablerequest` DISABLE KEYS */;
/*!40000 ALTER TABLE `payablerequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payablerequestlookup`
--

DROP TABLE IF EXISTS `payablerequestlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payablerequestlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payablerequestlookup`
--

LOCK TABLES `payablerequestlookup` WRITE;
/*!40000 ALTER TABLE `payablerequestlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `payablerequestlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payablestats`
--

DROP TABLE IF EXISTS `payablestats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payablestats` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `CombinedKey` varchar(255) DEFAULT NULL,
  `PaymentSystemId` int(11) DEFAULT NULL,
  `StatSequence` int(11) DEFAULT NULL,
  `StatCd` varchar(255) DEFAULT NULL,
  `StatSequenceReplace` int(11) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `PaymentAccountCd` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `PayToName` varchar(255) DEFAULT NULL,
  `ProviderCd` varchar(255) DEFAULT NULL,
  `PaymentStatusCd` varchar(255) DEFAULT NULL,
  `ProviderRef` int(11) DEFAULT NULL,
  `ItemAmt` decimal(28,6) DEFAULT NULL,
  `ItemNumber` varchar(255) DEFAULT NULL,
  `ItemDt` date DEFAULT NULL,
  `PrinterTemplateIdRef` varchar(255) DEFAULT NULL,
  `ClassificationCd` varchar(255) DEFAULT NULL,
  `RequestDt` date DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `SourceRef` varchar(255) DEFAULT NULL,
  `AllocationAmt` decimal(28,6) DEFAULT NULL,
  `TransactionTypeCd` varchar(255) DEFAULT NULL,
  `TransactionDt` date DEFAULT NULL,
  `TransactionTm` varchar(255) DEFAULT NULL,
  `TransactionUser` varchar(255) DEFAULT NULL,
  `TransactionAmt` decimal(28,6) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `PaymentMethodCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `DividendPaymentInd` varchar(255) DEFAULT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  `AccountTransReference` varchar(255) DEFAULT NULL,
  `AccountTransIdRef` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyProviderRef` int(11) DEFAULT NULL,
  `LossDt` date DEFAULT NULL,
  `CombinePaymentInd` varchar(255) DEFAULT NULL,
  `ClaimantNumber` int(11) DEFAULT NULL,
  `ClaimNumber` varchar(255) DEFAULT NULL,
  `SequenceNumber` int(11) DEFAULT NULL,
  `ClaimantTransactionIdRef` varchar(255) DEFAULT NULL,
  `ServicePeriodStartDt` date DEFAULT NULL,
  `ServicePeriodEndDt` date DEFAULT NULL,
  PRIMARY KEY (`SystemId`),
  KEY `PayableStats_1` (`CombinedKey`),
  KEY `PayableStats_Idx_1` (`CombinedKey`,`StatusCd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payablestats`
--

LOCK TABLES `payablestats` WRITE;
/*!40000 ALTER TABLE `payablestats` DISABLE KEYS */;
/*!40000 ALTER TABLE `payablestats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paymentexports`
--

DROP TABLE IF EXISTS `paymentexports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentexports` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paymentexports`
--

LOCK TABLES `paymentexports` WRITE;
/*!40000 ALTER TABLE `paymentexports` DISABLE KEYS */;
/*!40000 ALTER TABLE `paymentexports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paymentexportslookup`
--

DROP TABLE IF EXISTS `paymentexportslookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentexportslookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paymentexportslookup`
--

LOCK TABLES `paymentexportslookup` WRITE;
/*!40000 ALTER TABLE `paymentexportslookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `paymentexportslookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paymentimports`
--

DROP TABLE IF EXISTS `paymentimports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentimports` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paymentimports`
--

LOCK TABLES `paymentimports` WRITE;
/*!40000 ALTER TABLE `paymentimports` DISABLE KEYS */;
/*!40000 ALTER TABLE `paymentimports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paymentimportslookup`
--

DROP TABLE IF EXISTS `paymentimportslookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentimportslookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paymentimportslookup`
--

LOCK TABLES `paymentimportslookup` WRITE;
/*!40000 ALTER TABLE `paymentimportslookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `paymentimportslookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paymentlookup`
--

DROP TABLE IF EXISTS `paymentlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paymentlookup`
--

LOCK TABLES `paymentlookup` WRITE;
/*!40000 ALTER TABLE `paymentlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `paymentlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policy`
--

DROP TABLE IF EXISTS `policy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policy` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policy`
--

LOCK TABLES `policy` WRITE;
/*!40000 ALTER TABLE `policy` DISABLE KEYS */;
/*!40000 ALTER TABLE `policy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policyarchive`
--

DROP TABLE IF EXISTS `policyarchive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyarchive` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policyarchive`
--

LOCK TABLES `policyarchive` WRITE;
/*!40000 ALTER TABLE `policyarchive` DISABLE KEYS */;
/*!40000 ALTER TABLE `policyarchive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policyarchivelookup`
--

DROP TABLE IF EXISTS `policyarchivelookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyarchivelookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policyarchivelookup`
--

LOCK TABLES `policyarchivelookup` WRITE;
/*!40000 ALTER TABLE `policyarchivelookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `policyarchivelookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policyhistory`
--

DROP TABLE IF EXISTS `policyhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyhistory` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policyhistory`
--

LOCK TABLES `policyhistory` WRITE;
/*!40000 ALTER TABLE `policyhistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `policyhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policyhistorylookup`
--

DROP TABLE IF EXISTS `policyhistorylookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyhistorylookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policyhistorylookup`
--

LOCK TABLES `policyhistorylookup` WRITE;
/*!40000 ALTER TABLE `policyhistorylookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `policyhistorylookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policylookup`
--

DROP TABLE IF EXISTS `policylookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policylookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policylookup`
--

LOCK TABLES `policylookup` WRITE;
/*!40000 ALTER TABLE `policylookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `policylookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policyreinsrenewalofferstats`
--

DROP TABLE IF EXISTS `policyreinsrenewalofferstats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyreinsrenewalofferstats` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `StatSequence` int(11) DEFAULT NULL,
  `StatSequenceReplace` int(11) DEFAULT NULL,
  `PolicyRef` int(11) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `CombinedKey` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `TransactionEffectiveDt` date DEFAULT NULL,
  `AccountingDt` date DEFAULT NULL,
  `WrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WrittenCommissionAmt` decimal(28,6) DEFAULT NULL,
  `EarnDays` int(11) DEFAULT NULL,
  `InforceChangeAmt` decimal(28,6) DEFAULT NULL,
  `PolicyYear` varchar(255) DEFAULT NULL,
  `TermDays` int(11) DEFAULT NULL,
  `InsuranceTypeCd` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `NewRenewalCd` varchar(255) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `ReinsuranceName` varchar(255) DEFAULT NULL,
  `MasterSubInd` varchar(255) DEFAULT NULL,
  `ReinsuranceItemName` varchar(255) DEFAULT NULL,
  `ProviderCd` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `ReinsuranceGroupId` varchar(255) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `PolicyStatusCd` varchar(255) DEFAULT NULL,
  `ProductName` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  `PolicyGroupCd` varchar(255) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `ReinsuranceCoverageGroupCd` varchar(255) DEFAULT NULL,
  `AdmiraltyFELALiabilityLimits` varchar(255) DEFAULT NULL,
  `AdmiraltyFELAMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AlcoholDrugFreeCreditInd` varchar(255) DEFAULT NULL,
  `AlcDrugFreeCreditAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheRate` decimal(28,6) DEFAULT NULL,
  `ContrClassPremAdjCreditFactor` varchar(255) DEFAULT NULL,
  `ContrClassPremAdjCreditAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleDiscountRatePct` decimal(28,6) DEFAULT NULL,
  `ELIncreasedLimitPct` decimal(28,6) DEFAULT NULL,
  `ELIncreaseLimitMinPremAmt` decimal(28,6) DEFAULT NULL,
  `ELIncreaseLimitMinPremChrgAmt` decimal(28,6) DEFAULT NULL,
  `EstimatedAnnualPremiumAmt` decimal(28,6) DEFAULT NULL,
  `ExpenseConstantAmt` decimal(28,6) DEFAULT NULL,
  `ExperienceMeritRating` varchar(255) DEFAULT NULL,
  `ExperienceModificationFactor` varchar(255) DEFAULT NULL,
  `ManagedCareCreditInd` varchar(255) DEFAULT NULL,
  `ManagedCareCreditAmt` decimal(28,6) DEFAULT NULL,
  `NAICS` varchar(255) DEFAULT NULL,
  `NCCIRiskIdNumber` varchar(255) DEFAULT NULL,
  `NumberOfClaims` varchar(255) DEFAULT NULL,
  `PremiumDiscountAmt` decimal(28,6) DEFAULT NULL,
  `RatingBureauID` varchar(255) DEFAULT NULL,
  `CertifiedSafetyHealthProgPct` decimal(28,6) DEFAULT NULL,
  `SchedRateClassPeculiarities` varchar(255) DEFAULT NULL,
  `SchedRateEmployee` varchar(255) DEFAULT NULL,
  `SchedRateManagement` varchar(255) DEFAULT NULL,
  `SchedRateMedicalFacilities` varchar(255) DEFAULT NULL,
  `SchedRateMgmtSafetyOrg` varchar(255) DEFAULT NULL,
  `SchedRatePremises` varchar(255) DEFAULT NULL,
  `SchedRateSafetyDevices` varchar(255) DEFAULT NULL,
  `SIC` varchar(255) DEFAULT NULL,
  `StandardPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjust` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjustSign` varchar(255) DEFAULT NULL,
  `TerrorismAmt` decimal(28,6) DEFAULT NULL,
  `TerrorismRate` decimal(28,6) DEFAULT NULL,
  `TotalManualPremimAmt` decimal(28,6) DEFAULT NULL,
  `TotalSubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationInd` varchar(255) DEFAULT NULL,
  `WaiverSubrogationRate` decimal(28,6) DEFAULT NULL,
  `YearsInBusiness` varchar(255) DEFAULT NULL,
  `DrugFreeWorkplace` varchar(255) DEFAULT NULL,
  `DividendPlanCd` varchar(255) DEFAULT NULL,
  `StatSourceIdRef` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`),
  KEY `PolicyReinsRenewalOfferStats_1` (`PolicyNumber`),
  KEY `PolicyReinsRenewalOfferStats_2` (`CombinedKey`),
  KEY `PolicyReinsRenewalOfferStats_3` (`StatusCd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policyreinsrenewalofferstats`
--

LOCK TABLES `policyreinsrenewalofferstats` WRITE;
/*!40000 ALTER TABLE `policyreinsrenewalofferstats` DISABLE KEYS */;
/*!40000 ALTER TABLE `policyreinsrenewalofferstats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policyreinsurancestats`
--

DROP TABLE IF EXISTS `policyreinsurancestats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyreinsurancestats` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `StatSequence` int(11) DEFAULT NULL,
  `StatSequenceReplace` int(11) DEFAULT NULL,
  `PolicyRef` int(11) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `CombinedKey` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `TransactionEffectiveDt` date DEFAULT NULL,
  `AccountingDt` date DEFAULT NULL,
  `WrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WrittenCommissionAmt` decimal(28,6) DEFAULT NULL,
  `EarnDays` int(11) DEFAULT NULL,
  `InforceChangeAmt` decimal(28,6) DEFAULT NULL,
  `PolicyYear` varchar(255) DEFAULT NULL,
  `TermDays` int(11) DEFAULT NULL,
  `InsuranceTypeCd` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `NewRenewalCd` varchar(255) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `ReinsuranceName` varchar(255) DEFAULT NULL,
  `MasterSubInd` varchar(255) DEFAULT NULL,
  `ReinsuranceItemName` varchar(255) DEFAULT NULL,
  `ProviderCd` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `ReinsuranceGroupId` varchar(255) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `PolicyStatusCd` varchar(255) DEFAULT NULL,
  `ProductName` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  `PolicyGroupCd` varchar(255) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `ReinsuranceCoverageGroupCd` varchar(255) DEFAULT NULL,
  `AdmiraltyFELALiabilityLimits` varchar(255) DEFAULT NULL,
  `AdmiraltyFELAMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AlcoholDrugFreeCreditInd` varchar(255) DEFAULT NULL,
  `AlcDrugFreeCreditAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheRate` decimal(28,6) DEFAULT NULL,
  `ContrClassPremAdjCreditFactor` varchar(255) DEFAULT NULL,
  `ContrClassPremAdjCreditAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleDiscountRatePct` decimal(28,6) DEFAULT NULL,
  `DiseaseAbrasiveAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAbrasivePaDeAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAsbestosAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAtomicEnergyAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseCoalmineAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseExperienceAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundryIronAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundryNoFerrAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundrySteelAmt` decimal(28,6) DEFAULT NULL,
  `ELIncreasedLimitPct` decimal(28,6) DEFAULT NULL,
  `ELIncreaseLimitMinPremAmt` decimal(28,6) DEFAULT NULL,
  `ELIncreaseLimitMinPremChrgAmt` decimal(28,6) DEFAULT NULL,
  `EstimatedAnnualPremiumAmt` decimal(28,6) DEFAULT NULL,
  `ExpenseConstantAmt` decimal(28,6) DEFAULT NULL,
  `ExperienceMeritRating` varchar(255) DEFAULT NULL,
  `ExperienceModificationFactor` varchar(255) DEFAULT NULL,
  `ManagedCareCreditInd` varchar(255) DEFAULT NULL,
  `ManagedCareCreditAmt` decimal(28,6) DEFAULT NULL,
  `NAICS` varchar(255) DEFAULT NULL,
  `NCCIRiskIdNumber` varchar(255) DEFAULT NULL,
  `NumberOfClaims` varchar(255) DEFAULT NULL,
  `PremiumDiscountAmt` decimal(28,6) DEFAULT NULL,
  `RatingBureauID` varchar(255) DEFAULT NULL,
  `CertifiedSafetyHealthProgPct` decimal(28,6) DEFAULT NULL,
  `SchedRateClassPeculiarities` varchar(255) DEFAULT NULL,
  `SchedRateEmployee` varchar(255) DEFAULT NULL,
  `SchedRateManagement` varchar(255) DEFAULT NULL,
  `SchedRateMedicalFacilities` varchar(255) DEFAULT NULL,
  `SchedRateMgmtSafetyOrg` varchar(255) DEFAULT NULL,
  `SchedRatePremises` varchar(255) DEFAULT NULL,
  `SchedRateSafetyDevices` varchar(255) DEFAULT NULL,
  `SIC` varchar(255) DEFAULT NULL,
  `StandardPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjust` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjustSign` varchar(255) DEFAULT NULL,
  `TerrorismAmt` decimal(28,6) DEFAULT NULL,
  `TerrorismRate` decimal(28,6) DEFAULT NULL,
  `TotalManualPremimAmt` decimal(28,6) DEFAULT NULL,
  `TotalSubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationInd` varchar(255) DEFAULT NULL,
  `WaiverSubrogationRate` decimal(28,6) DEFAULT NULL,
  `YearsInBusiness` varchar(255) DEFAULT NULL,
  `DrugFreeWorkplace` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`),
  KEY `PolicyReinsuranceStats_1` (`PolicyNumber`),
  KEY `PolicyReinsuranceStats_2` (`CombinedKey`),
  KEY `PolicyReinsuranceStats_3` (`StatusCd`),
  KEY `PolicyReinsuranceStatI1` (`CombinedKey`,`StatusCd`),
  KEY `PolicyReinsuranceStatI2` (`CombinedKey`,`AccountingDt`),
  KEY `PolicyReinsuranceStatI3` (`AccountingDt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policyreinsurancestats`
--

LOCK TABLES `policyreinsurancestats` WRITE;
/*!40000 ALTER TABLE `policyreinsurancestats` DISABLE KEYS */;
/*!40000 ALTER TABLE `policyreinsurancestats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policyreinsurancesummarystats`
--

DROP TABLE IF EXISTS `policyreinsurancesummarystats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyreinsurancesummarystats` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `StatSequence` int(11) DEFAULT NULL,
  `StatSequenceReplace` int(11) DEFAULT NULL,
  `ReportPeriod` varchar(255) DEFAULT NULL,
  `UpdateDt` date DEFAULT NULL,
  `PolicyRef` int(11) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `CombinedKey` varchar(255) DEFAULT NULL,
  `AccountingDt` date DEFAULT NULL,
  `PolicyYear` varchar(255) DEFAULT NULL,
  `InsuranceTypeCd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `NewRenewalCd` varchar(255) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `ReinsuranceName` varchar(255) DEFAULT NULL,
  `MasterSubInd` varchar(255) DEFAULT NULL,
  `ReinsuranceItemName` varchar(255) DEFAULT NULL,
  `ProviderCd` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `ReinsuranceGroupId` varchar(255) DEFAULT NULL,
  `MTDWrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `TTDWrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `YTDWrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `MTDWrittenCommissionAmt` decimal(28,6) DEFAULT NULL,
  `TTDWrittenCommissionAmt` decimal(28,6) DEFAULT NULL,
  `YTDWrittenCommissionAmt` decimal(28,6) DEFAULT NULL,
  `InforceAmt` decimal(28,6) DEFAULT NULL,
  `LastInforceAmt` decimal(28,6) DEFAULT NULL,
  `MonthEarnedPremiumAmt` decimal(28,6) DEFAULT NULL,
  `MTDEarnedPremiumAmt` decimal(28,6) DEFAULT NULL,
  `TTDEarnedPremiumAmt` decimal(28,6) DEFAULT NULL,
  `YTDEarnedPremiumAmt` decimal(28,6) DEFAULT NULL,
  `MonthUnearnedAmt` decimal(28,6) DEFAULT NULL,
  `UnearnedAmt` decimal(28,6) DEFAULT NULL,
  `UnearnedM00Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM01Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM02Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM03Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM04Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM05Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM06Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM07Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM08Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM09Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM10Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM11Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM12Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM13Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM14Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM15Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM16Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM17Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM18Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM19Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM20Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM21Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM22Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM23Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM24Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM25Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM26Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM27Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM28Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM29Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM30Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM31Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM32Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM33Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM34Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM35Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM36Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM37Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM38Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM39Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM40Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM41Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM42Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM43Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM44Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM45Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM46Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM47Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM48Amt` decimal(28,6) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `PolicyStatusCd` varchar(255) DEFAULT NULL,
  `ProductName` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  `PolicyGroupCd` varchar(255) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `ContractReinsuranceInd` varchar(255) DEFAULT NULL,
  `ReinsuranceCoverageGroupCd` varchar(255) DEFAULT NULL,
  `AdmiraltyFELALiabilityLimits` varchar(255) DEFAULT NULL,
  `AdmiraltyFELAMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AlcoholDrugFreeCreditInd` varchar(255) DEFAULT NULL,
  `AlcDrugFreeCreditAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheRate` decimal(28,6) DEFAULT NULL,
  `ContrClassPremAdjCreditFactor` varchar(255) DEFAULT NULL,
  `ContrClassPremAdjCreditAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleDiscountRatePct` decimal(28,6) DEFAULT NULL,
  `DiseaseAbrasiveAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAbrasivePaDeAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAsbestosAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAtomicEnergyAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseCoalmineAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseExperienceAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundryIronAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundryNoFerrAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundrySteelAmt` decimal(28,6) DEFAULT NULL,
  `ELIncreasedLimitPct` decimal(28,6) DEFAULT NULL,
  `ELIncreaseLimitMinPremAmt` decimal(28,6) DEFAULT NULL,
  `ELIncreaseLimitMinPremChrgAmt` decimal(28,6) DEFAULT NULL,
  `EstimatedAnnualPremiumAmt` varchar(255) DEFAULT NULL,
  `ExpenseConstantAmt` decimal(28,6) DEFAULT NULL,
  `ExperienceMeritRating` varchar(255) DEFAULT NULL,
  `ExperienceModificationFactor` varchar(255) DEFAULT NULL,
  `ManagedCareCreditInd` varchar(255) DEFAULT NULL,
  `ManagedCareCreditAmt` decimal(28,6) DEFAULT NULL,
  `NAICS` varchar(255) DEFAULT NULL,
  `NCCIRiskIdNumber` varchar(255) DEFAULT NULL,
  `NumberOfClaims` varchar(255) DEFAULT NULL,
  `PremiumDiscountAmt` decimal(28,6) DEFAULT NULL,
  `RatingBureauID` varchar(255) DEFAULT NULL,
  `CertifiedSafetyHealthProgPct` decimal(28,6) DEFAULT NULL,
  `SchedRateClassPeculiarities` varchar(255) DEFAULT NULL,
  `SchedRateEmployee` varchar(255) DEFAULT NULL,
  `SchedRateManagement` varchar(255) DEFAULT NULL,
  `SchedRateMedicalFacilities` varchar(255) DEFAULT NULL,
  `SchedRateMgmtSafetyOrg` varchar(255) DEFAULT NULL,
  `SchedRatePremises` varchar(255) DEFAULT NULL,
  `SchedRateSafetyDevices` varchar(255) DEFAULT NULL,
  `SIC` varchar(255) DEFAULT NULL,
  `StandardPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjust` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjustSign` varchar(255) DEFAULT NULL,
  `TerrorismAmt` decimal(28,6) DEFAULT NULL,
  `TerrorismRate` decimal(28,6) DEFAULT NULL,
  `TotalManualPremimAmt` decimal(28,6) DEFAULT NULL,
  `TotalSubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationInd` varchar(255) DEFAULT NULL,
  `WaiverSubrogationRate` decimal(28,6) DEFAULT NULL,
  `YearsInBusiness` varchar(255) DEFAULT NULL,
  `DrugFreeWorkplace` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`),
  KEY `PolicyReinsuranceSummaryStats_1` (`PolicyNumber`),
  KEY `PolicyReinsuranceSummaryStats_2` (`CombinedKey`),
  KEY `PolicyReinsuranceSummaryStats_3` (`StatusCd`),
  KEY `PolicyReinSumStatsI1` (`ReportPeriod`,`CombinedKey`,`UpdateDt`,`ContractReinsuranceInd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policyreinsurancesummarystats`
--

LOCK TABLES `policyreinsurancesummarystats` WRITE;
/*!40000 ALTER TABLE `policyreinsurancesummarystats` DISABLE KEYS */;
/*!40000 ALTER TABLE `policyreinsurancesummarystats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policyrenewalofferstats`
--

DROP TABLE IF EXISTS `policyrenewalofferstats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyrenewalofferstats` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `StatSequence` int(11) DEFAULT NULL,
  `StatSequenceReplace` int(11) DEFAULT NULL,
  `PolicyRef` int(11) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `LineCd` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `CoverageCd` varchar(255) DEFAULT NULL,
  `RiskCd` varchar(255) DEFAULT NULL,
  `RiskTypeCd` varchar(255) DEFAULT NULL,
  `SubTypeCd` varchar(255) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `CoverageItemCd` varchar(255) DEFAULT NULL,
  `FeeCd` varchar(255) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `CombinedKey` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `TransactionEffectiveDt` date DEFAULT NULL,
  `AccountingDt` date DEFAULT NULL,
  `WrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WrittenCommissionAmt` decimal(28,6) DEFAULT NULL,
  `WrittenPremiumFeeAmt` decimal(28,6) DEFAULT NULL,
  `WrittenCommissionFeeAmt` decimal(28,6) DEFAULT NULL,
  `EarnDays` int(11) DEFAULT NULL,
  `InforceChangeAmt` decimal(28,6) DEFAULT NULL,
  `PolicyYear` varchar(255) DEFAULT NULL,
  `TermDays` int(11) DEFAULT NULL,
  `InsuranceTypeCd` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `NewRenewalCd` varchar(255) DEFAULT NULL,
  `Limit1` varchar(255) DEFAULT NULL,
  `Limit2` varchar(255) DEFAULT NULL,
  `Limit3` varchar(255) DEFAULT NULL,
  `Deductible1` varchar(255) DEFAULT NULL,
  `Deductible2` varchar(255) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `CoinsurancePct` decimal(28,6) DEFAULT NULL,
  `CoveredPerilsCd` varchar(255) DEFAULT NULL,
  `CommissionKey` varchar(255) DEFAULT NULL,
  `CommissionAreaCd` varchar(255) DEFAULT NULL,
  `RateAreaName` varchar(255) DEFAULT NULL,
  `StatData` varchar(255) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `ShortRateStatInd` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `BillMethod` varchar(255) DEFAULT NULL,
  `StatementAccountNumber` varchar(255) DEFAULT NULL,
  `StatementAccountRef` int(11) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `ProducerCd` varchar(255) DEFAULT NULL,
  `BusinessSourceCd` varchar(255) DEFAULT NULL,
  `PreviousCarrierCd` varchar(255) DEFAULT NULL,
  `ConstructionCd` varchar(255) DEFAULT NULL,
  `YearBuilt` varchar(255) DEFAULT NULL,
  `SqFt` varchar(255) DEFAULT NULL,
  `Stories` varchar(255) DEFAULT NULL,
  `OccupancyCd` varchar(255) DEFAULT NULL,
  `Units` varchar(255) DEFAULT NULL,
  `ProtectionClass` varchar(255) DEFAULT NULL,
  `TerritoryCd` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  `PolicyStatusCd` varchar(255) DEFAULT NULL,
  `PolicyGroupCd` varchar(255) DEFAULT NULL,
  `CancelReason` varchar(255) DEFAULT NULL,
  `PayPlanCd` varchar(255) DEFAULT NULL,
  `ClassCd` varchar(255) DEFAULT NULL,
  `AgeOfHome` varchar(255) DEFAULT NULL,
  `LocationAddress1` varchar(255) DEFAULT NULL,
  `LocationAddress2` varchar(255) DEFAULT NULL,
  `LocationCity` varchar(255) DEFAULT NULL,
  `LocationCounty` varchar(255) DEFAULT NULL,
  `LocationState` varchar(255) DEFAULT NULL,
  `LocationZip` varchar(255) DEFAULT NULL,
  `VehicleNumber` varchar(255) DEFAULT NULL,
  `VehicleYear` varchar(255) DEFAULT NULL,
  `VehicleManufacturer` varchar(255) DEFAULT NULL,
  `VehicleModel` varchar(255) DEFAULT NULL,
  `VehicleType` varchar(255) DEFAULT NULL,
  `DriverNumber` varchar(255) DEFAULT NULL,
  `DriverFirst` varchar(255) DEFAULT NULL,
  `DriverMI` varchar(255) DEFAULT NULL,
  `DriverAge` varchar(255) DEFAULT NULL,
  `DriverLast` varchar(255) DEFAULT NULL,
  `DriverGenderCd` varchar(255) DEFAULT NULL,
  `RelationshipToInsuredCd` varchar(255) DEFAULT NULL,
  `DriverStartDt` date DEFAULT NULL,
  `PointsChargeable` int(11) DEFAULT NULL,
  `PointsCharged` int(11) DEFAULT NULL,
  `Limit4` varchar(255) DEFAULT NULL,
  `Limit5` varchar(255) DEFAULT NULL,
  `Limit6` varchar(255) DEFAULT NULL,
  `ControllingProductVersionIdRef` varchar(255) DEFAULT NULL,
  `ControllingStateCd` varchar(255) DEFAULT NULL,
  `DividendPlanCd` varchar(255) DEFAULT NULL,
  `ISSNumber` varchar(255) DEFAULT NULL,
  `NAICNumber` varchar(255) DEFAULT NULL,
  `ISONumber` varchar(255) DEFAULT NULL,
  `PolicyTerm` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL,
  `County` varchar(255) DEFAULT NULL,
  `ExposureBasis` varchar(255) DEFAULT NULL,
  `BurglaryOptionCd` varchar(255) DEFAULT NULL,
  `Exposure` varchar(255) DEFAULT NULL,
  `NamedPerils` varchar(255) DEFAULT NULL,
  `SprinklerCd` varchar(255) DEFAULT NULL,
  `BuyBackCovg` varchar(255) DEFAULT NULL,
  `SICCd` varchar(255) DEFAULT NULL,
  `PolicyFormCd` varchar(255) DEFAULT NULL,
  `TerrorismCovCd` varchar(255) DEFAULT NULL,
  `CompanyNumber` varchar(255) DEFAULT NULL,
  `MGAIndicator` varchar(255) DEFAULT NULL,
  `MultiPolicyAffiliatedSameCo` varchar(255) DEFAULT NULL,
  `PerPolIndCd` varchar(255) DEFAULT NULL,
  `BondTypeCd` varchar(255) DEFAULT NULL,
  `Subline` varchar(255) DEFAULT NULL,
  `TypeOfBusiness` varchar(255) DEFAULT NULL,
  `TypePolicyCode` varchar(255) DEFAULT NULL,
  `ZipCode` varchar(255) DEFAULT NULL,
  `Deductible3` varchar(255) DEFAULT NULL,
  `BldgCounterNumber` varchar(255) DEFAULT NULL,
  `ScheduleItemClass` varchar(255) DEFAULT NULL,
  `BroadCov` varchar(255) DEFAULT NULL,
  `LocationCounterNumber` varchar(255) DEFAULT NULL,
  `ExtendReptPeriod` varchar(255) DEFAULT NULL,
  `SchedPremMod` varchar(255) DEFAULT NULL,
  `AdmiraltyFELALiabilityLimits` varchar(255) DEFAULT NULL,
  `AdmiraltyFELAMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AlcoholDrugFreeCreditInd` varchar(255) DEFAULT NULL,
  `AlcDrugFreeCreditAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheRate` decimal(28,6) DEFAULT NULL,
  `ContrClassPremAdjCreditFactor` varchar(255) DEFAULT NULL,
  `ContrClassPremAdjCreditAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleDiscountRatePct` decimal(28,6) DEFAULT NULL,
  `DiseaseAbrasiveAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAbrasivePaDeAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAsbestosAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAtomicEnergyAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseCoalmineAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseExperienceAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundryIronAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundryNoFerrAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundrySteelAmt` decimal(28,6) DEFAULT NULL,
  `ELIncreasedLimitPct` decimal(28,6) DEFAULT NULL,
  `ELIncreaseLimitMinPremAmt` decimal(28,6) DEFAULT NULL,
  `ELIncreaseLimitMinPremChrgAmt` decimal(28,6) DEFAULT NULL,
  `EstimatedAnnualPremiumAmt` decimal(28,6) DEFAULT NULL,
  `ExpenseConstantAmt` decimal(28,6) DEFAULT NULL,
  `ExperienceMeritRating` varchar(255) DEFAULT NULL,
  `ExperienceModificationFactor` varchar(255) DEFAULT NULL,
  `ManagedCareCreditInd` varchar(255) DEFAULT NULL,
  `ManagedCareCreditAmt` decimal(28,6) DEFAULT NULL,
  `NAICS` varchar(255) DEFAULT NULL,
  `NCCIRiskIdNumber` varchar(255) DEFAULT NULL,
  `NumberOfClaims` varchar(255) DEFAULT NULL,
  `PremiumDiscountAmt` decimal(28,6) DEFAULT NULL,
  `RatingBureauID` varchar(255) DEFAULT NULL,
  `CertifiedSafetyHealthProgPct` decimal(28,6) DEFAULT NULL,
  `SchedRateClassPeculiarities` varchar(255) DEFAULT NULL,
  `SchedRateEmployee` varchar(255) DEFAULT NULL,
  `SchedRateManagement` varchar(255) DEFAULT NULL,
  `SchedRateMedicalFacilities` varchar(255) DEFAULT NULL,
  `SchedRateMgmtSafetyOrg` varchar(255) DEFAULT NULL,
  `SchedRatePremises` varchar(255) DEFAULT NULL,
  `SchedRateSafetyDevices` varchar(255) DEFAULT NULL,
  `SIC` varchar(255) DEFAULT NULL,
  `StandardPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjust` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjustSign` varchar(255) DEFAULT NULL,
  `TerrorismAmt` decimal(28,6) DEFAULT NULL,
  `TerrorismRate` decimal(28,6) DEFAULT NULL,
  `TotalManualPremimAmt` decimal(28,6) DEFAULT NULL,
  `TotalSubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationInd` varchar(255) DEFAULT NULL,
  `WaiverSubrogationRate` decimal(28,6) DEFAULT NULL,
  `YearsInBusiness` varchar(255) DEFAULT NULL,
  `DrugFreeWorkplace` varchar(255) DEFAULT NULL,
  `StatSourceIdRef` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`),
  KEY `PolicyRenewalOfferStats_1` (`PolicyNumber`),
  KEY `PolicyRenewalOfferStats_2` (`CombinedKey`),
  KEY `PolicyRenewalOfferStats_3` (`AccountingDt`),
  KEY `PolicyRenewalOfferStats_4` (`StatusCd`),
  KEY `PolicyRenewalOfferStats_5` (`CommissionKey`),
  KEY `PolicyRenewalOfferStats_Idx_1` (`CombinedKey`,`StatusCd`),
  KEY `PolicyRenewalOfferStats_Idx_2` (`CombinedKey`,`AccountingDt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policyrenewalofferstats`
--

LOCK TABLES `policyrenewalofferstats` WRITE;
/*!40000 ALTER TABLE `policyrenewalofferstats` DISABLE KEYS */;
/*!40000 ALTER TABLE `policyrenewalofferstats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policystats`
--

DROP TABLE IF EXISTS `policystats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policystats` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `StatSequence` int(11) DEFAULT NULL,
  `StatSequenceReplace` int(11) DEFAULT NULL,
  `PolicyRef` int(11) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `LineCd` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `CoverageCd` varchar(255) DEFAULT NULL,
  `RiskCd` varchar(255) DEFAULT NULL,
  `RiskTypeCd` varchar(255) DEFAULT NULL,
  `SubTypeCd` varchar(255) DEFAULT NULL,
  `CoverageItemCd` varchar(255) DEFAULT NULL,
  `FeeCd` varchar(255) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `CombinedKey` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `TransactionEffectiveDt` date DEFAULT NULL,
  `AccountingDt` date DEFAULT NULL,
  `WrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WrittenCommissionAmt` decimal(28,6) DEFAULT NULL,
  `WrittenPremiumFeeAmt` decimal(28,6) DEFAULT NULL,
  `WrittenCommissionFeeAmt` decimal(28,6) DEFAULT NULL,
  `EarnDays` int(11) DEFAULT NULL,
  `InforceChangeAmt` decimal(28,6) DEFAULT NULL,
  `PolicyYear` varchar(255) DEFAULT NULL,
  `TermDays` int(11) DEFAULT NULL,
  `InsuranceTypeCd` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `NewRenewalCd` varchar(255) DEFAULT NULL,
  `Limit1` varchar(255) DEFAULT NULL,
  `Limit2` varchar(255) DEFAULT NULL,
  `Limit3` varchar(255) DEFAULT NULL,
  `Deductible1` varchar(255) DEFAULT NULL,
  `Deductible2` varchar(255) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `CoinsurancePct` decimal(28,6) DEFAULT NULL,
  `CoveredPerilsCd` varchar(255) DEFAULT NULL,
  `CommissionKey` varchar(255) DEFAULT NULL,
  `CommissionAreaCd` varchar(255) DEFAULT NULL,
  `RateAreaName` varchar(255) DEFAULT NULL,
  `StatData` varchar(255) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `ShortRateStatInd` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `BillMethod` varchar(255) DEFAULT NULL,
  `StatementAccountNumber` varchar(255) DEFAULT NULL,
  `StatementAccountRef` int(11) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `ProducerCd` varchar(255) DEFAULT NULL,
  `BusinessSourceCd` varchar(255) DEFAULT NULL,
  `PreviousCarrierCd` varchar(255) DEFAULT NULL,
  `ConstructionCd` varchar(255) DEFAULT NULL,
  `YearBuilt` varchar(255) DEFAULT NULL,
  `SqFt` varchar(255) DEFAULT NULL,
  `Stories` varchar(255) DEFAULT NULL,
  `OccupancyCd` varchar(255) DEFAULT NULL,
  `Units` varchar(255) DEFAULT NULL,
  `ProtectionClass` varchar(255) DEFAULT NULL,
  `TerritoryCd` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  `PolicyStatusCd` varchar(255) DEFAULT NULL,
  `PolicyGroupCd` varchar(255) DEFAULT NULL,
  `CancelReason` varchar(255) DEFAULT NULL,
  `PayPlanCd` varchar(255) DEFAULT NULL,
  `ClassCd` varchar(255) DEFAULT NULL,
  `AgeOfHome` varchar(255) DEFAULT NULL,
  `LocationAddress1` varchar(255) DEFAULT NULL,
  `LocationAddress2` varchar(255) DEFAULT NULL,
  `LocationCity` varchar(255) DEFAULT NULL,
  `LocationCounty` varchar(255) DEFAULT NULL,
  `LocationState` varchar(255) DEFAULT NULL,
  `LocationZip` varchar(255) DEFAULT NULL,
  `VehicleNumber` varchar(255) DEFAULT NULL,
  `VehicleYear` varchar(255) DEFAULT NULL,
  `VehicleManufacturer` varchar(255) DEFAULT NULL,
  `VehicleModel` varchar(255) DEFAULT NULL,
  `VehicleType` varchar(255) DEFAULT NULL,
  `DriverNumber` varchar(255) DEFAULT NULL,
  `DriverFirst` varchar(255) DEFAULT NULL,
  `DriverMI` varchar(255) DEFAULT NULL,
  `DriverAge` varchar(255) DEFAULT NULL,
  `DriverLast` varchar(255) DEFAULT NULL,
  `DriverGenderCd` varchar(255) DEFAULT NULL,
  `RelationshipToInsuredCd` varchar(255) DEFAULT NULL,
  `DriverStartDt` date DEFAULT NULL,
  `PointsChargeable` int(11) DEFAULT NULL,
  `PointsCharged` int(11) DEFAULT NULL,
  `Limit4` varchar(255) DEFAULT NULL,
  `Limit5` varchar(255) DEFAULT NULL,
  `Limit6` varchar(255) DEFAULT NULL,
  `ControllingStateCd` varchar(255) DEFAULT NULL,
  `ControllingProductVersionIdRef` varchar(255) DEFAULT NULL,
  `DividendPlanCd` varchar(255) DEFAULT NULL,
  `NAICNumber` varchar(255) DEFAULT NULL,
  `ISSNumber` varchar(255) DEFAULT NULL,
  `ISONumber` varchar(255) DEFAULT NULL,
  `PolicyTerm` varchar(255) DEFAULT NULL,
  `County` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL,
  `ExposureBasis` varchar(255) DEFAULT NULL,
  `Exposure` varchar(255) DEFAULT NULL,
  `BurglaryOptionCd` varchar(255) DEFAULT NULL,
  `SprinklerCd` varchar(255) DEFAULT NULL,
  `NamedPerils` varchar(255) DEFAULT NULL,
  `BuyBackCovg` varchar(255) DEFAULT NULL,
  `SICCd` varchar(255) DEFAULT NULL,
  `TerrorismCovCd` varchar(255) DEFAULT NULL,
  `PolicyFormCd` varchar(255) DEFAULT NULL,
  `CompanyNumber` varchar(255) DEFAULT NULL,
  `MGAIndicator` varchar(255) DEFAULT NULL,
  `MultiPolicyAffiliatedSameCo` varchar(255) DEFAULT NULL,
  `PerPolIndCd` varchar(255) DEFAULT NULL,
  `BondTypeCd` varchar(255) DEFAULT NULL,
  `TypeOfBusiness` varchar(255) DEFAULT NULL,
  `Subline` varchar(255) DEFAULT NULL,
  `TypePolicyCode` varchar(255) DEFAULT NULL,
  `ZipCode` varchar(255) DEFAULT NULL,
  `Deductible3` varchar(255) DEFAULT NULL,
  `ScheduleItemClass` varchar(255) DEFAULT NULL,
  `SchedPremMod` varchar(255) DEFAULT NULL,
  `BldgCounterNumber` varchar(255) DEFAULT NULL,
  `LocationCounterNumber` varchar(255) DEFAULT NULL,
  `BroadCov` varchar(255) DEFAULT NULL,
  `ExtendReptPeriod` varchar(255) DEFAULT NULL,
  `AdmiraltyFELALiabilityLimits` varchar(255) DEFAULT NULL,
  `AdmiraltyFELAMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AlcoholDrugFreeCreditInd` varchar(255) DEFAULT NULL,
  `AlcDrugFreeCreditAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheRate` decimal(28,6) DEFAULT NULL,
  `ContrClassPremAdjCreditFactor` varchar(255) DEFAULT NULL,
  `ContrClassPremAdjCreditAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleDiscountRatePct` decimal(28,6) DEFAULT NULL,
  `DiseaseAbrasiveAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAbrasivePaDeAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAsbestosAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAtomicEnergyAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseCoalmineAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseExperienceAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundryIronAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundryNoFerrAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundrySteelAmt` decimal(28,6) DEFAULT NULL,
  `ELIncreasedLimitPct` decimal(28,6) DEFAULT NULL,
  `ELIncreaseLimitMinPremAmt` decimal(28,6) DEFAULT NULL,
  `ELIncreaseLimitMinPremChrgAmt` decimal(28,6) DEFAULT NULL,
  `EstimatedAnnualPremiumAmt` decimal(28,6) DEFAULT NULL,
  `ExpenseConstantAmt` decimal(28,6) DEFAULT NULL,
  `ExperienceMeritRating` varchar(255) DEFAULT NULL,
  `ExperienceModificationFactor` varchar(255) DEFAULT NULL,
  `ManagedCareCreditInd` varchar(255) DEFAULT NULL,
  `ManagedCareCreditAmt` decimal(28,6) DEFAULT NULL,
  `NAICS` varchar(255) DEFAULT NULL,
  `NCCIRiskIdNumber` varchar(255) DEFAULT NULL,
  `NumberOfClaims` varchar(255) DEFAULT NULL,
  `PremiumDiscountAmt` decimal(28,6) DEFAULT NULL,
  `RatingBureauID` varchar(255) DEFAULT NULL,
  `CertifiedSafetyHealthProgPct` decimal(28,6) DEFAULT NULL,
  `SchedRateClassPeculiarities` varchar(255) DEFAULT NULL,
  `SchedRateEmployee` varchar(255) DEFAULT NULL,
  `SchedRateManagement` varchar(255) DEFAULT NULL,
  `SchedRateMedicalFacilities` varchar(255) DEFAULT NULL,
  `SchedRateMgmtSafetyOrg` varchar(255) DEFAULT NULL,
  `SchedRatePremises` varchar(255) DEFAULT NULL,
  `SchedRateSafetyDevices` varchar(255) DEFAULT NULL,
  `SIC` varchar(255) DEFAULT NULL,
  `StandardPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjust` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjustSign` varchar(255) DEFAULT NULL,
  `TerrorismAmt` decimal(28,6) DEFAULT NULL,
  `TerrorismRate` decimal(28,6) DEFAULT NULL,
  `TotalManualPremimAmt` decimal(28,6) DEFAULT NULL,
  `TotalSubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationInd` varchar(255) DEFAULT NULL,
  `WaiverSubrogationRate` decimal(28,6) DEFAULT NULL,
  `YearsInBusiness` varchar(255) DEFAULT NULL,
  `DrugFreeWorkplace` varchar(255) DEFAULT NULL,
  `StatSourceIdRef` varchar(255) DEFAULT NULL,
  `ConversionTemplateIdRef` varchar(255) DEFAULT NULL,
  `ConversionGroup` varchar(255) DEFAULT NULL,
  `ConversionJobRef` varchar(255) DEFAULT NULL,
  `ConversionFileName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`),
  KEY `PolicyStats_1` (`PolicyNumber`),
  KEY `PolicyStats_2` (`CombinedKey`),
  KEY `PolicyStats_3` (`AccountingDt`),
  KEY `PolicyStats_4` (`StatusCd`),
  KEY `PolicyStats_5` (`CommissionKey`),
  KEY `PolicyStats_Idx_1` (`CombinedKey`,`StatusCd`),
  KEY `PolicyStats_Idx_2` (`CombinedKey`,`AccountingDt`),
  KEY `PolicyStats_Idx_3` (`AccountingDt`,`PolicyRef`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policystats`
--

LOCK TABLES `policystats` WRITE;
/*!40000 ALTER TABLE `policystats` DISABLE KEYS */;
/*!40000 ALTER TABLE `policystats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policysummarystats`
--

DROP TABLE IF EXISTS `policysummarystats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policysummarystats` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `StatSequence` int(11) DEFAULT NULL,
  `StatSequenceReplace` int(11) DEFAULT NULL,
  `ReportPeriod` varchar(255) DEFAULT NULL,
  `UpdateDt` date DEFAULT NULL,
  `PolicyRef` int(11) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `LineCd` varchar(255) DEFAULT NULL,
  `RiskCd` varchar(255) DEFAULT NULL,
  `RiskTypeCd` varchar(255) DEFAULT NULL,
  `SubTypeCd` varchar(255) DEFAULT NULL,
  `CoverageCd` varchar(255) DEFAULT NULL,
  `CoverageItemCd` varchar(255) DEFAULT NULL,
  `FeeCd` varchar(255) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `CombinedKey` varchar(255) DEFAULT NULL,
  `AccountingDt` date DEFAULT NULL,
  `PolicyYear` varchar(255) DEFAULT NULL,
  `InsuranceTypeCd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `NewRenewalCd` varchar(255) DEFAULT NULL,
  `Limit1` varchar(255) DEFAULT NULL,
  `Limit2` varchar(255) DEFAULT NULL,
  `Limit3` varchar(255) DEFAULT NULL,
  `Limit4` varchar(255) DEFAULT NULL,
  `Limit5` varchar(255) DEFAULT NULL,
  `Limit6` varchar(255) DEFAULT NULL,
  `Limit7` varchar(255) DEFAULT NULL,
  `Limit8` varchar(255) DEFAULT NULL,
  `Limit9` varchar(255) DEFAULT NULL,
  `Deductible1` varchar(255) DEFAULT NULL,
  `Deductible2` varchar(255) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `CoinsurancePct` decimal(28,6) DEFAULT NULL,
  `CoveredPerilsCd` varchar(255) DEFAULT NULL,
  `CommissionAreaCd` varchar(255) DEFAULT NULL,
  `MTDWrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `TTDWrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `YTDWrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `MTDWrittenCommissionAmt` decimal(28,6) DEFAULT NULL,
  `YTDWrittenCommissionAmt` decimal(28,6) DEFAULT NULL,
  `TTDWrittenCommissionAmt` decimal(28,6) DEFAULT NULL,
  `MTDWrittenPremiumFeeAmt` decimal(28,6) DEFAULT NULL,
  `YTDWrittenPremiumFeeAmt` decimal(28,6) DEFAULT NULL,
  `TTDWrittenPremiumFeeAmt` decimal(28,6) DEFAULT NULL,
  `MTDWrittenCommissionFeeAmt` decimal(28,6) DEFAULT NULL,
  `YTDWrittenCommissionFeeAmt` decimal(28,6) DEFAULT NULL,
  `TTDWrittenCommissionFeeAmt` decimal(28,6) DEFAULT NULL,
  `InforceAmt` decimal(28,6) DEFAULT NULL,
  `LastInforceAmt` decimal(28,6) DEFAULT NULL,
  `MonthEarnedPremiumAmt` decimal(28,6) DEFAULT NULL,
  `MTDEarnedPremiumAmt` decimal(28,6) DEFAULT NULL,
  `YTDEarnedPremiumAmt` decimal(28,6) DEFAULT NULL,
  `TTDEarnedPremiumAmt` decimal(28,6) DEFAULT NULL,
  `PolicyInforceCount` int(11) DEFAULT NULL,
  `LocationInforceCount` int(11) DEFAULT NULL,
  `RiskInforceCount` int(11) DEFAULT NULL,
  `MonthUnearnedAmt` decimal(28,6) DEFAULT NULL,
  `UnearnedAmt` decimal(28,6) DEFAULT NULL,
  `UnearnedM00Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM01Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM02Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM03Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM04Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM05Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM06Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM07Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM08Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM09Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM10Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM11Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM12Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM13Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM14Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM15Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM16Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM17Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM18Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM19Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM20Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM21Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM22Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM23Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM24Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM25Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM26Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM27Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM28Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM29Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM30Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM31Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM32Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM33Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM34Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM35Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM36Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM37Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM38Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM39Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM40Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM41Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM42Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM43Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM44Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM45Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM46Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM47Amt` decimal(28,6) DEFAULT NULL,
  `UnearnedM48Amt` decimal(28,6) DEFAULT NULL,
  `RateAreaName` varchar(255) DEFAULT NULL,
  `StatData` varchar(255) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `BillMethod` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `ProducerCd` varchar(255) DEFAULT NULL,
  `BusinessSourceCd` varchar(255) DEFAULT NULL,
  `PreviousCarrierCd` varchar(255) DEFAULT NULL,
  `ConstructionCd` varchar(255) DEFAULT NULL,
  `YearBuilt` varchar(255) DEFAULT NULL,
  `SqFt` varchar(255) DEFAULT NULL,
  `Stories` varchar(255) DEFAULT NULL,
  `OccupancyCd` varchar(255) DEFAULT NULL,
  `Units` varchar(255) DEFAULT NULL,
  `ProtectionClass` varchar(255) DEFAULT NULL,
  `TerritoryCd` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `ControllingStateCd` varchar(255) DEFAULT NULL,
  `ControllingProductVersionIdRef` varchar(255) DEFAULT NULL,
  `DividendPlanCd` varchar(255) DEFAULT NULL,
  `NAICNumber` varchar(255) DEFAULT NULL,
  `ISSNumber` varchar(255) DEFAULT NULL,
  `ISONumber` varchar(255) DEFAULT NULL,
  `PolicyTerm` varchar(255) DEFAULT NULL,
  `County` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL,
  `ClassCd` varchar(255) DEFAULT NULL,
  `ExposureBasis` varchar(255) DEFAULT NULL,
  `Exposure` varchar(255) DEFAULT NULL,
  `BurglaryOptionCd` varchar(255) DEFAULT NULL,
  `SprinklerCd` varchar(255) DEFAULT NULL,
  `NamedPerils` varchar(255) DEFAULT NULL,
  `BuyBackCovg` varchar(255) DEFAULT NULL,
  `SICCd` varchar(255) DEFAULT NULL,
  `TerrorismCovCd` varchar(255) DEFAULT NULL,
  `PolicyFormCd` varchar(255) DEFAULT NULL,
  `CompanyNumber` varchar(255) DEFAULT NULL,
  `MGAIndicator` varchar(255) DEFAULT NULL,
  `MultiPolicyAffiliatedSameCo` varchar(255) DEFAULT NULL,
  `PerPolIndCd` varchar(255) DEFAULT NULL,
  `BondTypeCd` varchar(255) DEFAULT NULL,
  `TypeOfBusiness` varchar(255) DEFAULT NULL,
  `Subline` varchar(255) DEFAULT NULL,
  `TypePolicyCode` varchar(255) DEFAULT NULL,
  `ZipCode` varchar(255) DEFAULT NULL,
  `AdmiraltyFELALiabilityLimits` varchar(255) DEFAULT NULL,
  `AdmiraltyFELAMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AlcoholDrugFreeCreditInd` varchar(255) DEFAULT NULL,
  `AlcDrugFreeCreditAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheRate` decimal(28,6) DEFAULT NULL,
  `ContrClassPremAdjCreditFactor` varchar(255) DEFAULT NULL,
  `ContrClassPremAdjCreditAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleDiscountRatePct` decimal(28,6) DEFAULT NULL,
  `DiseaseAbrasiveAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAbrasivePaDeAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAsbestosAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAtomicEnergyAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseCoalmineAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseExperienceAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundryIronAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundryNoFerrAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundrySteelAmt` decimal(28,6) DEFAULT NULL,
  `ELIncreasedLimitPct` decimal(28,6) DEFAULT NULL,
  `ELIncreaseLimitMinPremAmt` decimal(28,6) DEFAULT NULL,
  `ELIncreaseLimitMinPremChrgAmt` decimal(28,6) DEFAULT NULL,
  `EstimatedAnnualPremiumAmt` decimal(28,6) DEFAULT NULL,
  `ExpenseConstantAmt` decimal(28,6) DEFAULT NULL,
  `ExperienceMeritRating` varchar(255) DEFAULT NULL,
  `ExperienceModificationFactor` varchar(255) DEFAULT NULL,
  `ManagedCareCreditInd` varchar(255) DEFAULT NULL,
  `ManagedCareCreditAmt` decimal(28,6) DEFAULT NULL,
  `NAICS` varchar(255) DEFAULT NULL,
  `NCCIRiskIdNumber` varchar(255) DEFAULT NULL,
  `NumberOfClaims` varchar(255) DEFAULT NULL,
  `PremiumDiscountAmt` decimal(28,6) DEFAULT NULL,
  `RatingBureauID` varchar(255) DEFAULT NULL,
  `CertifiedSafetyHealthProgPct` decimal(28,6) DEFAULT NULL,
  `SchedRateClassPeculiarities` varchar(255) DEFAULT NULL,
  `SchedRateEmployee` varchar(255) DEFAULT NULL,
  `SchedRateManagement` varchar(255) DEFAULT NULL,
  `SchedRateMedicalFacilities` varchar(255) DEFAULT NULL,
  `SchedRateMgmtSafetyOrg` varchar(255) DEFAULT NULL,
  `SchedRatePremises` varchar(255) DEFAULT NULL,
  `SchedRateSafetyDevices` varchar(255) DEFAULT NULL,
  `SIC` varchar(255) DEFAULT NULL,
  `StandardPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjust` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjustSign` varchar(255) DEFAULT NULL,
  `TerrorismAmt` decimal(28,6) DEFAULT NULL,
  `TerrorismRate` decimal(28,6) DEFAULT NULL,
  `TotalManualPremimAmt` decimal(28,6) DEFAULT NULL,
  `TotalSubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationInd` varchar(255) DEFAULT NULL,
  `WaiverSubrogationRate` decimal(28,6) DEFAULT NULL,
  `YearsInBusiness` varchar(255) DEFAULT NULL,
  `DrugFreeWorkplace` varchar(255) DEFAULT NULL,
  `ConversionTemplateIdRef` varchar(255) DEFAULT NULL,
  `ConversionGroup` varchar(255) DEFAULT NULL,
  `ConversionJobRef` varchar(255) DEFAULT NULL,
  `ConversionFileName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`),
  KEY `PolicySummaryStats_1` (`PolicyNumber`),
  KEY `PolicySummaryStats_Idx_1` (`ReportPeriod`,`CombinedKey`,`UpdateDt`),
  KEY `PolicySummaryStats_Idx_2` (`ReportPeriod`,`PolicyRef`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policysummarystats`
--

LOCK TABLES `policysummarystats` WRITE;
/*!40000 ALTER TABLE `policysummarystats` DISABLE KEYS */;
/*!40000 ALTER TABLE `policysummarystats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processor`
--

DROP TABLE IF EXISTS `processor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processor` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processor`
--

LOCK TABLES `processor` WRITE;
/*!40000 ALTER TABLE `processor` DISABLE KEYS */;
/*!40000 ALTER TABLE `processor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processorlookup`
--

DROP TABLE IF EXISTS `processorlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processorlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processorlookup`
--

LOCK TABLES `processorlookup` WRITE;
/*!40000 ALTER TABLE `processorlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `processorlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider`
--

DROP TABLE IF EXISTS `provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider`
--

LOCK TABLES `provider` WRITE;
/*!40000 ALTER TABLE `provider` DISABLE KEYS */;
INSERT INTO `provider` VALUES (1,25,'admin','2017-05-08 14:36:44','<?xml version=\"1.0\" encoding=\"UTF-8\"?><Provider SystemId=\"1\" id=\"Provider-493130578-2143305343\" UpdateCount=\"25\" UpdateUser=\"admin\" UpdateTimestamp=\"05/08/2017 02:36:44.431 PM\" Version=\"1.50\" IndexName=\"SPIAAGENCYINC\" ProviderTypeCd=\"Producer\" StatusCd=\"Active\" StatusDt=\"20150101\" ProviderNumber=\"123456\" PaymentPreferenceCd=\"Check\" PreferredDeliveryMethod=\"None\" ProviderOnCommissionPlan=\"No\" CombinePaymentInd=\"Yes\"><ProviderAcct id=\"ProviderAcct-1521436002-1758358864\"><PartyInfo id=\"PartyInfo-347934414-1415261897\" PartyTypeCd=\"AcctParty\"><EmailInfo id=\"EmailInfo-334957087-517428174\" EmailTypeCd=\"AcctEmail\" PreferredInd=\"No\" /><TaxInfo id=\"TaxInfo-461838953-896423960\" TaxTypeCd=\"AcctTaxInfo\" WithholdingExemptInd=\"Yes\" Received1099Ind=\"Yes\" ReceivedW9Ind=\"Yes\" Required1099Ind=\"Yes\" /><PhoneInfo id=\"PhoneInfo-352901420-2133890000\" PhoneTypeCd=\"AcctPhone\" PreferredInd=\"No\" /><NameInfo id=\"NameInfo-1962927477-1874899543\" NameTypeCd=\"AcctName\" CommercialName=\"SPIA Agency, Inc\" CommercialName2=\"SPIA Agency, Inc\" /><Addr id=\"Addr-600035118-1477867157\" AddrTypeCd=\"AcctMailingAddr\" Addr1=\"100 Great Oaks Blvd\" City=\"San Jose\" StateProvCd=\"CA\" PostalCode=\"95119-1462\" /><PhoneInfo id=\"PhoneInfo-1421662683-1892788641\" PhoneTypeCd=\"AcctFax\" PreferredInd=\"No\" /><Addr id=\"Addr-1784024653-397884935\" AddrTypeCd=\"AcctMailingLookupAddr\" /><Addr id=\"Addr-707803488-1098465389\" AddrTypeCd=\"AcctStreetAddr\" /></PartyInfo></ProviderAcct><UserDefined id=\"UserDefined-909695484-1901193659\" UserDefinedCd=\"Misc1\" /><StateInfo id=\"StateInfo-453874915-1799635375\" CarrierGroup=\"IIC\" StateCd=\"CA\" AppointedDt=\"20150101\" Status=\"Active\" /><ChangeInfo id=\"ChangeInfo-1237183661-1450067759\" UserId=\"admin\" AddDt=\"20160414\" AddTm=\"22:03:15 CDT\" EffectiveDt=\"20160414\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-986307890-992477860\" IdRef=\"StateInfo-453874915-1799635375\" ParentIdRef=\"Provider-493130578-2143305343\" ActionCd=\"A\" Field=\"StateInfo\" Status=\"Pending\" /><ModInfo id=\"ModInfo-724885642-2014849436\" IdRef=\"StateInfo-453874915-1799635375\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1613882796-146185247\" IdRef=\"StateInfo-453874915-1799635375\" ActionCd=\"M\" NewValue=\"CA\" Field=\"StateCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-76160307-1038582007\" IdRef=\"StateInfo-453874915-1799635375\" ActionCd=\"M\" NewValue=\"20150101\" Field=\"AppointedDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1377050210-1862153819\" IdRef=\"StateInfo-453874915-1799635375\" ActionCd=\"M\" NewValue=\"Active\" Field=\"Status\" Status=\"Pending\" /></ChangeInfo><ProviderEDD id=\"ProviderEDD-1277733609-816856552\" ReportingInd=\"Yes\"><PartyInfo id=\"PartyInfo-558391927-1386883908\" PartyTypeCd=\"EDDParty\"><EmailInfo id=\"EmailInfo-143333663-200864577\" EmailTypeCd=\"EDDEmail\" PreferredInd=\"No\" /><TaxInfo id=\"TaxInfo-1585725814-1426236564\" TaxTypeCd=\"EDDTaxInfo\" /><PhoneInfo id=\"PhoneInfo-897233496-1268358326\" PhoneTypeCd=\"EDDPhone\" PreferredInd=\"No\" /><NameInfo id=\"NameInfo-1532800026-1790084580\" NameTypeCd=\"EDDName\" /><Addr id=\"Addr-925646142-811039553\" AddrTypeCd=\"EDDMailingAddr\" /><PhoneInfo id=\"PhoneInfo-453650235-1470414304\" PhoneTypeCd=\"EDDFax\" PreferredInd=\"No\" /><Addr id=\"Addr-2049876367-144132247\" AddrTypeCd=\"EDDStreetAddr\" /></PartyInfo></ProviderEDD><LicensedProducts id=\"LicensedProducts-507415788-357555566\"><LicensedProduct id=\"LicensedProduct-1429906623-1687668745\" StatusCd=\"Active\" LicenseClassCd=\"WC\" CarrierGroup=\"IIC\" StateProvCd=\"CA\" EffectiveDt=\"20150101\" CommissionNewPct=\"10\" CommissionRenewalPct=\"10\" SubmitToCd=\"Group\" /><LicensedProduct id=\"LicensedProduct-2011298118-975696316\" StatusCd=\"Active\" LicenseClassCd=\"WC\" CarrierGroup=\"IIC\" StateProvCd=\"IL\" EffectiveDt=\"20150101\" CommissionNewPct=\"10\" CommissionRenewalPct=\"10\" SubmitToCd=\"Group\" /><LicensedProduct id=\"LicensedProduct-927220981-1529488943\" StatusCd=\"Active\" LicenseClassCd=\"WC\" CarrierGroup=\"IIC\" StateProvCd=\"WI\" EffectiveDt=\"20150101\" CommissionNewPct=\"10\" CommissionRenewalPct=\"10\" SubmitToCd=\"Group\" /><LicensedProduct id=\"LicensedProduct-1095313398-833905527\" StatusCd=\"Active\" LicenseClassCd=\"WC\" CarrierGroup=\"IIC\" StateProvCd=\"IN\" EffectiveDt=\"20150101\" CommissionNewPct=\"10\" CommissionRenewalPct=\"10\" SubmitToCd=\"Group\" /><LicensedProduct id=\"LicensedProduct-1431548644-1520257032\" StatusCd=\"Active\" LicenseClassCd=\"WC\" CarrierGroup=\"IIC\" StateProvCd=\"HI\" EffectiveDt=\"20150101\" CommissionNewPct=\"10\" CommissionRenewalPct=\"10\" SubmitToCd=\"Group\" /><LicensedProduct id=\"LicensedProduct-29508869-7721407\" StatusCd=\"Active\" LicenseClassCd=\"CPP\" CarrierGroup=\"IIC\" StateProvCd=\"CA\" EffectiveDt=\"19990101\" CommissionNewPct=\"15\" CommissionRenewalPct=\"12\" SubmitToCd=\"Group\" /><LicensedProduct id=\"LicensedProduct-21016258-16878329\" StatusCd=\"Active\" LicenseClassCd=\"PP\" CarrierGroup=\"IIC\" StateProvCd=\"TX\" EffectiveDt=\"19990101\" CommissionNewPct=\"15\" CommissionRenewalPct=\"12\" SubmitToCd=\"Group\" /><LicensedProduct id=\"LicensedProduct-13988464-13469125\" StatusCd=\"Active\" LicenseClassCd=\"PP\" CarrierGroup=\"IIC\" StateProvCd=\"VA\" EffectiveDt=\"19990101\" CommissionNewPct=\"15\" CommissionRenewalPct=\"12\" SubmitToCd=\"Group\" /><LicensedProduct id=\"LicensedProduct-22926393-28884318\" StatusCd=\"Active\" LicenseClassCd=\"BOP\" CarrierGroup=\"IIC\" StateProvCd=\"CA\" EffectiveDt=\"19990101\" CommissionNewPct=\"15\" CommissionRenewalPct=\"12\" SubmitToCd=\"Group\" /><LicensedProduct id=\"LicensedProduct-15839946-13004827\" StatusCd=\"Active\" LicenseClassCd=\"PUL\" CarrierGroup=\"IIC\" StateProvCd=\"CA\" EffectiveDt=\"19990101\" CommissionNewPct=\"15\" CommissionRenewalPct=\"12\" SubmitToCd=\"Group\" /><LicensedProduct id=\"LicensedProduct-17182699-18627354\" StatusCd=\"Active\" LicenseClassCd=\"PP\" CarrierGroup=\"IIC\" StateProvCd=\"CA\" EffectiveDt=\"19990101\" CommissionNewPct=\"15\" CommissionRenewalPct=\"12\" SubmitToCd=\"Group\" /><LicensedProduct id=\"LicensedProduct-18801868-2203588\" StatusCd=\"Active\" LicenseClassCd=\"PLML\" CarrierGroup=\"IIC\" StateProvCd=\"CA\" EffectiveDt=\"19990101\" CommissionNewPct=\"15\" CommissionRenewalPct=\"12\" SubmitToCd=\"Group\" /><LicensedProduct id=\"LicensedProduct-23864577-11621342\" StatusCd=\"Active\" LicenseClassCd=\"PA\" CarrierGroup=\"IIC\" StateProvCd=\"CA\" EffectiveDt=\"19990101\" CommissionNewPct=\"15\" CommissionRenewalPct=\"12\" SubmitToCd=\"Group\" /><LicensedProduct id=\"LicensedProduct-32614989-25442619\" StatusCd=\"Active\" LicenseClassCd=\"DP\" CarrierGroup=\"IIC\" StateProvCd=\"CA\" EffectiveDt=\"19990101\" CommissionNewPct=\"15\" CommissionRenewalPct=\"12\" SubmitToCd=\"Group\" /></LicensedProducts><PartyInfo id=\"PartyInfo-43104147-1874978217\" PartyTypeCd=\"ProviderParty\"><EmailInfo id=\"EmailInfo-661253580-1206233125\" EmailTypeCd=\"ProviderEmail\" EmailAddr=\"BitBucket@iscs.com\" PreferredInd=\"No\" /><TaxInfo id=\"TaxInfo-1378383929-1752394822\" TaxTypeCd=\"ProviderTaxInfo\" LegalEntityCd=\"LLC\" /><PhoneInfo id=\"PhoneInfo-556637132-1455564082\" PhoneTypeCd=\"ProviderPrimaryPhone\" PreferredInd=\"No\" /><NameInfo id=\"NameInfo-1451627776-684456553\" NameTypeCd=\"ProviderName\" GivenName=\"SPIA Agency, Inc\" CommercialName=\"SPIA Agency, Inc\" /><Addr id=\"Addr-220376900-1161803109\" AddrTypeCd=\"ProviderMailingAddr\" /><PhoneInfo id=\"PhoneInfo-1051567531-1415808731\" PhoneTypeCd=\"ProviderSecondaryPhone\" PreferredInd=\"No\" /><PhoneInfo id=\"PhoneInfo-425559136-1592904140\" PhoneTypeCd=\"ProviderFax\" PreferredInd=\"No\" /><Addr id=\"Addr-808736550-1624987461\" AddrTypeCd=\"ProviderBillingAddr\" Addr1=\"100 Great Oaks Blvd\" City=\"San Jose\" StateProvCd=\"CA\" PostalCode=\"95119-1462\" /><Addr id=\"Addr-2015075477-1085774446\" AddrTypeCd=\"ProviderStreetAddr\" Addr1=\"100 Great Oaks Blvd\" City=\"San Jose\" StateProvCd=\"CA\" PostalCode=\"95119-1462\" /></PartyInfo><ElectronicPaymentContract id=\"ElectronicPaymentContract-2008520435-1841421331\" ContractTypeCd=\"Commission\"><ElectronicPaymentDestination id=\"ElectronicPaymentDestination-964187325-35654990\" DestinationTypeCd=\"CommissionDestination\" ACHName=\"SPIA Agency, Inc\" /></ElectronicPaymentContract><ProducerInfo id=\"ProducerInfo-1210015761-563985638\" ProducerTypeCd=\"Producer\" AppointedDt=\"20150101\" SubmitToCd=\"Group\" PayToCd=\"Producer\" DirectPortalInd=\"Yes\" /><UserDefined id=\"UserDefined-467813009-1882015790\" UserDefinedCd=\"Misc2\" /><UserDefined id=\"UserDefined-2787566-2118694241\" UserDefinedCd=\"Misc3\" /><ElectronicPaymentContract id=\"ElectronicPaymentContract-1231754231-715747213\" ContractTypeCd=\"Premium\"><ElectronicPaymentSource id=\"ElectronicPaymentSource-1368329177-1397171931\" SourceTypeCd=\"PremiumSource\" ACHName=\"SPIA Agency, Inc\" /></ElectronicPaymentContract><StateInfo id=\"StateInfo-1196300376-363278516\" CarrierGroup=\"IIC\" StateCd=\"WI\" AppointedDt=\"20150101\" Status=\"Active\" /><ChangeInfo id=\"ChangeInfo-365911185-2089788991\" UserId=\"admin\" AddDt=\"20160414\" AddTm=\"22:03:31 CDT\" EffectiveDt=\"20160414\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-77665868-489328732\" IdRef=\"StateInfo-1196300376-363278516\" ParentIdRef=\"Provider-493130578-2143305343\" ActionCd=\"A\" Field=\"StateInfo\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1794464369-1612508738\" IdRef=\"StateInfo-1196300376-363278516\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1194534040-1790568595\" IdRef=\"StateInfo-1196300376-363278516\" ActionCd=\"M\" NewValue=\"WI\" Field=\"StateCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-710873447-758551696\" IdRef=\"StateInfo-1196300376-363278516\" ActionCd=\"M\" NewValue=\"20150101\" Field=\"AppointedDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1937732170-1445454278\" IdRef=\"StateInfo-1196300376-363278516\" ActionCd=\"M\" NewValue=\"Active\" Field=\"Status\" Status=\"Pending\" /></ChangeInfo><StateInfo id=\"StateInfo-2071531169-2094384793\" CarrierGroup=\"IIC\" StateCd=\"IN\" AppointedDt=\"20150101\" Status=\"Active\" /><ChangeInfo id=\"ChangeInfo-1800569390-272576550\" UserId=\"admin\" AddDt=\"20160414\" AddTm=\"22:03:49 CDT\" EffectiveDt=\"20160414\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-1679996858-501295021\" IdRef=\"StateInfo-2071531169-2094384793\" ParentIdRef=\"Provider-493130578-2143305343\" ActionCd=\"A\" Field=\"StateInfo\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1799027480-1229886479\" IdRef=\"StateInfo-2071531169-2094384793\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1661797074-616972926\" IdRef=\"StateInfo-2071531169-2094384793\" ActionCd=\"M\" NewValue=\"IN\" Field=\"StateCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-404262853-634101285\" IdRef=\"StateInfo-2071531169-2094384793\" ActionCd=\"M\" NewValue=\"20150101\" Field=\"AppointedDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-777716700-941122011\" IdRef=\"StateInfo-2071531169-2094384793\" ActionCd=\"M\" NewValue=\"Active\" Field=\"Status\" Status=\"Pending\" /></ChangeInfo><StateInfo id=\"StateInfo-717058413-314428652\" CarrierGroup=\"IIC\" StateCd=\"HI\" AppointedDt=\"20150101\" Status=\"Active\" /><ChangeInfo id=\"ChangeInfo-370954623-2064810677\" UserId=\"admin\" AddDt=\"20160414\" AddTm=\"22:04:04 CDT\" EffectiveDt=\"20160414\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-1496505823-926755677\" IdRef=\"StateInfo-717058413-314428652\" ParentIdRef=\"Provider-493130578-2143305343\" ActionCd=\"A\" Field=\"StateInfo\" Status=\"Pending\" /><ModInfo id=\"ModInfo-415635639-1205754392\" IdRef=\"StateInfo-717058413-314428652\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1702897713-1427439160\" IdRef=\"StateInfo-717058413-314428652\" ActionCd=\"M\" NewValue=\"HI\" Field=\"StateCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-2109687693-1208278200\" IdRef=\"StateInfo-717058413-314428652\" ActionCd=\"M\" NewValue=\"20150101\" Field=\"AppointedDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-748790825-1580624571\" IdRef=\"StateInfo-717058413-314428652\" ActionCd=\"M\" NewValue=\"Active\" Field=\"Status\" Status=\"Pending\" /></ChangeInfo><StateInfo id=\"StateInfo-559032040-2116042512\" CarrierGroup=\"IIC\" StateCd=\"TX\" AppointedDt=\"20150101\" Status=\"Active\" /><ChangeInfo id=\"ChangeInfo-80673932-1193901272\" UserId=\"admin\" AddDt=\"20160414\" AddTm=\"22:04:24 CDT\" EffectiveDt=\"20160414\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-861853011-237993592\" IdRef=\"StateInfo-559032040-2116042512\" ParentIdRef=\"Provider-493130578-2143305343\" ActionCd=\"A\" Field=\"StateInfo\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1203740084-398263927\" IdRef=\"StateInfo-559032040-2116042512\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-741286602-1386033420\" IdRef=\"StateInfo-559032040-2116042512\" ActionCd=\"M\" NewValue=\"TX\" Field=\"StateCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1777620182-377841953\" IdRef=\"StateInfo-559032040-2116042512\" ActionCd=\"M\" NewValue=\"20150101\" Field=\"AppointedDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1130967113-2026339449\" IdRef=\"StateInfo-559032040-2116042512\" ActionCd=\"M\" NewValue=\"Active\" Field=\"Status\" Status=\"Pending\" /></ChangeInfo><StateInfo id=\"StateInfo-1366057382-2051571773\" CarrierGroup=\"IIC\" StateCd=\"IL\" AppointedDt=\"20150101\" Status=\"Active\" /><ChangeInfo id=\"ChangeInfo-2027146332-1153164926\" UserId=\"admin\" AddDt=\"20160414\" AddTm=\"22:04:38 CDT\" EffectiveDt=\"20160414\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-740230968-831613340\" IdRef=\"StateInfo-1366057382-2051571773\" ParentIdRef=\"Provider-493130578-2143305343\" ActionCd=\"A\" Field=\"StateInfo\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1120154973-1364842964\" IdRef=\"StateInfo-1366057382-2051571773\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-174273794-1292943747\" IdRef=\"StateInfo-1366057382-2051571773\" ActionCd=\"M\" NewValue=\"IL\" Field=\"StateCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-773684129-823496911\" IdRef=\"StateInfo-1366057382-2051571773\" ActionCd=\"M\" NewValue=\"20150101\" Field=\"AppointedDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-583887050-1763359177\" IdRef=\"StateInfo-1366057382-2051571773\" ActionCd=\"M\" NewValue=\"Active\" Field=\"Status\" Status=\"Pending\" /></ChangeInfo><ChangeInfo id=\"ChangeInfo-304261561-175403145\" UserId=\"admin\" AddDt=\"20160414\" AddTm=\"22:04:59 CDT\" EffectiveDt=\"20160414\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-304017712-1135641832\" IdRef=\"LicensedProduct-1429906623-1687668745\" ParentIdRef=\"LicensedProducts-507415788-357555566\" ActionCd=\"A\" Field=\"LicensedProduct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-214704381-629184791\" IdRef=\"LicensedProduct-1429906623-1687668745\" ActionCd=\"M\" NewValue=\"Active\" Field=\"StatusCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-832576752-961840538\" IdRef=\"LicensedProduct-1429906623-1687668745\" ActionCd=\"M\" NewValue=\"WC\" Field=\"LicenseClassCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1425179013-1964761645\" IdRef=\"LicensedProduct-1429906623-1687668745\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1156392930-1493697200\" IdRef=\"LicensedProduct-1429906623-1687668745\" ActionCd=\"M\" NewValue=\"CA\" Field=\"StateProvCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-422989501-857132348\" IdRef=\"LicensedProduct-1429906623-1687668745\" ActionCd=\"M\" NewValue=\"20150101\" Field=\"EffectiveDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-518598283-124629604\" IdRef=\"LicensedProduct-1429906623-1687668745\" ActionCd=\"M\" NewValue=\"10\" Field=\"CommissionNewPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-746932114-696193043\" IdRef=\"LicensedProduct-1429906623-1687668745\" ActionCd=\"M\" NewValue=\"10\" Field=\"CommissionRenewalPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-2125852353-346136033\" IdRef=\"LicensedProduct-1429906623-1687668745\" ActionCd=\"M\" NewValue=\"Group\" Field=\"SubmitToCd\" Status=\"Pending\" /></ChangeInfo><ChangeInfo id=\"ChangeInfo-479930207-867846604\" UserId=\"admin\" AddDt=\"20160414\" AddTm=\"22:05:21 CDT\" EffectiveDt=\"20160414\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-1719872137-856994399\" IdRef=\"LicensedProduct-2011298118-975696316\" ParentIdRef=\"LicensedProducts-507415788-357555566\" ActionCd=\"A\" Field=\"LicensedProduct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-868910466-841302484\" IdRef=\"LicensedProduct-2011298118-975696316\" ActionCd=\"M\" NewValue=\"Active\" Field=\"StatusCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-629161564-917388573\" IdRef=\"LicensedProduct-2011298118-975696316\" ActionCd=\"M\" NewValue=\"WC\" Field=\"LicenseClassCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1370446047-1226610911\" IdRef=\"LicensedProduct-2011298118-975696316\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1833577854-717988639\" IdRef=\"LicensedProduct-2011298118-975696316\" ActionCd=\"M\" NewValue=\"IL\" Field=\"StateProvCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-25784121-746509751\" IdRef=\"LicensedProduct-2011298118-975696316\" ActionCd=\"M\" NewValue=\"20150101\" Field=\"EffectiveDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-283323547-1732753091\" IdRef=\"LicensedProduct-2011298118-975696316\" ActionCd=\"M\" NewValue=\"10\" Field=\"CommissionNewPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-771620277-2012501712\" IdRef=\"LicensedProduct-2011298118-975696316\" ActionCd=\"M\" NewValue=\"10\" Field=\"CommissionRenewalPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-2130324289-617704435\" IdRef=\"LicensedProduct-2011298118-975696316\" ActionCd=\"M\" NewValue=\"Group\" Field=\"SubmitToCd\" Status=\"Pending\" /></ChangeInfo><ChangeInfo id=\"ChangeInfo-1319798645-1975218960\" UserId=\"admin\" AddDt=\"20160414\" AddTm=\"22:05:47 CDT\" EffectiveDt=\"20160414\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-407250800-1149310597\" IdRef=\"LicensedProduct-927220981-1529488943\" ParentIdRef=\"LicensedProducts-507415788-357555566\" ActionCd=\"A\" Field=\"LicensedProduct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-731713874-1481237016\" IdRef=\"LicensedProduct-927220981-1529488943\" ActionCd=\"M\" NewValue=\"Active\" Field=\"StatusCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1849345393-181052657\" IdRef=\"LicensedProduct-927220981-1529488943\" ActionCd=\"M\" NewValue=\"WC\" Field=\"LicenseClassCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1226711931-1582322018\" IdRef=\"LicensedProduct-927220981-1529488943\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1719207572-961268614\" IdRef=\"LicensedProduct-927220981-1529488943\" ActionCd=\"M\" NewValue=\"WI\" Field=\"StateProvCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-32907823-1576122418\" IdRef=\"LicensedProduct-927220981-1529488943\" ActionCd=\"M\" NewValue=\"20150101\" Field=\"EffectiveDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1930578110-350399137\" IdRef=\"LicensedProduct-927220981-1529488943\" ActionCd=\"M\" NewValue=\"10\" Field=\"CommissionNewPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-615843144-1960122820\" IdRef=\"LicensedProduct-927220981-1529488943\" ActionCd=\"M\" NewValue=\"10\" Field=\"CommissionRenewalPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-358873176-341180407\" IdRef=\"LicensedProduct-927220981-1529488943\" ActionCd=\"M\" NewValue=\"Group\" Field=\"SubmitToCd\" Status=\"Pending\" /></ChangeInfo><ChangeInfo id=\"ChangeInfo-1068248067-1436415242\" UserId=\"admin\" AddDt=\"20160414\" AddTm=\"22:06:20 CDT\" EffectiveDt=\"20160414\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-914054199-1079223183\" IdRef=\"LicensedProduct-1095313398-833905527\" ParentIdRef=\"LicensedProducts-507415788-357555566\" ActionCd=\"A\" Field=\"LicensedProduct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1754237531-479501001\" IdRef=\"LicensedProduct-1095313398-833905527\" ActionCd=\"M\" NewValue=\"Active\" Field=\"StatusCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-481685158-495814781\" IdRef=\"LicensedProduct-1095313398-833905527\" ActionCd=\"M\" NewValue=\"WC\" Field=\"LicenseClassCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-155456709-834573814\" IdRef=\"LicensedProduct-1095313398-833905527\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-47025593-1945252861\" IdRef=\"LicensedProduct-1095313398-833905527\" ActionCd=\"M\" NewValue=\"IN\" Field=\"StateProvCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1522131834-527289598\" IdRef=\"LicensedProduct-1095313398-833905527\" ActionCd=\"M\" NewValue=\"20150101\" Field=\"EffectiveDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1898130159-330072813\" IdRef=\"LicensedProduct-1095313398-833905527\" ActionCd=\"M\" NewValue=\"10\" Field=\"CommissionNewPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-102437329-2002790352\" IdRef=\"LicensedProduct-1095313398-833905527\" ActionCd=\"M\" NewValue=\"10\" Field=\"CommissionRenewalPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-536612737-1747996025\" IdRef=\"LicensedProduct-1095313398-833905527\" ActionCd=\"M\" NewValue=\"Group\" Field=\"SubmitToCd\" Status=\"Pending\" /></ChangeInfo><ChangeInfo id=\"ChangeInfo-356160389-1409831044\" UserId=\"admin\" AddDt=\"20160414\" AddTm=\"22:06:44 CDT\" EffectiveDt=\"20160414\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-36651796-1984978238\" IdRef=\"LicensedProduct-1431548644-1520257032\" ParentIdRef=\"LicensedProducts-507415788-357555566\" ActionCd=\"A\" Field=\"LicensedProduct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-902857737-1650589427\" IdRef=\"LicensedProduct-1431548644-1520257032\" ActionCd=\"M\" NewValue=\"Active\" Field=\"StatusCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-143369458-1904935630\" IdRef=\"LicensedProduct-1431548644-1520257032\" ActionCd=\"M\" NewValue=\"WC\" Field=\"LicenseClassCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1086988346-758241462\" IdRef=\"LicensedProduct-1431548644-1520257032\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-2026390998-1375511923\" IdRef=\"LicensedProduct-1431548644-1520257032\" ActionCd=\"M\" NewValue=\"HI\" Field=\"StateProvCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1076266118-1668350044\" IdRef=\"LicensedProduct-1431548644-1520257032\" ActionCd=\"M\" NewValue=\"20150101\" Field=\"EffectiveDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1504517905-1833893482\" IdRef=\"LicensedProduct-1431548644-1520257032\" ActionCd=\"M\" NewValue=\"10\" Field=\"CommissionNewPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-431506929-1914750330\" IdRef=\"LicensedProduct-1431548644-1520257032\" ActionCd=\"M\" NewValue=\"10\" Field=\"CommissionRenewalPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1128532408-1508053524\" IdRef=\"LicensedProduct-1431548644-1520257032\" ActionCd=\"M\" NewValue=\"Group\" Field=\"SubmitToCd\" Status=\"Pending\" /></ChangeInfo><StateInfo id=\"StateInfo-493610308-904372767\" CarrierGroup=\"IIC\" StateCd=\"TX\" AppointedDt=\"20150101\" Status=\"Active\" /><ChangeInfo id=\"ChangeInfo-836618060-1661412378\" UserId=\"admin\" AddDt=\"20160414\" AddTm=\"22:07:03 CDT\" EffectiveDt=\"20160414\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-234089243-1602581008\" IdRef=\"StateInfo-493610308-904372767\" ParentIdRef=\"Provider-493130578-2143305343\" ActionCd=\"A\" Field=\"StateInfo\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1077079698-1099824881\" IdRef=\"StateInfo-493610308-904372767\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1760155546-205638272\" IdRef=\"StateInfo-493610308-904372767\" ActionCd=\"M\" NewValue=\"TX\" Field=\"StateCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1532027876-1971647497\" IdRef=\"StateInfo-493610308-904372767\" ActionCd=\"M\" NewValue=\"20150101\" Field=\"AppointedDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1400350756-1373662464\" IdRef=\"StateInfo-493610308-904372767\" ActionCd=\"M\" NewValue=\"Active\" Field=\"Status\" Status=\"Pending\" /></ChangeInfo><ChangeInfo id=\"ChangeInfo-1681899-22029721\" UserId=\"admin\" AddDt=\"20170502\" AddTm=\"15:11:17 CDT\" EffectiveDt=\"20170502\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-26497153-28136576\" IdRef=\"LicensedProduct-29508869-7721407\" ParentIdRef=\"LicensedProducts-507415788-357555566\" ActionCd=\"A\" Field=\"LicensedProduct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-31336518-21986152\" IdRef=\"LicensedProduct-29508869-7721407\" ActionCd=\"M\" NewValue=\"Active\" Field=\"StatusCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-32913823-8021\" IdRef=\"LicensedProduct-29508869-7721407\" ActionCd=\"M\" NewValue=\"CPP\" Field=\"LicenseClassCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-11176779-2765453\" IdRef=\"LicensedProduct-29508869-7721407\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-5955030-2264447\" IdRef=\"LicensedProduct-29508869-7721407\" ActionCd=\"M\" NewValue=\"CA\" Field=\"StateProvCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-3318755-24991282\" IdRef=\"LicensedProduct-29508869-7721407\" ActionCd=\"M\" NewValue=\"19990101\" Field=\"EffectiveDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-32866470-29406226\" IdRef=\"LicensedProduct-29508869-7721407\" ActionCd=\"M\" NewValue=\"15\" Field=\"CommissionNewPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-15738226-28257048\" IdRef=\"LicensedProduct-29508869-7721407\" ActionCd=\"M\" NewValue=\"12\" Field=\"CommissionRenewalPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-11959803-15348251\" IdRef=\"LicensedProduct-29508869-7721407\" ActionCd=\"M\" NewValue=\"Group\" Field=\"SubmitToCd\" Status=\"Pending\" /></ChangeInfo><ChangeInfo id=\"ChangeInfo-29054651-30401019\" UserId=\"admin\" AddDt=\"20170503\" AddTm=\"11:10:30 CDT\" EffectiveDt=\"20170503\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-32931676-28740178\" IdRef=\"LicensedProduct-21016258-16878329\" ParentIdRef=\"LicensedProducts-507415788-357555566\" ActionCd=\"A\" Field=\"LicensedProduct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-11674906-2871762\" IdRef=\"LicensedProduct-21016258-16878329\" ActionCd=\"M\" NewValue=\"Active\" Field=\"StatusCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-25915445-23175004\" IdRef=\"LicensedProduct-21016258-16878329\" ActionCd=\"M\" NewValue=\"PP\" Field=\"LicenseClassCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-5958303-7143431\" IdRef=\"LicensedProduct-21016258-16878329\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-2870951-23936833\" IdRef=\"LicensedProduct-21016258-16878329\" ActionCd=\"M\" NewValue=\"TX\" Field=\"StateProvCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-12856737-2318854\" IdRef=\"LicensedProduct-21016258-16878329\" ActionCd=\"M\" NewValue=\"19990101\" Field=\"EffectiveDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-9290131-29748\" IdRef=\"LicensedProduct-21016258-16878329\" ActionCd=\"M\" NewValue=\"15\" Field=\"CommissionNewPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-22867504-2787151\" IdRef=\"LicensedProduct-21016258-16878329\" ActionCd=\"M\" NewValue=\"12\" Field=\"CommissionRenewalPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-7036672-24982125\" IdRef=\"LicensedProduct-21016258-16878329\" ActionCd=\"M\" NewValue=\"Group\" Field=\"SubmitToCd\" Status=\"Pending\" /></ChangeInfo><StateInfo id=\"StateInfo-1220323-7999424\" CarrierGroup=\"IIC\" StateCd=\"VA\" AppointedDt=\"19990101\" Status=\"Active\" /><ChangeInfo id=\"ChangeInfo-28283872-14403543\" UserId=\"admin\" AddDt=\"20170503\" AddTm=\"11:10:59 CDT\" EffectiveDt=\"20170503\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-21035372-32555350\" IdRef=\"StateInfo-1220323-7999424\" ParentIdRef=\"Provider-493130578-2143305343\" ActionCd=\"A\" Field=\"StateInfo\" Status=\"Pending\" /><ModInfo id=\"ModInfo-32905345-15167422\" IdRef=\"StateInfo-1220323-7999424\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-33472832-15531278\" IdRef=\"StateInfo-1220323-7999424\" ActionCd=\"M\" NewValue=\"VA\" Field=\"StateCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-25942466-15257618\" IdRef=\"StateInfo-1220323-7999424\" ActionCd=\"M\" NewValue=\"19990101\" Field=\"AppointedDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-23983694-3957280\" IdRef=\"StateInfo-1220323-7999424\" ActionCd=\"M\" NewValue=\"Active\" Field=\"Status\" Status=\"Pending\" /></ChangeInfo><ChangeInfo id=\"ChangeInfo-489399-17376023\" UserId=\"admin\" AddDt=\"20170503\" AddTm=\"11:11:15 CDT\" EffectiveDt=\"20170503\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-18471573-15480090\" IdRef=\"LicensedProduct-13988464-13469125\" ParentIdRef=\"LicensedProducts-507415788-357555566\" ActionCd=\"A\" Field=\"LicensedProduct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-30180276-7541864\" IdRef=\"LicensedProduct-13988464-13469125\" ActionCd=\"M\" NewValue=\"Active\" Field=\"StatusCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-19220662-17660591\" IdRef=\"LicensedProduct-13988464-13469125\" ActionCd=\"M\" NewValue=\"PP\" Field=\"LicenseClassCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-8622809-15009781\" IdRef=\"LicensedProduct-13988464-13469125\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-18782311-24794729\" IdRef=\"LicensedProduct-13988464-13469125\" ActionCd=\"M\" NewValue=\"VA\" Field=\"StateProvCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-33233605-20413984\" IdRef=\"LicensedProduct-13988464-13469125\" ActionCd=\"M\" NewValue=\"19990101\" Field=\"EffectiveDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-12259457-28958369\" IdRef=\"LicensedProduct-13988464-13469125\" ActionCd=\"M\" NewValue=\"15\" Field=\"CommissionNewPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-12697953-2585511\" IdRef=\"LicensedProduct-13988464-13469125\" ActionCd=\"M\" NewValue=\"12\" Field=\"CommissionRenewalPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-678586-25807884\" IdRef=\"LicensedProduct-13988464-13469125\" ActionCd=\"M\" NewValue=\"Group\" Field=\"SubmitToCd\" Status=\"Pending\" /></ChangeInfo><ChangeInfo id=\"ChangeInfo-14126214-9283856\" UserId=\"admin\" AddDt=\"20170508\" AddTm=\"14:15:31 CDT\" EffectiveDt=\"20170508\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-2013510-28305254\" IdRef=\"LicensedProduct-22926393-28884318\" ParentIdRef=\"LicensedProducts-507415788-357555566\" ActionCd=\"A\" Field=\"LicensedProduct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-12095935-9376315\" IdRef=\"LicensedProduct-22926393-28884318\" ActionCd=\"M\" NewValue=\"Active\" Field=\"StatusCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-823216-8111686\" IdRef=\"LicensedProduct-22926393-28884318\" ActionCd=\"M\" NewValue=\"BOP\" Field=\"LicenseClassCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-12360095-15224889\" IdRef=\"LicensedProduct-22926393-28884318\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-8075931-16026980\" IdRef=\"LicensedProduct-22926393-28884318\" ActionCd=\"M\" NewValue=\"CA\" Field=\"StateProvCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-9079812-93428\" IdRef=\"LicensedProduct-22926393-28884318\" ActionCd=\"M\" NewValue=\"19990101\" Field=\"EffectiveDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-20262029-25821218\" IdRef=\"LicensedProduct-22926393-28884318\" ActionCd=\"M\" NewValue=\"15\" Field=\"CommissionNewPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-29252859-12318580\" IdRef=\"LicensedProduct-22926393-28884318\" ActionCd=\"M\" NewValue=\"12\" Field=\"CommissionRenewalPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-11595626-28865230\" IdRef=\"LicensedProduct-22926393-28884318\" ActionCd=\"M\" NewValue=\"Group\" Field=\"SubmitToCd\" Status=\"Pending\" /></ChangeInfo><ChangeInfo id=\"ChangeInfo-17734320-29896167\" UserId=\"admin\" AddDt=\"20170508\" AddTm=\"14:35:29 CDT\" EffectiveDt=\"20170508\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-20334498-21351217\" IdRef=\"LicensedProduct-15839946-13004827\" ParentIdRef=\"LicensedProducts-507415788-357555566\" ActionCd=\"A\" Field=\"LicensedProduct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-5695979-18204391\" IdRef=\"LicensedProduct-15839946-13004827\" ActionCd=\"M\" NewValue=\"Active\" Field=\"StatusCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-11042606-32826268\" IdRef=\"LicensedProduct-15839946-13004827\" ActionCd=\"M\" NewValue=\"PUL\" Field=\"LicenseClassCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-11551096-27630711\" IdRef=\"LicensedProduct-15839946-13004827\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-6407586-8076145\" IdRef=\"LicensedProduct-15839946-13004827\" ActionCd=\"M\" NewValue=\"CA\" Field=\"StateProvCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-4852359-18838173\" IdRef=\"LicensedProduct-15839946-13004827\" ActionCd=\"M\" NewValue=\"19990101\" Field=\"EffectiveDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-33027017-14085301\" IdRef=\"LicensedProduct-15839946-13004827\" ActionCd=\"M\" NewValue=\"15\" Field=\"CommissionNewPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-4906138-16499028\" IdRef=\"LicensedProduct-15839946-13004827\" ActionCd=\"M\" NewValue=\"12\" Field=\"CommissionRenewalPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-4724921-27852673\" IdRef=\"LicensedProduct-15839946-13004827\" ActionCd=\"M\" NewValue=\"Group\" Field=\"SubmitToCd\" Status=\"Pending\" /></ChangeInfo><ChangeInfo id=\"ChangeInfo-4242552-9133775\" UserId=\"admin\" AddDt=\"20170508\" AddTm=\"14:35:48 CDT\" EffectiveDt=\"20170508\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-5689057-18729100\" IdRef=\"LicensedProduct-17182699-18627354\" ParentIdRef=\"LicensedProducts-507415788-357555566\" ActionCd=\"A\" Field=\"LicensedProduct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-31295372-26956785\" IdRef=\"LicensedProduct-17182699-18627354\" ActionCd=\"M\" NewValue=\"Active\" Field=\"StatusCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-9481758-7834668\" IdRef=\"LicensedProduct-17182699-18627354\" ActionCd=\"M\" NewValue=\"PP\" Field=\"LicenseClassCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-32480329-14441054\" IdRef=\"LicensedProduct-17182699-18627354\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-20052933-19603299\" IdRef=\"LicensedProduct-17182699-18627354\" ActionCd=\"M\" NewValue=\"CA\" Field=\"StateProvCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-9689371-8484029\" IdRef=\"LicensedProduct-17182699-18627354\" ActionCd=\"M\" NewValue=\"19990101\" Field=\"EffectiveDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1196667-23433309\" IdRef=\"LicensedProduct-17182699-18627354\" ActionCd=\"M\" NewValue=\"15\" Field=\"CommissionNewPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-32168939-9660663\" IdRef=\"LicensedProduct-17182699-18627354\" ActionCd=\"M\" NewValue=\"12\" Field=\"CommissionRenewalPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-2814852-23886628\" IdRef=\"LicensedProduct-17182699-18627354\" ActionCd=\"M\" NewValue=\"Group\" Field=\"SubmitToCd\" Status=\"Pending\" /></ChangeInfo><ChangeInfo id=\"ChangeInfo-11369743-8561905\" UserId=\"admin\" AddDt=\"20170508\" AddTm=\"14:36:05 CDT\" EffectiveDt=\"20170508\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-14339161-673647\" IdRef=\"LicensedProduct-18801868-2203588\" ParentIdRef=\"LicensedProducts-507415788-357555566\" ActionCd=\"A\" Field=\"LicensedProduct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-24745889-4195055\" IdRef=\"LicensedProduct-18801868-2203588\" ActionCd=\"M\" NewValue=\"Active\" Field=\"StatusCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-13423477-23636794\" IdRef=\"LicensedProduct-18801868-2203588\" ActionCd=\"M\" NewValue=\"PLML\" Field=\"LicenseClassCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-391209-11847868\" IdRef=\"LicensedProduct-18801868-2203588\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-9299300-12100404\" IdRef=\"LicensedProduct-18801868-2203588\" ActionCd=\"M\" NewValue=\"CA\" Field=\"StateProvCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-29088570-31594201\" IdRef=\"LicensedProduct-18801868-2203588\" ActionCd=\"M\" NewValue=\"19990101\" Field=\"EffectiveDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-4944900-21747386\" IdRef=\"LicensedProduct-18801868-2203588\" ActionCd=\"M\" NewValue=\"15\" Field=\"CommissionNewPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-5548834-18561583\" IdRef=\"LicensedProduct-18801868-2203588\" ActionCd=\"M\" NewValue=\"12\" Field=\"CommissionRenewalPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-11509020-27948597\" IdRef=\"LicensedProduct-18801868-2203588\" ActionCd=\"M\" NewValue=\"Group\" Field=\"SubmitToCd\" Status=\"Pending\" /></ChangeInfo><ChangeInfo id=\"ChangeInfo-22650179-25625581\" UserId=\"admin\" AddDt=\"20170508\" AddTm=\"14:36:27 CDT\" EffectiveDt=\"20170508\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-22589543-12552243\" IdRef=\"LicensedProduct-23864577-11621342\" ParentIdRef=\"LicensedProducts-507415788-357555566\" ActionCd=\"A\" Field=\"LicensedProduct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-2730275-33188980\" IdRef=\"LicensedProduct-23864577-11621342\" ActionCd=\"M\" NewValue=\"Active\" Field=\"StatusCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-8837412-9728712\" IdRef=\"LicensedProduct-23864577-11621342\" ActionCd=\"M\" NewValue=\"PA\" Field=\"LicenseClassCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-27240008-26672003\" IdRef=\"LicensedProduct-23864577-11621342\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-20141868-12214512\" IdRef=\"LicensedProduct-23864577-11621342\" ActionCd=\"M\" NewValue=\"CA\" Field=\"StateProvCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-11091209-8885756\" IdRef=\"LicensedProduct-23864577-11621342\" ActionCd=\"M\" NewValue=\"19990101\" Field=\"EffectiveDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-28190199-7496082\" IdRef=\"LicensedProduct-23864577-11621342\" ActionCd=\"M\" NewValue=\"15\" Field=\"CommissionNewPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-24527008-17432775\" IdRef=\"LicensedProduct-23864577-11621342\" ActionCd=\"M\" NewValue=\"12\" Field=\"CommissionRenewalPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-19991500-7767177\" IdRef=\"LicensedProduct-23864577-11621342\" ActionCd=\"M\" NewValue=\"Group\" Field=\"SubmitToCd\" Status=\"Pending\" /></ChangeInfo><ChangeInfo id=\"ChangeInfo-31348793-2391162\" UserId=\"admin\" AddDt=\"20170508\" AddTm=\"14:36:44 CDT\" EffectiveDt=\"20170508\" Status=\"Active\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-23960837-23133049\" IdRef=\"LicensedProduct-32614989-25442619\" ParentIdRef=\"LicensedProducts-507415788-357555566\" ActionCd=\"A\" Field=\"LicensedProduct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-12467195-13224099\" IdRef=\"LicensedProduct-32614989-25442619\" ActionCd=\"M\" NewValue=\"Active\" Field=\"StatusCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-33207006-19325669\" IdRef=\"LicensedProduct-32614989-25442619\" ActionCd=\"M\" NewValue=\"DP\" Field=\"LicenseClassCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-9071897-13903163\" IdRef=\"LicensedProduct-32614989-25442619\" ActionCd=\"M\" NewValue=\"IIC\" Field=\"CarrierGroup\" Status=\"Pending\" /><ModInfo id=\"ModInfo-2098621-4842390\" IdRef=\"LicensedProduct-32614989-25442619\" ActionCd=\"M\" NewValue=\"CA\" Field=\"StateProvCd\" Status=\"Pending\" /><ModInfo id=\"ModInfo-22721059-7781070\" IdRef=\"LicensedProduct-32614989-25442619\" ActionCd=\"M\" NewValue=\"19990101\" Field=\"EffectiveDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-9652368-20529479\" IdRef=\"LicensedProduct-32614989-25442619\" ActionCd=\"M\" NewValue=\"15\" Field=\"CommissionNewPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-23474217-14899265\" IdRef=\"LicensedProduct-32614989-25442619\" ActionCd=\"M\" NewValue=\"12\" Field=\"CommissionRenewalPct\" Status=\"Pending\" /><ModInfo id=\"ModInfo-14041541-27791713\" IdRef=\"LicensedProduct-32614989-25442619\" ActionCd=\"M\" NewValue=\"Group\" Field=\"SubmitToCd\" Status=\"Pending\" /></ChangeInfo></Provider>','<?xml version=\"1.0\" encoding=\"UTF-8\"?><ProviderMini SystemId=\"1\" id=\"Provider-493130578-2143305343\" Version=\"1.50\" IndexName=\"SPIAAGENCYINC\" ProviderTypeCd=\"Producer\" StatusCd=\"Active\" StatusDt=\"20150101\" ProviderNumber=\"123456\" PaymentPreferenceCd=\"Check\" PreferredDeliveryMethod=\"None\" ProviderOnCommissionPlan=\"No\" CombinePaymentInd=\"Yes\"><ProviderAcct id=\"ProviderAcct-1521436002-1758358864\"><PartyInfo id=\"PartyInfo-347934414-1415261897\" PartyTypeCd=\"AcctParty\"><EmailInfo id=\"EmailInfo-334957087-517428174\" EmailTypeCd=\"AcctEmail\" PreferredInd=\"No\" /><TaxInfo id=\"TaxInfo-461838953-896423960\" TaxTypeCd=\"AcctTaxInfo\" WithholdingExemptInd=\"Yes\" Received1099Ind=\"Yes\" ReceivedW9Ind=\"Yes\" Required1099Ind=\"Yes\" /><PhoneInfo id=\"PhoneInfo-352901420-2133890000\" PhoneTypeCd=\"AcctPhone\" PreferredInd=\"No\" /><NameInfo id=\"NameInfo-1962927477-1874899543\" NameTypeCd=\"AcctName\" CommercialName=\"SPIA Agency, Inc\" CommercialName2=\"SPIA Agency, Inc\" /><Addr id=\"Addr-600035118-1477867157\" AddrTypeCd=\"AcctMailingAddr\" Addr1=\"100 Great Oaks Blvd\" City=\"San Jose\" StateProvCd=\"CA\" PostalCode=\"95119-1462\" /><PhoneInfo id=\"PhoneInfo-1421662683-1892788641\" PhoneTypeCd=\"AcctFax\" PreferredInd=\"No\" /><Addr id=\"Addr-1784024653-397884935\" AddrTypeCd=\"AcctMailingLookupAddr\" /><Addr id=\"Addr-707803488-1098465389\" AddrTypeCd=\"AcctStreetAddr\" /></PartyInfo></ProviderAcct><PartyInfo id=\"PartyInfo-43104147-1874978217\" PartyTypeCd=\"ProviderParty\"><EmailInfo id=\"EmailInfo-661253580-1206233125\" EmailTypeCd=\"ProviderEmail\" EmailAddr=\"BitBucket@iscs.com\" PreferredInd=\"No\" /><TaxInfo id=\"TaxInfo-1378383929-1752394822\" TaxTypeCd=\"ProviderTaxInfo\" LegalEntityCd=\"LLC\" /><PhoneInfo id=\"PhoneInfo-556637132-1455564082\" PhoneTypeCd=\"ProviderPrimaryPhone\" PreferredInd=\"No\" /><NameInfo id=\"NameInfo-1451627776-684456553\" NameTypeCd=\"ProviderName\" GivenName=\"SPIA Agency, Inc\" CommercialName=\"SPIA Agency, Inc\" /><Addr id=\"Addr-220376900-1161803109\" AddrTypeCd=\"ProviderMailingAddr\" /><PhoneInfo id=\"PhoneInfo-1051567531-1415808731\" PhoneTypeCd=\"ProviderSecondaryPhone\" PreferredInd=\"No\" /><PhoneInfo id=\"PhoneInfo-425559136-1592904140\" PhoneTypeCd=\"ProviderFax\" PreferredInd=\"No\" /><Addr id=\"Addr-808736550-1624987461\" AddrTypeCd=\"ProviderBillingAddr\" Addr1=\"100 Great Oaks Blvd\" City=\"San Jose\" StateProvCd=\"CA\" PostalCode=\"95119-1462\" /><Addr id=\"Addr-2015075477-1085774446\" AddrTypeCd=\"ProviderStreetAddr\" Addr1=\"100 Great Oaks Blvd\" City=\"San Jose\" StateProvCd=\"CA\" PostalCode=\"95119-1462\" /></PartyInfo><ProducerInfo id=\"ProducerInfo-1210015761-563985638\" ProducerTypeCd=\"Producer\" AppointedDt=\"20150101\" SubmitToCd=\"Group\" PayToCd=\"Producer\" DirectPortalInd=\"Yes\" /><CommonItemCounts /></ProviderMini>');
/*!40000 ALTER TABLE `provider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `providerlookup`
--

DROP TABLE IF EXISTS `providerlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `providerlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `providerlookup`
--

LOCK TABLES `providerlookup` WRITE;
/*!40000 ALTER TABLE `providerlookup` DISABLE KEYS */;
INSERT INTO `providerlookup` VALUES (1,'AcctTaxId','',NULL),(1,'BusinessName','SPIAAGENCYINC',NULL),(1,'BusinessType','LLC',NULL),(1,'CarrierGroupLicenseClassState','IIC|BOP|CA',NULL),(1,'CarrierGroupLicenseClassState','IIC|CPP|CA',NULL),(1,'CarrierGroupLicenseClassState','IIC|DP|CA',NULL),(1,'CarrierGroupLicenseClassState','IIC|PA|CA',NULL),(1,'CarrierGroupLicenseClassState','IIC|PLML|CA',NULL),(1,'CarrierGroupLicenseClassState','IIC|PP|CA',NULL),(1,'CarrierGroupLicenseClassState','IIC|PP|TX',NULL),(1,'CarrierGroupLicenseClassState','IIC|PP|VA',NULL),(1,'CarrierGroupLicenseClassState','IIC|PUL|CA',NULL),(1,'CarrierGroupLicenseClassState','IIC|WC|CA',NULL),(1,'CarrierGroupLicenseClassState','IIC|WC|HI',NULL),(1,'CarrierGroupLicenseClassState','IIC|WC|IL',NULL),(1,'CarrierGroupLicenseClassState','IIC|WC|IN',NULL),(1,'CarrierGroupLicenseClassState','IIC|WC|WI',NULL),(1,'City','SANJOSE',NULL),(1,'DBAIndexName','',NULL),(1,'DirectPortalInd','YES',NULL),(1,'export','TRUE',NULL),(1,'IndexName','SPIAAGENCYINC',NULL),(1,'PersonalName','SPIAAGENCYINC',NULL),(1,'PhoneNumber','',NULL),(1,'PostalCode','951191462',NULL),(1,'ProducerTypeCd','PRODUCER',NULL),(1,'ProviderNumber','123456',NULL),(1,'ProviderType','PRODUCER',NULL),(1,'StateProvCd','CA',NULL),(1,'StatusCd','ACTIVE',NULL),(1,'StatusProviderType','ACTIVEPRODUCER',NULL),(1,'StreetAddr','100GREATOAKSBLVDSANJOSECA951191462',NULL),(1,'TaxID','',NULL);
/*!40000 ALTER TABLE `providerlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `providerstatement`
--

DROP TABLE IF EXISTS `providerstatement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `providerstatement` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `providerstatement`
--

LOCK TABLES `providerstatement` WRITE;
/*!40000 ALTER TABLE `providerstatement` DISABLE KEYS */;
/*!40000 ALTER TABLE `providerstatement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `providerstatementlookup`
--

DROP TABLE IF EXISTS `providerstatementlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `providerstatementlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `providerstatementlookup`
--

LOCK TABLES `providerstatementlookup` WRITE;
/*!40000 ALTER TABLE `providerstatementlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `providerstatementlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `renewalofferstats`
--

DROP TABLE IF EXISTS `renewalofferstats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `renewalofferstats` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `StatSequence` int(11) DEFAULT NULL,
  `StatSequenceReplace` int(11) DEFAULT NULL,
  `PolicyRef` int(11) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `LineCd` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `CoverageCd` varchar(255) DEFAULT NULL,
  `RiskCd` varchar(255) DEFAULT NULL,
  `RiskTypeCd` varchar(255) DEFAULT NULL,
  `SubTypeCd` varchar(255) DEFAULT NULL,
  `CoverageItemCd` varchar(255) DEFAULT NULL,
  `FeeCd` varchar(255) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `CombinedKey` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `TransactionEffectiveDt` date DEFAULT NULL,
  `AccountingDt` date DEFAULT NULL,
  `WrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WrittenCommissionAmt` decimal(28,6) DEFAULT NULL,
  `WrittenPremiumFeeAmt` decimal(28,6) DEFAULT NULL,
  `WrittenCommissionFeeAmt` decimal(28,6) DEFAULT NULL,
  `EarnDays` int(11) DEFAULT NULL,
  `InforceChangeAmt` decimal(28,6) DEFAULT NULL,
  `PolicyYear` varchar(255) DEFAULT NULL,
  `TermDays` int(11) DEFAULT NULL,
  `InsuranceTypeCd` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `NewRenewalCd` varchar(255) DEFAULT NULL,
  `Limit1` varchar(255) DEFAULT NULL,
  `Limit2` varchar(255) DEFAULT NULL,
  `Deductible1` varchar(255) DEFAULT NULL,
  `Deductible2` varchar(255) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `CoinsurancePct` decimal(28,6) DEFAULT NULL,
  `CoveredPerilsCd` varchar(255) DEFAULT NULL,
  `CommissionKey` varchar(255) DEFAULT NULL,
  `CommissionAreaCd` varchar(255) DEFAULT NULL,
  `RateAreaName` varchar(255) DEFAULT NULL,
  `StatData` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `BusinessSourceCd` varchar(255) DEFAULT NULL,
  `ProducerCd` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `PolicyStatusCd` varchar(255) DEFAULT NULL,
  `ProductName` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  `PolicyGroupCd` varchar(255) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `ShortRateStatInd` varchar(255) DEFAULT NULL,
  `PayPlanCd` varchar(255) DEFAULT NULL,
  `ControllingProductVersionIdRef` varchar(255) DEFAULT NULL,
  `ControllingStateCd` varchar(255) DEFAULT NULL,
  `DividendPlanCd` varchar(255) DEFAULT NULL,
  `BillMethod` varchar(255) DEFAULT NULL,
  `StatementAccountNumber` varchar(255) DEFAULT NULL,
  `StatementAccountRef` int(11) DEFAULT NULL,
  `AdmiraltyFELALiabilityLimits` varchar(255) DEFAULT NULL,
  `AdmiraltyFELAMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AlcoholDrugFreeCreditInd` varchar(255) DEFAULT NULL,
  `AlcDrugFreeCreditAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheRate` decimal(28,6) DEFAULT NULL,
  `ContrClassPremAdjCreditFactor` varchar(255) DEFAULT NULL,
  `ContrClassPremAdjCreditAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleDiscountRatePct` decimal(28,6) DEFAULT NULL,
  `DiseaseAbrasiveAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAbrasivePaDeAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAsbestosAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAtomicEnergyAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseCoalmineAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseExperienceAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundryIronAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundryNoFerrAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundrySteelAmt` decimal(28,6) DEFAULT NULL,
  `ELIncreasedLimitPct` decimal(28,6) DEFAULT NULL,
  `ELIncreaseLimitMinPremAmt` decimal(28,6) DEFAULT NULL,
  `ELIncreaseLimitMinPremChrgAmt` decimal(28,6) DEFAULT NULL,
  `EstimatedAnnualPremiumAmt` decimal(28,6) DEFAULT NULL,
  `ExpenseConstantAmt` decimal(28,6) DEFAULT NULL,
  `ExperienceMeritRating` varchar(255) DEFAULT NULL,
  `ExperienceModificationFactor` varchar(255) DEFAULT NULL,
  `ManagedCareCreditInd` varchar(255) DEFAULT NULL,
  `ManagedCareCreditAmt` decimal(28,6) DEFAULT NULL,
  `NAICS` varchar(255) DEFAULT NULL,
  `NCCIRiskIdNumber` varchar(255) DEFAULT NULL,
  `NumberOfClaims` varchar(255) DEFAULT NULL,
  `PremiumDiscountAmt` decimal(28,6) DEFAULT NULL,
  `RatingBureauID` varchar(255) DEFAULT NULL,
  `CertifiedSafetyHealthProgPct` decimal(28,6) DEFAULT NULL,
  `SchedRateClassPeculiarities` varchar(255) DEFAULT NULL,
  `SchedRateEmployee` varchar(255) DEFAULT NULL,
  `SchedRateManagement` varchar(255) DEFAULT NULL,
  `SchedRateMedicalFacilities` varchar(255) DEFAULT NULL,
  `SchedRateMgmtSafetyOrg` varchar(255) DEFAULT NULL,
  `SchedRatePremises` varchar(255) DEFAULT NULL,
  `SchedRateSafetyDevices` varchar(255) DEFAULT NULL,
  `SIC` varchar(255) DEFAULT NULL,
  `StandardPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjust` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjustSign` varchar(255) DEFAULT NULL,
  `TerrorismAmt` decimal(28,6) DEFAULT NULL,
  `TerrorismRate` decimal(28,6) DEFAULT NULL,
  `TotalManualPremimAmt` decimal(28,6) DEFAULT NULL,
  `TotalSubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationInd` varchar(255) DEFAULT NULL,
  `WaiverSubrogationRate` decimal(28,6) DEFAULT NULL,
  `YearsInBusiness` varchar(255) DEFAULT NULL,
  `DrugFreeWorkplace` varchar(255) DEFAULT NULL,
  `StatSourceIdRef` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`),
  KEY `RenewalOfferStats_1` (`PolicyNumber`),
  KEY `RenewalOfferStats_2` (`CombinedKey`),
  KEY `RenewalOfferStats_3` (`BookDt`),
  KEY `RenewalOfferStats_4` (`AccountingDt`),
  KEY `RenewalOfferStats_5` (`StatusCd`),
  KEY `RenewalOfferStats_6` (`CommissionKey`),
  KEY `RenewalOfferStats_Idx_1` (`CombinedKey`,`StatusCd`),
  KEY `RenewalOfferStats_Idx_2` (`CombinedKey`,`AccountingDt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `renewalofferstats`
--

LOCK TABLES `renewalofferstats` WRITE;
/*!40000 ALTER TABLE `renewalofferstats` DISABLE KEYS */;
/*!40000 ALTER TABLE `renewalofferstats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `replicationmonitor`
--

DROP TABLE IF EXISTS `replicationmonitor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `replicationmonitor` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `replicationmonitor`
--

LOCK TABLES `replicationmonitor` WRITE;
/*!40000 ALTER TABLE `replicationmonitor` DISABLE KEYS */;
/*!40000 ALTER TABLE `replicationmonitor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `replicationmonitorlookup`
--

DROP TABLE IF EXISTS `replicationmonitorlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `replicationmonitorlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `replicationmonitorlookup`
--

LOCK TABLES `replicationmonitorlookup` WRITE;
/*!40000 ALTER TABLE `replicationmonitorlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `replicationmonitorlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reportwizard`
--

DROP TABLE IF EXISTS `reportwizard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reportwizard` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reportwizard`
--

LOCK TABLES `reportwizard` WRITE;
/*!40000 ALTER TABLE `reportwizard` DISABLE KEYS */;
/*!40000 ALTER TABLE `reportwizard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reportwizardlookup`
--

DROP TABLE IF EXISTS `reportwizardlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reportwizardlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reportwizardlookup`
--

LOCK TABLES `reportwizardlookup` WRITE;
/*!40000 ALTER TABLE `reportwizardlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `reportwizardlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statementaccount`
--

DROP TABLE IF EXISTS `statementaccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statementaccount` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statementaccount`
--

LOCK TABLES `statementaccount` WRITE;
/*!40000 ALTER TABLE `statementaccount` DISABLE KEYS */;
/*!40000 ALTER TABLE `statementaccount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statementaccountlookup`
--

DROP TABLE IF EXISTS `statementaccountlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statementaccountlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statementaccountlookup`
--

LOCK TABLES `statementaccountlookup` WRITE;
/*!40000 ALTER TABLE `statementaccountlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `statementaccountlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `storagebin`
--

DROP TABLE IF EXISTS `storagebin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storagebin` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `storagebin`
--

LOCK TABLES `storagebin` WRITE;
/*!40000 ALTER TABLE `storagebin` DISABLE KEYS */;
/*!40000 ALTER TABLE `storagebin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `storagebinlookup`
--

DROP TABLE IF EXISTS `storagebinlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storagebinlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `storagebinlookup`
--

LOCK TABLES `storagebinlookup` WRITE;
/*!40000 ALTER TABLE `storagebinlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `storagebinlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `storagefile`
--

DROP TABLE IF EXISTS `storagefile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storagefile` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `storagefile`
--

LOCK TABLES `storagefile` WRITE;
/*!40000 ALTER TABLE `storagefile` DISABLE KEYS */;
/*!40000 ALTER TABLE `storagefile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `storagefilelookup`
--

DROP TABLE IF EXISTS `storagefilelookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storagefilelookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `storagefilelookup`
--

LOCK TABLES `storagefilelookup` WRITE;
/*!40000 ALTER TABLE `storagefilelookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `storagefilelookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriptions` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscriptions`
--

LOCK TABLES `subscriptions` WRITE;
/*!40000 ALTER TABLE `subscriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscriptionslookup`
--

DROP TABLE IF EXISTS `subscriptionslookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriptionslookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscriptionslookup`
--

LOCK TABLES `subscriptionslookup` WRITE;
/*!40000 ALTER TABLE `subscriptionslookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscriptionslookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suspense`
--

DROP TABLE IF EXISTS `suspense`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suspense` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suspense`
--

LOCK TABLES `suspense` WRITE;
/*!40000 ALTER TABLE `suspense` DISABLE KEYS */;
/*!40000 ALTER TABLE `suspense` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suspenselookup`
--

DROP TABLE IF EXISTS `suspenselookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suspenselookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suspenselookup`
--

LOCK TABLES `suspenselookup` WRITE;
/*!40000 ALTER TABLE `suspenselookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `suspenselookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suspensestats`
--

DROP TABLE IF EXISTS `suspensestats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suspensestats` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `StatSequence` int(11) DEFAULT NULL,
  `StatSequenceReplace` int(11) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `CombinedKey` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  `TransactionTypeCd` varchar(255) DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `TransactionNumber` varchar(255) DEFAULT NULL,
  `CheckNumber` varchar(255) DEFAULT NULL,
  `CheckAmt` decimal(28,6) DEFAULT NULL,
  `CheckDt` varchar(255) DEFAULT NULL,
  `BatchId` varchar(255) DEFAULT NULL,
  `ActivityTypeCd` varchar(255) DEFAULT NULL,
  `ReceiptAmt` decimal(28,6) DEFAULT NULL,
  `PendingAmt` decimal(28,6) DEFAULT NULL,
  `DepositoryLocationCd` varchar(255) DEFAULT NULL,
  `SystemCheckNumber` varchar(255) DEFAULT NULL,
  `RecreateSuspenseInd` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`),
  KEY `SuspenseStats_1` (`PolicyNumber`),
  KEY `SuspenseStats_2` (`CombinedKey`),
  KEY `SuspenseStats_3` (`BookDt`),
  KEY `SuspenseStats_4` (`StatusCd`),
  KEY `SuspenseStats_5` (`AccountNumber`),
  KEY `SuspenseStats_Idx_1` (`CombinedKey`,`StatusCd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suspensestats`
--

LOCK TABLES `suspensestats` WRITE;
/*!40000 ALTER TABLE `suspensestats` DISABLE KEYS */;
/*!40000 ALTER TABLE `suspensestats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemdata`
--

DROP TABLE IF EXISTS `systemdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemdata` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemdata`
--

LOCK TABLES `systemdata` WRITE;
/*!40000 ALTER TABLE `systemdata` DISABLE KEYS */;
INSERT INTO `systemdata` VALUES (1,4,'admin','2017-05-08 10:05:56','<?xml version=\"1.0\" encoding=\"UTF-8\"?><SystemData SystemId=\"1\" id=\"SystemData-1832709101-1295338045\" UpdateCount=\"4\" UpdateUser=\"admin\" UpdateTimestamp=\"05/08/2017 10:05:56.247 AM\" Name=\"BookDt\" Value=\"20160104\" />',NULL);
/*!40000 ALTER TABLE `systemdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemdatalookup`
--

DROP TABLE IF EXISTS `systemdatalookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemdatalookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemdatalookup`
--

LOCK TABLES `systemdatalookup` WRITE;
/*!40000 ALTER TABLE `systemdatalookup` DISABLE KEYS */;
INSERT INTO `systemdatalookup` VALUES (1,'export','FALSE',NULL),(1,'Name','BOOKDT',NULL);
/*!40000 ALTER TABLE `systemdatalookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task`
--

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasklookup`
--

DROP TABLE IF EXISTS `tasklookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasklookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasklookup`
--

LOCK TABLES `tasklookup` WRITE;
/*!40000 ALTER TABLE `tasklookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `tasklookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testperson`
--

DROP TABLE IF EXISTS `testperson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testperson` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testperson`
--

LOCK TABLES `testperson` WRITE;
/*!40000 ALTER TABLE `testperson` DISABLE KEYS */;
/*!40000 ALTER TABLE `testperson` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testpersonlookup`
--

DROP TABLE IF EXISTS `testpersonlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testpersonlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testpersonlookup`
--

LOCK TABLES `testpersonlookup` WRITE;
/*!40000 ALTER TABLE `testpersonlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `testpersonlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thing`
--

DROP TABLE IF EXISTS `thing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thing` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thing`
--

LOCK TABLES `thing` WRITE;
/*!40000 ALTER TABLE `thing` DISABLE KEYS */;
/*!40000 ALTER TABLE `thing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thinglookup`
--

DROP TABLE IF EXISTS `thinglookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thinglookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thinglookup`
--

LOCK TABLES `thinglookup` WRITE;
/*!40000 ALTER TABLE `thinglookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `thinglookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userinfo`
--

DROP TABLE IF EXISTS `userinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userinfo`
--

LOCK TABLES `userinfo` WRITE;
/*!40000 ALTER TABLE `userinfo` DISABLE KEYS */;
INSERT INTO `userinfo` VALUES (1,41,'Anonymous','2017-05-16 16:03:57','<?xml version=\"1.0\" encoding=\"UTF-8\"?><UserInfo SystemId=\"1\" id=\"UserInfo-88832401-1199615808\" UpdateCount=\"41\" UpdateUser=\"Anonymous\" UpdateTimestamp=\"05/16/2017 04:03:57.471 PM\" LoginId=\"admin\" TypeCd=\"sysadmin\" SuperUserInd=\"true\" Password=\"~fa246d262c3925617b0c72bb2eeb1d\" LastName=\"Administrator\" FirstName=\"System\" ConcurrentSessions=\"1\" StatusCd=\"Active\" PasswordMustChangeInd=\"No\" DefaultLanguageCd=\"en_US\" LastLogonDt=\"20170516\" LastLogonTm=\"16:03:57.471 -0500\" Version=\"1.10\"><UserRole id=\"N-10670473-1095183794612\" StartDt=\"19010101\" EndDt=\"99991231\" AuthorityRoleIdRef=\"Everything\" /><BeanStats id=\"BeanStats-16051016-1534221\" BeanUpdateUserIdRef=\"admin\" BeanUpdateDt=\"20170503\" BeanUpdateTm=\"11:14:39 CDT\" /><UserTaskControl id=\"UserTaskControl-11676701-24010693\" /><PartyInfo id=\"PartyInfo-1575238933-14046915\" PartyTypeCd=\"UserInfoParty\"><PhoneInfo id=\"PhoneInfo-1578053817-481787543\" PhoneTypeCd=\"UserInfoPhoneOne\" PreferredInd=\"No\" /><Addr id=\"Addr-232796359-1907978889\" AddrTypeCd=\"UserInfoWorkAddr\" /><PhoneInfo id=\"PhoneInfo-1615328385-1377162756\" PhoneTypeCd=\"UserInfoPhoneTwo\" PreferredInd=\"No\" /><PhoneInfo id=\"PhoneInfo-1966648839-1381774833\" PhoneTypeCd=\"UserInfoFax\" PreferredInd=\"No\" /><Addr id=\"Addr-1103209007-1912726619\" AddrTypeCd=\"UserInfoHomeAddr\" /></PartyInfo><ChangeInfo id=\"ChangeInfo-678019266-1130433310\" AddDt=\"20160414\" AddTm=\"21:57:13 CDT\" Status=\"Completed\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-872342726-688409980\" IdRef=\"UserInfo-88832401-1199615808\" ActionCd=\"M\" PreviousValue=\"~d41d8cd98f0b24e980998ecf8427e\" NewValue=\"~fa246d262c3925617b0c72bb2eeb1d\" Field=\"Password\" Status=\"Pending\" /><ModInfo id=\"ModInfo-559441057-1819660638\" IdRef=\"UserInfo-88832401-1199615808\" ActionCd=\"M\" NewValue=\"No\" Field=\"PasswordMustChangeInd\" Status=\"Pending\" /></ChangeInfo><UserRole id=\"UserRole-5362379-11697179\" StartDt=\"19010101\" EndDt=\"99991231\" AuthorityRoleIdRef=\"RatingServiceDeveloper\" /><ChangeInfo id=\"ChangeInfo-17353133-15652441\" UserId=\"admin\" AddDt=\"20170503\" AddTm=\"11:14:33 CDT\" Status=\"Completed\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-8799898-32489412\" IdRef=\"UserRole-5362379-11697179\" ParentIdRef=\"UserInfo-88832401-1199615808\" ActionCd=\"A\" Field=\"UserRole\" Status=\"Pending\" /><ModInfo id=\"ModInfo-4915614-27104830\" IdRef=\"UserRole-5362379-11697179\" ActionCd=\"M\" NewValue=\"19010101\" Field=\"StartDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-18038631-29906584\" IdRef=\"UserRole-5362379-11697179\" ActionCd=\"M\" NewValue=\"99991231\" Field=\"EndDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-28174862-13414115\" IdRef=\"UserRole-5362379-11697179\" ActionCd=\"M\" NewValue=\"RatingServiceDeveloper\" Field=\"AuthorityRoleIdRef\" Status=\"Pending\" /></ChangeInfo></UserInfo>',NULL),(2,2,'unknown','2016-04-14 21:57:13','<?xml version=\"1.0\" encoding=\"UTF-8\"?><UserInfo SystemId=\"2\" id=\"UserInfo-1923041767-1567907954\" UpdateCount=\"2\" UpdateUser=\"unknown\" UpdateTimestamp=\"04/14/2016 09:57:13.672 PM\" LoginId=\"webportal\" TypeCd=\"Guest\" SuperUserInd=\"true\" Password=\"~fa246d262c3925617b0c72bb2eeb1d\" Supervisor=\"admin\" LastName=\"Portal\" FirstName=\"Web\" ConcurrentSessions=\"0\" StatusCd=\"Active\" PasswordMustChangeInd=\"No\" DefaultLanguageCd=\"en_US\" Version=\"1.10\"><UserRole id=\"UserRole-616987549-491038049\" StartDt=\"19010101\" EndDt=\"99991231\" AuthorityRoleIdRef=\"ServicePortal\" /><UserTaskControl id=\"UserTaskControl-1051816604-1404803556\" /><PartyInfo id=\"PartyInfo-362547039-1796571758\" PartyTypeCd=\"UserInfoParty\"><PhoneInfo id=\"PhoneInfo-2090341890-1154472797\" PhoneTypeCd=\"UserInfoPhoneOne\" PreferredInd=\"No\" /><Addr id=\"Addr-637279473-947565493\" AddrTypeCd=\"UserInfoWorkAddr\" /><PhoneInfo id=\"PhoneInfo-832342628-1994221771\" PhoneTypeCd=\"UserInfoPhoneTwo\" PreferredInd=\"No\" /><PhoneInfo id=\"PhoneInfo-1246320660-1885763379\" PhoneTypeCd=\"UserInfoFax\" PreferredInd=\"No\" /><Addr id=\"Addr-955499862-2070934164\" AddrTypeCd=\"UserInfoHomeAddr\" /></PartyInfo><ChangeInfo id=\"ChangeInfo-1749773688-333611688\" AddDt=\"20160414\" AddTm=\"21:57:13 CDT\" Status=\"Completed\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-682148366-1955723369\" IdRef=\"UserInfo-1923041767-1567907954\" ActionCd=\"M\" PreviousValue=\"~d41d8cd98f0b24e980998ecf8427e\" NewValue=\"~fa246d262c3925617b0c72bb2eeb1d\" Field=\"Password\" Status=\"Pending\" /><ModInfo id=\"ModInfo-2065651742-110978839\" IdRef=\"UserInfo-1923041767-1567907954\" ActionCd=\"M\" NewValue=\"No\" Field=\"PasswordMustChangeInd\" Status=\"Pending\" /></ChangeInfo></UserInfo>',NULL),(3,2,'unknown','2016-04-14 21:57:13','<?xml version=\"1.0\" encoding=\"UTF-8\"?><UserInfo SystemId=\"3\" id=\"UserInfo-1652217815-241276412\" UpdateCount=\"2\" UpdateUser=\"unknown\" UpdateTimestamp=\"04/14/2016 09:57:13.774 PM\" LoginId=\"directportal\" TypeCd=\"Guest\" SuperUserInd=\"true\" Password=\"~fa246d262c3925617b0c72bb2eeb1d\" Supervisor=\"admin\" LastName=\"Portal\" FirstName=\"Direct\" ConcurrentSessions=\"0\" StatusCd=\"Active\" PasswordMustChangeInd=\"No\" DefaultLanguageCd=\"en_US\" Version=\"1.10\"><UserRole id=\"UserRole-1496027150-388079322\" StartDt=\"19010101\" EndDt=\"99991231\" AuthorityRoleIdRef=\"DirectSalesPortal\" /><UserTaskControl id=\"UserTaskControl-226624955-1261000399\" /><PartyInfo id=\"PartyInfo-600909702-563972213\" PartyTypeCd=\"UserInfoParty\"><PhoneInfo id=\"PhoneInfo-607373224-1065310906\" PhoneTypeCd=\"UserInfoPhoneOne\" PreferredInd=\"No\" /><Addr id=\"Addr-2007392450-1976985467\" AddrTypeCd=\"UserInfoWorkAddr\" /><PhoneInfo id=\"PhoneInfo-364427204-1487624998\" PhoneTypeCd=\"UserInfoPhoneTwo\" PreferredInd=\"No\" /><PhoneInfo id=\"PhoneInfo-1646254364-670098799\" PhoneTypeCd=\"UserInfoFax\" PreferredInd=\"No\" /><Addr id=\"Addr-1763547108-1239862325\" AddrTypeCd=\"UserInfoHomeAddr\" /></PartyInfo><ChangeInfo id=\"ChangeInfo-1955937948-563532714\" AddDt=\"20160414\" AddTm=\"21:57:13 CDT\" Status=\"Completed\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-1532192002-3849617\" IdRef=\"UserInfo-1652217815-241276412\" ActionCd=\"M\" PreviousValue=\"~d41d8cd98f0b24e980998ecf8427e\" NewValue=\"~fa246d262c3925617b0c72bb2eeb1d\" Field=\"Password\" Status=\"Pending\" /><ModInfo id=\"ModInfo-927662272-883832446\" IdRef=\"UserInfo-1652217815-241276412\" ActionCd=\"M\" NewValue=\"No\" Field=\"PasswordMustChangeInd\" Status=\"Pending\" /></ChangeInfo></UserInfo>',NULL),(4,2,'unknown','2016-04-14 21:57:13','<?xml version=\"1.0\" encoding=\"UTF-8\"?><UserInfo SystemId=\"4\" id=\"UserInfo-305650253-1480430300\" UpdateCount=\"2\" UpdateUser=\"unknown\" UpdateTimestamp=\"04/14/2016 09:57:13.866 PM\" LoginId=\"email-listener\" TypeCd=\"sysadmin\" SuperUserInd=\"true\" Password=\"~fa246d262c3925617b0c72bb2eeb1d\" Supervisor=\"admin\" LastName=\"Listener\" FirstName=\"Email\" ConcurrentSessions=\"0\" StatusCd=\"Active\" PasswordMustChangeInd=\"No\" DefaultLanguageCd=\"en_US\" Version=\"1.10\"><UserRole id=\"UserRole-80154998-723204301\" StartDt=\"19010101\" EndDt=\"99991231\" AuthorityRoleIdRef=\"EmailListener\" /><UserTaskControl id=\"UserTaskControl-283579070-1192149690\" /><PartyInfo id=\"PartyInfo-122845777-869114645\" PartyTypeCd=\"UserInfoParty\"><PhoneInfo id=\"PhoneInfo-264063163-2098382033\" PhoneTypeCd=\"UserInfoPhoneOne\" PreferredInd=\"No\" /><Addr id=\"Addr-1700499515-970837108\" AddrTypeCd=\"UserInfoWorkAddr\" /><PhoneInfo id=\"PhoneInfo-1762048464-932018926\" PhoneTypeCd=\"UserInfoPhoneTwo\" PreferredInd=\"No\" /><PhoneInfo id=\"PhoneInfo-1385169972-961509115\" PhoneTypeCd=\"UserInfoFax\" PreferredInd=\"No\" /><Addr id=\"Addr-1807666911-696555005\" AddrTypeCd=\"UserInfoHomeAddr\" /></PartyInfo><ChangeInfo id=\"ChangeInfo-1049648600-255009794\" AddDt=\"20160414\" AddTm=\"21:57:13 CDT\" Status=\"Completed\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-1105593860-997280868\" IdRef=\"UserInfo-305650253-1480430300\" ActionCd=\"M\" PreviousValue=\"~d41d8cd98f0b24e980998ecf8427e\" NewValue=\"~fa246d262c3925617b0c72bb2eeb1d\" Field=\"Password\" Status=\"Pending\" /><ModInfo id=\"ModInfo-1684779479-1337439244\" IdRef=\"UserInfo-305650253-1480430300\" ActionCd=\"M\" NewValue=\"No\" Field=\"PasswordMustChangeInd\" Status=\"Pending\" /></ChangeInfo></UserInfo>',NULL),(5,5,'admin','2017-05-16 16:00:34','<?xml version=\"1.0\" encoding=\"UTF-8\"?><UserInfo SystemId=\"5\" id=\"UserInfo-21359527-3592898\" UpdateCount=\"5\" UpdateUser=\"admin\" UpdateTimestamp=\"05/16/2017 04:00:34.474 PM\" LoginId=\"file-listener\" TypeCd=\"sysadmin\" SuperUserInd=\"false\" Password=\"~fa246d262c3925617b0c72bb2eeb1d\" Supervisor=\"admin\" LastName=\"Listener\" FirstName=\"File\" StartWork=\"TaskList\" ConcurrentSessions=\"0\" StatusCd=\"Active\" PasswordMustChangeInd=\"No\" DefaultLanguageCd=\"en_US\" Version=\"1.10\"><UserRole id=\"UserRole-32612364-16470265\" StartDt=\"19010101\" EndDt=\"99991231\" AuthorityRoleIdRef=\"FileListener\" /><BeanStats id=\"BeanStats-5780257-32196914\" BeanAddUserIdRef=\"admin\" BeanAddDt=\"20170516\" BeanAddTm=\"16:00:02 CDT\" BeanUpdateUserIdRef=\"file-listener\" BeanUpdateDt=\"20170516\" BeanUpdateTm=\"16:00:34 CDT\" /><UserTaskControl id=\"UserTaskControl-283579070-1192149690\" /><PartyInfo id=\"PartyInfo-24185195-14622325\" PartyTypeCd=\"UserInfoParty\"><PhoneInfo id=\"PhoneInfo-8379738-4169749\" PhoneTypeCd=\"UserInfoPhoneOne\" PreferredInd=\"No\" /><Addr id=\"Addr-28042152-9314311\" AddrTypeCd=\"UserInfoWorkAddr\" /><PhoneInfo id=\"PhoneInfo-31377512-21514896\" PhoneTypeCd=\"UserInfoPhoneTwo\" PreferredInd=\"No\" /><PhoneInfo id=\"PhoneInfo-31055278-124232\" PhoneTypeCd=\"UserInfoFax\" PreferredInd=\"No\" /><Addr id=\"Addr-17542101-12001753\" AddrTypeCd=\"UserInfoHomeAddr\" /></PartyInfo><ChangeInfo id=\"ChangeInfo-14879262-3668335\" UserId=\"admin\" AddDt=\"20170516\" AddTm=\"16:00:03 CDT\" Status=\"Completed\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-5052620-561564\" IdRef=\"UserInfo-21359527-3592898\" ActionCd=\"M\" PreviousValue=\"~d41d8cd98f0b24e980998ecf8427e\" NewValue=\"~fa246d262c3925617b0c72bb2eeb1d\" Field=\"Password\" Status=\"Pending\" /><ModInfo id=\"ModInfo-22930356-7535716\" IdRef=\"UserInfo-21359527-3592898\" ActionCd=\"M\" NewValue=\"No\" Field=\"PasswordMustChangeInd\" Status=\"Pending\" /></ChangeInfo><ChangeInfo id=\"ChangeInfo-18394473-19002442\" UserId=\"admin\" AddDt=\"20170516\" AddTm=\"16:00:22 CDT\" Status=\"Completed\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-1912469-31094097\" IdRef=\"UserRole-32612364-16470265\" ParentIdRef=\"UserInfo-21359527-3592898\" ActionCd=\"A\" Field=\"UserRole\" Status=\"Pending\" /><ModInfo id=\"ModInfo-2889436-31776580\" IdRef=\"UserRole-32612364-16470265\" ActionCd=\"M\" NewValue=\"19010101\" Field=\"StartDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-21145526-19646974\" IdRef=\"UserRole-32612364-16470265\" ActionCd=\"M\" NewValue=\"99991231\" Field=\"EndDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-19642993-14309167\" IdRef=\"UserRole-32612364-16470265\" ActionCd=\"M\" NewValue=\"FileListener\" Field=\"AuthorityRoleIdRef\" Status=\"Pending\" /></ChangeInfo><ChangeInfo id=\"ChangeInfo-1188214-1473279\" UserId=\"admin\" AddDt=\"20170516\" AddTm=\"16:00:27 CDT\" Status=\"Completed\" ChangeType=\"Normal\"><ModInfo id=\"ModInfo-32144045-16896107\" IdRef=\"UserRole-80154998-723204301\" ActionCd=\"M\" PreviousValue=\"19010101\" Field=\"StartDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-26396514-25469955\" IdRef=\"UserRole-80154998-723204301\" ActionCd=\"M\" PreviousValue=\"99991231\" Field=\"EndDt\" Status=\"Pending\" /><ModInfo id=\"ModInfo-19891020-1872330\" IdRef=\"UserRole-80154998-723204301\" ActionCd=\"M\" PreviousValue=\"EmailListener\" Field=\"AuthorityRoleIdRef\" Status=\"Pending\" /><ModInfo id=\"ModInfo-21946684-26798250\" IdRef=\"UserRole-80154998-723204301\" ParentIdRef=\"UserInfo-21359527-3592898\" ActionCd=\"D\" Field=\"UserRole\" Status=\"Pending\" /></ChangeInfo></UserInfo>',NULL);
/*!40000 ALTER TABLE `userinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userinfolookup`
--

DROP TABLE IF EXISTS `userinfolookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfolookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userinfolookup`
--

LOCK TABLES `userinfolookup` WRITE;
/*!40000 ALTER TABLE `userinfolookup` DISABLE KEYS */;
INSERT INTO `userinfolookup` VALUES (1,'export','TRUE',NULL),(1,'FirstName','SYSTEM',NULL),(1,'LastName','ADMINISTRATOR',NULL),(1,'LoginId','ADMIN',NULL),(1,'Supervisor','',NULL),(1,'UserEmailAddr','',NULL),(1,'UserLoginId','ADMIN',NULL),(1,'UserRoleName','EVERYTHING',NULL),(1,'UserRoleName','RATINGSERVICEDEVELOPER',NULL),(1,'UserTypeCd','SYSADMIN',NULL),(2,'export','FALSE',NULL),(2,'FirstName','WEB',NULL),(2,'LastName','PORTAL',NULL),(2,'LoginId','WEBPORTAL',NULL),(2,'Supervisor','ADMIN',NULL),(2,'UserEmailAddr','',NULL),(2,'UserLoginId','WEBPORTAL',NULL),(2,'UserRoleName','SERVICEPORTAL',NULL),(2,'UserTypeCd','GUEST',NULL),(3,'export','FALSE',NULL),(3,'FirstName','DIRECT',NULL),(3,'LastName','PORTAL',NULL),(3,'LoginId','DIRECTPORTAL',NULL),(3,'Supervisor','ADMIN',NULL),(3,'UserEmailAddr','',NULL),(3,'UserLoginId','DIRECTPORTAL',NULL),(3,'UserRoleName','DIRECTSALESPORTAL',NULL),(3,'UserTypeCd','GUEST',NULL),(4,'export','FALSE',NULL),(4,'FirstName','EMAIL',NULL),(4,'LastName','LISTENER',NULL),(4,'LoginId','EMAILLISTENER',NULL),(4,'Supervisor','ADMIN',NULL),(4,'UserEmailAddr','',NULL),(4,'UserLoginId','EMAIL-LISTENER',NULL),(4,'UserRoleName','EMAILLISTENER',NULL),(4,'UserTypeCd','SYSADMIN',NULL),(5,'export','TRUE',NULL),(5,'FirstName','FILE',NULL),(5,'LastName','LISTENER',NULL),(5,'LoginId','FILELISTENER',NULL),(5,'Supervisor','ADMIN',NULL),(5,'UserEmailAddr','',NULL),(5,'UserLoginId','FILE-LISTENER',NULL),(5,'UserManagementGroupCd','',NULL),(5,'UserRoleName','FILELISTENER',NULL),(5,'UserTypeCd','SYSADMIN',NULL);
/*!40000 ALTER TABLE `userinfolookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userrecentactivity`
--

DROP TABLE IF EXISTS `userrecentactivity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userrecentactivity` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userrecentactivity`
--

LOCK TABLES `userrecentactivity` WRITE;
/*!40000 ALTER TABLE `userrecentactivity` DISABLE KEYS */;
/*!40000 ALTER TABLE `userrecentactivity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userrecentactivitylookup`
--

DROP TABLE IF EXISTS `userrecentactivitylookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userrecentactivitylookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userrecentactivitylookup`
--

LOCK TABLES `userrecentactivitylookup` WRITE;
/*!40000 ALTER TABLE `userrecentactivitylookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `userrecentactivitylookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehiclemanufacturer`
--

DROP TABLE IF EXISTS `vehiclemanufacturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehiclemanufacturer` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehiclemanufacturer`
--

LOCK TABLES `vehiclemanufacturer` WRITE;
/*!40000 ALTER TABLE `vehiclemanufacturer` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehiclemanufacturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehiclemanufacturerlookup`
--

DROP TABLE IF EXISTS `vehiclemanufacturerlookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehiclemanufacturerlookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehiclemanufacturerlookup`
--

LOCK TABLES `vehiclemanufacturerlookup` WRITE;
/*!40000 ALTER TABLE `vehiclemanufacturerlookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehiclemanufacturerlookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehiclemodel`
--

DROP TABLE IF EXISTS `vehiclemodel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehiclemodel` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehiclemodel`
--

LOCK TABLES `vehiclemodel` WRITE;
/*!40000 ALTER TABLE `vehiclemodel` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehiclemodel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehiclemodellookup`
--

DROP TABLE IF EXISTS `vehiclemodellookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehiclemodellookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehiclemodellookup`
--

LOCK TABLES `vehiclemodellookup` WRITE;
/*!40000 ALTER TABLE `vehiclemodellookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehiclemodellookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wcpolscontrib`
--

DROP TABLE IF EXISTS `wcpolscontrib`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcpolscontrib` (
  `SystemId` int(11) NOT NULL AUTO_INCREMENT,
  `UpdateCount` int(11) NOT NULL DEFAULT '0',
  `UpdateUser` varchar(50) DEFAULT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `XmlContent` longtext NOT NULL,
  `MiniXmlContent` longtext,
  UNIQUE KEY `SystemId` (`SystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wcpolscontrib`
--

LOCK TABLES `wcpolscontrib` WRITE;
/*!40000 ALTER TABLE `wcpolscontrib` DISABLE KEYS */;
/*!40000 ALTER TABLE `wcpolscontrib` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wcpolscontriblookup`
--

DROP TABLE IF EXISTS `wcpolscontriblookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcpolscontriblookup` (
  `SystemId` int(11) NOT NULL,
  `LookupKey` varchar(255) NOT NULL DEFAULT '',
  `LookupValue` varchar(255) NOT NULL,
  `Preview` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SystemId`,`LookupKey`,`LookupValue`),
  KEY `LookupKeyValue` (`LookupKey`,`LookupValue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wcpolscontriblookup`
--

LOCK TABLES `wcpolscontriblookup` WRITE;
/*!40000 ALTER TABLE `wcpolscontriblookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `wcpolscontriblookup` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-16 16:06:51
