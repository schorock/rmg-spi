-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: start_develop_dw
-- ------------------------------------------------------
-- Server version	5.6.28-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `AccountPrevRef` int(11) DEFAULT NULL,
  `AccountNextRef` int(11) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  `AccountDisplayNumber` varchar(255) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `IndexName` varchar(255) DEFAULT NULL,
  `ProductVersionCd` varchar(255) DEFAULT NULL,
  `InstallmentPlan` varchar(255) DEFAULT NULL,
  `BillToCd` varchar(255) DEFAULT NULL,
  `ProviderCode` varchar(255) DEFAULT NULL,
  `ProviderName` varchar(255) DEFAULT NULL,
  `PrintInvoiceInd` varchar(255) DEFAULT NULL,
  `PaidInd` varchar(255) DEFAULT NULL,
  `PayPlanCd` varchar(255) DEFAULT NULL,
  `CancelCd` varchar(255) DEFAULT NULL,
  `CommissionRate` decimal(28,6) DEFAULT NULL,
  `WrittenAmt` decimal(28,6) DEFAULT NULL,
  `CommissionAmt` decimal(28,6) DEFAULT NULL,
  `NetAmt` decimal(28,6) DEFAULT NULL,
  `OpenAmt` decimal(28,6) DEFAULT NULL,
  `UnbilledAmt` decimal(28,6) DEFAULT NULL,
  `TotalAmt` decimal(28,6) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `AdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `CreditAmt` decimal(28,6) DEFAULT NULL,
  `CarryDt` date DEFAULT NULL,
  `CancelDt` date DEFAULT NULL,
  `NoticeDt` date DEFAULT NULL,
  `CycleCd` varchar(255) DEFAULT NULL,
  `NextCycleActionCd` varchar(255) DEFAULT NULL,
  `NextCycleActionDt` date DEFAULT NULL,
  `NextCycleActionDesc` varchar(255) DEFAULT NULL,
  `ReceiptOnLegalInd` varchar(255) DEFAULT NULL,
  `ReceiptOnCancelInd` varchar(255) DEFAULT NULL,
  `Version` varchar(255) DEFAULT NULL,
  `NetOpenAmt` decimal(28,6) DEFAULT NULL,
  `NetTotalAmt` decimal(28,6) DEFAULT NULL,
  `NetPaidAmt` decimal(28,6) DEFAULT NULL,
  `NetAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `NetUnbilledAmt` decimal(28,6) DEFAULT NULL,
  `ActivatedInd` varchar(255) DEFAULT NULL,
  `BillingEntityCarryDt` date DEFAULT NULL,
  `BillingEntitySequenceNumber` varchar(255) DEFAULT NULL,
  `BillingEntityTypeCd` varchar(255) DEFAULT NULL,
  `VIPLevel` varchar(255) DEFAULT NULL,
  `PreviousCancelCd` varchar(255) DEFAULT NULL,
  `AuditAccountInd` varchar(255) DEFAULT NULL,
  `StatementAccountRef` int(11) DEFAULT NULL,
  KEY `accountIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accountbalance`
--

DROP TABLE IF EXISTS `accountbalance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountbalance` (
  `AsOfDt` date DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyVersion` decimal(10,0) DEFAULT NULL,
  `SequenceNum` decimal(10,0) DEFAULT NULL,
  `ReportText` varchar(255) DEFAULT NULL,
  `AccountTypeCd` varchar(255) DEFAULT NULL,
  `TransactionTypeCd` varchar(255) DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `ChargeAmt` decimal(28,6) DEFAULT NULL,
  `CommissionChargeAmt` decimal(28,6) DEFAULT NULL,
  `BalanceAmt` decimal(28,6) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `AdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `CategoryCd` varchar(255) DEFAULT NULL,
  `StatementTypeCd` varchar(255) DEFAULT NULL,
  `ActivityTypeCd` varchar(255) DEFAULT NULL,
  `CommisionPct` decimal(28,6) DEFAULT NULL,
  `CommissionTypeCd` varchar(255) DEFAULT NULL,
  `CommissionKey` varchar(255) DEFAULT NULL,
  `TotalAmt` decimal(28,6) DEFAULT NULL,
  `DBPremiumAmt` decimal(28,6) DEFAULT NULL,
  `ABPremiumAmt` decimal(28,6) DEFAULT NULL,
  `PolicyFeeAmt` decimal(28,6) DEFAULT NULL,
  `PolicyFeeEarnedAmt` decimal(28,6) DEFAULT NULL,
  `InstallmentFeeAmt` decimal(28,6) DEFAULT NULL,
  `NSFFeeAmt` decimal(28,6) DEFAULT NULL,
  `LateFeeAmt` decimal(28,6) DEFAULT NULL,
  `ReinstatementFeeAmt` decimal(28,6) DEFAULT NULL,
  `SIGAFeeAmt` decimal(28,6) DEFAULT NULL,
  `TerrorismFeeAmt` decimal(28,6) DEFAULT NULL,
  `InspectionFeeAmt` decimal(28,6) DEFAULT NULL,
  `OtherFeesAmt` decimal(28,6) DEFAULT NULL,
  `SuspenseAmt` decimal(28,6) DEFAULT NULL,
  `DBCommissionAmt` decimal(28,6) DEFAULT NULL,
  `TotalLineAmt` decimal(28,6) DEFAULT NULL,
  `ABCommissionAmt` decimal(28,6) DEFAULT NULL,
  `ClosingBalanceDt` date DEFAULT NULL,
  `BillMethod` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `accountingstats`
--

DROP TABLE IF EXISTS `accountingstats`;
/*!50001 DROP VIEW IF EXISTS `accountingstats`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `accountingstats` AS SELECT 
 1 AS `SystemId`,
 1 AS `id`,
 1 AS `StatSequence`,
 1 AS `StatSequenceReplace`,
 1 AS `PolicyRef`,
 1 AS `PolicyNumber`,
 1 AS `PolicyVersion`,
 1 AS `LineCd`,
 1 AS `TransactionNumber`,
 1 AS `CoverageCd`,
 1 AS `RiskCd`,
 1 AS `RiskTypeCd`,
 1 AS `SubTypeCd`,
 1 AS `CoverageItemCd`,
 1 AS `FeeCd`,
 1 AS `EffectiveDt`,
 1 AS `ExpirationDt`,
 1 AS `CombinedKey`,
 1 AS `AddDt`,
 1 AS `BookDt`,
 1 AS `TransactionEffectiveDt`,
 1 AS `AccountingDt`,
 1 AS `WrittenPremiumAmt`,
 1 AS `WrittenCommissionAmt`,
 1 AS `WrittenPremiumFeeAmt`,
 1 AS `WrittenCommissionFeeAmt`,
 1 AS `EarnDays`,
 1 AS `InforceChangeAmt`,
 1 AS `PolicyYear`,
 1 AS `TermDays`,
 1 AS `InsuranceTypeCd`,
 1 AS `StartDt`,
 1 AS `EndDt`,
 1 AS `StatusCd`,
 1 AS `NewRenewalCd`,
 1 AS `Limit1`,
 1 AS `Limit2`,
 1 AS `Deductible1`,
 1 AS `Deductible2`,
 1 AS `AnnualStatementLineCd`,
 1 AS `CoinsurancePct`,
 1 AS `CoveredPerilsCd`,
 1 AS `CommissionKey`,
 1 AS `CommissionAreaCd`,
 1 AS `RateAreaName`,
 1 AS `StatData`,
 1 AS `CarrierGroupCd`,
 1 AS `CarrierCd`,
 1 AS `ProductVersionIdRef`,
 1 AS `StateCd`,
 1 AS `BusinessSourceCd`,
 1 AS `ProducerCd`,
 1 AS `TransactionCd`,
 1 AS `PolicyStatusCd`,
 1 AS `ProductName`,
 1 AS `PolicyTypeCd`,
 1 AS `PolicyGroupCd`,
 1 AS `CustomerRef`,
 1 AS `ShortRateStatInd`,
 1 AS `PayPlanCd`,
 1 AS `ControllingProductVersionIdRef`,
 1 AS `ControllingStateCd`,
 1 AS `DividendPlanCd`,
 1 AS `StatementAccountNumber`,
 1 AS `StatementAccountRef`,
 1 AS `ConversionTemplateIdRef`,
 1 AS `ConversionGroup`,
 1 AS `ConversionJobRef`,
 1 AS `ConversionFileName`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `accountingsummarystats`
--

DROP TABLE IF EXISTS `accountingsummarystats`;
/*!50001 DROP VIEW IF EXISTS `accountingsummarystats`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `accountingsummarystats` AS SELECT 
 1 AS `SystemId`,
 1 AS `id`,
 1 AS `StatSequence`,
 1 AS `StatSequenceReplace`,
 1 AS `ReportPeriod`,
 1 AS `UpdateDt`,
 1 AS `PolicyRef`,
 1 AS `PolicyNumber`,
 1 AS `PolicyVersion`,
 1 AS `TransactionNumber`,
 1 AS `LineCd`,
 1 AS `RiskCd`,
 1 AS `RiskTypeCd`,
 1 AS `SubTypeCd`,
 1 AS `CoverageCd`,
 1 AS `CoverageItemCd`,
 1 AS `FeeCd`,
 1 AS `EffectiveDt`,
 1 AS `ExpirationDt`,
 1 AS `CombinedKey`,
 1 AS `AccountingDt`,
 1 AS `PolicyYear`,
 1 AS `InsuranceTypeCd`,
 1 AS `StatusCd`,
 1 AS `NewRenewalCd`,
 1 AS `Limit1`,
 1 AS `Limit2`,
 1 AS `Limit3`,
 1 AS `Limit4`,
 1 AS `Limit5`,
 1 AS `Limit6`,
 1 AS `Limit7`,
 1 AS `Limit8`,
 1 AS `Limit9`,
 1 AS `Deductible1`,
 1 AS `Deductible2`,
 1 AS `AnnualStatementLineCd`,
 1 AS `CoinsurancePct`,
 1 AS `CoveredPerilsCd`,
 1 AS `CommissionAreaCd`,
 1 AS `MTDWrittenPremiumAmt`,
 1 AS `TTDWrittenPremiumAmt`,
 1 AS `YTDWrittenPremiumAmt`,
 1 AS `MTDWrittenCommissionAmt`,
 1 AS `YTDWrittenCommissionAmt`,
 1 AS `TTDWrittenCommissionAmt`,
 1 AS `MTDWrittenPremiumFeeAmt`,
 1 AS `YTDWrittenPremiumFeeAmt`,
 1 AS `TTDWrittenPremiumFeeAmt`,
 1 AS `MTDWrittenCommissionFeeAmt`,
 1 AS `YTDWrittenCommissionFeeAmt`,
 1 AS `TTDWrittenCommissionFeeAmt`,
 1 AS `InforceAmt`,
 1 AS `LastInforceAmt`,
 1 AS `MonthEarnedPremiumAmt`,
 1 AS `MTDEarnedPremiumAmt`,
 1 AS `YTDEarnedPremiumAmt`,
 1 AS `TTDEarnedPremiumAmt`,
 1 AS `PolicyInforceCount`,
 1 AS `LocationInforceCount`,
 1 AS `RiskInforceCount`,
 1 AS `MonthUnearnedAmt`,
 1 AS `UnearnedAmt`,
 1 AS `UnearnedM00Amt`,
 1 AS `UnearnedM01Amt`,
 1 AS `UnearnedM02Amt`,
 1 AS `UnearnedM03Amt`,
 1 AS `UnearnedM04Amt`,
 1 AS `UnearnedM05Amt`,
 1 AS `UnearnedM06Amt`,
 1 AS `UnearnedM07Amt`,
 1 AS `UnearnedM08Amt`,
 1 AS `UnearnedM09Amt`,
 1 AS `UnearnedM10Amt`,
 1 AS `UnearnedM11Amt`,
 1 AS `UnearnedM12Amt`,
 1 AS `UnearnedM13Amt`,
 1 AS `UnearnedM14Amt`,
 1 AS `UnearnedM15Amt`,
 1 AS `UnearnedM16Amt`,
 1 AS `UnearnedM17Amt`,
 1 AS `UnearnedM18Amt`,
 1 AS `UnearnedM19Amt`,
 1 AS `UnearnedM20Amt`,
 1 AS `UnearnedM21Amt`,
 1 AS `UnearnedM22Amt`,
 1 AS `UnearnedM23Amt`,
 1 AS `UnearnedM24Amt`,
 1 AS `UnearnedM25Amt`,
 1 AS `UnearnedM26Amt`,
 1 AS `UnearnedM27Amt`,
 1 AS `UnearnedM28Amt`,
 1 AS `UnearnedM29Amt`,
 1 AS `UnearnedM30Amt`,
 1 AS `UnearnedM31Amt`,
 1 AS `UnearnedM32Amt`,
 1 AS `UnearnedM33Amt`,
 1 AS `UnearnedM34Amt`,
 1 AS `UnearnedM35Amt`,
 1 AS `UnearnedM36Amt`,
 1 AS `UnearnedM37Amt`,
 1 AS `UnearnedM38Amt`,
 1 AS `UnearnedM39Amt`,
 1 AS `UnearnedM40Amt`,
 1 AS `UnearnedM41Amt`,
 1 AS `UnearnedM42Amt`,
 1 AS `UnearnedM43Amt`,
 1 AS `UnearnedM44Amt`,
 1 AS `UnearnedM45Amt`,
 1 AS `UnearnedM46Amt`,
 1 AS `UnearnedM47Amt`,
 1 AS `UnearnedM48Amt`,
 1 AS `RateAreaName`,
 1 AS `StatData`,
 1 AS `CarrierGroupCd`,
 1 AS `CarrierCd`,
 1 AS `ProductVersionIdRef`,
 1 AS `StateCd`,
 1 AS `BusinessSourceCd`,
 1 AS `ProducerCd`,
 1 AS `TransactionCd`,
 1 AS `PolicyStatusCd`,
 1 AS `ProductName`,
 1 AS `PolicyTypeCd`,
 1 AS `PolicyGroupCd`,
 1 AS `CustomerRef`,
 1 AS `ControllingProductVersionIdRef`,
 1 AS `ControllingStateCd`,
 1 AS `DividendPlanCd`,
 1 AS `ConversionTemplateIdRef`,
 1 AS `ConversionGroup`,
 1 AS `ConversionJobRef`,
 1 AS `ConversionFileName`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `accountstats`
--

DROP TABLE IF EXISTS `accountstats`;
/*!50001 DROP VIEW IF EXISTS `accountstats`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `accountstats` AS SELECT 
 1 AS `SystemId`,
 1 AS `id`,
 1 AS `StatSequence`,
 1 AS `StatSequenceReplace`,
 1 AS `PolicyNumber`,
 1 AS `PolicyVersion`,
 1 AS `BillingEntitySequenceNumber`,
 1 AS `CombinedKey`,
 1 AS `AddDt`,
 1 AS `BookDt`,
 1 AS `StartDt`,
 1 AS `EndDt`,
 1 AS `StatusCd`,
 1 AS `AccountNumber`,
 1 AS `AccountTypeCd`,
 1 AS `PayPlanCd`,
 1 AS `TransactionTypeCd`,
 1 AS `SourceCd`,
 1 AS `TransactionNumber`,
 1 AS `PolicyTransactionCd`,
 1 AS `ChargeAmt`,
 1 AS `CommissionChargeAmt`,
 1 AS `BalanceAmt`,
 1 AS `PaidAmt`,
 1 AS `AdjustmentAmt`,
 1 AS `CategoryCd`,
 1 AS `NetPaidAmt`,
 1 AS `EffectiveDt`,
 1 AS `ProviderCd`,
 1 AS `CheckNumber`,
 1 AS `CheckAmt`,
 1 AS `CheckDt`,
 1 AS `BatchId`,
 1 AS `NextDueDt`,
 1 AS `InsuredName`,
 1 AS `PaidBy`,
 1 AS `PaidByName`,
 1 AS `StatementTypeCd`,
 1 AS `ActivityTypeCd`,
 1 AS `PolicyTransactionNumber`,
 1 AS `CommisionPct`,
 1 AS `CommissionTypeCd`,
 1 AS `AccountRef`,
 1 AS `PolicyRef`,
 1 AS `PayOffAmt`,
 1 AS `ReversalStopInd`,
 1 AS `CommissionKey`,
 1 AS `PolicyTransactionEffectiveDt`,
 1 AS `AccountingDt`,
 1 AS `CarrierGroupCd`,
 1 AS `CarrierCd`,
 1 AS `CustomerRef`,
 1 AS `BillMethod`,
 1 AS `StateCd`,
 1 AS `DepositoryLocationCd`,
 1 AS `NetChargeAmt`,
 1 AS `NetAdjustmentAmt`,
 1 AS `GrossNetInd`,
 1 AS `NetBalanceAmt`,
 1 AS `CommissionTakenAmt`,
 1 AS `TransactionDesc`,
 1 AS `ReverseReason`,
 1 AS `EntryDt`,
 1 AS `SystemCheckNumber`,
 1 AS `AdjustmentTypeCd`,
 1 AS `ACHAgent`,
 1 AS `SettlementDt`,
 1 AS `AuditAccountInd`,
 1 AS `StatementAccountNumber`,
 1 AS `StatementAccountRef`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `accountsummarystats`
--

DROP TABLE IF EXISTS `accountsummarystats`;
/*!50001 DROP VIEW IF EXISTS `accountsummarystats`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `accountsummarystats` AS SELECT 
 1 AS `SystemId`,
 1 AS `id`,
 1 AS `ReportPeriod`,
 1 AS `UpdateDt`,
 1 AS `PolicyNumber`,
 1 AS `PolicyVersion`,
 1 AS `BillingEntitySequenceNumber`,
 1 AS `AccountNumber`,
 1 AS `PolicyRef`,
 1 AS `AccountRef`,
 1 AS `AccountTypeCd`,
 1 AS `PayPlanCd`,
 1 AS `StatusCd`,
 1 AS `EffectiveDt`,
 1 AS `ProviderCd`,
 1 AS `InsuredName`,
 1 AS `NextDueDt`,
 1 AS `CommissionPct`,
 1 AS `CommissionTypeCd`,
 1 AS `PremMTDChargeAmt`,
 1 AS `PremYTDChargeAmt`,
 1 AS `PremTTDChargeAmt`,
 1 AS `PremMTDCommissionChargeAmt`,
 1 AS `PremTTDCommissionChargeAmt`,
 1 AS `PremYTDCommissionChargeAmt`,
 1 AS `PremBalanceAmt`,
 1 AS `PremMTDPaidAmt`,
 1 AS `PremTTDPaidAmt`,
 1 AS `PremYTDPaidAmt`,
 1 AS `PremMTDAdjustmentAmt`,
 1 AS `PremYTDAdjustmentAmt`,
 1 AS `PremTTDAdjustmentAmt`,
 1 AS `C1MTDChargeAmt`,
 1 AS `C1TTDChargeAmt`,
 1 AS `C1YTDChargeAmt`,
 1 AS `C1MTDCommissionChargeAmt`,
 1 AS `C1YTDCommissionChargeAmt`,
 1 AS `C1TTDCommissionChargeAmt`,
 1 AS `C1BalanceAmt`,
 1 AS `C1MTDPaidAmt`,
 1 AS `C1YTDPaidAmt`,
 1 AS `C1TTDPaidAmt`,
 1 AS `C1MTDAdjustmentAmt`,
 1 AS `C1TTDAdjustmentAmt`,
 1 AS `C1YTDAdjustmentAmt`,
 1 AS `C2MTDChargeAmt`,
 1 AS `C2YTDChargeAmt`,
 1 AS `C2MTDCommissionChargeAmt`,
 1 AS `C2TTDChargeAmt`,
 1 AS `C2TTDCommissionChargeAmt`,
 1 AS `C2YTDCommissionChargeAmt`,
 1 AS `C2BalanceAmt`,
 1 AS `C2MTDPaidAmt`,
 1 AS `C2TTDPaidAmt`,
 1 AS `C2YTDPaidAmt`,
 1 AS `C2MTDAdjustmentAmt`,
 1 AS `C2YTDAdjustmentAmt`,
 1 AS `C2TTDAdjustmentAmt`,
 1 AS `C3MTDChargeAmt`,
 1 AS `C3TTDChargeAmt`,
 1 AS `C3YTDChargeAmt`,
 1 AS `C3MTDCommissionChargeAmt`,
 1 AS `C3TTDCommissionChargeAmt`,
 1 AS `C3BalanceAmt`,
 1 AS `C3YTDCommissionChargeAmt`,
 1 AS `C3MTDPaidAmt`,
 1 AS `C3TTDPaidAmt`,
 1 AS `C3MTDAdjustmentAmt`,
 1 AS `C3YTDPaidAmt`,
 1 AS `C3TTDAdjustmentAmt`,
 1 AS `C3YTDAdjustmentAmt`,
 1 AS `C4MTDChargeAmt`,
 1 AS `C4YTDChargeAmt`,
 1 AS `C4MTDCommissionChargeAmt`,
 1 AS `C4TTDChargeAmt`,
 1 AS `C4TTDCommissionChargeAmt`,
 1 AS `C4YTDCommissionChargeAmt`,
 1 AS `C4BalanceAmt`,
 1 AS `C4MTDPaidAmt`,
 1 AS `C4TTDPaidAmt`,
 1 AS `C4YTDPaidAmt`,
 1 AS `C4MTDAdjustmentAmt`,
 1 AS `C4YTDAdjustmentAmt`,
 1 AS `C4TTDAdjustmentAmt`,
 1 AS `NSFCount`,
 1 AS `LateCount`,
 1 AS `ReinstatementCount`,
 1 AS `CancellationCount`,
 1 AS `CancellationNoticeCount`,
 1 AS `LessOr30AmtDueDt`,
 1 AS `Over30AmtDueDt`,
 1 AS `Over60AmtDueDt`,
 1 AS `Over90AmtDueDt`,
 1 AS `LessOr30AmtEffectiveDt`,
 1 AS `Over30AmtEffectiveDt`,
 1 AS `Over60AmtEffectiveDt`,
 1 AS `Over90AmtEffectiveDt`,
 1 AS `CarrierGroupCd`,
 1 AS `CarrierCd`,
 1 AS `CustomerRef`,
 1 AS `TotalMTDChargeAmt`,
 1 AS `TotalTTDChargeAmt`,
 1 AS `TotalYTDChargeAmt`,
 1 AS `TotalMTDCommissionChargeAmt`,
 1 AS `TotalTTDCommissionChargeAmt`,
 1 AS `TotalYTDCommissionChargeAmt`,
 1 AS `TotalMTDPaidAmt`,
 1 AS `TotalYTDPaidAmt`,
 1 AS `TotalTDDPaidAmt`,
 1 AS `TotalMTDAdjustmentAmt`,
 1 AS `TotalTTDAdjustmentAmt`,
 1 AS `TotalYTDAdjustmentAmt`,
 1 AS `PremPayOffBalanceAmt`,
 1 AS `TotalBalanceAmt`,
 1 AS `C1PayOffBalanceAmt`,
 1 AS `C2PayOffBalanceAmt`,
 1 AS `C3PayOffBalanceAmt`,
 1 AS `C4PayOffBalanceAmt`,
 1 AS `TotalPayOffBalanceAmt`,
 1 AS `CurrentAmtDueDt`,
 1 AS `CurrentAmtEffectiveDt`,
 1 AS `StatementTypeCd`,
 1 AS `BillMethod`,
 1 AS `StateCd`,
 1 AS `DepositoryLocationCd`,
 1 AS `FutureChargeDueAmt`,
 1 AS `FuturePaidDueAmt`,
 1 AS `FutureBalanceDueAmt`,
 1 AS `FutureChargeEffectiveAmt`,
 1 AS `FuturePaidEffectiveAmt`,
 1 AS `FutureBalanceEffectiveAmt`,
 1 AS `CurrentUnbilledAmtDueDt`,
 1 AS `CurrentUnbilledAmtEffectiveDt`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `acroccurance`
--

DROP TABLE IF EXISTS `acroccurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acroccurance` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `BenefitACRCd` varchar(255) DEFAULT NULL,
  `BenefitACR` varchar(255) DEFAULT NULL,
  `BenefitACREndDt` varchar(255) DEFAULT NULL,
  `BenefitACRWeeklyAmt` varchar(255) DEFAULT NULL,
  KEY `acroccuranceIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `action`
--

DROP TABLE IF EXISTS `action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ActionTypeCd` varchar(255) DEFAULT NULL,
  `Desc` varchar(255) DEFAULT NULL,
  `ActionDt` date DEFAULT NULL,
  `ActionTm` varchar(255) DEFAULT NULL,
  `Memo` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  KEY `actionIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `additionalinsured`
--

DROP TABLE IF EXISTS `additionalinsured`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `additionalinsured` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `IndexName` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `InsuredNumber` int(11) DEFAULT NULL,
  `EntityTypeCd` varchar(255) DEFAULT NULL,
  `PreferredDeliveryMethod` varchar(255) DEFAULT NULL,
  KEY `additionalinsuredIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `addr`
--

DROP TABLE IF EXISTS `addr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addr` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AddrTypeCd` varchar(255) DEFAULT NULL,
  `Addr1` varchar(255) DEFAULT NULL,
  `Addr2` varchar(255) DEFAULT NULL,
  `Addr3` varchar(255) DEFAULT NULL,
  `Addr4` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL,
  `StateProvCd` varchar(255) DEFAULT NULL,
  `PostalCode` varchar(255) DEFAULT NULL,
  `County` varchar(255) DEFAULT NULL,
  `RegionCd` varchar(255) DEFAULT NULL,
  `PreDirectional` varchar(255) DEFAULT NULL,
  `PrimaryNumber` varchar(255) DEFAULT NULL,
  `StreetName` varchar(255) DEFAULT NULL,
  `Suffix` varchar(255) DEFAULT NULL,
  `PostDirectional` varchar(255) DEFAULT NULL,
  `SecondaryDesignator` varchar(255) DEFAULT NULL,
  `SecondaryNumber` varchar(255) DEFAULT NULL,
  `VerificationMsg` varchar(255) DEFAULT NULL,
  `VerificationHash` varchar(255) DEFAULT NULL,
  `Latitude` varchar(255) DEFAULT NULL,
  `Longitude` varchar(255) DEFAULT NULL,
  `Score` varchar(255) DEFAULT NULL,
  `PrimaryNumberSuffix` varchar(255) DEFAULT NULL,
  `DPV` varchar(255) DEFAULT NULL,
  `DPVDesc` varchar(255) DEFAULT NULL,
  `DPVNotes` varchar(255) DEFAULT NULL,
  `DPVNotesDesc` varchar(255) DEFAULT NULL,
  `Attention` varchar(255) DEFAULT NULL,
  KEY `addrIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ai`
--

DROP TABLE IF EXISTS `ai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ai` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `SequenceNumber` int(11) DEFAULT NULL,
  `InterestCd` varchar(255) DEFAULT NULL,
  `InterestTypeCd` varchar(255) DEFAULT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  `InterestFormCd` varchar(255) DEFAULT NULL,
  `InterestName` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `AdditionalComments` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `PreferredDeliveryMethod` varchar(255) DEFAULT NULL,
  `EscrowInd` varchar(255) DEFAULT NULL,
  `FutureStatusCd` varchar(255) DEFAULT NULL,
  `ClassCodeCd` varchar(255) DEFAULT NULL,
  `IncludeExcludeCd` varchar(255) DEFAULT NULL,
  `OwnershipPct` varchar(255) DEFAULT NULL,
  `RemunerationPayrollAmt` decimal(28,6) DEFAULT NULL,
  `TitleRelationshipCd` varchar(255) DEFAULT NULL,
  `Duties` varchar(255) DEFAULT NULL,
  `CoveredStateIdRef` varchar(255) DEFAULT NULL,
  `LocationIdRef` varchar(255) DEFAULT NULL,
  `ActualRemunerationPayrollAmt` decimal(28,6) DEFAULT NULL,
  KEY `aiIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aircraftpremendinfo`
--

DROP TABLE IF EXISTS `aircraftpremendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aircraftpremendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `StateCd01` int(11) DEFAULT NULL,
  `TypeofAircraft01` varchar(255) DEFAULT NULL,
  `PassengerSeatChargeAmt01` int(11) DEFAULT NULL,
  `MaximumChargeAmt01` int(11) DEFAULT NULL,
  `EstimatedPremiumAmt01` int(11) DEFAULT NULL,
  `StateCd02` int(11) DEFAULT NULL,
  `TypeofAircraft02` varchar(255) DEFAULT NULL,
  `PassengerSeatChargeAmt02` int(11) DEFAULT NULL,
  `MaximumChargeAmt02` int(11) DEFAULT NULL,
  `EstimatedPremiumAmt02` int(11) DEFAULT NULL,
  `StateCd03` int(11) DEFAULT NULL,
  `TypeofAircraft03` varchar(255) DEFAULT NULL,
  `PassengerSeatChargeAmt03` int(11) DEFAULT NULL,
  `MaximumChargeAmt03` int(11) DEFAULT NULL,
  `EstimatedPremiumAmt03` int(11) DEFAULT NULL,
  `StateCd04` int(11) DEFAULT NULL,
  `TypeofAircraft04` varchar(255) DEFAULT NULL,
  `PassengerSeatChargeAmt04` int(11) DEFAULT NULL,
  `MaximumChargeAmt04` int(11) DEFAULT NULL,
  `EstimatedPremiumAmt04` int(11) DEFAULT NULL,
  `StateCd05` int(11) DEFAULT NULL,
  `TypeofAircraft05` varchar(255) DEFAULT NULL,
  `PassengerSeatChargeAmt05` int(11) DEFAULT NULL,
  `MaximumChargeAmt05` int(11) DEFAULT NULL,
  `EstimatedPremiumAmt05` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `aircraftpremendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `allcoderefs`
--

DROP TABLE IF EXISTS `allcoderefs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `allcoderefs` (
  `Package` varchar(255) DEFAULT NULL,
  `Repository` varchar(255) DEFAULT NULL,
  `Coderef` varchar(255) DEFAULT NULL,
  `Key` varchar(255) DEFAULT NULL,
  `Value` varchar(1100) DEFAULT NULL,
  `Label` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `allcontacts`
--

DROP TABLE IF EXISTS `allcontacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `allcontacts` (
  `UniqueId` int(11) NOT NULL AUTO_INCREMENT,
  `SystemId` int(11) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `PartyInfoIdRef` varchar(255) DEFAULT NULL,
  `ContactTypeCd` varchar(255) DEFAULT NULL,
  `SourceIdRef` varchar(255) DEFAULT NULL,
  `SourceTypeCd` varchar(255) DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `CommercialName` varchar(255) DEFAULT NULL,
  `PrimaryPhoneName` varchar(255) DEFAULT NULL,
  `PrimaryPhoneNumber` varchar(255) DEFAULT NULL,
  `SecondaryPhoneName` varchar(255) DEFAULT NULL,
  `SecondaryPhoneNumber` varchar(255) DEFAULT NULL,
  `FaxNumber` varchar(255) DEFAULT NULL,
  `EmailAddr` varchar(255) DEFAULT NULL,
  `MailingAddr1` varchar(255) DEFAULT NULL,
  `MailingAddr2` varchar(255) DEFAULT NULL,
  `MailingCity` varchar(255) DEFAULT NULL,
  `MailingStateProvCd` varchar(255) DEFAULT NULL,
  `MailingPostalCode` varchar(255) DEFAULT NULL,
  `BillingAddr1` varchar(255) DEFAULT NULL,
  `BillingAddr2` varchar(255) DEFAULT NULL,
  `BillingCity` varchar(255) DEFAULT NULL,
  `BillingStateProvCd` varchar(255) DEFAULT NULL,
  `BillingPostalCode` varchar(255) DEFAULT NULL,
  `BestAddr1` varchar(255) DEFAULT NULL,
  `BestAddr2` varchar(255) DEFAULT NULL,
  `BestCity` varchar(255) DEFAULT NULL,
  `BestStateProvCd` varchar(255) DEFAULT NULL,
  `BestPostalCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`UniqueId`),
  KEY `allcontactsIdC` (`SystemId`,`CMMContainer`),
  KEY `allcontactsSc` (`SourceCd`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `allproductlist`
--

DROP TABLE IF EXISTS `allproductlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `allproductlist` (
  `Package` varchar(255) DEFAULT NULL,
  `Product` varchar(255) DEFAULT NULL,
  `ProductVersion` varchar(255) DEFAULT NULL,
  `Coderef` varchar(255) DEFAULT NULL,
  `OptionKey` varchar(255) DEFAULT NULL,
  `Value` text,
  `Name` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `alternateemprendinfo`
--

DROP TABLE IF EXISTS `alternateemprendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alternateemprendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `WorkDescription` varchar(255) DEFAULT NULL,
  `AltEmpName` varchar(255) DEFAULT NULL,
  `AltEmpAddress` varchar(255) DEFAULT NULL,
  `SpecialTempEmpState` varchar(255) DEFAULT NULL,
  `ContractorProjName` varchar(255) DEFAULT NULL,
  `EndorsementSeqNo` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `alternateemprendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `annivratingendinfo`
--

DROP TABLE IF EXISTS `annivratingendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annivratingendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `AnniversaryRatingDt` int(11) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `annivratingendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `annotation`
--

DROP TABLE IF EXISTS `annotation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annotation` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `AddUser` varchar(255) DEFAULT NULL,
  `UpdateDt` date DEFAULT NULL,
  `UpdateTm` varchar(255) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `Note` varchar(255) DEFAULT NULL,
  `CheckedInd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `IconCd` varchar(255) DEFAULT NULL,
  `Top` decimal(28,6) DEFAULT NULL,
  `Bottom` decimal(28,6) DEFAULT NULL,
  `Left` decimal(28,6) DEFAULT NULL,
  `Right` decimal(28,6) DEFAULT NULL,
  `Color` varchar(255) DEFAULT NULL,
  `Opacity` decimal(28,6) DEFAULT NULL,
  `PageNumber` varchar(255) DEFAULT NULL,
  KEY `annotationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `annualstatement`
--

DROP TABLE IF EXISTS `annualstatement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annualstatement` (
  `SystemId` int(11) DEFAULT NULL,
  `ReportPeriod` char(6) DEFAULT NULL,
  `GenerationDt` date DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `AnnualStatementLine` varchar(255) DEFAULT NULL,
  `DirectPremiumWritten` decimal(28,6) DEFAULT NULL,
  `DirectPremiumEarned` decimal(28,6) DEFAULT NULL,
  `DividendsPaid` decimal(28,6) DEFAULT NULL,
  `DirectUnearnedPremium` decimal(28,6) DEFAULT NULL,
  `DirectLossesPaid` decimal(28,6) DEFAULT NULL,
  `DirectLossesIncurred` decimal(28,6) DEFAULT NULL,
  `DirectLossesUnpaid` decimal(28,6) DEFAULT NULL,
  `DirectDefensePaid` decimal(28,6) DEFAULT NULL,
  `DirectDefenseIncurred` decimal(28,6) DEFAULT NULL,
  `DirectDefenseUnpaid` decimal(28,6) DEFAULT NULL,
  `DirectAdjustmentPaid` decimal(28,6) DEFAULT NULL,
  `DirectAdjustmentIncurred` decimal(28,6) DEFAULT NULL,
  `DirectAdjustmentUnpaid` decimal(28,6) DEFAULT NULL,
  `CommissionExpenses` decimal(28,6) DEFAULT NULL,
  `TaxesLicensesFees` decimal(28,6) DEFAULT NULL,
  `BillingServiceCharges` decimal(28,6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application`
--

DROP TABLE IF EXISTS `application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `ApplicationNumber` varchar(255) DEFAULT NULL,
  `Version` varchar(255) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `PolicyRef` int(11) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `ChangeInfoRef` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `LockTaskId` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `ExternalStateData` varchar(255) DEFAULT NULL,
  `VIPLevel` varchar(255) DEFAULT NULL,
  `AuditAccountRef` int(11) DEFAULT NULL,
  `StatementAccountRef` int(11) DEFAULT NULL,
  `PolicyScreenInd` varchar(255) DEFAULT NULL,
  `UnderwritingScreenInd` varchar(255) DEFAULT NULL,
  `LocationsScreenInd` varchar(255) DEFAULT NULL,
  `RisksScreenInd` varchar(255) DEFAULT NULL,
  `PropertyLineScreenInd` varchar(255) DEFAULT NULL,
  `PropertyScreenInd` varchar(255) DEFAULT NULL,
  `LiabilityScreenInd` varchar(255) DEFAULT NULL,
  `LossHistoryScreenInd` varchar(255) DEFAULT NULL,
  `AdditionalInterestsScreenInd` varchar(255) DEFAULT NULL,
  `FormsScreenInd` varchar(255) DEFAULT NULL,
  `ReviewScreenInd` varchar(255) DEFAULT NULL,
  `ReinsuranceScreenInd` varchar(255) DEFAULT NULL,
  `CommissionScreenInd` varchar(255) DEFAULT NULL,
  `PremiumInfoScreenInd` varchar(255) DEFAULT NULL,
  `WorksheetsScreenInd` varchar(255) DEFAULT NULL,
  `SpecialOptionsScreenInd` varchar(255) DEFAULT NULL,
  `CloseoutScreenInd` varchar(255) DEFAULT NULL,
  KEY `applicationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `applicationbaseline`
--

DROP TABLE IF EXISTS `applicationbaseline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicationbaseline` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `Version` varchar(255) DEFAULT NULL,
  `SubmissionApplicationRef` varchar(255) DEFAULT NULL,
  KEY `applicationbaselineIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `applicationinfo`
--

DROP TABLE IF EXISTS `applicationinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicationinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `NeedByDt` date DEFAULT NULL,
  `SubmittedDt` date DEFAULT NULL,
  `FirstMailedDt` date DEFAULT NULL,
  `LastMailedDt` date DEFAULT NULL,
  `CloseReasonCd` varchar(255) DEFAULT NULL,
  `CloseSubReasonCd` varchar(255) DEFAULT NULL,
  `CloseDt` date DEFAULT NULL,
  `CloseSubReasonLabel` varchar(255) DEFAULT NULL,
  `CloseTm` varchar(255) DEFAULT NULL,
  `CloseUser` varchar(255) DEFAULT NULL,
  `CloseComment` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `AddUser` varchar(255) DEFAULT NULL,
  `UpdateDt` date DEFAULT NULL,
  `UpdateTm` varchar(255) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `MasterQuoteRef` int(11) DEFAULT NULL,
  `IterationDescription` varchar(255) DEFAULT NULL,
  `RenewalApplyInd` varchar(255) DEFAULT NULL,
  KEY `applicationinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aractivity`
--

DROP TABLE IF EXISTS `aractivity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aractivity` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ARTransIdRef` varchar(255) DEFAULT NULL,
  `CategoryCd` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `Reference` varchar(255) DEFAULT NULL,
  `ActivityAmt` decimal(28,6) DEFAULT NULL,
  `ActivityCommissionAmt` decimal(28,6) DEFAULT NULL,
  `CommissionTakenAmt` varchar(255) DEFAULT NULL,
  KEY `aractivityIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aradjustment`
--

DROP TABLE IF EXISTS `aradjustment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aradjustment` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CategoryCd` varchar(255) DEFAULT NULL,
  `Amount` decimal(28,6) DEFAULT NULL,
  `AdjustmentTypeCd` decimal(28,6) DEFAULT NULL,
  `CommissionAmt` decimal(28,6) DEFAULT NULL,
  KEY `aradjustmentIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `arapply`
--

DROP TABLE IF EXISTS `arapply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arapply` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CategoryCd` varchar(255) DEFAULT NULL,
  `Amount` decimal(28,6) DEFAULT NULL,
  `Rate` decimal(28,6) DEFAULT NULL,
  `Scale` int(11) DEFAULT NULL,
  `CommissionAmt` decimal(28,6) DEFAULT NULL,
  `ApplyTo` varchar(255) DEFAULT NULL,
  KEY `arapplyIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `arbalance`
--

DROP TABLE IF EXISTS `arbalance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arbalance` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CategoryCd` varchar(255) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `AdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `TotalAmt` decimal(28,6) DEFAULT NULL,
  `NetAmt` decimal(28,6) DEFAULT NULL,
  `BillAmt` decimal(28,6) DEFAULT NULL,
  `OpenAmt` decimal(28,6) DEFAULT NULL,
  `NetBillAmt` decimal(28,6) DEFAULT NULL,
  `NetOpenAmt` decimal(28,6) DEFAULT NULL,
  KEY `arbalanceIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `arcategory`
--

DROP TABLE IF EXISTS `arcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arcategory` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CategoryCd` varchar(255) DEFAULT NULL,
  `RecipientCd` varchar(255) DEFAULT NULL,
  `Amount` decimal(28,6) DEFAULT NULL,
  `EarnCd` varchar(255) DEFAULT NULL,
  `ApplyTo` varchar(255) DEFAULT NULL,
  `Rate` decimal(28,6) DEFAULT NULL,
  `Scale` int(11) DEFAULT NULL,
  `PostPriority` varchar(255) DEFAULT NULL,
  `Source` varchar(255) DEFAULT NULL,
  `RebuildInd` varchar(255) DEFAULT NULL,
  KEY `arcategoryIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `arearn`
--

DROP TABLE IF EXISTS `arearn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arearn` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Reference` varchar(255) DEFAULT NULL,
  `EarnRate` decimal(28,6) DEFAULT NULL,
  `EarnRateStartDt` date DEFAULT NULL,
  `EarnRateEndDt` date DEFAULT NULL,
  `EarnDays` int(11) DEFAULT NULL,
  `EarnAmt` decimal(28,6) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  KEY `arearnIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `arinvoice`
--

DROP TABLE IF EXISTS `arinvoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arinvoice` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ARTransIdRef` varchar(255) DEFAULT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `InvoiceDt` date DEFAULT NULL,
  `DueDt` date DEFAULT NULL,
  `Desc` varchar(255) DEFAULT NULL,
  `TotalAmt` decimal(28,6) DEFAULT NULL,
  `OpenAmt` decimal(28,6) DEFAULT NULL,
  `OpenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `OpenFeeAmt` decimal(28,6) DEFAULT NULL,
  `PreviousAmt` decimal(28,6) DEFAULT NULL,
  `RevisedInd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `Reference` varchar(255) DEFAULT NULL,
  `InterimInd` varchar(255) DEFAULT NULL,
  `NetTotalAmt` decimal(28,6) DEFAULT NULL,
  `NetOpenAmt` decimal(28,6) DEFAULT NULL,
  `NetOpenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `NetOpenFeeAmt` decimal(28,6) DEFAULT NULL,
  `PrintedInd` varchar(255) DEFAULT NULL,
  `StatementAccountInvoiceNumber` varchar(255) DEFAULT NULL,
  `ARTransactionOutput` varchar(255) DEFAULT NULL,
  `ForceInvoiceInd` varchar(255) DEFAULT NULL,
  KEY `arinvoiceIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aritem`
--

DROP TABLE IF EXISTS `aritem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aritem` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ARTransIdRef` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `CategoryCd` varchar(255) DEFAULT NULL,
  `Desc` varchar(255) DEFAULT NULL,
  `CancelInd` varchar(255) DEFAULT NULL,
  `HoldInd` varchar(255) DEFAULT NULL,
  `RollDays` varchar(255) DEFAULT NULL,
  `RollDt` date DEFAULT NULL,
  `BillDt` date DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `AdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `TotalAmt` decimal(28,6) DEFAULT NULL,
  `NetAmt` decimal(28,6) DEFAULT NULL,
  `PostPriority` varchar(255) DEFAULT NULL,
  `BillAmt` decimal(28,6) DEFAULT NULL,
  `OpenAmt` decimal(28,6) DEFAULT NULL,
  `CommissionPct` decimal(28,6) DEFAULT NULL,
  `CommissionAmt` decimal(28,6) DEFAULT NULL,
  `NetBillAmt` decimal(28,6) DEFAULT NULL,
  `NetOpenAmt` decimal(28,6) DEFAULT NULL,
  `NetPaidAmt` decimal(28,6) DEFAULT NULL,
  `NetAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `CommissionTakenAmt` varchar(255) DEFAULT NULL,
  KEY `aritemIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aroutput`
--

DROP TABLE IF EXISTS `aroutput`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aroutput` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `OutputTemplateIdRef` varchar(255) DEFAULT NULL,
  `FormTemplateIdRef` varchar(255) DEFAULT NULL,
  KEY `aroutputIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aroutputrecipient`
--

DROP TABLE IF EXISTS `aroutputrecipient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aroutputrecipient` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Recipient` varchar(255) DEFAULT NULL,
  KEY `aroutputrecipientIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aroutputs`
--

DROP TABLE IF EXISTS `aroutputs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aroutputs` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `aroutputsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `arpart`
--

DROP TABLE IF EXISTS `arpart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arpart` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ARTransIdRef` varchar(255) DEFAULT NULL,
  `CategoryCd` varchar(255) DEFAULT NULL,
  KEY `arpartIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `arpaymentmethod`
--

DROP TABLE IF EXISTS `arpaymentmethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arpaymentmethod` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `MethodCd` varchar(255) DEFAULT NULL,
  `Amount` decimal(28,6) DEFAULT NULL,
  `ApplyTo` varchar(255) DEFAULT NULL,
  `MinAmountToReinstate` decimal(28,6) DEFAULT NULL,
  KEY `arpaymentmethodIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `arpayplan`
--

DROP TABLE IF EXISTS `arpayplan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arpayplan` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `BillMethod` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `LegalModeCd` varchar(255) DEFAULT NULL,
  `CancelModeCd` varchar(255) DEFAULT NULL,
  `PolicyChangeModeCd` varchar(255) DEFAULT NULL,
  `PayPlanCd` varchar(255) DEFAULT NULL,
  `PayPlanTypeCd` varchar(255) DEFAULT NULL,
  `PayPlan` varchar(255) DEFAULT NULL,
  `PayPlanItems` int(11) DEFAULT NULL,
  `StatementTypeCd` varchar(255) DEFAULT NULL,
  `InstallmentFee` decimal(28,6) DEFAULT NULL,
  `PolicyFee` decimal(28,6) DEFAULT NULL,
  `TaxRate` decimal(28,6) DEFAULT NULL,
  `DueDayTypeCd` varchar(255) DEFAULT NULL,
  `DueDays` int(11) DEFAULT NULL,
  `InstallmentDueDay` int(11) DEFAULT NULL,
  `PolicyChangeDueDays` int(11) DEFAULT NULL,
  `LegalDays` int(11) DEFAULT NULL,
  `CancelDays` int(11) DEFAULT NULL,
  `PremiumWaiverCreditAmt` decimal(28,6) DEFAULT NULL,
  `PremiumWaiverDebitAmt` decimal(28,6) DEFAULT NULL,
  `AlwaysRollFirstInd` varchar(255) DEFAULT NULL,
  `CarryBalanceForwardInd` varchar(255) DEFAULT NULL,
  `CarryBalanceForwardSpreadCd` varchar(255) DEFAULT NULL,
  `RenewalActivateThresholdPctg` decimal(28,6) DEFAULT NULL,
  `MinimumForInstallmentAmt` decimal(28,6) DEFAULT NULL,
  `MinimumForInstallmentDays` int(11) DEFAULT NULL,
  `MinimumBillAmt` decimal(28,6) DEFAULT NULL,
  `MinimumDaysBeforeDueDate` int(11) DEFAULT NULL,
  `PremiumInCycleActionCd` varchar(255) DEFAULT NULL,
  `RevisedInvoiceReturnPremiumPct` varchar(255) DEFAULT NULL,
  `RevisedInvoiceAddlPremiumPct` varchar(255) DEFAULT NULL,
  `AutoReinstatementThresholdDays` varchar(255) DEFAULT NULL,
  `AutoReinstatementTaskIdRef` varchar(255) DEFAULT NULL,
  `ManualReinstatementTaskIdRef` varchar(255) DEFAULT NULL,
  `CancelEffectiveCarryDtInd` varchar(255) DEFAULT NULL,
  `CancelNoticeTaskIdRef` varchar(255) DEFAULT NULL,
  `RenewalActivateTaskIdRef` varchar(255) DEFAULT NULL,
  `RevisedInvoiceMinDaysBeforeDue` varchar(255) DEFAULT NULL,
  `MethodCd` varchar(255) DEFAULT NULL,
  `DelayRollDays` varchar(255) DEFAULT NULL,
  `AutomaticReinThresholdPct` varchar(255) DEFAULT NULL,
  `EquityLegalDays` int(11) DEFAULT NULL,
  `EquityCancelDays` int(11) DEFAULT NULL,
  `PolicyChangeLegalDays` int(11) DEFAULT NULL,
  `PolicyChangeCancelDays` int(11) DEFAULT NULL,
  `FeeSpreadModeCd` varchar(255) DEFAULT NULL,
  `ReminderNoticeMaxDays` int(11) DEFAULT NULL,
  `ReminderNoticeMinDays` int(11) DEFAULT NULL,
  `PolicyChgReminderNoticeMaxDays` int(11) DEFAULT NULL,
  `PolicyChgReminderNoticeMinDays` int(11) DEFAULT NULL,
  `ReinWithLapseThreshholdDays` varchar(255) DEFAULT NULL,
  `ReinWithLapseTaskIdRef` varchar(255) DEFAULT NULL,
  `MaxACHThresholdDays` int(11) DEFAULT NULL,
  `NotEnoughTimeToCXNoteTaskIdRef` varchar(255) DEFAULT NULL,
  `StatementAccountInd` varchar(255) DEFAULT NULL,
  `ARCyclePriority` varchar(255) DEFAULT NULL,
  `ScheduleText` varchar(255) DEFAULT NULL,
  `ARScheduleRule` varchar(255) DEFAULT NULL,
  KEY `arpayplanIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `arpolicy`
--

DROP TABLE IF EXISTS `arpolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arpolicy` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PolicyRef` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyDisplayNumber` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `ProviderCode` varchar(255) DEFAULT NULL,
  `ProviderName` varchar(255) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `LineCd` varchar(255) DEFAULT NULL,
  `TransactionEffectiveDt` date DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `TransactionReasonCd` varchar(255) DEFAULT NULL,
  `DownPaymentAmt` decimal(28,6) DEFAULT NULL,
  `WrittenAmt` decimal(28,6) DEFAULT NULL,
  `FullTermAmt` decimal(28,6) DEFAULT NULL,
  `FinalPremiumAmt` decimal(28,6) DEFAULT NULL,
  `TotalWrittenAmt` decimal(28,6) DEFAULT NULL,
  `OpenAmt` decimal(28,6) DEFAULT NULL,
  `TotalInstallments` int(11) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `PaymentDay` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  KEY `arpolicyIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `arreceipt`
--

DROP TABLE IF EXISTS `arreceipt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arreceipt` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ReceiptId` varchar(255) DEFAULT NULL,
  `ARTransIdRef` varchar(255) DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `SourceRef` varchar(255) DEFAULT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  `PolicyEffectiveDt` date DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `Reference` varchar(255) DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `ReceiptDt` date DEFAULT NULL,
  `ReceiptAmt` varchar(255) DEFAULT NULL,
  `Desc` varchar(255) DEFAULT NULL,
  `Memo` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `ReceiptTypeCd` varchar(255) DEFAULT NULL,
  `CheckAmt` decimal(28,6) DEFAULT NULL,
  `PayBy` varchar(255) DEFAULT NULL,
  `AccountingDt` date DEFAULT NULL,
  `MethodCd` varchar(255) DEFAULT NULL,
  `DepositoryLocationCd` varchar(255) DEFAULT NULL,
  `GrossNetInd` varchar(255) DEFAULT NULL,
  `CommissionTakenAmt` varchar(255) DEFAULT NULL,
  `ARSystemReceiptReference` varchar(255) DEFAULT NULL,
  `ACHAgent` varchar(255) DEFAULT NULL,
  `PaymentOnFileId` varchar(255) DEFAULT NULL,
  `UnpostedReceiptTypeCd` varchar(255) DEFAULT NULL,
  KEY `arreceiptIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `arrule`
--

DROP TABLE IF EXISTS `arrule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arrule` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Expression` varchar(255) DEFAULT NULL,
  KEY `arruleIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `arschedule`
--

DROP TABLE IF EXISTS `arschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arschedule` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ARTransIdRef` varchar(255) DEFAULT NULL,
  `Number` varchar(255) DEFAULT NULL,
  `Desc` varchar(255) DEFAULT NULL,
  `CategoryCd` varchar(255) DEFAULT NULL,
  `HoldInd` varchar(255) DEFAULT NULL,
  `RollDays` int(11) DEFAULT NULL,
  `DueDays` int(11) DEFAULT NULL,
  `LeadDays` int(11) DEFAULT NULL,
  `RollDt` date DEFAULT NULL,
  `DueDt` date DEFAULT NULL,
  `Basis` varchar(255) DEFAULT NULL,
  `Percentage` decimal(28,6) DEFAULT NULL,
  `Amount` decimal(28,6) DEFAULT NULL,
  `GrossAmt` decimal(28,6) DEFAULT NULL,
  `CommissionAmt` decimal(28,6) DEFAULT NULL,
  `NetAmt` decimal(28,6) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `RollMonths` int(11) DEFAULT NULL,
  `LegalDays` int(11) DEFAULT NULL,
  `CancelDays` int(11) DEFAULT NULL,
  `ReminderNoticeMaxDays` int(11) DEFAULT NULL,
  `ReminderNoticeMinDays` int(11) DEFAULT NULL,
  KEY `arscheduleIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `artrans`
--

DROP TABLE IF EXISTS `artrans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artrans` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `CycleCd` varchar(255) DEFAULT NULL,
  `ARPolicyIdRef` varchar(255) DEFAULT NULL,
  `Reference` varchar(255) DEFAULT NULL,
  `ARReceiptTypeCd` varchar(255) DEFAULT NULL,
  `ARReceiptReference` varchar(255) DEFAULT NULL,
  `ARReceiptDt` date DEFAULT NULL,
  `ARReceiptAmt` decimal(28,6) DEFAULT NULL,
  `ARReceiptId` varchar(255) DEFAULT NULL,
  `ARReceiptStatusCd` varchar(255) DEFAULT NULL,
  `ARReceiptMemo` varchar(255) DEFAULT NULL,
  `Desc` varchar(255) DEFAULT NULL,
  `ReasonCd` varchar(255) DEFAULT NULL,
  `ReasonDesc` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `TransactionReasonCd` varchar(255) DEFAULT NULL,
  `TransactionUserId` varchar(255) DEFAULT NULL,
  `TransactionDt` date DEFAULT NULL,
  `TransactionTm` varchar(255) DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `RequestedAmt` decimal(28,6) DEFAULT NULL,
  `AdjustmentTypeCd` varchar(255) DEFAULT NULL,
  `AdjustmentCategoryCd` varchar(255) DEFAULT NULL,
  `Amount` decimal(28,6) DEFAULT NULL,
  `InstallmentNumber` varchar(255) DEFAULT NULL,
  `DueDt` date DEFAULT NULL,
  `DueAmt` decimal(28,6) DEFAULT NULL,
  `TotalAmt` decimal(28,6) DEFAULT NULL,
  `SpreadAmountInd` varchar(255) DEFAULT NULL,
  `ReInvoiceInd` varchar(255) DEFAULT NULL,
  `ForceRollInd` varchar(255) DEFAULT NULL,
  `ReversableInd` varchar(255) DEFAULT NULL,
  `SystemReversableInd` varchar(255) DEFAULT NULL,
  `ARTransIdRef` varchar(255) DEFAULT NULL,
  `EarnChangeInd` varchar(255) DEFAULT NULL,
  `EarnRate` decimal(28,6) DEFAULT NULL,
  `EarnRateStartDt` date DEFAULT NULL,
  `ChangeWrittenAmt` decimal(28,6) DEFAULT NULL,
  `ChangeCommissionAmt` decimal(28,6) DEFAULT NULL,
  `ChangeNetAmt` decimal(28,6) DEFAULT NULL,
  `ChangeInfoRef` varchar(255) DEFAULT NULL,
  `CheckAmt` decimal(28,6) DEFAULT NULL,
  `PayBy` varchar(255) DEFAULT NULL,
  `AccountingDt` date DEFAULT NULL,
  `DelayInvoiceInd` varchar(255) DEFAULT NULL,
  `DepositoryLocationCd` varchar(255) DEFAULT NULL,
  `NetAmt` decimal(28,6) DEFAULT NULL,
  `NetOpenAmt` decimal(28,6) DEFAULT NULL,
  `NetTotalAmt` decimal(28,6) DEFAULT NULL,
  `GrossNetInd` varchar(255) DEFAULT NULL,
  `CommissionTakenAmt` varchar(255) DEFAULT NULL,
  `CommissionAmt` varchar(255) DEFAULT NULL,
  `PayPlanChangeInd` varchar(255) DEFAULT NULL,
  `RequestElectronicPaymentInd` varchar(255) DEFAULT NULL,
  `ConversionTemplateIdRef` varchar(255) DEFAULT NULL,
  `ConversionGroup` varchar(255) DEFAULT NULL,
  `ConversionJobRef` varchar(255) DEFAULT NULL,
  `ARSystemReceiptReference` varchar(255) DEFAULT NULL,
  `MasterCarryDt` date DEFAULT NULL,
  `ReallocationInd` varchar(255) DEFAULT NULL,
  `ReallocationFeeInd` varchar(255) DEFAULT NULL,
  `ACHAgent` varchar(255) DEFAULT NULL,
  `SettlementDt` date DEFAULT NULL,
  `UseReceiptAmt` varchar(255) DEFAULT NULL,
  `ForceInvoiceInd` varchar(255) DEFAULT NULL,
  `CategoryCd` varchar(255) DEFAULT NULL,
  KEY `artransIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `artransactionoutput`
--

DROP TABLE IF EXISTS `artransactionoutput`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artransactionoutput` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  KEY `artransactionoutputIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assignedprovider`
--

DROP TABLE IF EXISTS `assignedprovider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignedprovider` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AssignedProviderTypeCd` varchar(255) DEFAULT NULL,
  `ProviderRef` int(11) DEFAULT NULL,
  `ProviderTypeCd` varchar(255) DEFAULT NULL,
  KEY `assignedproviderIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `attachment`
--

DROP TABLE IF EXISTS `attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachment` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `IdRef` varchar(255) DEFAULT NULL,
  `TemplateId` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Filename` varchar(255) DEFAULT NULL,
  `OriginalFilename` varchar(255) DEFAULT NULL,
  `Memo` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `AddUser` varchar(255) DEFAULT NULL,
  `UpdateDt` date DEFAULT NULL,
  `UpdateTm` varchar(255) DEFAULT NULL,
  `HasAnnotationsInd` varchar(255) DEFAULT NULL,
  `TransactionNumber` varchar(255) DEFAULT NULL,
  `SourceRef` varchar(255) DEFAULT NULL,
  `SourceContainerName` varchar(255) DEFAULT NULL,
  KEY `attachmentIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `attachmentkey`
--

DROP TABLE IF EXISTS `attachmentkey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachmentkey` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `IdRef` varchar(255) DEFAULT NULL,
  `Keyword` varchar(255) DEFAULT NULL,
  KEY `attachmentkeyIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `attachmenttemplateref`
--

DROP TABLE IF EXISTS `attachmenttemplateref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachmenttemplateref` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TemplateIdRef` varchar(255) DEFAULT NULL,
  KEY `attachmenttemplaterefIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auditdata`
--

DROP TABLE IF EXISTS `auditdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auditdata` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TransactionNumber` varchar(255) DEFAULT NULL,
  `AuditSource` varchar(255) DEFAULT NULL,
  `RevisedInd` varchar(255) DEFAULT NULL,
  `ApplyToRenewalInd` varchar(255) DEFAULT NULL,
  `RaterTermLength` varchar(255) DEFAULT NULL,
  KEY `auditdataIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auditdataitem`
--

DROP TABLE IF EXISTS `auditdataitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auditdataitem` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Key` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `ActualAmt` decimal(28,6) DEFAULT NULL,
  `EstimateAmt` decimal(28,6) DEFAULT NULL,
  `AnnualizedActualAmt` decimal(28,6) DEFAULT NULL,
  `IdRef` varchar(255) DEFAULT NULL,
  `PriorActualAmt` decimal(28,6) DEFAULT NULL,
  `Exposure` decimal(28,6) DEFAULT NULL,
  `ExposureBasis` varchar(255) DEFAULT NULL,
  `LocationNumber` varchar(255) DEFAULT NULL,
  `BuildingNumber` varchar(255) DEFAULT NULL,
  `ManualProcessInd` varchar(255) DEFAULT NULL,
  KEY `auditdataitemIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auditdataset`
--

DROP TABLE IF EXISTS `auditdataset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auditdataset` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `auditdatasetIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auditpayplan`
--

DROP TABLE IF EXISTS `auditpayplan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auditpayplan` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AuditPayPlanCd` varchar(255) DEFAULT NULL,
  `PaymentDay` varchar(255) DEFAULT NULL,
  `PayPlanTemplateIdRef` varchar(255) DEFAULT NULL,
  KEY `auditpayplanIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bankaccount`
--

DROP TABLE IF EXISTS `bankaccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bankaccount` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `AccountCd` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `AccountTypeCd` varchar(255) DEFAULT NULL,
  `APAccountCode` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `StatusDt` date DEFAULT NULL,
  `BankRoutingNumber` varchar(255) DEFAULT NULL,
  `NextCheckNumber` int(11) DEFAULT NULL,
  `CheckNumberFormat` varchar(255) DEFAULT NULL,
  `ChecksPerPage` int(11) DEFAULT NULL,
  `CheckOriginalMICR` varchar(255) DEFAULT NULL,
  `CheckCopyMICR` varchar(255) DEFAULT NULL,
  `CheckOriginalFormTemplateIdRef` varchar(255) DEFAULT NULL,
  `CheckCopyFormTemplateIdRef` varchar(255) DEFAULT NULL,
  `CheckCopyPrinterTemplateIdRef` varchar(255) DEFAULT NULL,
  `CheckAcctFormTemplateIdRef` varchar(255) DEFAULT NULL,
  `CheckAccountingMICR` varchar(255) DEFAULT NULL,
  `CheckAcctPrinterTemplateIdRef` varchar(255) DEFAULT NULL,
  `CheckInquiryFormTemplateIdRef` varchar(255) DEFAULT NULL,
  `BatchPrintLockInd` varchar(255) DEFAULT NULL,
  `CategoryRuleMDA` varchar(255) DEFAULT NULL,
  `BatchPrintLockDt` date DEFAULT NULL,
  `BatchPrintLockUser` varchar(255) DEFAULT NULL,
  `BatchPrintLockTm` varchar(255) DEFAULT NULL,
  `Version` varchar(255) DEFAULT NULL,
  KEY `bankaccountIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `basicpolicy`
--

DROP TABLE IF EXISTS `basicpolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `basicpolicy` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PolicyDisplayNumber` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `CompanyProductCd` varchar(255) DEFAULT NULL,
  `PolicyProductName` varchar(255) DEFAULT NULL,
  `ProductTypeCd` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `ControllingStateCd` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `InceptionDt` date DEFAULT NULL,
  `InceptionTm` varchar(255) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `EffectiveTm` varchar(255) DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `ChangeDt` date DEFAULT NULL,
  `CancelDt` date DEFAULT NULL,
  `ReinstateDt` date DEFAULT NULL,
  `RatedInd` varchar(255) DEFAULT NULL,
  `SubProducerCd` varchar(255) DEFAULT NULL,
  `UnderwriterCd` varchar(255) DEFAULT NULL,
  `Commission` decimal(28,6) DEFAULT NULL,
  `SICCode` varchar(255) DEFAULT NULL,
  `PayPlanCd` varchar(255) DEFAULT NULL,
  `TerritoryCd` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `TransactionStatus` varchar(255) DEFAULT NULL,
  `TransactionCommissionAmt` decimal(28,6) DEFAULT NULL,
  `ProtectionClass` varchar(255) DEFAULT NULL,
  `InstallmentPlanCd` varchar(255) DEFAULT NULL,
  `RenewalPolicyRef` int(11) DEFAULT NULL,
  `RenewedFromPolicyRef` int(11) DEFAULT NULL,
  `RenewalTermCd` varchar(255) DEFAULT NULL,
  `NonRenewInd` varchar(255) DEFAULT NULL,
  `RewritePolicyRef` int(11) DEFAULT NULL,
  `RewriteFromPolicyRef` int(11) DEFAULT NULL,
  `PolicyNumberPrefix` varchar(255) DEFAULT NULL,
  `PolicyNumberSuffix` varchar(255) DEFAULT NULL,
  `ExpiringPremiumAmt` decimal(28,6) DEFAULT NULL,
  `ExpiringFeeAmt` decimal(28,6) DEFAULT NULL,
  `Branch` varchar(255) DEFAULT NULL,
  `BusinessSourceCd` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `PreviousCarrierCd` varchar(255) DEFAULT NULL,
  `PreviousPolicyNumber` varchar(255) DEFAULT NULL,
  `PreviousExpirationDt` date DEFAULT NULL,
  `PreviousPremium` decimal(28,6) DEFAULT NULL,
  `StatsRef` varchar(255) DEFAULT NULL,
  `QuoteNumber` varchar(255) DEFAULT NULL,
  `QuoteNumberLookup` varchar(255) DEFAULT NULL,
  `OriginalApplicationRef` varchar(255) DEFAULT NULL,
  `UnderwritingHoldInd` varchar(255) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `FullTermAmt` decimal(28,6) DEFAULT NULL,
  `CommissionAmt` decimal(28,6) DEFAULT NULL,
  `WrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `FinalPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SubTypeCd` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `ManualReinstateInd` varchar(255) DEFAULT NULL,
  `ManualRenewalInd` varchar(255) DEFAULT NULL,
  `ManualRenewalReason` varchar(255) DEFAULT NULL,
  `ProviderRef` int(11) DEFAULT NULL,
  `RenewalProviderRef` int(11) DEFAULT NULL,
  `PriorTermPendingChgPolicyRef` int(11) DEFAULT NULL,
  `PriorTermPendingChgTransNum` int(11) DEFAULT NULL,
  `RenewalSubProducerCd` varchar(255) DEFAULT NULL,
  `ExternalId` varchar(255) DEFAULT NULL,
  `WrittenFeeAmt` decimal(28,6) DEFAULT NULL,
  `WrittenCommissionFeeAmt` decimal(28,6) DEFAULT NULL,
  `PaymentDay` varchar(255) DEFAULT NULL,
  `ReplacedByPolicyRef` int(11) DEFAULT NULL,
  `ManualReinstateReason` varchar(255) DEFAULT NULL,
  `DisplayDescription` varchar(255) DEFAULT NULL,
  `PremiumDiscountTableTypeCd` varchar(255) DEFAULT NULL,
  `ShellPolicyInd` varchar(255) DEFAULT NULL,
  `DividendFormNumber` varchar(255) DEFAULT NULL,
  `DividendPlanCd` varchar(255) DEFAULT NULL,
  `DividendPlanDisqualifyInd` varchar(255) DEFAULT NULL,
  `DividendPlanDisqualifyReason` varchar(255) DEFAULT NULL,
  `DividendPlanDisqualifyComment` varchar(255) DEFAULT NULL,
  `ManualBillingEntitySplitInd` varchar(255) DEFAULT NULL,
  `ProgramCd` varchar(255) DEFAULT NULL,
  `AffinityGroupCd` varchar(255) DEFAULT NULL,
  `PromotionCd` varchar(255) DEFAULT NULL,
  `DirectorsAndOfficersLineInd` varchar(255) DEFAULT NULL,
  `ErrorsAndOmissionLineInd` varchar(255) DEFAULT NULL,
  `EmploymentAndPracticeLineInd` varchar(255) DEFAULT NULL,
  `CLUEPropertyRequestInd` varchar(255) DEFAULT NULL,
  `CLUEPersonalAutoRequestInd` varchar(255) DEFAULT NULL,
  `AnniversaryRatingDay` varchar(255) DEFAULT NULL,
  `ARDRuleEnabled` varchar(255) DEFAULT NULL,
  `BankAccountNumber` varchar(255) DEFAULT NULL,
  `BankAccountTypeCd` varchar(255) DEFAULT NULL,
  `RoutingNumber` varchar(255) DEFAULT NULL,
  KEY `basicpolicyIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `batchprint`
--

DROP TABLE IF EXISTS `batchprint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batchprint` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `PrintDt` date DEFAULT NULL,
  `PrintUser` varchar(255) DEFAULT NULL,
  `PrintTm` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  KEY `batchprintIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `batchprintdocument`
--

DROP TABLE IF EXISTS `batchprintdocument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batchprintdocument` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `OutputNumber` varchar(255) DEFAULT NULL,
  `OutputName` varchar(255) DEFAULT NULL,
  `RecipientTypeCd` varchar(255) DEFAULT NULL,
  `RecipientName` varchar(255) DEFAULT NULL,
  `RecipientCd` varchar(255) DEFAULT NULL,
  `MailProofCd` varchar(255) DEFAULT NULL,
  `CoversheetFormTemplateIdRef` varchar(255) DEFAULT NULL,
  `MergeFileURL` varchar(255) DEFAULT NULL,
  `MDAContainerPackage` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `AddUser` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `NumSimplexPrintPages` int(11) DEFAULT NULL,
  `NumDuplexPrintPages` int(11) DEFAULT NULL,
  `CategoryCd` varchar(255) DEFAULT NULL,
  `DocumentTypeCd` varchar(255) DEFAULT NULL,
  `PrintEngineCd` varchar(255) DEFAULT NULL,
  KEY `batchprintdocumentIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `batchreceipt`
--

DROP TABLE IF EXISTS `batchreceipt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batchreceipt` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `BatchId` varchar(255) DEFAULT NULL,
  `SourceName` varchar(255) DEFAULT NULL,
  `AppliedUserId` varchar(255) DEFAULT NULL,
  `AppliedDt` date DEFAULT NULL,
  `TotalAmt` decimal(28,6) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `FileName` varchar(255) DEFAULT NULL,
  `UserId` varchar(255) DEFAULT NULL,
  `AllowDeleteInd` varchar(255) DEFAULT NULL,
  `AllowAddInd` varchar(255) DEFAULT NULL,
  `AutomatedProcessingInd` varchar(255) DEFAULT NULL,
  `DepositoryLocationCd` varchar(255) DEFAULT NULL,
  `ScheduledPaymentDt` date DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  KEY `batchreceiptIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `batchreturn`
--

DROP TABLE IF EXISTS `batchreturn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batchreturn` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `BatchId` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `AddUser` varchar(255) DEFAULT NULL,
  KEY `batchreturnIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `batchreturndetail`
--

DROP TABLE IF EXISTS `batchreturndetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batchreturndetail` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `IncludeInd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `AccountRef` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  `RefundAmt` decimal(28,6) DEFAULT NULL,
  `PayTo` varchar(255) DEFAULT NULL,
  `AgeOfCredit` varchar(255) DEFAULT NULL,
  `ReasonDesc` varchar(255) DEFAULT NULL,
  `TotalAmt` decimal(28,6) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `CheckNumber` varchar(255) DEFAULT NULL,
  `CheckDate` date DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `ARTransIdRef` varchar(255) DEFAULT NULL,
  `CreditAmt` decimal(28,6) DEFAULT NULL,
  `NetCreditAmt` decimal(28,6) DEFAULT NULL,
  `GrossNetInd` varchar(255) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `OverrideAmountInd` varchar(255) DEFAULT NULL,
  `RefundMethodCd` varchar(255) DEFAULT NULL,
  `StatementAccountRef` int(11) DEFAULT NULL,
  KEY `batchreturndetailIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `batchtransferdetail`
--

DROP TABLE IF EXISTS `batchtransferdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batchtransferdetail` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `IncludeInd` varchar(255) DEFAULT NULL,
  `AccountRef` varchar(255) DEFAULT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  `TotalAmt` decimal(28,6) DEFAULT NULL,
  `TransferAmt` decimal(28,6) DEFAULT NULL,
  `ARTransIdRef` varchar(255) DEFAULT NULL,
  `FromAccountRef` varchar(255) DEFAULT NULL,
  `FromAccountNumber` varchar(255) DEFAULT NULL,
  `FromARTransIdRef` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `UseReceiptAmt` varchar(255) DEFAULT NULL,
  KEY `batchtransferdetailIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `batchwriteoff`
--

DROP TABLE IF EXISTS `batchwriteoff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batchwriteoff` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `BatchId` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `AddUser` varchar(255) DEFAULT NULL,
  KEY `batchwriteoffIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `batchwriteoffdetail`
--

DROP TABLE IF EXISTS `batchwriteoffdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batchwriteoffdetail` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `IncludeInd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `AccountRef` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  `WriteOffAmt` decimal(28,6) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `ARTransIdRef` varchar(255) DEFAULT NULL,
  KEY `batchwriteoffdetailIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `beanstats`
--

DROP TABLE IF EXISTS `beanstats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `beanstats` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `BeanNewUserIdRef` varchar(255) DEFAULT NULL,
  `BeanNewDt` date DEFAULT NULL,
  `BeanNewTm` varchar(255) DEFAULT NULL,
  `BeanAddUserIdRef` varchar(255) DEFAULT NULL,
  `BeanAddDt` date DEFAULT NULL,
  `BeanAddTm` varchar(255) DEFAULT NULL,
  `BeanUpdateUserIdRef` varchar(255) DEFAULT NULL,
  `BeanUpdateDt` date DEFAULT NULL,
  `BeanUpdateTm` varchar(255) DEFAULT NULL,
  `BeanLockSessionIdRef` varchar(255) DEFAULT NULL,
  `BeanLockUserIdRef` varchar(255) DEFAULT NULL,
  `BeanLockDt` date DEFAULT NULL,
  `BeanLockTm` varchar(255) DEFAULT NULL,
  KEY `beanstatsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `benefitoccurance`
--

DROP TABLE IF EXISTS `benefitoccurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `benefitoccurance` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `BenefitTypeCd` varchar(255) DEFAULT NULL,
  `MaintenanceTypeCd` varchar(255) DEFAULT NULL,
  `GrossWeeklyAmt` varchar(255) DEFAULT NULL,
  `GrossWeeklyAmtEffectiveDt` varchar(255) DEFAULT NULL,
  `NetWeeklyAmt` varchar(255) DEFAULT NULL,
  `NetWeeklyAmtEffectiveDt` varchar(255) DEFAULT NULL,
  `BenefitPeriod` varchar(255) DEFAULT NULL,
  `BenefitPeriodThroughDt` varchar(255) DEFAULT NULL,
  `BenefitTypeClaimWeeks` varchar(255) DEFAULT NULL,
  `BenefitTypeClaimDays` varchar(255) DEFAULT NULL,
  `BenefitTypeAmtPaid` varchar(255) DEFAULT NULL,
  `BenefitPaymentIssueDt` varchar(255) DEFAULT NULL,
  KEY `benefitoccuranceIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `billingentity`
--

DROP TABLE IF EXISTS `billingentity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billingentity` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `SequenceNumber` varchar(255) DEFAULT NULL,
  `SourceIdRef` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `PreferredDeliveryMethod` varchar(255) DEFAULT NULL,
  `PayPlanCd` varchar(255) DEFAULT NULL,
  `PaymentDay` varchar(255) DEFAULT NULL,
  `AccountRef` varchar(255) DEFAULT NULL,
  `UserLinkRefEditsAllowInd` varchar(255) DEFAULT NULL,
  `FutureStatusCd` varchar(255) DEFAULT NULL,
  `AuditAccountRef` varchar(255) DEFAULT NULL,
  `SplitPct` decimal(28,6) DEFAULT NULL,
  KEY `billingentityIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `building`
--

DROP TABLE IF EXISTS `building`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `building` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `BldgNumber` int(11) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `BusinessCategory` varchar(255) DEFAULT NULL,
  `BusinessClass` varchar(255) DEFAULT NULL,
  `FireAlliedClassCode` varchar(255) DEFAULT NULL,
  `GLClassCode` varchar(255) DEFAULT NULL,
  `CrimeClassCode` varchar(255) DEFAULT NULL,
  `AuditType` varchar(255) DEFAULT NULL,
  `ConstructionCd` varchar(255) DEFAULT NULL,
  `RoofCd` varchar(255) DEFAULT NULL,
  `YearBuilt` int(11) DEFAULT NULL,
  `SqFt` int(11) DEFAULT NULL,
  `Stories` int(11) DEFAULT NULL,
  `InsuredSqFt` int(11) DEFAULT NULL,
  `Units` int(11) DEFAULT NULL,
  `OccupancyCd` varchar(255) DEFAULT NULL,
  `OccupiedPct` decimal(28,6) DEFAULT NULL,
  `InspectionDt` date DEFAULT NULL,
  `ProtectionClass` varchar(255) DEFAULT NULL,
  `TerritoryCd` varchar(255) DEFAULT NULL,
  `WindClassCd` varchar(255) DEFAULT NULL,
  `BuildingLimit` int(11) DEFAULT NULL,
  `ContentsLimit` int(11) DEFAULT NULL,
  `Deductible` int(11) DEFAULT NULL,
  `ValuationMethod` varchar(255) DEFAULT NULL,
  `InflationGuardPct` int(11) DEFAULT NULL,
  `OrdinanceOrLawInd` varchar(255) DEFAULT NULL,
  `ScheduledPremiumMod` int(11) DEFAULT NULL,
  `BurglarAlarmCd` varchar(255) DEFAULT NULL,
  `BurglarAlarmCertificateNumber` varchar(255) DEFAULT NULL,
  `BurglarAlarmExpirationDt` date DEFAULT NULL,
  `BurglarAlarmExtent` varchar(255) DEFAULT NULL,
  `BurglarAlarmGrade` varchar(255) DEFAULT NULL,
  `BurglarAlarmServicedBy` varchar(255) DEFAULT NULL,
  `NumberOfGuards` int(11) DEFAULT NULL,
  `GuardsClockHourly` varchar(255) DEFAULT NULL,
  `FireProtection` varchar(255) DEFAULT NULL,
  `SprinkleredPct` int(11) DEFAULT NULL,
  `FireAlarmCd` varchar(255) DEFAULT NULL,
  `FireAlarmManufacturer` varchar(255) DEFAULT NULL,
  `FormType` varchar(255) DEFAULT NULL,
  `BuildingCoverageType` varchar(255) DEFAULT NULL,
  `BuildingCovLimit` int(11) DEFAULT NULL,
  `BuildingCoinsurancePct` int(11) DEFAULT NULL,
  `BuildingDeductible` int(11) DEFAULT NULL,
  `BuildingFormType` varchar(255) DEFAULT NULL,
  `BuildingValuationMethod` varchar(255) DEFAULT NULL,
  `BuildingInflationGuardPct` int(11) DEFAULT NULL,
  `BuildingOffPremisePowerFailInd` varchar(255) DEFAULT NULL,
  `BuildingOrdinanceOrLawInd` varchar(255) DEFAULT NULL,
  `BuildingVacancyPermitInd` varchar(255) DEFAULT NULL,
  `BuildingVacancyPermitMonths` int(11) DEFAULT NULL,
  `BuildingWindHailInd` varchar(255) DEFAULT NULL,
  `BuildingWindHailDeductible` int(11) DEFAULT NULL,
  `BuildingBlanketRiskGroupRef` varchar(255) DEFAULT NULL,
  `BuildingTheftExclusion` varchar(255) DEFAULT NULL,
  `BuildingAgreedValue` varchar(255) DEFAULT NULL,
  `BuildingSubsidenceInd` varchar(255) DEFAULT NULL,
  `ContentsCoverageType` varchar(255) DEFAULT NULL,
  `ContentsCovLimit` int(11) DEFAULT NULL,
  `ContentsCoinsurancePct` int(11) DEFAULT NULL,
  `ContentsDeductible` int(11) DEFAULT NULL,
  `ContentsFormType` varchar(255) DEFAULT NULL,
  `ContentsValuationMethod` varchar(255) DEFAULT NULL,
  `ContentsOffPremisePowerFailInd` varchar(255) DEFAULT NULL,
  `ContentsWindHailInd` varchar(255) DEFAULT NULL,
  `ContentsWindHailDeductible` int(11) DEFAULT NULL,
  `ContentsBlanketRiskGroupRef` varchar(255) DEFAULT NULL,
  `ContentsTheftExclusion` varchar(255) DEFAULT NULL,
  `ContentsTheftDeductible` int(11) DEFAULT NULL,
  `ContentsAgreedValue` varchar(255) DEFAULT NULL,
  `ContentsSubsidenceInd` varchar(255) DEFAULT NULL,
  `BusinessIncomeCoverageType` varchar(255) DEFAULT NULL,
  `BusinessIncomeCovLimit` int(11) DEFAULT NULL,
  `BusinessIncomeCoinsurancePct` int(11) DEFAULT NULL,
  `BusinessIncomeDeductible` int(11) DEFAULT NULL,
  `BusinessIncomeFormType` varchar(255) DEFAULT NULL,
  `BusinessIncomeValuationMethod` varchar(255) DEFAULT NULL,
  `BusinessIncomeLimitLossPayCd` varchar(255) DEFAULT NULL,
  `BusIncOrdinaryPayrollDays` int(11) DEFAULT NULL,
  `BusIncElectronicRecordsDays` int(11) DEFAULT NULL,
  `BusIncExtPerOfIndemnityDays` int(11) DEFAULT NULL,
  `BusIncIncPerOfRestorationDays` int(11) DEFAULT NULL,
  `BusinessIncomeWindHailInd` varchar(255) DEFAULT NULL,
  `BusIncWindHailDeductible` int(11) DEFAULT NULL,
  `BusIncBlanketRiskGroupRef` varchar(255) DEFAULT NULL,
  `BusinessIncomeAgreedValue` varchar(255) DEFAULT NULL,
  `BusinessIncomeType` varchar(255) DEFAULT NULL,
  `BusIncDependentPropertyLimit` int(11) DEFAULT NULL,
  `BusinessIncomeOptions` varchar(255) DEFAULT NULL,
  `BusIncOptCoinsurance` varchar(255) DEFAULT NULL,
  `BusIncOptMonthlyIndemnity` varchar(255) DEFAULT NULL,
  `BusinessIncomeSubsidenceInd` varchar(255) DEFAULT NULL,
  `ExtraExpenseCoverageType` varchar(255) DEFAULT NULL,
  `ExtraExpenseCovLimit` int(11) DEFAULT NULL,
  `ExtraExpenseCoinsurancePct` int(11) DEFAULT NULL,
  `ExtraExpenseDeductible` int(11) DEFAULT NULL,
  `ExtraExpenseFormType` varchar(255) DEFAULT NULL,
  `ExtraExpenseWindHailInd` varchar(255) DEFAULT NULL,
  `ExtraExpenseWindHailDeductible` int(11) DEFAULT NULL,
  `ExtraExpenseLiability` varchar(255) DEFAULT NULL,
  `ExtraExpenseSubsidenceInd` varchar(255) DEFAULT NULL,
  `ConstructionIICd` varchar(255) DEFAULT NULL,
  `SprinkerExclusion` varchar(255) DEFAULT NULL,
  `VandalisionExclusion` varchar(255) DEFAULT NULL,
  `GroupII` varchar(255) DEFAULT NULL,
  `SprinkerCredit` varchar(255) DEFAULT NULL,
  `BCEGS` varchar(255) DEFAULT NULL,
  `ImprovementsLimit` int(11) DEFAULT NULL,
  `BuildingOrdLawBLimit` int(11) DEFAULT NULL,
  `BuildingOrdLawCLimit` int(11) DEFAULT NULL,
  `TheftExclusion` varchar(255) DEFAULT NULL,
  `BlanketOptionCd` varchar(255) DEFAULT NULL,
  `WindHailExclusion` varchar(255) DEFAULT NULL,
  `WindHailPct` varchar(255) DEFAULT NULL,
  `ClassCd` varchar(255) DEFAULT NULL,
  `PrimaryResidenceInd` varchar(255) DEFAULT NULL,
  `CovALimit` int(11) DEFAULT NULL,
  `CovBLimit` int(11) DEFAULT NULL,
  `CovBLimitIncluded` int(11) DEFAULT NULL,
  `CovBLimitIncrease` int(11) DEFAULT NULL,
  `CovCLimit` int(11) DEFAULT NULL,
  `CovCLimitIncluded` int(11) DEFAULT NULL,
  `CovCLimitIncrease` int(11) DEFAULT NULL,
  `CovDLimit` int(11) DEFAULT NULL,
  `CovDLimitIncluded` int(11) DEFAULT NULL,
  `CovDLimitIncrease` int(11) DEFAULT NULL,
  `CovELimit` int(11) DEFAULT NULL,
  `CovFLimit` int(11) DEFAULT NULL,
  `AllPerilDed` varchar(255) DEFAULT NULL,
  `PackageCoverageInd` varchar(255) DEFAULT NULL,
  `SmokeDetectorInd` varchar(255) DEFAULT NULL,
  `GateGuardedInd` varchar(255) DEFAULT NULL,
  KEY `buildingIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `businessinfo`
--

DROP TABLE IF EXISTS `businessinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `businessinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `BusinessInfoCd` varchar(255) DEFAULT NULL,
  `NatureOfBusiness` varchar(255) DEFAULT NULL,
  `NatureBusinessCd` varchar(255) DEFAULT NULL,
  `YearsInBusiness` varchar(255) DEFAULT NULL,
  `AnnualSalesAmt` varchar(255) DEFAULT NULL,
  `AnnualPayrollAmt` varchar(255) DEFAULT NULL,
  `NumberEmployees` varchar(255) DEFAULT NULL,
  `BusinessTypeCd` varchar(255) DEFAULT NULL,
  KEY `businessinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `caagreementalternatecovrecordinfo`
--

DROP TABLE IF EXISTS `caagreementalternatecovrecordinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caagreementalternatecovrecordinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `EndorsementSerialNo` varchar(255) DEFAULT NULL,
  `InsuredNameAltCov` varchar(255) DEFAULT NULL,
  `InsurerCdAltCov` int(11) DEFAULT NULL,
  `InsurerNameAltCov` varchar(255) DEFAULT NULL,
  `PolicyNoAltCov` varchar(255) DEFAULT NULL,
  `UnitNoIdAltCov` int(11) DEFAULT NULL,
  `PolicyInceptionDtAltCov` int(11) DEFAULT NULL,
  `PolicyExpirationDtAltCov` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `caagreementalternatecovrecordinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `caagreementform10recordinfo`
--

DROP TABLE IF EXISTS `caagreementform10recordinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caagreementform10recordinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `EndorsementSerialNo` varchar(255) DEFAULT NULL,
  `NameofEmployee` varchar(255) DEFAULT NULL,
  `NameofOperation` varchar(255) DEFAULT NULL,
  `OperationTitle` varchar(255) DEFAULT NULL,
  `AddressofLocation` varchar(255) DEFAULT NULL,
  `ClassCd` int(11) DEFAULT NULL,
  `ClassCdSuffix` int(11) DEFAULT NULL,
  `ClassCdWording` varchar(255) DEFAULT NULL,
  `FutureRef` varchar(255) DEFAULT NULL,
  `EndorsementSeqNo` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `caagreementform10recordinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `caagreementform11recordinfo`
--

DROP TABLE IF EXISTS `caagreementform11recordinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caagreementform11recordinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `EndorsementSerialNo` varchar(255) DEFAULT NULL,
  `ExcludedOperDesc01` varchar(255) DEFAULT NULL,
  `ExcludedOperDesc02` varchar(255) DEFAULT NULL,
  `FutureRef` varchar(255) DEFAULT NULL,
  `EndorsementSeqNo` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `caagreementform11recordinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cabinet`
--

DROP TABLE IF EXISTS `cabinet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cabinet` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Version` varchar(255) DEFAULT NULL,
  `UserId` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `UpdatedUser` varchar(255) DEFAULT NULL,
  `UpdatedDt` date DEFAULT NULL,
  `UpdatedTm` varchar(255) DEFAULT NULL,
  `DeletedUser` varchar(255) DEFAULT NULL,
  `DeletedDt` date DEFAULT NULL,
  `DeletedTm` varchar(255) DEFAULT NULL,
  `OwnerRoles` varchar(255) DEFAULT NULL,
  `ContributorRoles` varchar(255) DEFAULT NULL,
  `MemberRoles` varchar(255) DEFAULT NULL,
  `OwnerUsers` varchar(255) DEFAULT NULL,
  `ContributorUsers` varchar(255) DEFAULT NULL,
  `MemberUsers` varchar(255) DEFAULT NULL,
  KEY `cabinetIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `caempleaseendbyclientnameinfo`
--

DROP TABLE IF EXISTS `caempleaseendbyclientnameinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caempleaseendbyclientnameinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `EndorsementSerialNumber` varchar(255) DEFAULT NULL,
  `LaborContractName` varchar(255) DEFAULT NULL,
  `LaborContractAddrStreet` varchar(255) DEFAULT NULL,
  `LaborContractAddrCity` varchar(255) DEFAULT NULL,
  `LaborContractAddrState` varchar(255) DEFAULT NULL,
  `LaborContractAddrZipCode` varchar(255) DEFAULT NULL,
  `EndorsementSeqNo` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `caempleaseendbyclientnameinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `caempleaseendbylaborconinfo`
--

DROP TABLE IF EXISTS `caempleaseendbylaborconinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caempleaseendbylaborconinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `NameofClient` varchar(255) DEFAULT NULL,
  `AddressStreet` varchar(255) DEFAULT NULL,
  `AddressCity` varchar(255) DEFAULT NULL,
  `AddressState` varchar(255) DEFAULT NULL,
  `AddressZipCode` varchar(255) DEFAULT NULL,
  `EndorsementSeqNo` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `caempleaseendbylaborconinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cagroupinscovendinfo`
--

DROP TABLE IF EXISTS `cagroupinscovendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cagroupinscovendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `NameofGroup` varchar(255) DEFAULT NULL,
  `FutureRef` varchar(255) DEFAULT NULL,
  `GroupInsEffectiveDt` int(11) DEFAULT NULL,
  `GroupInsExpirationDt` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `cagroupinscovendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `calendarexception`
--

DROP TABLE IF EXISTS `calendarexception`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendarexception` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `CalendarDt` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  KEY `calendarexceptionIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `camultipurposetextrecordinfo`
--

DROP TABLE IF EXISTS `camultipurposetextrecordinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `camultipurposetextrecordinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `EndorsementSerialNo` varchar(255) DEFAULT NULL,
  `EndorsementLine01` varchar(255) DEFAULT NULL,
  `EndorsementLine02` varchar(255) DEFAULT NULL,
  `EndorsementSeqNo` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `camultipurposetextrecordinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `caofficersdirectorscovexlendinfo`
--

DROP TABLE IF EXISTS `caofficersdirectorscovexlendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caofficersdirectorscovexlendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `OfficerorDirectorExcluded01` varchar(255) DEFAULT NULL,
  `OfficerorDirectorExcluded02` varchar(255) DEFAULT NULL,
  `OfficerorDirectorExcluded03` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `caofficersdirectorscovexlendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `capartnerexclcovendinfo`
--

DROP TABLE IF EXISTS `capartnerexclcovendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `capartnerexclcovendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `PartnerNameExcluded01` varchar(255) DEFAULT NULL,
  `PartnerNameExcluded02` varchar(255) DEFAULT NULL,
  `PartnerNameExcluded03` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `capartnerexclcovendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `catastrophe`
--

DROP TABLE IF EXISTS `catastrophe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catastrophe` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `CatastropheYr` int(11) DEFAULT NULL,
  `CatastropheNumber` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `StartTm` varchar(255) DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `EndTm` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  KEY `catastropheIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `catastrophelocation`
--

DROP TABLE IF EXISTS `catastrophelocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catastrophelocation` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateProvCd` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `Location` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `RequireManualVerificationInd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  KEY `catastrophelocationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `causlhactcovendinfo`
--

DROP TABLE IF EXISTS `causlhactcovendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `causlhactcovendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `ClassCd` int(11) DEFAULT NULL,
  `ClassCdSuffix` varchar(255) DEFAULT NULL,
  `ClassCdWording` varchar(255) DEFAULT NULL,
  `ExposureAmt` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `causlhactcovendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cavoluntarycompemprliabcovendinfo`
--

DROP TABLE IF EXISTS `cavoluntarycompemprliabcovendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cavoluntarycompemprliabcovendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `GroupName01` varchar(255) DEFAULT NULL,
  `GroupName02` varchar(255) DEFAULT NULL,
  `GroupName03` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `cavoluntarycompemprliabcovendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cawosendinfo`
--

DROP TABLE IF EXISTS `cawosendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cawosendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `PersonorOrgJobDesc01` varchar(255) DEFAULT NULL,
  `PersonorOrgJobDesc02` varchar(255) DEFAULT NULL,
  `PremiumPercentage` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `cawosendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `citymatch`
--

DROP TABLE IF EXISTS `citymatch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `citymatch` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CityName` varchar(255) DEFAULT NULL,
  KEY `citymatchIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `citymatches`
--

DROP TABLE IF EXISTS `citymatches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `citymatches` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `citymatchesIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claim`
--

DROP TABLE IF EXISTS `claim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claim` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `ClaimNumber` varchar(255) DEFAULT NULL,
  `LossDt` date DEFAULT NULL,
  `LossTm` varchar(255) DEFAULT NULL,
  `ReportedDt` date DEFAULT NULL,
  `ReportedTm` varchar(255) DEFAULT NULL,
  `ShortDesc` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `DamageEstimateAmt` decimal(28,6) DEFAULT NULL,
  `AuthorityContacted` varchar(255) DEFAULT NULL,
  `CaseNumber` varchar(255) DEFAULT NULL,
  `ReportedBy` varchar(255) DEFAULT NULL,
  `ReportedTo` varchar(255) DEFAULT NULL,
  `ForRecordOnlyInd` varchar(255) DEFAULT NULL,
  `InSIUInd` varchar(255) DEFAULT NULL,
  `SIUReasonCd` varchar(255) DEFAULT NULL,
  `SIUAdjusterRef` int(11) DEFAULT NULL,
  `SIUAssignedDate` date DEFAULT NULL,
  `SIUComment` varchar(255) DEFAULT NULL,
  `SIUStatus` varchar(255) DEFAULT NULL,
  `SIUStage` varchar(255) DEFAULT NULL,
  `SIUResolution` varchar(255) DEFAULT NULL,
  `SIUResolutionDate` date DEFAULT NULL,
  `SIUResolutionComment` varchar(255) DEFAULT NULL,
  `DOIComplaintInd` varchar(255) DEFAULT NULL,
  `CatastropheRef` int(11) DEFAULT NULL,
  `CatastropheNoneSelectedInd` varchar(255) DEFAULT NULL,
  `CatastropheNoneSelectedReason` varchar(255) DEFAULT NULL,
  `ExaminerProviderRef` int(11) DEFAULT NULL,
  `BranchCd` varchar(255) DEFAULT NULL,
  `MasterClaimRef` int(11) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `StatusDt` date DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `ProductLineCd` varchar(255) DEFAULT NULL,
  `RiskIdRef` varchar(255) DEFAULT NULL,
  `DriverIdRef` varchar(255) DEFAULT NULL,
  `OtherVehicleDesc` varchar(255) DEFAULT NULL,
  `OtherVIN` varchar(255) DEFAULT NULL,
  `OtherVehiclePurchaseDt` date DEFAULT NULL,
  `PermissiveDriverName` varchar(255) DEFAULT NULL,
  `PermissiveDriverLicensedState` varchar(255) DEFAULT NULL,
  `PermissiveDriverLicenseNumber` varchar(255) DEFAULT NULL,
  `PermissiveDriverRelationship` varchar(255) DEFAULT NULL,
  `AtFaultCd` varchar(255) DEFAULT NULL,
  `LossCauseCd` varchar(255) DEFAULT NULL,
  `SubLossCauseCd` varchar(255) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `LossLocationDesc` varchar(255) DEFAULT NULL,
  `ClaimRef` int(11) DEFAULT NULL,
  `LossNoticeRef` int(11) DEFAULT NULL,
  `LockTaskId` varchar(255) DEFAULT NULL,
  `Version` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `DamageDesc` varchar(255) DEFAULT NULL,
  `VehicleLocation` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `SourceRef` varchar(255) DEFAULT NULL,
  `CategoryCd` varchar(255) DEFAULT NULL,
  `SuitFiledInd` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `ClaimLossTemplateIdRef` varchar(255) DEFAULT NULL,
  `AwarenessDt` date DEFAULT NULL,
  `DamagedInd` varchar(255) DEFAULT NULL,
  `VehicleInvolvedInd` varchar(255) DEFAULT NULL,
  `VIPLevel` varchar(255) DEFAULT NULL,
  `FraudScore` varchar(255) DEFAULT NULL,
  `FraudScoreDescription` varchar(255) DEFAULT NULL,
  `HistoryModeInd` varchar(255) DEFAULT NULL,
  `OriginalClaimNumber` varchar(255) DEFAULT NULL,
  `FileReasonCd` varchar(255) DEFAULT NULL,
  `FileReasonDesc` varchar(255) DEFAULT NULL,
  `UnmarkedFileOnlyInd` varchar(255) DEFAULT NULL,
  `PendingFileOnlyAction` varchar(255) DEFAULT NULL,
  `NoticeReceivedDt` date DEFAULT NULL,
  `ReportedToEmployerDt` date DEFAULT NULL,
  `NoticeReceivedCd` varchar(255) DEFAULT NULL,
  `LossCauseTypeCd` varchar(255) DEFAULT NULL,
  `LossLocationCd` varchar(255) DEFAULT NULL,
  `LossSettlementType` varchar(255) DEFAULT NULL,
  `EmployerPremisesInd` varchar(255) DEFAULT NULL,
  `DepartmentOfLabor` varchar(255) DEFAULT NULL,
  `JurisdictionStateCd` varchar(255) DEFAULT NULL,
  `LastPaidDt` date DEFAULT NULL,
  `DisabilityDt` date DEFAULT NULL,
  `RTWDt` date DEFAULT NULL,
  `SalaryContinued` varchar(255) DEFAULT NULL,
  `ContactNumber` varchar(255) DEFAULT NULL,
  `EmployeeBeginWorkTm` varchar(255) DEFAULT NULL,
  `Department` varchar(255) DEFAULT NULL,
  `ShortDescription` varchar(255) DEFAULT NULL,
  `LossCoverageAct` varchar(255) DEFAULT NULL,
  `PhoneName` varchar(255) DEFAULT NULL,
  `PhoneNumber` varchar(255) DEFAULT NULL,
  `PREmailInd` varchar(255) DEFAULT NULL,
  `OriginalAdministrationNumber` varchar(255) DEFAULT NULL,
  `FraudulentClaimCode` varchar(255) DEFAULT NULL,
  `LateReasonCd` varchar(255) DEFAULT NULL,
  `JurisdictionClaimNumber` varchar(255) DEFAULT NULL,
  `TypeOfLossCd` varchar(255) DEFAULT NULL,
  `SubrogationInd` varchar(255) DEFAULT NULL,
  `UWAlrtUnrepairedDmgPriorClaim` varchar(255) DEFAULT NULL,
  `UWAlrtConditionCurrClaim` varchar(255) DEFAULT NULL,
  `UWAlrtOtherConditions` varchar(255) DEFAULT NULL,
  `UWAlrtHazardDetail` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics1` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics2` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics3` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics4` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics5` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics6` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics7` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics8` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics9` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics10` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics11` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics12` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics13` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics14` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics15` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics16` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics17` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics18` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics19` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics20` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics21` varchar(255) DEFAULT NULL,
  `UWAlrtHazardCharacteristics22` varchar(255) DEFAULT NULL,
  `EBEmailClaimInd` varchar(255) DEFAULT NULL,
  `LinkCompanion` varchar(255) DEFAULT NULL,
  `LinkCompanionDate` varchar(255) DEFAULT NULL,
  `NamedStormInd` varchar(255) DEFAULT NULL,
  `FloodInsuranceInd` varchar(255) DEFAULT NULL,
  `FloodInsuranceCompany` varchar(255) DEFAULT NULL,
  `FloodInsurancePolicy` varchar(255) DEFAULT NULL,
  `FloodInsurancePolicyEffectiveDate` date DEFAULT NULL,
  `FloodInsurancePolicyExpirationDate` date DEFAULT NULL,
  `ReportedToProducerBy` varchar(255) DEFAULT NULL,
  `ReceivedBy` varchar(255) DEFAULT NULL,
  KEY `claimIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimant`
--

DROP TABLE IF EXISTS `claimant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimant` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ClaimantNumber` int(11) DEFAULT NULL,
  `ClaimantTypeCd` varchar(255) DEFAULT NULL,
  `ClaimantSubTypeCd` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `StatusDt` date DEFAULT NULL,
  `InjuryDesc` varchar(255) DEFAULT NULL,
  `MajorTraumaCd` varchar(255) DEFAULT NULL,
  `FatalityInd` varchar(255) DEFAULT NULL,
  `IndexName` varchar(255) DEFAULT NULL,
  `PreferredDeliveryMethod` varchar(255) DEFAULT NULL,
  `InjuryInvolvedInd` varchar(255) DEFAULT NULL,
  `InjuredPartyRelationshipCd` varchar(255) DEFAULT NULL,
  `MedicareReportingFilename` varchar(255) DEFAULT NULL,
  `ClaimantLinkIdRef` varchar(255) DEFAULT NULL,
  `ClaimantLinkDescription` varchar(255) DEFAULT NULL,
  `PreferredPaymentMethodCd` varchar(255) DEFAULT NULL,
  `DeathDt` date DEFAULT NULL,
  `MMIDt` date DEFAULT NULL,
  `MMIPct` varchar(255) DEFAULT NULL,
  `InjuredPartyContacted` varchar(255) DEFAULT NULL,
  `NumberOfDependents` varchar(255) DEFAULT NULL,
  `NatureCd` varchar(255) DEFAULT NULL,
  `BodyPartGeneralCd` varchar(255) DEFAULT NULL,
  `BodyPartSpecificCd` varchar(255) DEFAULT NULL,
  `BodyPartIAIABCCd` varchar(255) DEFAULT NULL,
  `InitialTreatment` varchar(255) DEFAULT NULL,
  `DisabilityTypeCd` varchar(255) DEFAULT NULL,
  `ClassCd` varchar(255) DEFAULT NULL,
  `EmploymentStatusCd` varchar(255) DEFAULT NULL,
  `ShiftCd` varchar(255) DEFAULT NULL,
  `HourlySalaryCd` varchar(255) DEFAULT NULL,
  `AverageWeeklyWagesAmt` varchar(255) DEFAULT NULL,
  `HireDt` date DEFAULT NULL,
  `PayrollLocation` varchar(255) DEFAULT NULL,
  `Department` varchar(255) DEFAULT NULL,
  `LastWorkedDt` date DEFAULT NULL,
  `MealsLodging` varchar(255) DEFAULT NULL,
  `InpatientOvernight` varchar(255) DEFAULT NULL,
  `EmergencyRoomOnly` varchar(255) DEFAULT NULL,
  `LiabilityDenied` varchar(255) DEFAULT NULL,
  `LiabilityDeniedDesc` varchar(255) DEFAULT NULL,
  `PaidFullWages` varchar(255) DEFAULT NULL,
  `SalaryContinued` varchar(255) DEFAULT NULL,
  `BenefitAmt` varchar(255) DEFAULT NULL,
  `DeathBenefitAmt` varchar(255) DEFAULT NULL,
  `DaysPaid` varchar(255) DEFAULT NULL,
  `WeeksPaid` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` date DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `BenefitRate` varchar(255) DEFAULT NULL,
  `OverrideRate` varchar(255) DEFAULT NULL,
  `OverrideRateComments` varchar(255) DEFAULT NULL,
  `ShortDescription` varchar(255) DEFAULT NULL,
  `DetailDescription` varchar(255) DEFAULT NULL,
  `OccupationDescription` varchar(255) DEFAULT NULL,
  `ManagedCareOrganizationTypeCd` varchar(255) DEFAULT NULL,
  `EducationLevel` varchar(255) DEFAULT NULL,
  `PreExistingDisabilityCd` varchar(255) DEFAULT NULL,
  `ReducedBenefitAmountCd` varchar(255) DEFAULT NULL,
  `AdjusterName` varchar(255) DEFAULT NULL,
  `AdjusterCompany` varchar(255) DEFAULT NULL,
  `AttorneyName` varchar(255) DEFAULT NULL,
  `AttorneyFirm` varchar(255) DEFAULT NULL,
  `PhoneNamePA` varchar(255) DEFAULT NULL,
  `PhoneNumberPA` varchar(255) DEFAULT NULL,
  `EmailAddrPA` varchar(255) DEFAULT NULL,
  `PhoneNameAttorney` varchar(255) DEFAULT NULL,
  `PhoneNumberAttorney` varchar(255) DEFAULT NULL,
  `EmailAddrAttorney` varchar(255) DEFAULT NULL,
  KEY `claimantIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimanttransaction`
--

DROP TABLE IF EXISTS `claimanttransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimanttransaction` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `PaymentStatus` varchar(255) DEFAULT NULL,
  `PaymentStatusDt` date DEFAULT NULL,
  `PaymentTypeCd` varchar(255) DEFAULT NULL,
  `PaymentMethodCd` varchar(255) DEFAULT NULL,
  `PaymentAccountCd` varchar(255) DEFAULT NULL,
  `PaymentReference` varchar(255) DEFAULT NULL,
  `PayToName` varchar(255) DEFAULT NULL,
  `PayToClaimantInd` varchar(255) DEFAULT NULL,
  `PayToProviderInd` varchar(255) DEFAULT NULL,
  `ProviderRef` int(11) DEFAULT NULL,
  `PayToInterestInd` varchar(255) DEFAULT NULL,
  `InterestCd` varchar(255) DEFAULT NULL,
  `InterestIdRef` varchar(255) DEFAULT NULL,
  `PayToOverrideInd` varchar(255) DEFAULT NULL,
  `RecoveryCheckNumber` varchar(255) DEFAULT NULL,
  `RecoveryCheckDt` date DEFAULT NULL,
  `RecoveryPaidBy` varchar(255) DEFAULT NULL,
  `ClaimTransactionHistoryRef` varchar(255) DEFAULT NULL,
  `Memo` varchar(255) DEFAULT NULL,
  `MemoCd` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `ReserveAmt` decimal(28,6) DEFAULT NULL,
  `RecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ExpectedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ClassificationCd` varchar(255) DEFAULT NULL,
  `SequenceNumber` int(11) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `TransactionDt` date DEFAULT NULL,
  `TransactionTm` varchar(255) DEFAULT NULL,
  `TransactionUser` varchar(255) DEFAULT NULL,
  `ClaimantTransactionRef` varchar(255) DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `PrinterTemplateIdRef` varchar(255) DEFAULT NULL,
  `RecoveryCheckAmt` decimal(28,6) DEFAULT NULL,
  `AutomatedPaymentInd` varchar(255) DEFAULT NULL,
  `VoidAllowedInd` varchar(255) DEFAULT NULL,
  `SubrogationIdRef` varchar(255) DEFAULT NULL,
  `ServicePeriodStartDt` date DEFAULT NULL,
  `SystemCheckReference` varchar(255) DEFAULT NULL,
  `ServicePeriodEndDt` date DEFAULT NULL,
  `ReasonCd` varchar(255) DEFAULT NULL,
  `DaysPaid` varchar(255) DEFAULT NULL,
  `WeeksPaid` varchar(255) DEFAULT NULL,
  `DenialReason` varchar(255) DEFAULT NULL,
  `NonConsecutivePeriod` varchar(255) DEFAULT NULL,
  `LumpSumPaymentCd` varchar(255) DEFAULT NULL,
  `RecoveryType` varchar(255) DEFAULT NULL,
  `InvoiceNumber` varchar(255) DEFAULT NULL,
  KEY `claimanttransactionIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimbaseline`
--

DROP TABLE IF EXISTS `claimbaseline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimbaseline` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `Version` varchar(255) DEFAULT NULL,
  `SubmissionClaimRef` int(11) DEFAULT NULL,
  KEY `claimbaselineIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimcatastrophe`
--

DROP TABLE IF EXISTS `claimcatastrophe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimcatastrophe` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CatastropheRef` int(11) DEFAULT NULL,
  `CatastropheLocationIdRef` varchar(255) DEFAULT NULL,
  `ReviewedInd` varchar(255) DEFAULT NULL,
  `ReviewedBy` varchar(255) DEFAULT NULL,
  `ReviewedDt` date DEFAULT NULL,
  `ReviewedTm` varchar(255) DEFAULT NULL,
  `AssignedInd` varchar(255) DEFAULT NULL,
  `ManuallyVerifiedInd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  KEY `claimcatastropheIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimpolicyinfo`
--

DROP TABLE IF EXISTS `claimpolicyinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimpolicyinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyRef` varchar(255) DEFAULT NULL,
  `InceptionDt` date DEFAULT NULL,
  `InceptionTm` varchar(255) DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `TermBeginDt` date DEFAULT NULL,
  `TermEndDt` date DEFAULT NULL,
  `ProviderRef` int(11) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `ControllingStateCd` varchar(255) DEFAULT NULL,
  `PolicyDescription` varchar(255) DEFAULT NULL,
  `CarrierName` varchar(255) DEFAULT NULL,
  `ExternalSystemInd` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `CancelDt` date DEFAULT NULL,
  `PolicyProductVersionIdRef` varchar(255) DEFAULT NULL,
  `PolicyProductName` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  `PolicyGroupCd` varchar(255) DEFAULT NULL,
  `PreferredDeliveryMethod` varchar(255) DEFAULT NULL,
  `CoverageTriggerCd` varchar(255) DEFAULT NULL,
  `RetroactiveDt` date DEFAULT NULL,
  `ContinuityDt` date DEFAULT NULL,
  `ExtendedReportingDt` date DEFAULT NULL,
  `AsOfDt` date DEFAULT NULL,
  `CancelDtAsOfLossDt` date DEFAULT NULL,
  `PolicyTransactionNumber` int(11) DEFAULT NULL,
  `ShellPolicyInd` varchar(255) DEFAULT NULL,
  KEY `claimpolicyinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimpolicylimit`
--

DROP TABLE IF EXISTS `claimpolicylimit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimpolicylimit` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `CoverageCd` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Limit` varchar(255) DEFAULT NULL,
  `LimitAdjusted` varchar(255) DEFAULT NULL,
  `LimitOverridden` varchar(255) DEFAULT NULL,
  `AggregateLimitTypeCd` varchar(255) DEFAULT NULL,
  `AggregateLimit` varchar(255) DEFAULT NULL,
  `AggregateLimitAdjusted` varchar(255) DEFAULT NULL,
  `AggregateLimitOverridden` varchar(255) DEFAULT NULL,
  `Deductible` varchar(255) DEFAULT NULL,
  `DeductibleAdjusted` varchar(255) DEFAULT NULL,
  `DeductibleOverridden` varchar(255) DEFAULT NULL,
  `AggregateLimitDescription` varchar(255) DEFAULT NULL,
  `LimitDescription` varchar(255) DEFAULT NULL,
  `DeductibleDescription` varchar(255) DEFAULT NULL,
  `OriginalCoverageCd` varchar(255) DEFAULT NULL,
  `RateAreaName` varchar(255) DEFAULT NULL,
  `StatData` varchar(255) DEFAULT NULL,
  `LineCd` varchar(255) DEFAULT NULL,
  `DeductibleAllocationCd` varchar(255) DEFAULT NULL,
  `DeductibleAllocationTypeCd` varchar(255) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `ReinsuranceCoverageGroupCd` varchar(255) DEFAULT NULL,
  KEY `claimpolicylimitIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimpolicyreinsdetail`
--

DROP TABLE IF EXISTS `claimpolicyreinsdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimpolicyreinsdetail` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `GroupIdRef` varchar(255) DEFAULT NULL,
  `GroupIdName` varchar(255) DEFAULT NULL,
  `ReinsuranceTemplateIdRef` varchar(255) DEFAULT NULL,
  `ProbableMaxLoss` int(11) DEFAULT NULL,
  `TotalInsuredValue` int(11) DEFAULT NULL,
  `PlacementAmt` int(11) DEFAULT NULL,
  `StartLimitAmt` int(11) DEFAULT NULL,
  `AmtToPlace` int(11) DEFAULT NULL,
  `PMLOverrideInd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  KEY `claimpolicyreinsdetailIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimpolicyreinsplace`
--

DROP TABLE IF EXISTS `claimpolicyreinsplace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimpolicyreinsplace` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PlacementId` varchar(255) DEFAULT NULL,
  `ReinsuranceType` varchar(255) DEFAULT NULL,
  `StartLimitAmt` int(11) DEFAULT NULL,
  `PlacementAmt` int(11) DEFAULT NULL,
  `TreatyContract` varchar(255) DEFAULT NULL,
  `PlacementPct` decimal(28,6) DEFAULT NULL,
  `PremiumAmt` decimal(28,6) DEFAULT NULL,
  `TreatyTypeCd` varchar(255) DEFAULT NULL,
  `CommissionAmt` decimal(28,6) DEFAULT NULL,
  `CommissionPct` decimal(28,6) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `TransactionPremiumAmt` decimal(28,6) DEFAULT NULL,
  `TransactionCommissionAmt` decimal(28,6) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `LossCalculationTypeCd` varchar(255) DEFAULT NULL,
  `AutomaticRecoveryAdjustmentInd` varchar(255) DEFAULT NULL,
  KEY `claimpolicyreinsplaceIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimpolicyreinsurance`
--

DROP TABLE IF EXISTS `claimpolicyreinsurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimpolicyreinsurance` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CoveredElsewhereInd` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  KEY `claimpolicyreinsuranceIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimpolicysublimit`
--

DROP TABLE IF EXISTS `claimpolicysublimit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimpolicysublimit` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CoverageSubCd` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `CategoryDescription` varchar(255) DEFAULT NULL,
  `ItemNumber` int(11) DEFAULT NULL,
  `Limit` varchar(255) DEFAULT NULL,
  `LimitAdjusted` varchar(255) DEFAULT NULL,
  `LimitOverridden` varchar(255) DEFAULT NULL,
  `Deductible` varchar(255) DEFAULT NULL,
  `DeductibleAdjusted` varchar(255) DEFAULT NULL,
  `DeductibleOverridden` varchar(255) DEFAULT NULL,
  `LimitDescription` varchar(255) DEFAULT NULL,
  `DeductibleDescription` varchar(255) DEFAULT NULL,
  `OriginalCoverageSubCd` varchar(255) DEFAULT NULL,
  `RateAreaName` varchar(255) DEFAULT NULL,
  `StatData` varchar(255) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `ReinsuranceCoverageGroupCd` varchar(255) DEFAULT NULL,
  KEY `claimpolicysublimitIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimreinrecoverytrans`
--

DROP TABLE IF EXISTS `claimreinrecoverytrans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimreinrecoverytrans` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `RecoveryCheckDt` date DEFAULT NULL,
  `RecoveryCheckNumber` varchar(255) DEFAULT NULL,
  `RecoveryCheckAmt` decimal(28,6) DEFAULT NULL,
  `ReserveAmt` decimal(28,6) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `RecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ExpectedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `SequenceNumber` int(11) DEFAULT NULL,
  `TransactionDt` date DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `TransactionTm` varchar(255) DEFAULT NULL,
  `TransactionUser` varchar(255) DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `ClaimTransactionHistoryRef` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `ClaimReinRecoveryTranRef` varchar(255) DEFAULT NULL,
  KEY `claimreinrecoverytransIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimreinsdetailreserve`
--

DROP TABLE IF EXISTS `claimreinsdetailreserve`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimreinsdetailreserve` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ReserveCd` varchar(255) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `RecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ReserveAmt` decimal(28,6) DEFAULT NULL,
  `ExpectedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  KEY `claimreinsdetailreserveIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimreinsdetlresrvsubj`
--

DROP TABLE IF EXISTS `claimreinsdetlresrvsubj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimreinsdetlresrvsubj` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `ReinsuranceCoverageGroupCd` varchar(255) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `RecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ReserveAmt` decimal(28,6) DEFAULT NULL,
  `ExpectedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  KEY `claimreinsdetlresrvsubjIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimreinsplacement`
--

DROP TABLE IF EXISTS `claimreinsplacement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimreinsplacement` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `GroupIdRef` varchar(255) DEFAULT NULL,
  `GroupIdName` varchar(255) DEFAULT NULL,
  `PlacementId` varchar(255) DEFAULT NULL,
  `ReinsuranceType` varchar(255) DEFAULT NULL,
  `PlacementAmt` int(11) DEFAULT NULL,
  `StartLimitAmt` int(11) DEFAULT NULL,
  `PlacementPct` decimal(28,6) DEFAULT NULL,
  `TreatyContract` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `TreatyTypeCd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `ReserveAmt` decimal(28,6) DEFAULT NULL,
  `RecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ExpectedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `CalculationLevel` int(11) DEFAULT NULL,
  `LossCalculationTypeCd` varchar(255) DEFAULT NULL,
  `AutomaticRecoveryAdjustmentInd` varchar(255) DEFAULT NULL,
  KEY `claimreinsplacementIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimreinsplacementresrv`
--

DROP TABLE IF EXISTS `claimreinsplacementresrv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimreinsplacementresrv` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ReserveCd` varchar(255) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `RecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ReserveAmt` decimal(28,6) DEFAULT NULL,
  `ExpectedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  KEY `claimreinsplacementresrvIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimreinsplaceresrvsubj`
--

DROP TABLE IF EXISTS `claimreinsplaceresrvsubj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimreinsplaceresrvsubj` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `ReinsuranceCoverageGroupCd` varchar(255) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `RecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ReserveAmt` decimal(28,6) DEFAULT NULL,
  `ExpectedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  KEY `claimreinsplaceresrvsubjIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimreinstransaction`
--

DROP TABLE IF EXISTS `claimreinstransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimreinstransaction` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `TransactionDt` date DEFAULT NULL,
  `TransactionTm` varchar(255) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `ReserveAmt` decimal(28,6) DEFAULT NULL,
  `RecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ExpectedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `ClaimRef` int(11) DEFAULT NULL,
  `ReinsuranceRecoveryIdRef` varchar(255) DEFAULT NULL,
  KEY `claimreinstransactionIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimreinstransreserve`
--

DROP TABLE IF EXISTS `claimreinstransreserve`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimreinstransreserve` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ReserveCd` varchar(255) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `RecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ReserveAmt` decimal(28,6) DEFAULT NULL,
  `ExpectedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `ReinsuranceCoverageGroupCd` varchar(255) DEFAULT NULL,
  KEY `claimreinstransreserveIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimreinsurance`
--

DROP TABLE IF EXISTS `claimreinsurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimreinsurance` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `CoveredElsewhereInd` varchar(255) DEFAULT NULL,
  KEY `claimreinsuranceIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimreinsurancedetail`
--

DROP TABLE IF EXISTS `claimreinsurancedetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimreinsurancedetail` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ReinsuranceTemplateIdRef` varchar(255) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `StartLimitAmt` int(11) DEFAULT NULL,
  `RecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ReserveAmt` decimal(28,6) DEFAULT NULL,
  `ExpectedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  KEY `claimreinsurancedetailIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimreinsurancerecovery`
--

DROP TABLE IF EXISTS `claimreinsurancerecovery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimreinsurancerecovery` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `StatusDt` date DEFAULT NULL,
  KEY `claimreinsurancerecoveryIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `claimreinsurancestats`
--

DROP TABLE IF EXISTS `claimreinsurancestats`;
/*!50001 DROP VIEW IF EXISTS `claimreinsurancestats`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `claimreinsurancestats` AS SELECT 
 1 AS `SystemId`,
 1 AS `id`,
 1 AS `StatSequence`,
 1 AS `StatSequenceReplace`,
 1 AS `PolicyRef`,
 1 AS `PolicyNumber`,
 1 AS `PolicyVersion`,
 1 AS `TransactionNumber`,
 1 AS `EffectiveDt`,
 1 AS `ExpirationDt`,
 1 AS `CombinedKey`,
 1 AS `AddDt`,
 1 AS `BookDt`,
 1 AS `PolicyYear`,
 1 AS `StartDt`,
 1 AS `EndDt`,
 1 AS `StatusCd`,
 1 AS `ReinsuranceName`,
 1 AS `ReinsuranceItemName`,
 1 AS `ReserveCd`,
 1 AS `ReserveTypeCd`,
 1 AS `MasterSubInd`,
 1 AS `TransactionCd`,
 1 AS `AnnualStatementLineCd`,
 1 AS `StateCd`,
 1 AS `ClaimNumber`,
 1 AS `ClaimRef`,
 1 AS `CarrierCd`,
 1 AS `CarrierGroupCd`,
 1 AS `LossYear`,
 1 AS `ExpectedRecoveryChangeAmt`,
 1 AS `ReserveChangeAmt`,
 1 AS `PaidAmt`,
 1 AS `PostedRecoveryAmt`,
 1 AS `ProductVersionIdRef`,
 1 AS `LossDt`,
 1 AS `ReportDt`,
 1 AS `LossCauseCd`,
 1 AS `SubLossCauseCd`,
 1 AS `ClaimStatusCd`,
 1 AS `ProductName`,
 1 AS `PolicyProductVersionIdRef`,
 1 AS `PolicyProductName`,
 1 AS `PolicyTypeCd`,
 1 AS `PolicyGroupCd`,
 1 AS `ReversalStopInd`,
 1 AS `CustomerRef`,
 1 AS `SummaryKey`,
 1 AS `RecoveryTransactionCd`,
 1 AS `CatastropheRef`,
 1 AS `RecoveryTransactionNumber`,
 1 AS `CatastropheNumber`,
 1 AS `HistoricReserveAmt`,
 1 AS `HistoricExpectedRecoveryAmt`,
 1 AS `HistoricPaidAmt`,
 1 AS `ReinsuranceCoverageGroupCd`,
 1 AS `ReinsuranceReserveCd`,
 1 AS `CoverageTriggerCd`,
 1 AS `AwarenessDt`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `claimreinsurancesummarystats`
--

DROP TABLE IF EXISTS `claimreinsurancesummarystats`;
/*!50001 DROP VIEW IF EXISTS `claimreinsurancesummarystats`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `claimreinsurancesummarystats` AS SELECT 
 1 AS `SystemId`,
 1 AS `id`,
 1 AS `ReportPeriod`,
 1 AS `UpdateDt`,
 1 AS `PolicyRef`,
 1 AS `PolicyNumber`,
 1 AS `PolicyVersion`,
 1 AS `TransactionNumber`,
 1 AS `EffectiveDt`,
 1 AS `ExpirationDt`,
 1 AS `SummaryKey`,
 1 AS `PolicyYear`,
 1 AS `StatusCd`,
 1 AS `ReinsuranceName`,
 1 AS `ReinsuranceItemName`,
 1 AS `ReserveCd`,
 1 AS `ReserveTypeCd`,
 1 AS `MasterSubInd`,
 1 AS `TransactionCd`,
 1 AS `AnnualStatementLineCd`,
 1 AS `StateCd`,
 1 AS `ClaimNumber`,
 1 AS `ClaimRef`,
 1 AS `CarrierCd`,
 1 AS `CarrierGroupCd`,
 1 AS `LossYear`,
 1 AS `OutstandingAmt`,
 1 AS `MTDReserveChangeAmt`,
 1 AS `YTDReserveChangeAmt`,
 1 AS `OutstandingExpectedRecoveryAmt`,
 1 AS `MTDExpectedRecoveryChangeAmt`,
 1 AS `YTDExpectedRecoveryChangeAmt`,
 1 AS `ITDPaidAmt`,
 1 AS `MTDPaidAmt`,
 1 AS `YTDPaidAmt`,
 1 AS `ITDPostedRecoveryAmt`,
 1 AS `MTDPostedRecoveryAmt`,
 1 AS `YTDPostedRecoveryAmt`,
 1 AS `ProductVersionIdRef`,
 1 AS `LossDt`,
 1 AS `ReportDt`,
 1 AS `LossCauseCd`,
 1 AS `SubLossCauseCd`,
 1 AS `ClaimStatusCd`,
 1 AS `ProductName`,
 1 AS `PolicyProductVersionIdRef`,
 1 AS `PolicyProductName`,
 1 AS `PolicyTypeCd`,
 1 AS `PolicyGroupCd`,
 1 AS `MTDIncurredAmt`,
 1 AS `YTDIncurredAmt`,
 1 AS `ITDIncurredAmt`,
 1 AS `MTDIncurredNetRecoveryAmt`,
 1 AS `YTDIncurredNetRecoveryAmt`,
 1 AS `ITDIncurredNetRecoveryAmt`,
 1 AS `CustomerRef`,
 1 AS `CatastropheRef`,
 1 AS `CatastropheNumber`,
 1 AS `HistoricPaidAmt`,
 1 AS `ReinsuranceCoverageGroupCd`,
 1 AS `ReinsuranceReserveCd`,
 1 AS `CoverageTriggerCd`,
 1 AS `AwarenessDt`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `claimstats`
--

DROP TABLE IF EXISTS `claimstats`;
/*!50001 DROP VIEW IF EXISTS `claimstats`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `claimstats` AS SELECT 
 1 AS `SystemId`,
 1 AS `id`,
 1 AS `StatSequence`,
 1 AS `StatSequenceReplace`,
 1 AS `CombinedKey`,
 1 AS `PolicyNumber`,
 1 AS `PolicyVersion`,
 1 AS `PolicyRef`,
 1 AS `LineCd`,
 1 AS `RiskCd`,
 1 AS `CoverageCd`,
 1 AS `CoverageItemCd`,
 1 AS `PolicyLimit`,
 1 AS `PolicyDeductible`,
 1 AS `Limit`,
 1 AS `Deductible`,
 1 AS `LimitDescription`,
 1 AS `DeductibleDescription`,
 1 AS `CarrierCd`,
 1 AS `CarrierGroupCd`,
 1 AS `ClaimNumber`,
 1 AS `ClaimRef`,
 1 AS `ClaimantCd`,
 1 AS `FeatureCd`,
 1 AS `FeatureSubCd`,
 1 AS `FeatureTypeCd`,
 1 AS `ItemNumber`,
 1 AS `ReserveCd`,
 1 AS `ReserveTypeCd`,
 1 AS `TransactionCd`,
 1 AS `TransactionNumber`,
 1 AS `ClaimantTransactionCd`,
 1 AS `ClaimantTransactionNumber`,
 1 AS `ClaimantTransactionIdRef`,
 1 AS `EffectiveDt`,
 1 AS `ExpirationDt`,
 1 AS `AddDt`,
 1 AS `BookDt`,
 1 AS `PolicyYear`,
 1 AS `LossYear`,
 1 AS `StateCd`,
 1 AS `AnnualStatementLineCd`,
 1 AS `ProducerProviderCd`,
 1 AS `ProducerProviderRef`,
 1 AS `InsuranceTypeCd`,
 1 AS `ReserveChangeAmt`,
 1 AS `ExpectedRecoveryChangeAmt`,
 1 AS `PaidAmt`,
 1 AS `PostedRecoveryAmt`,
 1 AS `AdjusterProviderCd`,
 1 AS `AdjusterProviderRef`,
 1 AS `ExaminerProviderCd`,
 1 AS `ExaminerProviderRef`,
 1 AS `BranchCd`,
 1 AS `PayToName`,
 1 AS `PaymentAccountCd`,
 1 AS `StartDt`,
 1 AS `EndDt`,
 1 AS `StatusCd`,
 1 AS `RateAreaName`,
 1 AS `StatData`,
 1 AS `ClaimStatusCd`,
 1 AS `ClaimantStatusCd`,
 1 AS `FeatureStatusCd`,
 1 AS `ReserveStatusCd`,
 1 AS `LossDt`,
 1 AS `ReportDt`,
 1 AS `ClaimStatusChgInd`,
 1 AS `ClaimantStatusChgInd`,
 1 AS `FeatureStatusChgInd`,
 1 AS `ReserveStatusChgInd`,
 1 AS `SummaryKey`,
 1 AS `ProductVersionIdRef`,
 1 AS `LossCauseCd`,
 1 AS `SubLossCauseCd`,
 1 AS `ProductName`,
 1 AS `PolicyProductVersionIdRef`,
 1 AS `PolicyProductName`,
 1 AS `PolicyTypeCd`,
 1 AS `PolicyGroupCd`,
 1 AS `ReversalStopInd`,
 1 AS `CustomerRef`,
 1 AS `AggregateLimit`,
 1 AS `AggregateLimitDescription`,
 1 AS `CheckDt`,
 1 AS `CheckNumber`,
 1 AS `CheckAmt`,
 1 AS `CatastropheRef`,
 1 AS `CatastropheNumber`,
 1 AS `PropertyDamagedIdRef`,
 1 AS `PropertyDamagedNumber`,
 1 AS `RecordOnly`,
 1 AS `FileReasonCd`,
 1 AS `SystemCheckReference`,
 1 AS `ClaimantLinkIdRef`,
 1 AS `ServicePeriodStartDt`,
 1 AS `ServicePeriodEndDt`,
 1 AS `HistoricReserveAmt`,
 1 AS `HistoricExpectedRecoveryAmt`,
 1 AS `HistoricPaidAmt`,
 1 AS `HistoricPostedRecoveryAmt`,
 1 AS `ReasonCd`,
 1 AS `DenyTypeCd`,
 1 AS `DenyReasonCd`,
 1 AS `ClaimantMaritalStatusCd`,
 1 AS `ClaimantNumberOfDependents`,
 1 AS `ClaimantNatureCd`,
 1 AS `ClaimantBodyPartGeneralCd`,
 1 AS `ClaimantBodyPartSpecificCd`,
 1 AS `ClaimantBodyPartIAIABCCd`,
 1 AS `ClaimantInitialTreatment`,
 1 AS `ClaimantDisabilityTypeCd`,
 1 AS `ClaimantClassCd`,
 1 AS `ClaimantEmploymentStatusCd`,
 1 AS `ClaimantShiftCd`,
 1 AS `ClaimantHourlySalaryCd`,
 1 AS `ClaimantAverageWeeklyWagesAmt`,
 1 AS `ClaimantHireDt`,
 1 AS `ClaimantLastWorkedDt`,
 1 AS `CoverageTriggerCd`,
 1 AS `AwarenessDt`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `claimsummarystats`
--

DROP TABLE IF EXISTS `claimsummarystats`;
/*!50001 DROP VIEW IF EXISTS `claimsummarystats`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `claimsummarystats` AS SELECT 
 1 AS `SystemId`,
 1 AS `id`,
 1 AS `ReportPeriod`,
 1 AS `UpdateDt`,
 1 AS `SummaryKey`,
 1 AS `PolicyNumber`,
 1 AS `PolicyVersion`,
 1 AS `PolicyRef`,
 1 AS `LineCd`,
 1 AS `RiskCd`,
 1 AS `CoverageCd`,
 1 AS `CoverageItemCd`,
 1 AS `PolicyLimit`,
 1 AS `PolicyDeductible`,
 1 AS `Limit`,
 1 AS `Deductible`,
 1 AS `LimitDescription`,
 1 AS `DeductibleDescription`,
 1 AS `CarrierCd`,
 1 AS `CarrierGroupCd`,
 1 AS `ClaimNumber`,
 1 AS `ClaimRef`,
 1 AS `ClaimantCd`,
 1 AS `FeatureCd`,
 1 AS `FeatureSubCd`,
 1 AS `FeatureTypeCd`,
 1 AS `ItemNumber`,
 1 AS `ReserveCd`,
 1 AS `SubReserveCd`,
 1 AS `ReserveTypeCd`,
 1 AS `StatusCd`,
 1 AS `EffectiveDt`,
 1 AS `ExpirationDt`,
 1 AS `LossYear`,
 1 AS `PolicyYear`,
 1 AS `AnnualStatementLineCd`,
 1 AS `StateCd`,
 1 AS `InsuranceTypeCd`,
 1 AS `LitigationInd`,
 1 AS `LossDt`,
 1 AS `ReportDt`,
 1 AS `FirstClosedDt`,
 1 AS `OpenedDt`,
 1 AS `ClosedDt`,
 1 AS `DenialDt`,
 1 AS `FirstIndemnityPaymentDt`,
 1 AS `MTDReserveChangeAmt`,
 1 AS `OutstandingAmt`,
 1 AS `YTDReserveChangeAmt`,
 1 AS `IncreaseDecreaseAmt`,
 1 AS `MTDExpectedRecoveryChangeAmt`,
 1 AS `OutstandingExpectedRecoveryAmt`,
 1 AS `YTDExpectedRecoveryChangeAmt`,
 1 AS `MTDPaidAmt`,
 1 AS `YTDPaidAmt`,
 1 AS `ITDPaidAmt`,
 1 AS `MTDPostedRecoveryAmt`,
 1 AS `ITDPostedRecoveryAmt`,
 1 AS `YTDPostedRecoveryAmt`,
 1 AS `OpeningReserve`,
 1 AS `OriginalReserve`,
 1 AS `OpenedInPeriodCount`,
 1 AS `ReOpenedInPeriodCount`,
 1 AS `ClosedInPeriodCount`,
 1 AS `OpenEndOfPeriodCount`,
 1 AS `ClaimantOpenCount`,
 1 AS `ClaimOpenCount`,
 1 AS `FeatureOpenCount`,
 1 AS `ClaimLastCloseDt`,
 1 AS `ClaimantLastCloseDt`,
 1 AS `FeatureLastCloseDt`,
 1 AS `ReserveLastCloseDt`,
 1 AS `ProductVersionIdRef`,
 1 AS `LossCauseCd`,
 1 AS `SubLossCauseCd`,
 1 AS `ProducerProviderCd`,
 1 AS `ClaimStatusCd`,
 1 AS `ProductName`,
 1 AS `PolicyProductVersionIdRef`,
 1 AS `PolicyProductName`,
 1 AS `PolicyTypeCd`,
 1 AS `PolicyGroupCd`,
 1 AS `MTDIncurredAmt`,
 1 AS `YTDIncurredAmt`,
 1 AS `ITDIncurredAmt`,
 1 AS `MTDIncurredNetRecoveryAmt`,
 1 AS `YTDIncurredNetRecoveryAmt`,
 1 AS `ITDIncurredNetRecoveryAmt`,
 1 AS `CustomerRef`,
 1 AS `AggregateLimit`,
 1 AS `AggregateLimitDescription`,
 1 AS `CatastropheRef`,
 1 AS `CatastropheNumber`,
 1 AS `PropertyDamagedIdRef`,
 1 AS `PropertyDamagedNumber`,
 1 AS `RecordOnly`,
 1 AS `FileReasonCd`,
 1 AS `ClaimantLinkIdRef`,
 1 AS `HistoricPaidAmt`,
 1 AS `HistoricPostedRecoveryAmt`,
 1 AS `DenyTypeCd`,
 1 AS `DenyReasonCd`,
 1 AS `CoverageTriggerCd`,
 1 AS `AwarenessDt`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `claimtransactionhistory`
--

DROP TABLE IF EXISTS `claimtransactionhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimtransactionhistory` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `claimtransactionhistoryIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimtransactioninfo`
--

DROP TABLE IF EXISTS `claimtransactioninfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimtransactioninfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `TransactionEffectiveDt` date DEFAULT NULL,
  `TransactionEffectiveTm` varchar(255) DEFAULT NULL,
  `TransactionUser` varchar(255) DEFAULT NULL,
  `TransactionDt` date DEFAULT NULL,
  `TransactionTm` varchar(255) DEFAULT NULL,
  `TransactionLongDescription` varchar(255) DEFAULT NULL,
  `TransactionShortDescription` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `ChangeInfoRef` varchar(255) DEFAULT NULL,
  `SubmissionNumber` int(11) DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `ConversionTemplateIdRef` varchar(255) DEFAULT NULL,
  `ConversionGroup` varchar(255) DEFAULT NULL,
  `ConversionJobRef` varchar(255) DEFAULT NULL,
  `StatProcessedInd` varchar(255) DEFAULT NULL,
  `HistoricalTransactionDt` date DEFAULT NULL,
  KEY `claimtransactioninfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claimtransactiontext`
--

DROP TABLE IF EXISTS `claimtransactiontext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimtransactiontext` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TextCd` varchar(255) DEFAULT NULL,
  `TransactionTextTypeCd` varchar(255) DEFAULT NULL,
  `Text` varchar(255) DEFAULT NULL,
  `IdRef` varchar(255) DEFAULT NULL,
  `ModelName` varchar(255) DEFAULT NULL,
  KEY `claimtransactiontextIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `commissionarea`
--

DROP TABLE IF EXISTS `commissionarea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commissionarea` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CommissionAreaCd` varchar(255) DEFAULT NULL,
  `CommissionPct` decimal(28,6) DEFAULT NULL,
  `ContributionPct` decimal(28,6) DEFAULT NULL,
  `PrevContributionPct` decimal(28,6) DEFAULT NULL,
  `FinalCommissionPct` decimal(28,6) DEFAULT NULL,
  `PrevFinalCommissionPct` decimal(28,6) DEFAULT NULL,
  `OverrideInd` varchar(255) DEFAULT NULL,
  `OverridePct` decimal(28,6) DEFAULT NULL,
  `FullTermAmt` decimal(28,6) DEFAULT NULL,
  `FinalPremiumAmt` decimal(28,6) DEFAULT NULL,
  `CommissionAmt` decimal(28,6) DEFAULT NULL,
  `TransactionCommissionAmt` decimal(28,6) DEFAULT NULL,
  `FullTermFeeAmt` decimal(28,6) DEFAULT NULL,
  `FinalFeeAmt` decimal(28,6) DEFAULT NULL,
  `KeepCommissionAtRenewalInd` varchar(255) DEFAULT NULL,
  `CoveredStateIdRef` varchar(255) DEFAULT NULL,
  KEY `commissionareaIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `commissionplan`
--

DROP TABLE IF EXISTS `commissionplan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commissionplan` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `PlanCd` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `State` varchar(255) DEFAULT NULL,
  `LicenceClass` varchar(255) DEFAULT NULL,
  `CommissionArea` varchar(255) DEFAULT NULL,
  `BusinessSource` varchar(255) DEFAULT NULL,
  `CodeCd` varchar(255) DEFAULT NULL,
  `NewBusiness` varchar(255) DEFAULT NULL,
  `Renewal` varchar(255) DEFAULT NULL,
  KEY `commissionplanIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `commldriver`
--

DROP TABLE IF EXISTS `commldriver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commldriver` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `HiredDt` date DEFAULT NULL,
  `SurchargeFactor` decimal(28,6) DEFAULT NULL,
  `CommlExperience` int(11) DEFAULT NULL,
  `ClassCd` varchar(255) DEFAULT NULL,
  KEY `commldriverIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `commlname`
--

DROP TABLE IF EXISTS `commlname`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commlname` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CommercialName` varchar(255) DEFAULT NULL,
  `DBAName` varchar(255) DEFAULT NULL,
  KEY `commlnameIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `ContactTypeCd` varchar(255) DEFAULT NULL,
  `PreferredDeliveryMethod` varchar(255) DEFAULT NULL,
  KEY `contactIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contactinfo`
--

DROP TABLE IF EXISTS `contactinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `contactinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `coverage`
--

DROP TABLE IF EXISTS `coverage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coverage` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `CoverageCd` varchar(255) DEFAULT NULL,
  `FullTermAmt` decimal(28,6) DEFAULT NULL,
  `FullTermManualAmt` decimal(28,6) DEFAULT NULL,
  `WrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `FinalPremiumAmt` decimal(28,6) DEFAULT NULL,
  `EarnedAmt` decimal(28,6) DEFAULT NULL,
  `AdditionalInd` varchar(255) DEFAULT NULL,
  `OriginalCommissionPct` decimal(28,6) DEFAULT NULL,
  `CommissionPct` decimal(28,6) DEFAULT NULL,
  `ContributionPct` decimal(28,6) DEFAULT NULL,
  `CommissionAmt` decimal(28,6) DEFAULT NULL,
  `TransactionCommissionAmt` decimal(28,6) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `PrevFinalAmt` decimal(28,6) DEFAULT NULL,
  `PrevWrittenAmt` decimal(28,6) DEFAULT NULL,
  `ShortRateAmt` decimal(28,6) DEFAULT NULL,
  `CoinsurancePct` decimal(28,6) DEFAULT NULL,
  `LapseInd` varchar(255) DEFAULT NULL,
  `FutureStatusCd` varchar(255) DEFAULT NULL,
  `ReinsuranceCoverageGroupCd` varchar(255) DEFAULT NULL,
  `ScheduledPremiumMod` int(11) DEFAULT NULL,
  `NumberOfEmployees` int(11) DEFAULT NULL,
  `NumberOfClass1Employees` int(11) DEFAULT NULL,
  `InsideLimit` varchar(255) DEFAULT NULL,
  `InsideDeductible` varchar(255) DEFAULT NULL,
  `OutsideLimit` varchar(255) DEFAULT NULL,
  `OutsideDeductible` varchar(255) DEFAULT NULL,
  `PapersKeptInCd` varchar(255) DEFAULT NULL,
  `DuplicateOffPremisesRecordsPct` varchar(255) DEFAULT NULL,
  `EDPEquipmentLimit` varchar(255) DEFAULT NULL,
  `EDPEquipmentInTransitLimit` varchar(255) DEFAULT NULL,
  `EDPDataLimit` varchar(255) DEFAULT NULL,
  `EDPMediaLimit` varchar(255) DEFAULT NULL,
  `EDPDataAndMediaInTransitLimit` varchar(255) DEFAULT NULL,
  `EDPBusinessIncomeLimit` varchar(255) DEFAULT NULL,
  `EDPExtraExpenseLimit` varchar(255) DEFAULT NULL,
  `FormType` varchar(255) DEFAULT NULL,
  `NumberOfDays` int(11) DEFAULT NULL,
  `NumberOfLocations` int(11) DEFAULT NULL,
  `CoverageType` varchar(255) DEFAULT NULL,
  `ClassificationRisk` varchar(255) DEFAULT NULL,
  `EQNumberOfStories` int(11) DEFAULT NULL,
  `EQRoofTankBuilding` varchar(255) DEFAULT NULL,
  `EQSublimitPercentage` varchar(255) DEFAULT NULL,
  `EQGradeDescription` varchar(255) DEFAULT NULL,
  `EQMasonryVeneerLimit` varchar(255) DEFAULT NULL,
  `EQBuildingConstruction` varchar(255) DEFAULT NULL,
  KEY `coverageIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `coveragedata`
--

DROP TABLE IF EXISTS `coveragedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coveragedata` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CoverageDataCd` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  KEY `coveragedataIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `coverageerror`
--

DROP TABLE IF EXISTS `coverageerror`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coverageerror` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Msg` varchar(255) DEFAULT NULL,
  KEY `coverageerrorIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `coverageerrors`
--

DROP TABLE IF EXISTS `coverageerrors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coverageerrors` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  KEY `coverageerrorsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `coverageitem`
--

DROP TABLE IF EXISTS `coverageitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coverageitem` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `CoverageItemCd` varchar(255) DEFAULT NULL,
  `FullTermAmt` decimal(28,6) DEFAULT NULL,
  `FullTermManualAmt` decimal(28,6) DEFAULT NULL,
  `FinalPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `EarnedAmt` decimal(28,6) DEFAULT NULL,
  `SequenceNumber` int(11) DEFAULT NULL,
  `ItemDesc` varchar(255) DEFAULT NULL,
  `PrevFinalAmt` decimal(28,6) DEFAULT NULL,
  `PrevWrittenAmt` decimal(28,6) DEFAULT NULL,
  `TransactionCommissionAmt` decimal(28,6) DEFAULT NULL,
  `ShortRateAmt` decimal(28,6) DEFAULT NULL,
  `FutureStatusCd` varchar(255) DEFAULT NULL,
  `DriverIdRef` varchar(255) DEFAULT NULL,
  `NumFamilies` varchar(255) DEFAULT NULL,
  `AdditionalPlanChargePct` int(11) DEFAULT NULL,
  KEY `coverageitemIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `coveredstate`
--

DROP TABLE IF EXISTS `coveredstate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coveredstate` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `FullTermAmt` decimal(28,6) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `EarnedAmt` decimal(28,6) DEFAULT NULL,
  `PrimaryInd` varchar(255) DEFAULT NULL,
  `CommissionAmt` decimal(28,6) DEFAULT NULL,
  `WrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `FinalPremiumAmt` decimal(28,6) DEFAULT NULL,
  `TransactionCommissionAmt` decimal(28,6) DEFAULT NULL,
  `Index` int(11) DEFAULT NULL,
  `RuleStatus` varchar(255) DEFAULT NULL,
  `Order` int(11) DEFAULT NULL,
  `RatingEffectiveDt` date DEFAULT NULL,
  `AlcoholDrugFreeCreditInd` varchar(255) DEFAULT NULL,
  `ContrClassPremAdjCreditPct` decimal(28,6) DEFAULT NULL,
  `CoinsuranceInd` varchar(255) DEFAULT NULL,
  `DeductibleAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleType` varchar(255) DEFAULT NULL,
  `ELVoluntaryCompensationInd` varchar(255) DEFAULT NULL,
  `ExperienceModificationFactor` varchar(255) DEFAULT NULL,
  `GLScheduledPremiumMod` varchar(255) DEFAULT NULL,
  `ManagedCareCreditInd` varchar(255) DEFAULT NULL,
  `NumberOfClaims` varchar(255) DEFAULT NULL,
  `CertifiedSafetyHealthProgramPct` decimal(28,6) DEFAULT NULL,
  `TotalDiscountedPremiumAmt` decimal(28,6) DEFAULT NULL,
  `TotalManualPremiumAmt` decimal(28,6) DEFAULT NULL,
  `TotalStandardPremiumAmt` decimal(28,6) DEFAULT NULL,
  `TotalSubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `CommissionPct` decimal(28,6) DEFAULT NULL,
  `TotalStateEstimatedPremium` decimal(28,6) DEFAULT NULL,
  `FinalAmt` decimal(28,6) DEFAULT NULL,
  `PrevFinalAmt` decimal(28,6) DEFAULT NULL,
  `PrevWrittenAmt` decimal(28,6) DEFAULT NULL,
  `ContributionPct` decimal(28,6) DEFAULT NULL,
  `ShortRateAmt` decimal(28,6) DEFAULT NULL,
  `AuditValidationStatus` varchar(255) DEFAULT NULL,
  `ExperienceRatingCd` varchar(255) DEFAULT NULL,
  `ExperienceModificationStatus` varchar(255) DEFAULT NULL,
  `UnemploymentNumber` varchar(255) DEFAULT NULL,
  `WorkfareProgramWeeksNum` varchar(255) DEFAULT NULL,
  `LargeDeductibleFactor` decimal(28,6) DEFAULT NULL,
  `DividendPlanCd` varchar(255) DEFAULT NULL,
  `ARDRuleEnabled` varchar(255) DEFAULT NULL,
  `WorkStudyProgram` varchar(255) DEFAULT NULL,
  `NoOfContractsForWaiverOfSubrogation` varchar(255) DEFAULT NULL,
  `WaiverOfSubrogationFlatInd` varchar(255) DEFAULT NULL,
  `NoncomplianceChargeAmt` decimal(28,6) DEFAULT NULL,
  `PackageFactorInd` varchar(255) DEFAULT NULL,
  KEY `coveredstateIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpaccountoverride`
--

DROP TABLE IF EXISTS `cpaccountoverride`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpaccountoverride` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CPAccountNumber` varchar(255) DEFAULT NULL,
  `CPAccountSuffix` varchar(255) DEFAULT NULL,
  KEY `cpaccountoverrideIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpadditionalinsured`
--

DROP TABLE IF EXISTS `cpadditionalinsured`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpadditionalinsured` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `CommercialName` varchar(255) DEFAULT NULL,
  KEY `cpadditionalinsuredIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpaddress`
--

DROP TABLE IF EXISTS `cpaddress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpaddress` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AddressTypeCd` varchar(255) DEFAULT NULL,
  `PrimaryNumber` varchar(255) DEFAULT NULL,
  `StreetName` varchar(255) DEFAULT NULL,
  `SecondaryNumber` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL,
  `StateProvCd` varchar(255) DEFAULT NULL,
  `PostalCode` varchar(255) DEFAULT NULL,
  `LastAtThisAddressDt` date DEFAULT NULL,
  `ClassCd` varchar(255) DEFAULT NULL,
  `ClassDisplay` varchar(255) DEFAULT NULL,
  `FirstAtThisAddressDt` date DEFAULT NULL,
  `GroupUsageInd` varchar(255) DEFAULT NULL,
  `YearsAtAddress` varchar(255) DEFAULT NULL,
  `MonthsAtAddress` int(11) DEFAULT NULL,
  `County` varchar(255) DEFAULT NULL,
  `OwnOrRentCd` varchar(255) DEFAULT NULL,
  KEY `cpaddressIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpaddressreportinfo`
--

DROP TABLE IF EXISTS `cpaddressreportinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpaddressreportinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PrimaryNumberChangedInd` varchar(255) DEFAULT NULL,
  `StreetNameChangedInd` varchar(255) DEFAULT NULL,
  `SecondaryNumberChangedInd` varchar(255) DEFAULT NULL,
  `CityChangedInd` varchar(255) DEFAULT NULL,
  `StateProvCdChangedInd` varchar(255) DEFAULT NULL,
  `PostalCodeChangedInd` varchar(255) DEFAULT NULL,
  `AssociationDt` varchar(255) DEFAULT NULL,
  `CPAddressTypeCd` varchar(255) DEFAULT NULL,
  `DataSourceIndicator` varchar(255) DEFAULT NULL,
  `FirstAtThisAddressDtChangedInd` varchar(255) DEFAULT NULL,
  `LastAtThisAddressDtChangedInd` varchar(255) DEFAULT NULL,
  `AddressTypeCd` varchar(255) DEFAULT NULL,
  `YearsAtAddressChangedInd` varchar(255) DEFAULT NULL,
  `MonthsAtAddressChangedInd` varchar(255) DEFAULT NULL,
  KEY `cpaddressreportinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpbatchinfo`
--

DROP TABLE IF EXISTS `cpbatchinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpbatchinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  KEY `cpbatchinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpcarriercontrib`
--

DROP TABLE IF EXISTS `cpcarriercontrib`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpcarriercontrib` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `ReportPeriodBeginDt` date DEFAULT NULL,
  `ReportPeriodEndDt` date DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `RegeneratedInd` varchar(255) DEFAULT NULL,
  `PolicyRef` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `ProcessDt` date DEFAULT NULL,
  `ProcessTm` varchar(255) DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  KEY `cpcarriercontribIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpcccoverage`
--

DROP TABLE IF EXISTS `cpcccoverage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpcccoverage` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CoverageCd` varchar(255) DEFAULT NULL,
  `IndividualLimit` varchar(255) DEFAULT NULL,
  `OccurrenceLimit` varchar(255) DEFAULT NULL,
  `CombinedSingleLimit` varchar(255) DEFAULT NULL,
  KEY `cpcccoverageIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpccmisc`
--

DROP TABLE IF EXISTS `cpccmisc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpccmisc` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CancelReasonCd` varchar(255) DEFAULT NULL,
  `ReinstatementDt` date DEFAULT NULL,
  `HouseholdNumber` varchar(255) DEFAULT NULL,
  KEY `cpccmiscIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpccpolicyinfo`
--

DROP TABLE IF EXISTS `cpccpolicyinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpccpolicyinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AMBestNumber` varchar(255) DEFAULT NULL,
  `NAICNumber` varchar(255) DEFAULT NULL,
  `InsuranceType` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `ChangeEffectiveDt` date DEFAULT NULL,
  `CompanyName` varchar(255) DEFAULT NULL,
  `SubTypeCd` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `InceptionDt` date DEFAULT NULL,
  `CancelDt` date DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `PayPlanCd` varchar(255) DEFAULT NULL,
  `WrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `PaymentMethodCd` varchar(255) DEFAULT NULL,
  `MailingAddressPrimaryNumber` varchar(255) DEFAULT NULL,
  `MailingAddressStreetName` varchar(255) DEFAULT NULL,
  `MailingAddressSecondaryNumber` varchar(255) DEFAULT NULL,
  `MailingAddressCity` varchar(255) DEFAULT NULL,
  `MailingAddressStateProvCd` varchar(255) DEFAULT NULL,
  `MailingAddressPostalCode` varchar(255) DEFAULT NULL,
  `MailingAddressPostalCodeExt` varchar(255) DEFAULT NULL,
  `TelephoneAreaCode` varchar(255) DEFAULT NULL,
  `TelephoneNumber` varchar(255) DEFAULT NULL,
  `TelephoneExt` varchar(255) DEFAULT NULL,
  `PolicyState` varchar(255) DEFAULT NULL,
  KEY `cpccpolicyinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpccpropertydetails`
--

DROP TABLE IF EXISTS `cpccpropertydetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpccpropertydetails` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PropertyId` int(11) DEFAULT NULL,
  `SqFt` int(11) DEFAULT NULL,
  `Stories` int(11) DEFAULT NULL,
  `Baths` int(11) DEFAULT NULL,
  `YearBuilt` int(11) DEFAULT NULL,
  `ACInd` varchar(255) DEFAULT NULL,
  `Fireplaces` int(11) DEFAULT NULL,
  `BasementFinishedSqFt` int(11) DEFAULT NULL,
  `BasementRawSqFt` int(11) DEFAULT NULL,
  `Bedrooms` int(11) DEFAULT NULL,
  `ConstructionQuality` int(11) DEFAULT NULL,
  `PoolInd` varchar(255) DEFAULT NULL,
  KEY `cpccpropertydetailsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpccpropertyinfo`
--

DROP TABLE IF EXISTS `cpccpropertyinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpccpropertyinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `VehIdentificationNumber` varchar(255) DEFAULT NULL,
  `ModelYr` varchar(255) DEFAULT NULL,
  `BusinessUseInd` varchar(255) DEFAULT NULL,
  `Deductible1` varchar(255) DEFAULT NULL,
  `Deductible2` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `LocationAddressPrimaryNumber` varchar(255) DEFAULT NULL,
  `LocationAddressStreetName` varchar(255) DEFAULT NULL,
  `LocationAddressSecondaryNumber` varchar(255) DEFAULT NULL,
  `LocationAddressCity` varchar(255) DEFAULT NULL,
  `LocationAddressStateProvCd` varchar(255) DEFAULT NULL,
  `LocationAddressPostalCode` varchar(255) DEFAULT NULL,
  `LocationAddressPostalCodeExt` varchar(255) DEFAULT NULL,
  `FinanceCompanyName` varchar(255) DEFAULT NULL,
  `FinanceCompanyPrimaryNumber` varchar(255) DEFAULT NULL,
  `FinanceCompanyStreetName` varchar(255) DEFAULT NULL,
  `FinanceCompanySecondaryNumber` varchar(255) DEFAULT NULL,
  `FinanceCompanyCity` varchar(255) DEFAULT NULL,
  `FinanceCompanyStateProvCd` varchar(255) DEFAULT NULL,
  `FinanceCompanyPostalCode` varchar(255) DEFAULT NULL,
  `FinanceCompanyPostalCodeExt` varchar(255) DEFAULT NULL,
  `FinanceCompanyLoanNumber` varchar(255) DEFAULT NULL,
  `FinanceCompanyType` varchar(255) DEFAULT NULL,
  `PropertyId` int(11) DEFAULT NULL,
  `LeasedVehicleInd` varchar(255) DEFAULT NULL,
  `PropertyCancellationDt` date DEFAULT NULL,
  `PropertyCancellationInd` varchar(255) DEFAULT NULL,
  `SuppressNotificationInd` varchar(255) DEFAULT NULL,
  `PropertyType` varchar(255) DEFAULT NULL,
  KEY `cpccpropertyinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpccsubjectinfo`
--

DROP TABLE IF EXISTS `cpccsubjectinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpccsubjectinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `LicenseNumber` varchar(255) DEFAULT NULL,
  `RelationshipToInsuredCd` varchar(255) DEFAULT NULL,
  `LicensedStateProvCd` varchar(255) DEFAULT NULL,
  `GivenName` varchar(255) DEFAULT NULL,
  `Surname` varchar(255) DEFAULT NULL,
  `OtherGivenName` varchar(255) DEFAULT NULL,
  `SuffixCd` varchar(255) DEFAULT NULL,
  `BirthDt` date DEFAULT NULL,
  `SSN` varchar(255) DEFAULT NULL,
  `GenderCd` varchar(255) DEFAULT NULL,
  KEY `cpccsubjectinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpccvehicle`
--

DROP TABLE IF EXISTS `cpccvehicle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpccvehicle` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `VehIdentificationNumber` varchar(255) DEFAULT NULL,
  `VehicleStateProvCd` varchar(255) DEFAULT NULL,
  `VehicleType` varchar(255) DEFAULT NULL,
  `VehicleStatus` varchar(255) DEFAULT NULL,
  `VehicleStatusDt` date DEFAULT NULL,
  `NonCoverageReason` varchar(255) DEFAULT NULL,
  `VehicleCancelReason` varchar(255) DEFAULT NULL,
  `LicensePlateNumber` varchar(255) DEFAULT NULL,
  `StateTrackingNumber` varchar(255) DEFAULT NULL,
  `InsuredSequenceNumber` varchar(255) DEFAULT NULL,
  `VehicleAddDt` date DEFAULT NULL,
  `VehicleMileage` int(11) DEFAULT NULL,
  KEY `cpccvehicleIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpclaim`
--

DROP TABLE IF EXISTS `cpclaim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpclaim` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ClaimTypeCd` varchar(255) DEFAULT NULL,
  KEY `cpclaimIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpclaimpayment`
--

DROP TABLE IF EXISTS `cpclaimpayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpclaimpayment` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `DispositionCd` varchar(255) DEFAULT NULL,
  `DispositionDisplay` varchar(255) DEFAULT NULL,
  `PaymentAmt` decimal(28,6) DEFAULT NULL,
  `ClaimTypeCd` varchar(255) DEFAULT NULL,
  `ClaimTypeDisplay` varchar(255) DEFAULT NULL,
  KEY `cpclaimpaymentIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpclueclaiminfo`
--

DROP TABLE IF EXISTS `cpclueclaiminfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpclueclaiminfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AMBestNumber` varchar(255) DEFAULT NULL,
  `InsuredPrefixCd1` varchar(255) DEFAULT NULL,
  `InsuredSurname1` varchar(255) DEFAULT NULL,
  `InsuredGivenName1` varchar(255) DEFAULT NULL,
  `InsuredOtherGivenName1` varchar(255) DEFAULT NULL,
  `InsuredSuffixCd1` varchar(255) DEFAULT NULL,
  `InsuredSSN1` varchar(255) DEFAULT NULL,
  `InsuredBirthDt1` date DEFAULT NULL,
  `InsuredGenderCd1` varchar(255) DEFAULT NULL,
  `InsuredLicenseNumber1` varchar(255) DEFAULT NULL,
  `InsuredLicenseStateCd1` varchar(255) DEFAULT NULL,
  `InsuredAddressPrimaryNumber` varchar(255) DEFAULT NULL,
  `InsuredAddressStreetName` varchar(255) DEFAULT NULL,
  `InsuredAddressSecondaryNumber` varchar(255) DEFAULT NULL,
  `InsuredAddressCity` varchar(255) DEFAULT NULL,
  `InsuredAddressStateProvCd` varchar(255) DEFAULT NULL,
  `InsuredAddressPostalCode` varchar(255) DEFAULT NULL,
  `InsuredAddressPostalCodeExt` varchar(255) DEFAULT NULL,
  `InsuredPrefixCd2` varchar(255) DEFAULT NULL,
  `InsuredSurname2` varchar(255) DEFAULT NULL,
  `InsuredGivenName2` varchar(255) DEFAULT NULL,
  `InsuredOtherGivenName2` varchar(255) DEFAULT NULL,
  `InsuredSSN2` varchar(255) DEFAULT NULL,
  `InsuredSuffixCd2` varchar(255) DEFAULT NULL,
  `InsuredBirthDt2` date DEFAULT NULL,
  `InsuredGenderCd2` varchar(255) DEFAULT NULL,
  `InsuredLicenseNumber2` varchar(255) DEFAULT NULL,
  `InsuredLicenseStateCd2` varchar(255) DEFAULT NULL,
  `DriverPrefixCd` varchar(255) DEFAULT NULL,
  `DriverSurname` varchar(255) DEFAULT NULL,
  `DriverGivenName` varchar(255) DEFAULT NULL,
  `DriverOtherGivenName` varchar(255) DEFAULT NULL,
  `DriverSuffixCd` varchar(255) DEFAULT NULL,
  `DriverSSN` varchar(255) DEFAULT NULL,
  `DriverBirthDt` date DEFAULT NULL,
  `DriverGenderCd` varchar(255) DEFAULT NULL,
  `DriverLicenseNumber` varchar(255) DEFAULT NULL,
  `DriverLicenseStateCd` varchar(255) DEFAULT NULL,
  `DriverAddressPrimaryNumber` varchar(255) DEFAULT NULL,
  `DriverAddressStreetName` varchar(255) DEFAULT NULL,
  `DriverAddressSecondaryNumber` varchar(255) DEFAULT NULL,
  `DriverAddressCity` varchar(255) DEFAULT NULL,
  `DriverAddressStateProvCd` varchar(255) DEFAULT NULL,
  `DriverAddressPostalCode` varchar(255) DEFAULT NULL,
  `DriverAddressPostalCodeExt` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  `ClaimNumber` varchar(255) DEFAULT NULL,
  `ClaimTypeCd` varchar(255) DEFAULT NULL,
  `LossDt` date DEFAULT NULL,
  `ClaimAmt` decimal(28,6) DEFAULT NULL,
  `ReportStatusCd` varchar(255) DEFAULT NULL,
  `DispositionCd` varchar(255) DEFAULT NULL,
  `FaultInd` varchar(255) DEFAULT NULL,
  `FirstPaymentDt` date DEFAULT NULL,
  `DriverRelationshipCd` varchar(255) DEFAULT NULL,
  `VIN` varchar(255) DEFAULT NULL,
  `MakeModel` varchar(255) DEFAULT NULL,
  `ModelYr` varchar(255) DEFAULT NULL,
  `VehicleDispositionCd` varchar(255) DEFAULT NULL,
  KEY `cpclueclaiminfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpcluecontrib`
--

DROP TABLE IF EXISTS `cpcluecontrib`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpcluecontrib` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `ReportPeriodBeginDt` date DEFAULT NULL,
  `ReportPeriodEndDt` date DEFAULT NULL,
  `RegeneratedInd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `ClaimRef` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `ProcessDt` date DEFAULT NULL,
  `ProcessTm` varchar(255) DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  KEY `cpcluecontribIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpcluepasearchcriteria`
--

DROP TABLE IF EXISTS `cpcluepasearchcriteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpcluepasearchcriteria` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ReportUsageCd` varchar(255) DEFAULT NULL,
  `ReportServiceTypeCd` varchar(255) DEFAULT NULL,
  `HomeownerVerificationInd` varchar(255) DEFAULT NULL,
  `FormerPolicyCompanyName` varchar(255) DEFAULT NULL,
  `FormerPolicyNumber` varchar(255) DEFAULT NULL,
  `FormerPolicyTypeCd` varchar(255) DEFAULT NULL,
  `FormerAddressPrimaryNumber` varchar(255) DEFAULT NULL,
  `FormerAddressSecondaryNumber` varchar(255) DEFAULT NULL,
  `FormerAddressStreetName` varchar(255) DEFAULT NULL,
  `FormerAddressCity` varchar(255) DEFAULT NULL,
  `FormerAddressStateProvCd` varchar(255) DEFAULT NULL,
  `FormerAddressPostalCode` varchar(255) DEFAULT NULL,
  KEY `cpcluepasearchcriteriaIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpcluepersonalautoreport`
--

DROP TABLE IF EXISTS `cpcluepersonalautoreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpcluepersonalautoreport` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ProcessingStatusDisplay` varchar(255) DEFAULT NULL,
  `ProcessingStatusCd` varchar(255) DEFAULT NULL,
  KEY `cpcluepersonalautoreportIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpcluepropertyreport`
--

DROP TABLE IF EXISTS `cpcluepropertyreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpcluepropertyreport` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CLUEPropertyReportTypeCd` varchar(255) DEFAULT NULL,
  `ProcessingStatusDisplay` varchar(255) DEFAULT NULL,
  `ProcessingStatusCd` varchar(255) DEFAULT NULL,
  `RiskTotalClaims` int(11) DEFAULT NULL,
  `PriorInquiryTotal` int(11) DEFAULT NULL,
  `RequestedBy` varchar(255) DEFAULT NULL,
  `SubjectTotalClaims` int(11) DEFAULT NULL,
  KEY `cpcluepropertyreportIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpcluepropsrchcriteria`
--

DROP TABLE IF EXISTS `cpcluepropsrchcriteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpcluepropsrchcriteria` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ReportUsageCd` varchar(255) DEFAULT NULL,
  `ReportServiceTypeCd` varchar(255) DEFAULT NULL,
  `RequestedBy` varchar(255) DEFAULT NULL,
  `MailingAddressPrimaryNumber` varchar(255) DEFAULT NULL,
  `MailingAddressStreetName` varchar(255) DEFAULT NULL,
  `MailingAddressSecondaryNumber` varchar(255) DEFAULT NULL,
  `MailingAddressCity` varchar(255) DEFAULT NULL,
  `MailingAddressPostalCode` varchar(255) DEFAULT NULL,
  `MailingAddressStateProvCd` varchar(255) DEFAULT NULL,
  `FormerAddressPrimaryNumber` varchar(255) DEFAULT NULL,
  `FormerAddressStreetName` varchar(255) DEFAULT NULL,
  `FormerAddressSecondaryNumber` varchar(255) DEFAULT NULL,
  `FormerAddressCity` varchar(255) DEFAULT NULL,
  `FormerAddressPostalCode` varchar(255) DEFAULT NULL,
  `FormerAddressStateProvCd` varchar(255) DEFAULT NULL,
  `MortgageCompany` varchar(255) DEFAULT NULL,
  `MortgageLoanNumber` varchar(255) DEFAULT NULL,
  `FormerPolicyNumber` varchar(255) DEFAULT NULL,
  `FormerPolicyCompanyName` varchar(255) DEFAULT NULL,
  `FormerPolicyTypeCd` varchar(255) DEFAULT NULL,
  KEY `cpcluepropsrchcriteriaIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpcluepropsrchsubject`
--

DROP TABLE IF EXISTS `cpcluepropsrchsubject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpcluepropsrchsubject` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `SearchSubjectTypeCd` varchar(255) DEFAULT NULL,
  `PrefixCd` varchar(255) DEFAULT NULL,
  `Surname` varchar(255) DEFAULT NULL,
  `OtherGivenName` varchar(255) DEFAULT NULL,
  `GivenName` varchar(255) DEFAULT NULL,
  `SuffixCd` varchar(255) DEFAULT NULL,
  `GenderCd` varchar(255) DEFAULT NULL,
  `SSN` varchar(255) DEFAULT NULL,
  `BirthDt` date DEFAULT NULL,
  `TelephoneAreaCode` varchar(255) DEFAULT NULL,
  `TelephoneExchange` varchar(255) DEFAULT NULL,
  `TelephoneNumber` varchar(255) DEFAULT NULL,
  `TelephoneExt` varchar(255) DEFAULT NULL,
  `TelephoneHoursFrom` varchar(255) DEFAULT NULL,
  `TelephoneHoursTo` varchar(255) DEFAULT NULL,
  KEY `cpcluepropsrchsubjectIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpclupclaiminfo`
--

DROP TABLE IF EXISTS `cpclupclaiminfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpclupclaiminfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AMBestNumber` varchar(255) DEFAULT NULL,
  `ClaimNumber` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  `LossDt` date DEFAULT NULL,
  `LossCauseCd` varchar(255) DEFAULT NULL,
  `LossLocationInd` varchar(255) DEFAULT NULL,
  `ClaimAmt` decimal(28,6) DEFAULT NULL,
  `ReportStatusCd` varchar(255) DEFAULT NULL,
  `DispositionCd` varchar(255) DEFAULT NULL,
  `CatastropheInd` varchar(255) DEFAULT NULL,
  `MortgageeName` varchar(255) DEFAULT NULL,
  `MortgageeLoanNumber` varchar(255) DEFAULT NULL,
  `RiskAddressPrimaryNumber` varchar(255) DEFAULT NULL,
  `RiskAddressStreetName` varchar(255) DEFAULT NULL,
  `RiskAddressSecondaryNumber` varchar(255) DEFAULT NULL,
  `RiskAddressCity` varchar(255) DEFAULT NULL,
  `RiskAddressStateProvCd` varchar(255) DEFAULT NULL,
  `RiskAddressPostalCode` varchar(255) DEFAULT NULL,
  `RiskAddressPostalCodeExt` varchar(255) DEFAULT NULL,
  `MailingAddressPrimaryNumber` varchar(255) DEFAULT NULL,
  `MailingAddressStreetName` varchar(255) DEFAULT NULL,
  `MailingAddressSecondaryNumber` varchar(255) DEFAULT NULL,
  `MailingAddressCity` varchar(255) DEFAULT NULL,
  `MailingAddressStateProvCd` varchar(255) DEFAULT NULL,
  `MailingAddressPostalCode` varchar(255) DEFAULT NULL,
  `MailingAddressPostalCodeExt` varchar(255) DEFAULT NULL,
  `TelephoneAreaCode` varchar(255) DEFAULT NULL,
  `TelephoneNumber` varchar(255) DEFAULT NULL,
  `InsuredPrefixCd1` varchar(255) DEFAULT NULL,
  `InsuredSurname1` varchar(255) DEFAULT NULL,
  `InsuredGivenName1` varchar(255) DEFAULT NULL,
  `InsuredOtherGivenName1` varchar(255) DEFAULT NULL,
  `InsuredSuffixCd1` varchar(255) DEFAULT NULL,
  `InsuredSSN1` varchar(255) DEFAULT NULL,
  `InsuredBirthDt1` date DEFAULT NULL,
  `InsuredGenderCd1` varchar(255) DEFAULT NULL,
  `InsuredPrefixCd2` varchar(255) DEFAULT NULL,
  `InsuredSurname2` varchar(255) DEFAULT NULL,
  `InsuredGivenName2` varchar(255) DEFAULT NULL,
  `InsuredOtherGivenName2` varchar(255) DEFAULT NULL,
  `InsuredSSN2` varchar(255) DEFAULT NULL,
  `InsuredSuffixCd2` varchar(255) DEFAULT NULL,
  `InsuredBirthDt2` date DEFAULT NULL,
  `InsuredGenderCd2` varchar(255) DEFAULT NULL,
  `ClaimantPrefixCd` varchar(255) DEFAULT NULL,
  `ClaimantSurname` varchar(255) DEFAULT NULL,
  `ClaimantGivenName` varchar(255) DEFAULT NULL,
  `ClaimantOtherGivenName` varchar(255) DEFAULT NULL,
  `ClaimantSuffixCd` varchar(255) DEFAULT NULL,
  `ClaimantSSN` varchar(255) DEFAULT NULL,
  `ClaimantBirthDt` date DEFAULT NULL,
  `ClaimantGenderCd` varchar(255) DEFAULT NULL,
  `ClaimantAddressPrimaryNumber` varchar(255) DEFAULT NULL,
  `ClaimantAddressStreetName` varchar(255) DEFAULT NULL,
  `ClaimantAddressCity` varchar(255) DEFAULT NULL,
  `ClaimantAddressSecondaryNumber` varchar(255) DEFAULT NULL,
  `ClaimantAddressPostalCode` varchar(255) DEFAULT NULL,
  `ClaimantAddressStateProvCd` varchar(255) DEFAULT NULL,
  `ClaimantAddressPostalCodeExt` varchar(255) DEFAULT NULL,
  `ClaimantTelephoneAreaCode` varchar(255) DEFAULT NULL,
  `ClaimantTelephoneNumber` varchar(255) DEFAULT NULL,
  KEY `cpclupclaiminfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpclupcontrib`
--

DROP TABLE IF EXISTS `cpclupcontrib`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpclupcontrib` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `ReportPeriodBeginDt` date DEFAULT NULL,
  `ReportPeriodEndDt` date DEFAULT NULL,
  `RegeneratedInd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `ClaimRef` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `ProcessDt` date DEFAULT NULL,
  `ProcessTm` varchar(255) DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  KEY `cpclupcontribIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpconsumernarrative`
--

DROP TABLE IF EXISTS `cpconsumernarrative`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpconsumernarrative` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ConsumerNarrativeTypeCd` varchar(255) DEFAULT NULL,
  `ClassCd` varchar(255) DEFAULT NULL,
  `StatementFilingDt` date DEFAULT NULL,
  `FilerName` varchar(255) DEFAULT NULL,
  `StatementPurgeDt` date DEFAULT NULL,
  `RelationshipToClaimantCd` varchar(255) DEFAULT NULL,
  KEY `cpconsumernarrativeIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpcurrentcarrier`
--

DROP TABLE IF EXISTS `cpcurrentcarrier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpcurrentcarrier` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ReportTypeCd` varchar(255) DEFAULT NULL,
  `ReportTypeDisplay` varchar(255) DEFAULT NULL,
  `ReferenceNumber` varchar(255) DEFAULT NULL,
  `SearchResultCd` varchar(255) DEFAULT NULL,
  `SearchResultDisplay` varchar(255) DEFAULT NULL,
  `CurrentInd` varchar(255) DEFAULT NULL,
  `PriorPolicyCount` int(11) DEFAULT NULL,
  `PolicyLapseInd` varchar(255) DEFAULT NULL,
  `CoverageLapseInfoAvailableInd` varchar(255) DEFAULT NULL,
  `CoverageLapseInd` varchar(255) DEFAULT NULL,
  `CurrentPolicyAgeYears` int(11) DEFAULT NULL,
  `PriorPolicyAlertInd` varchar(255) DEFAULT NULL,
  `PriorPolicyAlertCd` varchar(255) DEFAULT NULL,
  `PriorPolicyAlertCount` varchar(255) DEFAULT NULL,
  `SecurityFreezeInd` varchar(255) DEFAULT NULL,
  `SecurityFreezeCd` varchar(255) DEFAULT NULL,
  `SecurityFreezeDisplay` varchar(255) DEFAULT NULL,
  `SecurityFreezeMessage` varchar(255) DEFAULT NULL,
  `SecurityFreezeSubjectUnitNum` int(11) DEFAULT NULL,
  `SecurityAlertMessage` varchar(255) DEFAULT NULL,
  `DisclaimerNotice` varchar(255) DEFAULT NULL,
  KEY `cpcurrentcarrierIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpcustomerorderdata`
--

DROP TABLE IF EXISTS `cpcustomerorderdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpcustomerorderdata` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `LicenseNumber` varchar(255) DEFAULT NULL,
  `Surname` varchar(255) DEFAULT NULL,
  `GivenName` varchar(255) DEFAULT NULL,
  `OtherGivenName` varchar(255) DEFAULT NULL,
  `BirthDt` date DEFAULT NULL,
  `SuffixCd` varchar(255) DEFAULT NULL,
  `GenderCd` varchar(255) DEFAULT NULL,
  `SSN` varchar(255) DEFAULT NULL,
  KEY `cpcustomerorderdataIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpdriver`
--

DROP TABLE IF EXISTS `cpdriver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpdriver` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `BirthDt` date DEFAULT NULL,
  `Height` varchar(255) DEFAULT NULL,
  `Weight` varchar(255) DEFAULT NULL,
  `EyeColor` varchar(255) DEFAULT NULL,
  `GenderCd` varchar(255) DEFAULT NULL,
  `Alias` varchar(255) DEFAULT NULL,
  `SSN` varchar(255) DEFAULT NULL,
  KEY `cpdriverIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpdriverlicense`
--

DROP TABLE IF EXISTS `cpdriverlicense`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpdriverlicense` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `DriverLicenseTypeCd` varchar(255) DEFAULT NULL,
  `ClassCd` varchar(255) DEFAULT NULL,
  `ClassDisplay` varchar(255) DEFAULT NULL,
  `DriverLicenseNumber` varchar(255) DEFAULT NULL,
  `DriverLicenseStateProvCd` varchar(255) DEFAULT NULL,
  `IssuedDt` date DEFAULT NULL,
  `Restrictions` varchar(255) DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  KEY `cpdriverlicenseIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpdriverlicreportinfo`
--

DROP TABLE IF EXISTS `cpdriverlicreportinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpdriverlicreportinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `DataSourceIndicator` varchar(255) DEFAULT NULL,
  `ClassCdChangedInd` varchar(255) DEFAULT NULL,
  `DriverLicenseStateChangedInd` varchar(255) DEFAULT NULL,
  `AssociationDt` date DEFAULT NULL,
  `DriverLicenseNumberChangedInd` varchar(255) DEFAULT NULL,
  KEY `cpdriverlicreportinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpdrivingviolation`
--

DROP TABLE IF EXISTS `cpdrivingviolation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpdrivingviolation` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ViolationType` varchar(255) DEFAULT NULL,
  `ViolationOrSuspensionDt` date DEFAULT NULL,
  `ConvictionOrReinstatementDt` date DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `StateViolationCd` varchar(255) DEFAULT NULL,
  `Points` varchar(255) DEFAULT NULL,
  `StandardViolationCd` varchar(255) DEFAULT NULL,
  `StandardViolationDescription` varchar(255) DEFAULT NULL,
  `CustomerSpecificCd` varchar(255) DEFAULT NULL,
  `ViolationData` varchar(255) DEFAULT NULL,
  KEY `cpdrivingviolationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cperrorreport`
--

DROP TABLE IF EXISTS `cperrorreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cperrorreport` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ApplicationStatus` varchar(255) DEFAULT NULL,
  `TransactionStatus` varchar(255) DEFAULT NULL,
  `Message` varchar(255) DEFAULT NULL,
  KEY `cperrorreportIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpfirstfinanceinfo`
--

DROP TABLE IF EXISTS `cpfirstfinanceinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpfirstfinanceinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `FinanceCompanyName` varchar(255) DEFAULT NULL,
  `FinanceCompanyPrimaryNumber` varchar(255) DEFAULT NULL,
  `FinanceCompanyType` varchar(255) DEFAULT NULL,
  `FinanceCompanySecondaryNumber` varchar(255) DEFAULT NULL,
  `FinanceCompanyStreetName` varchar(255) DEFAULT NULL,
  `FinanceCompanyCity` varchar(255) DEFAULT NULL,
  `FinanceCompanyStateProvCd` varchar(255) DEFAULT NULL,
  `FinanceCompanyPostalCode` varchar(255) DEFAULT NULL,
  `FinanceCompanyPostalCodeExt` varchar(255) DEFAULT NULL,
  `FinanceCompanyLoanNumber` varchar(255) DEFAULT NULL,
  `FinanceCompanyPremiumAmtDue` varchar(255) DEFAULT NULL,
  KEY `cpfirstfinanceinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpfirstpolicyinfo`
--

DROP TABLE IF EXISTS `cpfirstpolicyinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpfirstpolicyinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AgentSurname` varchar(255) DEFAULT NULL,
  `AgentGivenName` varchar(255) DEFAULT NULL,
  `AgentMailingPrimaryNumber` varchar(255) DEFAULT NULL,
  `AgentMailingSecondaryNumber` varchar(255) DEFAULT NULL,
  `AgentMailingStreetName` varchar(255) DEFAULT NULL,
  `AgentMailingCity` varchar(255) DEFAULT NULL,
  `AgentMailingStateProvCd` varchar(255) DEFAULT NULL,
  `AgentMailingPostalCode` varchar(255) DEFAULT NULL,
  `AgentMailingPostalCodeExt` varchar(255) DEFAULT NULL,
  `AgentTelephoneAreaCode` varchar(255) DEFAULT NULL,
  `AgentTelephoneNumber` varchar(255) DEFAULT NULL,
  `AgentTelephoneExt` varchar(255) DEFAULT NULL,
  `PayableTo` varchar(255) DEFAULT NULL,
  `RemitMailingPrimaryNumber` varchar(255) DEFAULT NULL,
  `RemitMailingStreetName` varchar(255) DEFAULT NULL,
  `RemitMailingCity` varchar(255) DEFAULT NULL,
  `RemitMailingSecondaryNumber` varchar(255) DEFAULT NULL,
  `RemitMailingPostalCode` varchar(255) DEFAULT NULL,
  `RemitMailingStateProvCd` varchar(255) DEFAULT NULL,
  `RemitMailingPostalCodeExt` varchar(255) DEFAULT NULL,
  `RemitTelephoneAreaCode` varchar(255) DEFAULT NULL,
  `RemitTelephoneExt` varchar(255) DEFAULT NULL,
  `RemitTelephoneNumber` varchar(255) DEFAULT NULL,
  `PremiumIncreaseAmt` varchar(255) DEFAULT NULL,
  `PremiumInstallAmt` varchar(255) DEFAULT NULL,
  `NotificationPolicyType` varchar(255) DEFAULT NULL,
  KEY `cpfirstpolicyinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpfirstpropertyinfo`
--

DROP TABLE IF EXISTS `cpfirstpropertyinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpfirstpropertyinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `VehIdentificationNumber` varchar(255) DEFAULT NULL,
  `PropertyId` varchar(255) DEFAULT NULL,
  `TransactionId` varchar(255) DEFAULT NULL,
  `NotificationActionCd` varchar(255) DEFAULT NULL,
  `NotificationActionDt` date DEFAULT NULL,
  `Endorsement1` varchar(255) DEFAULT NULL,
  `EndorsementStateProvCd` varchar(255) DEFAULT NULL,
  `Endorsement2` varchar(255) DEFAULT NULL,
  `Endorsement3` varchar(255) DEFAULT NULL,
  `Endorsement4` varchar(255) DEFAULT NULL,
  `Endorsement5` varchar(255) DEFAULT NULL,
  `Endorsement6` varchar(255) DEFAULT NULL,
  `Endorsement7` varchar(255) DEFAULT NULL,
  `Endorsement8` varchar(255) DEFAULT NULL,
  `PropertyLegalDesc` varchar(255) DEFAULT NULL,
  `FloodZone` varchar(255) DEFAULT NULL,
  `LiabilityOnlyInd` varchar(255) DEFAULT NULL,
  `Notes` varchar(255) DEFAULT NULL,
  `FinancialNotificationInd` varchar(255) DEFAULT NULL,
  KEY `cpfirstpropertyinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpinquiryhistory`
--

DROP TABLE IF EXISTS `cpinquiryhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpinquiryhistory` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ClassCd` varchar(255) DEFAULT NULL,
  `InquiryDt` date DEFAULT NULL,
  `InformationSourceCd` varchar(255) DEFAULT NULL,
  `InquirerName` varchar(255) DEFAULT NULL,
  `ReportUsageCd` varchar(255) DEFAULT NULL,
  `UnitClassCd` varchar(255) DEFAULT NULL,
  `InquirerId` varchar(255) DEFAULT NULL,
  KEY `cpinquiryhistoryIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpinquiryinfo`
--

DROP TABLE IF EXISTS `cpinquiryinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpinquiryinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ProcessingStatusCd` varchar(255) DEFAULT NULL,
  `ProcessingStatusDisplay` varchar(255) DEFAULT NULL,
  `ChoicePointReferenceNumber` varchar(255) DEFAULT NULL,
  `OrderDt` date DEFAULT NULL,
  `CompletionDt` date DEFAULT NULL,
  `ReceiptDt` date DEFAULT NULL,
  `QuoteBack` varchar(255) DEFAULT NULL,
  `ReportTm` varchar(255) DEFAULT NULL,
  `ReportCd` varchar(255) DEFAULT NULL,
  `ReportTypeCd` varchar(255) DEFAULT NULL,
  `ReportTypeDisplay` varchar(255) DEFAULT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  `ReportUsageCd` varchar(255) DEFAULT NULL,
  `ReportUsageDisplay` varchar(255) DEFAULT NULL,
  `Attachment` varchar(255) DEFAULT NULL,
  `ProductGroup` varchar(255) DEFAULT NULL,
  `AccountSuffix` varchar(255) DEFAULT NULL,
  KEY `cpinquiryinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cploss`
--

DROP TABLE IF EXISTS `cploss`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cploss` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ClassCd` varchar(255) DEFAULT NULL,
  `LossTypeCd` varchar(255) DEFAULT NULL,
  `AgeMonths` int(11) DEFAULT NULL,
  `AgeYears` int(11) DEFAULT NULL,
  `ClaimTypeCd` varchar(255) DEFAULT NULL,
  `ClaimTypeDisplay` varchar(255) DEFAULT NULL,
  `DispositionCd` varchar(255) DEFAULT NULL,
  `DispositionDisplay` varchar(255) DEFAULT NULL,
  `ContributorClaimNumber` varchar(255) DEFAULT NULL,
  `ScopeCd` varchar(255) DEFAULT NULL,
  `ScopeDisplay` varchar(255) DEFAULT NULL,
  `AtFaultCd` varchar(255) DEFAULT NULL,
  `AtFaultDisplay` varchar(255) DEFAULT NULL,
  `FirstPaymentDt` date DEFAULT NULL,
  `LatestPaymentDt` date DEFAULT NULL,
  `LocationOfLossCd` varchar(255) DEFAULT NULL,
  `LocationOfLossDisplay` varchar(255) DEFAULT NULL,
  `ContributorAMBestNumber` varchar(255) DEFAULT NULL,
  `DisputeClearanceDt` date DEFAULT NULL,
  `PolicyHolderRelationshipCd` varchar(255) DEFAULT NULL,
  `VehicleOperatorInd` varchar(255) DEFAULT NULL,
  `ChoicePointClaimFileNumber` varchar(255) DEFAULT NULL,
  `ClaimDt` date DEFAULT NULL,
  `PolicyHolderRelationDisplay` varchar(255) DEFAULT NULL,
  `PossibleRelatedClaimInd` varchar(255) DEFAULT NULL,
  KEY `cplossIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cplossreportinfo`
--

DROP TABLE IF EXISTS `cplossreportinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cplossreportinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ClaimAddressMatchCd` varchar(255) DEFAULT NULL,
  `ClaimAddressMatchDisplay` varchar(255) DEFAULT NULL,
  `TelephoneMatchInd` varchar(255) DEFAULT NULL,
  `DriversLicenseNumberMatchInd` varchar(255) DEFAULT NULL,
  `SSNMatchInd` varchar(255) DEFAULT NULL,
  `ClaimAssociationCd` varchar(255) DEFAULT NULL,
  `ClaimAssociationDisplay` varchar(255) DEFAULT NULL,
  KEY `cplossreportinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpmessage`
--

DROP TABLE IF EXISTS `cpmessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpmessage` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `MessageCd` varchar(255) DEFAULT NULL,
  `Message` varchar(255) DEFAULT NULL,
  KEY `cpmessageIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpmiscinfo`
--

DROP TABLE IF EXISTS `cpmiscinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpmiscinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `RecordSequence` int(11) DEFAULT NULL,
  `Info` varchar(255) DEFAULT NULL,
  KEY `cpmiscinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpmortgage`
--

DROP TABLE IF EXISTS `cpmortgage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpmortgage` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CompanyName` varchar(255) DEFAULT NULL,
  `LoanNumber` varchar(255) DEFAULT NULL,
  KEY `cpmortgageIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpmortgagereportinfo`
--

DROP TABLE IF EXISTS `cpmortgagereportinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpmortgagereportinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `LoanNumberChangedInd` varchar(255) DEFAULT NULL,
  `CompanyNameChangedInd` varchar(255) DEFAULT NULL,
  KEY `cpmortgagereportinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpmvrreport`
--

DROP TABLE IF EXISTS `cpmvrreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpmvrreport` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateProvCd` varchar(255) DEFAULT NULL,
  `ProcessingStatusCd` varchar(255) DEFAULT NULL,
  `ProcessingStatusDisplay` varchar(255) DEFAULT NULL,
  `DMVAccountNumber` varchar(255) DEFAULT NULL,
  `ProcessDt` date DEFAULT NULL,
  `CPAccountNumber` varchar(255) DEFAULT NULL,
  `LicenseNumber` varchar(255) DEFAULT NULL,
  `OriginCd` varchar(255) DEFAULT NULL,
  `OriginDisplay` varchar(255) DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `SourceDisplay` varchar(255) DEFAULT NULL,
  `QuoteBack` varchar(255) DEFAULT NULL,
  KEY `cpmvrreportIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpmvrsearchcriteria`
--

DROP TABLE IF EXISTS `cpmvrsearchcriteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpmvrsearchcriteria` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ReportUsageCd` varchar(255) DEFAULT NULL,
  `ReportServiceTypeCd` varchar(255) DEFAULT NULL,
  KEY `cpmvrsearchcriteriaIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpnameoraddressline`
--

DROP TABLE IF EXISTS `cpnameoraddressline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpnameoraddressline` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `RecordSequence` int(11) DEFAULT NULL,
  `NameOrAddress` varchar(255) DEFAULT NULL,
  KEY `cpnameoraddresslineIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfbankaccount`
--

DROP TABLE IF EXISTS `cpncfbankaccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfbankaccount` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `InformationSourceCd` varchar(255) DEFAULT NULL,
  `ReportedDt` date DEFAULT NULL,
  `AccountTypeCd` varchar(255) DEFAULT NULL,
  `ReportingMemberNumber` varchar(255) DEFAULT NULL,
  `Amount` decimal(28,6) DEFAULT NULL,
  `OpenedDt` date DEFAULT NULL,
  `ReasonCd` varchar(255) DEFAULT NULL,
  KEY `cpncfbankaccountIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfbankruptcy`
--

DROP TABLE IF EXISTS `cpncfbankruptcy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfbankruptcy` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AssetsReportedCount` int(11) DEFAULT NULL,
  `FiledDt` date DEFAULT NULL,
  `DichargedDt` date DEFAULT NULL,
  `LiabilitiesReportedCount` int(11) DEFAULT NULL,
  `CourtNumber` varchar(255) DEFAULT NULL,
  `InformationSourceCd` varchar(255) DEFAULT NULL,
  `BankruptcyTypeCd` varchar(255) DEFAULT NULL,
  `CaseNumber` varchar(255) DEFAULT NULL,
  `ExemptAmt` decimal(28,6) DEFAULT NULL,
  `FilingTypeCd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  KEY `cpncfbankruptcyIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfcollection`
--

DROP TABLE IF EXISTS `cpncfcollection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfcollection` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `InformationSourceCd` varchar(255) DEFAULT NULL,
  `AssignedDt` date DEFAULT NULL,
  `ReportedDt` date DEFAULT NULL,
  `ClientNumber` varchar(255) DEFAULT NULL,
  `ReportingMemberNumber` varchar(255) DEFAULT NULL,
  `AccountSerialNumber` varchar(255) DEFAULT NULL,
  `ECOACd` varchar(255) DEFAULT NULL,
  `LastActivityDt` date DEFAULT NULL,
  `OriginalAmt` decimal(28,6) DEFAULT NULL,
  `BalanceAmt` decimal(28,6) DEFAULT NULL,
  `BalanceDt` date DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `StatusDt` date DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  KEY `cpncfcollectionIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfcreditinquiry`
--

DROP TABLE IF EXISTS `cpncfcreditinquiry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfcreditinquiry` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `InquirerName` varchar(255) DEFAULT NULL,
  `InquiryDt` date DEFAULT NULL,
  `InquirerId` varchar(255) DEFAULT NULL,
  `Quoteback` varchar(255) DEFAULT NULL,
  `InformationSourceCd` varchar(255) DEFAULT NULL,
  `UnitClassCd` varchar(255) DEFAULT NULL,
  `ReportUsageCd` varchar(255) DEFAULT NULL,
  KEY `cpncfcreditinquiryIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfcreditreportsummary`
--

DROP TABLE IF EXISTS `cpncfcreditreportsummary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfcreditreportsummary` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `FileEstablishedDt` date DEFAULT NULL,
  `OldestTradeDt` date DEFAULT NULL,
  `LatestActivityDt` date DEFAULT NULL,
  `LatestTradeDt` date DEFAULT NULL,
  `BankruptciesIncludedInd` varchar(255) DEFAULT NULL,
  `LatestBankruptcyDt` date DEFAULT NULL,
  `CollectionItemsIncludedInd` varchar(255) DEFAULT NULL,
  `PublicRecordsIncludedInd` varchar(255) DEFAULT NULL,
  `ConsumerStatementsIncludedInd` varchar(255) DEFAULT NULL,
  `CreditRangeHighAmt` decimal(28,6) DEFAULT NULL,
  `CreditRangeLowAmt` decimal(28,6) DEFAULT NULL,
  `TradeLinesCount` int(11) DEFAULT NULL,
  `RevolvingHighCreditAmt` decimal(28,6) DEFAULT NULL,
  `RevolvingOwedAmt` decimal(28,6) DEFAULT NULL,
  `OpenEndedOwedAmt` decimal(28,6) DEFAULT NULL,
  `InstallmentOwedAmt` decimal(28,6) DEFAULT NULL,
  `InstallmentHighCreditAmt` decimal(28,6) DEFAULT NULL,
  `OpenEndedHighCreditAmt` decimal(28,6) DEFAULT NULL,
  `OpenEndedPastDueAmt` decimal(28,6) DEFAULT NULL,
  `RevolvingPastDueAmt` decimal(28,6) DEFAULT NULL,
  `InstallmentPastDueAmt` decimal(28,6) DEFAULT NULL,
  `InquiryHistory90DayCount` int(11) DEFAULT NULL,
  `InquiryHistory90DayDt` date DEFAULT NULL,
  `RevolvingAccountCount` int(11) DEFAULT NULL,
  `OpenEndedAccountCount` int(11) DEFAULT NULL,
  `InstallmentAccountCount` int(11) DEFAULT NULL,
  `AccountsWithCurrentStatus0` int(11) DEFAULT NULL,
  `AccountsWithCurrentStatus1` int(11) DEFAULT NULL,
  `AccountsWithCurrentStatus2` int(11) DEFAULT NULL,
  `AccountsWithCurrentStatus3` int(11) DEFAULT NULL,
  `AccountsWithCurrentStatus4` int(11) DEFAULT NULL,
  `AccountsWithCurrentStatus5` int(11) DEFAULT NULL,
  `AccountsWithCurrentStatus6` int(11) DEFAULT NULL,
  `AccountsWithCurrentStatus7` int(11) DEFAULT NULL,
  `AccountsWithCurrentStatus8` int(11) DEFAULT NULL,
  `AccountsWithCurrentStatus9` int(11) DEFAULT NULL,
  `AccountsWithCurrentStatusBlank` int(11) DEFAULT NULL,
  `AccountsWithHistoryStatus1` int(11) DEFAULT NULL,
  `AccountsWithHistoryStatus2` int(11) DEFAULT NULL,
  `AccountsWithHistoryStatus3` int(11) DEFAULT NULL,
  `AccountsWithHistoryStatus4` int(11) DEFAULT NULL,
  `AccountsWithHistoryStatus5` int(11) DEFAULT NULL,
  `AccountsWithHistoryStatus7` int(11) DEFAULT NULL,
  `AccountsWithHistoryStatus8` int(11) DEFAULT NULL,
  `AccountsWithHistoryStatus9` int(11) DEFAULT NULL,
  KEY `cpncfcreditreportsummaryIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfcredittrade`
--

DROP TABLE IF EXISTS `cpncfcredittrade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfcredittrade` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ReportingMemberNumber` varchar(255) DEFAULT NULL,
  `MemberName` varchar(255) DEFAULT NULL,
  `TapeSupplierInd` varchar(255) DEFAULT NULL,
  `OpenedDt` date DEFAULT NULL,
  `ReportedDt` date DEFAULT NULL,
  `HighestCreditAmt` decimal(28,6) DEFAULT NULL,
  `Terms` varchar(255) DEFAULT NULL,
  `AccountBalanceAmt` decimal(28,6) DEFAULT NULL,
  `PastDueAmt` decimal(28,6) DEFAULT NULL,
  `AccountTypeCd` varchar(255) DEFAULT NULL,
  `CurrentRateCd` varchar(255) DEFAULT NULL,
  `AccountDesignatorCd` varchar(255) DEFAULT NULL,
  `MonthsReviewed` int(11) DEFAULT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  `Counter30Days` int(11) DEFAULT NULL,
  `Counter60Days` int(11) DEFAULT NULL,
  `Counter90Days` int(11) DEFAULT NULL,
  `LastActivityDt` date DEFAULT NULL,
  KEY `cpncfcredittradeIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfemployment`
--

DROP TABLE IF EXISTS `cpncfemployment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfemployment` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `EmploymentTypeCd` varchar(255) DEFAULT NULL,
  `EmployerName` varchar(255) DEFAULT NULL,
  `PositionTypeCd` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `StartingDt` date DEFAULT NULL,
  `VerifiedDt` date DEFAULT NULL,
  `IndirectlyVerifiedInd` varchar(255) DEFAULT NULL,
  `LeftDt` date DEFAULT NULL,
  `MonthlySalaryAmt` decimal(28,6) DEFAULT NULL,
  KEY `cpncfemploymentIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncffinancialcounselor`
--

DROP TABLE IF EXISTS `cpncffinancialcounselor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncffinancialcounselor` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `InformationSourceCd` varchar(255) DEFAULT NULL,
  `ReportedDt` date DEFAULT NULL,
  `ReportingMemberNumber` varchar(255) DEFAULT NULL,
  `Amount` decimal(28,6) DEFAULT NULL,
  `CheckedDt` date DEFAULT NULL,
  `SettledDt` date DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  KEY `cpncffinancialcounselorIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfforeclosure`
--

DROP TABLE IF EXISTS `cpncfforeclosure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfforeclosure` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `InformationSourceCd` varchar(255) DEFAULT NULL,
  `CheckedDt` date DEFAULT NULL,
  `ReportedDt` date DEFAULT NULL,
  `ReportingMemberNumber` varchar(255) DEFAULT NULL,
  KEY `cpncfforeclosureIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfforeignbureau`
--

DROP TABLE IF EXISTS `cpncfforeignbureau`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfforeignbureau` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `BureauCd` varchar(255) DEFAULT NULL,
  `CityAndNarrative` varchar(255) DEFAULT NULL,
  `StateAndNarrative` varchar(255) DEFAULT NULL,
  `ReportingDt` date DEFAULT NULL,
  KEY `cpncfforeignbureauIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfgarnishment`
--

DROP TABLE IF EXISTS `cpncfgarnishment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfgarnishment` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `InformationSourceCd` varchar(255) DEFAULT NULL,
  `ReportedDt` date DEFAULT NULL,
  `SatisfiedDt` date DEFAULT NULL,
  `CheckedDt` date DEFAULT NULL,
  `CourtNumber` varchar(255) DEFAULT NULL,
  `CaseNumber` varchar(255) DEFAULT NULL,
  `Plaintiff` varchar(255) DEFAULT NULL,
  `Defendant` varchar(255) DEFAULT NULL,
  `Garnishee` varchar(255) DEFAULT NULL,
  `Amount` decimal(28,6) DEFAULT NULL,
  KEY `cpncfgarnishmentIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfguaranteedloan`
--

DROP TABLE IF EXISTS `cpncfguaranteedloan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfguaranteedloan` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `InformationSourceCd` varchar(255) DEFAULT NULL,
  `CourtNumber` varchar(255) DEFAULT NULL,
  `FiledDt` date DEFAULT NULL,
  `CaseNumber` varchar(255) DEFAULT NULL,
  `IndustryCd` varchar(255) DEFAULT NULL,
  `MaturityDt` date DEFAULT NULL,
  KEY `cpncfguaranteedloanIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfjudgment`
--

DROP TABLE IF EXISTS `cpncfjudgment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfjudgment` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `InformationSourceCd` varchar(255) DEFAULT NULL,
  `FiledDt` date DEFAULT NULL,
  `CaseNumber` varchar(255) DEFAULT NULL,
  `CourtNumber` varchar(255) DEFAULT NULL,
  `Amount` decimal(28,6) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `SatisfiedDt` date DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `Defendant` varchar(255) DEFAULT NULL,
  `VerifiedDt` date DEFAULT NULL,
  `Plaintiff` varchar(255) DEFAULT NULL,
  KEY `cpncfjudgmentIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfmarriage`
--

DROP TABLE IF EXISTS `cpncfmarriage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfmarriage` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `InformationSourceCd` varchar(255) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FiledDt` date DEFAULT NULL,
  `VerifiedDt` date DEFAULT NULL,
  `CourtNumber` varchar(255) DEFAULT NULL,
  `SpouseName` varchar(255) DEFAULT NULL,
  `CaseNumber` varchar(255) DEFAULT NULL,
  KEY `cpncfmarriageIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfnonmembercredtrade`
--

DROP TABLE IF EXISTS `cpncfnonmembercredtrade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfnonmembercredtrade` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AccountTypeCd` varchar(255) DEFAULT NULL,
  `ReportedDt` date DEFAULT NULL,
  `CurrentRateCd` varchar(255) DEFAULT NULL,
  `OpenedDt` date DEFAULT NULL,
  `CustomerNarrative` varchar(255) DEFAULT NULL,
  `HighestCreditAmt` decimal(28,6) DEFAULT NULL,
  `AccountBalanceAmt` decimal(28,6) DEFAULT NULL,
  `PastDueAmt` decimal(28,6) DEFAULT NULL,
  `RateLessThanZero` varchar(255) DEFAULT NULL,
  KEY `cpncfnonmembercredtradeIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfpreviousrate`
--

DROP TABLE IF EXISTS `cpncfpreviousrate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfpreviousrate` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Order` int(11) DEFAULT NULL,
  `RateCd` varchar(255) DEFAULT NULL,
  `RateDt` date DEFAULT NULL,
  KEY `cpncfpreviousrateIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfreport`
--

DROP TABLE IF EXISTS `cpncfreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfreport` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ProcessingStatusCd` varchar(255) DEFAULT NULL,
  `NCFReportCd` varchar(255) DEFAULT NULL,
  `ProcessingStatusDisplay` varchar(255) DEFAULT NULL,
  `NCFReportDisplay` varchar(255) DEFAULT NULL,
  `CreditVendorCd` varchar(255) DEFAULT NULL,
  `CreditVendorResultCd` varchar(255) DEFAULT NULL,
  `ReportCount` int(11) DEFAULT NULL,
  `ReportSequence` int(11) DEFAULT NULL,
  KEY `cpncfreportIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfscore`
--

DROP TABLE IF EXISTS `cpncfscore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfscore` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  KEY `cpncfscoreIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfscorereason`
--

DROP TABLE IF EXISTS `cpncfscorereason`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfscorereason` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ReasonCd` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  KEY `cpncfscorereasonIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfsearchcriteria`
--

DROP TABLE IF EXISTS `cpncfsearchcriteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfsearchcriteria` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ReportServiceTypeCd` varchar(255) DEFAULT NULL,
  `ProductLineCd` varchar(255) DEFAULT NULL,
  `RequestedHandlingLocationCd` varchar(255) DEFAULT NULL,
  `CreditBureauOverrideCd` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  `PolicyRatingStateCd` varchar(255) DEFAULT NULL,
  `FormerAddressPrimaryNumber` varchar(255) DEFAULT NULL,
  `FormerAddressStreetName` varchar(255) DEFAULT NULL,
  `FormerAddressSecondaryNumber` varchar(255) DEFAULT NULL,
  `FormerAddressCity` varchar(255) DEFAULT NULL,
  `FormerAddressStateProvCd` varchar(255) DEFAULT NULL,
  `FormerAddressPostalCode` varchar(255) DEFAULT NULL,
  `AddressPrimaryNumber` varchar(255) DEFAULT NULL,
  `AddressSecondaryNumber` varchar(255) DEFAULT NULL,
  `AddressStreetName` varchar(255) DEFAULT NULL,
  `AddressCity` varchar(255) DEFAULT NULL,
  `AddressStateProvCd` varchar(255) DEFAULT NULL,
  `AddressPostalCode` varchar(255) DEFAULT NULL,
  KEY `cpncfsearchcriteriaIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfsearchsubject`
--

DROP TABLE IF EXISTS `cpncfsearchsubject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfsearchsubject` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `cpncfsearchsubjectIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncfsubject`
--

DROP TABLE IF EXISTS `cpncfsubject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncfsubject` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `cpncfsubjectIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpncftaxlien`
--

DROP TABLE IF EXISTS `cpncftaxlien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpncftaxlien` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `InformationSourceCd` varchar(255) DEFAULT NULL,
  `CourtNumber` varchar(255) DEFAULT NULL,
  `FiledDt` date DEFAULT NULL,
  `Amount` decimal(28,6) DEFAULT NULL,
  `CreditorClassCd` varchar(255) DEFAULT NULL,
  `CaseNumber` varchar(255) DEFAULT NULL,
  `VerifiedDt` date DEFAULT NULL,
  `ReleasedDt` date DEFAULT NULL,
  KEY `cpncftaxlienIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpperson`
--

DROP TABLE IF EXISTS `cpperson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpperson` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PersonTypeCd` varchar(255) DEFAULT NULL,
  `GivenName` varchar(255) DEFAULT NULL,
  `Surname` varchar(255) DEFAULT NULL,
  `OtherGivenName` varchar(255) DEFAULT NULL,
  `PrefixCd` varchar(255) DEFAULT NULL,
  `SuffixCd` varchar(255) DEFAULT NULL,
  `BirthDt` date DEFAULT NULL,
  `GenderCd` varchar(255) DEFAULT NULL,
  `SSN` varchar(255) DEFAULT NULL,
  `MaritalStatusCd` varchar(255) DEFAULT NULL,
  `Age` int(11) DEFAULT NULL,
  `HeightFeet` int(11) DEFAULT NULL,
  `HeightInches` int(11) DEFAULT NULL,
  `WeightLbs` int(11) DEFAULT NULL,
  `EyeColorCd` varchar(255) DEFAULT NULL,
  `HairColorCd` varchar(255) DEFAULT NULL,
  `NumberOfDependents` int(11) DEFAULT NULL,
  `RaceCd` varchar(255) DEFAULT NULL,
  `DeathDt` date DEFAULT NULL,
  `RelationshipCd` varchar(255) DEFAULT NULL,
  `RelationshipDisplay` varchar(255) DEFAULT NULL,
  `RelationshipDescription` varchar(255) DEFAULT NULL,
  `QuoteBack` varchar(255) DEFAULT NULL,
  `Alias` varchar(255) DEFAULT NULL,
  `ClassCd` varchar(255) DEFAULT NULL,
  `ClassDisplay` varchar(255) DEFAULT NULL,
  KEY `cppersonIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cppersonreportinfo`
--

DROP TABLE IF EXISTS `cppersonreportinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cppersonreportinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AssociationDt` date DEFAULT NULL,
  `DataSourceIndicator` varchar(255) DEFAULT NULL,
  `PrefixCdChangedInd` varchar(255) DEFAULT NULL,
  `RelationshipChangedInd` varchar(255) DEFAULT NULL,
  `OtherGivenNameChangedInd` varchar(255) DEFAULT NULL,
  `SurnameChangedInd` varchar(255) DEFAULT NULL,
  `SuffixCdChangedInd` varchar(255) DEFAULT NULL,
  `AgeChangedInd` varchar(255) DEFAULT NULL,
  `NameAssociationCd` varchar(255) DEFAULT NULL,
  `NameAssociationDisplay` varchar(255) DEFAULT NULL,
  `BirthDtChangedInd` varchar(255) DEFAULT NULL,
  `GenderCdChangedInd` varchar(255) DEFAULT NULL,
  `HeightFeetChangedInd` varchar(255) DEFAULT NULL,
  `SSNChangedInd` varchar(255) DEFAULT NULL,
  `HeightInchesChangedInd` varchar(255) DEFAULT NULL,
  `WeightChangedInd` varchar(255) DEFAULT NULL,
  `EyeColorChangedInd` varchar(255) DEFAULT NULL,
  `MaritalStatusCdChangedInd` varchar(255) DEFAULT NULL,
  `HairColorChangedInd` varchar(255) DEFAULT NULL,
  `SSNVerifiedInd` varchar(255) DEFAULT NULL,
  `DeathDtChangedInd` varchar(255) DEFAULT NULL,
  `GivenNameChangedInd` varchar(255) DEFAULT NULL,
  `PolicyWatchCd` varchar(255) DEFAULT NULL,
  `PolicyWatchDisplay` varchar(255) DEFAULT NULL,
  KEY `cppersonreportinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cppolicy`
--

DROP TABLE IF EXISTS `cppolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cppolicy` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CompanyName` varchar(255) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `RatingStateProvCd` varchar(255) DEFAULT NULL,
  `VendorOverride` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `TypeDisplay` varchar(255) DEFAULT NULL,
  `TypeDescription` varchar(255) DEFAULT NULL,
  `PolicyLapsedInd` varchar(255) DEFAULT NULL,
  `InsuranceTypeCd` varchar(255) DEFAULT NULL,
  `RiskTypeCd` varchar(255) DEFAULT NULL,
  `RiskTypeDisplay` varchar(255) DEFAULT NULL,
  `ConsumerStatementOnFileInd` varchar(255) DEFAULT NULL,
  `NAICCode` varchar(255) DEFAULT NULL,
  `CancellationDt` date DEFAULT NULL,
  `InceptionDt` date DEFAULT NULL,
  `PaymentPlanCd` varchar(255) DEFAULT NULL,
  `PaymentPlanDisplay` varchar(255) DEFAULT NULL,
  `Premium` varchar(255) DEFAULT NULL,
  `ConsumerContributionInd` varchar(255) DEFAULT NULL,
  `PaymentMethodCd` varchar(255) DEFAULT NULL,
  `PaymentMethodDisplay` varchar(255) DEFAULT NULL,
  `IssuingCompanyId` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `ClassCd` varchar(255) DEFAULT NULL,
  `ClassDisplay` varchar(255) DEFAULT NULL,
  KEY `cppolicyIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cppolicyreportinfo`
--

DROP TABLE IF EXISTS `cppolicyreportinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cppolicyreportinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PolicyNumberChangedInd` varchar(255) DEFAULT NULL,
  `PolicyTypeChangedInd` varchar(255) DEFAULT NULL,
  `EffectiveDtChangedInd` varchar(255) DEFAULT NULL,
  `ExpirationDtChangedInd` varchar(255) DEFAULT NULL,
  `IssuingCompanyIdChangedInd` varchar(255) DEFAULT NULL,
  `IssuingCompanyNameChangedInd` varchar(255) DEFAULT NULL,
  KEY `cppolicyreportinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpprefillsearchcriteria`
--

DROP TABLE IF EXISTS `cpprefillsearchcriteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpprefillsearchcriteria` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `FormerAddressPrimaryNumber` varchar(255) DEFAULT NULL,
  `FormerAddressStreetName` varchar(255) DEFAULT NULL,
  `FormerAddressSecondaryNumber` varchar(255) DEFAULT NULL,
  `FormerAddressCity` varchar(255) DEFAULT NULL,
  `FormerAddressStateProvCd` varchar(255) DEFAULT NULL,
  `FormerAddressPostalCode` varchar(255) DEFAULT NULL,
  `AddressPrimaryNumber` varchar(255) DEFAULT NULL,
  `AddressSecondaryNumber` varchar(255) DEFAULT NULL,
  `AddressStreetName` varchar(255) DEFAULT NULL,
  `AddressCity` varchar(255) DEFAULT NULL,
  `AddressStateProvCd` varchar(255) DEFAULT NULL,
  `AddressPostalCode` varchar(255) DEFAULT NULL,
  KEY `cpprefillsearchcriteriaIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cppriorpolicy`
--

DROP TABLE IF EXISTS `cppriorpolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cppriorpolicy` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CarrierName` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `AMBestNumber` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  `NAICNumber` varchar(255) DEFAULT NULL,
  `PolicyTypeDisplay` varchar(255) DEFAULT NULL,
  `InceptionDt` date DEFAULT NULL,
  `RiskTypeCd` varchar(255) DEFAULT NULL,
  `PolicyStateCd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `LastCancelReasonCd` varchar(255) DEFAULT NULL,
  KEY `cppriorpolicyIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cppriorpolicycoverage`
--

DROP TABLE IF EXISTS `cppriorpolicycoverage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cppriorpolicycoverage` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CoverageTypeCd` varchar(255) DEFAULT NULL,
  `CoverageTypeDisplay` varchar(255) DEFAULT NULL,
  KEY `cppriorpolicycoverageIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cppriorpolicyinsured`
--

DROP TABLE IF EXISTS `cppriorpolicyinsured`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cppriorpolicyinsured` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PolicyHolderName` varchar(255) DEFAULT NULL,
  `SubjectCd` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  KEY `cppriorpolicyinsuredIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cppriorpolicylapse`
--

DROP TABLE IF EXISTS `cppriorpolicylapse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cppriorpolicylapse` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Occurrences` int(11) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `LastCancelDt` date DEFAULT NULL,
  KEY `cppriorpolicylapseIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cppriorpolicyvehicle`
--

DROP TABLE IF EXISTS `cppriorpolicyvehicle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cppriorpolicyvehicle` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `VehIdentificationNumber` varchar(255) DEFAULT NULL,
  `VehicleTypeCd` varchar(255) DEFAULT NULL,
  `VehicleYear` int(11) DEFAULT NULL,
  `BusinessUseInd` varchar(255) DEFAULT NULL,
  `VehicleTypeDisplay` varchar(255) DEFAULT NULL,
  `Limits` varchar(255) DEFAULT NULL,
  `VehicleMake` varchar(255) DEFAULT NULL,
  KEY `cppriorpolicyvehicleIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpremark`
--

DROP TABLE IF EXISTS `cpremark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpremark` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Order` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `TypeDisplay` varchar(255) DEFAULT NULL,
  `ClassCd` varchar(255) DEFAULT NULL,
  `ClassDisplay` varchar(255) DEFAULT NULL,
  `Remarks1` varchar(255) DEFAULT NULL,
  `Remarks2` varchar(255) DEFAULT NULL,
  `NarrativeRemarksCd1` varchar(255) DEFAULT NULL,
  `NarrativeRemarksCd2` varchar(255) DEFAULT NULL,
  KEY `cpremarkIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpremarks`
--

DROP TABLE IF EXISTS `cpremarks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpremarks` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `RemarksTypeCd` varchar(255) DEFAULT NULL,
  KEY `cpremarksIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpreportrouting`
--

DROP TABLE IF EXISTS `cpreportrouting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpreportrouting` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  KEY `cpreportroutingIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpsearchsubject`
--

DROP TABLE IF EXISTS `cpsearchsubject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpsearchsubject` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `SearchSubjectTypeCd` varchar(255) DEFAULT NULL,
  `GivenName` varchar(255) DEFAULT NULL,
  `Surname` varchar(255) DEFAULT NULL,
  `OtherGivenName` varchar(255) DEFAULT NULL,
  `SuffixCd` varchar(255) DEFAULT NULL,
  `GenderCd` varchar(255) DEFAULT NULL,
  `MaritalStatusCd` varchar(255) DEFAULT NULL,
  `DriverLicenseNumber` varchar(255) DEFAULT NULL,
  `DriverLicenseStateProvCd` varchar(255) DEFAULT NULL,
  `BirthDt` date DEFAULT NULL,
  `SSN` varchar(255) DEFAULT NULL,
  KEY `cpsearchsubjectIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpsubject`
--

DROP TABLE IF EXISTS `cpsubject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpsubject` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `SubjectTypeCd` varchar(255) DEFAULT NULL,
  KEY `cpsubjectIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cptelephone`
--

DROP TABLE IF EXISTS `cptelephone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cptelephone` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TelephoneTypeCd` varchar(255) DEFAULT NULL,
  `ClassCd` varchar(255) DEFAULT NULL,
  `AreaCd` varchar(255) DEFAULT NULL,
  `Exchange` varchar(255) DEFAULT NULL,
  `Number` varchar(255) DEFAULT NULL,
  `Ext` varchar(255) DEFAULT NULL,
  `HoursContactFrom` varchar(255) DEFAULT NULL,
  `GroupUsageCd` varchar(255) DEFAULT NULL,
  `HoursContactTo` varchar(255) DEFAULT NULL,
  KEY `cptelephoneIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cptelephonereportinfo`
--

DROP TABLE IF EXISTS `cptelephonereportinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cptelephonereportinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AreaCodeChangedInd` varchar(255) DEFAULT NULL,
  `ExchangedChangedInd` varchar(255) DEFAULT NULL,
  `NumberChangedInd` varchar(255) DEFAULT NULL,
  `DataAssociationDt` date DEFAULT NULL,
  `ExtChangedInd` varchar(255) DEFAULT NULL,
  `DataSourceCd` varchar(255) DEFAULT NULL,
  KEY `cptelephonereportinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpvehicle`
--

DROP TABLE IF EXISTS `cpvehicle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpvehicle` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `VehicleTypeCd` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `ClassCd` varchar(255) DEFAULT NULL,
  `ClassDisplay` varchar(255) DEFAULT NULL,
  `ModelYr` varchar(255) DEFAULT NULL,
  `VehIdentificationNumber` varchar(255) DEFAULT NULL,
  `VehLicenseNumber` varchar(255) DEFAULT NULL,
  `RegistrationStateProvCd` varchar(255) DEFAULT NULL,
  `FormatIndicator` varchar(255) DEFAULT NULL,
  `UnparsedMakeModel` varchar(255) DEFAULT NULL,
  `VehicleClassCd` varchar(255) DEFAULT NULL,
  KEY `cpvehicleIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpvehiclereportinfo`
--

DROP TABLE IF EXISTS `cpvehiclereportinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpvehiclereportinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Odometer` varchar(255) DEFAULT NULL,
  `VehicleDispositionCd` varchar(255) DEFAULT NULL,
  `VehicleDispositionDisplay` varchar(255) DEFAULT NULL,
  `LicensePlateNumberChangedInd` varchar(255) DEFAULT NULL,
  `TitleNumber` varchar(255) DEFAULT NULL,
  `LicenseStateChangedInd` varchar(255) DEFAULT NULL,
  `MakeModelChangedInd` varchar(255) DEFAULT NULL,
  `VINChangedInd` varchar(255) DEFAULT NULL,
  `ModelYrChangedInd` varchar(255) DEFAULT NULL,
  KEY `cpvehiclereportinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cpvinservicesreport`
--

DROP TABLE IF EXISTS `cpvinservicesreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpvinservicesreport` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ProcessingStatusDisplay` varchar(255) DEFAULT NULL,
  `ProcessingStatusCd` varchar(255) DEFAULT NULL,
  KEY `cpvinservicesreportIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `CustomerNumber` varchar(255) DEFAULT NULL,
  `IndexName` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `EntityTypeCd` varchar(255) DEFAULT NULL,
  `Version` varchar(255) DEFAULT NULL,
  `PreferredDeliveryMethod` varchar(255) DEFAULT NULL,
  `VIPLevel` varchar(255) DEFAULT NULL,
  KEY `customerIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customerlogin`
--

DROP TABLE IF EXISTS `customerlogin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerlogin` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `UserId` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `LastLogonDt` date DEFAULT NULL,
  `LastLogonTm` varchar(255) DEFAULT NULL,
  `TermAcceptInd` varchar(255) DEFAULT NULL,
  `TermAcceptDt` varchar(255) DEFAULT NULL,
  `LanguageCd` varchar(255) DEFAULT NULL,
  `AddDt` varchar(255) DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `UpdateDt` varchar(255) DEFAULT NULL,
  `UpdateTm` varchar(255) DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `ChangePasswordAtLogonInd` varchar(255) DEFAULT NULL,
  `TemporaryPasswordExpirationDt` varchar(255) DEFAULT NULL,
  `TemporaryPasswordExpirationTm` varchar(255) DEFAULT NULL,
  KEY `customerloginIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dataextracterrorreport`
--

DROP TABLE IF EXISTS `dataextracterrorreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataextracterrorreport` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ErrorCd` varchar(255) DEFAULT NULL,
  `Message` varchar(255) DEFAULT NULL,
  KEY `dataextracterrorreportIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dataqualdataextractresponse`
--

DROP TABLE IF EXISTS `dataqualdataextractresponse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataqualdataextractresponse` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Extract` varchar(255) DEFAULT NULL,
  `CorrelationId` varchar(255) DEFAULT NULL,
  KEY `dataqualdataextractresponseIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dataqualsearchcriteria`
--

DROP TABLE IF EXISTS `dataqualsearchcriteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataqualsearchcriteria` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CustomerRef` varchar(255) DEFAULT NULL,
  `ProviderRef` varchar(255) DEFAULT NULL,
  `PolicyRef` varchar(255) DEFAULT NULL,
  `ClassificationType` varchar(255) DEFAULT NULL,
  `FileName` varchar(255) DEFAULT NULL,
  KEY `dataqualsearchcriteriaIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `datareport`
--

DROP TABLE IF EXISTS `datareport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datareport` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `TemplateIdRef` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `ResultCd` varchar(255) DEFAULT NULL,
  `RequestTypeCd` varchar(255) DEFAULT NULL,
  `TransmittedDt` date DEFAULT NULL,
  `TransmittedTm` varchar(255) DEFAULT NULL,
  `ReceivedDt` date DEFAULT NULL,
  `ReceivedTm` varchar(255) DEFAULT NULL,
  `ProcessedDt` date DEFAULT NULL,
  `ProcessedTm` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  KEY `datareportIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `datareportrequest`
--

DROP TABLE IF EXISTS `datareportrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datareportrequest` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TemplateIdRef` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `DataReportRef` int(11) DEFAULT NULL,
  `DataReportSourceCd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `AddUser` varchar(255) DEFAULT NULL,
  `CancelDt` date DEFAULT NULL,
  `CancelTm` varchar(255) DEFAULT NULL,
  `CancelUser` varchar(255) DEFAULT NULL,
  `AppliedDt` date DEFAULT NULL,
  `AppliedTm` varchar(255) DEFAULT NULL,
  `RequestTypeCd` varchar(255) DEFAULT NULL,
  `SourceIdRef` varchar(255) DEFAULT NULL,
  `ISOClaimSearchReportType` varchar(255) DEFAULT NULL,
  `OriginalLossDate` date DEFAULT NULL,
  `OriginalPolicyNumber` varchar(255) DEFAULT NULL,
  KEY `datareportrequestIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `daybalance`
--

DROP TABLE IF EXISTS `daybalance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `daybalance` (
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `ReportDate` date DEFAULT NULL,
  `BalanceType` varchar(255) DEFAULT NULL,
  `BalanceGroup` varchar(255) DEFAULT NULL,
  `BalanceLine` varchar(255) DEFAULT NULL,
  `BalanceKey` varchar(255) DEFAULT NULL,
  `BalanceSubKey` varchar(255) DEFAULT NULL,
  `BalanceErrorYN` varchar(255) DEFAULT NULL,
  `BalanceLinesAfter` decimal(28,6) DEFAULT NULL,
  `BalanceSource` varchar(255) DEFAULT NULL,
  `BalanceText` varchar(255) DEFAULT NULL,
  `BalanceAmount` decimal(28,6) DEFAULT NULL,
  `BalanceAmount1` decimal(28,6) DEFAULT NULL,
  `BalanceAmount2` decimal(28,6) DEFAULT NULL,
  `BalanceAmount3` decimal(28,6) DEFAULT NULL,
  `BalanceStatusCd` varchar(255) DEFAULT NULL,
  `BalanceGroupText` varchar(255) DEFAULT NULL,
  KEY `daybalance_1` (`ReportDate`,`BalanceType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `daybalancedetail`
--

DROP TABLE IF EXISTS `daybalancedetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `daybalancedetail` (
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `ReportDate` date DEFAULT NULL,
  `BalanceType` varchar(255) DEFAULT NULL,
  `BalanceSource` varchar(255) DEFAULT NULL,
  `BalanceText` varchar(255) DEFAULT NULL,
  `BalanceGroup` varchar(255) DEFAULT NULL,
  `BalanceLine` varchar(255) DEFAULT NULL,
  `BalanceAmount` decimal(28,6) DEFAULT NULL,
  `BalanceAmount1` decimal(28,6) DEFAULT NULL,
  `BalanceAmount2` decimal(28,6) DEFAULT NULL,
  `BalanceAmount3` decimal(28,6) DEFAULT NULL,
  `BalanceGroupText` varchar(255) DEFAULT NULL,
  `BalanceLinesAfter` decimal(28,6) DEFAULT NULL,
  `Key1Label` varchar(255) DEFAULT NULL,
  `Key1` varchar(255) DEFAULT NULL,
  `Key2Label` varchar(255) DEFAULT NULL,
  `Key2` varchar(255) DEFAULT NULL,
  `Key3Label` varchar(255) DEFAULT NULL,
  `Key3` varchar(255) DEFAULT NULL,
  `Key4Label` varchar(255) DEFAULT NULL,
  `Key4` varchar(255) DEFAULT NULL,
  `Key5Label` varchar(255) DEFAULT NULL,
  `Key5` varchar(255) DEFAULT NULL,
  `CombinedKey` varchar(255) DEFAULT NULL,
  KEY `daybalancedetail_1` (`ReportDate`,`BalanceType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `deductible`
--

DROP TABLE IF EXISTS `deductible`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deductible` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `DeductibleCd` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  KEY `deductibleIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `deductibleallocation`
--

DROP TABLE IF EXISTS `deductibleallocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deductibleallocation` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `DeductibleAllocationTypeCd` varchar(255) DEFAULT NULL,
  `TotalDeductibleAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleOverrideAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleAppliedAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleAllocationCd` varchar(255) DEFAULT NULL,
  `DeductibleOverrideComment` varchar(255) DEFAULT NULL,
  `DeductibleReturnAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleReturnedToDateAmt` decimal(28,6) DEFAULT NULL,
  `PropertyDamagedIdRef` varchar(255) DEFAULT NULL,
  KEY `deductibleallocationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `deductibleendinfo`
--

DROP TABLE IF EXISTS `deductibleendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deductibleendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `LossesSubjecttoDeductibleCd` int(11) DEFAULT NULL,
  `BasisofDeductibleCalculationCd` int(11) DEFAULT NULL,
  `DeductiblePercentage` int(11) DEFAULT NULL,
  `DeductibleAmtPerClaimOrAccident` int(11) DEFAULT NULL,
  `DeductibleAmtAggregate` int(11) DEFAULT NULL,
  `PremiumReductionPercentage` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `deductibleendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `defensebaseactcovendinfo`
--

DROP TABLE IF EXISTS `defensebaseactcovendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `defensebaseactcovendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `WorkDescription` varchar(255) DEFAULT NULL,
  `EndorsementSeqNo` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `defensebaseactcovendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `deliveryattempt`
--

DROP TABLE IF EXISTS `deliveryattempt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliveryattempt` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `DeliveryDt` date DEFAULT NULL,
  `DeliveryStatus` varchar(255) DEFAULT NULL,
  `DeliveryTm` varchar(255) DEFAULT NULL,
  `Message` varchar(255) DEFAULT NULL,
  `LogFilename` varchar(255) DEFAULT NULL,
  KEY `deliveryattemptIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `denialreasonoccurance`
--

DROP TABLE IF EXISTS `denialreasonoccurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `denialreasonoccurance` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `FullDenialReasonCd` varchar(255) DEFAULT NULL,
  KEY `denialreasonoccuranceIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `denialreasontimeoccurance`
--

DROP TABLE IF EXISTS `denialreasontimeoccurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `denialreasontimeoccurance` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `DenialReasonNarrative` varchar(255) DEFAULT NULL,
  KEY `denialreasontimeoccuranceIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `desigworkexcendinfo`
--

DROP TABLE IF EXISTS `desigworkexcendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `desigworkexcendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `WorkDescription` varchar(255) DEFAULT NULL,
  `EndorsementSeqNo` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `desigworkexcendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `destination`
--

DROP TABLE IF EXISTS `destination`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `destination` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Transport` varchar(255) DEFAULT NULL,
  KEY `destinationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `diagnosis`
--

DROP TABLE IF EXISTS `diagnosis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diagnosis` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `DiagnosisNumber` int(11) DEFAULT NULL,
  `DiagnosisCd` varchar(255) DEFAULT NULL,
  KEY `diagnosisIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `discount`
--

DROP TABLE IF EXISTS `discount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discount` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `DiscountAmt` decimal(28,6) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `AppliedInd` varchar(255) DEFAULT NULL,
  KEY `discountIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `distribution`
--

DROP TABLE IF EXISTS `distribution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distribution` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `DeliveryStatus` varchar(255) DEFAULT NULL,
  `SenderName` varchar(255) DEFAULT NULL,
  `SenderEmailAddr` varchar(255) DEFAULT NULL,
  `SenderPhone` varchar(255) DEFAULT NULL,
  `SenderFax` varchar(255) DEFAULT NULL,
  `ReplyToName` varchar(255) DEFAULT NULL,
  `ReplyToEmailAddr` varchar(255) DEFAULT NULL,
  `Subject` varchar(255) DEFAULT NULL,
  `Memo` varchar(255) DEFAULT NULL,
  `Urgency` varchar(255) DEFAULT NULL,
  KEY `distributionIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `distributiondocument`
--

DROP TABLE IF EXISTS `distributiondocument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distributiondocument` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  KEY `distributiondocumentIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `DocumentTypeCd` varchar(255) DEFAULT NULL,
  `DocumentNumber` varchar(255) DEFAULT NULL,
  `Desc` varchar(255) DEFAULT NULL,
  `Filename` varchar(255) DEFAULT NULL,
  `Location` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `AddUser` varchar(255) DEFAULT NULL,
  `OriginatingUser` varchar(255) DEFAULT NULL,
  `MasterDocId` varchar(255) DEFAULT NULL,
  `ManualFormInd` varchar(255) DEFAULT NULL,
  KEY `documentIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `documentdata`
--

DROP TABLE IF EXISTS `documentdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentdata` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  KEY `documentdataIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `documenthistory`
--

DROP TABLE IF EXISTS `documenthistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documenthistory` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ActionDt` date DEFAULT NULL,
  KEY `documenthistoryIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `domagricexclendinfo`
--

DROP TABLE IF EXISTS `domagricexclendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domagricexclendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `FarmOrAgricultureWorkersName` varchar(255) DEFAULT NULL,
  `DomesticOrHouseholdWorkersName` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `domagricexclendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `driver`
--

DROP TABLE IF EXISTS `driver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `driver` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PartyInfoRef` varchar(255) DEFAULT NULL,
  `DriverNumber` int(11) DEFAULT NULL,
  `DriverLicenseNumber` varchar(255) DEFAULT NULL,
  `LicensedDt` date DEFAULT NULL,
  `LicensedStateProvCd` varchar(255) DEFAULT NULL,
  `LengthTimeDriving` int(11) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `FutureStatusCd` varchar(255) DEFAULT NULL,
  KEY `driverIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `driverinfo`
--

DROP TABLE IF EXISTS `driverinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `driverinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `DriverInfoCd` varchar(255) DEFAULT NULL,
  `DriverNumber` int(11) DEFAULT NULL,
  `DriverTypeCd` varchar(255) DEFAULT NULL,
  `DriverStatusCd` varchar(255) DEFAULT NULL,
  `LicenseNumber` varchar(255) DEFAULT NULL,
  `LicenseDt` date DEFAULT NULL,
  `LicensedStateProvCd` varchar(255) DEFAULT NULL,
  `LengthTimeDriving` int(11) DEFAULT NULL,
  `RelationshipToInsuredCd` varchar(255) DEFAULT NULL,
  `ScholasticDiscountInd` varchar(255) DEFAULT NULL,
  `DefensiveDriverInd` varchar(255) DEFAULT NULL,
  `DefensiveDriverEffectiveDt` date DEFAULT NULL,
  `DefensiveDriverExpirationDt` date DEFAULT NULL,
  `GoodDriverDiscountInd` varchar(255) DEFAULT NULL,
  `DriverStartDt` date DEFAULT NULL,
  `RequiredProofOfInsuranceInd` varchar(255) DEFAULT NULL,
  `StatusComments` varchar(255) DEFAULT NULL,
  `MVRRequestInd` varchar(255) DEFAULT NULL,
  `MVRStatus` varchar(255) DEFAULT NULL,
  `MVRStatusDt` date DEFAULT NULL,
  `AccidentPreventCourseCompletDt` date DEFAULT NULL,
  `AccidentPreventionCourseInd` varchar(255) DEFAULT NULL,
  `DriverTrainingInd` varchar(255) DEFAULT NULL,
  `DriverTrainingCompletionDt` date DEFAULT NULL,
  `AttachedVehicleRef` varchar(255) DEFAULT NULL,
  `PermanentLicenseInd` varchar(255) DEFAULT NULL,
  `ScholasticCertificationDt` date DEFAULT NULL,
  `GoodDriverInd` varchar(255) DEFAULT NULL,
  `MatureDriverInd` varchar(255) DEFAULT NULL,
  KEY `driverinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `driverlink`
--

DROP TABLE IF EXISTS `driverlink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `driverlink` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `DriverRef` varchar(255) DEFAULT NULL,
  `DriverTypeCd` varchar(255) DEFAULT NULL,
  `PercentUse` int(11) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `SystemDriverRef` varchar(255) DEFAULT NULL,
  `ManualDriverRef` varchar(255) DEFAULT NULL,
  KEY `driverlinkIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `driverpoints`
--

DROP TABLE IF EXISTS `driverpoints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `driverpoints` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TemplateId` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `DriverPointsNumber` int(11) DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `InfractionCd` varchar(255) DEFAULT NULL,
  `InfractionDt` date DEFAULT NULL,
  `PointsChargeable` int(11) DEFAULT NULL,
  `PointsCharged` int(11) DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `IgnoreInd` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `ConvictionDt` date DEFAULT NULL,
  `DirectPortalIncidentId` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  KEY `driverpointsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `drivingrecord`
--

DROP TABLE IF EXISTS `drivingrecord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drivingrecord` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `DrivingRecordCd` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  KEY `drivingrecordIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `earningoccurance`
--

DROP TABLE IF EXISTS `earningoccurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `earningoccurance` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ReducedEarningsWeekNo` varchar(255) DEFAULT NULL,
  `ActualReducedEarnings` varchar(255) DEFAULT NULL,
  `DeemedReducedEarnings` varchar(255) DEFAULT NULL,
  KEY `earningoccuranceIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `elecpaymentcontract`
--

DROP TABLE IF EXISTS `elecpaymentcontract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elecpaymentcontract` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ContractTypeCd` varchar(255) DEFAULT NULL,
  `MethodCd` varchar(255) DEFAULT NULL,
  `ACHImmediateOrigin` varchar(255) DEFAULT NULL,
  `ACHImmediateOriginName` varchar(255) DEFAULT NULL,
  `ACHImmediateDestination` varchar(255) DEFAULT NULL,
  `ACHImmediateDestinationName` varchar(255) DEFAULT NULL,
  `ACHCompanyId` varchar(255) DEFAULT NULL,
  `ACHProcessUrl` varchar(255) DEFAULT NULL,
  `ACHExceptionUrl` varchar(255) DEFAULT NULL,
  `ACHExceptionBackupFilename` varchar(255) DEFAULT NULL,
  `ACHExceptionFileExpectedInd` varchar(255) DEFAULT NULL,
  `ACHIncludeBalanceRecordInd` varchar(255) DEFAULT NULL,
  KEY `elecpaymentcontractIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `elecpaymentdestination`
--

DROP TABLE IF EXISTS `elecpaymentdestination`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elecpaymentdestination` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `DestinationTypeCd` varchar(255) DEFAULT NULL,
  `ACHName` varchar(255) DEFAULT NULL,
  `ACHBankAccountNumber` varchar(255) DEFAULT NULL,
  `ACHBankAccountTypeCd` varchar(255) DEFAULT NULL,
  `ACHRoutingNumber` varchar(255) DEFAULT NULL,
  `ACHBankName` varchar(255) DEFAULT NULL,
  `ACHStandardEntryClassCd` varchar(255) DEFAULT NULL,
  `DebitCardIssuer` varchar(255) DEFAULT NULL,
  `DebitCardTypeCd` varchar(255) DEFAULT NULL,
  `DebitCardNumber` varchar(255) DEFAULT NULL,
  `DebitCardId` varchar(255) DEFAULT NULL,
  `DebitCardPaymentId` varchar(255) DEFAULT NULL,
  `DebitCardTransactionId` varchar(255) DEFAULT NULL,
  KEY `elecpaymentdestinationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `electronicpayment`
--

DROP TABLE IF EXISTS `electronicpayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `electronicpayment` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `MethodCd` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `SourceModule` varchar(255) DEFAULT NULL,
  `SourceModelName` varchar(255) DEFAULT NULL,
  `SourceRef` varchar(255) DEFAULT NULL,
  `SourceIdRef` varchar(255) DEFAULT NULL,
  `SourceName` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `Amount` decimal(28,6) DEFAULT NULL,
  `SentDt` date DEFAULT NULL,
  `SentTm` varchar(255) DEFAULT NULL,
  `AcknowledgeDt` date DEFAULT NULL,
  `AcknowledgeTm` varchar(255) DEFAULT NULL,
  `SettlementDt` date DEFAULT NULL,
  `ReceiptIdRef` varchar(255) DEFAULT NULL,
  `ElectronicPaymentStatementId` varchar(255) DEFAULT NULL,
  `StatementAccountRef` varchar(255) DEFAULT NULL,
  KEY `electronicpaymentIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `electronicpaymentsource`
--

DROP TABLE IF EXISTS `electronicpaymentsource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `electronicpaymentsource` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `SourceTypeCd` varchar(255) DEFAULT NULL,
  `SourceName` varchar(255) DEFAULT NULL,
  `MethodCd` varchar(255) DEFAULT NULL,
  `ACHName` varchar(255) DEFAULT NULL,
  `ACHBankAccountTypeCd` varchar(255) DEFAULT NULL,
  `ACHBankAccountNumber` varchar(255) DEFAULT NULL,
  `ACHRoutingNumber` varchar(255) DEFAULT NULL,
  `ACHBankName` varchar(255) DEFAULT NULL,
  `ACHStandardEntryClassCd` varchar(255) DEFAULT NULL,
  `AgentTrustInd` varchar(255) DEFAULT NULL,
  `CreditCardNumber` varchar(255) DEFAULT NULL,
  `CreditCardTypeCd` varchar(255) DEFAULT NULL,
  `CreditCardExpirationMonth` varchar(255) DEFAULT NULL,
  `CreditCardExpirationYr` varchar(255) DEFAULT NULL,
  `CreditCardHolderName` varchar(255) DEFAULT NULL,
  `CreditCardSecurityCd` varchar(255) DEFAULT NULL,
  `CreditCardAuthorizationCd` varchar(255) DEFAULT NULL,
  `CreditCardAuthorizationMessage` varchar(255) DEFAULT NULL,
  `CustomerProfileId` varchar(255) DEFAULT NULL,
  `CustomerPaymentProfileId` varchar(255) DEFAULT NULL,
  `TransactionId` varchar(255) DEFAULT NULL,
  `ReminderDt` date DEFAULT NULL,
  KEY `electronicpaymentsourceIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `electronicpaymentstatus`
--

DROP TABLE IF EXISTS `electronicpaymentstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `electronicpaymentstatus` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ACHBatchId` varchar(255) DEFAULT NULL,
  `ACHTraceNumber` varchar(255) DEFAULT NULL,
  `ACHExceptionCd` varchar(255) DEFAULT NULL,
  `ACHExceptionMsg` varchar(255) DEFAULT NULL,
  KEY `electronicpaymentstatusIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emailinfo`
--

DROP TABLE IF EXISTS `emailinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `EmailTypeCd` varchar(255) DEFAULT NULL,
  `EmailAddr` varchar(255) DEFAULT NULL,
  `PreferredInd` varchar(255) DEFAULT NULL,
  KEY `emailinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `employersoccurance`
--

DROP TABLE IF EXISTS `employersoccurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employersoccurance` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ConcurrentEmployerName` varchar(255) DEFAULT NULL,
  `ConcurrentEmployerPhoneNo` varchar(255) DEFAULT NULL,
  `ConcurrentEmployerWage` varchar(255) DEFAULT NULL,
  KEY `employersoccuranceIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emprliabcovendinfo`
--

DROP TABLE IF EXISTS `emprliabcovendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emprliabcovendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `emprliabcovendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `eombalance`
--

DROP TABLE IF EXISTS `eombalance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eombalance` (
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `ReportPeriod` varchar(6) DEFAULT NULL,
  `BalanceType` varchar(255) DEFAULT NULL,
  `BalanceGroup` varchar(255) DEFAULT NULL,
  `BalanceLine` varchar(255) DEFAULT NULL,
  `BalanceKey` varchar(255) DEFAULT NULL,
  `BalanceSubKey` varchar(255) DEFAULT NULL,
  `BalanceErrorYN` varchar(255) DEFAULT NULL,
  `BalancePageBreak` varchar(255) DEFAULT NULL,
  `BalanceLinesBefore` decimal(28,6) DEFAULT NULL,
  `BalanceLinesAfter` decimal(28,6) DEFAULT NULL,
  `BalanceSource` varchar(255) DEFAULT NULL,
  `BalanceText` varchar(255) DEFAULT NULL,
  `BalanceAmount` decimal(28,6) DEFAULT NULL,
  `BalanceAmount1` decimal(28,6) DEFAULT NULL,
  `BalanceAmount2` decimal(28,6) DEFAULT NULL,
  `BalanceAmount3` decimal(28,6) DEFAULT NULL,
  `BalanceStatusCd` varchar(255) DEFAULT NULL,
  `BalanceGroupText` varchar(255) DEFAULT NULL,
  KEY `eombalance_1` (`ReportPeriod`,`BalanceType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `eombalancedetail`
--

DROP TABLE IF EXISTS `eombalancedetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eombalancedetail` (
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `ReportPeriod` varchar(255) DEFAULT NULL,
  `BalanceType` varchar(255) DEFAULT NULL,
  `BalanceSource` varchar(255) DEFAULT NULL,
  `BalanceText` varchar(255) DEFAULT NULL,
  `BalanceGroup` varchar(255) DEFAULT NULL,
  `BalanceLine` varchar(255) DEFAULT NULL,
  `BalanceAmount` decimal(28,6) DEFAULT NULL,
  `BalanceAmount1` decimal(28,6) DEFAULT NULL,
  `BalanceAmount2` decimal(28,6) DEFAULT NULL,
  `BalanceAmount3` decimal(28,6) DEFAULT NULL,
  `BalanceGroupText` varchar(255) DEFAULT NULL,
  `BalanceLinesAfter` decimal(28,6) DEFAULT NULL,
  `Key1Label` varchar(255) DEFAULT NULL,
  `Key1` varchar(255) DEFAULT NULL,
  `Key2Label` varchar(255) DEFAULT NULL,
  `Key2` varchar(255) DEFAULT NULL,
  `Key3Label` varchar(255) DEFAULT NULL,
  `Key3` varchar(255) DEFAULT NULL,
  `Key4Label` varchar(255) DEFAULT NULL,
  `Key4` varchar(255) DEFAULT NULL,
  `Key5Label` varchar(255) DEFAULT NULL,
  `Key5` varchar(255) DEFAULT NULL,
  `CombinedKey` varchar(255) DEFAULT NULL,
  KEY `eombalancedetail_1` (`ReportPeriod`,`BalanceType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `experiencechart`
--

DROP TABLE IF EXISTS `experiencechart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experiencechart` (
  `ReportPeriod` char(6) DEFAULT NULL,
  `DataPeriod` char(6) DEFAULT NULL,
  `ExperienceType` varchar(255) DEFAULT NULL,
  `ExperienceKey` varchar(255) DEFAULT NULL,
  `ExperienceSort1` varchar(255) DEFAULT NULL,
  `ExperienceSort2` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `ProducerCd` varchar(255) DEFAULT NULL,
  `BranchCd` varchar(255) DEFAULT NULL,
  `ProductCd` varchar(255) DEFAULT NULL,
  `MTDWrittenAmt` decimal(28,6) DEFAULT NULL,
  `MTDEarnedAmt` decimal(28,6) DEFAULT NULL,
  `MTDIndemnityIAmt` decimal(28,6) DEFAULT NULL,
  `PartyName` varchar(255) DEFAULT NULL,
  `Address1` varchar(255) DEFAULT NULL,
  `Address2` varchar(255) DEFAULT NULL,
  `Address3` varchar(255) DEFAULT NULL,
  `Address4` varchar(255) DEFAULT NULL,
  `Address5` varchar(255) DEFAULT NULL,
  `TotalWork` decimal(28,6) DEFAULT NULL,
  `DateWork` varchar(255) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `experienceranking`
--

DROP TABLE IF EXISTS `experienceranking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experienceranking` (
  `Rownum` int(11) NOT NULL AUTO_INCREMENT,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `ProducerCd` varchar(255) DEFAULT NULL,
  `BranchCd` varchar(255) DEFAULT NULL,
  `ReportPeriod` varchar(255) DEFAULT NULL,
  `RankingText` varchar(255) DEFAULT NULL,
  `YTDWrittenAmt` decimal(28,6) DEFAULT NULL,
  `PYTDWrittenAmt` decimal(28,6) DEFAULT NULL,
  `HistWrittenAmt` decimal(28,6) DEFAULT NULL,
  `YTDWrittenRank` decimal(28,0) DEFAULT NULL,
  `PYTDWrittenRank` decimal(28,0) DEFAULT NULL,
  `YTDEarnedAmt` decimal(28,6) DEFAULT NULL,
  `PYTDEarnedAmt` decimal(28,6) DEFAULT NULL,
  `HistEarnedAmt` decimal(28,6) DEFAULT NULL,
  `YTDEarnedRank` decimal(28,0) DEFAULT NULL,
  `PYTDEarnedRank` decimal(28,0) DEFAULT NULL,
  `YTDIncurredAmt` decimal(28,6) DEFAULT NULL,
  `PYTDIncurredAmt` decimal(28,6) DEFAULT NULL,
  `HistIncurredAmt` decimal(28,6) DEFAULT NULL,
  `YTDIncurredRank` decimal(28,0) DEFAULT NULL,
  `PYTDIncurredRank` decimal(28,0) DEFAULT NULL,
  `YTDLossRatio` decimal(28,6) DEFAULT NULL,
  `PYTDLossRatio` decimal(28,6) DEFAULT NULL,
  `HistLossRatio` decimal(28,6) DEFAULT NULL,
  `YTDLossRatioRank` decimal(28,0) DEFAULT NULL,
  `PYTDLossRatioRank` decimal(28,0) DEFAULT NULL,
  `YTDPIFCount` decimal(28,0) DEFAULT NULL,
  `PYTDPIFCount` decimal(28,0) DEFAULT NULL,
  `HistAVGPIFCount` decimal(28,0) DEFAULT NULL,
  `YTDPIFRank` decimal(28,0) DEFAULT NULL,
  `PYTDPIFRank` decimal(28,0) DEFAULT NULL,
  `YTDRetentionRatio` decimal(28,6) DEFAULT NULL,
  `PYTDRetentionRatio` decimal(28,6) DEFAULT NULL,
  `HistRetentionRatio` decimal(28,6) DEFAULT NULL,
  `YTDRetentionRank` decimal(28,0) DEFAULT NULL,
  `PYTDRetentionRank` decimal(28,0) DEFAULT NULL,
  `YTDPremGrowthAmt` decimal(28,6) DEFAULT NULL,
  `PYTDPremGrowthAmt` decimal(28,6) DEFAULT NULL,
  `HistAVGPremGrowthAmt` decimal(28,6) DEFAULT NULL,
  `YTDPremGrowthRank` decimal(28,0) DEFAULT NULL,
  `PYTDPremGrowthRank` decimal(28,0) DEFAULT NULL,
  `YTDProfitAmt` decimal(28,6) DEFAULT NULL,
  `PYTDProfitAmt` decimal(28,6) DEFAULT NULL,
  `HistAVGProfitAmt` decimal(28,6) DEFAULT NULL,
  `YTDProfitRank` decimal(28,0) DEFAULT NULL,
  `PYTDProfitRank` decimal(28,0) DEFAULT NULL,
  `ProducerName` varchar(255) DEFAULT NULL,
  `WorkAMT1` decimal(28,6) DEFAULT NULL,
  `WorkAMT2` decimal(28,6) DEFAULT NULL,
  `Sort1` decimal(8,0) DEFAULT NULL,
  `Sort2` decimal(28,6) DEFAULT NULL,
  `Text1` varchar(255) DEFAULT NULL,
  `Text2` varchar(255) DEFAULT NULL,
  `Rank1` decimal(8,0) DEFAULT NULL,
  `Rank2` decimal(8,0) DEFAULT NULL,
  `Change1` decimal(8,0) DEFAULT NULL,
  KEY `Rownum` (`Rownum`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `experiencereport`
--

DROP TABLE IF EXISTS `experiencereport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experiencereport` (
  `ReportPeriod` char(6) DEFAULT NULL,
  `ExperienceType` varchar(255) DEFAULT NULL,
  `ExperienceKey` varchar(255) DEFAULT NULL,
  `ExperienceSort1` varchar(255) DEFAULT NULL,
  `ExperienceSort2` varchar(255) DEFAULT NULL,
  `ExperienceCd` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `ProducerCd` varchar(255) DEFAULT NULL,
  `BranchCd` varchar(255) DEFAULT NULL,
  `YTDWrittenAmt` decimal(28,6) DEFAULT NULL,
  `LYTDWrittenAmt` decimal(28,6) DEFAULT NULL,
  `PercentWrittenAmt` decimal(28,6) DEFAULT NULL,
  `WPPercentChange` decimal(28,6) DEFAULT NULL,
  `YTDEarnedAmt` decimal(28,6) DEFAULT NULL,
  `LYTDEarnedAmt` decimal(28,6) DEFAULT NULL,
  `YTDIndemnityIAmt` decimal(28,6) DEFAULT NULL,
  `LYTDIndemnityIAmt` decimal(28,6) DEFAULT NULL,
  `YTDIERatio` decimal(28,6) DEFAULT NULL,
  `LYTDIERatio` decimal(28,6) DEFAULT NULL,
  `YTDExpenseIAmt` decimal(28,6) DEFAULT NULL,
  `LYTDExpenseIAmt` decimal(28,6) DEFAULT NULL,
  `RenewingPremium` decimal(28,6) DEFAULT NULL,
  `RenewedPremium` decimal(28,6) DEFAULT NULL,
  `RenewedPercent` decimal(28,6) DEFAULT NULL,
  `RenewingCount` decimal(28,6) DEFAULT NULL,
  `RenewedCount` decimal(28,6) DEFAULT NULL,
  `PercentRenewedCount` decimal(28,6) DEFAULT NULL,
  `RenewingPremium12MO` decimal(28,6) DEFAULT NULL,
  `RenewedPremium12MO` decimal(28,6) DEFAULT NULL,
  `RenewingCount12MO` decimal(28,6) DEFAULT NULL,
  `RenewedCount12MO` decimal(28,6) DEFAULT NULL,
  `InForcePolicyCount` decimal(28,6) DEFAULT NULL,
  `InForceCountSameMonthPY` decimal(28,6) DEFAULT NULL,
  `InForceCountEOPY` decimal(28,6) DEFAULT NULL,
  `ChangeInPIF` decimal(28,6) DEFAULT NULL,
  `NewBusinessPolicyCount` decimal(28,6) DEFAULT NULL,
  `RenewalPolicyCount` decimal(28,6) DEFAULT NULL,
  `NewBusinessBookedCount` decimal(28,6) DEFAULT NULL,
  `RenewalBusinessBookedCount` decimal(28,6) DEFAULT NULL,
  `NewBusinessPolicyCountYTD` decimal(28,6) DEFAULT NULL,
  `RenewalPolicyCountYTD` decimal(28,6) DEFAULT NULL,
  `NewBusinessBookedCountYTD` decimal(28,6) DEFAULT NULL,
  `RenewalBusinessBookedCountYTD` decimal(28,6) DEFAULT NULL,
  `PartyName` varchar(255) DEFAULT NULL,
  `Address1` varchar(255) DEFAULT NULL,
  `Address2` varchar(255) DEFAULT NULL,
  `Address3` varchar(255) DEFAULT NULL,
  `Address4` varchar(255) DEFAULT NULL,
  `Address5` varchar(255) DEFAULT NULL,
  `TotalWork` decimal(28,6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `expratingendinfo`
--

DROP TABLE IF EXISTS `expratingendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expratingendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` int(11) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `ExpModEffDt` varchar(255) DEFAULT NULL,
  `ExpModFactor` varchar(255) DEFAULT NULL,
  `ExpModStsCd` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `expratingendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `expratingmodfactorendinfo`
--

DROP TABLE IF EXISTS `expratingmodfactorendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expratingmodfactorendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `ContingentModEffectiveDt` int(11) DEFAULT NULL,
  `ContingentExpModFactor` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `expratingmodfactorendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `feature`
--

DROP TABLE IF EXISTS `feature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feature` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `FeatureCd` varchar(255) DEFAULT NULL,
  `FeatureSubCd` varchar(255) DEFAULT NULL,
  `ItemNumber` int(11) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `StatusDt` date DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `ReserveAmt` decimal(28,6) DEFAULT NULL,
  `RecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ExpectedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `DenyInd` varchar(255) DEFAULT NULL,
  `DenyDt` date DEFAULT NULL,
  `PropertyDamagedIdRef` varchar(255) DEFAULT NULL,
  `DenyTypeCd` varchar(255) DEFAULT NULL,
  `DenyReasonCd` varchar(255) DEFAULT NULL,
  KEY `featureIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `featureallocation`
--

DROP TABLE IF EXISTS `featureallocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `featureallocation` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `FeatureCd` varchar(255) DEFAULT NULL,
  `FeatureSubCd` varchar(255) DEFAULT NULL,
  `ItemNumber` int(11) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `ReserveAmt` decimal(28,6) DEFAULT NULL,
  `RecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ExpectedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `StatusDt` date DEFAULT NULL,
  `PropertyDamagedIdRef` varchar(255) DEFAULT NULL,
  `DenyTypeCd` varchar(255) DEFAULT NULL,
  `DenyReasonCd` varchar(255) DEFAULT NULL,
  KEY `featureallocationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `featureinfo`
--

DROP TABLE IF EXISTS `featureinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `featureinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `FeatureIdRef` varchar(255) DEFAULT NULL,
  `FeatureCd` varchar(255) DEFAULT NULL,
  `FeatureSubCd` varchar(255) DEFAULT NULL,
  `NumberVisits` varchar(255) DEFAULT NULL,
  `Rate` varchar(255) DEFAULT NULL,
  `TotalRecommendedReserveAmt` varchar(255) DEFAULT NULL,
  `TotalOutstandindReserveAmt` varchar(255) DEFAULT NULL,
  `NetChangeAmt` varchar(255) DEFAULT NULL,
  `TotalPaidAmt` varchar(255) DEFAULT NULL,
  `TotalRecoveriesAmt` varchar(255) DEFAULT NULL,
  `NewTotalIncurredAmt` varchar(255) DEFAULT NULL,
  KEY `featureinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `federalcmhsactcovendinfo`
--

DROP TABLE IF EXISTS `federalcmhsactcovendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `federalcmhsactcovendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `WorkDescription` varchar(255) DEFAULT NULL,
  `EndorsementSeqNo` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `federalcmhsactcovendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `federalemprliabactendinfo`
--

DROP TABLE IF EXISTS `federalemprliabactendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `federalemprliabactendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `BIAAmount` int(11) DEFAULT NULL,
  `BIDAmount` int(11) DEFAULT NULL,
  `ScheduleStateCd` int(11) DEFAULT NULL,
  `EndorsementSeqNo` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `federalemprliabactendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fee`
--

DROP TABLE IF EXISTS `fee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fee` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CategoryCd` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `FullTermAmt` decimal(28,6) DEFAULT NULL,
  `FullTermManualAmt` decimal(28,6) DEFAULT NULL,
  `WrittenAmt` decimal(28,6) DEFAULT NULL,
  `FinalAmt` decimal(28,6) DEFAULT NULL,
  `OriginalCommissionPct` decimal(28,6) DEFAULT NULL,
  `CommissionPct` decimal(28,6) DEFAULT NULL,
  `ContributionPct` decimal(28,6) DEFAULT NULL,
  `CommissionAmt` decimal(28,6) DEFAULT NULL,
  `PrevFinalAmt` decimal(28,6) DEFAULT NULL,
  `PrevWrittenAmt` decimal(28,6) DEFAULT NULL,
  `TransactionCommissionAmt` decimal(28,6) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `TransactionIdRef` varchar(255) DEFAULT NULL,
  `BillingCategoryCd` varchar(255) DEFAULT NULL,
  `ShortRateAmt` decimal(28,6) DEFAULT NULL,
  `CoveredStateIdRef` varchar(255) DEFAULT NULL,
  KEY `feeIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fieldcomparison`
--

DROP TABLE IF EXISTS `fieldcomparison`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldcomparison` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `FieldDescription` varchar(255) DEFAULT NULL,
  KEY `fieldcomparisonIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fieldvalueformatted`
--

DROP TABLE IF EXISTS `fieldvalueformatted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldvalueformatted` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `QuoteIdRef` int(11) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  KEY `fieldvalueformattedIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `financialinstitutioninfo`
--

DROP TABLE IF EXISTS `financialinstitutioninfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `financialinstitutioninfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ACHRoutingNumber` varchar(255) DEFAULT NULL,
  `ACHNewRoutingNumber` varchar(255) DEFAULT NULL,
  `AddUser` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `ChangeUser` varchar(255) DEFAULT NULL,
  `ChangeDt` date DEFAULT NULL,
  KEY `financialinstitutioninfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `form`
--

DROP TABLE IF EXISTS `form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `FormNum` varchar(255) DEFAULT NULL,
  `FormEdition` varchar(255) DEFAULT NULL,
  `IncludeInFormListInd` varchar(255) DEFAULT NULL,
  `PrintInd` varchar(255) DEFAULT NULL,
  `AreaCd` varchar(255) DEFAULT NULL,
  `LevelCd` varchar(255) DEFAULT NULL,
  `Order` varchar(255) DEFAULT NULL,
  `FillinInd` varchar(255) DEFAULT NULL,
  `ProductFormIdRef` varchar(255) DEFAULT NULL,
  `SystemAttachedInd` varchar(255) DEFAULT NULL,
  `FutureStatusCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  KEY `formIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `formlink`
--

DROP TABLE IF EXISTS `formlink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formlink` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `FormLinkCd` varchar(255) DEFAULT NULL,
  `ProductFormRef` varchar(255) DEFAULT NULL,
  `RequiredInd` varchar(255) DEFAULT NULL,
  `ShowToUserInd` varchar(255) DEFAULT NULL,
  KEY `formlinkIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `froiclaiminfo`
--

DROP TABLE IF EXISTS `froiclaiminfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `froiclaiminfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TransactionSetID` varchar(255) DEFAULT NULL,
  `MaintenanceTypeCd` varchar(255) DEFAULT NULL,
  `MaintenanceTypeCdDt` varchar(255) DEFAULT NULL,
  `IndustryCd` varchar(255) DEFAULT NULL,
  `InsurerFein` varchar(255) DEFAULT NULL,
  `InsuredLocationId` varchar(255) DEFAULT NULL,
  `InjuryTm` varchar(255) DEFAULT NULL,
  `AccidentSitePostalCd` varchar(255) DEFAULT NULL,
  `NatureofInjuryCd` varchar(255) DEFAULT NULL,
  `PartofBodyInjuredCd` varchar(255) DEFAULT NULL,
  `CauseofInjuryCd` varchar(255) DEFAULT NULL,
  `InitialTreatmentCd` varchar(255) DEFAULT NULL,
  `EmployerReportedDt` varchar(255) DEFAULT NULL,
  `NoticeReceivedDt` varchar(255) DEFAULT NULL,
  `DisabilityBeganDt` varchar(255) DEFAULT NULL,
  `EmploymentStsCd` varchar(255) DEFAULT NULL,
  `ClassificationCd` varchar(255) DEFAULT NULL,
  `HireDt` varchar(255) DEFAULT NULL,
  `Wage` varchar(255) DEFAULT NULL,
  `WagePeriodCd` varchar(255) DEFAULT NULL,
  `DaysWorkedPerWeek` varchar(255) DEFAULT NULL,
  `LastWorkedDt` varchar(255) DEFAULT NULL,
  KEY `froiclaiminfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `froicommondata`
--

DROP TABLE IF EXISTS `froicommondata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `froicommondata` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `JurisdictionCd` varchar(255) DEFAULT NULL,
  `ClaimNo` varchar(255) DEFAULT NULL,
  `ClaimAdminCity` varchar(255) DEFAULT NULL,
  `ClaimAdminStateCd` varchar(255) DEFAULT NULL,
  `ClaimAdminPostalCd` varchar(255) DEFAULT NULL,
  `ClaimAdminClaimNo` varchar(255) DEFAULT NULL,
  `ClaimAdminFEIN` varchar(255) DEFAULT NULL,
  `ClaimAdminName` varchar(255) DEFAULT NULL,
  `ClaimRepName` varchar(255) DEFAULT NULL,
  `ClaimRepPhoneNo` varchar(255) DEFAULT NULL,
  `ClaimRepFaxNo` varchar(255) DEFAULT NULL,
  `ClaimRepEmailId` varchar(255) DEFAULT NULL,
  `ClaimAdminInfoAttentionLine` varchar(255) DEFAULT NULL,
  `ClaimAdminPrimaryAddress` varchar(255) DEFAULT NULL,
  `ClaimAdminSecondaryAddress` varchar(255) DEFAULT NULL,
  `ClaimAdminCountryCd` varchar(255) DEFAULT NULL,
  `EmployerFEIN` varchar(255) DEFAULT NULL,
  `EmployerPhysicalCity` varchar(255) DEFAULT NULL,
  `EmployerPhysicalStateCd` varchar(255) DEFAULT NULL,
  `EmployerPhysicalPostalCd` varchar(255) DEFAULT NULL,
  `PolicyEffectiveDt` varchar(255) DEFAULT NULL,
  `PolicyExpirationDt` varchar(255) DEFAULT NULL,
  `PolicyNo` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EmployerName` varchar(255) DEFAULT NULL,
  `EmployerPrimAddress` varchar(255) DEFAULT NULL,
  `EmployerSecAddress` varchar(255) DEFAULT NULL,
  `EmployerPhysicalCountryCd` varchar(255) DEFAULT NULL,
  `EmployerContactNo` varchar(255) DEFAULT NULL,
  `EmployerContactName` varchar(255) DEFAULT NULL,
  `EmployerCountyCd` varchar(255) DEFAULT NULL,
  `EmployeeFirstName` varchar(255) DEFAULT NULL,
  `EmployeeMailingCity` varchar(255) DEFAULT NULL,
  `EmployeeMailingStateCd` varchar(255) DEFAULT NULL,
  `EmployeeMailingPostalCd` varchar(255) DEFAULT NULL,
  `EmployeeDOB` varchar(255) DEFAULT NULL,
  `EmployeeGenderCd` varchar(255) DEFAULT NULL,
  `EmployeeMaritalStatusCd` varchar(255) DEFAULT NULL,
  `EmployeeNoOfDependents` varchar(255) DEFAULT NULL,
  `EmployeeLastName` varchar(255) DEFAULT NULL,
  `EmployeeLastNameSuffix` varchar(255) DEFAULT NULL,
  `EmployeeMiddleNameInitial` varchar(255) DEFAULT NULL,
  `EmployeeMailingPrimaryAddress` varchar(255) DEFAULT NULL,
  `EmployeeMailingSecondaryAddress` varchar(255) DEFAULT NULL,
  `EmployeePhoneNo` varchar(255) DEFAULT NULL,
  `ClaimStatusCd` varchar(255) DEFAULT NULL,
  `ClaimTypeCd` varchar(255) DEFAULT NULL,
  `DeathResultofInjuryCd` varchar(255) DEFAULT NULL,
  `TypeOfLossCd` varchar(255) DEFAULT NULL,
  `ReturnToWorkTypeCd` varchar(255) DEFAULT NULL,
  `ReturnToWorkDate` varchar(255) DEFAULT NULL,
  `PhysicalRestrictionsInd` varchar(255) DEFAULT NULL,
  `LateReasonCd` varchar(255) DEFAULT NULL,
  `DisabilityDt` varchar(255) DEFAULT NULL,
  `DeathDt` varchar(255) DEFAULT NULL,
  `InjuryDt` varchar(255) DEFAULT NULL,
  `JurisdictionClaimNo` varchar(255) DEFAULT NULL,
  `EmpID` varchar(255) DEFAULT NULL,
  `FullDenialEffectiveDt` varchar(255) DEFAULT NULL,
  `EmploymentStsCd` varchar(255) DEFAULT NULL,
  `WorkDt` varchar(255) DEFAULT NULL,
  `FullWagesPaid` varchar(255) DEFAULT NULL,
  KEY `froicommondataIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `froicompanioninfo`
--

DROP TABLE IF EXISTS `froicompanioninfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `froicompanioninfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ClaimNo` varchar(255) DEFAULT NULL,
  `TransactionSetID` varchar(255) DEFAULT NULL,
  `AccidentPremisesCd` varchar(255) DEFAULT NULL,
  `AccidentSiteCounty` varchar(255) DEFAULT NULL,
  `AccidentSiteLocationDesc` varchar(255) DEFAULT NULL,
  `AccidentSiteCity` varchar(255) DEFAULT NULL,
  `AccidentSiteStreet` varchar(255) DEFAULT NULL,
  `AccidentSiteStateCd` varchar(255) DEFAULT NULL,
  `AccidentSiteCountryCd` varchar(255) DEFAULT NULL,
  `EmployerName` varchar(255) DEFAULT NULL,
  `InsurerName` varchar(255) DEFAULT NULL,
  `EmployerMailingAttentionLine` varchar(255) DEFAULT NULL,
  `EmployerMailingPhysicalCity` varchar(255) DEFAULT NULL,
  `EmployerMailingCountyCd` varchar(255) DEFAULT NULL,
  `EmployerMailingPostalCd` varchar(255) DEFAULT NULL,
  `EmployerMailingPrimAddress` varchar(255) DEFAULT NULL,
  `EmployerMailingSecAddress` varchar(255) DEFAULT NULL,
  `EmployerMailingStateCd` varchar(255) DEFAULT NULL,
  `EmployerMailingCity` varchar(255) DEFAULT NULL,
  `OccupationDescription` varchar(255) DEFAULT NULL,
  `EmployerUINo` varchar(255) DEFAULT NULL,
  KEY `froicompanioninfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `froicontrib`
--

DROP TABLE IF EXISTS `froicontrib`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `froicontrib` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `ReportPeriodBeginDt` date DEFAULT NULL,
  `ReportPeriodEndDt` date DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `RegeneratedInd` varchar(255) DEFAULT NULL,
  `ClaimRef` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `ProcessDt` date DEFAULT NULL,
  `ProcessTm` varchar(255) DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  KEY `froicontribIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `froiheader`
--

DROP TABLE IF EXISTS `froiheader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `froiheader` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TransactionSetID` varchar(255) DEFAULT NULL,
  `SenderFein` varchar(255) DEFAULT NULL,
  `SenderPostalID` varchar(255) DEFAULT NULL,
  `ReceiverFein` varchar(255) DEFAULT NULL,
  `ReceiverPostalID` varchar(255) DEFAULT NULL,
  `TransmissionSentDt` varchar(255) DEFAULT NULL,
  `TransmissionSentTm` varchar(255) DEFAULT NULL,
  `OriginalTransmissionDt` varchar(255) DEFAULT NULL,
  `OriginalTransmissionTm` varchar(255) DEFAULT NULL,
  `TestProductionCd` varchar(255) DEFAULT NULL,
  `ReleaseNo` varchar(255) DEFAULT NULL,
  `VersionNo` varchar(255) DEFAULT NULL,
  `BatchTypeCd` varchar(255) DEFAULT NULL,
  KEY `froiheaderIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `froitrailer`
--

DROP TABLE IF EXISTS `froitrailer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `froitrailer` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TransactionSetID` varchar(255) DEFAULT NULL,
  `DetailRecordCnt` varchar(255) DEFAULT NULL,
  `TransactionCnt` varchar(255) DEFAULT NULL,
  KEY `froitrailerIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `glclass`
--

DROP TABLE IF EXISTS `glclass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glclass` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ExposureNumber` int(11) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `FutureStatusCd` varchar(255) DEFAULT NULL,
  `ScheduledPremiumMod` int(11) DEFAULT NULL,
  `Stories` int(11) DEFAULT NULL,
  `Units` int(11) DEFAULT NULL,
  `Pools` int(11) DEFAULT NULL,
  `SprinkleredInd` varchar(255) DEFAULT NULL,
  `RestaurantInd` varchar(255) DEFAULT NULL,
  `VehicleTypeCd` varchar(255) DEFAULT NULL,
  `PremisesPremiumAmt` decimal(28,6) DEFAULT NULL,
  `Vehicles` int(11) DEFAULT NULL,
  `ProductsPremiumAmt` decimal(28,6) DEFAULT NULL,
  `LiquorPercentCd` varchar(255) DEFAULT NULL,
  `ReceiptsAmt` decimal(28,6) DEFAULT NULL,
  `IncludesLiquorLawInd` varchar(255) DEFAULT NULL,
  `HazardGroupCode` varchar(255) DEFAULT NULL,
  `BusinessCategory` varchar(255) DEFAULT NULL,
  `BusinessClass` varchar(255) DEFAULT NULL,
  `GLClassCode` varchar(255) DEFAULT NULL,
  `ExposureBasis` varchar(255) DEFAULT NULL,
  `Exposure` varchar(255) DEFAULT NULL,
  `RiskRef` varchar(255) DEFAULT NULL,
  `AuditableInd` varchar(255) DEFAULT NULL,
  `ClassCode` varchar(255) DEFAULT NULL,
  `RateTypeCd` varchar(255) DEFAULT NULL,
  `ManualRate` decimal(28,6) DEFAULT NULL,
  `FullTermAmt` decimal(28,6) DEFAULT NULL,
  `RateExposureBasis` decimal(28,6) DEFAULT NULL,
  `OtherClassCode` varchar(255) DEFAULT NULL,
  `OtherClassCodeDesc` varchar(255) DEFAULT NULL,
  `LiabilityLimits` varchar(255) DEFAULT NULL,
  `Deductible` int(11) DEFAULT NULL,
  `TrustPlanType` varchar(255) DEFAULT NULL,
  `TrustPlanTypeOtherDesc` varchar(255) DEFAULT NULL,
  `TrustPlanEstablishedDt` varchar(255) DEFAULT NULL,
  `BenefitsSecuredInsurancePct` int(11) DEFAULT NULL,
  `BenefitsSecuredSelfInsPct` int(11) DEFAULT NULL,
  `BenefitsSecuredInsuranceDesc` varchar(255) DEFAULT NULL,
  `NumberOfTrustees` int(11) DEFAULT NULL,
  `NumberOfEmployees` int(11) DEFAULT NULL,
  `ServiceProviderChangesDesc` varchar(255) DEFAULT NULL,
  `ScheduleRatingPremiumMod` int(11) DEFAULT NULL,
  `ExpenseFactorsPremiumMod` int(11) DEFAULT NULL,
  `CovIRSComplianceInd` varchar(255) DEFAULT NULL,
  `CovLEDDSubLimit` int(11) DEFAULT NULL,
  `CovBroadFormInd` varchar(255) DEFAULT NULL,
  `CovHIPAASubLimit` int(11) DEFAULT NULL,
  `ForeignChargePct` int(11) DEFAULT NULL,
  `AlcDrugFreeCreditAmt` decimal(28,6) DEFAULT NULL,
  `ClassUSLHWFlag` varchar(255) DEFAULT NULL,
  `ContrClassPremAdjCreditFactor` varchar(255) DEFAULT NULL,
  `ContrClassPremAdjCreditAmt` decimal(28,6) DEFAULT NULL,
  `CraftDescription` varchar(255) DEFAULT NULL,
  `CraftNumberOfSeats` int(11) DEFAULT NULL,
  `CraftRateAmt` decimal(28,6) DEFAULT NULL,
  `CraftSurchargeAmt` decimal(28,6) DEFAULT NULL,
  `CraftType` varchar(255) DEFAULT NULL,
  `EmployersLiabilityLimitFactor` decimal(28,6) DEFAULT NULL,
  `EmployersLiabilityLimitMinPremAmt` decimal(28,6) DEFAULT NULL,
  `ExposureAuditedAmt` varchar(255) DEFAULT NULL,
  `Extension` varchar(255) DEFAULT NULL,
  `FederalActCd` varchar(255) DEFAULT NULL,
  `FederalActExposureAmt` decimal(28,6) DEFAULT NULL,
  `IfAnyInd` varchar(255) DEFAULT NULL,
  `IsUSLHProgramFlag` varchar(255) DEFAULT NULL,
  `ManagedCareCreditAmt` decimal(28,6) DEFAULT NULL,
  `ManualARate` decimal(28,6) DEFAULT NULL,
  `ManualMigrantSeasonalAgriculturalRate` decimal(28,6) DEFAULT NULL,
  `GoverningClassInd` varchar(255) DEFAULT NULL,
  `RiskIndex` int(11) DEFAULT NULL,
  `Suffix` varchar(255) DEFAULT NULL,
  `SupplDisLoadAdjust` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjustSign` varchar(255) DEFAULT NULL,
  `TotalEmployersLiabilityMinPremAmt` decimal(28,6) DEFAULT NULL,
  `USLHWOfPayrollAmt` decimal(28,6) DEFAULT NULL,
  `USLHWOfPayrollExposureAmt` decimal(28,6) DEFAULT NULL,
  `USLHWOfPayrollFactor` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationDesc` varchar(255) DEFAULT NULL,
  `WaiverSubrogationInd` varchar(255) DEFAULT NULL,
  `WaiverSubrogationExposureAmt` varchar(255) DEFAULT NULL,
  `OwnerOfficerRemunerationPayrollAmt` varchar(255) DEFAULT NULL,
  `WorkersCompensationClassCd` varchar(255) DEFAULT NULL,
  `NumberOfPartTimeEmployees` varchar(255) DEFAULT NULL,
  `NumberOfFullTimeEmployees` varchar(255) DEFAULT NULL,
  `BeforeAuditExposure` decimal(28,6) DEFAULT NULL,
  `BeforeAuditManualRate` decimal(28,6) DEFAULT NULL,
  `ReminderMaxPayroll` varchar(255) DEFAULT NULL,
  KEY `glclassIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `historylocationclass`
--

DROP TABLE IF EXISTS `historylocationclass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historylocationclass` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StatusCx` varchar(255) DEFAULT NULL,
  `AnnualExposureAmt` decimal(28,6) DEFAULT NULL,
  `ClassificationCd` varchar(255) DEFAULT NULL,
  `AnnualExposureSubjectToFederalActsAmt` decimal(28,6) DEFAULT NULL,
  `ExposureAmt` decimal(28,6) DEFAULT NULL,
  `AnnualExposureSubjectToUSLHWActAmt` decimal(28,6) DEFAULT NULL,
  `AnnualExposureSubjectToWaiverOfSubrogationAmt` decimal(28,6) DEFAULT NULL,
  `AnnualOwnerOfficerRemunerationPayrollAmt` decimal(28,6) DEFAULT NULL,
  `ExposureSubjectToFederalActsAmt` decimal(28,6) DEFAULT NULL,
  `ExposureBasis` varchar(255) DEFAULT NULL,
  `ExposureSubjectToUSLHWActAmt` decimal(28,6) DEFAULT NULL,
  `ExposureSubjectToWaiverOfSubrogationAmt` decimal(28,6) DEFAULT NULL,
  `FederalAct` varchar(255) DEFAULT NULL,
  `ManualMigrantSeasonalAgriculturalRate` decimal(28,6) DEFAULT NULL,
  `IfAnyInd` varchar(255) DEFAULT NULL,
  `LocationClassIdRef` varchar(255) DEFAULT NULL,
  `RiskIdRef` varchar(255) DEFAULT NULL,
  `SupplementalDiseaseLoadingAdjustAmt` decimal(28,6) DEFAULT NULL,
  `SupplementalDiseaseLoadingAdjustSign` varchar(255) DEFAULT NULL,
  `WaiverSubrogationInd` varchar(255) DEFAULT NULL,
  KEY `historylocationclassIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `historyperiod`
--

DROP TABLE IF EXISTS `historyperiod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historyperiod` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ARDPeriodInd` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `StatusCx` varchar(255) DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `NumDays` int(11) DEFAULT NULL,
  `ProRateFactor` decimal(28,6) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `PriorityBalanceMinPrem` int(11) DEFAULT NULL,
  `PriorityWOSAdj` int(11) DEFAULT NULL,
  `PriorityELAdj` int(11) DEFAULT NULL,
  `PriorityAdmiraltyAdj` int(11) DEFAULT NULL,
  `ClassCodeMinimumPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AdmiraltyFELAIncrLiabilityLimitMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `EmployersLiabilityIncrLimitMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WaiverOfSubrogationMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AdmiraltyFELALiabilityLimits` varchar(255) DEFAULT NULL,
  `ReasonCd` varchar(255) DEFAULT NULL,
  `AlcoholDrugFreeCreditInd` varchar(255) DEFAULT NULL,
  `ContrClassPremAdjCreditPct` decimal(28,6) DEFAULT NULL,
  `WorkfareProgramWeeksNum` varchar(255) DEFAULT NULL,
  `LargeDeductibleFactor` decimal(28,6) DEFAULT NULL,
  `CoinsuranceInd` varchar(255) DEFAULT NULL,
  `DeductibleAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleType` varchar(255) DEFAULT NULL,
  `ELVoluntaryCompensationInd` varchar(255) DEFAULT NULL,
  `EmployersLiabilityLimits` varchar(255) DEFAULT NULL,
  `ExperienceModificationFactor` decimal(28,6) DEFAULT NULL,
  `ExperienceRatingCd` varchar(255) DEFAULT NULL,
  `ExperienceModificationStatus` varchar(255) DEFAULT NULL,
  `FederalActsLiabilityLimits` varchar(255) DEFAULT NULL,
  `ManagedCareCreditInd` varchar(255) DEFAULT NULL,
  `NumberOfClaims` decimal(28,6) DEFAULT NULL,
  `PremiumDiscountFactor` decimal(28,6) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `CertifiedSafetyHealthProgramPct` decimal(28,6) DEFAULT NULL,
  `GLScheduledPremiumMod` decimal(28,6) DEFAULT NULL,
  `StatePeriodIdRef` varchar(255) DEFAULT NULL,
  `TotalStandardPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationInd` varchar(255) DEFAULT NULL,
  KEY `historyperiodIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `historyperiodaircraft`
--

DROP TABLE IF EXISTS `historyperiodaircraft`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historyperiodaircraft` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StatusCx` varchar(255) DEFAULT NULL,
  `NumSeats` int(11) DEFAULT NULL,
  `PeriodAircraftIdRef` varchar(255) DEFAULT NULL,
  `RiskIdRef` varchar(255) DEFAULT NULL,
  KEY `historyperiodaircraftIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `historyperiodlocation`
--

DROP TABLE IF EXISTS `historyperiodlocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historyperiodlocation` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StatusCx` varchar(255) DEFAULT NULL,
  `LocationIdRef` varchar(255) DEFAULT NULL,
  `LocationNumber` int(11) DEFAULT NULL,
  `PeriodLocationIdRef` varchar(255) DEFAULT NULL,
  KEY `historyperiodlocationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `holdbackallocation`
--

DROP TABLE IF EXISTS `holdbackallocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `holdbackallocation` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `HoldbackAmt` varchar(255) DEFAULT NULL,
  `HoldbackComment` varchar(255) DEFAULT NULL,
  `FeatureCd` varchar(255) DEFAULT NULL,
  `FeatureSubCd` varchar(255) DEFAULT NULL,
  `ItemNumber` varchar(255) DEFAULT NULL,
  `HoldbackBalance` varchar(255) DEFAULT NULL,
  `PropertyDamagedIdRef` varchar(255) DEFAULT NULL,
  `PayInFullInd` varchar(255) DEFAULT NULL,
  `PaymentAmt` varchar(255) DEFAULT NULL,
  KEY `holdbackallocationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `homevaluecov`
--

DROP TABLE IF EXISTS `homevaluecov`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homevaluecov` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CoverageId` varchar(255) DEFAULT NULL,
  `EstimatedValueAmt` decimal(28,6) DEFAULT NULL,
  `LowAmt` decimal(28,6) DEFAULT NULL,
  `HighAmt` decimal(28,6) DEFAULT NULL,
  KEY `homevaluecovIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `homevaluecovpart`
--

DROP TABLE IF EXISTS `homevaluecovpart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homevaluecovpart` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `CostPerSqFt` decimal(28,6) DEFAULT NULL,
  `EstimatedValueAmt` decimal(28,6) DEFAULT NULL,
  `Percent` decimal(28,6) DEFAULT NULL,
  KEY `homevaluecovpartIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `homevalueproduct`
--

DROP TABLE IF EXISTS `homevalueproduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homevalueproduct` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  KEY `homevalueproductIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `homevaluereport`
--

DROP TABLE IF EXISTS `homevaluereport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homevaluereport` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `RequestId` varchar(255) DEFAULT NULL,
  `ReportXml` varchar(255) DEFAULT NULL,
  `LocGuid` varchar(255) DEFAULT NULL,
  `Agent` varchar(255) DEFAULT NULL,
  `QuoteNumberId` varchar(255) DEFAULT NULL,
  `UnderwriterId` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `ReferenceNumber` varchar(255) DEFAULT NULL,
  KEY `homevaluereportIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `homevaluereportrequest`
--

DROP TABLE IF EXISTS `homevaluereportrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homevaluereportrequest` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `RequestId` varchar(255) DEFAULT NULL,
  `ApplicationIdRef` varchar(255) DEFAULT NULL,
  KEY `homevaluereportrequestIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inboxbuddyuser`
--

DROP TABLE IF EXISTS `inboxbuddyuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inboxbuddyuser` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `BuddyUserId` varchar(255) DEFAULT NULL,
  KEY `inboxbuddyuserIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inboxview`
--

DROP TABLE IF EXISTS `inboxview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inboxview` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `LoginId` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `FilterStatus` varchar(255) DEFAULT NULL,
  `FilterWorkDate` varchar(255) DEFAULT NULL,
  `FilterWorkDateFromDays` int(11) DEFAULT NULL,
  `FilterWorkDateToDays` int(11) DEFAULT NULL,
  `FilterCompletedDate` varchar(255) DEFAULT NULL,
  `FilterCompletedDateFromDays` int(11) DEFAULT NULL,
  `FilterCompletedDateToDays` int(11) DEFAULT NULL,
  `FilterByOwnerInd` varchar(255) DEFAULT NULL,
  `FilterByTemplateInd` varchar(255) DEFAULT NULL,
  `FilterByCategoryInd` varchar(255) DEFAULT NULL,
  `FilterBySourceInd` varchar(255) DEFAULT NULL,
  `FilterByOwner` varchar(255) DEFAULT NULL,
  `FilterBySubordinateInd` varchar(255) DEFAULT NULL,
  `FilterBySubordinate` varchar(255) DEFAULT NULL,
  `FilterByTemplate` varchar(255) DEFAULT NULL,
  `FilterByCategory` varchar(255) DEFAULT NULL,
  `FilterBySource` varchar(255) DEFAULT NULL,
  `FilterEnteredBy` varchar(255) DEFAULT NULL,
  `FilterDescription` varchar(255) DEFAULT NULL,
  `ShowHiddenTasksInd` varchar(255) DEFAULT NULL,
  `DisplayChosen` varchar(255) DEFAULT NULL,
  `DisplayOrder` varchar(255) DEFAULT NULL,
  `DisplayGroupByInd` varchar(255) DEFAULT NULL,
  `DisplayGroupDefault` varchar(255) DEFAULT NULL,
  `Roles` varchar(255) DEFAULT NULL,
  KEY `inboxviewIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inboxviewfilter`
--

DROP TABLE IF EXISTS `inboxviewfilter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inboxviewfilter` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `FilterName` varchar(255) DEFAULT NULL,
  `FilterValue` varchar(255) DEFAULT NULL,
  KEY `inboxviewfilterIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inforce`
--

DROP TABLE IF EXISTS `inforce`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inforce` (
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `ProductName` varchar(255) DEFAULT NULL,
  `ProducerCd` varchar(255) DEFAULT NULL,
  `ReportPeriod` char(6) DEFAULT NULL,
  `BasePeriod` char(6) DEFAULT NULL,
  `CompPeriod` char(6) DEFAULT NULL,
  `NewPolicyCNT` decimal(10,0) DEFAULT NULL,
  `RenewalPolicyCNT` decimal(10,0) DEFAULT NULL,
  `LapsePolicyCNT` decimal(10,0) DEFAULT NULL,
  `CancelledPolicyCNT` decimal(10,0) DEFAULT NULL,
  `BaseIFPoliciesCNT` decimal(10,0) DEFAULT NULL,
  `BasePremiumIFAMT` decimal(28,6) DEFAULT NULL,
  `BaseRenewablePIFCNT` decimal(10,0) DEFAULT NULL,
  `BaseRenewableIFAMT` decimal(28,6) DEFAULT NULL,
  `BaseRenewedPIFCNT` decimal(10,0) DEFAULT NULL,
  `BaseRenewedIFAMT` decimal(28,6) DEFAULT NULL,
  `CompIFPoliciesCNT` decimal(10,0) DEFAULT NULL,
  `CompPremiumIFAMT` decimal(28,6) DEFAULT NULL,
  `CompRenewableIFCNT` decimal(10,0) DEFAULT NULL,
  `CompRenewableIFPAMT` decimal(28,6) DEFAULT NULL,
  `BaseUnitCount` decimal(20,0) DEFAULT NULL,
  `CompUnitCount` decimal(20,0) DEFAULT NULL,
  `ChangeinPoliciesIF` decimal(10,0) DEFAULT NULL,
  `ChangeinPremiumIF` decimal(10,0) DEFAULT NULL,
  `PercentChangeinPIF` decimal(28,6) DEFAULT NULL,
  `PercentChangeinIFP` decimal(28,6) DEFAULT NULL,
  `GrossPolicyRetention` decimal(28,6) DEFAULT NULL,
  `GrossPremiumRetention` decimal(28,6) DEFAULT NULL,
  `NetPolicyRetention` decimal(28,6) DEFAULT NULL,
  `NetPremiumRetention` decimal(28,6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `injuredparty`
--

DROP TABLE IF EXISTS `injuredparty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `injuredparty` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `MedicareBeneficiaryCd` varchar(255) DEFAULT NULL,
  `HealthInsuranceClaimNumber` varchar(255) DEFAULT NULL,
  `InjuryCauseCd` varchar(255) DEFAULT NULL,
  `ProductLiabilityCd` varchar(255) DEFAULT NULL,
  `ProductGenericName` varchar(255) DEFAULT NULL,
  `ProductBrandName` varchar(255) DEFAULT NULL,
  `ProductManufacturer` varchar(255) DEFAULT NULL,
  `ProductAllegedHarmDesc` varchar(255) DEFAULT NULL,
  `ORMInd` varchar(255) DEFAULT NULL,
  `ORMTerminationDt` date DEFAULT NULL,
  `RepresentativeTypeCd` varchar(255) DEFAULT NULL,
  `StateOfVenue` varchar(255) DEFAULT NULL,
  `NoFaultInsuranceLimit` decimal(28,6) DEFAULT NULL,
  `ExhaustDt` date DEFAULT NULL,
  `DeleteFromCMS` varchar(255) DEFAULT NULL,
  `NotSendCovCMS` varchar(255) DEFAULT NULL,
  `InjuryCauseTypeCd` varchar(255) DEFAULT NULL,
  `CMSDt` date DEFAULT NULL,
  KEY `injuredpartyIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `installment`
--

DROP TABLE IF EXISTS `installment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `installment` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `DueDt` date DEFAULT NULL,
  `DueAmt` varchar(255) DEFAULT NULL,
  `PostedRecoveryAmt` varchar(255) DEFAULT NULL,
  `LastPymtDate` date DEFAULT NULL,
  `TotalPastDueAmt` varchar(255) DEFAULT NULL,
  `NumberOfPendInstallments` varchar(255) DEFAULT NULL,
  `InstallmentNumber` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `RecoveryAmtDue` varchar(255) DEFAULT NULL,
  KEY `installmentIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `installments`
--

DROP TABLE IF EXISTS `installments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `installments` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `NumberOfInstallments` int(11) DEFAULT NULL,
  `OrigRecoveryAmt` int(11) DEFAULT NULL,
  `OpenDt` date DEFAULT NULL,
  `CreditBalance` varchar(255) DEFAULT NULL,
  KEY `installmentsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `insurancescore`
--

DROP TABLE IF EXISTS `insurancescore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `insurancescore` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `InsuranceScoreTypeCd` varchar(255) DEFAULT NULL,
  `InsuranceScore` varchar(255) DEFAULT NULL,
  `OverriddenInsuranceScore` varchar(255) DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `SourceIdRef` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  KEY `insurancescoreIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `insurancescorereason`
--

DROP TABLE IF EXISTS `insurancescorereason`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `insurancescorereason` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ReasonCd` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  KEY `insurancescorereasonIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `insured`
--

DROP TABLE IF EXISTS `insured`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `insured` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `IndexName` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `EntityTypeCd` varchar(255) DEFAULT NULL,
  `PreferredDeliveryMethod` varchar(255) DEFAULT NULL,
  `NAICS` varchar(255) DEFAULT NULL,
  `NCCIRiskIdNumber` varchar(255) DEFAULT NULL,
  `RatingBureauID` varchar(255) DEFAULT NULL,
  `SelfInsured` varchar(255) DEFAULT NULL,
  `SIC` varchar(255) DEFAULT NULL,
  `WebsiteAddress` varchar(255) DEFAULT NULL,
  `YearsInBusiness` int(11) DEFAULT NULL,
  KEY `insuredIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `isoclaimsearchreportrq`
--

DROP TABLE IF EXISTS `isoclaimsearchreportrq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `isoclaimsearchreportrq` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ClaimIdRef` varchar(255) DEFAULT NULL,
  KEY `isoclaimsearchreportrqIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `iteminfo`
--

DROP TABLE IF EXISTS `iteminfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iteminfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ItemNumber` varchar(255) DEFAULT NULL,
  `ReferenceNumber` varchar(255) DEFAULT NULL,
  `ReferenceName` varchar(255) DEFAULT NULL,
  `TotalOpenAmt` decimal(28,6) DEFAULT NULL,
  `GrossPremiumAmt` decimal(28,6) DEFAULT NULL,
  `TotalAmt` decimal(28,6) DEFAULT NULL,
  `AppliedAmt` decimal(28,6) DEFAULT NULL,
  `CommissionAmt` decimal(28,6) DEFAULT NULL,
  `NetPremiumAmt` decimal(28,6) DEFAULT NULL,
  `NonPremiumAmt` decimal(28,6) DEFAULT NULL,
  `BlankRecordInd` varchar(255) DEFAULT NULL,
  `GrossNetInd` varchar(255) DEFAULT NULL,
  `EffectiveDt` varchar(255) DEFAULT NULL,
  `StatementOpenAmount` decimal(28,6) DEFAULT NULL,
  KEY `iteminfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `job`
--

DROP TABLE IF EXISTS `job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `JobTemplateIdRef` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `StartTm` varchar(255) DEFAULT NULL,
  `StartUserId` varchar(255) DEFAULT NULL,
  `LogFilename` varchar(255) DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `EndTm` varchar(255) DEFAULT NULL,
  `CancelDt` date DEFAULT NULL,
  `CancelTm` varchar(255) DEFAULT NULL,
  `CancelUserId` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  KEY `jobIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jobaction`
--

DROP TABLE IF EXISTS `jobaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobaction` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `JobActionTemplateIdRef` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `StartTm` varchar(255) DEFAULT NULL,
  `EndTm` varchar(255) DEFAULT NULL,
  `LastActivityDt` date DEFAULT NULL,
  `LastActivityTm` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `SkipInd` varchar(255) DEFAULT NULL,
  KEY `jobactionIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jobactions`
--

DROP TABLE IF EXISTS `jobactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobactions` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `jobactionsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `licensedproduct`
--

DROP TABLE IF EXISTS `licensedproduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licensedproduct` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ProductLookupKey` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `LicenseClassCd` varchar(255) DEFAULT NULL,
  `CarrierGroup` varchar(255) DEFAULT NULL,
  `StateProvCd` varchar(255) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `CommissionNewPct` decimal(28,6) DEFAULT NULL,
  `CommissionRenewalPct` decimal(28,6) DEFAULT NULL,
  `CommissionOtherPct` decimal(28,6) DEFAULT NULL,
  `NewExpirationDt` date DEFAULT NULL,
  `RenewalExpirationDt` date DEFAULT NULL,
  `SubmitTo` varchar(255) DEFAULT NULL,
  `SubmitToCd` varchar(255) DEFAULT NULL,
  `CommissionPayInd` varchar(255) DEFAULT NULL,
  `CommissionPayRule` varchar(255) DEFAULT NULL,
  KEY `licensedproductIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `licensedproducts`
--

DROP TABLE IF EXISTS `licensedproducts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licensedproducts` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `licensedproductsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `limit`
--

DROP TABLE IF EXISTS `limit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `limit` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `LimitCd` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  KEY `limitIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `line`
--

DROP TABLE IF EXISTS `line`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `line` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `LineCd` varchar(255) DEFAULT NULL,
  `FullTermAmt` decimal(28,6) DEFAULT NULL,
  `RatingService` varchar(255) DEFAULT NULL,
  `WrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `EarnedAmt` decimal(28,6) DEFAULT NULL,
  `CommissionAmt` decimal(28,6) DEFAULT NULL,
  `FinalPremiumAmt` decimal(28,6) DEFAULT NULL,
  `TransactionCommissionAmt` decimal(28,6) DEFAULT NULL,
  `CoverageTriggerCd` varchar(255) DEFAULT NULL,
  `RetroactiveDt` date DEFAULT NULL,
  `ContinuityDt` date DEFAULT NULL,
  `ExtendedReportingDt` date DEFAULT NULL,
  `MultiPolicyDiscountInd` varchar(255) DEFAULT NULL,
  `RelatedPolicyNumber` varchar(255) DEFAULT NULL,
  `WaivePolicyFeeInd` varchar(255) DEFAULT NULL,
  `MultiCarDiscountInd` varchar(255) DEFAULT NULL,
  `UMPDWCDInd` varchar(255) DEFAULT NULL,
  `EmployeeDiscountInd` varchar(255) DEFAULT NULL,
  `FormType` varchar(255) DEFAULT NULL,
  `GLOccurrenceLimit` varchar(255) DEFAULT NULL,
  `GLAggregateLimit` varchar(255) DEFAULT NULL,
  `FireLegalLiabilityLimit` varchar(255) DEFAULT NULL,
  `MedicalExpensesLimit` varchar(255) DEFAULT NULL,
  `AggregateLimitPerLocationInd` varchar(255) DEFAULT NULL,
  `GLScheduledPremiumMod` varchar(255) DEFAULT NULL,
  `ProductsAggregateLimit` varchar(255) DEFAULT NULL,
  `RetainedLimit` varchar(255) DEFAULT NULL,
  `FirstDollarDefenseInd` varchar(255) DEFAULT NULL,
  `PersonalInjuryLimit` varchar(255) DEFAULT NULL,
  `ProductsAndCompletedOpsAggLim` varchar(255) DEFAULT NULL,
  `EmployeeBenefitsLimit` varchar(255) DEFAULT NULL,
  `ClaimsMadeInd` varchar(255) DEFAULT NULL,
  `GLDeductible` varchar(255) DEFAULT NULL,
  `GLOccurrenceAggregateLimit` varchar(255) DEFAULT NULL,
  `GLPropDamageLiabDeductible` int(11) DEFAULT NULL,
  `GLDeductibleType` varchar(255) DEFAULT NULL,
  `LimitType` varchar(255) DEFAULT NULL,
  `BILimitSplit` varchar(255) DEFAULT NULL,
  `PDLimitSplit` varchar(255) DEFAULT NULL,
  `LiabilityLimitCSL` varchar(255) DEFAULT NULL,
  `LiabilityDed` varchar(255) DEFAULT NULL,
  `LiabilityVehicleDesignationCd` varchar(255) DEFAULT NULL,
  `LiabilityPremiumMod` int(11) DEFAULT NULL,
  `MedPayLimit` varchar(255) DEFAULT NULL,
  `MedPayVehicleDesignationCd` varchar(255) DEFAULT NULL,
  `UMBILimitSplit` varchar(255) DEFAULT NULL,
  `UMLimitCSL` varchar(255) DEFAULT NULL,
  `UMBIVehicleDesignationCd` varchar(255) DEFAULT NULL,
  `ComprehensiveDed` varchar(255) DEFAULT NULL,
  `ComprehensiveVehDesignationCd` varchar(255) DEFAULT NULL,
  `CollisionDed` varchar(255) DEFAULT NULL,
  `CollisionVehicleDesignationCd` varchar(255) DEFAULT NULL,
  `PhysicalDamagePremiumMod` int(11) DEFAULT NULL,
  `FleetFactor` varchar(255) DEFAULT NULL,
  `UMPDLimitCSL` varchar(255) DEFAULT NULL,
  `NumberEmployeesFullTime` int(11) DEFAULT NULL,
  `NumberEmployeesPartTime` int(11) DEFAULT NULL,
  `NumberEmployeesTempWorkers` int(11) DEFAULT NULL,
  `NumberEmployeesLeased` int(11) DEFAULT NULL,
  `NumberEmployeesSeasonalWorkers` int(11) DEFAULT NULL,
  `NumberIndependentContractors` int(11) DEFAULT NULL,
  `NumberVolunteers` int(11) DEFAULT NULL,
  `NumberLawClerks` int(11) DEFAULT NULL,
  `NumberClerical` int(11) DEFAULT NULL,
  `NumberParaLegals` int(11) DEFAULT NULL,
  `NumberLawyers` int(11) DEFAULT NULL,
  `NumberNurses` int(11) DEFAULT NULL,
  `NumberPhysicians` int(11) DEFAULT NULL,
  `NumberNurseAssistants` int(11) DEFAULT NULL,
  `PracticeName` varchar(255) DEFAULT NULL,
  `DefenseIncludedInRetainedInd` varchar(255) DEFAULT NULL,
  `CovELimit` varchar(255) DEFAULT NULL,
  `CovFLimit` varchar(255) DEFAULT NULL,
  `BILimit` varchar(255) DEFAULT NULL,
  `PDLimit` varchar(255) DEFAULT NULL,
  `UMBILimit` varchar(255) DEFAULT NULL,
  `AdmiraltyFELALiabilityLimits` varchar(255) DEFAULT NULL,
  `AdmiraltyFELAMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AlcoholDrugFreeCreditInd` varchar(255) DEFAULT NULL,
  `AlcDrugFreeCreditAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheRate` decimal(28,6) DEFAULT NULL,
  `ContrClassPremAdjCreditPct` decimal(28,6) DEFAULT NULL,
  `ContrClassPremAdjCreditFactor` varchar(255) DEFAULT NULL,
  `ContrClassPremAdjCreditAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleDiscountRatePct` decimal(28,6) DEFAULT NULL,
  `DeductibleType` varchar(255) DEFAULT NULL,
  `DiseaseAbrasiveAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAbrasivePaDeAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAsbestosAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseAtomicEnergyAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseCoalmineAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseExperienceAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundryIronAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundryNoFerrAmt` decimal(28,6) DEFAULT NULL,
  `DiseaseFoundrySteelAmt` decimal(28,6) DEFAULT NULL,
  `ELIncreaseLimitMinPremAmt` decimal(28,6) DEFAULT NULL,
  `ELIncreaseLimitMinPremChrgAmt` decimal(28,6) DEFAULT NULL,
  `ELVoluntaryCompensationInd` varchar(255) DEFAULT NULL,
  `EstimatedAnnualPremiumAmt` decimal(28,6) DEFAULT NULL,
  `ExpenseConstantAmt` decimal(28,6) DEFAULT NULL,
  `ExperienceMeritRating` varchar(255) DEFAULT NULL,
  `ExperienceModificationFactor` varchar(255) DEFAULT NULL,
  `FederalActsLiabilityLimits` varchar(255) DEFAULT NULL,
  `EmployersLiabilityLimits` varchar(255) DEFAULT NULL,
  `ManagedCareCreditInd` varchar(255) DEFAULT NULL,
  `ManagedCareCreditAmt` decimal(28,6) DEFAULT NULL,
  `MinimumPremiumAmt` decimal(28,6) DEFAULT NULL,
  `NumberOfClaims` varchar(255) DEFAULT NULL,
  `PremiumDiscountAmt` decimal(28,6) DEFAULT NULL,
  `CertifiedSafetyHealthProgramPct` decimal(28,6) DEFAULT NULL,
  `StandardPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjust` decimal(28,6) DEFAULT NULL,
  `SupplDisLoadAdjustSign` varchar(255) DEFAULT NULL,
  `TerrorismAmt` decimal(28,6) DEFAULT NULL,
  `TerrorismRate` decimal(28,6) DEFAULT NULL,
  `TotalManualPremimAmt` decimal(28,6) DEFAULT NULL,
  `TotalSubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `USLHProgramFlag` varchar(255) DEFAULT NULL,
  `WaiverSubrogationAmt` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationInd` varchar(255) DEFAULT NULL,
  `WaiverSubrogationRate` decimal(28,6) DEFAULT NULL,
  `OtherStatesItem3C` varchar(255) DEFAULT NULL,
  KEY `lineIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `linedata`
--

DROP TABLE IF EXISTS `linedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linedata` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `LineDataCd` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  KEY `linedataIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `linehistory`
--

DROP TABLE IF EXISTS `linehistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linehistory` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `linehistoryIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `linehistoryitem`
--

DROP TABLE IF EXISTS `linehistoryitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linehistoryitem` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `BeforeTransactionNumber` int(11) DEFAULT NULL,
  `EmployersLiabilityLimits` varchar(255) DEFAULT NULL,
  `AdmiraltyFELALiabilityLimits` varchar(255) DEFAULT NULL,
  `WaiverSubrogationInd` varchar(255) DEFAULT NULL,
  `AlcoholDrugFreeCreditInd` varchar(255) DEFAULT NULL,
  `FederalActsLiabilityLimits` varchar(255) DEFAULT NULL,
  `ExperienceModificationFactor` varchar(255) DEFAULT NULL,
  `ManagedCareCreditInd` varchar(255) DEFAULT NULL,
  `ELVoluntaryCompensationInd` varchar(255) DEFAULT NULL,
  KEY `linehistoryitemIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `linkreference`
--

DROP TABLE IF EXISTS `linkreference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linkreference` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `SystemIdRef` varchar(255) DEFAULT NULL,
  `IdRef` varchar(255) DEFAULT NULL,
  `ModelName` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `DescriptionRef` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `PrevStatus` varchar(255) DEFAULT NULL,
  `FutureStatusCd` varchar(255) DEFAULT NULL,
  KEY `linkreferenceIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `litigation`
--

DROP TABLE IF EXISTS `litigation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `litigation` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `SuitFiledInd` varchar(255) DEFAULT NULL,
  `SuitDt` date DEFAULT NULL,
  `Caption` varchar(255) DEFAULT NULL,
  `CaseFacts` varchar(255) DEFAULT NULL,
  `CaseAnalysis` varchar(255) DEFAULT NULL,
  `SuitStatusCd` varchar(255) DEFAULT NULL,
  `SuitSettlementCd` varchar(255) DEFAULT NULL,
  `ClaimSettledDt` date DEFAULT NULL,
  `DocketNumber` varchar(255) DEFAULT NULL,
  `SuitClosedDt` date DEFAULT NULL,
  `EmployeeRepresentationDt` date DEFAULT NULL,
  KEY `litigationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `litigationassociate`
--

DROP TABLE IF EXISTS `litigationassociate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `litigationassociate` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AssociateNumber` int(11) DEFAULT NULL,
  `AssociateTypeCd` varchar(255) DEFAULT NULL,
  `AssociateProviderRef` int(11) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `PreferredDeliveryMethod` varchar(255) DEFAULT NULL,
  KEY `litigationassociateIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `LocationTypeCd` varchar(255) DEFAULT NULL,
  `LocationNumber` int(11) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `LocationDesc` varchar(255) DEFAULT NULL,
  `FutureStatusCd` varchar(255) DEFAULT NULL,
  `PrimaryInd` varchar(255) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `CoveredStateIdRef` varchar(255) DEFAULT NULL,
  `ProtectionClass` varchar(255) DEFAULT NULL,
  `CrimeTerritoryCd` varchar(255) DEFAULT NULL,
  `TerritoryCd` varchar(255) DEFAULT NULL,
  `FireTerritoryCd` varchar(255) DEFAULT NULL,
  `InlandMarineTerritoryCd` varchar(255) DEFAULT NULL,
  `LiabilityTerritoryCd` varchar(255) DEFAULT NULL,
  `EQClass` varchar(255) DEFAULT NULL,
  `EQZone` varchar(255) DEFAULT NULL,
  `EQSubZone` varchar(255) DEFAULT NULL,
  `EQTerritoryCd` varchar(255) DEFAULT NULL,
  `PropertyTerritoryIICd` varchar(255) DEFAULT NULL,
  `BusinessAutoTerritoryCd` varchar(255) DEFAULT NULL,
  `UnderlyingCarrierName` varchar(255) DEFAULT NULL,
  `UnderlyingPolicyTypeCd` varchar(255) DEFAULT NULL,
  `UnderlyingPolicyNumber` varchar(255) DEFAULT NULL,
  `UnderlyingPolicyEffectiveDt` date DEFAULT NULL,
  `UnderlyingPolicyExpirationDt` date DEFAULT NULL,
  `UnderlyingCSLOccurrenceLimit` varchar(255) DEFAULT NULL,
  `UnderlyingBIOccurrenceLimit` varchar(255) DEFAULT NULL,
  `UnderlyingBIPerPersonLimit` varchar(255) DEFAULT NULL,
  `UnderlyingPDOccurrenceLimit` varchar(255) DEFAULT NULL,
  `UnderlyingGLPolicyTypeCd` varchar(255) DEFAULT NULL,
  `UnderlyingGLOccurrenceLimit` varchar(255) DEFAULT NULL,
  `UnderlyingGLAggregateLimit` varchar(255) DEFAULT NULL,
  `UnderlyingProductAggLimit` varchar(255) DEFAULT NULL,
  `UnderlyingDamageToRentedLimit` varchar(255) DEFAULT NULL,
  `UnderlyingMedicalExpenseLimit` varchar(255) DEFAULT NULL,
  `UnderlyingPersonalAdvInjLimit` varchar(255) DEFAULT NULL,
  `UnderlyingDiseasePerEmplLimit` varchar(255) DEFAULT NULL,
  `UnderlyingELOccurrenceLimit` varchar(255) DEFAULT NULL,
  `UnderlyingDiseasePolicyLimit` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `RemoveDt` date DEFAULT NULL,
  `BasicGroup1` varchar(255) DEFAULT NULL,
  `District` varchar(255) DEFAULT NULL,
  `BasicGroup2` varchar(255) DEFAULT NULL,
  `FireDepartment` varchar(255) DEFAULT NULL,
  `EQCauseOfLossForm` varchar(255) DEFAULT NULL,
  `SpecialCauseOfLoss` varchar(255) DEFAULT NULL,
  `FloodCoverageApply` varchar(255) DEFAULT NULL,
  `MultiplePropertiesCreditApply` varchar(255) DEFAULT NULL,
  `IncreaseFireDepartmentLimit` varchar(255) DEFAULT NULL,
  `MultipleBuildingsAsOneUnit` varchar(255) DEFAULT NULL,
  `IncreaseLimitForNewLocation` varchar(255) DEFAULT NULL,
  `BOPTerritoryCd` varchar(255) DEFAULT NULL,
  `Exposure` decimal(28,6) DEFAULT NULL,
  `ManualPremiumAmt` decimal(28,6) DEFAULT NULL,
  `NetManualPremiumAmt` decimal(28,6) DEFAULT NULL,
  `PrevNetManualPremiumAmt` decimal(28,6) DEFAULT NULL,
  KEY `locationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `locationclass`
--

DROP TABLE IF EXISTS `locationclass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locationclass` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StatusCx` varchar(255) DEFAULT NULL,
  `ClassificationCd` varchar(255) DEFAULT NULL,
  `AnnualExposureAmt` decimal(28,6) DEFAULT NULL,
  `ExposureAmt` decimal(28,6) DEFAULT NULL,
  `ActualExposureAmt` decimal(28,6) DEFAULT NULL,
  `AnnualExposureSubjectToFederalActsAmt` decimal(28,6) DEFAULT NULL,
  `ActualExposureSubjectToFederalActsAmt` decimal(28,6) DEFAULT NULL,
  `ExposureSubjectToFederalActsAmt` decimal(28,6) DEFAULT NULL,
  `AnnualExposureSubjectToUSLHWActAmt` decimal(28,6) DEFAULT NULL,
  `ActualExposureSubjectToUSLHWActAmt` decimal(28,6) DEFAULT NULL,
  `ExposureSubjectToUSLHWActAmt` decimal(28,6) DEFAULT NULL,
  `AnnualExposureSubjectToWaiverOfSubrogationAmt` decimal(28,6) DEFAULT NULL,
  `ActualExposureSubjectToWaiverOfSubrogationAmt` decimal(28,6) DEFAULT NULL,
  `ExposureSubjectToWaiverOfSubrogationAmt` decimal(28,6) DEFAULT NULL,
  `AnnualOwnerOfficerRemunerationPayrollAmt` decimal(28,6) DEFAULT NULL,
  `ActualOwnerOfficerRemunerationPayrollAmt` decimal(28,6) DEFAULT NULL,
  `OwnerOfficerRemunerationPayrollAmt` decimal(28,6) DEFAULT NULL,
  `GoverningClassInd` varchar(255) DEFAULT NULL,
  `AdmiraltyFELAIncrLiabilityLimitPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AdmiraltyFELAIncrLiabilityLimitProgramIFactor` decimal(28,6) DEFAULT NULL,
  `AdmiraltyFELAIncrLiabilityLimitProgramIIFactor` decimal(28,6) DEFAULT NULL,
  `AtomicEnergyRadiationFactor` decimal(28,6) DEFAULT NULL,
  `AtomicEnergyRadiationPremiumAmt` decimal(28,6) DEFAULT NULL,
  `ClassCodeMinimumPremiumAmt` decimal(28,6) DEFAULT NULL,
  `CoalMineDiseaseFactor` decimal(28,6) DEFAULT NULL,
  `CoalMineDiseasePremiumAmt` decimal(28,6) DEFAULT NULL,
  `ExposureBasis` varchar(255) DEFAULT NULL,
  `FederalAct` varchar(255) DEFAULT NULL,
  `FederalActFactor` decimal(28,6) DEFAULT NULL,
  `FederalActPremiumAmt` decimal(28,6) DEFAULT NULL,
  `ManualMigrantSeasonalAgriculturalRate` decimal(28,6) DEFAULT NULL,
  `IfAnyInd` varchar(255) DEFAULT NULL,
  `ManualPremiumAmt` decimal(28,6) DEFAULT NULL,
  `RateAmt` decimal(28,6) DEFAULT NULL,
  `RiskIdRef` varchar(255) DEFAULT NULL,
  `SpecificDiseaseFactor` decimal(28,6) DEFAULT NULL,
  `SpecificDiseasePremiumAmt` decimal(28,6) DEFAULT NULL,
  `SupplementalDiseaseLoadingAdjustAmt` decimal(28,6) DEFAULT NULL,
  `SupplementalDiseaseLoadingAdjustSign` varchar(255) DEFAULT NULL,
  `SupplementalDiseaseLoadingFactor` decimal(28,6) DEFAULT NULL,
  `SupplementalDiseaseLoadingPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SupplementaryDiseaseFactor` decimal(28,6) DEFAULT NULL,
  `SupplementaryDiseasePremiumAmt` decimal(28,6) DEFAULT NULL,
  `TotalManualPremiumAmt` decimal(28,6) DEFAULT NULL,
  `USLHWActFactor` decimal(28,6) DEFAULT NULL,
  `USLHWActPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WaiverOfSubrogationFactor` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationInd` varchar(255) DEFAULT NULL,
  `WaiverOfSubrogationPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AuditRiskInd` varchar(255) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  KEY `locationclassIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `locationdetail`
--

DROP TABLE IF EXISTS `locationdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locationdetail` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `DetailTypeCd` varchar(255) DEFAULT NULL,
  KEY `locationdetailIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `locationriskgroup`
--

DROP TABLE IF EXISTS `locationriskgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locationriskgroup` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `RiskGroupCd` varchar(255) DEFAULT NULL,
  `RiskGroupRef` varchar(255) DEFAULT NULL,
  KEY `locationriskgroupIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `logbean`
--

DROP TABLE IF EXISTS `logbean`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logbean` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Container` varchar(255) DEFAULT NULL,
  `ModelName` varchar(255) DEFAULT NULL,
  `IdRef` varchar(255) DEFAULT NULL,
  `QuickKey` varchar(255) DEFAULT NULL,
  KEY `logbeanIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `logentry`
--

DROP TABLE IF EXISTS `logentry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logentry` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `UserId` varchar(255) DEFAULT NULL,
  `Operation` varchar(255) DEFAULT NULL,
  `Container` varchar(255) DEFAULT NULL,
  `ContainerRef` varchar(255) DEFAULT NULL,
  `ModelName` varchar(255) DEFAULT NULL,
  `IdRef` varchar(255) DEFAULT NULL,
  `OperationDtTm` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `OperationDt` date DEFAULT NULL,
  `OperationTm` varchar(255) DEFAULT NULL,
  `TemplateIdRef` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Text` varchar(255) DEFAULT NULL,
  `InternalInd` varchar(255) DEFAULT NULL,
  `Type` varchar(255) DEFAULT NULL,
  `SubType` varchar(255) DEFAULT NULL,
  `Severity` varchar(255) DEFAULT NULL,
  KEY `logentryIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `logintoken`
--

DROP TABLE IF EXISTS `logintoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logintoken` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `LoginId` varchar(255) DEFAULT NULL,
  `LoginToken` varchar(255) DEFAULT NULL,
  `CustomerLoginRef` varchar(255) DEFAULT NULL,
  `LastUsedDt` varchar(255) DEFAULT NULL,
  `LastUsedTm` varchar(255) DEFAULT NULL,
  KEY `logintokenIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `logkey`
--

DROP TABLE IF EXISTS `logkey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logkey` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ModelName` varchar(255) DEFAULT NULL,
  `Key` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  KEY `logkeyIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `losscause`
--

DROP TABLE IF EXISTS `losscause`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `losscause` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `LossCauseCd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  KEY `losscauseIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `losshistory`
--

DROP TABLE IF EXISTS `losshistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `losshistory` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `LossHistoryNumber` int(11) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `IgnoreInd` varchar(255) DEFAULT NULL,
  `LossDt` date DEFAULT NULL,
  `LossCauseCd` varchar(255) DEFAULT NULL,
  `LossAmt` decimal(28,6) DEFAULT NULL,
  `LossDesc` varchar(255) DEFAULT NULL,
  `AtFaultCd` varchar(255) DEFAULT NULL,
  `CatastropheNumber` varchar(255) DEFAULT NULL,
  `ClaimNumber` varchar(255) DEFAULT NULL,
  `ClaimStatusCd` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  `CarrierName` varchar(255) DEFAULT NULL,
  `DriverName` varchar(255) DEFAULT NULL,
  `DriverLicensedStateProvCd` varchar(255) DEFAULT NULL,
  `DriverLicenseNumber` varchar(255) DEFAULT NULL,
  `VehIdentificationNumber` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `RiskIdRef` varchar(255) DEFAULT NULL,
  `DriverRef` varchar(255) DEFAULT NULL,
  `InjuryInd` varchar(255) DEFAULT NULL,
  `DirectPortalIncidentId` varchar(255) DEFAULT NULL,
  `FutureStatusCd` varchar(255) DEFAULT NULL,
  KEY `losshistoryIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lossnoticeinfo`
--

DROP TABLE IF EXISTS `lossnoticeinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lossnoticeinfo` (
  `UniqueId` int(11) NOT NULL AUTO_INCREMENT,
  `SystemId` int(11) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `PolicyRef` int(11) DEFAULT NULL,
  `LossNoticeNumber` varchar(255) DEFAULT NULL,
  `ClaimNumber` varchar(255) DEFAULT NULL,
  `ClaimRef` int(11) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `AtFaultCd` varchar(255) DEFAULT NULL,
  `AuthorityContacted` varchar(255) DEFAULT NULL,
  `BranchCd` varchar(255) DEFAULT NULL,
  `CatastropheRef` int(11) DEFAULT NULL,
  `CatastropheCd` varchar(255) DEFAULT NULL,
  `DamageDesc` varchar(255) DEFAULT NULL,
  `DamageEstimateAmt` decimal(28,2) DEFAULT NULL,
  `CurrentlyAssignedTo` varchar(255) DEFAULT NULL,
  `LossCauseCd` varchar(255) DEFAULT NULL,
  `SubLossCauseCd` varchar(255) DEFAULT NULL,
  `LossDt` date DEFAULT NULL,
  `LossTm` varchar(255) DEFAULT NULL,
  `LossLocationDesc` varchar(255) DEFAULT NULL,
  `ReportedBy` varchar(255) DEFAULT NULL,
  `ReportedDt` date DEFAULT NULL,
  `ReportedTm` varchar(255) DEFAULT NULL,
  `ReportedTo` varchar(255) DEFAULT NULL,
  `ShortDesc` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `StatusDt` date DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `AcknowledgedDt` date DEFAULT NULL,
  `AcknowledgedTm` varchar(255) DEFAULT NULL,
  `DamagedInd` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`UniqueId`),
  KEY `lossnoticeinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `managedgroup`
--

DROP TABLE IF EXISTS `managedgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `managedgroup` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ManagedGroupCd` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  KEY `managedgroupIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `maritimecovendinfo`
--

DROP TABLE IF EXISTS `maritimecovendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maritimecovendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `WorkDescription` varchar(255) DEFAULT NULL,
  `EmprLiabBIAAmt` int(11) DEFAULT NULL,
  `EmprLiabBIDAmt` int(11) DEFAULT NULL,
  `TWMCurePremAmt` int(11) DEFAULT NULL,
  `EndorsementSeqNo` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `maritimecovendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `matrixstate`
--

DROP TABLE IF EXISTS `matrixstate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixstate` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PrimaryInd` varchar(255) DEFAULT NULL,
  `StatusCx` varchar(255) DEFAULT NULL,
  `AnniversaryRatingDay` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `CoveredStateIdRef` varchar(255) DEFAULT NULL,
  `Index` int(11) DEFAULT NULL,
  `InitialStateEstimatedAnnualPremium` decimal(28,6) DEFAULT NULL,
  KEY `matrixstateIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `meta`
--

DROP TABLE IF EXISTS `meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  KEY `metaIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mod`
--

DROP TABLE IF EXISTS `mod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mod` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ModCd` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  KEY `modIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `moratorium`
--

DROP TABLE IF EXISTS `moratorium`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moratorium` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `MoratoriumCd` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `NewsBannerDetails` varchar(255) DEFAULT NULL,
  `NewsBannerInd` varchar(255) DEFAULT NULL,
  `NewsBannerRef` int(11) DEFAULT NULL,
  KEY `moratoriumIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `moratoriumlocation`
--

DROP TABLE IF EXISTS `moratoriumlocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moratoriumlocation` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateProvCd` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `Location` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  KEY `moratoriumlocationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `moratoriumproduct`
--

DROP TABLE IF EXISTS `moratoriumproduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moratoriumproduct` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ProductId` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  KEY `moratoriumproductIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `moratoriumtype`
--

DROP TABLE IF EXISTS `moratoriumtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moratoriumtype` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `MoratoriumTypeCd` varchar(255) DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `StartTm` varchar(255) DEFAULT NULL,
  `EndTm` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  KEY `moratoriumtypeIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nameinfo`
--

DROP TABLE IF EXISTS `nameinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nameinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `IndexName` varchar(255) DEFAULT NULL,
  `NameTypeCd` varchar(255) DEFAULT NULL,
  `PositionCd` varchar(255) DEFAULT NULL,
  `GivenName` varchar(255) DEFAULT NULL,
  `Surname` varchar(255) DEFAULT NULL,
  `OtherGivenName` varchar(255) DEFAULT NULL,
  `ShortName` varchar(255) DEFAULT NULL,
  `CommercialName` varchar(255) DEFAULT NULL,
  `CommercialName2` varchar(255) DEFAULT NULL,
  `DBAName` varchar(255) DEFAULT NULL,
  `DBAIndexName` varchar(255) DEFAULT NULL,
  `PrefixCd` varchar(255) DEFAULT NULL,
  `SuffixCd` varchar(255) DEFAULT NULL,
  `ExtendedName` varchar(255) DEFAULT NULL,
  KEY `nameinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `newsbanner`
--

DROP TABLE IF EXISTS `newsbanner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsbanner` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `TemplateId` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `HtmlContent` varchar(255) DEFAULT NULL,
  `SortOrder` int(11) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `StartTm` varchar(255) DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `EndTm` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `AddUser` varchar(255) DEFAULT NULL,
  `UpdateDt` date DEFAULT NULL,
  `UpdateTm` varchar(255) DEFAULT NULL,
  `SiteLocation` varchar(255) DEFAULT NULL,
  `Version` varchar(255) DEFAULT NULL,
  KEY `newsbannerIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `newsbanneruserrole`
--

DROP TABLE IF EXISTS `newsbanneruserrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsbanneruserrole` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AuthorityRoleIdRef` varchar(255) DEFAULT NULL,
  KEY `newsbanneruserroleIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nonapprfundactcovendinfo`
--

DROP TABLE IF EXISTS `nonapprfundactcovendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nonapprfundactcovendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `WorkDescription` varchar(255) DEFAULT NULL,
  `EndorsementSeqNo` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `nonapprfundactcovendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `note`
--

DROP TABLE IF EXISTS `note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `note` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `TemplateId` varchar(255) DEFAULT NULL,
  `IdRef` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `PriorityCd` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Memo` varchar(255) DEFAULT NULL,
  `StickyInd` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `AddUser` varchar(255) DEFAULT NULL,
  `UpdateDt` date DEFAULT NULL,
  `UpdateTm` varchar(255) DEFAULT NULL,
  `SourceRef` varchar(255) DEFAULT NULL,
  `TransactionNumber` varchar(255) DEFAULT NULL,
  `SourceContainerName` varchar(255) DEFAULT NULL,
  KEY `noteIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notetemplateref`
--

DROP TABLE IF EXISTS `notetemplateref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notetemplateref` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TemplateIdRef` varchar(255) DEFAULT NULL,
  KEY `notetemplaterefIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notificationrequest`
--

DROP TABLE IF EXISTS `notificationrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notificationrequest` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `EventCd` varchar(255) DEFAULT NULL,
  `EventUserId` varchar(255) DEFAULT NULL,
  `NotificationTemplateIdRef` varchar(255) DEFAULT NULL,
  `ImmediateNotificationStatusCd` varchar(255) DEFAULT NULL,
  `BatchNotificationStatusCd` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `UpdateDt` date DEFAULT NULL,
  `UpdateTm` varchar(255) DEFAULT NULL,
  KEY `notificationrequestIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ocslactcovendinfo`
--

DROP TABLE IF EXISTS `ocslactcovendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ocslactcovendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `WorkDescription` varchar(255) DEFAULT NULL,
  `EndorsementSeqNo` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `ocslactcovendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `otherbenefitoccurance`
--

DROP TABLE IF EXISTS `otherbenefitoccurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `otherbenefitoccurance` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `OtherBenefitTypeCd` varchar(255) DEFAULT NULL,
  `OtherBenefitTypeAmt` varchar(255) DEFAULT NULL,
  KEY `otherbenefitoccuranceIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `otherinsurance`
--

DROP TABLE IF EXISTS `otherinsurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `otherinsurance` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `OtherInsuranceCd` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `ClaimNumber` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `PreferredDeliveryMethod` varchar(255) DEFAULT NULL,
  KEY `otherinsuranceIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `output`
--

DROP TABLE IF EXISTS `output`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `output` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `DocumentType` varchar(255) DEFAULT NULL,
  `OutputTemplateIdRef` varchar(255) DEFAULT NULL,
  `OutputNumber` varchar(255) DEFAULT NULL,
  `TransactionNumber` varchar(255) DEFAULT NULL,
  `SubmissionNumber` varchar(255) DEFAULT NULL,
  `AddUser` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `OriginalOutputIdRef` varchar(255) DEFAULT NULL,
  `FormURL` varchar(255) DEFAULT NULL,
  `ClaimantIdRef` varchar(255) DEFAULT NULL,
  `ClaimantTransactionIdRef` varchar(255) DEFAULT NULL,
  KEY `outputIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `outputitem`
--

DROP TABLE IF EXISTS `outputitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outputitem` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `IncludeInd` varchar(255) DEFAULT NULL,
  `ItemName` varchar(255) DEFAULT NULL,
  `ItemDescription` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `FormCd` varchar(255) DEFAULT NULL,
  `DeliveryCd` varchar(255) DEFAULT NULL,
  `DestinationCd` varchar(255) DEFAULT NULL,
  `DefaultInd` varchar(255) DEFAULT NULL,
  `MergeFileURL` varchar(255) DEFAULT NULL,
  `FormURL` varchar(255) DEFAULT NULL,
  `RecipientCd` varchar(255) DEFAULT NULL,
  `DistributionRef` varchar(255) DEFAULT NULL,
  `OverrideInd` varchar(255) DEFAULT NULL,
  `MailProofCd` varchar(255) DEFAULT NULL,
  `CoversheetFormTemplateIdRef` varchar(255) DEFAULT NULL,
  `OutputTemplateRecipientIdRef` varchar(255) DEFAULT NULL,
  `NumSimplexPrintPages` int(11) DEFAULT NULL,
  `NumDuplexPrintPages` int(11) DEFAULT NULL,
  `DocumentTypeCd` varchar(255) DEFAULT NULL,
  `PrintEngineCd` varchar(255) DEFAULT NULL,
  `SourceIdRef` varchar(255) DEFAULT NULL,
  `FormatTemplateIdRef` varchar(255) DEFAULT NULL,
  KEY `outputitemIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `partnerofficerotherexcendinfo`
--

DROP TABLE IF EXISTS `partnerofficerotherexcendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partnerofficerotherexcendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `DescriptorCd0` varchar(255) DEFAULT NULL,
  `DescriptorCd1` varchar(255) DEFAULT NULL,
  `DescriptorCd2` varchar(255) DEFAULT NULL,
  `NameofPersonIncluded0` varchar(255) DEFAULT NULL,
  `NameofPersonIncluded1` varchar(255) DEFAULT NULL,
  `NameofPersonIncluded2` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `partnerofficerotherexcendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `partyinfo`
--

DROP TABLE IF EXISTS `partyinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partyinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PartyTypeCd` varchar(255) DEFAULT NULL,
  `ContactName` varchar(255) DEFAULT NULL,
  `Desc` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `Code` varchar(255) DEFAULT NULL,
  `FutureStatusCd` varchar(255) DEFAULT NULL,
  `YearsOfService` int(11) DEFAULT NULL,
  KEY `partyinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `passportproduct`
--

DROP TABLE IF EXISTS `passportproduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passportproduct` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  KEY `passportproductIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `passportreport`
--

DROP TABLE IF EXISTS `passportreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passportreport` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `RequestId` varchar(255) DEFAULT NULL,
  `ReportXml` varchar(255) DEFAULT NULL,
  `LocGuid` varchar(255) DEFAULT NULL,
  `Agent` varchar(255) DEFAULT NULL,
  `QuoteNumberId` varchar(255) DEFAULT NULL,
  `UnderwriterId` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `ReferenceNumber` varchar(255) DEFAULT NULL,
  KEY `passportreportIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `passportreportrequest`
--

DROP TABLE IF EXISTS `passportreportrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passportreportrequest` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `RequestId` varchar(255) DEFAULT NULL,
  `ApplicationIdRef` varchar(255) DEFAULT NULL,
  KEY `passportreportrequestIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payablebatchrequest`
--

DROP TABLE IF EXISTS `payablebatchrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payablebatchrequest` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `PaymentAccountCd` varchar(255) DEFAULT NULL,
  `PrintCategoryCd` varchar(255) DEFAULT NULL,
  `PaymentReference` varchar(255) DEFAULT NULL,
  `PayToName` varchar(255) DEFAULT NULL,
  `ProviderRef` int(11) DEFAULT NULL,
  `ItemNumber` varchar(255) DEFAULT NULL,
  `ItemDt` date DEFAULT NULL,
  `ItemAmt` decimal(28,6) DEFAULT NULL,
  `PrinterTemplateIdRef` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `PrintCategoryGroup` varchar(255) DEFAULT NULL,
  KEY `payablebatchrequestIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payablebatchrequestalloc`
--

DROP TABLE IF EXISTS `payablebatchrequestalloc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payablebatchrequestalloc` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TransactionDt` date DEFAULT NULL,
  `TransactionTm` varchar(255) DEFAULT NULL,
  `TransactionUser` varchar(255) DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `RequestDt` date DEFAULT NULL,
  `Memo` varchar(255) DEFAULT NULL,
  `MemoCd` varchar(255) DEFAULT NULL,
  `ClassificationCd` varchar(255) DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `SourceRef` varchar(255) DEFAULT NULL,
  `AllocationAmt` decimal(28,6) DEFAULT NULL,
  KEY `payablebatchrequestallocIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payabledetail`
--

DROP TABLE IF EXISTS `payabledetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payabledetail` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  `AccountTransReference` varchar(255) DEFAULT NULL,
  `AccountTransIdRef` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyProviderRef` int(11) DEFAULT NULL,
  `LossDt` date DEFAULT NULL,
  `CombinePaymentInd` varchar(255) DEFAULT NULL,
  `ClaimantNumber` int(11) DEFAULT NULL,
  `ClaimNumber` varchar(255) DEFAULT NULL,
  `SequenceNumber` int(11) DEFAULT NULL,
  `ClaimantTransactionIdRef` varchar(255) DEFAULT NULL,
  `ServicePeriodStartDt` date DEFAULT NULL,
  `ServicePeriodEndDt` date DEFAULT NULL,
  KEY `payabledetailIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payablerequest`
--

DROP TABLE IF EXISTS `payablerequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payablerequest` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `Version` varchar(255) DEFAULT NULL,
  `PaymentMethodCd` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `PaymentAccountCd` varchar(255) DEFAULT NULL,
  `PaymentReference` varchar(255) DEFAULT NULL,
  `PayToName` varchar(255) DEFAULT NULL,
  `ProviderRef` int(11) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `StatusDt` date DEFAULT NULL,
  `Memo` varchar(255) DEFAULT NULL,
  `RequestDt` date DEFAULT NULL,
  `ClassificationCd` varchar(255) DEFAULT NULL,
  `MemoCd` varchar(255) DEFAULT NULL,
  `ItemNumber` varchar(255) DEFAULT NULL,
  `ItemAmt` decimal(28,6) DEFAULT NULL,
  `ItemDt` date DEFAULT NULL,
  `PrinterTemplateIdRef` varchar(255) DEFAULT NULL,
  `TransactionDt` date DEFAULT NULL,
  `TransactionTm` varchar(255) DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `TransactionUser` varchar(255) DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `SourceRef` varchar(255) DEFAULT NULL,
  `SystemCheckReference` varchar(255) DEFAULT NULL,
  KEY `payablerequestIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `payablestats`
--

DROP TABLE IF EXISTS `payablestats`;
/*!50001 DROP VIEW IF EXISTS `payablestats`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `payablestats` AS SELECT 
 1 AS `SystemId`,
 1 AS `id`,
 1 AS `StatusCd`,
 1 AS `CombinedKey`,
 1 AS `PaymentSystemId`,
 1 AS `StatSequence`,
 1 AS `StatCd`,
 1 AS `StatSequenceReplace`,
 1 AS `AddDt`,
 1 AS `PaymentAccountCd`,
 1 AS `TypeCd`,
 1 AS `PayToName`,
 1 AS `ProviderCd`,
 1 AS `PaymentStatusCd`,
 1 AS `ProviderRef`,
 1 AS `ItemAmt`,
 1 AS `ItemNumber`,
 1 AS `ItemDt`,
 1 AS `PrinterTemplateIdRef`,
 1 AS `ClassificationCd`,
 1 AS `RequestDt`,
 1 AS `BookDt`,
 1 AS `SourceCd`,
 1 AS `SourceRef`,
 1 AS `AllocationAmt`,
 1 AS `TransactionTypeCd`,
 1 AS `TransactionDt`,
 1 AS `TransactionTm`,
 1 AS `TransactionUser`,
 1 AS `TransactionAmt`,
 1 AS `TransactionNumber`,
 1 AS `AccountNumber`,
 1 AS `AccountTransReference`,
 1 AS `AccountTransIdRef`,
 1 AS `PolicyNumber`,
 1 AS `PolicyProviderRef`,
 1 AS `LossDt`,
 1 AS `CombinePaymentInd`,
 1 AS `ClaimantNumber`,
 1 AS `ClaimNumber`,
 1 AS `SequenceNumber`,
 1 AS `ClaimantTransactionIdRef`,
 1 AS `ServicePeriodStartDt`,
 1 AS `ServicePeriodEndDt`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `PaymentAccountCd` varchar(255) DEFAULT NULL,
  `PaymentReference` varchar(255) DEFAULT NULL,
  `PayToName` varchar(255) DEFAULT NULL,
  `ProviderRef` int(11) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `ItemNumber` varchar(255) DEFAULT NULL,
  `StatusDt` date DEFAULT NULL,
  `ItemAmt` decimal(28,6) DEFAULT NULL,
  `ItemDt` date DEFAULT NULL,
  `PrinterTemplateIdRef` varchar(255) DEFAULT NULL,
  `InquiryFilename` varchar(255) DEFAULT NULL,
  `SystemCheckReference` varchar(255) DEFAULT NULL,
  `CheckUpdatedInd` varchar(255) DEFAULT NULL,
  `PaymentMethodCd` varchar(255) DEFAULT NULL,
  `PaymentMethodId` varchar(255) DEFAULT NULL,
  KEY `paymentIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paymentallocation`
--

DROP TABLE IF EXISTS `paymentallocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentallocation` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Memo` varchar(255) DEFAULT NULL,
  `MemoCd` varchar(255) DEFAULT NULL,
  `ClassificationCd` varchar(255) DEFAULT NULL,
  `TransactionDt` date DEFAULT NULL,
  `TransactionTm` varchar(255) DEFAULT NULL,
  `TransactionUser` varchar(255) DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `RequestDt` date DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `SourceRef` varchar(255) DEFAULT NULL,
  `AllocationAmt` decimal(28,6) DEFAULT NULL,
  KEY `paymentallocationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paymentexport`
--

DROP TABLE IF EXISTS `paymentexport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentexport` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PaymentAccountCd` varchar(255) DEFAULT NULL,
  `PaymentReference` varchar(255) DEFAULT NULL,
  `PayToName` varchar(255) DEFAULT NULL,
  `PaymentTransactionTypeCd` varchar(255) DEFAULT NULL,
  `ProviderRef` int(11) DEFAULT NULL,
  `TransactionDt` date DEFAULT NULL,
  `TransactionTm` varchar(255) DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `TransactionUser` varchar(255) DEFAULT NULL,
  `ItemDt` date DEFAULT NULL,
  `ItemNumber` varchar(255) DEFAULT NULL,
  `ItemAmt` decimal(28,6) DEFAULT NULL,
  KEY `paymentexportIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paymentexports`
--

DROP TABLE IF EXISTS `paymentexports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentexports` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `PaymentAccountCd` varchar(255) DEFAULT NULL,
  `ProcessDt` date DEFAULT NULL,
  `ProcessTm` varchar(255) DEFAULT NULL,
  `ProcessUser` varchar(255) DEFAULT NULL,
  KEY `paymentexportsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paymentimport`
--

DROP TABLE IF EXISTS `paymentimport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentimport` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PaymentAccountCd` varchar(255) DEFAULT NULL,
  `PaymentTransactionTypeCd` varchar(255) DEFAULT NULL,
  `TransactionDt` date DEFAULT NULL,
  `TransactionTm` varchar(255) DEFAULT NULL,
  `TransactionUser` varchar(255) DEFAULT NULL,
  `ItemDt` date DEFAULT NULL,
  `ItemNumber` varchar(255) DEFAULT NULL,
  `ItemAmt` decimal(28,6) DEFAULT NULL,
  KEY `paymentimportIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paymentimports`
--

DROP TABLE IF EXISTS `paymentimports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentimports` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `PaymentAccountCd` varchar(255) DEFAULT NULL,
  `ProcessDt` date DEFAULT NULL,
  `ProcessTm` varchar(255) DEFAULT NULL,
  `ProcessUser` varchar(255) DEFAULT NULL,
  KEY `paymentimportsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paymentoccurance`
--

DROP TABLE IF EXISTS `paymentoccurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentoccurance` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PaymentReasonCd` varchar(255) DEFAULT NULL,
  `Payee` varchar(255) DEFAULT NULL,
  `PaymentAmt` varchar(255) DEFAULT NULL,
  `PaymentCoversPeriod` varchar(255) DEFAULT NULL,
  `PaymentCoversPeriodDt` varchar(255) DEFAULT NULL,
  `PaymentIssueDt` varchar(255) DEFAULT NULL,
  KEY `paymentoccuranceIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paymenttransaction`
--

DROP TABLE IF EXISTS `paymenttransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymenttransaction` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PaymentTransactionTypeCd` varchar(255) DEFAULT NULL,
  `TransactionDt` date DEFAULT NULL,
  `TransactionTm` varchar(255) DEFAULT NULL,
  `TransactionUser` varchar(255) DEFAULT NULL,
  `ExportDt` date DEFAULT NULL,
  `ExportStatusInd` varchar(255) DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `PositivePayStatusCd` varchar(255) DEFAULT NULL,
  `PositivePayExportDt` date DEFAULT NULL,
  `PaymentTransactionMethodId` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  KEY `paymenttransactionIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pendingrenewalchanges`
--

DROP TABLE IF EXISTS `pendingrenewalchanges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pendingrenewalchanges` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TransactionDt` varchar(255) DEFAULT NULL,
  `TransactionUser` varchar(255) DEFAULT NULL,
  `TransactionTm` varchar(255) DEFAULT NULL,
  `TransactionNumber` varchar(255) DEFAULT NULL,
  KEY `pendingrenewalchangesIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `periodaircraft`
--

DROP TABLE IF EXISTS `periodaircraft`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `periodaircraft` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StatusCx` varchar(255) DEFAULT NULL,
  `AircraftSeatSurchargeAmt` decimal(28,6) DEFAULT NULL,
  `AircraftSeatSurchargeRate` decimal(28,6) DEFAULT NULL,
  `MaxAircraftSeatSurchargeAmt` decimal(28,6) DEFAULT NULL,
  `NumSeats` int(11) DEFAULT NULL,
  `ActualNumSeats` int(11) DEFAULT NULL,
  `RiskIdRef` varchar(255) DEFAULT NULL,
  KEY `periodaircraftIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `periodlocation`
--

DROP TABLE IF EXISTS `periodlocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `periodlocation` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StatusCx` varchar(255) DEFAULT NULL,
  `LocationIdRef` varchar(255) DEFAULT NULL,
  `LocationNumber` int(11) DEFAULT NULL,
  `ManualPremiumAmt` decimal(28,6) DEFAULT NULL,
  KEY `periodlocationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `personalinfo`
--

DROP TABLE IF EXISTS `personalinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personalinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `FirstName` varchar(255) DEFAULT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `MiddleName` varchar(255) DEFAULT NULL,
  KEY `personalinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `personinfo`
--

DROP TABLE IF EXISTS `personinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PersonTypeCd` varchar(255) DEFAULT NULL,
  `GenderCd` varchar(255) DEFAULT NULL,
  `BirthDt` date DEFAULT NULL,
  `MaritalStatusCd` varchar(255) DEFAULT NULL,
  `OccupationClassCd` varchar(255) DEFAULT NULL,
  `EmployerCd` varchar(255) DEFAULT NULL,
  `PositionTitle` varchar(255) DEFAULT NULL,
  `BestWayToContact` varchar(255) DEFAULT NULL,
  `BestTimeToContact` varchar(255) DEFAULT NULL,
  `Age` int(11) DEFAULT NULL,
  `YearsLicensed` int(11) DEFAULT NULL,
  KEY `personinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `personname`
--

DROP TABLE IF EXISTS `personname`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personname` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `GivenName` varchar(255) DEFAULT NULL,
  `Surname` varchar(255) DEFAULT NULL,
  `OtherGivenName` varchar(255) DEFAULT NULL,
  KEY `personnameIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `phoneinfo`
--

DROP TABLE IF EXISTS `phoneinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phoneinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PhoneTypeCd` varchar(255) DEFAULT NULL,
  `PhoneNumber` varchar(255) DEFAULT NULL,
  `PreferredInd` varchar(255) DEFAULT NULL,
  `PhoneName` varchar(255) DEFAULT NULL,
  KEY `phoneinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policy`
--

DROP TABLE IF EXISTS `policy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policy` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `AccountRef` int(11) DEFAULT NULL,
  `ExternalSystemInd` varchar(255) DEFAULT NULL,
  `Version` varchar(255) DEFAULT NULL,
  `VIPLevel` varchar(255) DEFAULT NULL,
  `AuditAccountRef` int(11) DEFAULT NULL,
  `StatementAccountRef` int(11) DEFAULT NULL,
  KEY `policyIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policyai`
--

DROP TABLE IF EXISTS `policyai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyai` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `InterestCd` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `PreferredDeliveryMethod` varchar(255) DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `SequenceNumber` int(11) DEFAULT NULL,
  `AdditionalComments` varchar(255) DEFAULT NULL,
  KEY `policyaiIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policyais`
--

DROP TABLE IF EXISTS `policyais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyais` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `policyaisIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policyarchive`
--

DROP TABLE IF EXISTS `policyarchive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyarchive` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `Version` varchar(255) DEFAULT NULL,
  KEY `policyarchiveIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policyclass`
--

DROP TABLE IF EXISTS `policyclass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyclass` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ClassCd` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `LocationNumber` varchar(255) DEFAULT NULL,
  `Extension` varchar(255) DEFAULT NULL,
  `Suffix` varchar(255) DEFAULT NULL,
  KEY `policyclassIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policyclasses`
--

DROP TABLE IF EXISTS `policyclasses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyclasses` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `policyclassesIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policycoveredstate`
--

DROP TABLE IF EXISTS `policycoveredstate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policycoveredstate` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  KEY `policycoveredstateIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policycoveredstates`
--

DROP TABLE IF EXISTS `policycoveredstates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policycoveredstates` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `policycoveredstatesIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policydrivers`
--

DROP TABLE IF EXISTS `policydrivers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policydrivers` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `policydriversIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policyform`
--

DROP TABLE IF EXISTS `policyform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyform` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `FormNum` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `FormEdition` varchar(255) DEFAULT NULL,
  KEY `policyformIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policyforms`
--

DROP TABLE IF EXISTS `policyforms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyforms` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `policyformsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policyhistory`
--

DROP TABLE IF EXISTS `policyhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyhistory` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `TransactionNumber` varchar(255) DEFAULT NULL,
  `AddDt` varchar(255) DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `Version` varchar(255) DEFAULT NULL,
  KEY `policyhistoryIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policyinfoaddresschangeendinfo`
--

DROP TABLE IF EXISTS `policyinfoaddresschangeendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyinfoaddresschangeendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `AddressStreet` varchar(255) DEFAULT NULL,
  `AddressCity` varchar(255) DEFAULT NULL,
  `AddressState` varchar(255) DEFAULT NULL,
  `AddressZipCode` varchar(255) DEFAULT NULL,
  `NameLinkIdentifier` int(11) DEFAULT NULL,
  `StateCodeLink` int(11) DEFAULT NULL,
  `ExposureRecordLink` int(11) DEFAULT NULL,
  `NumberofEmployees` int(11) DEFAULT NULL,
  `IndustryCode` varchar(255) DEFAULT NULL,
  `InsuredPhoneNumber` int(11) DEFAULT NULL,
  `GeographicArea` varchar(255) DEFAULT NULL,
  `CountryCode` varchar(255) DEFAULT NULL,
  `AddressRevisionCd` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `policyinfoaddresschangeendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policyinfoclassratechangeendinfo`
--

DROP TABLE IF EXISTS `policyinfoclassratechangeendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyinfoclassratechangeendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `ExposurePeriodEffDt` int(11) DEFAULT NULL,
  `ClassificationCode` int(11) DEFAULT NULL,
  `ExposureCovCode` int(11) DEFAULT NULL,
  `ManualChargedRate` int(11) DEFAULT NULL,
  `EstExposureAmount` int(11) DEFAULT NULL,
  `EstPremiumAmount` int(11) DEFAULT NULL,
  `ClassWordingSuffix` varchar(255) DEFAULT NULL,
  `ClassWording` varchar(255) DEFAULT NULL,
  `NameLinkIdentifier` int(11) DEFAULT NULL,
  `StateCodeLink` int(11) DEFAULT NULL,
  `ExpRecordLinkforExpCode` int(11) DEFAULT NULL,
  `ApparatusCount` int(11) DEFAULT NULL,
  `VolunteersCount` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `policyinfoclassratechangeendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policyinfodatachangeendinfo`
--

DROP TABLE IF EXISTS `policyinfodatachangeendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyinfodatachangeendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `ExpirationDate` int(11) DEFAULT NULL,
  `LegalNatureofInsuredCd` int(11) DEFAULT NULL,
  `LegalNatureofInsuredCdText` varchar(255) DEFAULT NULL,
  `Item3AOr3CCd` varchar(255) DEFAULT NULL,
  `Item3CInclOrExclCd` int(11) DEFAULT NULL,
  `Item3AOr3CStateCd01` int(11) DEFAULT NULL,
  `Item3AOr3CStateCd02` int(11) DEFAULT NULL,
  `Item3AOr3CStateCd03` int(11) DEFAULT NULL,
  `Item3AOr3CStateCd04` int(11) DEFAULT NULL,
  `Item3AOr3CStateCd05` int(11) DEFAULT NULL,
  `Item3AOr3CStateCd06` int(11) DEFAULT NULL,
  `Item3AOr3CStateCd07` int(11) DEFAULT NULL,
  `Item3AOr3CStateCd08` int(11) DEFAULT NULL,
  `Item3AOr3CStateCd09` int(11) DEFAULT NULL,
  `Item3AOr3CStateCd10` int(11) DEFAULT NULL,
  `Item3AOr3CStateCd11` int(11) DEFAULT NULL,
  `Item3AOr3CStateCd12` int(11) DEFAULT NULL,
  `Item3AOr3CStateCd13` int(11) DEFAULT NULL,
  `Item3AOr3CStateCd14` int(11) DEFAULT NULL,
  `Item3AOr3CStateCd15` int(11) DEFAULT NULL,
  `EmprLiabLimitAmtBIBA` int(11) DEFAULT NULL,
  `EmprLiabLimitAmtBIBD` int(11) DEFAULT NULL,
  `EmprLiabLimitAmtBIBDEmpAmount` int(11) DEFAULT NULL,
  `PremAdjustmentPeriodCd` int(11) DEFAULT NULL,
  `ProducerName` varchar(255) DEFAULT NULL,
  `RiskIDNumber01` int(11) DEFAULT NULL,
  `RiskIDNumber02` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `policyinfodatachangeendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policyinfonamechangeendinfo`
--

DROP TABLE IF EXISTS `policyinfonamechangeendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyinfonamechangeendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `NameTypeCd` int(11) DEFAULT NULL,
  `NameLinkIdentifier` int(11) DEFAULT NULL,
  `NameofInsured` int(11) DEFAULT NULL,
  `FEIN` int(11) DEFAULT NULL,
  `ContinuationSeqNo` int(11) DEFAULT NULL,
  `LegalNatureofEntityCode` int(11) DEFAULT NULL,
  `LegalNatureofEntityCodeText` varchar(255) DEFAULT NULL,
  `StateCd01` int(11) DEFAULT NULL,
  `StateUnemploymentNo01` varchar(255) DEFAULT NULL,
  `StateCd02` int(11) DEFAULT NULL,
  `StateUnemploymentNo02` varchar(255) DEFAULT NULL,
  `StateCd03` int(11) DEFAULT NULL,
  `StateUnemploymentNo03` varchar(255) DEFAULT NULL,
  `StateUnemploymentNumber04` int(11) DEFAULT NULL,
  `NameRevisionCd` varchar(255) DEFAULT NULL,
  `ClientCompanyCd` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `PolicyChangeEffectiveDate` int(11) DEFAULT NULL,
  `NameLinkCounterIdentifier` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `policyinfonamechangeendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policyinfosuppdatachangeendinfo`
--

DROP TABLE IF EXISTS `policyinfosuppdatachangeendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyinfosuppdatachangeendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `DataElementChangeNo` varchar(255) DEFAULT NULL,
  `ExpRatingCd` int(11) DEFAULT NULL,
  `FederalEmployerId` int(11) DEFAULT NULL,
  `TypeofCoverageCd` int(11) DEFAULT NULL,
  `EmployeeLeasePolicyTypeCd` int(11) DEFAULT NULL,
  `PolicyTermCd` int(11) DEFAULT NULL,
  `PriorPolicyNumber` varchar(255) DEFAULT NULL,
  `PriorUnitCertificateNo` int(11) DEFAULT NULL,
  `TypeofPlanCd` int(11) DEFAULT NULL,
  `BusinessSegmentId` int(11) DEFAULT NULL,
  `MinimumPremiumAmt` int(11) DEFAULT NULL,
  `MinimumPremiumStateCd` int(11) DEFAULT NULL,
  `EstStdPremiumTotal` int(11) DEFAULT NULL,
  `DepositPremiumAmt` int(11) DEFAULT NULL,
  `AuditFrequencyCd` int(11) DEFAULT NULL,
  `BillingFrequencyCd` int(11) DEFAULT NULL,
  `RetrospecRatingCd` int(11) DEFAULT NULL,
  `GroupCovStsCd` int(11) DEFAULT NULL,
  `AssignmentDate` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `policyinfosuppdatachangeendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policylocation`
--

DROP TABLE IF EXISTS `policylocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policylocation` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `LocationNumber` varchar(255) DEFAULT NULL,
  `LocationDesc` varchar(255) DEFAULT NULL,
  `LossLocationDesc` varchar(255) DEFAULT NULL,
  KEY `policylocationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policylocations`
--

DROP TABLE IF EXISTS `policylocations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policylocations` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `policylocationsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policyperiodendinfo`
--

DROP TABLE IF EXISTS `policyperiodendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyperiodendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `EffectiveDt01` int(11) DEFAULT NULL,
  `ExpirationDt01` int(11) DEFAULT NULL,
  `EffectiveDt02` int(11) DEFAULT NULL,
  `ExpirationDt02` int(11) DEFAULT NULL,
  `EffectiveDt03` int(11) DEFAULT NULL,
  `ExpirationDt03` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `policyperiodendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policyreinsdetailsubject`
--

DROP TABLE IF EXISTS `policyreinsdetailsubject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyreinsdetailsubject` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `ReinsuranceCoverageGroupCd` varchar(255) DEFAULT NULL,
  `TotalInsuredValue` decimal(28,6) DEFAULT NULL,
  `BasePremiumAmt` decimal(28,6) DEFAULT NULL,
  KEY `policyreinsdetailsubjectIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policyreinsplacement`
--

DROP TABLE IF EXISTS `policyreinsplacement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyreinsplacement` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PlacementId` varchar(255) DEFAULT NULL,
  `ReinsuranceType` varchar(255) DEFAULT NULL,
  `StartLimitAmt` int(11) DEFAULT NULL,
  `PlacementAmt` int(11) DEFAULT NULL,
  `TreatyContract` varchar(255) DEFAULT NULL,
  `PlacementPct` decimal(28,6) DEFAULT NULL,
  `PremiumAmt` decimal(28,6) DEFAULT NULL,
  `TreatyTypeCd` varchar(255) DEFAULT NULL,
  `CommissionAmt` decimal(28,6) DEFAULT NULL,
  `CommissionPct` decimal(28,6) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `TransactionCommissionAmt` decimal(28,6) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `LossCalculationTypeCd` varchar(255) DEFAULT NULL,
  `BasePremiumAmt` decimal(28,6) DEFAULT NULL,
  `PrevFinalAmt` decimal(28,6) DEFAULT NULL,
  `WrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `PrevWrittenAmt` decimal(28,6) DEFAULT NULL,
  `AutomaticRecoveryAdjustmentInd` varchar(255) DEFAULT NULL,
  `BaseWrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `BaseWrittenPremiumToDateAmt` decimal(28,6) DEFAULT NULL,
  `PremiumWrittenAmt` decimal(28,6) DEFAULT NULL,
  `PrevPremiumWrittenAmt` decimal(28,6) DEFAULT NULL,
  KEY `policyreinsplacementIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policyreinsplacementsubj`
--

DROP TABLE IF EXISTS `policyreinsplacementsubj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyreinsplacementsubj` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `DetailSubjectIdRef` varchar(255) DEFAULT NULL,
  `PremiumAmt` decimal(28,6) DEFAULT NULL,
  `PrevFinalAmt` decimal(28,6) DEFAULT NULL,
  `WrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `PrevWrittenAmt` decimal(28,6) DEFAULT NULL,
  `PremiumWrittenAmt` decimal(28,6) DEFAULT NULL,
  `PrevPremiumWrittenAmt` decimal(28,6) DEFAULT NULL,
  `CommissionAmt` decimal(28,6) DEFAULT NULL,
  `TransactionCommissionAmt` decimal(28,6) DEFAULT NULL,
  KEY `policyreinsplacementsubjIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `policyreinsrenewalofferstats`
--

DROP TABLE IF EXISTS `policyreinsrenewalofferstats`;
/*!50001 DROP VIEW IF EXISTS `policyreinsrenewalofferstats`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `policyreinsrenewalofferstats` AS SELECT 
 1 AS `SystemId`,
 1 AS `id`,
 1 AS `StatSequence`,
 1 AS `StatSequenceReplace`,
 1 AS `PolicyRef`,
 1 AS `PolicyNumber`,
 1 AS `PolicyVersion`,
 1 AS `TransactionNumber`,
 1 AS `EffectiveDt`,
 1 AS `ExpirationDt`,
 1 AS `CombinedKey`,
 1 AS `AddDt`,
 1 AS `BookDt`,
 1 AS `TransactionEffectiveDt`,
 1 AS `AccountingDt`,
 1 AS `WrittenPremiumAmt`,
 1 AS `WrittenCommissionAmt`,
 1 AS `EarnDays`,
 1 AS `InforceChangeAmt`,
 1 AS `PolicyYear`,
 1 AS `TermDays`,
 1 AS `InsuranceTypeCd`,
 1 AS `StartDt`,
 1 AS `EndDt`,
 1 AS `StatusCd`,
 1 AS `NewRenewalCd`,
 1 AS `AnnualStatementLineCd`,
 1 AS `ReinsuranceName`,
 1 AS `MasterSubInd`,
 1 AS `ReinsuranceItemName`,
 1 AS `ProviderCd`,
 1 AS `TransactionCd`,
 1 AS `StateCd`,
 1 AS `ReinsuranceGroupId`,
 1 AS `ProductVersionIdRef`,
 1 AS `CarrierGroupCd`,
 1 AS `CarrierCd`,
 1 AS `PolicyStatusCd`,
 1 AS `ProductName`,
 1 AS `PolicyTypeCd`,
 1 AS `PolicyGroupCd`,
 1 AS `CustomerRef`,
 1 AS `ReinsuranceCoverageGroupCd`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `policyreinsurance`
--

DROP TABLE IF EXISTS `policyreinsurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyreinsurance` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CoveredElsewhereInd` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `BaseWrittenPremiumInd` varchar(255) DEFAULT NULL,
  KEY `policyreinsuranceIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policyreinsurancedetail`
--

DROP TABLE IF EXISTS `policyreinsurancedetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyreinsurancedetail` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `GroupIdRef` varchar(255) DEFAULT NULL,
  `GroupIdName` varchar(255) DEFAULT NULL,
  `ReinsuranceTemplateIdRef` varchar(255) DEFAULT NULL,
  `ProbableMaxLoss` int(11) DEFAULT NULL,
  `TotalInsuredValue` int(11) DEFAULT NULL,
  `PlacementAmt` int(11) DEFAULT NULL,
  `StartLimitAmt` int(11) DEFAULT NULL,
  `AmtToPlace` int(11) DEFAULT NULL,
  `BasePremiumAmt` decimal(28,6) DEFAULT NULL,
  `PMLOverrideInd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `BaseWrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `BaseWrittenPremiumToDateAmt` decimal(28,6) DEFAULT NULL,
  KEY `policyreinsurancedetailIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `policyreinsurancestats`
--

DROP TABLE IF EXISTS `policyreinsurancestats`;
/*!50001 DROP VIEW IF EXISTS `policyreinsurancestats`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `policyreinsurancestats` AS SELECT 
 1 AS `SystemId`,
 1 AS `id`,
 1 AS `StatSequence`,
 1 AS `StatSequenceReplace`,
 1 AS `PolicyRef`,
 1 AS `PolicyNumber`,
 1 AS `PolicyVersion`,
 1 AS `TransactionNumber`,
 1 AS `EffectiveDt`,
 1 AS `ExpirationDt`,
 1 AS `CombinedKey`,
 1 AS `AddDt`,
 1 AS `BookDt`,
 1 AS `TransactionEffectiveDt`,
 1 AS `AccountingDt`,
 1 AS `WrittenPremiumAmt`,
 1 AS `WrittenCommissionAmt`,
 1 AS `EarnDays`,
 1 AS `InforceChangeAmt`,
 1 AS `PolicyYear`,
 1 AS `TermDays`,
 1 AS `InsuranceTypeCd`,
 1 AS `StartDt`,
 1 AS `EndDt`,
 1 AS `StatusCd`,
 1 AS `NewRenewalCd`,
 1 AS `AnnualStatementLineCd`,
 1 AS `ReinsuranceName`,
 1 AS `MasterSubInd`,
 1 AS `ReinsuranceItemName`,
 1 AS `ProviderCd`,
 1 AS `TransactionCd`,
 1 AS `StateCd`,
 1 AS `ReinsuranceGroupId`,
 1 AS `ProductVersionIdRef`,
 1 AS `CarrierGroupCd`,
 1 AS `CarrierCd`,
 1 AS `PolicyStatusCd`,
 1 AS `ProductName`,
 1 AS `PolicyTypeCd`,
 1 AS `PolicyGroupCd`,
 1 AS `CustomerRef`,
 1 AS `ReinsuranceCoverageGroupCd`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `policyreinsurancesummarystats`
--

DROP TABLE IF EXISTS `policyreinsurancesummarystats`;
/*!50001 DROP VIEW IF EXISTS `policyreinsurancesummarystats`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `policyreinsurancesummarystats` AS SELECT 
 1 AS `SystemId`,
 1 AS `id`,
 1 AS `StatSequence`,
 1 AS `StatSequenceReplace`,
 1 AS `ReportPeriod`,
 1 AS `UpdateDt`,
 1 AS `PolicyRef`,
 1 AS `PolicyNumber`,
 1 AS `PolicyVersion`,
 1 AS `TransactionNumber`,
 1 AS `EffectiveDt`,
 1 AS `ExpirationDt`,
 1 AS `CombinedKey`,
 1 AS `AccountingDt`,
 1 AS `PolicyYear`,
 1 AS `InsuranceTypeCd`,
 1 AS `StatusCd`,
 1 AS `NewRenewalCd`,
 1 AS `AnnualStatementLineCd`,
 1 AS `ReinsuranceName`,
 1 AS `MasterSubInd`,
 1 AS `ReinsuranceItemName`,
 1 AS `ProviderCd`,
 1 AS `TransactionCd`,
 1 AS `StateCd`,
 1 AS `ReinsuranceGroupId`,
 1 AS `MTDWrittenPremiumAmt`,
 1 AS `TTDWrittenPremiumAmt`,
 1 AS `YTDWrittenPremiumAmt`,
 1 AS `MTDWrittenCommissionAmt`,
 1 AS `TTDWrittenCommissionAmt`,
 1 AS `YTDWrittenCommissionAmt`,
 1 AS `InforceAmt`,
 1 AS `LastInforceAmt`,
 1 AS `MonthEarnedPremiumAmt`,
 1 AS `MTDEarnedPremiumAmt`,
 1 AS `TTDEarnedPremiumAmt`,
 1 AS `YTDEarnedPremiumAmt`,
 1 AS `MonthUnearnedAmt`,
 1 AS `UnearnedAmt`,
 1 AS `UnearnedM00Amt`,
 1 AS `UnearnedM01Amt`,
 1 AS `UnearnedM02Amt`,
 1 AS `UnearnedM03Amt`,
 1 AS `UnearnedM04Amt`,
 1 AS `UnearnedM05Amt`,
 1 AS `UnearnedM06Amt`,
 1 AS `UnearnedM07Amt`,
 1 AS `UnearnedM08Amt`,
 1 AS `UnearnedM09Amt`,
 1 AS `UnearnedM10Amt`,
 1 AS `UnearnedM11Amt`,
 1 AS `UnearnedM12Amt`,
 1 AS `UnearnedM13Amt`,
 1 AS `UnearnedM14Amt`,
 1 AS `UnearnedM15Amt`,
 1 AS `UnearnedM16Amt`,
 1 AS `UnearnedM17Amt`,
 1 AS `UnearnedM18Amt`,
 1 AS `UnearnedM19Amt`,
 1 AS `UnearnedM20Amt`,
 1 AS `UnearnedM21Amt`,
 1 AS `UnearnedM22Amt`,
 1 AS `UnearnedM23Amt`,
 1 AS `UnearnedM24Amt`,
 1 AS `UnearnedM25Amt`,
 1 AS `UnearnedM26Amt`,
 1 AS `UnearnedM27Amt`,
 1 AS `UnearnedM28Amt`,
 1 AS `UnearnedM29Amt`,
 1 AS `UnearnedM30Amt`,
 1 AS `UnearnedM31Amt`,
 1 AS `UnearnedM32Amt`,
 1 AS `UnearnedM33Amt`,
 1 AS `UnearnedM34Amt`,
 1 AS `UnearnedM35Amt`,
 1 AS `UnearnedM36Amt`,
 1 AS `UnearnedM37Amt`,
 1 AS `UnearnedM38Amt`,
 1 AS `UnearnedM39Amt`,
 1 AS `UnearnedM40Amt`,
 1 AS `UnearnedM41Amt`,
 1 AS `UnearnedM42Amt`,
 1 AS `UnearnedM43Amt`,
 1 AS `UnearnedM44Amt`,
 1 AS `UnearnedM45Amt`,
 1 AS `UnearnedM46Amt`,
 1 AS `UnearnedM47Amt`,
 1 AS `UnearnedM48Amt`,
 1 AS `ProductVersionIdRef`,
 1 AS `CarrierGroupCd`,
 1 AS `CarrierCd`,
 1 AS `PolicyStatusCd`,
 1 AS `ProductName`,
 1 AS `PolicyTypeCd`,
 1 AS `PolicyGroupCd`,
 1 AS `CustomerRef`,
 1 AS `ContractReinsuranceInd`,
 1 AS `ReinsuranceCoverageGroupCd`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `policyrenewalofferstats`
--

DROP TABLE IF EXISTS `policyrenewalofferstats`;
/*!50001 DROP VIEW IF EXISTS `policyrenewalofferstats`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `policyrenewalofferstats` AS SELECT 
 1 AS `SystemId`,
 1 AS `id`,
 1 AS `StatSequence`,
 1 AS `StatSequenceReplace`,
 1 AS `PolicyRef`,
 1 AS `PolicyNumber`,
 1 AS `PolicyVersion`,
 1 AS `LineCd`,
 1 AS `TransactionNumber`,
 1 AS `CoverageCd`,
 1 AS `RiskCd`,
 1 AS `RiskTypeCd`,
 1 AS `SubTypeCd`,
 1 AS `ProductVersionIdRef`,
 1 AS `CoverageItemCd`,
 1 AS `FeeCd`,
 1 AS `EffectiveDt`,
 1 AS `ExpirationDt`,
 1 AS `CombinedKey`,
 1 AS `AddDt`,
 1 AS `BookDt`,
 1 AS `TransactionEffectiveDt`,
 1 AS `AccountingDt`,
 1 AS `WrittenPremiumAmt`,
 1 AS `WrittenCommissionAmt`,
 1 AS `WrittenPremiumFeeAmt`,
 1 AS `WrittenCommissionFeeAmt`,
 1 AS `EarnDays`,
 1 AS `InforceChangeAmt`,
 1 AS `PolicyYear`,
 1 AS `TermDays`,
 1 AS `InsuranceTypeCd`,
 1 AS `StartDt`,
 1 AS `EndDt`,
 1 AS `StatusCd`,
 1 AS `NewRenewalCd`,
 1 AS `Limit1`,
 1 AS `Limit2`,
 1 AS `Limit3`,
 1 AS `Deductible1`,
 1 AS `Deductible2`,
 1 AS `AnnualStatementLineCd`,
 1 AS `CoinsurancePct`,
 1 AS `CoveredPerilsCd`,
 1 AS `CommissionKey`,
 1 AS `CommissionAreaCd`,
 1 AS `RateAreaName`,
 1 AS `StatData`,
 1 AS `CustomerRef`,
 1 AS `ShortRateStatInd`,
 1 AS `TransactionCd`,
 1 AS `StateCd`,
 1 AS `StatementAccountNumber`,
 1 AS `StatementAccountRef`,
 1 AS `CarrierGroupCd`,
 1 AS `CarrierCd`,
 1 AS `ProducerCd`,
 1 AS `BusinessSourceCd`,
 1 AS `PreviousCarrierCd`,
 1 AS `ConstructionCd`,
 1 AS `YearBuilt`,
 1 AS `SqFt`,
 1 AS `Stories`,
 1 AS `OccupancyCd`,
 1 AS `Units`,
 1 AS `ProtectionClass`,
 1 AS `TerritoryCd`,
 1 AS `PolicyTypeCd`,
 1 AS `PolicyStatusCd`,
 1 AS `PolicyGroupCd`,
 1 AS `CancelReason`,
 1 AS `PayPlanCd`,
 1 AS `ClassCd`,
 1 AS `AgeOfHome`,
 1 AS `LocationAddress1`,
 1 AS `LocationAddress2`,
 1 AS `LocationCity`,
 1 AS `LocationCounty`,
 1 AS `LocationState`,
 1 AS `LocationZip`,
 1 AS `VehicleNumber`,
 1 AS `VehicleYear`,
 1 AS `VehicleManufacturer`,
 1 AS `VehicleModel`,
 1 AS `VehicleType`,
 1 AS `DriverNumber`,
 1 AS `DriverFirst`,
 1 AS `DriverMI`,
 1 AS `DriverAge`,
 1 AS `DriverLast`,
 1 AS `DriverGenderCd`,
 1 AS `RelationshipToInsuredCd`,
 1 AS `DriverStartDt`,
 1 AS `PointsChargeable`,
 1 AS `PointsCharged`,
 1 AS `Limit4`,
 1 AS `Limit5`,
 1 AS `Limit6`,
 1 AS `ControllingProductVersionIdRef`,
 1 AS `ControllingStateCd`,
 1 AS `DividendPlanCd`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `policyrisk`
--

DROP TABLE IF EXISTS `policyrisk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyrisk` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `RiskNumber` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `RiskIdRef` varchar(255) DEFAULT NULL,
  `VehicleModelYr` varchar(255) DEFAULT NULL,
  `VehicleManufacturer` varchar(255) DEFAULT NULL,
  `VehicleModel` varchar(255) DEFAULT NULL,
  `VehicleIdentificationNumber` varchar(255) DEFAULT NULL,
  `LineCd` varchar(255) DEFAULT NULL,
  `LocationNumber` varchar(255) DEFAULT NULL,
  `NCICMake` varchar(255) DEFAULT NULL,
  `VehicleInd` varchar(255) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `ClaimSetupName` varchar(255) DEFAULT NULL,
  KEY `policyriskIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policyrisks`
--

DROP TABLE IF EXISTS `policyrisks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policyrisks` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `policyrisksIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `policystats`
--

DROP TABLE IF EXISTS `policystats`;
/*!50001 DROP VIEW IF EXISTS `policystats`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `policystats` AS SELECT 
 1 AS `SystemId`,
 1 AS `id`,
 1 AS `StatSequence`,
 1 AS `StatSequenceReplace`,
 1 AS `PolicyRef`,
 1 AS `ProductVersionIdRef`,
 1 AS `PolicyNumber`,
 1 AS `PolicyVersion`,
 1 AS `LineCd`,
 1 AS `TransactionNumber`,
 1 AS `CoverageCd`,
 1 AS `RiskCd`,
 1 AS `RiskTypeCd`,
 1 AS `SubTypeCd`,
 1 AS `CoverageItemCd`,
 1 AS `FeeCd`,
 1 AS `EffectiveDt`,
 1 AS `ExpirationDt`,
 1 AS `CombinedKey`,
 1 AS `AddDt`,
 1 AS `BookDt`,
 1 AS `TransactionEffectiveDt`,
 1 AS `AccountingDt`,
 1 AS `WrittenPremiumAmt`,
 1 AS `WrittenCommissionAmt`,
 1 AS `WrittenPremiumFeeAmt`,
 1 AS `WrittenCommissionFeeAmt`,
 1 AS `EarnDays`,
 1 AS `InforceChangeAmt`,
 1 AS `PolicyYear`,
 1 AS `TermDays`,
 1 AS `InsuranceTypeCd`,
 1 AS `StartDt`,
 1 AS `EndDt`,
 1 AS `StatusCd`,
 1 AS `NewRenewalCd`,
 1 AS `Limit1`,
 1 AS `Limit2`,
 1 AS `Limit3`,
 1 AS `Deductible1`,
 1 AS `Deductible2`,
 1 AS `AnnualStatementLineCd`,
 1 AS `CoinsurancePct`,
 1 AS `CoveredPerilsCd`,
 1 AS `CommissionKey`,
 1 AS `CommissionAreaCd`,
 1 AS `RateAreaName`,
 1 AS `StatData`,
 1 AS `CustomerRef`,
 1 AS `ShortRateStatInd`,
 1 AS `TransactionCd`,
 1 AS `StateCd`,
 1 AS `StatementAccountNumber`,
 1 AS `StatementAccountRef`,
 1 AS `CarrierGroupCd`,
 1 AS `CarrierCd`,
 1 AS `ProducerCd`,
 1 AS `BusinessSourceCd`,
 1 AS `PreviousCarrierCd`,
 1 AS `ConstructionCd`,
 1 AS `YearBuilt`,
 1 AS `SqFt`,
 1 AS `Stories`,
 1 AS `OccupancyCd`,
 1 AS `Units`,
 1 AS `ProtectionClass`,
 1 AS `TerritoryCd`,
 1 AS `PolicyTypeCd`,
 1 AS `PolicyStatusCd`,
 1 AS `PolicyGroupCd`,
 1 AS `CancelReason`,
 1 AS `PayPlanCd`,
 1 AS `ClassCd`,
 1 AS `AgeOfHome`,
 1 AS `LocationAddress1`,
 1 AS `LocationAddress2`,
 1 AS `LocationCity`,
 1 AS `LocationCounty`,
 1 AS `LocationState`,
 1 AS `LocationZip`,
 1 AS `VehicleNumber`,
 1 AS `VehicleYear`,
 1 AS `VehicleManufacturer`,
 1 AS `VehicleModel`,
 1 AS `VehicleType`,
 1 AS `DriverNumber`,
 1 AS `DriverFirst`,
 1 AS `DriverMI`,
 1 AS `DriverAge`,
 1 AS `DriverLast`,
 1 AS `DriverGenderCd`,
 1 AS `RelationshipToInsuredCd`,
 1 AS `DriverStartDt`,
 1 AS `PointsChargeable`,
 1 AS `PointsCharged`,
 1 AS `Limit4`,
 1 AS `Limit5`,
 1 AS `Limit6`,
 1 AS `ControllingStateCd`,
 1 AS `ControllingProductVersionIdRef`,
 1 AS `DividendPlanCd`,
 1 AS `ConversionTemplateIdRef`,
 1 AS `ConversionGroup`,
 1 AS `ConversionJobRef`,
 1 AS `ConversionFileName`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `policysummarystats`
--

DROP TABLE IF EXISTS `policysummarystats`;
/*!50001 DROP VIEW IF EXISTS `policysummarystats`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `policysummarystats` AS SELECT 
 1 AS `SystemId`,
 1 AS `id`,
 1 AS `StatSequence`,
 1 AS `StatSequenceReplace`,
 1 AS `ReportPeriod`,
 1 AS `UpdateDt`,
 1 AS `PolicyRef`,
 1 AS `ProductVersionIdRef`,
 1 AS `PolicyNumber`,
 1 AS `PolicyVersion`,
 1 AS `TransactionNumber`,
 1 AS `LineCd`,
 1 AS `RiskCd`,
 1 AS `RiskTypeCd`,
 1 AS `SubTypeCd`,
 1 AS `CoverageCd`,
 1 AS `CoverageItemCd`,
 1 AS `FeeCd`,
 1 AS `EffectiveDt`,
 1 AS `ExpirationDt`,
 1 AS `CombinedKey`,
 1 AS `AccountingDt`,
 1 AS `PolicyYear`,
 1 AS `InsuranceTypeCd`,
 1 AS `StatusCd`,
 1 AS `NewRenewalCd`,
 1 AS `Limit1`,
 1 AS `Limit2`,
 1 AS `Limit3`,
 1 AS `Limit4`,
 1 AS `Limit5`,
 1 AS `Limit6`,
 1 AS `Limit7`,
 1 AS `Limit8`,
 1 AS `Limit9`,
 1 AS `Deductible1`,
 1 AS `Deductible2`,
 1 AS `AnnualStatementLineCd`,
 1 AS `CoinsurancePct`,
 1 AS `CoveredPerilsCd`,
 1 AS `CommissionAreaCd`,
 1 AS `MTDWrittenPremiumAmt`,
 1 AS `TTDWrittenPremiumAmt`,
 1 AS `YTDWrittenPremiumAmt`,
 1 AS `MTDWrittenCommissionAmt`,
 1 AS `YTDWrittenCommissionAmt`,
 1 AS `TTDWrittenCommissionAmt`,
 1 AS `MTDWrittenPremiumFeeAmt`,
 1 AS `YTDWrittenPremiumFeeAmt`,
 1 AS `TTDWrittenPremiumFeeAmt`,
 1 AS `MTDWrittenCommissionFeeAmt`,
 1 AS `YTDWrittenCommissionFeeAmt`,
 1 AS `TTDWrittenCommissionFeeAmt`,
 1 AS `InforceAmt`,
 1 AS `LastInforceAmt`,
 1 AS `MonthEarnedPremiumAmt`,
 1 AS `MTDEarnedPremiumAmt`,
 1 AS `YTDEarnedPremiumAmt`,
 1 AS `TTDEarnedPremiumAmt`,
 1 AS `PolicyInforceCount`,
 1 AS `LocationInforceCount`,
 1 AS `RiskInforceCount`,
 1 AS `MonthUnearnedAmt`,
 1 AS `UnearnedAmt`,
 1 AS `UnearnedM00Amt`,
 1 AS `UnearnedM01Amt`,
 1 AS `UnearnedM02Amt`,
 1 AS `UnearnedM03Amt`,
 1 AS `UnearnedM04Amt`,
 1 AS `UnearnedM05Amt`,
 1 AS `UnearnedM06Amt`,
 1 AS `UnearnedM07Amt`,
 1 AS `UnearnedM08Amt`,
 1 AS `UnearnedM09Amt`,
 1 AS `UnearnedM10Amt`,
 1 AS `UnearnedM11Amt`,
 1 AS `UnearnedM12Amt`,
 1 AS `UnearnedM13Amt`,
 1 AS `UnearnedM14Amt`,
 1 AS `UnearnedM15Amt`,
 1 AS `UnearnedM16Amt`,
 1 AS `UnearnedM17Amt`,
 1 AS `UnearnedM18Amt`,
 1 AS `UnearnedM19Amt`,
 1 AS `UnearnedM20Amt`,
 1 AS `UnearnedM21Amt`,
 1 AS `UnearnedM22Amt`,
 1 AS `UnearnedM23Amt`,
 1 AS `UnearnedM24Amt`,
 1 AS `UnearnedM25Amt`,
 1 AS `UnearnedM26Amt`,
 1 AS `UnearnedM27Amt`,
 1 AS `UnearnedM28Amt`,
 1 AS `UnearnedM29Amt`,
 1 AS `UnearnedM30Amt`,
 1 AS `UnearnedM31Amt`,
 1 AS `UnearnedM32Amt`,
 1 AS `UnearnedM33Amt`,
 1 AS `UnearnedM34Amt`,
 1 AS `UnearnedM35Amt`,
 1 AS `UnearnedM36Amt`,
 1 AS `UnearnedM37Amt`,
 1 AS `UnearnedM38Amt`,
 1 AS `UnearnedM39Amt`,
 1 AS `UnearnedM40Amt`,
 1 AS `UnearnedM41Amt`,
 1 AS `UnearnedM42Amt`,
 1 AS `UnearnedM43Amt`,
 1 AS `UnearnedM44Amt`,
 1 AS `UnearnedM45Amt`,
 1 AS `UnearnedM46Amt`,
 1 AS `UnearnedM47Amt`,
 1 AS `UnearnedM48Amt`,
 1 AS `RateAreaName`,
 1 AS `StatData`,
 1 AS `CustomerRef`,
 1 AS `StateCd`,
 1 AS `CarrierGroupCd`,
 1 AS `CarrierCd`,
 1 AS `ProducerCd`,
 1 AS `BusinessSourceCd`,
 1 AS `PreviousCarrierCd`,
 1 AS `ConstructionCd`,
 1 AS `YearBuilt`,
 1 AS `SqFt`,
 1 AS `Stories`,
 1 AS `OccupancyCd`,
 1 AS `Units`,
 1 AS `ProtectionClass`,
 1 AS `TerritoryCd`,
 1 AS `TransactionCd`,
 1 AS `ControllingStateCd`,
 1 AS `ControllingProductVersionIdRef`,
 1 AS `DividendPlanCd`,
 1 AS `ConversionTemplateIdRef`,
 1 AS `ConversionGroup`,
 1 AS `ConversionJobRef`,
 1 AS `ConversionFileName`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `premiumdiscountendinfo`
--

DROP TABLE IF EXISTS `premiumdiscountendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `premiumdiscountendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `FirstPremiumDiscountLayer` int(11) DEFAULT NULL,
  `FirstPremiumDiscountPercentage` int(11) DEFAULT NULL,
  `SecondPremiumDiscountLayer` int(11) DEFAULT NULL,
  `SecondPremiumDiscountPercentage` int(11) DEFAULT NULL,
  `ThirdPremiumDiscountLayer` int(11) DEFAULT NULL,
  `ThirdPremiumDiscountPercentage` int(11) DEFAULT NULL,
  `BalancePremiumDiscountLayer` int(11) DEFAULT NULL,
  `BalancePremiumDiscountPercentage` int(11) DEFAULT NULL,
  `AveragePercentageDiscount` int(11) DEFAULT NULL,
  `EndorsementSeqNo` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `premiumdiscountendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `premiumtransactionexperience`
--

DROP TABLE IF EXISTS `premiumtransactionexperience`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `premiumtransactionexperience` (
  `SystemID` decimal(30,0) DEFAULT NULL,
  `ReportPeriod` char(6) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `BranchCd` varchar(255) DEFAULT NULL,
  `ProducerCd` varchar(255) DEFAULT NULL,
  `PolicyType` varchar(255) DEFAULT NULL,
  `ProductName` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `NewPremAmt` decimal(28,6) DEFAULT NULL,
  `NewPolicyCount` decimal(28,0) DEFAULT NULL,
  `RenlPremAmt` decimal(20,6) DEFAULT NULL,
  `RenlPolicyCount` decimal(20,0) DEFAULT NULL,
  `EndrPremAmt` decimal(20,6) DEFAULT NULL,
  `EndrPolicyCount` decimal(20,0) DEFAULT NULL,
  `CanclPremAmt` decimal(20,6) DEFAULT NULL,
  `CanclPolicyCount` decimal(20,0) DEFAULT NULL,
  `ReinPremAmt` decimal(20,6) DEFAULT NULL,
  `ReinPolicyCount` decimal(20,0) DEFAULT NULL,
  `ReWrPremAmt` decimal(20,6) DEFAULT NULL,
  `ReWrPolicyCount` decimal(20,0) DEFAULT NULL,
  `LapsePremAmt` decimal(20,6) DEFAULT NULL,
  `LapsePolicyCount` decimal(20,0) DEFAULT NULL,
  `InForcePremAmt` decimal(20,6) DEFAULT NULL,
  `InForcePolicyCount` decimal(20,0) DEFAULT NULL,
  `WorkAmt1` decimal(20,6) DEFAULT NULL,
  `WorkCount1` decimal(20,0) DEFAULT NULL,
  `WorkAmt2` decimal(20,6) DEFAULT NULL,
  `WorkCount2` decimal(20,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `printer`
--

DROP TABLE IF EXISTS `printer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `printer` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PrinterTypeCd` varchar(255) DEFAULT NULL,
  `PrimaryInd` varchar(255) DEFAULT NULL,
  `PrinterControlIdRef` varchar(255) DEFAULT NULL,
  KEY `printerIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `producerinfo`
--

DROP TABLE IF EXISTS `producerinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producerinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ProducerAgency` varchar(255) DEFAULT NULL,
  `ProducerGroup` varchar(255) DEFAULT NULL,
  `EOCarrier` varchar(255) DEFAULT NULL,
  `EOPolicyNumber` varchar(255) DEFAULT NULL,
  `EOLimit` int(11) DEFAULT NULL,
  `EOExpirationDt` date DEFAULT NULL,
  `ProducerTypeCd` varchar(255) DEFAULT NULL,
  `BranchCd` varchar(255) DEFAULT NULL,
  `AppointedDt` date DEFAULT NULL,
  `SubmitTo` varchar(255) DEFAULT NULL,
  `SubmitToCd` varchar(255) DEFAULT NULL,
  `CommissionPayMethodCd` varchar(255) DEFAULT NULL,
  `CommissionPayThroughDt` date DEFAULT NULL,
  `CommissionPayBankAccount` varchar(255) DEFAULT NULL,
  `CommissionPayBankRouteId` varchar(255) DEFAULT NULL,
  `CommissionPayBankType` varchar(255) DEFAULT NULL,
  `PayToCd` varchar(255) DEFAULT NULL,
  `CPAccountNumber` varchar(255) DEFAULT NULL,
  `CPAccountSuffix` varchar(255) DEFAULT NULL,
  `DirectPortalInd` varchar(255) DEFAULT NULL,
  KEY `producerinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productionexperiencestats`
--

DROP TABLE IF EXISTS `productionexperiencestats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productionexperiencestats` (
  `SystemId` decimal(38,0) DEFAULT NULL,
  `id` varchar(255) DEFAULT NULL,
  `StatSequence` int(11) DEFAULT NULL,
  `StatSequenceReplace` int(11) DEFAULT NULL,
  `ReportPeriod` varchar(255) DEFAULT NULL,
  `UpdateDt` date DEFAULT NULL,
  `ExperienceType` varchar(255) DEFAULT NULL,
  `ExperienceCd` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `ProducerCd` varchar(255) DEFAULT NULL,
  `BranchCd` varchar(255) DEFAULT NULL,
  `InsuranceTypeCd` varchar(255) DEFAULT NULL,
  `ExperienceCategoryCd` varchar(255) DEFAULT NULL,
  `NewRenewalCd` varchar(255) DEFAULT NULL,
  `CombinedKey` varchar(255) DEFAULT NULL,
  `MTDWrittenAmt` decimal(28,6) DEFAULT NULL,
  `LYMTDWrittenAmt` decimal(28,6) DEFAULT NULL,
  `YTDWrittenAmt` decimal(28,6) DEFAULT NULL,
  `LYTDWrittenAmt` decimal(28,6) DEFAULT NULL,
  `LYWrittenAmt` decimal(28,6) DEFAULT NULL,
  `LY2WrittenAmt` decimal(28,6) DEFAULT NULL,
  `LY3WrittenAmt` decimal(28,6) DEFAULT NULL,
  `LY4WrittenAmt` decimal(28,6) DEFAULT NULL,
  `LY5WrittenAmt` decimal(28,6) DEFAULT NULL,
  `ITDWrittenAmt` decimal(28,6) DEFAULT NULL,
  `MTDEarnedAmt` decimal(28,6) DEFAULT NULL,
  `LYMTDEarnedAmt` decimal(28,6) DEFAULT NULL,
  `YTDEarnedAmt` decimal(28,6) DEFAULT NULL,
  `LYTDEarnedAmt` decimal(28,6) DEFAULT NULL,
  `LYEarnedAmt` decimal(28,6) DEFAULT NULL,
  `LY2EarnedAmt` decimal(28,6) DEFAULT NULL,
  `LY3EarnedAmt` decimal(28,6) DEFAULT NULL,
  `LY4EarnedAmt` decimal(28,6) DEFAULT NULL,
  `LY5EarnedAmt` decimal(28,6) DEFAULT NULL,
  `ITDEarnedAmt` decimal(28,6) DEFAULT NULL,
  `MTDIndemnityIAmt` decimal(28,6) DEFAULT NULL,
  `LYMTDIndemnityIAmt` decimal(28,6) DEFAULT NULL,
  `YTDIndemnityIAmt` decimal(28,6) DEFAULT NULL,
  `LYTDIndemnityIAmt` decimal(28,6) DEFAULT NULL,
  `LYIndemnityIAmt` decimal(28,6) DEFAULT NULL,
  `LY2IndemnityIAmt` decimal(28,6) DEFAULT NULL,
  `LY3IndemnityIAmt` decimal(28,6) DEFAULT NULL,
  `LY4IndemnityIAmt` decimal(28,6) DEFAULT NULL,
  `LY5IndemnityIAmt` decimal(28,6) DEFAULT NULL,
  `ITDIndemnityIAmt` decimal(28,6) DEFAULT NULL,
  `MTDExpenseIAmt` decimal(28,6) DEFAULT NULL,
  `LYMTDExpenseIAmt` decimal(28,6) DEFAULT NULL,
  `YTDExpenseIAmt` decimal(28,6) DEFAULT NULL,
  `LYTDExpenseIAmt` decimal(28,6) DEFAULT NULL,
  `LYExpenseIAmt` decimal(28,6) DEFAULT NULL,
  `LY2ExpenseIAmt` decimal(28,6) DEFAULT NULL,
  `LY3ExpenseIAmt` decimal(28,6) DEFAULT NULL,
  `LY4ExpenseIAmt` decimal(28,6) DEFAULT NULL,
  `LY5ExpenseIAmt` decimal(28,6) DEFAULT NULL,
  `ITDExpenseIAmt` decimal(28,6) DEFAULT NULL,
  `MTDIndemnityPAmt` decimal(28,6) DEFAULT NULL,
  `LYMTDIndemnityPAmt` decimal(28,6) DEFAULT NULL,
  `YTDIndemnityPAmt` decimal(28,6) DEFAULT NULL,
  `LYTDIndemnityPAmt` decimal(28,6) DEFAULT NULL,
  `LYIndemnityPAmt` decimal(28,6) DEFAULT NULL,
  `LY2IndemnityPAmt` decimal(28,6) DEFAULT NULL,
  `LY3IndemnityPAmt` decimal(28,6) DEFAULT NULL,
  `LY4IndemnityPAmt` decimal(28,6) DEFAULT NULL,
  `LY5IndemnityPAmt` decimal(28,6) DEFAULT NULL,
  `ITDIndemnityPAmt` decimal(28,6) DEFAULT NULL,
  `MTDExpensePAmt` decimal(28,6) DEFAULT NULL,
  `LYMTDExpensePAmt` decimal(28,6) DEFAULT NULL,
  `YTDExpensePAmt` decimal(28,6) DEFAULT NULL,
  `LYTDExpensePAmt` decimal(28,6) DEFAULT NULL,
  `LYExpensePAmt` decimal(28,6) DEFAULT NULL,
  `LY2ExpensePAmt` decimal(28,6) DEFAULT NULL,
  `LY3ExpensePAmt` decimal(28,6) DEFAULT NULL,
  `LY4ExpensePAmt` decimal(28,6) DEFAULT NULL,
  `LY5ExpensePAmt` decimal(28,6) DEFAULT NULL,
  `ITDExpensePAmt` decimal(28,6) DEFAULT NULL,
  `RenewingPremium` decimal(28,6) DEFAULT NULL,
  `RenewedPremium` decimal(28,6) DEFAULT NULL,
  `RenewingCount` decimal(28,6) DEFAULT NULL,
  `RenewedCount` decimal(28,6) DEFAULT NULL,
  `RenewingPremium12MO` decimal(28,6) DEFAULT NULL,
  `RenewedPremium12MO` decimal(28,6) DEFAULT NULL,
  `RenewingCount12MO` decimal(28,6) DEFAULT NULL,
  `RenewedCount12MO` decimal(28,6) DEFAULT NULL,
  `InForcePolicyCount` decimal(28,6) DEFAULT NULL,
  `InForceCountSameMonthPY` decimal(28,6) DEFAULT NULL,
  `InForceCountEOPY` decimal(28,6) DEFAULT NULL,
  `NewBusinessPolicyCount` decimal(28,6) DEFAULT NULL,
  `RenewalPolicyCount` decimal(28,6) DEFAULT NULL,
  `NewBusinessBookedCount` decimal(28,6) DEFAULT NULL,
  `RenewalBusinessBookedCount` decimal(28,6) DEFAULT NULL,
  `NewBusinessPolicyCountYTD` decimal(28,6) DEFAULT NULL,
  `RenewalPolicyCountYTD` decimal(28,6) DEFAULT NULL,
  `NewBusinessBookedCountYTD` decimal(28,6) DEFAULT NULL,
  `RenewalBusinessBookedCountYTD` decimal(28,6) DEFAULT NULL,
  KEY `productionexperiencestats_1` (`CarrierGroupCd`,`CarrierCd`,`StateCd`,`ProducerCd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `propertydamaged`
--

DROP TABLE IF EXISTS `propertydamaged`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `propertydamaged` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PropertyDamagedNumber` int(11) DEFAULT NULL,
  `PropertyTypeCd` varchar(255) DEFAULT NULL,
  `PolicyRiskIdRef` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `DamageDesc` varchar(255) DEFAULT NULL,
  `DamageEstimateAmt` decimal(28,6) DEFAULT NULL,
  `LocationDesc` varchar(255) DEFAULT NULL,
  `VehicleLocation` varchar(255) DEFAULT NULL,
  `VehicleModelYr` varchar(255) DEFAULT NULL,
  `VehicleManufacturer` varchar(255) DEFAULT NULL,
  `VehicleModel` varchar(255) DEFAULT NULL,
  `VehicleIdentificationNumber` varchar(255) DEFAULT NULL,
  `VehicleLicenseStateProvCd` varchar(255) DEFAULT NULL,
  `VehicleLicenseNumber` varchar(255) DEFAULT NULL,
  `TotalLossInd` varchar(255) DEFAULT NULL,
  `SalvageProviderRef` int(11) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `SalvagePickupDt` date DEFAULT NULL,
  `SalvageSettledDt` date DEFAULT NULL,
  `SalvagePoolDt` date DEFAULT NULL,
  `SalvagePoolTitleDt` date DEFAULT NULL,
  `SalvagePoolLocation` varchar(255) DEFAULT NULL,
  `SalvagePoolNumber` varchar(255) DEFAULT NULL,
  `SalvageTitleReceivedDt` date DEFAULT NULL,
  `SalvageOwnerRetainedInd` varchar(255) DEFAULT NULL,
  `SalvageDeductibleWaivedInd` varchar(255) DEFAULT NULL,
  `SalvageGrossSaleAmt` decimal(28,6) DEFAULT NULL,
  `SalvageDMVRefundAmt` decimal(28,6) DEFAULT NULL,
  `SalvageLienFeeAmt` decimal(28,6) DEFAULT NULL,
  `SalvagePoolDays` int(11) DEFAULT NULL,
  `SalvageTowChargeAmt` decimal(28,6) DEFAULT NULL,
  `SalvageOtherChargeAmt` decimal(28,6) DEFAULT NULL,
  `SalvagePoolChargeAmt` decimal(28,6) DEFAULT NULL,
  `SalvageStorageChargeAmt` decimal(28,6) DEFAULT NULL,
  `SalvageTotalChargeAmt` decimal(28,6) DEFAULT NULL,
  `SalvageACVAmt` decimal(28,6) DEFAULT NULL,
  `SalvageTaxAmt` decimal(28,6) DEFAULT NULL,
  `SalvageVehicleLicenseFeeAmt` decimal(28,6) DEFAULT NULL,
  `SalvageTitleTransferFeeAmt` decimal(28,6) DEFAULT NULL,
  `SalvageCertificationFeeAmt` decimal(28,6) DEFAULT NULL,
  `SalvageTotalOutlayAmt` decimal(28,6) DEFAULT NULL,
  `SalvageNetRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `SalvageNetPct` decimal(28,6) DEFAULT NULL,
  `SalvageGrossPct` decimal(28,6) DEFAULT NULL,
  KEY `propertydamagedIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `provider`
--

DROP TABLE IF EXISTS `provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `Version` varchar(255) DEFAULT NULL,
  `IndexName` varchar(255) DEFAULT NULL,
  `ProviderTypeCd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `StatusDt` date DEFAULT NULL,
  `ProviderNumber` varchar(255) DEFAULT NULL,
  `NewProviderRef` int(11) DEFAULT NULL,
  `NewProviderDt` date DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `PaymentPreferenceCd` varchar(255) DEFAULT NULL,
  `PreferredDeliveryMethod` varchar(255) DEFAULT NULL,
  `ServicePortalContactNotes` varchar(255) DEFAULT NULL,
  `ProviderOnCommissionPlan` varchar(255) DEFAULT NULL,
  `CombinePaymentInd` varchar(255) DEFAULT NULL,
  `NotificationMethod` varchar(255) DEFAULT NULL,
  KEY `providerIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `provideracct`
--

DROP TABLE IF EXISTS `provideracct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provideracct` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `provideracctIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `provideredd`
--

DROP TABLE IF EXISTS `provideredd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provideredd` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ReportingInd` varchar(255) DEFAULT NULL,
  `ReportDt` date DEFAULT NULL,
  KEY `providereddIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `providerinfo`
--

DROP TABLE IF EXISTS `providerinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `providerinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ProviderNumber` varchar(255) DEFAULT NULL,
  `CommercialName` varchar(255) DEFAULT NULL,
  `PostingMethodCd` varchar(255) DEFAULT NULL,
  `PostDt` date DEFAULT NULL,
  KEY `providerinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `providersecurity`
--

DROP TABLE IF EXISTS `providersecurity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `providersecurity` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ProviderSecurityCd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  KEY `providersecurityIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `providerservice`
--

DROP TABLE IF EXISTS `providerservice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `providerservice` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ProviderServiceCd` varchar(255) DEFAULT NULL,
  `AreaType` varchar(255) DEFAULT NULL,
  `AreaList` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  KEY `providerserviceIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `providerstatement`
--

DROP TABLE IF EXISTS `providerstatement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `providerstatement` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `BatchId` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `UserId` varchar(255) DEFAULT NULL,
  `DepositoryLocationCd` varchar(255) DEFAULT NULL,
  `RunMonth` varchar(255) DEFAULT NULL,
  `RunYear` varchar(255) DEFAULT NULL,
  KEY `providerstatementIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `questionreplies`
--

DROP TABLE IF EXISTS `questionreplies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionreplies` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `QuestionSourceMDA` varchar(255) DEFAULT NULL,
  KEY `questionrepliesIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `questionreply`
--

DROP TABLE IF EXISTS `questionreply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionreply` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `VisibleInd` varchar(255) DEFAULT NULL,
  KEY `questionreplyIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quotecomparison`
--

DROP TABLE IF EXISTS `quotecomparison`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quotecomparison` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ComparisonQuoteIdRef` int(11) DEFAULT NULL,
  `ComparisonQuoteNumber` varchar(255) DEFAULT NULL,
  `ComparisonQuoteDescription` varchar(255) DEFAULT NULL,
  `ComparisonQuoteFinalAmt` decimal(28,6) DEFAULT NULL,
  `ComparisonQuotePremium` decimal(28,6) DEFAULT NULL,
  KEY `quotecomparisonIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quotecomparisons`
--

DROP TABLE IF EXISTS `quotecomparisons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quotecomparisons` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `quotecomparisonsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quoteinfo`
--

DROP TABLE IF EXISTS `quoteinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quoteinfo` (
  `UniqueId` int(11) NOT NULL AUTO_INCREMENT,
  `SystemId` int(11) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `ApplicationNumber` varchar(255) DEFAULT NULL,
  `CustomerRef` int(11) DEFAULT NULL,
  `CustomerNumber` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `AddUser` varchar(255) DEFAULT NULL,
  `UpdateDt` date DEFAULT NULL,
  `UpdateTm` varchar(255) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `SubmittedDt` date DEFAULT NULL,
  `FirstMailedDt` date DEFAULT NULL,
  `LastMailedDt` date DEFAULT NULL,
  `CloseReasonCd` varchar(255) DEFAULT NULL,
  `CloseSubReasonCd` varchar(255) DEFAULT NULL,
  `CloseSubReasonLabel` varchar(255) DEFAULT NULL,
  `CloseDt` date DEFAULT NULL,
  `CloseTm` varchar(255) DEFAULT NULL,
  `CloseUser` varchar(255) DEFAULT NULL,
  `CloseComment` varchar(255) DEFAULT NULL,
  `Branch` varchar(255) DEFAULT NULL,
  `BusinessSourceCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `ChangeDt` date DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `CompanyProductCd` varchar(255) DEFAULT NULL,
  `ControllingStateCd` varchar(255) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `FinalPremiumAmt` decimal(28,2) DEFAULT NULL,
  `FullTermAmt` decimal(28,2) DEFAULT NULL,
  `ExpiringPremiumAmt` decimal(28,2) DEFAULT NULL,
  `WrittenPremiumAmt` decimal(28,2) DEFAULT NULL,
  `InceptionDt` date DEFAULT NULL,
  `PayPlanCd` varchar(255) DEFAULT NULL,
  `PolicyDisplayNumber` varchar(255) DEFAULT NULL,
  `PolicyRef` int(11) DEFAULT NULL,
  `PreviousCarrierCd` varchar(255) DEFAULT NULL,
  `PreviousExpirationDt` date DEFAULT NULL,
  `PreviousPolicyNumber` varchar(255) DEFAULT NULL,
  `PreviousPremium` decimal(28,2) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `ProviderRef` int(11) DEFAULT NULL,
  `ProviderCd` varchar(255) DEFAULT NULL,
  `SubProducerCd` varchar(255) DEFAULT NULL,
  `SubTypeCd` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `TransactionCommissionAmt` decimal(28,2) DEFAULT NULL,
  `UnderwriterCd` varchar(255) DEFAULT NULL,
  `FullTermFeeAmt` decimal(28,2) DEFAULT NULL,
  `FinalFeeAmt` decimal(28,2) DEFAULT NULL,
  `ExpiringFeeAmt` decimal(28,2) DEFAULT NULL,
  `WrittenFeeAmt` decimal(28,2) DEFAULT NULL,
  `CommercialName` varchar(255) DEFAULT NULL,
  `PrimaryPhoneName` varchar(255) DEFAULT NULL,
  `PrimaryPhoneNumber` varchar(255) DEFAULT NULL,
  `SecondaryPhoneName` varchar(255) DEFAULT NULL,
  `SecondaryPhoneNumber` varchar(255) DEFAULT NULL,
  `FaxNumber` varchar(255) DEFAULT NULL,
  `EmailAddr` varchar(255) DEFAULT NULL,
  `MailingAddr1` varchar(255) DEFAULT NULL,
  `MailingAddr2` varchar(255) DEFAULT NULL,
  `MailingCity` varchar(255) DEFAULT NULL,
  `MailingStateProvCd` varchar(255) DEFAULT NULL,
  `MailingPostalCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`UniqueId`),
  KEY `quoteinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ratearea`
--

DROP TABLE IF EXISTS `ratearea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratearea` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AreaName` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `StatData` varchar(255) DEFAULT NULL,
  `FullTermAmt` decimal(28,6) DEFAULT NULL,
  `FullTermManualAmt` decimal(28,6) DEFAULT NULL,
  `FinalPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `PrevFinalAmt` decimal(28,6) DEFAULT NULL,
  `PrevWrittenAmt` decimal(28,6) DEFAULT NULL,
  `CommissionAmt` decimal(28,6) DEFAULT NULL,
  `ShortRateAmt` decimal(28,6) DEFAULT NULL,
  `TransactionCommissionAmt` decimal(28,6) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `ReinsuranceCoverageGroupCd` varchar(255) DEFAULT NULL,
  KEY `rateareaIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ratechangeendinfo`
--

DROP TABLE IF EXISTS `ratechangeendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratechangeendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `RevisedRatePolicyEffDt` int(11) DEFAULT NULL,
  `StateCovChangeFactor` int(11) DEFAULT NULL,
  `USLHWActCoveChangeFactor` int(11) DEFAULT NULL,
  `StateChangeIncOrDecCd` int(11) DEFAULT NULL,
  `USLHWChangeIncOrDecCd` int(11) DEFAULT NULL,
  `NewUSLHWActCovFactor` int(11) DEFAULT NULL,
  `ClassificationCode` int(11) DEFAULT NULL,
  `ExposureActOrCovCd01` int(11) DEFAULT NULL,
  `ManualOrChargedRate01` int(11) DEFAULT NULL,
  `ClassificationCd01` int(11) DEFAULT NULL,
  `ExposureActOrCovCd02` int(11) DEFAULT NULL,
  `ManualOrChargedRate02` int(11) DEFAULT NULL,
  `ClassificationCd02` int(11) DEFAULT NULL,
  `ExposureActOrCovCd03` int(11) DEFAULT NULL,
  `ManualOrChargedRate03` int(11) DEFAULT NULL,
  `ClassificationCd03` int(11) DEFAULT NULL,
  `ExposureActOrCovCd04` int(11) DEFAULT NULL,
  `ManualOrChargedRate04` int(11) DEFAULT NULL,
  `ClassificationCd04` int(11) DEFAULT NULL,
  `ExposureActOrCovCd05` int(11) DEFAULT NULL,
  `ManualOrChargedRate05` int(11) DEFAULT NULL,
  `ClassificationCd05` int(11) DEFAULT NULL,
  `ExposureActOrCovCd06` int(11) DEFAULT NULL,
  `ManualOrChargedRate06` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `ratechangeendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ratemessage`
--

DROP TABLE IF EXISTS `ratemessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratemessage` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  KEY `ratemessageIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ratemessages`
--

DROP TABLE IF EXISTS `ratemessages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratemessages` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `ratemessagesIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ratingmatrix`
--

DROP TABLE IF EXISTS `ratingmatrix`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratingmatrix` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AuditedPolicyInd` varchar(255) DEFAULT NULL,
  `PolicyMaxClassMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `PolicyMaxAdmiraltyFELAMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `PolicyMaxELIncreaseLimitMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `PolicyMaxWaiverOfSubrogationMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `TotalAuditEarnedPremiumAmt` decimal(28,6) DEFAULT NULL,
  `LessPreviouslyBilledPolicyPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AuditPremiumAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `TotalAuditTaxesFeeAmt` decimal(28,6) DEFAULT NULL,
  `LessPreviouslyBilledPolicyTaxesFeeAmt` decimal(28,6) DEFAULT NULL,
  `AuditTaxesFeeAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `TotalCancellationEarnedPremiumAmt` decimal(28,6) DEFAULT NULL,
  `CancellationPremiumAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `TotalCancellationTaxesFeeAmt` decimal(28,6) DEFAULT NULL,
  `CancellationTaxesFeeAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `MultiStatePremiumDiscountCalculationInd` varchar(255) DEFAULT NULL,
  `PolicyTotalStandardPremiumAmt` decimal(28,6) DEFAULT NULL,
  `PolicyTotalPremiumDiscountAmt` decimal(28,6) DEFAULT NULL,
  `AnnualTotalPolicyMinimumPremiumAmt` decimal(28,6) DEFAULT NULL,
  `StateForExpenseConstant` varchar(255) DEFAULT NULL,
  KEY `ratingmatrixIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `receiptinfo`
--

DROP TABLE IF EXISTS `receiptinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receiptinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `ReceiptDt` date DEFAULT NULL,
  `ReceiptNumber` varchar(255) DEFAULT NULL,
  `ReceiptAmt` decimal(28,6) DEFAULT NULL,
  `ReceiptId` varchar(255) DEFAULT NULL,
  `PendUnappliedInd` varchar(255) DEFAULT NULL,
  KEY `receiptinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `recentactivity`
--

DROP TABLE IF EXISTS `recentactivity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recentactivity` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ModelName` varchar(255) DEFAULT NULL,
  `SystemTimeMillis` varchar(255) DEFAULT NULL,
  `SystemIdRef` int(11) DEFAULT NULL,
  KEY `recentactivityIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `recipient`
--

DROP TABLE IF EXISTS `recipient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recipient` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TransmissionResultCd` varchar(255) DEFAULT NULL,
  KEY `recipientIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `recoveryoccurance`
--

DROP TABLE IF EXISTS `recoveryoccurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recoveryoccurance` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `RecoveryCd` varchar(255) DEFAULT NULL,
  `RecoveryAmt` varchar(255) DEFAULT NULL,
  KEY `recoveryoccuranceIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reinsreservesubject`
--

DROP TABLE IF EXISTS `reinsreservesubject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reinsreservesubject` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ReserveCd` varchar(255) DEFAULT NULL,
  `AnnualStatementLineCd` varchar(255) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `ReinsuranceCoverageGroupCd` varchar(255) DEFAULT NULL,
  `RecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ReserveAmt` decimal(28,6) DEFAULT NULL,
  `ExpectedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  KEY `reinsreservesubjectIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reinsurancerecap`
--

DROP TABLE IF EXISTS `reinsurancerecap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reinsurancerecap` (
  `ReportPeriod` char(6) DEFAULT NULL,
  `GenerationDt` date DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `AnnualStatementLine` varchar(255) DEFAULT NULL,
  `ReinsuranceName` varchar(255) DEFAULT NULL,
  `CededPremiumInForce` decimal(28,6) DEFAULT NULL,
  `CededPremiumUnearned` decimal(28,6) DEFAULT NULL,
  `CededPremiumWritten` decimal(28,6) DEFAULT NULL,
  `CededPremiumEarned` decimal(28,6) DEFAULT NULL,
  `CededCommissionAmt` decimal(28,6) DEFAULT NULL,
  `CededLossesPaid` decimal(28,6) DEFAULT NULL,
  `CededLossesIncurred` decimal(28,6) DEFAULT NULL,
  `CededLossesUnpaid` decimal(28,6) DEFAULT NULL,
  `CededAdjustmentPaid` decimal(28,6) DEFAULT NULL,
  `CededAdjustmentIncurred` decimal(28,6) DEFAULT NULL,
  `CededAdjustmentUnpaid` decimal(28,6) DEFAULT NULL,
  `CededRecovery` decimal(28,6) DEFAULT NULL,
  `DirectPremiumInForce` decimal(28,6) DEFAULT NULL,
  `DirectPremiumUnearned` decimal(28,6) DEFAULT NULL,
  `DirectPremiumWritten` decimal(28,6) DEFAULT NULL,
  `DirectPremiumEarned` decimal(28,6) DEFAULT NULL,
  `DirectCommissionAmt` decimal(28,6) DEFAULT NULL,
  `DirectLossesPaid` decimal(28,6) DEFAULT NULL,
  `DirectLossesIncurred` decimal(28,6) DEFAULT NULL,
  `DirectLossesUnpaid` decimal(28,6) DEFAULT NULL,
  `DirectAdjustmentPaid` decimal(28,6) DEFAULT NULL,
  `DirectAdjustmentIncurred` decimal(28,6) DEFAULT NULL,
  `DirectAdjustmentUnpaid` decimal(28,6) DEFAULT NULL,
  `DirectRecovery` decimal(28,6) DEFAULT NULL,
  `PercentPremiumInForce` decimal(28,6) DEFAULT NULL,
  `PercentPremiumUnearned` decimal(28,6) DEFAULT NULL,
  `PercentPremiumWritten` decimal(28,6) DEFAULT NULL,
  `PercentPremiumEarned` decimal(28,6) DEFAULT NULL,
  `PercentCommissionAmt` decimal(28,6) DEFAULT NULL,
  `PercentLossesPaid` decimal(28,6) DEFAULT NULL,
  `PercentLossesIncurred` decimal(28,6) DEFAULT NULL,
  `PercentLossesUnpaid` decimal(28,6) DEFAULT NULL,
  `PercentAdjustmentPaid` decimal(28,6) DEFAULT NULL,
  `PercentAdjustmentIncurred` decimal(28,6) DEFAULT NULL,
  `PercentAdjustmentUnpaid` decimal(28,6) DEFAULT NULL,
  `PercentRecovery` decimal(28,6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `replicationmonitor`
--

DROP TABLE IF EXISTS `replicationmonitor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `replicationmonitor` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `Counter` int(11) DEFAULT NULL,
  KEY `replicationmonitorIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reportwizard`
--

DROP TABLE IF EXISTS `reportwizard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reportwizard` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `UserId` varchar(255) DEFAULT NULL,
  `DataSet` varchar(255) DEFAULT NULL,
  `DataSource` varchar(255) DEFAULT NULL,
  `Subtitle` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `CreatedDt` date DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `ModifiedDt` date DEFAULT NULL,
  `WizardXml` varchar(255) DEFAULT NULL,
  `Visibility` varchar(255) DEFAULT NULL,
  `Roles` varchar(255) DEFAULT NULL,
  KEY `reportwizardIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reserve`
--

DROP TABLE IF EXISTS `reserve`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reserve` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ReserveCd` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `StatusDt` date DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `ReserveAmt` decimal(28,6) DEFAULT NULL,
  `RecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ExpectedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  KEY `reserveIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reserveallocation`
--

DROP TABLE IF EXISTS `reserveallocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reserveallocation` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ReserveCd` varchar(255) DEFAULT NULL,
  `PaidAmt` decimal(28,6) DEFAULT NULL,
  `ReserveAmt` decimal(28,6) DEFAULT NULL,
  `RecoveryAmt` decimal(28,6) DEFAULT NULL,
  `ExpectedRecoveryAmt` decimal(28,6) DEFAULT NULL,
  `HoldbackAmt` decimal(28,6) DEFAULT NULL,
  `SetHoldbackInd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `StatusDt` date DEFAULT NULL,
  KEY `reserveallocationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `retrospecpremendchangeinfo`
--

DROP TABLE IF EXISTS `retrospecpremendchangeinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retrospecpremendchangeinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `ExcessLossPremFactorChangeState` int(11) DEFAULT NULL,
  `ExcessLossPremFactorChangeFederal` int(11) DEFAULT NULL,
  `ExcessLossPremFactorChangeEffectiveDt` int(11) DEFAULT NULL,
  `NonRetrospecPremAmtStates` varchar(255) DEFAULT NULL,
  `RetrospecFactorChangeFirstFactor` int(11) DEFAULT NULL,
  `RetrospecFactorChangeSecondFactor` int(11) DEFAULT NULL,
  `RetrospecFactorChangeThirdFactor` int(11) DEFAULT NULL,
  `RetrospecFactorChangeEffectiveDt` int(11) DEFAULT NULL,
  `TaxMultiplierFactorChangeState` int(11) DEFAULT NULL,
  `TaxMultiplierFactorChangeFederal` int(11) DEFAULT NULL,
  `TaxMultiplierFactorChangeEffectiveDt` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `retrospecpremendchangeinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `retrospecpremendexclinfo`
--

DROP TABLE IF EXISTS `retrospecpremendexclinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retrospecpremendexclinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `ClassificationCodes` varchar(255) DEFAULT NULL,
  `EndorsementSeqNo` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `retrospecpremendexclinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `retrospecpremshorttermendinfo`
--

DROP TABLE IF EXISTS `retrospecpremshorttermendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retrospecpremshorttermendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `retrospecpremshorttermendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `retrospecpremsurchargeendinfo`
--

DROP TABLE IF EXISTS `retrospecpremsurchargeendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retrospecpremsurchargeendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `ClassificationCodeList` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `retrospecpremsurchargeendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `risk`
--

DROP TABLE IF EXISTS `risk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `risk` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `LocationRef` varchar(255) DEFAULT NULL,
  `FullTermAmt` decimal(28,6) DEFAULT NULL,
  `FinalPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `CommissionAmt` decimal(28,6) DEFAULT NULL,
  `TransactionCommissionAmt` decimal(28,6) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `FutureStatusCd` varchar(255) DEFAULT NULL,
  `PrimaryInd` varchar(255) DEFAULT NULL,
  `RiskBeanName` varchar(255) DEFAULT NULL,
  `RiskAddDt` date DEFAULT NULL,
  `RiskCancelDt` date DEFAULT NULL,
  `RiskAddPolicyVersion` varchar(255) DEFAULT NULL,
  `RiskAddTransactionNo` varchar(255) DEFAULT NULL,
  `RiskCancelPolicyVersion` varchar(255) DEFAULT NULL,
  `RiskCancelTransactionNo` varchar(255) DEFAULT NULL,
  `CoveredStateIdRef` varchar(255) DEFAULT NULL,
  `BasicPremiumAmt` decimal(28,6) DEFAULT NULL,
  `ShortDescription` varchar(255) DEFAULT NULL,
  `EffectiveDt` date DEFAULT NULL,
  `AuditAddedInd` varchar(255) DEFAULT NULL,
  KEY `riskIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskcharacteristicmod`
--

DROP TABLE IF EXISTS `riskcharacteristicmod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskcharacteristicmod` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `FundingLevel` varchar(255) DEFAULT NULL,
  `InvestmentsAInd` varchar(255) DEFAULT NULL,
  `InvestmentsBInd` varchar(255) DEFAULT NULL,
  `ExpensesInd` varchar(255) DEFAULT NULL,
  `BenefitsReducedInd` varchar(255) DEFAULT NULL,
  `OutsideProfessionals` varchar(255) DEFAULT NULL,
  `LitigationSponsorInd` varchar(255) DEFAULT NULL,
  `LitigationPlanInd` varchar(255) DEFAULT NULL,
  KEY `riskcharacteristicmodIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskdata`
--

DROP TABLE IF EXISTS `riskdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskdata` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  KEY `riskdataIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskgroup`
--

DROP TABLE IF EXISTS `riskgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskgroup` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `GroupNumber` int(11) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `GroupType` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `FutureStatusCd` varchar(255) DEFAULT NULL,
  KEY `riskgroupIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskmeterbldgchars`
--

DROP TABLE IF EXISTS `riskmeterbldgchars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskmeterbldgchars` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AddrRef` varchar(255) DEFAULT NULL,
  `Street` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL,
  `State` varchar(255) DEFAULT NULL,
  `Zip` varchar(255) DEFAULT NULL,
  `YrBuilt` varchar(255) DEFAULT NULL,
  `LivingArea` varchar(255) DEFAULT NULL,
  `FoundationSF` varchar(255) DEFAULT NULL,
  `NumStories` varchar(255) DEFAULT NULL,
  `AssessorParcelID` varchar(255) DEFAULT NULL,
  KEY `riskmeterbldgcharsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskmeterbldgcharsrs`
--

DROP TABLE IF EXISTS `riskmeterbldgcharsrs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskmeterbldgcharsrs` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `riskmeterbldgcharsrsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskmeterdistfirestat`
--

DROP TABLE IF EXISTS `riskmeterdistfirestat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskmeterdistfirestat` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AddrRef` varchar(255) DEFAULT NULL,
  `Distance` varchar(255) DEFAULT NULL,
  `InROI` varchar(255) DEFAULT NULL,
  `AcceptableDistance` varchar(255) DEFAULT NULL,
  `Station` varchar(255) DEFAULT NULL,
  `Staffing` varchar(255) DEFAULT NULL,
  KEY `riskmeterdistfirestatIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskmeterdistfirestatrs`
--

DROP TABLE IF EXISTS `riskmeterdistfirestatrs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskmeterdistfirestatrs` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `riskmeterdistfirestatrsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskmeterdisttoshore`
--

DROP TABLE IF EXISTS `riskmeterdisttoshore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskmeterdisttoshore` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AddrRef` varchar(255) DEFAULT NULL,
  `Distance` varchar(255) DEFAULT NULL,
  `UpperBound` varchar(255) DEFAULT NULL,
  `ZoneIndex` varchar(255) DEFAULT NULL,
  KEY `riskmeterdisttoshoreIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskmeterdisttoshorers`
--

DROP TABLE IF EXISTS `riskmeterdisttoshorers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskmeterdisttoshorers` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `riskmeterdisttoshorersIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskmetereqe`
--

DROP TABLE IF EXISTS `riskmetereqe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskmetereqe` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AddrRef` varchar(255) DEFAULT NULL,
  KEY `riskmetereqeIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskmetereqegrndshkng`
--

DROP TABLE IF EXISTS `riskmetereqegrndshkng`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskmetereqegrndshkng` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CdTitle` varchar(255) DEFAULT NULL,
  `ControllingFault` varchar(255) DEFAULT NULL,
  `DistanceToControllingFault` varchar(255) DEFAULT NULL,
  `SoilType` varchar(255) DEFAULT NULL,
  `LiquifactionSusceptibility` varchar(255) DEFAULT NULL,
  `Mercalli100` varchar(255) DEFAULT NULL,
  `Mercalli250` varchar(255) DEFAULT NULL,
  `Mercalli500` varchar(255) DEFAULT NULL,
  `Mercalli100Num` varchar(255) DEFAULT NULL,
  `Mercalli250Num` varchar(255) DEFAULT NULL,
  `Mercalli500Num` varchar(255) DEFAULT NULL,
  `PGA100` varchar(255) DEFAULT NULL,
  `PGA250` varchar(255) DEFAULT NULL,
  `PGA500` varchar(255) DEFAULT NULL,
  `Magnitude` varchar(255) DEFAULT NULL,
  KEY `riskmetereqegrndshkngIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskmetereqepropattr`
--

DROP TABLE IF EXISTS `riskmetereqepropattr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskmetereqepropattr` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CdTitle` varchar(255) DEFAULT NULL,
  `NumStories` varchar(255) DEFAULT NULL,
  `YearBuilt` varchar(255) DEFAULT NULL,
  `OccupancyType` varchar(255) DEFAULT NULL,
  `Coverage` varchar(255) DEFAULT NULL,
  `PerilType` varchar(255) DEFAULT NULL,
  `StructureType` varchar(255) DEFAULT NULL,
  `ReplacementValue` varchar(255) DEFAULT NULL,
  `YearUpgrade` varchar(255) DEFAULT NULL,
  `NumBuildings` varchar(255) DEFAULT NULL,
  `Legend` varchar(255) DEFAULT NULL,
  KEY `riskmetereqepropattrIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskmetereqeresponse`
--

DROP TABLE IF EXISTS `riskmetereqeresponse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskmetereqeresponse` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `riskmetereqeresponseIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskmetereqeriskscore`
--

DROP TABLE IF EXISTS `riskmetereqeriskscore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskmetereqeriskscore` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CdTitle` varchar(255) DEFAULT NULL,
  `RiskScore` varchar(255) DEFAULT NULL,
  KEY `riskmetereqeriskscoreIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskmetererrorreport`
--

DROP TABLE IF EXISTS `riskmetererrorreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskmetererrorreport` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TransactionStatus` varchar(255) DEFAULT NULL,
  `Message` varchar(255) DEFAULT NULL,
  KEY `riskmetererrorreportIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskmeterrepfrdstr`
--

DROP TABLE IF EXISTS `riskmeterrepfrdstr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskmeterrepfrdstr` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AddrRef` varchar(255) DEFAULT NULL,
  `CountyName` varchar(255) DEFAULT NULL,
  `CommunityName` varchar(255) DEFAULT NULL,
  KEY `riskmeterrepfrdstrIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskmeterrepfrdstrrs`
--

DROP TABLE IF EXISTS `riskmeterrepfrdstrrs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskmeterrepfrdstrrs` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `riskmeterrepfrdstrrsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskmetersearchcriteria`
--

DROP TABLE IF EXISTS `riskmetersearchcriteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskmetersearchcriteria` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `riskmetersearchcriteriaIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskmetertonecnty`
--

DROP TABLE IF EXISTS `riskmetertonecnty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskmetertonecnty` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AddrRef` varchar(255) DEFAULT NULL,
  `TierOne` varchar(255) DEFAULT NULL,
  KEY `riskmetertonecntyIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `riskmetertonecntyrs`
--

DROP TABLE IF EXISTS `riskmetertonecntyrs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskmetertonecntyrs` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `riskmetertonecntyrsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rolling12month`
--

DROP TABLE IF EXISTS `rolling12month`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rolling12month` (
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `ReportPeriod` char(6) DEFAULT NULL,
  `ReportStart` char(6) DEFAULT NULL,
  `ReportEnd` char(6) DEFAULT NULL,
  `PolicyType` varchar(255) DEFAULT NULL,
  `ProductName` varchar(255) DEFAULT NULL,
  `LineCd` varchar(255) DEFAULT NULL,
  `Coverage` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `PolicyVersion` varchar(255) DEFAULT NULL,
  `PolicyBOT` date DEFAULT NULL,
  `PolicyEOT` date DEFAULT NULL,
  `PolicyYear` char(4) DEFAULT NULL,
  `PolicyReportDt` char(6) DEFAULT NULL,
  `FeeCd` varchar(255) DEFAULT NULL,
  `FeeAmt` decimal(28,6) DEFAULT NULL,
  `WrittenAmt` decimal(28,6) DEFAULT NULL,
  `EarnedAmt` decimal(28,6) DEFAULT NULL,
  `EarnedPOT` decimal(8,3) DEFAULT NULL,
  `WrittenExp` decimal(28,6) DEFAULT NULL,
  `EarnedExposure` decimal(28,6) DEFAULT NULL,
  `InForcePremiumAmt` decimal(28,6) DEFAULT NULL,
  `NewRenewalCd` varchar(255) DEFAULT NULL,
  `NewPolicyCount` decimal(20,0) DEFAULT NULL,
  `RenlPolicyCount` decimal(20,0) DEFAULT NULL,
  `PolicyCount` decimal(20,0) DEFAULT NULL,
  `UnitCount` decimal(20,0) DEFAULT NULL,
  `AveragePremium` decimal(28,6) DEFAULT NULL,
  `ClaimNumber` varchar(255) DEFAULT NULL,
  `CauseOfLoss` varchar(255) DEFAULT NULL,
  `IncurredLoss` decimal(28,6) DEFAULT NULL,
  `IncurredPOT` decimal(10,3) DEFAULT NULL,
  `IncurredExpense` decimal(28,6) DEFAULT NULL,
  `ClaimCount` decimal(20,0) DEFAULT NULL,
  `NewClaimCount` decimal(20,0) DEFAULT NULL,
  `OpenClaimCount` decimal(20,0) DEFAULT NULL,
  `AverageClaim` decimal(28,6) DEFAULT NULL,
  `LossRatio` decimal(28,6) DEFAULT NULL,
  `EarnedExpPerClaim` decimal(10,3) DEFAULT NULL,
  `TableRowID` decimal(20,0) DEFAULT NULL,
  `FirstRowInd` varchar(255) DEFAULT NULL,
  `ClaimNoPolicy` decimal(20,0) DEFAULT NULL,
  `ClaimEarnedPremAmt` decimal(28,6) DEFAULT NULL,
  `ClaimEarnedExpAmt` decimal(28,6) DEFAULT NULL,
  `ThisRownum` decimal(30,0) DEFAULT NULL,
  `MaxRownum` decimal(30,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schdpaymentallocation`
--

DROP TABLE IF EXISTS `schdpaymentallocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schdpaymentallocation` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `InitialPaymentAmt` decimal(28,6) DEFAULT NULL,
  `ScheduledPaymentAmt` decimal(28,6) DEFAULT NULL,
  `FeatureIdRef` varchar(255) DEFAULT NULL,
  `ReserveIdRef` varchar(255) DEFAULT NULL,
  `RemainingPaymentAmt` decimal(28,6) DEFAULT NULL,
  `TotalPaymentAmt` decimal(28,6) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  KEY `schdpaymentallocationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schdpaymentinstallment`
--

DROP TABLE IF EXISTS `schdpaymentinstallment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schdpaymentinstallment` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PaymentDt` date DEFAULT NULL,
  `ScheduleNumber` int(11) DEFAULT NULL,
  `PaymentAmt` decimal(28,6) DEFAULT NULL,
  `ServicePeriodStartDt` date DEFAULT NULL,
  `ServicePeriodStopDt` date DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `ClaimantTransactionIdRef` varchar(255) DEFAULT NULL,
  KEY `schdpaymentinstallmentIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schedpremmoddetail`
--

DROP TABLE IF EXISTS `schedpremmoddetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedpremmoddetail` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Value` int(11) DEFAULT NULL,
  KEY `schedpremmoddetailIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schedpremmoddetails`
--

DROP TABLE IF EXISTS `schedpremmoddetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedpremmoddetails` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `SourceMDA` varchar(255) DEFAULT NULL,
  `Context` varchar(255) DEFAULT NULL,
  `Source` varchar(255) DEFAULT NULL,
  `Rationale` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `FutureStatusCd` varchar(255) DEFAULT NULL,
  KEY `schedpremmoddetailsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ScheduleTypeCd` varchar(255) DEFAULT NULL,
  KEY `scheduleIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `scheduledpayment`
--

DROP TABLE IF EXISTS `scheduledpayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scheduledpayment` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `StopDt` date DEFAULT NULL,
  `ServicePeriodStartDt` date DEFAULT NULL,
  `FrequencyCd` varchar(255) DEFAULT NULL,
  `PaymentMethodCd` varchar(255) DEFAULT NULL,
  `PaymentTypeCd` varchar(255) DEFAULT NULL,
  `ClassificationCd` varchar(255) DEFAULT NULL,
  `PaymentAccountCd` varchar(255) DEFAULT NULL,
  `PayToClaimantInd` varchar(255) DEFAULT NULL,
  `PayToName` varchar(255) DEFAULT NULL,
  `PayToProviderInd` varchar(255) DEFAULT NULL,
  `ProviderRef` int(11) DEFAULT NULL,
  `InterestCd` varchar(255) DEFAULT NULL,
  `PayToInterestInd` varchar(255) DEFAULT NULL,
  `InterestIdRef` varchar(255) DEFAULT NULL,
  `PayToOverrideInd` varchar(255) DEFAULT NULL,
  `Memo` varchar(255) DEFAULT NULL,
  `MemoCd` varchar(255) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `NumberOfPayments` int(11) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `DaysPaid` varchar(255) DEFAULT NULL,
  `WeeksPaid` varchar(255) DEFAULT NULL,
  KEY `scheduledpaymentIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schedulep`
--

DROP TABLE IF EXISTS `schedulep`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedulep` (
  `SystemId` int(11) DEFAULT NULL,
  `ReportPeriod` char(6) DEFAULT NULL,
  `GenerationDt` date DEFAULT NULL,
  `SchedPPart` varchar(255) DEFAULT NULL,
  `SchedPPartCd` varchar(255) DEFAULT NULL,
  `SchedPPartText1` varchar(255) DEFAULT NULL,
  `SchedPPartText2` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `AnnualStatementLines` varchar(255) DEFAULT NULL,
  `Year1Text` char(4) DEFAULT NULL,
  `Year2Text` char(4) DEFAULT NULL,
  `Year3Text` char(4) DEFAULT NULL,
  `Year4Text` char(4) DEFAULT NULL,
  `Year5Text` char(4) DEFAULT NULL,
  `Year6Text` char(4) DEFAULT NULL,
  `Year7Text` char(4) DEFAULT NULL,
  `Year8Text` char(4) DEFAULT NULL,
  `Year9Text` char(4) DEFAULT NULL,
  `Year10Text` char(4) DEFAULT NULL,
  `Work1Text` varchar(225) DEFAULT NULL,
  `Work2Text` varchar(225) DEFAULT NULL,
  `PriorYears` char(20) DEFAULT NULL,
  `PriorYR1` decimal(28,0) DEFAULT NULL,
  `PriorYR2` decimal(28,0) DEFAULT NULL,
  `PriorYR3` decimal(28,0) DEFAULT NULL,
  `PriorYR4` decimal(28,0) DEFAULT NULL,
  `PriorYR5` decimal(28,0) DEFAULT NULL,
  `PriorYR6` decimal(28,0) DEFAULT NULL,
  `PriorYR7` decimal(28,0) DEFAULT NULL,
  `PriorYR8` decimal(28,0) DEFAULT NULL,
  `PriorYR9` decimal(28,0) DEFAULT NULL,
  `PriorYR10` decimal(28,0) DEFAULT NULL,
  `PriorWK1` decimal(28,0) DEFAULT NULL,
  `PriorWK2` decimal(28,0) DEFAULT NULL,
  `CurrentYRMinus9` char(4) DEFAULT NULL,
  `CYM9YR1` decimal(28,0) DEFAULT NULL,
  `CYM9YR2` decimal(28,0) DEFAULT NULL,
  `CYM9YR3` decimal(28,0) DEFAULT NULL,
  `CYM9YR4` decimal(28,0) DEFAULT NULL,
  `CYM9YR5` decimal(28,0) DEFAULT NULL,
  `CYM9YR6` decimal(28,0) DEFAULT NULL,
  `CYM9YR7` decimal(28,0) DEFAULT NULL,
  `CYM9YR8` decimal(28,0) DEFAULT NULL,
  `CYM9YR9` decimal(28,0) DEFAULT NULL,
  `CYM9YR10` decimal(28,0) DEFAULT NULL,
  `CYM9WK1` decimal(28,0) DEFAULT NULL,
  `CYM9WK2` decimal(28,0) DEFAULT NULL,
  `CurrentYRMinus8` char(4) DEFAULT NULL,
  `CYM8YR1` decimal(28,0) DEFAULT NULL,
  `CYM8YR2` decimal(28,0) DEFAULT NULL,
  `CYM8YR3` decimal(28,0) DEFAULT NULL,
  `CYM8YR4` decimal(28,0) DEFAULT NULL,
  `CYM8YR5` decimal(28,0) DEFAULT NULL,
  `CYM8YR6` decimal(28,0) DEFAULT NULL,
  `CYM8YR7` decimal(28,0) DEFAULT NULL,
  `CYM8YR8` decimal(28,0) DEFAULT NULL,
  `CYM8YR9` decimal(28,0) DEFAULT NULL,
  `CYM8YR10` decimal(28,0) DEFAULT NULL,
  `CYM8WK1` decimal(28,0) DEFAULT NULL,
  `CYM8WK2` decimal(28,0) DEFAULT NULL,
  `CurrentYRMinus7` char(4) DEFAULT NULL,
  `CYM7YR1` decimal(28,0) DEFAULT NULL,
  `CYM7YR2` decimal(28,0) DEFAULT NULL,
  `CYM7YR3` decimal(28,0) DEFAULT NULL,
  `CYM7YR4` decimal(28,0) DEFAULT NULL,
  `CYM7YR5` decimal(28,0) DEFAULT NULL,
  `CYM7YR6` decimal(28,0) DEFAULT NULL,
  `CYM7YR7` decimal(28,0) DEFAULT NULL,
  `CYM7YR8` decimal(28,0) DEFAULT NULL,
  `CYM7YR9` decimal(28,0) DEFAULT NULL,
  `CYM7YR10` decimal(28,0) DEFAULT NULL,
  `CYM7WK1` decimal(28,0) DEFAULT NULL,
  `CYM7WK2` decimal(28,0) DEFAULT NULL,
  `CurrentYRMinus6` char(4) DEFAULT NULL,
  `CYM6YR1` decimal(28,0) DEFAULT NULL,
  `CYM6YR2` decimal(28,0) DEFAULT NULL,
  `CYM6YR3` decimal(28,0) DEFAULT NULL,
  `CYM6YR4` decimal(28,0) DEFAULT NULL,
  `CYM6YR5` decimal(28,0) DEFAULT NULL,
  `CYM6YR6` decimal(28,0) DEFAULT NULL,
  `CYM6YR7` decimal(28,0) DEFAULT NULL,
  `CYM6YR8` decimal(28,0) DEFAULT NULL,
  `CYM6YR9` decimal(28,0) DEFAULT NULL,
  `CYM6YR10` decimal(28,0) DEFAULT NULL,
  `CYM6WK1` decimal(28,0) DEFAULT NULL,
  `CYM6WK2` decimal(28,0) DEFAULT NULL,
  `CurrentYRMinus5` char(4) DEFAULT NULL,
  `CYM5YR1` decimal(28,0) DEFAULT NULL,
  `CYM5YR2` decimal(28,0) DEFAULT NULL,
  `CYM5YR3` decimal(28,0) DEFAULT NULL,
  `CYM5YR4` decimal(28,0) DEFAULT NULL,
  `CYM5YR5` decimal(28,0) DEFAULT NULL,
  `CYM5YR6` decimal(28,0) DEFAULT NULL,
  `CYM5YR7` decimal(28,0) DEFAULT NULL,
  `CYM5YR8` decimal(28,0) DEFAULT NULL,
  `CYM5YR9` decimal(28,0) DEFAULT NULL,
  `CYM5YR10` decimal(28,0) DEFAULT NULL,
  `CYM5WK1` decimal(28,0) DEFAULT NULL,
  `CYM5WK2` decimal(28,0) DEFAULT NULL,
  `CurrentYRMinus4` char(4) DEFAULT NULL,
  `CYM4YR1` decimal(28,0) DEFAULT NULL,
  `CYM4YR2` decimal(28,0) DEFAULT NULL,
  `CYM4YR3` decimal(28,0) DEFAULT NULL,
  `CYM4YR4` decimal(28,0) DEFAULT NULL,
  `CYM4YR5` decimal(28,0) DEFAULT NULL,
  `CYM4YR6` decimal(28,0) DEFAULT NULL,
  `CYM4YR7` decimal(28,0) DEFAULT NULL,
  `CYM4YR8` decimal(28,0) DEFAULT NULL,
  `CYM4YR9` decimal(28,0) DEFAULT NULL,
  `CYM4YR10` decimal(28,0) DEFAULT NULL,
  `CYM4WK1` decimal(28,0) DEFAULT NULL,
  `CYM4WK2` decimal(28,0) DEFAULT NULL,
  `CurrentYRMinus3` char(4) DEFAULT NULL,
  `CYM3YR1` decimal(28,0) DEFAULT NULL,
  `CYM3YR2` decimal(28,0) DEFAULT NULL,
  `CYM3YR3` decimal(28,0) DEFAULT NULL,
  `CYM3YR4` decimal(28,0) DEFAULT NULL,
  `CYM3YR5` decimal(28,0) DEFAULT NULL,
  `CYM3YR6` decimal(28,0) DEFAULT NULL,
  `CYM3YR7` decimal(28,0) DEFAULT NULL,
  `CYM3YR8` decimal(28,0) DEFAULT NULL,
  `CYM3YR9` decimal(28,0) DEFAULT NULL,
  `CYM3YR10` decimal(28,0) DEFAULT NULL,
  `CYM3WK1` decimal(28,0) DEFAULT NULL,
  `CYM3WK2` decimal(28,0) DEFAULT NULL,
  `CurrentYRMinus2` char(4) DEFAULT NULL,
  `CYM2YR1` decimal(28,0) DEFAULT NULL,
  `CYM2YR2` decimal(28,0) DEFAULT NULL,
  `CYM2YR3` decimal(28,0) DEFAULT NULL,
  `CYM2YR4` decimal(28,0) DEFAULT NULL,
  `CYM2YR5` decimal(28,0) DEFAULT NULL,
  `CYM2YR6` decimal(28,0) DEFAULT NULL,
  `CYM2YR7` decimal(28,0) DEFAULT NULL,
  `CYM2YR8` decimal(28,0) DEFAULT NULL,
  `CYM2YR9` decimal(28,0) DEFAULT NULL,
  `CYM2YR10` decimal(28,0) DEFAULT NULL,
  `CYM2WK1` decimal(28,0) DEFAULT NULL,
  `CYM2WK2` decimal(28,0) DEFAULT NULL,
  `CurrentYRMinus1` char(4) DEFAULT NULL,
  `CYM1YR1` decimal(28,0) DEFAULT NULL,
  `CYM1YR2` decimal(28,0) DEFAULT NULL,
  `CYM1YR3` decimal(28,0) DEFAULT NULL,
  `CYM1YR4` decimal(28,0) DEFAULT NULL,
  `CYM1YR5` decimal(28,0) DEFAULT NULL,
  `CYM1YR6` decimal(28,0) DEFAULT NULL,
  `CYM1YR7` decimal(28,0) DEFAULT NULL,
  `CYM1YR8` decimal(28,0) DEFAULT NULL,
  `CYM1YR9` decimal(28,0) DEFAULT NULL,
  `CYM1YR10` decimal(28,0) DEFAULT NULL,
  `CYM1WK1` decimal(28,0) DEFAULT NULL,
  `CYM1WK2` decimal(28,0) DEFAULT NULL,
  `CurrentYR` char(4) DEFAULT NULL,
  `CYYR1` decimal(28,0) DEFAULT NULL,
  `CYYR2` decimal(28,0) DEFAULT NULL,
  `CYYR3` decimal(28,0) DEFAULT NULL,
  `CYYR4` decimal(28,0) DEFAULT NULL,
  `CYYR5` decimal(28,0) DEFAULT NULL,
  `CYYR6` decimal(28,0) DEFAULT NULL,
  `CYYR7` decimal(28,0) DEFAULT NULL,
  `CYYR8` decimal(28,0) DEFAULT NULL,
  `CYYR9` decimal(28,0) DEFAULT NULL,
  `CYYR10` decimal(28,0) DEFAULT NULL,
  `CYWK1` decimal(28,0) DEFAULT NULL,
  `CYWK2` decimal(28,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schedulepdriver`
--

DROP TABLE IF EXISTS `schedulepdriver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedulepdriver` (
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `SchedPPart` varchar(255) DEFAULT NULL,
  `SchedPPartCd` varchar(255) DEFAULT NULL,
  `SchedPPartText1` varchar(255) DEFAULT NULL,
  `SchedPPartText2` varchar(255) DEFAULT NULL,
  `ASLineWK` varchar(255) DEFAULT NULL,
  `ASLine` varchar(255) DEFAULT NULL,
  UNIQUE KEY `schedulepdriver_01` (`CarrierGroupCd`,`CarrierCd`,`StateCd`(100),`SchedPPart`(100),`SchedPPartCd`(100),`ASLineWK`(100))
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `selfservicecontact`
--

DROP TABLE IF EXISTS `selfservicecontact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selfservicecontact` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `AllowProducerOverrideInd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `SelfServiceContactIdRef` varchar(255) DEFAULT NULL,
  KEY `selfservicecontactIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `selfservicefeature`
--

DROP TABLE IF EXISTS `selfservicefeature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selfservicefeature` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `Constraints` varchar(255) DEFAULT NULL,
  `AllowProducerOverrideInd` varchar(255) DEFAULT NULL,
  `ProducerOverrideConstraints` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `SelfServiceFeatureIdRef` varchar(255) DEFAULT NULL,
  `RequiredProductTypesMDA` varchar(255) DEFAULT NULL,
  `ExcludedProductTypesMDA` varchar(255) DEFAULT NULL,
  `FeaturePriority` int(11) DEFAULT NULL,
  `RiskBeanNameRequired` varchar(255) DEFAULT NULL,
  KEY `selfservicefeatureIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `senderinfo`
--

DROP TABLE IF EXISTS `senderinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `senderinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `EmailAddr` varchar(255) DEFAULT NULL,
  `PhoneNumber` varchar(255) DEFAULT NULL,
  `FaxNumber` varchar(255) DEFAULT NULL,
  KEY `senderinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `serviceitem`
--

DROP TABLE IF EXISTS `serviceitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `serviceitem` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ServiceDt` date DEFAULT NULL,
  `EmployedFTPT` varchar(255) DEFAULT NULL,
  `AttendingSchoolFTPT` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  KEY `serviceitemIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `serviceitems`
--

DROP TABLE IF EXISTS `serviceitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `serviceitems` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `serviceitemsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `softechbatchinfo`
--

DROP TABLE IF EXISTS `softechbatchinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `softechbatchinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Key` varchar(255) DEFAULT NULL,
  `RequestStatusCd` varchar(255) DEFAULT NULL,
  `RequestStatusDisplay` varchar(255) DEFAULT NULL,
  `RequestDt` date DEFAULT NULL,
  KEY `softechbatchinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `softechdriver`
--

DROP TABLE IF EXISTS `softechdriver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `softechdriver` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `BirthDt` date DEFAULT NULL,
  `Height` varchar(255) DEFAULT NULL,
  `Weight` varchar(255) DEFAULT NULL,
  `EyeColor` varchar(255) DEFAULT NULL,
  `HairColor` varchar(255) DEFAULT NULL,
  `GenderCd` varchar(255) DEFAULT NULL,
  `RaceCd` varchar(255) DEFAULT NULL,
  `Alias` varchar(255) DEFAULT NULL,
  `SSN` varchar(255) DEFAULT NULL,
  KEY `softechdriverIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `softechdriverlicense`
--

DROP TABLE IF EXISTS `softechdriverlicense`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `softechdriverlicense` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `DriverLicenseTypeCd` varchar(255) DEFAULT NULL,
  `DriverLicenseNumber` varchar(255) DEFAULT NULL,
  `DriverLicenseStateProvCd` varchar(255) DEFAULT NULL,
  `IssueDt` date DEFAULT NULL,
  `ExpirationDt` date DEFAULT NULL,
  `ClassCd` varchar(255) DEFAULT NULL,
  `ClassDescription` varchar(255) DEFAULT NULL,
  `StatusDescription` varchar(255) DEFAULT NULL,
  `TypeDescription` varchar(255) DEFAULT NULL,
  `RestrictionCd` varchar(255) DEFAULT NULL,
  `RestrictionDescription` varchar(255) DEFAULT NULL,
  `EndorsementCd` varchar(255) DEFAULT NULL,
  `EndorsementDescription` varchar(255) DEFAULT NULL,
  KEY `softechdriverlicenseIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `softechdrivingviolation`
--

DROP TABLE IF EXISTS `softechdrivingviolation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `softechdrivingviolation` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ViolationNumber` int(11) DEFAULT NULL,
  `IncidentNumber` int(11) DEFAULT NULL,
  `ViolationTypeCd` varchar(255) DEFAULT NULL,
  `ViolationOrSuspensionDt` date DEFAULT NULL,
  `ConvictionOrReinstatementDt` date DEFAULT NULL,
  `Points` varchar(255) DEFAULT NULL,
  `StateViolationCd` varchar(255) DEFAULT NULL,
  `StandardViolationCd` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OutOfStateInd` varchar(255) DEFAULT NULL,
  KEY `softechdrivingviolationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `softecherrorreport`
--

DROP TABLE IF EXISTS `softecherrorreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `softecherrorreport` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ErrorCd` varchar(255) DEFAULT NULL,
  `Message` varchar(255) DEFAULT NULL,
  KEY `softecherrorreportIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `softechmvrreport`
--

DROP TABLE IF EXISTS `softechmvrreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `softechmvrreport` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ProcessingStatusCd` varchar(255) DEFAULT NULL,
  `ProcessingStatusDisplay` varchar(255) DEFAULT NULL,
  `ProcessDt` date DEFAULT NULL,
  `SoftechAccountNumber` varchar(255) DEFAULT NULL,
  `DriverLicenseStateProvCd` varchar(255) DEFAULT NULL,
  `DriverLicenseNumber` varchar(255) DEFAULT NULL,
  KEY `softechmvrreportIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `softechmvrsearchcriteria`
--

DROP TABLE IF EXISTS `softechmvrsearchcriteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `softechmvrsearchcriteria` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `softechmvrsearchcriteriaIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `softechsearchsubject`
--

DROP TABLE IF EXISTS `softechsearchsubject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `softechsearchsubject` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `SearchSubjectTypeCd` varchar(255) DEFAULT NULL,
  `GivenName` varchar(255) DEFAULT NULL,
  `OtherGivenName` varchar(255) DEFAULT NULL,
  `Surname` varchar(255) DEFAULT NULL,
  `GenderCd` varchar(255) DEFAULT NULL,
  `DriverLicenseNumber` varchar(255) DEFAULT NULL,
  `DriverLicenseStateProvCd` varchar(255) DEFAULT NULL,
  `BirthDt` date DEFAULT NULL,
  `SSN` varchar(255) DEFAULT NULL,
  KEY `softechsearchsubjectIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `solepropcovendinfo`
--

DROP TABLE IF EXISTS `solepropcovendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solepropcovendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd2` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `DescriptorCd2` varchar(255) DEFAULT NULL,
  `NameofPersonIncluded2` varchar(255) DEFAULT NULL,
  `DescriptorCd0` varchar(255) DEFAULT NULL,
  `NameofPersonIncluded0` varchar(255) DEFAULT NULL,
  `StateCd0` int(11) DEFAULT NULL,
  `DescriptorCd1` varchar(255) DEFAULT NULL,
  `NameofPersonIncluded1` varchar(255) DEFAULT NULL,
  `StateCd1` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `solepropcovendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sroiclaiminfo`
--

DROP TABLE IF EXISTS `sroiclaiminfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sroiclaiminfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ClaimNo` varchar(255) DEFAULT NULL,
  `TransactionSetID` varchar(255) DEFAULT NULL,
  `PreExistingDisabilityCd` varchar(255) DEFAULT NULL,
  `MedicalImprovementDt` varchar(255) DEFAULT NULL,
  `AgreementtoCompensateCd` varchar(255) DEFAULT NULL,
  `EmployeeRepresentationDt` varchar(255) DEFAULT NULL,
  KEY `sroiclaiminfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sroicompanioninfo`
--

DROP TABLE IF EXISTS `sroicompanioninfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sroicompanioninfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ClaimNo` varchar(255) DEFAULT NULL,
  `TransactionSetID` varchar(255) DEFAULT NULL,
  `EmployeeEducationLevel` varchar(255) DEFAULT NULL,
  `AnticipatedWageLossInd` varchar(255) DEFAULT NULL,
  `ReducedBenefitAmtCd` varchar(255) DEFAULT NULL,
  `NonConsecutivePeriodCd` varchar(255) DEFAULT NULL,
  `LastDayWorkedDt` varchar(255) DEFAULT NULL,
  `SuspensionEffectiveDt` varchar(255) DEFAULT NULL,
  `WeeklyCompensationAmt` varchar(255) DEFAULT NULL,
  `LumpSumPaymentCd` varchar(255) DEFAULT NULL,
  `AverageWage` varchar(255) DEFAULT NULL,
  `InitialDtofLossTime` varchar(255) DEFAULT NULL,
  `AwardOrderTime` varchar(255) DEFAULT NULL,
  `AllOccurance` varchar(255) DEFAULT NULL,
  KEY `sroicompanioninfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `statefee`
--

DROP TABLE IF EXISTS `statefee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statefee` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StatusCx` varchar(255) DEFAULT NULL,
  `CategoryCd` varchar(255) DEFAULT NULL,
  `Value` decimal(28,6) DEFAULT NULL,
  KEY `statefeeIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `statehistory`
--

DROP TABLE IF EXISTS `statehistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statehistory` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `statehistoryIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `statehistoryitem`
--

DROP TABLE IF EXISTS `statehistoryitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statehistoryitem` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `BeforeTransactionNumber` int(11) DEFAULT NULL,
  KEY `statehistoryitemIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stateinfo`
--

DROP TABLE IF EXISTS `stateinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stateinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CarrierGroup` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `AppointedDt` date DEFAULT NULL,
  `CancelledDt` date DEFAULT NULL,
  `LicensedDt` date DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  KEY `stateinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `statementaccount`
--

DROP TABLE IF EXISTS `statementaccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statementaccount` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `StatementAccountNumber` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `StatementPayPlanCd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `EntityTypeCd` varchar(255) DEFAULT NULL,
  `PaymentDay` varchar(255) DEFAULT NULL,
  `PreferredDeliveryMethod` varchar(255) DEFAULT NULL,
  `IndexName` varchar(255) DEFAULT NULL,
  `AllowPoliciesToBeAddedInd` varchar(255) DEFAULT NULL,
  `FeePayPlanTemplateIdRef` varchar(255) DEFAULT NULL,
  `StatementVersion` int(11) DEFAULT NULL,
  `StatementAccountDisplayNumber` varchar(255) DEFAULT NULL,
  `StatementFeeAccountRef` int(11) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  KEY `statementaccountIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `statementaccountcustomer`
--

DROP TABLE IF EXISTS `statementaccountcustomer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statementaccountcustomer` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CustomerRef` varchar(255) DEFAULT NULL,
  KEY `statementaccountcustomerIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `statementsummary`
--

DROP TABLE IF EXISTS `statementsummary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statementsummary` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `GrossPremiumAmt` decimal(28,6) DEFAULT NULL,
  `CommissionAmt` decimal(28,6) DEFAULT NULL,
  `NetPremiumAmt` decimal(28,6) DEFAULT NULL,
  `NonPremiumAmt` decimal(28,6) DEFAULT NULL,
  `TotalOpenAmt` decimal(28,6) DEFAULT NULL,
  `AppliedAmt` decimal(28,6) DEFAULT NULL,
  `UnappliedAmt` decimal(28,6) DEFAULT NULL,
  KEY `statementsummaryIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stateperiod`
--

DROP TABLE IF EXISTS `stateperiod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stateperiod` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ARDPeriodInd` varchar(255) DEFAULT NULL,
  `NoncomplianceChargeAmt` decimal(28,6) DEFAULT NULL,
  `StatusCx` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `RatingEffectiveDt` date DEFAULT NULL,
  `NumDays` int(11) DEFAULT NULL,
  `ProRateFactor` decimal(28,6) DEFAULT NULL,
  `ShortRateFactor` decimal(28,6) DEFAULT NULL,
  `PeriodRatio` decimal(28,6) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `ReasonCd` varchar(255) DEFAULT NULL,
  `PriorityBalanceMinPrem` int(11) DEFAULT NULL,
  `PriorityWOSAdj` int(11) DEFAULT NULL,
  `PriorityELAdj` int(11) DEFAULT NULL,
  `PriorityAdmiraltyAdj` int(11) DEFAULT NULL,
  `FinalPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AdmiraltyFELAIncrLiabilityLimitMinPremiumAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `AdmiraltyFELAIncrLiabilityLimitPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AdmiraltyFELAIncrLiabilityLimitMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `CancellationAdmiraltyFELAIncrLiabilityLimitMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AdmiraltyFELAIncrLiabilityLimitProgramIMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AdmiraltyFELAIncrLiabilityLimitProgramIIMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AdmiraltyFELALiabilityLimits` varchar(255) DEFAULT NULL,
  `AlcoholDrugFreeCreditAmt` decimal(28,6) DEFAULT NULL,
  `AlcoholDrugFreeCreditFactor` decimal(28,6) DEFAULT NULL,
  `AlcoholDrugFreeCreditInd` varchar(255) DEFAULT NULL,
  `AtomicEnergyRadiationPremiumAmt` decimal(28,6) DEFAULT NULL,
  `BalanceToPolicyMinimumPremiumAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheAmt` decimal(28,6) DEFAULT NULL,
  `CatastropheFactor` decimal(28,6) DEFAULT NULL,
  `ClassCodeMinimumPremiumAmt` decimal(28,6) DEFAULT NULL,
  `CancellationClassCodeMinimumPremiumAmt` decimal(28,6) DEFAULT NULL,
  `AdmiraltyFELAMinPremAdditionalToClassCodeMinPremium` decimal(28,6) DEFAULT NULL,
  `CoalMineDiseasePremiumAmt` decimal(28,6) DEFAULT NULL,
  `ContrClassPremAdjCreditAmt` decimal(28,6) DEFAULT NULL,
  `ContrClassPremAdjCreditFactor` decimal(28,6) DEFAULT NULL,
  `ContrClassPremAdjCreditPct` decimal(28,6) DEFAULT NULL,
  `WorkfareProgramWeeksNum` varchar(255) DEFAULT NULL,
  `WorkfareProgramEmployeesExposureAmt` decimal(28,6) DEFAULT NULL,
  `LargeDeductibleFactor` decimal(28,6) DEFAULT NULL,
  `LargeDeductibleCreditAmt` decimal(28,6) DEFAULT NULL,
  `CoinsuranceInd` varchar(255) DEFAULT NULL,
  `CoinsuranceDiscountAmt` decimal(28,6) DEFAULT NULL,
  `CoinsuranceDiscountFactor` decimal(28,6) DEFAULT NULL,
  `DeductibleAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleDiscountAmt` decimal(28,6) DEFAULT NULL,
  `DeductibleDiscountFactor` decimal(28,6) DEFAULT NULL,
  `DeductibleType` varchar(255) DEFAULT NULL,
  `ELVoluntaryCompensationAmt` decimal(28,6) DEFAULT NULL,
  `ELVoluntaryCompensationInd` varchar(255) DEFAULT NULL,
  `EmployersLiabilityIncrLimitFactor` decimal(28,6) DEFAULT NULL,
  `EmployersLiabilityIncrLimitMinPremiumAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `EmployersLiabilityIncrLimitMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `CancellationEmployersLiabilityIncrLimitMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `EmployersLiabilityIncrLimitPremiumAmt` decimal(28,6) DEFAULT NULL,
  `EmployersLiabilityLimits` varchar(255) DEFAULT NULL,
  `EstimatedTotalPeriodPremiumAmt` decimal(28,6) DEFAULT NULL,
  `EstimatedTotalPeriodPremiumInclTaxesFeesAssessmentsAmt` decimal(28,6) DEFAULT NULL,
  `ExpenseConstantAmt` decimal(28,6) DEFAULT NULL,
  `ExperienceModificationAmt` decimal(28,6) DEFAULT NULL,
  `ExperienceModificationFactor` decimal(28,6) DEFAULT NULL,
  `ExperienceRatingCd` varchar(255) DEFAULT NULL,
  `ExperienceModificationStatus` varchar(255) DEFAULT NULL,
  `FederalActFactor` decimal(28,6) DEFAULT NULL,
  `FederalActPremiumAmt` decimal(28,6) DEFAULT NULL,
  `FederalActsLiabilityLimits` varchar(255) DEFAULT NULL,
  `ManagedCareCreditAmt` decimal(28,6) DEFAULT NULL,
  `ManagedCareCreditFactor` decimal(28,6) DEFAULT NULL,
  `ManagedCareCreditInd` varchar(255) DEFAULT NULL,
  `MeritRatingCreditAmt` decimal(28,6) DEFAULT NULL,
  `MeritRatingFactor` decimal(28,6) DEFAULT NULL,
  `NumberOfClaims` decimal(28,6) DEFAULT NULL,
  `NewClassCount` int(11) DEFAULT NULL,
  `NonRatableCatastropheLoadingPremiumAmt` decimal(28,6) DEFAULT NULL,
  `PeriodFeeCIGAAmt` decimal(28,6) DEFAULT NULL,
  `PeriodFeeLECAmt` decimal(28,6) DEFAULT NULL,
  `PeriodFeeOSHFAmt` decimal(28,6) DEFAULT NULL,
  `PeriodFeeSIBTAmt` decimal(28,6) DEFAULT NULL,
  `PeriodFeeUEBTAmt` decimal(28,6) DEFAULT NULL,
  `PeriodFeeWCAAmt` decimal(28,6) DEFAULT NULL,
  `PeriodFeeWCFAAmt` decimal(28,6) DEFAULT NULL,
  `PremiumDiscountAmt` decimal(28,6) DEFAULT NULL,
  `PremiumDiscountFactor` decimal(28,6) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `CertifiedSafetyHealthProgramAmt` decimal(28,6) DEFAULT NULL,
  `CertifiedSafetyHealthProgramPct` decimal(28,6) DEFAULT NULL,
  `CertifiedSafetyHealthProgramFactor` decimal(28,6) DEFAULT NULL,
  `ScheduleRatingAmt` decimal(28,6) DEFAULT NULL,
  `ScheduleRatingFactor` decimal(28,6) DEFAULT NULL,
  `GLScheduledPremiumMod` decimal(28,6) DEFAULT NULL,
  `SpecificDiseasePremiumAmt` decimal(28,6) DEFAULT NULL,
  `SubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SupplementalDiseaseLoadingPremiumAmt` decimal(28,6) DEFAULT NULL,
  `SupplementaryDiseasePremiumAmt` decimal(28,6) DEFAULT NULL,
  `TerrorismAmt` decimal(28,6) DEFAULT NULL,
  `TerrorismFactor` decimal(28,6) DEFAULT NULL,
  `TotalDiscountedPremiumAmt` decimal(28,6) DEFAULT NULL,
  `TotalManualPremiumAmt` decimal(28,6) DEFAULT NULL,
  `TotalStandardPremiumAmt` decimal(28,6) DEFAULT NULL,
  `TotalStandardPremiumWithoutMinPremAmt` decimal(28,6) DEFAULT NULL,
  `TotalSubjectPremiumAmt` decimal(28,6) DEFAULT NULL,
  `USLHWActPremiumAmt` decimal(28,6) DEFAULT NULL,
  `USLHRuleFactor` decimal(28,6) DEFAULT NULL,
  `WaiverOfSubrogationFactor` decimal(28,6) DEFAULT NULL,
  `WaiverSubrogationInd` varchar(255) DEFAULT NULL,
  `WaiverOfSubrogationMinPremiumAdjustmentAmt` decimal(28,6) DEFAULT NULL,
  `WaiverOfSubrogationMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `CancellationWaiverOfSubrogationMinPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WaiverOfSubrogationPremiumAmt` decimal(28,6) DEFAULT NULL,
  `PremiumAfterPackageCreditsAmt` decimal(28,6) DEFAULT NULL,
  `PremiumAfterExpModMeritRateAmt` decimal(28,6) DEFAULT NULL,
  `EmployerAssessmentAmt` decimal(28,6) DEFAULT NULL,
  `OperationsFundSurchargeAmt` decimal(28,6) DEFAULT NULL,
  `OperationsFundSurchargeFactor` decimal(28,6) DEFAULT NULL,
  `WaiverOfSubrogationFlatInd` varchar(255) DEFAULT NULL,
  `PackageFactorInd` varchar(255) DEFAULT NULL,
  KEY `stateperiodIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `statepremchangeinfo`
--

DROP TABLE IF EXISTS `statepremchangeinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statepremchangeinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `ExpModPlanTypeCd` int(11) DEFAULT NULL,
  `RiskRatingFactor` int(11) DEFAULT NULL,
  `InsurerPremDeviationFactor` int(11) DEFAULT NULL,
  `InsurerPremDeviationTypeCd` int(11) DEFAULT NULL,
  `EstStateStdPremTot` int(11) DEFAULT NULL,
  `ExpenseConstantAmt` int(11) DEFAULT NULL,
  `LossConstantAmt` int(11) DEFAULT NULL,
  `PremiumDiscountAmt` int(11) DEFAULT NULL,
  `ProratedExpConstantAmtRsnCd` int(11) DEFAULT NULL,
  `ProratedMinPremAmtRsnCd` int(11) DEFAULT NULL,
  `ReasonForStateAddedPolicyCd` int(11) DEFAULT NULL,
  `PreviousExpModEffectiveDt` int(11) DEFAULT NULL,
  `PreviousAnniversaryRatingDt` int(11) DEFAULT NULL,
  `ARAPFactor` int(11) DEFAULT NULL,
  `TypeofNonStdCd` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `statepremchangeinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stationery`
--

DROP TABLE IF EXISTS `stationery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stationery` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  KEY `stationeryIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `storagebin`
--

DROP TABLE IF EXISTS `storagebin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storagebin` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `ServerType` varchar(255) DEFAULT NULL,
  `HostPath` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `UserName` varchar(255) DEFAULT NULL,
  `DirectoryPath` varchar(255) DEFAULT NULL,
  `BinName` varchar(255) DEFAULT NULL,
  `ExternalStorageInd` varchar(255) DEFAULT NULL,
  KEY `storagebinIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `storagefile`
--

DROP TABLE IF EXISTS `storagefile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storagefile` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `StorageBinRef` int(11) DEFAULT NULL,
  `DirectoryPath` varchar(255) DEFAULT NULL,
  `Filename` varchar(255) DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `Filetype` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `UpdateDt` date DEFAULT NULL,
  `Filesize` int(11) DEFAULT NULL,
  `UpdateTm` varchar(255) DEFAULT NULL,
  `Checksum` varchar(255) DEFAULT NULL,
  `OriginalFilename` varchar(255) DEFAULT NULL,
  KEY `storagefileIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `storagethumbnail`
--

DROP TABLE IF EXISTS `storagethumbnail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storagethumbnail` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Filename` varchar(255) DEFAULT NULL,
  `Size` varchar(255) DEFAULT NULL,
  `SubDirectoryPath` varchar(255) DEFAULT NULL,
  `LastUsedDt` date DEFAULT NULL,
  `Page` varchar(255) DEFAULT NULL,
  `Filetype` varchar(255) DEFAULT NULL,
  KEY `storagethumbnailIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `submitterissues`
--

DROP TABLE IF EXISTS `submitterissues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submitterissues` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `SubTypeCd` varchar(255) DEFAULT NULL,
  `Msg` varchar(255) DEFAULT NULL,
  KEY `submitterissuesIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `subproducer`
--

DROP TABLE IF EXISTS `subproducer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subproducer` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `SubProducerCd` varchar(255) DEFAULT NULL,
  `SubProducerTypeCd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  KEY `subproducerIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `subrogation`
--

DROP TABLE IF EXISTS `subrogation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subrogation` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `SubrogationStatusCd` varchar(255) DEFAULT NULL,
  `StatuteDt` date DEFAULT NULL,
  `OpenedDt` date DEFAULT NULL,
  `ClosedDt` date DEFAULT NULL,
  `DeductibleWaivedInd` varchar(255) DEFAULT NULL,
  `SubrogationProviderRef` int(11) DEFAULT NULL,
  `InstallmentPaymentInd` varchar(255) DEFAULT NULL,
  `ResponsiblePartyLiabilityPct` decimal(28,6) DEFAULT NULL,
  `RespPartyPreferDeliveryMethod` varchar(255) DEFAULT NULL,
  `RespOwnerPreferDeliveryMethod` varchar(255) DEFAULT NULL,
  `InstallmentAmount` varchar(255) DEFAULT NULL,
  `TotalRecoveryAmt` varchar(255) DEFAULT NULL,
  `FirstDueDate` date DEFAULT NULL,
  `SubrogationProviderNo` varchar(255) DEFAULT NULL,
  KEY `subrogationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `subscrdeliverymethod`
--

DROP TABLE IF EXISTS `subscrdeliverymethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscrdeliverymethod` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `SubscriberDeliveryMethodIdRef` varchar(255) DEFAULT NULL,
  `FormatCd` varchar(255) DEFAULT NULL,
  KEY `subscrdeliverymethodIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `subscriberdeliverymethod`
--

DROP TABLE IF EXISTS `subscriberdeliverymethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriberdeliverymethod` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `DeliveryMethodTemplateIdRef` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `UpdateDt` date DEFAULT NULL,
  `UpdateTm` varchar(255) DEFAULT NULL,
  KEY `subscriberdeliverymethodIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `subscription`
--

DROP TABLE IF EXISTS `subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `SubscriptionTemplateIdRef` varchar(255) DEFAULT NULL,
  `EventCd` varchar(255) DEFAULT NULL,
  `GroupCd` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `ScheduleCd` varchar(255) DEFAULT NULL,
  `DigestInd` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `UpdateDt` date DEFAULT NULL,
  `UpdateTm` varchar(255) DEFAULT NULL,
  KEY `subscriptionIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriptions` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `DigestInd` varchar(255) DEFAULT NULL,
  KEY `subscriptionsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `suspense`
--

DROP TABLE IF EXISTS `suspense`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suspense` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  `SourceName` varchar(255) DEFAULT NULL,
  `AppliedUserId` varchar(255) DEFAULT NULL,
  `AppliedDt` date DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `RecreateSuspenseInd` varchar(255) DEFAULT NULL,
  KEY `suspenseIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `suspensionoccurance`
--

DROP TABLE IF EXISTS `suspensionoccurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suspensionoccurance` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `SuspensionNarrative` varchar(255) DEFAULT NULL,
  KEY `suspensionoccuranceIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `systemdata`
--

DROP TABLE IF EXISTS `systemdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemdata` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  KEY `systemdataIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `TagTemplateIdRef` varchar(255) DEFAULT NULL,
  KEY `tagIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tagtemplate`
--

DROP TABLE IF EXISTS `tagtemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tagtemplate` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `AttachmentMethodCd` varchar(255) DEFAULT NULL,
  `TagTemplateIdRef` varchar(255) DEFAULT NULL,
  KEY `tagtemplateIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `Version` varchar(255) DEFAULT NULL,
  `TemplateId` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Priority` int(11) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OriginalOwner` varchar(255) DEFAULT NULL,
  `OriginalOwnerCd` varchar(255) DEFAULT NULL,
  `Text` varchar(255) DEFAULT NULL,
  `WorkDt` date DEFAULT NULL,
  `CriticalDtInd` varchar(255) DEFAULT NULL,
  `CriticalDt` date DEFAULT NULL,
  `ReminderInd` varchar(255) DEFAULT NULL,
  `ReminderDt` date DEFAULT NULL,
  `ReportToInd` varchar(255) DEFAULT NULL,
  `ReportTo` varchar(255) DEFAULT NULL,
  `ReportToCd` varchar(255) DEFAULT NULL,
  `RepeatingInd` varchar(255) DEFAULT NULL,
  `Note` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `AddedByUserId` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `CompleteDt` date DEFAULT NULL,
  `CompleteTm` varchar(255) DEFAULT NULL,
  `CompletedByUserId` varchar(255) DEFAULT NULL,
  `WorkRequiredInd` varchar(255) DEFAULT NULL,
  `CurrentOwner` varchar(255) DEFAULT NULL,
  `CurrentOwnerCd` varchar(255) DEFAULT NULL,
  `EscalatedInd` varchar(255) DEFAULT NULL,
  `NotificationRequestRef` int(11) DEFAULT NULL,
  `FromEmail` varchar(255) DEFAULT NULL,
  `ToEmail` varchar(255) DEFAULT NULL,
  `ReplyToEmail` varchar(255) DEFAULT NULL,
  `Subject` varchar(255) DEFAULT NULL,
  `GoverningStateCd` varchar(255) DEFAULT NULL,
  `ProductName` varchar(255) DEFAULT NULL,
  `LicenseClassCd` varchar(255) DEFAULT NULL,
  `CarrierGroupCd` varchar(255) DEFAULT NULL,
  `CarrierCd` varchar(255) DEFAULT NULL,
  `PolicyTypeCd` varchar(255) DEFAULT NULL,
  `PolicyGroupCd` varchar(255) DEFAULT NULL,
  `AssignedProducerCd` varchar(255) DEFAULT NULL,
  `AssignedProducerName` varchar(255) DEFAULT NULL,
  `AgencyCd` varchar(255) DEFAULT NULL,
  `AgencyName` varchar(255) DEFAULT NULL,
  `AgencyGroupCd` varchar(255) DEFAULT NULL,
  `AgencyGroupName` varchar(255) DEFAULT NULL,
  `LossCauseCd` varchar(255) DEFAULT NULL,
  `ExaminerCd` varchar(255) DEFAULT NULL,
  `ExaminerName` varchar(255) DEFAULT NULL,
  `BranchCd` varchar(255) DEFAULT NULL,
  `InSIUInd` varchar(255) DEFAULT NULL,
  `DOIComplaintInd` varchar(255) DEFAULT NULL,
  `CatastropheInd` varchar(255) DEFAULT NULL,
  `ClaimStatus` varchar(255) DEFAULT NULL,
  `InjuryInvolvedInd` varchar(255) DEFAULT NULL,
  `SubrogationId` varchar(255) DEFAULT NULL,
  `InstallmentNumber` varchar(255) DEFAULT NULL,
  `ServicingProducerCd` varchar(255) DEFAULT NULL,
  `ServicingProducerName` varchar(255) DEFAULT NULL,
  `CustomerName` varchar(255) DEFAULT NULL,
  KEY `taskIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `taskhistory`
--

DROP TABLE IF EXISTS `taskhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taskhistory` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `AssignedTo` varchar(255) DEFAULT NULL,
  `AssignedToCd` varchar(255) DEFAULT NULL,
  `AssignedBy` varchar(255) DEFAULT NULL,
  `Type` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  KEY `taskhistoryIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tasklink`
--

DROP TABLE IF EXISTS `tasklink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasklink` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `IdRef` varchar(255) DEFAULT NULL,
  `ModelName` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `LinkService` varchar(255) DEFAULT NULL,
  `LinkVariables` varchar(255) DEFAULT NULL,
  `SearchValue` varchar(255) DEFAULT NULL,
  `DefaultInd` varchar(255) DEFAULT NULL,
  KEY `tasklinkIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `taskread`
--

DROP TABLE IF EXISTS `taskread`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taskread` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ReadBy` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  KEY `taskreadIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `taskrule`
--

DROP TABLE IF EXISTS `taskrule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taskrule` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `RuleTypeCd` varchar(255) DEFAULT NULL,
  `Processor` varchar(255) DEFAULT NULL,
  `RuleSource` varchar(255) DEFAULT NULL,
  KEY `taskruleIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `taxinfo`
--

DROP TABLE IF EXISTS `taxinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TaxTypeCd` varchar(255) DEFAULT NULL,
  `FEIN` varchar(255) DEFAULT NULL,
  `SSN` varchar(255) DEFAULT NULL,
  `LegalEntityCd` varchar(255) DEFAULT NULL,
  `WithholdingExemptInd` varchar(255) DEFAULT NULL,
  `Received1099Ind` varchar(255) DEFAULT NULL,
  `ReceivedW9Ind` varchar(255) DEFAULT NULL,
  `Required1099Ind` varchar(255) DEFAULT NULL,
  `TaxIdTypeCd` varchar(255) DEFAULT NULL,
  KEY `taxinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `testperson`
--

DROP TABLE IF EXISTS `testperson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testperson` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `export` varchar(255) DEFAULT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `FirstName` varchar(255) DEFAULT NULL,
  `Gender` varchar(255) DEFAULT NULL,
  `Age` int(11) DEFAULT NULL,
  `BirthDate` varchar(255) DEFAULT NULL,
  `SSN` varchar(255) DEFAULT NULL,
  `Type` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  KEY `testpersonIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `thing`
--

DROP TABLE IF EXISTS `thing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thing` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `UID` varchar(255) DEFAULT NULL,
  `Listing` varchar(255) DEFAULT NULL,
  `Link` varchar(255) DEFAULT NULL,
  KEY `thingIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `totalpaymentobligation`
--

DROP TABLE IF EXISTS `totalpaymentobligation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `totalpaymentobligation` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `SequenceNumber` int(11) DEFAULT NULL,
  `TotalPaymentAmt` decimal(28,6) DEFAULT NULL,
  `PaymentDueDt` date DEFAULT NULL,
  `FundingDelayedDt` date DEFAULT NULL,
  KEY `totalpaymentobligationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transactionclosereason`
--

DROP TABLE IF EXISTS `transactionclosereason`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactionclosereason` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CloseReasonCd` varchar(255) DEFAULT NULL,
  `CloseSubReasonLabel` varchar(255) DEFAULT NULL,
  `CloseSubReasonCd` varchar(255) DEFAULT NULL,
  `CloseComment` varchar(255) DEFAULT NULL,
  KEY `transactionclosereasonIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transactionhistory`
--

DROP TABLE IF EXISTS `transactionhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactionhistory` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `TransactionEffectiveDt` date DEFAULT NULL,
  `TransactionEffectiveTm` varchar(255) DEFAULT NULL,
  `TransactionUser` varchar(255) DEFAULT NULL,
  `TransactionDt` date DEFAULT NULL,
  `TransactionTm` varchar(255) DEFAULT NULL,
  `TransactionLongDescription` varchar(255) DEFAULT NULL,
  `TransactionShortDescription` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `SubmissionNumber` int(11) DEFAULT NULL,
  `InforcePremiumAmt` decimal(28,6) DEFAULT NULL,
  `InforceChangePremiumAmt` decimal(28,6) DEFAULT NULL,
  `WrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `WrittenChangePremiumAmt` decimal(28,6) DEFAULT NULL,
  `CancelTypeCd` varchar(255) DEFAULT NULL,
  `CancelRequestedByCd` varchar(255) DEFAULT NULL,
  `RewriteToProductVersion` varchar(255) DEFAULT NULL,
  `PremiumCalculationDt` date DEFAULT NULL,
  `NewPolicyEffectiveDt` date DEFAULT NULL,
  `NewPolicyExpirationDt` date DEFAULT NULL,
  `ReleaseHoldReason` varchar(255) DEFAULT NULL,
  `ChangeInfoRef` varchar(255) DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `MasterTransactionNumber` int(11) DEFAULT NULL,
  `ReplacedByTransactionNumber` int(11) DEFAULT NULL,
  `ReplacementOfTransactionNumber` int(11) DEFAULT NULL,
  `UnAppliedByTransactionNumber` int(11) DEFAULT NULL,
  `UnApplyOfTransactionNumber` int(11) DEFAULT NULL,
  `StatProcessedInd` varchar(255) DEFAULT NULL,
  `OutputTypeCd` varchar(255) DEFAULT NULL,
  `ApplicationRef` int(11) DEFAULT NULL,
  `HoldType` varchar(255) DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `DelayInvoiceInd` varchar(255) DEFAULT NULL,
  `PaymentTypeCd` varchar(255) DEFAULT NULL,
  `CheckNumber` varchar(255) DEFAULT NULL,
  `PaymentAmt` varchar(255) DEFAULT NULL,
  `ConversionGroup` varchar(255) DEFAULT NULL,
  `ConversionTemplateIdRef` varchar(255) DEFAULT NULL,
  `ConversionJobRef` varchar(255) DEFAULT NULL,
  `RenewalProviderRef` int(11) DEFAULT NULL,
  `RenewalSubProducerCd` varchar(255) DEFAULT NULL,
  `WrittenFeeAmt` decimal(28,6) DEFAULT NULL,
  `WrittenCommissionFeeAmt` decimal(28,6) DEFAULT NULL,
  `TransactionCommissionAmt` decimal(28,6) DEFAULT NULL,
  `FutureTermAutoTransactionInd` varchar(255) DEFAULT NULL,
  `UnapplySource` varchar(255) DEFAULT NULL,
  `ChargeReinstatementFeeInd` varchar(255) DEFAULT NULL,
  `WaiveCancellationAuditInd` varchar(255) DEFAULT NULL,
  `TotalNetLocationsManualPremiumAmt` decimal(28,6) DEFAULT NULL,
  `ReinstatementCd` varchar(255) DEFAULT NULL,
  KEY `transactionhistoryIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transactioninfo`
--

DROP TABLE IF EXISTS `transactioninfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactioninfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TransactionCd` varchar(255) DEFAULT NULL,
  `TransactionEffectiveDt` date DEFAULT NULL,
  `TransactionEffectiveTm` varchar(255) DEFAULT NULL,
  `TransactionUser` varchar(255) DEFAULT NULL,
  `TransactionDt` date DEFAULT NULL,
  `TransactionTm` varchar(255) DEFAULT NULL,
  `TransactionShortDescription` varchar(255) DEFAULT NULL,
  `TransactionLongDescription` varchar(255) DEFAULT NULL,
  `ChangeInfoRef` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `SubmissionNumber` int(11) DEFAULT NULL,
  `InforceChangePremiumAmt` decimal(28,6) DEFAULT NULL,
  `InforcePremiumAmt` decimal(28,6) DEFAULT NULL,
  `WrittenPremiumAmt` decimal(28,6) DEFAULT NULL,
  `CancelTypeCd` varchar(255) DEFAULT NULL,
  `CancelRequestedByCd` varchar(255) DEFAULT NULL,
  `PremiumCalculationDt` date DEFAULT NULL,
  `RewriteToProductVersion` varchar(255) DEFAULT NULL,
  `NewPolicyEffectiveDt` date DEFAULT NULL,
  `NewPolicyExpirationDt` date DEFAULT NULL,
  `ReleaseHoldReason` varchar(255) DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  `MasterTransactionNumber` int(11) DEFAULT NULL,
  `ReplacementOfTransactionNumber` int(11) DEFAULT NULL,
  `UnApplyOfTransactionNumber` int(11) DEFAULT NULL,
  `StatProcessedInd` varchar(255) DEFAULT NULL,
  `OutputTypeCd` varchar(255) DEFAULT NULL,
  `HoldType` varchar(255) DEFAULT NULL,
  `SourceCd` varchar(255) DEFAULT NULL,
  `DelayInvoiceInd` varchar(255) DEFAULT NULL,
  `CheckNumber` varchar(255) DEFAULT NULL,
  `PaymentTypeCd` varchar(255) DEFAULT NULL,
  `PaymentAmt` varchar(255) DEFAULT NULL,
  `ConversionTemplateIdRef` varchar(255) DEFAULT NULL,
  `ConversionGroup` varchar(255) DEFAULT NULL,
  `ConversionJobRef` varchar(255) DEFAULT NULL,
  `RenewalProviderRef` int(11) DEFAULT NULL,
  `RenewalSubProducerCd` varchar(255) DEFAULT NULL,
  `CommissionChangeInd` varchar(255) DEFAULT NULL,
  `PrevStatusCd` varchar(255) DEFAULT NULL,
  `FutureTermAutoTransactionInd` varchar(255) DEFAULT NULL,
  `UnapplySource` varchar(255) DEFAULT NULL,
  `ChargeReinstatementFeeInd` varchar(255) DEFAULT NULL,
  `WaiveCancellationAuditInd` varchar(255) DEFAULT NULL,
  `TotalNetLocationsManualPremiumAmt` decimal(28,6) DEFAULT NULL,
  `ReinstatementCd` varchar(255) DEFAULT NULL,
  KEY `transactioninfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transactionreason`
--

DROP TABLE IF EXISTS `transactionreason`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactionreason` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ReasonCd` varchar(255) DEFAULT NULL,
  KEY `transactionreasonIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transactiontext`
--

DROP TABLE IF EXISTS `transactiontext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactiontext` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TransactionTextTypeCd` varchar(255) DEFAULT NULL,
  `TextCd` varchar(255) DEFAULT NULL,
  `Text` varchar(255) DEFAULT NULL,
  KEY `transactiontextIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trustplanhistory`
--

DROP TABLE IF EXISTS `trustplanhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trustplanhistory` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `HistoryCd` varchar(255) DEFAULT NULL,
  `Year` int(11) DEFAULT NULL,
  `TotalAssets` int(11) DEFAULT NULL,
  `NumberOfParticipants` int(11) DEFAULT NULL,
  `AnnualContributions` int(11) DEFAULT NULL,
  KEY `trustplanhistoryIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `umbrellapolicyinfo`
--

DROP TABLE IF EXISTS `umbrellapolicyinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `umbrellapolicyinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `IdRef` varchar(255) DEFAULT NULL,
  KEY `umbrellapolicyinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userdefined`
--

DROP TABLE IF EXISTS `userdefined`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userdefined` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UserDefinedCd` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  KEY `userdefinedIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo`
--

DROP TABLE IF EXISTS `userinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `LoginId` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `SuperUserInd` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Supervisor` varchar(255) DEFAULT NULL,
  `EmailAddr` varchar(255) DEFAULT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `FirstName` varchar(255) DEFAULT NULL,
  `StartWork` varchar(255) DEFAULT NULL,
  `Realm` varchar(255) DEFAULT NULL,
  `ConcurrentSessions` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `PasswordMustChangeInd` varchar(255) DEFAULT NULL,
  `DepartmentCd` varchar(255) DEFAULT NULL,
  `TerminatedDt` date DEFAULT NULL,
  `ProviderRef` int(11) DEFAULT NULL,
  `DefaultLanguageCd` varchar(255) DEFAULT NULL,
  `LastLogonDt` date DEFAULT NULL,
  `LastLogonTm` varchar(255) DEFAULT NULL,
  `LastViewedTasksDt` date DEFAULT NULL,
  `LastViewedTasksTm` varchar(255) DEFAULT NULL,
  `Version` varchar(255) DEFAULT NULL,
  `BranchCd` varchar(255) DEFAULT NULL,
  `ToolbarSearchMode` varchar(255) DEFAULT NULL,
  `UserManagementGroupCd` varchar(255) DEFAULT NULL,
  KEY `userinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userlogonrestrict`
--

DROP TABLE IF EXISTS `userlogonrestrict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userlogonrestrict` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `DayOfWeek` varchar(255) DEFAULT NULL,
  `Start1Tm` varchar(255) DEFAULT NULL,
  `End1Tm` varchar(255) DEFAULT NULL,
  `Start2Tm` varchar(255) DEFAULT NULL,
  `End2Tm` varchar(255) DEFAULT NULL,
  `ForceLogoutInd` varchar(255) DEFAULT NULL,
  KEY `userlogonrestrictIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userrecentactivity`
--

DROP TABLE IF EXISTS `userrecentactivity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userrecentactivity` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `UserInfoRef` int(11) DEFAULT NULL,
  KEY `userrecentactivityIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userrole`
--

DROP TABLE IF EXISTS `userrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userrole` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `AuthorityRoleIdRef` varchar(255) DEFAULT NULL,
  KEY `userroleIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userroleattribute`
--

DROP TABLE IF EXISTS `userroleattribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userroleattribute` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `AuthorityAttributeIdRef` varchar(255) DEFAULT NULL,
  KEY `userroleattributeIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usertaskcontrol`
--

DROP TABLE IF EXISTS `usertaskcontrol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usertaskcontrol` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TransferReason` varchar(255) DEFAULT NULL,
  `TransferStartDt` date DEFAULT NULL,
  `TransferEndDt` date DEFAULT NULL,
  `TransferToTypeCd` varchar(255) DEFAULT NULL,
  `TransferTo` varchar(255) DEFAULT NULL,
  `TransferToUser` varchar(255) DEFAULT NULL,
  `EscalationToTypeCd` varchar(255) DEFAULT NULL,
  `EscalationTo` varchar(255) DEFAULT NULL,
  `EscalationToUser` varchar(255) DEFAULT NULL,
  KEY `usertaskcontrolIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usertaskgroup`
--

DROP TABLE IF EXISTS `usertaskgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usertaskgroup` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TaskGroupCd` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `TransferInd` varchar(255) DEFAULT NULL,
  `TransferTo` varchar(255) DEFAULT NULL,
  `TransferToCd` varchar(255) DEFAULT NULL,
  KEY `usertaskgroupIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usertaskpriority`
--

DROP TABLE IF EXISTS `usertaskpriority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usertaskpriority` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TaskTypeCd` varchar(255) DEFAULT NULL,
  `Sequence` int(11) DEFAULT NULL,
  KEY `usertaskpriorityIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uslhactcovendinfo`
--

DROP TABLE IF EXISTS `uslhactcovendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uslhactcovendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `StCdAndUSHLCovFactor` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `uslhactcovendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `validationerror`
--

DROP TABLE IF EXISTS `validationerror`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `validationerror` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Msg` varchar(255) DEFAULT NULL,
  `SubTypeCd` varchar(255) DEFAULT NULL,
  `PermanentInd` varchar(255) DEFAULT NULL,
  `Link` varchar(255) DEFAULT NULL,
  KEY `validationerrorIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vehicle`
--

DROP TABLE IF EXISTS `vehicle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicle` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `VehNumber` int(11) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `VinValidationCd` varchar(255) DEFAULT NULL,
  `CarPoolInd` varchar(255) DEFAULT NULL,
  `NewOrUsedInd` varchar(255) DEFAULT NULL,
  `OdometerReading` int(11) DEFAULT NULL,
  `DaytimeRunningLights` varchar(255) DEFAULT NULL,
  `WeeksPerMonthDriven` int(11) DEFAULT NULL,
  `DaysPerWeekDriven` int(11) DEFAULT NULL,
  `PassiveSeatBeltInd` varchar(255) DEFAULT NULL,
  `TowingAndLaborInd` varchar(255) DEFAULT NULL,
  `UMPDLimit` varchar(255) DEFAULT NULL,
  `LiabilityWaiveInd` varchar(255) DEFAULT NULL,
  `RentalReimbursementInd` varchar(255) DEFAULT NULL,
  `ClassCd` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `ModelYr` varchar(255) DEFAULT NULL,
  `VehIdentificationNumber` varchar(255) DEFAULT NULL,
  `ValidVinInd` varchar(255) DEFAULT NULL,
  `VehLicenseNumber` varchar(255) DEFAULT NULL,
  `RegistrationStateProvCd` varchar(255) DEFAULT NULL,
  `VehBodyTypeCd` varchar(255) DEFAULT NULL,
  `VehTypeCd` varchar(255) DEFAULT NULL,
  `CostNewRange` varchar(255) DEFAULT NULL,
  `CostNewAmount` decimal(28,6) DEFAULT NULL,
  `PerformanceCd` varchar(255) DEFAULT NULL,
  `RestraintCd` varchar(255) DEFAULT NULL,
  `AntiBrakingSystemCd` varchar(255) DEFAULT NULL,
  `AntiTheftCd` varchar(255) DEFAULT NULL,
  `EngineSize` varchar(255) DEFAULT NULL,
  `EngineCylinders` varchar(255) DEFAULT NULL,
  `EngineHorsePower` varchar(255) DEFAULT NULL,
  `EngineType` varchar(255) DEFAULT NULL,
  `GrossVehicleWeight` varchar(255) DEFAULT NULL,
  `VehUseCd` varchar(255) DEFAULT NULL,
  `VehSecondaryUseCd` varchar(255) DEFAULT NULL,
  `DrivingRadius` varchar(255) DEFAULT NULL,
  `SeasonalMonths` varchar(255) DEFAULT NULL,
  `GarageTerritory` int(11) DEFAULT NULL,
  `FarthestTerritory` varchar(255) DEFAULT NULL,
  `CollisionDed` varchar(255) DEFAULT NULL,
  `ComprehensiveDed` varchar(255) DEFAULT NULL,
  `MedPayLimit` varchar(255) DEFAULT NULL,
  `StatusComments` varchar(255) DEFAULT NULL,
  `DeclineLiability` varchar(255) DEFAULT NULL,
  `ValuationMethod` varchar(255) DEFAULT NULL,
  `StatedAmt` decimal(28,6) DEFAULT NULL,
  `OperatorExperience` varchar(255) DEFAULT NULL,
  `VehicleClass` varchar(255) DEFAULT NULL,
  `ComprehensivePerilsCd` varchar(255) DEFAULT NULL,
  `UsedInDumping` varchar(255) DEFAULT NULL,
  `BobtailOperations` varchar(255) DEFAULT NULL,
  `RatingValue` varchar(255) DEFAULT NULL,
  `RatingValueOverrideCd` varchar(255) DEFAULT NULL,
  `CostNewAmt` decimal(28,6) DEFAULT NULL,
  `EstimatedAnnualDistance` int(11) DEFAULT NULL,
  `EstimatedWorkDistance` int(11) DEFAULT NULL,
  `LeasedVehInd` varchar(255) DEFAULT NULL,
  `PurchaseDt` date DEFAULT NULL,
  `StatedAmtInd` varchar(255) DEFAULT NULL,
  `CollisionForcedDed` varchar(255) DEFAULT NULL,
  `ComprehensiveForcedDed` varchar(255) DEFAULT NULL,
  `Trim` varchar(255) DEFAULT NULL,
  `NCICMake` varchar(255) DEFAULT NULL,
  KEY `vehicleIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vocationalrehabilitation`
--

DROP TABLE IF EXISTS `vocationalrehabilitation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vocationalrehabilitation` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `vocationalrehabilitationIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vocationalrehabilitationitem`
--

DROP TABLE IF EXISTS `vocationalrehabilitationitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vocationalrehabilitationitem` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `ServicingFacility` varchar(255) DEFAULT NULL,
  `FacilityAdvisorOrCounselor` varchar(255) DEFAULT NULL,
  `FacilityPhoneNumber` varchar(255) DEFAULT NULL,
  `ClinicalMeasureIdentified` varchar(255) DEFAULT NULL,
  `ActionSteps` varchar(255) DEFAULT NULL,
  `StartDt` date DEFAULT NULL,
  `EndDt` date DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `Order` int(11) DEFAULT NULL,
  KEY `vocationalrehabilitationitemIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `volcompcovendinfo`
--

DROP TABLE IF EXISTS `volcompcovendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `volcompcovendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `MasterCrews` varchar(255) DEFAULT NULL,
  `WCStateLaw` varchar(255) DEFAULT NULL,
  `WorkDescription` varchar(255) DEFAULT NULL,
  `EndorsementSeqNo` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `volcompcovendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `volcompemprliabcovendinfo`
--

DROP TABLE IF EXISTS `volcompemprliabcovendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `volcompemprliabcovendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `IdentifyEmployees` varchar(255) DEFAULT NULL,
  `StateofEmployment` varchar(255) DEFAULT NULL,
  `DesignatedWCLawDesc` varchar(255) DEFAULT NULL,
  `EndorsementSeqNo` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `volcompemprliabcovendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wcpolsaddressinfo`
--

DROP TABLE IF EXISTS `wcpolsaddressinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcpolsaddressinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `AddressTypeCd` int(11) DEFAULT NULL,
  `ForeignAddressInd` varchar(255) DEFAULT NULL,
  `AddressStrucCd` varchar(255) DEFAULT NULL,
  `Street` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL,
  `State` varchar(255) DEFAULT NULL,
  `ZipCd` varchar(255) DEFAULT NULL,
  `NameLinkId` int(11) DEFAULT NULL,
  `StateCdLink` int(11) DEFAULT NULL,
  `ExposureRecordLink` int(11) DEFAULT NULL,
  `AddressFutureRef02` int(11) DEFAULT NULL,
  `InsuredContactNo` varchar(255) DEFAULT NULL,
  `NoOfEmployees` int(11) DEFAULT NULL,
  `IndustryCd` varchar(255) DEFAULT NULL,
  `GeographicArea` varchar(255) DEFAULT NULL,
  `AddressFutureRef03` varchar(255) DEFAULT NULL,
  `CountryCd` varchar(255) DEFAULT NULL,
  `NameLinkCounterId` varchar(255) DEFAULT NULL,
  `AddressFutureRef04` varchar(255) DEFAULT NULL,
  `PolicyChangeEffDt` int(11) DEFAULT NULL,
  `PolicyChangeExpDt` int(11) DEFAULT NULL,
  KEY `wcpolsaddressinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wcpolscancorreininfo`
--

DROP TABLE IF EXISTS `wcpolscancorreininfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcpolscancorreininfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `RecordTypeCd` int(11) DEFAULT NULL,
  `CanOrReinCd` int(11) DEFAULT NULL,
  `CanTypeCd` int(11) DEFAULT NULL,
  `CanReason` int(11) DEFAULT NULL,
  `ReinTypeCd` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `InsuredAddr` varchar(255) DEFAULT NULL,
  `InsuredNature` varchar(255) DEFAULT NULL,
  `MailIntimationDt` int(11) DEFAULT NULL,
  `CanOrReinSeqNo` int(11) DEFAULT NULL,
  `ReinReason` int(11) DEFAULT NULL,
  `corresCanEffDt` int(11) DEFAULT NULL,
  `canOrReinEffDt` int(11) DEFAULT NULL,
  `FutureRef` int(11) DEFAULT NULL,
  `policyChangeEffDt` int(11) DEFAULT NULL,
  `policyChangeExpDt` int(11) DEFAULT NULL,
  KEY `wcpolscancorreininfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wcpolscontrib`
--

DROP TABLE IF EXISTS `wcpolscontrib`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcpolscontrib` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `UpdateCount` int(11) DEFAULT NULL,
  `UpdateUser` varchar(255) DEFAULT NULL,
  `UpdateTimestamp` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `ReportPeriodBeginDt` date DEFAULT NULL,
  `ReportPeriodEndDt` date DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `RegeneratedInd` varchar(255) DEFAULT NULL,
  `PolicyRef` varchar(255) DEFAULT NULL,
  `TransactionNumber` int(11) DEFAULT NULL,
  `ProductVersionIdRef` varchar(255) DEFAULT NULL,
  `AddDt` date DEFAULT NULL,
  `AddTm` varchar(255) DEFAULT NULL,
  `ProcessDt` date DEFAULT NULL,
  `ProcessTm` varchar(255) DEFAULT NULL,
  `BookDt` date DEFAULT NULL,
  KEY `wcpolscontribIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wcpolsendorsementrecordinfo`
--

DROP TABLE IF EXISTS `wcpolsendorsementrecordinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcpolsendorsementrecordinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` varchar(255) DEFAULT NULL,
  `RecordTypeCd` int(11) DEFAULT NULL,
  `EndorsementFormNos` varchar(255) DEFAULT NULL,
  `PolicyChangeEffDt` int(11) DEFAULT NULL,
  `PolicyChangeExpDt` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `wcpolsendorsementrecordinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wcpolsexposureinfo`
--

DROP TABLE IF EXISTS `wcpolsexposureinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcpolsexposureinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `ExposureFutureRef01` varchar(255) DEFAULT NULL,
  `ClassificationCd` int(11) DEFAULT NULL,
  `ClassificationUseCd` int(11) DEFAULT NULL,
  `ExposureFutureRef02` int(11) DEFAULT NULL,
  `ClassificationSuffix` varchar(255) DEFAULT NULL,
  `ExposureActOrCoverageCd` int(11) DEFAULT NULL,
  `ManualOrChargedRate` int(11) DEFAULT NULL,
  `ManualOrChargedRateDecPt` int(11) DEFAULT NULL,
  `ExpPeriodEffDt` int(11) DEFAULT NULL,
  `ExposureFutureRef03` int(11) DEFAULT NULL,
  `EstExpAmt` int(11) DEFAULT NULL,
  `EstPremAmt` int(11) DEFAULT NULL,
  `ExpPeriodCd` int(11) DEFAULT NULL,
  `ClassificationWording` varchar(255) DEFAULT NULL,
  `ExposureFutureRef04` int(11) DEFAULT NULL,
  `NameLinkId` int(11) DEFAULT NULL,
  `StateCdLink` int(11) DEFAULT NULL,
  `ExpRecordLink` int(11) DEFAULT NULL,
  `NameLinkCounterId` varchar(255) DEFAULT NULL,
  `ExposureFutureRef05` varchar(255) DEFAULT NULL,
  `NoOfPiecesOfAparatus` int(11) DEFAULT NULL,
  `NoOfVolunteers` int(11) DEFAULT NULL,
  `PolicySurchageFactor` int(11) DEFAULT NULL,
  `PremiumAdjustmentFactor` int(11) DEFAULT NULL,
  `ExposureFutureRef06` varchar(255) DEFAULT NULL,
  `policyChangeEffDt` int(11) DEFAULT NULL,
  `policyChangeExpDt` int(11) DEFAULT NULL,
  KEY `wcpolsexposureinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wcpolsheader`
--

DROP TABLE IF EXISTS `wcpolsheader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcpolsheader` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `ExpRatingCd` varchar(255) DEFAULT NULL,
  `RiskId` varchar(255) DEFAULT NULL,
  `PolicyExpDt` varchar(255) DEFAULT NULL,
  `Fein` varchar(255) DEFAULT NULL,
  `CoverageTypeCd` varchar(255) DEFAULT NULL,
  `EmpPolicyTypeCd` varchar(255) DEFAULT NULL,
  `PolicyTermCd` varchar(255) DEFAULT NULL,
  `PriorPolicyNumber` varchar(255) DEFAULT NULL,
  `HeaderFutureRef02` varchar(255) DEFAULT NULL,
  `EntityTypeCd` varchar(255) DEFAULT NULL,
  `PlanCd` varchar(255) DEFAULT NULL,
  `OcipCd` varchar(255) DEFAULT NULL,
  `BussinessSegCd` varchar(255) DEFAULT NULL,
  `MinPremiumAmt` varchar(255) DEFAULT NULL,
  `MinPremiumAmtStCd` varchar(255) DEFAULT NULL,
  `EstStandardPremTot` varchar(255) DEFAULT NULL,
  `DepositPremAmt` varchar(255) DEFAULT NULL,
  `AuditFreqCd` varchar(255) DEFAULT NULL,
  `BillingFreqCd` varchar(255) DEFAULT NULL,
  `RetrospecRatingCd` varchar(255) DEFAULT NULL,
  `EmpLiabAmtBIAEEA` varchar(255) DEFAULT NULL,
  `EmpLiabAmtBIDPLA` varchar(255) DEFAULT NULL,
  `EmpLiabAmtBIDEEA` varchar(255) DEFAULT NULL,
  `ProducerName` varchar(255) DEFAULT NULL,
  `RiskNoFirstSeven` varchar(255) DEFAULT NULL,
  `GroupCovStCd` varchar(255) DEFAULT NULL,
  `HeaderFutureRef03` varchar(255) DEFAULT NULL,
  `OrgCarrierCd` varchar(255) DEFAULT NULL,
  `OrgPolicyNumber` varchar(255) DEFAULT NULL,
  `OrgPolicyEffDt` varchar(255) DEFAULT NULL,
  `OtherLegalInsured` varchar(255) DEFAULT NULL,
  `AssignmentDt` varchar(255) DEFAULT NULL,
  `RiskNoLastEleven` varchar(255) DEFAULT NULL,
  `HeaderFutureRef04` varchar(255) DEFAULT NULL,
  KEY `wcpolsheaderIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wcpolslinkdata`
--

DROP TABLE IF EXISTS `wcpolslinkdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcpolslinkdata` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `CarrierCd` int(11) DEFAULT NULL,
  `PolicyNumber` varchar(255) DEFAULT NULL,
  `EffectiveDt` int(11) DEFAULT NULL,
  `TransactionEffDt` int(11) DEFAULT NULL,
  `TransactionCd` int(11) DEFAULT NULL,
  `PolicyChangeEffDt` int(11) DEFAULT NULL,
  `PolicyChangeExpDt` int(11) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorsementSeqNo` int(11) DEFAULT NULL,
  KEY `wcpolslinkdataIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wcpolsnameinfo`
--

DROP TABLE IF EXISTS `wcpolsnameinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcpolsnameinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `NameTypeCd` int(11) DEFAULT NULL,
  `NameLinkCd` int(11) DEFAULT NULL,
  `ClientCompanyCd` varchar(255) DEFAULT NULL,
  `AIInsuredName` varchar(255) DEFAULT NULL,
  `NameFutureRef02` varchar(255) DEFAULT NULL,
  `InsuredFein` int(11) DEFAULT NULL,
  `SeqNumber` int(11) DEFAULT NULL,
  `EntityCd` int(11) DEFAULT NULL,
  `StateCd0` int(11) DEFAULT NULL,
  `StateUnEmpNumber0` varchar(255) DEFAULT NULL,
  `StateCd1` int(11) DEFAULT NULL,
  `StateUnEmpNumber1` varchar(255) DEFAULT NULL,
  `StateCd2` int(11) DEFAULT NULL,
  `StateUnEmpNumber2` varchar(255) DEFAULT NULL,
  `NameFutureRef03` varchar(255) DEFAULT NULL,
  `StateUnEmpSeqNo` int(11) DEFAULT NULL,
  `OtherLegalEntity` varchar(255) DEFAULT NULL,
  `NameLinkCounterId` varchar(255) DEFAULT NULL,
  `NameFutureRef04` varchar(255) DEFAULT NULL,
  `PolicyChangeEffDt` int(11) DEFAULT NULL,
  `PolicyChangeExpDt` int(11) DEFAULT NULL,
  KEY `wcpolsnameinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wcpolsotherstatesinfo`
--

DROP TABLE IF EXISTS `wcpolsotherstatesinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcpolsotherstatesinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `IsStateIncOrExc` int(11) DEFAULT NULL,
  `OtherStateCd` int(11) DEFAULT NULL,
  `PolicyChangeEffDt` int(11) DEFAULT NULL,
  `PolicyChangeExpDt` int(11) DEFAULT NULL,
  KEY `wcpolsotherstatesinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wcpolspolicyinfo`
--

DROP TABLE IF EXISTS `wcpolspolicyinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcpolspolicyinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `wcpolspolicyinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wcpolsstatepremiuminfo`
--

DROP TABLE IF EXISTS `wcpolsstatepremiuminfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcpolsstatepremiuminfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `StateAddOrDelCd` varchar(255) DEFAULT NULL,
  `StatePremFutureRef01` varchar(255) DEFAULT NULL,
  `RiskId` varchar(255) DEFAULT NULL,
  `StatePremFutureRef02` varchar(255) DEFAULT NULL,
  `StatePremCarrierCd` int(11) DEFAULT NULL,
  `ExpModFactor` varchar(255) DEFAULT NULL,
  `ExpModStatusCd` int(11) DEFAULT NULL,
  `ExpModPlanTypeCd` int(11) DEFAULT NULL,
  `OtherIndRiskFactor` int(11) DEFAULT NULL,
  `InsurerPremDeviatFactor` int(11) DEFAULT NULL,
  `TypeOfPremDeviatCd` int(11) DEFAULT NULL,
  `EstStateStdPremTot` int(11) DEFAULT NULL,
  `ExpenseCnstAmt` int(11) DEFAULT NULL,
  `LossCnstAmt` int(11) DEFAULT NULL,
  `PremDiscountAmt` int(11) DEFAULT NULL,
  `ExpenseCnstReasonCd` int(11) DEFAULT NULL,
  `ProratedMinPremReasonCd` int(11) DEFAULT NULL,
  `ReasonStateAddedToPolicyCd` int(11) DEFAULT NULL,
  `StatePremFutureRef03` varchar(255) DEFAULT NULL,
  `ExpModEffDt` int(11) DEFAULT NULL,
  `AnnivRatingDt` int(11) DEFAULT NULL,
  `RiskAdjustPgmFactor` int(11) DEFAULT NULL,
  `StatePremFutureRef04` int(11) DEFAULT NULL,
  `PremAdjustmentPeriodCd` int(11) DEFAULT NULL,
  `NonStdIdType` int(11) DEFAULT NULL,
  `StatePremFutureRef05` varchar(255) DEFAULT NULL,
  `policyChangeEffDt` int(11) DEFAULT NULL,
  `policyChangeExpDt` int(11) DEFAULT NULL,
  KEY `wcpolsstatepremiuminfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wcpolstrailer`
--

DROP TABLE IF EXISTS `wcpolstrailer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcpolstrailer` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `TotalRecord` int(11) DEFAULT NULL,
  `TotalHeaderRecord` varchar(255) DEFAULT NULL,
  `TransactionBeginDate` varchar(255) DEFAULT NULL,
  `TransactionEndDate` varchar(255) DEFAULT NULL,
  KEY `wcpolstrailerIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `witness`
--

DROP TABLE IF EXISTS `witness`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `witness` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `WitnessNumber` int(11) DEFAULT NULL,
  `WitnessInformation` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  `TypeCd` varchar(255) DEFAULT NULL,
  `PreferredDeliveryMethod` varchar(255) DEFAULT NULL,
  KEY `witnessIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workhistory`
--

DROP TABLE IF EXISTS `workhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workhistory` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  KEY `workhistoryIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workhistoryitem`
--

DROP TABLE IF EXISTS `workhistoryitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workhistoryitem` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `Order` int(11) DEFAULT NULL,
  `WorkDt` date DEFAULT NULL,
  `WorkStatus` varchar(255) DEFAULT NULL,
  `ReturnToWorkQualifierCd` varchar(255) DEFAULT NULL,
  `WorkHistoryItemComment` varchar(255) DEFAULT NULL,
  `StatusCd` varchar(255) DEFAULT NULL,
  KEY `workhistoryitemIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `worksheetdetails`
--

DROP TABLE IF EXISTS `worksheetdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worksheetdetails` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `WorksheetCd` varchar(255) DEFAULT NULL,
  `IndemnityAmount` decimal(28,6) DEFAULT NULL,
  `DeductibleAppliedToDateAmt` decimal(28,6) DEFAULT NULL,
  `FinalPaymentAmt` decimal(28,6) DEFAULT NULL,
  `TotalReturnAmt` decimal(28,6) DEFAULT NULL,
  `TotalHoldbackBalance` decimal(28,6) DEFAULT NULL,
  `RemainingHoldbackBalance` decimal(28,6) DEFAULT NULL,
  KEY `worksheetdetailsIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wosendinfo`
--

DROP TABLE IF EXISTS `wosendinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wosendinfo` (
  `SystemId` int(11) DEFAULT NULL,
  `ParentId` varchar(255) DEFAULT NULL,
  `CMMContainer` varchar(255) DEFAULT NULL,
  `Id` varchar(255) DEFAULT NULL,
  `StateCd` int(11) DEFAULT NULL,
  `RecordTypeCd` varchar(255) DEFAULT NULL,
  `FormNo` varchar(255) DEFAULT NULL,
  `BureauVersionId` varchar(255) DEFAULT NULL,
  `CarrierVersionId` varchar(255) DEFAULT NULL,
  `NameofPerson` varchar(255) DEFAULT NULL,
  `NameofOrganization` varchar(255) DEFAULT NULL,
  `InsuredName` varchar(255) DEFAULT NULL,
  `EndorseEffDt` int(11) DEFAULT NULL,
  KEY `wosendinfoIdC` (`SystemId`,`CMMContainer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xxchangedbeans`
--

DROP TABLE IF EXISTS `xxchangedbeans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xxchangedbeans` (
  `CMMContainer` varchar(255) NOT NULL,
  `SystemId` int(11) NOT NULL,
  PRIMARY KEY (`CMMContainer`,`SystemId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Final view structure for view `accountingstats`
--

/*!50001 DROP VIEW IF EXISTS `accountingstats`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `accountingstats` AS select `start_develop_db`.`accountingstats`.`SystemId` AS `SystemId`,`start_develop_db`.`accountingstats`.`id` AS `id`,`start_develop_db`.`accountingstats`.`StatSequence` AS `StatSequence`,`start_develop_db`.`accountingstats`.`StatSequenceReplace` AS `StatSequenceReplace`,`start_develop_db`.`accountingstats`.`PolicyRef` AS `PolicyRef`,`start_develop_db`.`accountingstats`.`PolicyNumber` AS `PolicyNumber`,`start_develop_db`.`accountingstats`.`PolicyVersion` AS `PolicyVersion`,`start_develop_db`.`accountingstats`.`LineCd` AS `LineCd`,`start_develop_db`.`accountingstats`.`TransactionNumber` AS `TransactionNumber`,`start_develop_db`.`accountingstats`.`CoverageCd` AS `CoverageCd`,`start_develop_db`.`accountingstats`.`RiskCd` AS `RiskCd`,`start_develop_db`.`accountingstats`.`RiskTypeCd` AS `RiskTypeCd`,`start_develop_db`.`accountingstats`.`SubTypeCd` AS `SubTypeCd`,`start_develop_db`.`accountingstats`.`CoverageItemCd` AS `CoverageItemCd`,`start_develop_db`.`accountingstats`.`FeeCd` AS `FeeCd`,`start_develop_db`.`accountingstats`.`EffectiveDt` AS `EffectiveDt`,`start_develop_db`.`accountingstats`.`ExpirationDt` AS `ExpirationDt`,`start_develop_db`.`accountingstats`.`CombinedKey` AS `CombinedKey`,`start_develop_db`.`accountingstats`.`AddDt` AS `AddDt`,`start_develop_db`.`accountingstats`.`BookDt` AS `BookDt`,`start_develop_db`.`accountingstats`.`TransactionEffectiveDt` AS `TransactionEffectiveDt`,`start_develop_db`.`accountingstats`.`AccountingDt` AS `AccountingDt`,`start_develop_db`.`accountingstats`.`WrittenPremiumAmt` AS `WrittenPremiumAmt`,`start_develop_db`.`accountingstats`.`WrittenCommissionAmt` AS `WrittenCommissionAmt`,`start_develop_db`.`accountingstats`.`WrittenPremiumFeeAmt` AS `WrittenPremiumFeeAmt`,`start_develop_db`.`accountingstats`.`WrittenCommissionFeeAmt` AS `WrittenCommissionFeeAmt`,`start_develop_db`.`accountingstats`.`EarnDays` AS `EarnDays`,`start_develop_db`.`accountingstats`.`InforceChangeAmt` AS `InforceChangeAmt`,`start_develop_db`.`accountingstats`.`PolicyYear` AS `PolicyYear`,`start_develop_db`.`accountingstats`.`TermDays` AS `TermDays`,`start_develop_db`.`accountingstats`.`InsuranceTypeCd` AS `InsuranceTypeCd`,`start_develop_db`.`accountingstats`.`StartDt` AS `StartDt`,`start_develop_db`.`accountingstats`.`EndDt` AS `EndDt`,`start_develop_db`.`accountingstats`.`StatusCd` AS `StatusCd`,`start_develop_db`.`accountingstats`.`NewRenewalCd` AS `NewRenewalCd`,`start_develop_db`.`accountingstats`.`Limit1` AS `Limit1`,`start_develop_db`.`accountingstats`.`Limit2` AS `Limit2`,`start_develop_db`.`accountingstats`.`Deductible1` AS `Deductible1`,`start_develop_db`.`accountingstats`.`Deductible2` AS `Deductible2`,`start_develop_db`.`accountingstats`.`AnnualStatementLineCd` AS `AnnualStatementLineCd`,`start_develop_db`.`accountingstats`.`CoinsurancePct` AS `CoinsurancePct`,`start_develop_db`.`accountingstats`.`CoveredPerilsCd` AS `CoveredPerilsCd`,`start_develop_db`.`accountingstats`.`CommissionKey` AS `CommissionKey`,`start_develop_db`.`accountingstats`.`CommissionAreaCd` AS `CommissionAreaCd`,`start_develop_db`.`accountingstats`.`RateAreaName` AS `RateAreaName`,`start_develop_db`.`accountingstats`.`StatData` AS `StatData`,`start_develop_db`.`accountingstats`.`CarrierGroupCd` AS `CarrierGroupCd`,`start_develop_db`.`accountingstats`.`CarrierCd` AS `CarrierCd`,`start_develop_db`.`accountingstats`.`ProductVersionIdRef` AS `ProductVersionIdRef`,`start_develop_db`.`accountingstats`.`StateCd` AS `StateCd`,`start_develop_db`.`accountingstats`.`BusinessSourceCd` AS `BusinessSourceCd`,`start_develop_db`.`accountingstats`.`ProducerCd` AS `ProducerCd`,`start_develop_db`.`accountingstats`.`TransactionCd` AS `TransactionCd`,`start_develop_db`.`accountingstats`.`PolicyStatusCd` AS `PolicyStatusCd`,`start_develop_db`.`accountingstats`.`ProductName` AS `ProductName`,`start_develop_db`.`accountingstats`.`PolicyTypeCd` AS `PolicyTypeCd`,`start_develop_db`.`accountingstats`.`PolicyGroupCd` AS `PolicyGroupCd`,`start_develop_db`.`accountingstats`.`CustomerRef` AS `CustomerRef`,`start_develop_db`.`accountingstats`.`ShortRateStatInd` AS `ShortRateStatInd`,`start_develop_db`.`accountingstats`.`PayPlanCd` AS `PayPlanCd`,`start_develop_db`.`accountingstats`.`ControllingProductVersionIdRef` AS `ControllingProductVersionIdRef`,`start_develop_db`.`accountingstats`.`ControllingStateCd` AS `ControllingStateCd`,`start_develop_db`.`accountingstats`.`DividendPlanCd` AS `DividendPlanCd`,`start_develop_db`.`accountingstats`.`StatementAccountNumber` AS `StatementAccountNumber`,`start_develop_db`.`accountingstats`.`StatementAccountRef` AS `StatementAccountRef`,`start_develop_db`.`accountingstats`.`ConversionTemplateIdRef` AS `ConversionTemplateIdRef`,`start_develop_db`.`accountingstats`.`ConversionGroup` AS `ConversionGroup`,`start_develop_db`.`accountingstats`.`ConversionJobRef` AS `ConversionJobRef`,`start_develop_db`.`accountingstats`.`ConversionFileName` AS `ConversionFileName` from `start_develop_db`.`accountingstats` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `accountingsummarystats`
--

/*!50001 DROP VIEW IF EXISTS `accountingsummarystats`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `accountingsummarystats` AS select `start_develop_db`.`accountingsummarystats`.`SystemId` AS `SystemId`,`start_develop_db`.`accountingsummarystats`.`id` AS `id`,`start_develop_db`.`accountingsummarystats`.`StatSequence` AS `StatSequence`,`start_develop_db`.`accountingsummarystats`.`StatSequenceReplace` AS `StatSequenceReplace`,`start_develop_db`.`accountingsummarystats`.`ReportPeriod` AS `ReportPeriod`,`start_develop_db`.`accountingsummarystats`.`UpdateDt` AS `UpdateDt`,`start_develop_db`.`accountingsummarystats`.`PolicyRef` AS `PolicyRef`,`start_develop_db`.`accountingsummarystats`.`PolicyNumber` AS `PolicyNumber`,`start_develop_db`.`accountingsummarystats`.`PolicyVersion` AS `PolicyVersion`,`start_develop_db`.`accountingsummarystats`.`TransactionNumber` AS `TransactionNumber`,`start_develop_db`.`accountingsummarystats`.`LineCd` AS `LineCd`,`start_develop_db`.`accountingsummarystats`.`RiskCd` AS `RiskCd`,`start_develop_db`.`accountingsummarystats`.`RiskTypeCd` AS `RiskTypeCd`,`start_develop_db`.`accountingsummarystats`.`SubTypeCd` AS `SubTypeCd`,`start_develop_db`.`accountingsummarystats`.`CoverageCd` AS `CoverageCd`,`start_develop_db`.`accountingsummarystats`.`CoverageItemCd` AS `CoverageItemCd`,`start_develop_db`.`accountingsummarystats`.`FeeCd` AS `FeeCd`,`start_develop_db`.`accountingsummarystats`.`EffectiveDt` AS `EffectiveDt`,`start_develop_db`.`accountingsummarystats`.`ExpirationDt` AS `ExpirationDt`,`start_develop_db`.`accountingsummarystats`.`CombinedKey` AS `CombinedKey`,`start_develop_db`.`accountingsummarystats`.`AccountingDt` AS `AccountingDt`,`start_develop_db`.`accountingsummarystats`.`PolicyYear` AS `PolicyYear`,`start_develop_db`.`accountingsummarystats`.`InsuranceTypeCd` AS `InsuranceTypeCd`,`start_develop_db`.`accountingsummarystats`.`StatusCd` AS `StatusCd`,`start_develop_db`.`accountingsummarystats`.`NewRenewalCd` AS `NewRenewalCd`,`start_develop_db`.`accountingsummarystats`.`Limit1` AS `Limit1`,`start_develop_db`.`accountingsummarystats`.`Limit2` AS `Limit2`,`start_develop_db`.`accountingsummarystats`.`Limit3` AS `Limit3`,`start_develop_db`.`accountingsummarystats`.`Limit4` AS `Limit4`,`start_develop_db`.`accountingsummarystats`.`Limit5` AS `Limit5`,`start_develop_db`.`accountingsummarystats`.`Limit6` AS `Limit6`,`start_develop_db`.`accountingsummarystats`.`Limit7` AS `Limit7`,`start_develop_db`.`accountingsummarystats`.`Limit8` AS `Limit8`,`start_develop_db`.`accountingsummarystats`.`Limit9` AS `Limit9`,`start_develop_db`.`accountingsummarystats`.`Deductible1` AS `Deductible1`,`start_develop_db`.`accountingsummarystats`.`Deductible2` AS `Deductible2`,`start_develop_db`.`accountingsummarystats`.`AnnualStatementLineCd` AS `AnnualStatementLineCd`,`start_develop_db`.`accountingsummarystats`.`CoinsurancePct` AS `CoinsurancePct`,`start_develop_db`.`accountingsummarystats`.`CoveredPerilsCd` AS `CoveredPerilsCd`,`start_develop_db`.`accountingsummarystats`.`CommissionAreaCd` AS `CommissionAreaCd`,`start_develop_db`.`accountingsummarystats`.`MTDWrittenPremiumAmt` AS `MTDWrittenPremiumAmt`,`start_develop_db`.`accountingsummarystats`.`TTDWrittenPremiumAmt` AS `TTDWrittenPremiumAmt`,`start_develop_db`.`accountingsummarystats`.`YTDWrittenPremiumAmt` AS `YTDWrittenPremiumAmt`,`start_develop_db`.`accountingsummarystats`.`MTDWrittenCommissionAmt` AS `MTDWrittenCommissionAmt`,`start_develop_db`.`accountingsummarystats`.`YTDWrittenCommissionAmt` AS `YTDWrittenCommissionAmt`,`start_develop_db`.`accountingsummarystats`.`TTDWrittenCommissionAmt` AS `TTDWrittenCommissionAmt`,`start_develop_db`.`accountingsummarystats`.`MTDWrittenPremiumFeeAmt` AS `MTDWrittenPremiumFeeAmt`,`start_develop_db`.`accountingsummarystats`.`YTDWrittenPremiumFeeAmt` AS `YTDWrittenPremiumFeeAmt`,`start_develop_db`.`accountingsummarystats`.`TTDWrittenPremiumFeeAmt` AS `TTDWrittenPremiumFeeAmt`,`start_develop_db`.`accountingsummarystats`.`MTDWrittenCommissionFeeAmt` AS `MTDWrittenCommissionFeeAmt`,`start_develop_db`.`accountingsummarystats`.`YTDWrittenCommissionFeeAmt` AS `YTDWrittenCommissionFeeAmt`,`start_develop_db`.`accountingsummarystats`.`TTDWrittenCommissionFeeAmt` AS `TTDWrittenCommissionFeeAmt`,`start_develop_db`.`accountingsummarystats`.`InforceAmt` AS `InforceAmt`,`start_develop_db`.`accountingsummarystats`.`LastInforceAmt` AS `LastInforceAmt`,`start_develop_db`.`accountingsummarystats`.`MonthEarnedPremiumAmt` AS `MonthEarnedPremiumAmt`,`start_develop_db`.`accountingsummarystats`.`MTDEarnedPremiumAmt` AS `MTDEarnedPremiumAmt`,`start_develop_db`.`accountingsummarystats`.`YTDEarnedPremiumAmt` AS `YTDEarnedPremiumAmt`,`start_develop_db`.`accountingsummarystats`.`TTDEarnedPremiumAmt` AS `TTDEarnedPremiumAmt`,`start_develop_db`.`accountingsummarystats`.`PolicyInforceCount` AS `PolicyInforceCount`,`start_develop_db`.`accountingsummarystats`.`LocationInforceCount` AS `LocationInforceCount`,`start_develop_db`.`accountingsummarystats`.`RiskInforceCount` AS `RiskInforceCount`,`start_develop_db`.`accountingsummarystats`.`MonthUnearnedAmt` AS `MonthUnearnedAmt`,`start_develop_db`.`accountingsummarystats`.`UnearnedAmt` AS `UnearnedAmt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM00Amt` AS `UnearnedM00Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM01Amt` AS `UnearnedM01Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM02Amt` AS `UnearnedM02Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM03Amt` AS `UnearnedM03Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM04Amt` AS `UnearnedM04Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM05Amt` AS `UnearnedM05Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM06Amt` AS `UnearnedM06Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM07Amt` AS `UnearnedM07Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM08Amt` AS `UnearnedM08Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM09Amt` AS `UnearnedM09Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM10Amt` AS `UnearnedM10Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM11Amt` AS `UnearnedM11Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM12Amt` AS `UnearnedM12Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM13Amt` AS `UnearnedM13Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM14Amt` AS `UnearnedM14Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM15Amt` AS `UnearnedM15Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM16Amt` AS `UnearnedM16Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM17Amt` AS `UnearnedM17Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM18Amt` AS `UnearnedM18Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM19Amt` AS `UnearnedM19Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM20Amt` AS `UnearnedM20Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM21Amt` AS `UnearnedM21Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM22Amt` AS `UnearnedM22Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM23Amt` AS `UnearnedM23Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM24Amt` AS `UnearnedM24Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM25Amt` AS `UnearnedM25Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM26Amt` AS `UnearnedM26Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM27Amt` AS `UnearnedM27Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM28Amt` AS `UnearnedM28Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM29Amt` AS `UnearnedM29Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM30Amt` AS `UnearnedM30Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM31Amt` AS `UnearnedM31Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM32Amt` AS `UnearnedM32Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM33Amt` AS `UnearnedM33Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM34Amt` AS `UnearnedM34Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM35Amt` AS `UnearnedM35Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM36Amt` AS `UnearnedM36Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM37Amt` AS `UnearnedM37Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM38Amt` AS `UnearnedM38Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM39Amt` AS `UnearnedM39Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM40Amt` AS `UnearnedM40Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM41Amt` AS `UnearnedM41Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM42Amt` AS `UnearnedM42Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM43Amt` AS `UnearnedM43Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM44Amt` AS `UnearnedM44Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM45Amt` AS `UnearnedM45Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM46Amt` AS `UnearnedM46Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM47Amt` AS `UnearnedM47Amt`,`start_develop_db`.`accountingsummarystats`.`UnearnedM48Amt` AS `UnearnedM48Amt`,`start_develop_db`.`accountingsummarystats`.`RateAreaName` AS `RateAreaName`,`start_develop_db`.`accountingsummarystats`.`StatData` AS `StatData`,`start_develop_db`.`accountingsummarystats`.`CarrierGroupCd` AS `CarrierGroupCd`,`start_develop_db`.`accountingsummarystats`.`CarrierCd` AS `CarrierCd`,`start_develop_db`.`accountingsummarystats`.`ProductVersionIdRef` AS `ProductVersionIdRef`,`start_develop_db`.`accountingsummarystats`.`StateCd` AS `StateCd`,`start_develop_db`.`accountingsummarystats`.`BusinessSourceCd` AS `BusinessSourceCd`,`start_develop_db`.`accountingsummarystats`.`ProducerCd` AS `ProducerCd`,`start_develop_db`.`accountingsummarystats`.`TransactionCd` AS `TransactionCd`,`start_develop_db`.`accountingsummarystats`.`PolicyStatusCd` AS `PolicyStatusCd`,`start_develop_db`.`accountingsummarystats`.`ProductName` AS `ProductName`,`start_develop_db`.`accountingsummarystats`.`PolicyTypeCd` AS `PolicyTypeCd`,`start_develop_db`.`accountingsummarystats`.`PolicyGroupCd` AS `PolicyGroupCd`,`start_develop_db`.`accountingsummarystats`.`CustomerRef` AS `CustomerRef`,`start_develop_db`.`accountingsummarystats`.`ControllingProductVersionIdRef` AS `ControllingProductVersionIdRef`,`start_develop_db`.`accountingsummarystats`.`ControllingStateCd` AS `ControllingStateCd`,`start_develop_db`.`accountingsummarystats`.`DividendPlanCd` AS `DividendPlanCd`,`start_develop_db`.`accountingsummarystats`.`ConversionTemplateIdRef` AS `ConversionTemplateIdRef`,`start_develop_db`.`accountingsummarystats`.`ConversionGroup` AS `ConversionGroup`,`start_develop_db`.`accountingsummarystats`.`ConversionJobRef` AS `ConversionJobRef`,`start_develop_db`.`accountingsummarystats`.`ConversionFileName` AS `ConversionFileName` from `start_develop_db`.`accountingsummarystats` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `accountstats`
--

/*!50001 DROP VIEW IF EXISTS `accountstats`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `accountstats` AS select `start_develop_db`.`accountstats`.`SystemId` AS `SystemId`,`start_develop_db`.`accountstats`.`id` AS `id`,`start_develop_db`.`accountstats`.`StatSequence` AS `StatSequence`,`start_develop_db`.`accountstats`.`StatSequenceReplace` AS `StatSequenceReplace`,`start_develop_db`.`accountstats`.`PolicyNumber` AS `PolicyNumber`,`start_develop_db`.`accountstats`.`PolicyVersion` AS `PolicyVersion`,`start_develop_db`.`accountstats`.`BillingEntitySequenceNumber` AS `BillingEntitySequenceNumber`,`start_develop_db`.`accountstats`.`CombinedKey` AS `CombinedKey`,`start_develop_db`.`accountstats`.`AddDt` AS `AddDt`,`start_develop_db`.`accountstats`.`BookDt` AS `BookDt`,`start_develop_db`.`accountstats`.`StartDt` AS `StartDt`,`start_develop_db`.`accountstats`.`EndDt` AS `EndDt`,`start_develop_db`.`accountstats`.`StatusCd` AS `StatusCd`,`start_develop_db`.`accountstats`.`AccountNumber` AS `AccountNumber`,`start_develop_db`.`accountstats`.`AccountTypeCd` AS `AccountTypeCd`,`start_develop_db`.`accountstats`.`PayPlanCd` AS `PayPlanCd`,`start_develop_db`.`accountstats`.`TransactionTypeCd` AS `TransactionTypeCd`,`start_develop_db`.`accountstats`.`SourceCd` AS `SourceCd`,`start_develop_db`.`accountstats`.`TransactionNumber` AS `TransactionNumber`,`start_develop_db`.`accountstats`.`PolicyTransactionCd` AS `PolicyTransactionCd`,`start_develop_db`.`accountstats`.`ChargeAmt` AS `ChargeAmt`,`start_develop_db`.`accountstats`.`CommissionChargeAmt` AS `CommissionChargeAmt`,`start_develop_db`.`accountstats`.`BalanceAmt` AS `BalanceAmt`,`start_develop_db`.`accountstats`.`PaidAmt` AS `PaidAmt`,`start_develop_db`.`accountstats`.`AdjustmentAmt` AS `AdjustmentAmt`,`start_develop_db`.`accountstats`.`CategoryCd` AS `CategoryCd`,`start_develop_db`.`accountstats`.`NetPaidAmt` AS `NetPaidAmt`,`start_develop_db`.`accountstats`.`EffectiveDt` AS `EffectiveDt`,`start_develop_db`.`accountstats`.`ProviderCd` AS `ProviderCd`,`start_develop_db`.`accountstats`.`CheckNumber` AS `CheckNumber`,`start_develop_db`.`accountstats`.`CheckAmt` AS `CheckAmt`,`start_develop_db`.`accountstats`.`CheckDt` AS `CheckDt`,`start_develop_db`.`accountstats`.`BatchId` AS `BatchId`,`start_develop_db`.`accountstats`.`NextDueDt` AS `NextDueDt`,`start_develop_db`.`accountstats`.`InsuredName` AS `InsuredName`,`start_develop_db`.`accountstats`.`PaidBy` AS `PaidBy`,`start_develop_db`.`accountstats`.`PaidByName` AS `PaidByName`,`start_develop_db`.`accountstats`.`StatementTypeCd` AS `StatementTypeCd`,`start_develop_db`.`accountstats`.`ActivityTypeCd` AS `ActivityTypeCd`,`start_develop_db`.`accountstats`.`PolicyTransactionNumber` AS `PolicyTransactionNumber`,`start_develop_db`.`accountstats`.`CommisionPct` AS `CommisionPct`,`start_develop_db`.`accountstats`.`CommissionTypeCd` AS `CommissionTypeCd`,`start_develop_db`.`accountstats`.`AccountRef` AS `AccountRef`,`start_develop_db`.`accountstats`.`PolicyRef` AS `PolicyRef`,`start_develop_db`.`accountstats`.`PayOffAmt` AS `PayOffAmt`,`start_develop_db`.`accountstats`.`ReversalStopInd` AS `ReversalStopInd`,`start_develop_db`.`accountstats`.`CommissionKey` AS `CommissionKey`,`start_develop_db`.`accountstats`.`PolicyTransactionEffectiveDt` AS `PolicyTransactionEffectiveDt`,`start_develop_db`.`accountstats`.`AccountingDt` AS `AccountingDt`,`start_develop_db`.`accountstats`.`CarrierGroupCd` AS `CarrierGroupCd`,`start_develop_db`.`accountstats`.`CarrierCd` AS `CarrierCd`,`start_develop_db`.`accountstats`.`CustomerRef` AS `CustomerRef`,`start_develop_db`.`accountstats`.`BillMethod` AS `BillMethod`,`start_develop_db`.`accountstats`.`StateCd` AS `StateCd`,`start_develop_db`.`accountstats`.`DepositoryLocationCd` AS `DepositoryLocationCd`,`start_develop_db`.`accountstats`.`NetChargeAmt` AS `NetChargeAmt`,`start_develop_db`.`accountstats`.`NetAdjustmentAmt` AS `NetAdjustmentAmt`,`start_develop_db`.`accountstats`.`GrossNetInd` AS `GrossNetInd`,`start_develop_db`.`accountstats`.`NetBalanceAmt` AS `NetBalanceAmt`,`start_develop_db`.`accountstats`.`CommissionTakenAmt` AS `CommissionTakenAmt`,`start_develop_db`.`accountstats`.`TransactionDesc` AS `TransactionDesc`,`start_develop_db`.`accountstats`.`ReverseReason` AS `ReverseReason`,`start_develop_db`.`accountstats`.`EntryDt` AS `EntryDt`,`start_develop_db`.`accountstats`.`SystemCheckNumber` AS `SystemCheckNumber`,`start_develop_db`.`accountstats`.`AdjustmentTypeCd` AS `AdjustmentTypeCd`,`start_develop_db`.`accountstats`.`ACHAgent` AS `ACHAgent`,`start_develop_db`.`accountstats`.`SettlementDt` AS `SettlementDt`,`start_develop_db`.`accountstats`.`AuditAccountInd` AS `AuditAccountInd`,`start_develop_db`.`accountstats`.`StatementAccountNumber` AS `StatementAccountNumber`,`start_develop_db`.`accountstats`.`StatementAccountRef` AS `StatementAccountRef` from `start_develop_db`.`accountstats` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `accountsummarystats`
--

/*!50001 DROP VIEW IF EXISTS `accountsummarystats`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `accountsummarystats` AS select `start_develop_db`.`accountsummarystats`.`SystemId` AS `SystemId`,`start_develop_db`.`accountsummarystats`.`id` AS `id`,`start_develop_db`.`accountsummarystats`.`ReportPeriod` AS `ReportPeriod`,`start_develop_db`.`accountsummarystats`.`UpdateDt` AS `UpdateDt`,`start_develop_db`.`accountsummarystats`.`PolicyNumber` AS `PolicyNumber`,`start_develop_db`.`accountsummarystats`.`PolicyVersion` AS `PolicyVersion`,`start_develop_db`.`accountsummarystats`.`BillingEntitySequenceNumber` AS `BillingEntitySequenceNumber`,`start_develop_db`.`accountsummarystats`.`AccountNumber` AS `AccountNumber`,`start_develop_db`.`accountsummarystats`.`PolicyRef` AS `PolicyRef`,`start_develop_db`.`accountsummarystats`.`AccountRef` AS `AccountRef`,`start_develop_db`.`accountsummarystats`.`AccountTypeCd` AS `AccountTypeCd`,`start_develop_db`.`accountsummarystats`.`PayPlanCd` AS `PayPlanCd`,`start_develop_db`.`accountsummarystats`.`StatusCd` AS `StatusCd`,`start_develop_db`.`accountsummarystats`.`EffectiveDt` AS `EffectiveDt`,`start_develop_db`.`accountsummarystats`.`ProviderCd` AS `ProviderCd`,`start_develop_db`.`accountsummarystats`.`InsuredName` AS `InsuredName`,`start_develop_db`.`accountsummarystats`.`NextDueDt` AS `NextDueDt`,`start_develop_db`.`accountsummarystats`.`CommissionPct` AS `CommissionPct`,`start_develop_db`.`accountsummarystats`.`CommissionTypeCd` AS `CommissionTypeCd`,`start_develop_db`.`accountsummarystats`.`PremMTDChargeAmt` AS `PremMTDChargeAmt`,`start_develop_db`.`accountsummarystats`.`PremYTDChargeAmt` AS `PremYTDChargeAmt`,`start_develop_db`.`accountsummarystats`.`PremTTDChargeAmt` AS `PremTTDChargeAmt`,`start_develop_db`.`accountsummarystats`.`PremMTDCommissionChargeAmt` AS `PremMTDCommissionChargeAmt`,`start_develop_db`.`accountsummarystats`.`PremTTDCommissionChargeAmt` AS `PremTTDCommissionChargeAmt`,`start_develop_db`.`accountsummarystats`.`PremYTDCommissionChargeAmt` AS `PremYTDCommissionChargeAmt`,`start_develop_db`.`accountsummarystats`.`PremBalanceAmt` AS `PremBalanceAmt`,`start_develop_db`.`accountsummarystats`.`PremMTDPaidAmt` AS `PremMTDPaidAmt`,`start_develop_db`.`accountsummarystats`.`PremTTDPaidAmt` AS `PremTTDPaidAmt`,`start_develop_db`.`accountsummarystats`.`PremYTDPaidAmt` AS `PremYTDPaidAmt`,`start_develop_db`.`accountsummarystats`.`PremMTDAdjustmentAmt` AS `PremMTDAdjustmentAmt`,`start_develop_db`.`accountsummarystats`.`PremYTDAdjustmentAmt` AS `PremYTDAdjustmentAmt`,`start_develop_db`.`accountsummarystats`.`PremTTDAdjustmentAmt` AS `PremTTDAdjustmentAmt`,`start_develop_db`.`accountsummarystats`.`C1MTDChargeAmt` AS `C1MTDChargeAmt`,`start_develop_db`.`accountsummarystats`.`C1TTDChargeAmt` AS `C1TTDChargeAmt`,`start_develop_db`.`accountsummarystats`.`C1YTDChargeAmt` AS `C1YTDChargeAmt`,`start_develop_db`.`accountsummarystats`.`C1MTDCommissionChargeAmt` AS `C1MTDCommissionChargeAmt`,`start_develop_db`.`accountsummarystats`.`C1YTDCommissionChargeAmt` AS `C1YTDCommissionChargeAmt`,`start_develop_db`.`accountsummarystats`.`C1TTDCommissionChargeAmt` AS `C1TTDCommissionChargeAmt`,`start_develop_db`.`accountsummarystats`.`C1BalanceAmt` AS `C1BalanceAmt`,`start_develop_db`.`accountsummarystats`.`C1MTDPaidAmt` AS `C1MTDPaidAmt`,`start_develop_db`.`accountsummarystats`.`C1YTDPaidAmt` AS `C1YTDPaidAmt`,`start_develop_db`.`accountsummarystats`.`C1TTDPaidAmt` AS `C1TTDPaidAmt`,`start_develop_db`.`accountsummarystats`.`C1MTDAdjustmentAmt` AS `C1MTDAdjustmentAmt`,`start_develop_db`.`accountsummarystats`.`C1TTDAdjustmentAmt` AS `C1TTDAdjustmentAmt`,`start_develop_db`.`accountsummarystats`.`C1YTDAdjustmentAmt` AS `C1YTDAdjustmentAmt`,`start_develop_db`.`accountsummarystats`.`C2MTDChargeAmt` AS `C2MTDChargeAmt`,`start_develop_db`.`accountsummarystats`.`C2YTDChargeAmt` AS `C2YTDChargeAmt`,`start_develop_db`.`accountsummarystats`.`C2MTDCommissionChargeAmt` AS `C2MTDCommissionChargeAmt`,`start_develop_db`.`accountsummarystats`.`C2TTDChargeAmt` AS `C2TTDChargeAmt`,`start_develop_db`.`accountsummarystats`.`C2TTDCommissionChargeAmt` AS `C2TTDCommissionChargeAmt`,`start_develop_db`.`accountsummarystats`.`C2YTDCommissionChargeAmt` AS `C2YTDCommissionChargeAmt`,`start_develop_db`.`accountsummarystats`.`C2BalanceAmt` AS `C2BalanceAmt`,`start_develop_db`.`accountsummarystats`.`C2MTDPaidAmt` AS `C2MTDPaidAmt`,`start_develop_db`.`accountsummarystats`.`C2TTDPaidAmt` AS `C2TTDPaidAmt`,`start_develop_db`.`accountsummarystats`.`C2YTDPaidAmt` AS `C2YTDPaidAmt`,`start_develop_db`.`accountsummarystats`.`C2MTDAdjustmentAmt` AS `C2MTDAdjustmentAmt`,`start_develop_db`.`accountsummarystats`.`C2YTDAdjustmentAmt` AS `C2YTDAdjustmentAmt`,`start_develop_db`.`accountsummarystats`.`C2TTDAdjustmentAmt` AS `C2TTDAdjustmentAmt`,`start_develop_db`.`accountsummarystats`.`C3MTDChargeAmt` AS `C3MTDChargeAmt`,`start_develop_db`.`accountsummarystats`.`C3TTDChargeAmt` AS `C3TTDChargeAmt`,`start_develop_db`.`accountsummarystats`.`C3YTDChargeAmt` AS `C3YTDChargeAmt`,`start_develop_db`.`accountsummarystats`.`C3MTDCommissionChargeAmt` AS `C3MTDCommissionChargeAmt`,`start_develop_db`.`accountsummarystats`.`C3TTDCommissionChargeAmt` AS `C3TTDCommissionChargeAmt`,`start_develop_db`.`accountsummarystats`.`C3BalanceAmt` AS `C3BalanceAmt`,`start_develop_db`.`accountsummarystats`.`C3YTDCommissionChargeAmt` AS `C3YTDCommissionChargeAmt`,`start_develop_db`.`accountsummarystats`.`C3MTDPaidAmt` AS `C3MTDPaidAmt`,`start_develop_db`.`accountsummarystats`.`C3TTDPaidAmt` AS `C3TTDPaidAmt`,`start_develop_db`.`accountsummarystats`.`C3MTDAdjustmentAmt` AS `C3MTDAdjustmentAmt`,`start_develop_db`.`accountsummarystats`.`C3YTDPaidAmt` AS `C3YTDPaidAmt`,`start_develop_db`.`accountsummarystats`.`C3TTDAdjustmentAmt` AS `C3TTDAdjustmentAmt`,`start_develop_db`.`accountsummarystats`.`C3YTDAdjustmentAmt` AS `C3YTDAdjustmentAmt`,`start_develop_db`.`accountsummarystats`.`C4MTDChargeAmt` AS `C4MTDChargeAmt`,`start_develop_db`.`accountsummarystats`.`C4YTDChargeAmt` AS `C4YTDChargeAmt`,`start_develop_db`.`accountsummarystats`.`C4MTDCommissionChargeAmt` AS `C4MTDCommissionChargeAmt`,`start_develop_db`.`accountsummarystats`.`C4TTDChargeAmt` AS `C4TTDChargeAmt`,`start_develop_db`.`accountsummarystats`.`C4TTDCommissionChargeAmt` AS `C4TTDCommissionChargeAmt`,`start_develop_db`.`accountsummarystats`.`C4YTDCommissionChargeAmt` AS `C4YTDCommissionChargeAmt`,`start_develop_db`.`accountsummarystats`.`C4BalanceAmt` AS `C4BalanceAmt`,`start_develop_db`.`accountsummarystats`.`C4MTDPaidAmt` AS `C4MTDPaidAmt`,`start_develop_db`.`accountsummarystats`.`C4TTDPaidAmt` AS `C4TTDPaidAmt`,`start_develop_db`.`accountsummarystats`.`C4YTDPaidAmt` AS `C4YTDPaidAmt`,`start_develop_db`.`accountsummarystats`.`C4MTDAdjustmentAmt` AS `C4MTDAdjustmentAmt`,`start_develop_db`.`accountsummarystats`.`C4YTDAdjustmentAmt` AS `C4YTDAdjustmentAmt`,`start_develop_db`.`accountsummarystats`.`C4TTDAdjustmentAmt` AS `C4TTDAdjustmentAmt`,`start_develop_db`.`accountsummarystats`.`NSFCount` AS `NSFCount`,`start_develop_db`.`accountsummarystats`.`LateCount` AS `LateCount`,`start_develop_db`.`accountsummarystats`.`ReinstatementCount` AS `ReinstatementCount`,`start_develop_db`.`accountsummarystats`.`CancellationCount` AS `CancellationCount`,`start_develop_db`.`accountsummarystats`.`CancellationNoticeCount` AS `CancellationNoticeCount`,`start_develop_db`.`accountsummarystats`.`LessOr30AmtDueDt` AS `LessOr30AmtDueDt`,`start_develop_db`.`accountsummarystats`.`Over30AmtDueDt` AS `Over30AmtDueDt`,`start_develop_db`.`accountsummarystats`.`Over60AmtDueDt` AS `Over60AmtDueDt`,`start_develop_db`.`accountsummarystats`.`Over90AmtDueDt` AS `Over90AmtDueDt`,`start_develop_db`.`accountsummarystats`.`LessOr30AmtEffectiveDt` AS `LessOr30AmtEffectiveDt`,`start_develop_db`.`accountsummarystats`.`Over30AmtEffectiveDt` AS `Over30AmtEffectiveDt`,`start_develop_db`.`accountsummarystats`.`Over60AmtEffectiveDt` AS `Over60AmtEffectiveDt`,`start_develop_db`.`accountsummarystats`.`Over90AmtEffectiveDt` AS `Over90AmtEffectiveDt`,`start_develop_db`.`accountsummarystats`.`CarrierGroupCd` AS `CarrierGroupCd`,`start_develop_db`.`accountsummarystats`.`CarrierCd` AS `CarrierCd`,`start_develop_db`.`accountsummarystats`.`CustomerRef` AS `CustomerRef`,`start_develop_db`.`accountsummarystats`.`TotalMTDChargeAmt` AS `TotalMTDChargeAmt`,`start_develop_db`.`accountsummarystats`.`TotalTTDChargeAmt` AS `TotalTTDChargeAmt`,`start_develop_db`.`accountsummarystats`.`TotalYTDChargeAmt` AS `TotalYTDChargeAmt`,`start_develop_db`.`accountsummarystats`.`TotalMTDCommissionChargeAmt` AS `TotalMTDCommissionChargeAmt`,`start_develop_db`.`accountsummarystats`.`TotalTTDCommissionChargeAmt` AS `TotalTTDCommissionChargeAmt`,`start_develop_db`.`accountsummarystats`.`TotalYTDCommissionChargeAmt` AS `TotalYTDCommissionChargeAmt`,`start_develop_db`.`accountsummarystats`.`TotalMTDPaidAmt` AS `TotalMTDPaidAmt`,`start_develop_db`.`accountsummarystats`.`TotalYTDPaidAmt` AS `TotalYTDPaidAmt`,`start_develop_db`.`accountsummarystats`.`TotalTDDPaidAmt` AS `TotalTDDPaidAmt`,`start_develop_db`.`accountsummarystats`.`TotalMTDAdjustmentAmt` AS `TotalMTDAdjustmentAmt`,`start_develop_db`.`accountsummarystats`.`TotalTTDAdjustmentAmt` AS `TotalTTDAdjustmentAmt`,`start_develop_db`.`accountsummarystats`.`TotalYTDAdjustmentAmt` AS `TotalYTDAdjustmentAmt`,`start_develop_db`.`accountsummarystats`.`PremPayOffBalanceAmt` AS `PremPayOffBalanceAmt`,`start_develop_db`.`accountsummarystats`.`TotalBalanceAmt` AS `TotalBalanceAmt`,`start_develop_db`.`accountsummarystats`.`C1PayOffBalanceAmt` AS `C1PayOffBalanceAmt`,`start_develop_db`.`accountsummarystats`.`C2PayOffBalanceAmt` AS `C2PayOffBalanceAmt`,`start_develop_db`.`accountsummarystats`.`C3PayOffBalanceAmt` AS `C3PayOffBalanceAmt`,`start_develop_db`.`accountsummarystats`.`C4PayOffBalanceAmt` AS `C4PayOffBalanceAmt`,`start_develop_db`.`accountsummarystats`.`TotalPayOffBalanceAmt` AS `TotalPayOffBalanceAmt`,`start_develop_db`.`accountsummarystats`.`CurrentAmtDueDt` AS `CurrentAmtDueDt`,`start_develop_db`.`accountsummarystats`.`CurrentAmtEffectiveDt` AS `CurrentAmtEffectiveDt`,`start_develop_db`.`accountsummarystats`.`StatementTypeCd` AS `StatementTypeCd`,`start_develop_db`.`accountsummarystats`.`BillMethod` AS `BillMethod`,`start_develop_db`.`accountsummarystats`.`StateCd` AS `StateCd`,`start_develop_db`.`accountsummarystats`.`DepositoryLocationCd` AS `DepositoryLocationCd`,`start_develop_db`.`accountsummarystats`.`FutureChargeDueAmt` AS `FutureChargeDueAmt`,`start_develop_db`.`accountsummarystats`.`FuturePaidDueAmt` AS `FuturePaidDueAmt`,`start_develop_db`.`accountsummarystats`.`FutureBalanceDueAmt` AS `FutureBalanceDueAmt`,`start_develop_db`.`accountsummarystats`.`FutureChargeEffectiveAmt` AS `FutureChargeEffectiveAmt`,`start_develop_db`.`accountsummarystats`.`FuturePaidEffectiveAmt` AS `FuturePaidEffectiveAmt`,`start_develop_db`.`accountsummarystats`.`FutureBalanceEffectiveAmt` AS `FutureBalanceEffectiveAmt`,`start_develop_db`.`accountsummarystats`.`CurrentUnbilledAmtDueDt` AS `CurrentUnbilledAmtDueDt`,`start_develop_db`.`accountsummarystats`.`CurrentUnbilledAmtEffectiveDt` AS `CurrentUnbilledAmtEffectiveDt` from `start_develop_db`.`accountsummarystats` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `claimreinsurancestats`
--

/*!50001 DROP VIEW IF EXISTS `claimreinsurancestats`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `claimreinsurancestats` AS select `start_develop_db`.`claimreinsurancestats`.`SystemId` AS `SystemId`,`start_develop_db`.`claimreinsurancestats`.`id` AS `id`,`start_develop_db`.`claimreinsurancestats`.`StatSequence` AS `StatSequence`,`start_develop_db`.`claimreinsurancestats`.`StatSequenceReplace` AS `StatSequenceReplace`,`start_develop_db`.`claimreinsurancestats`.`PolicyRef` AS `PolicyRef`,`start_develop_db`.`claimreinsurancestats`.`PolicyNumber` AS `PolicyNumber`,`start_develop_db`.`claimreinsurancestats`.`PolicyVersion` AS `PolicyVersion`,`start_develop_db`.`claimreinsurancestats`.`TransactionNumber` AS `TransactionNumber`,`start_develop_db`.`claimreinsurancestats`.`EffectiveDt` AS `EffectiveDt`,`start_develop_db`.`claimreinsurancestats`.`ExpirationDt` AS `ExpirationDt`,`start_develop_db`.`claimreinsurancestats`.`CombinedKey` AS `CombinedKey`,`start_develop_db`.`claimreinsurancestats`.`AddDt` AS `AddDt`,`start_develop_db`.`claimreinsurancestats`.`BookDt` AS `BookDt`,`start_develop_db`.`claimreinsurancestats`.`PolicyYear` AS `PolicyYear`,`start_develop_db`.`claimreinsurancestats`.`StartDt` AS `StartDt`,`start_develop_db`.`claimreinsurancestats`.`EndDt` AS `EndDt`,`start_develop_db`.`claimreinsurancestats`.`StatusCd` AS `StatusCd`,`start_develop_db`.`claimreinsurancestats`.`ReinsuranceName` AS `ReinsuranceName`,`start_develop_db`.`claimreinsurancestats`.`ReinsuranceItemName` AS `ReinsuranceItemName`,`start_develop_db`.`claimreinsurancestats`.`ReserveCd` AS `ReserveCd`,`start_develop_db`.`claimreinsurancestats`.`ReserveTypeCd` AS `ReserveTypeCd`,`start_develop_db`.`claimreinsurancestats`.`MasterSubInd` AS `MasterSubInd`,`start_develop_db`.`claimreinsurancestats`.`TransactionCd` AS `TransactionCd`,`start_develop_db`.`claimreinsurancestats`.`AnnualStatementLineCd` AS `AnnualStatementLineCd`,`start_develop_db`.`claimreinsurancestats`.`StateCd` AS `StateCd`,`start_develop_db`.`claimreinsurancestats`.`ClaimNumber` AS `ClaimNumber`,`start_develop_db`.`claimreinsurancestats`.`ClaimRef` AS `ClaimRef`,`start_develop_db`.`claimreinsurancestats`.`CarrierCd` AS `CarrierCd`,`start_develop_db`.`claimreinsurancestats`.`CarrierGroupCd` AS `CarrierGroupCd`,`start_develop_db`.`claimreinsurancestats`.`LossYear` AS `LossYear`,`start_develop_db`.`claimreinsurancestats`.`ExpectedRecoveryChangeAmt` AS `ExpectedRecoveryChangeAmt`,`start_develop_db`.`claimreinsurancestats`.`ReserveChangeAmt` AS `ReserveChangeAmt`,`start_develop_db`.`claimreinsurancestats`.`PaidAmt` AS `PaidAmt`,`start_develop_db`.`claimreinsurancestats`.`PostedRecoveryAmt` AS `PostedRecoveryAmt`,`start_develop_db`.`claimreinsurancestats`.`ProductVersionIdRef` AS `ProductVersionIdRef`,`start_develop_db`.`claimreinsurancestats`.`LossDt` AS `LossDt`,`start_develop_db`.`claimreinsurancestats`.`ReportDt` AS `ReportDt`,`start_develop_db`.`claimreinsurancestats`.`LossCauseCd` AS `LossCauseCd`,`start_develop_db`.`claimreinsurancestats`.`SubLossCauseCd` AS `SubLossCauseCd`,`start_develop_db`.`claimreinsurancestats`.`ClaimStatusCd` AS `ClaimStatusCd`,`start_develop_db`.`claimreinsurancestats`.`ProductName` AS `ProductName`,`start_develop_db`.`claimreinsurancestats`.`PolicyProductVersionIdRef` AS `PolicyProductVersionIdRef`,`start_develop_db`.`claimreinsurancestats`.`PolicyProductName` AS `PolicyProductName`,`start_develop_db`.`claimreinsurancestats`.`PolicyTypeCd` AS `PolicyTypeCd`,`start_develop_db`.`claimreinsurancestats`.`PolicyGroupCd` AS `PolicyGroupCd`,`start_develop_db`.`claimreinsurancestats`.`ReversalStopInd` AS `ReversalStopInd`,`start_develop_db`.`claimreinsurancestats`.`CustomerRef` AS `CustomerRef`,`start_develop_db`.`claimreinsurancestats`.`SummaryKey` AS `SummaryKey`,`start_develop_db`.`claimreinsurancestats`.`RecoveryTransactionCd` AS `RecoveryTransactionCd`,`start_develop_db`.`claimreinsurancestats`.`CatastropheRef` AS `CatastropheRef`,`start_develop_db`.`claimreinsurancestats`.`RecoveryTransactionNumber` AS `RecoveryTransactionNumber`,`start_develop_db`.`claimreinsurancestats`.`CatastropheNumber` AS `CatastropheNumber`,`start_develop_db`.`claimreinsurancestats`.`HistoricReserveAmt` AS `HistoricReserveAmt`,`start_develop_db`.`claimreinsurancestats`.`HistoricExpectedRecoveryAmt` AS `HistoricExpectedRecoveryAmt`,`start_develop_db`.`claimreinsurancestats`.`HistoricPaidAmt` AS `HistoricPaidAmt`,`start_develop_db`.`claimreinsurancestats`.`ReinsuranceCoverageGroupCd` AS `ReinsuranceCoverageGroupCd`,`start_develop_db`.`claimreinsurancestats`.`ReinsuranceReserveCd` AS `ReinsuranceReserveCd`,`start_develop_db`.`claimreinsurancestats`.`CoverageTriggerCd` AS `CoverageTriggerCd`,`start_develop_db`.`claimreinsurancestats`.`AwarenessDt` AS `AwarenessDt` from `start_develop_db`.`claimreinsurancestats` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `claimreinsurancesummarystats`
--

/*!50001 DROP VIEW IF EXISTS `claimreinsurancesummarystats`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `claimreinsurancesummarystats` AS select `start_develop_db`.`claimreinsurancesummarystats`.`SystemId` AS `SystemId`,`start_develop_db`.`claimreinsurancesummarystats`.`id` AS `id`,`start_develop_db`.`claimreinsurancesummarystats`.`ReportPeriod` AS `ReportPeriod`,`start_develop_db`.`claimreinsurancesummarystats`.`UpdateDt` AS `UpdateDt`,`start_develop_db`.`claimreinsurancesummarystats`.`PolicyRef` AS `PolicyRef`,`start_develop_db`.`claimreinsurancesummarystats`.`PolicyNumber` AS `PolicyNumber`,`start_develop_db`.`claimreinsurancesummarystats`.`PolicyVersion` AS `PolicyVersion`,`start_develop_db`.`claimreinsurancesummarystats`.`TransactionNumber` AS `TransactionNumber`,`start_develop_db`.`claimreinsurancesummarystats`.`EffectiveDt` AS `EffectiveDt`,`start_develop_db`.`claimreinsurancesummarystats`.`ExpirationDt` AS `ExpirationDt`,`start_develop_db`.`claimreinsurancesummarystats`.`SummaryKey` AS `SummaryKey`,`start_develop_db`.`claimreinsurancesummarystats`.`PolicyYear` AS `PolicyYear`,`start_develop_db`.`claimreinsurancesummarystats`.`StatusCd` AS `StatusCd`,`start_develop_db`.`claimreinsurancesummarystats`.`ReinsuranceName` AS `ReinsuranceName`,`start_develop_db`.`claimreinsurancesummarystats`.`ReinsuranceItemName` AS `ReinsuranceItemName`,`start_develop_db`.`claimreinsurancesummarystats`.`ReserveCd` AS `ReserveCd`,`start_develop_db`.`claimreinsurancesummarystats`.`ReserveTypeCd` AS `ReserveTypeCd`,`start_develop_db`.`claimreinsurancesummarystats`.`MasterSubInd` AS `MasterSubInd`,`start_develop_db`.`claimreinsurancesummarystats`.`TransactionCd` AS `TransactionCd`,`start_develop_db`.`claimreinsurancesummarystats`.`AnnualStatementLineCd` AS `AnnualStatementLineCd`,`start_develop_db`.`claimreinsurancesummarystats`.`StateCd` AS `StateCd`,`start_develop_db`.`claimreinsurancesummarystats`.`ClaimNumber` AS `ClaimNumber`,`start_develop_db`.`claimreinsurancesummarystats`.`ClaimRef` AS `ClaimRef`,`start_develop_db`.`claimreinsurancesummarystats`.`CarrierCd` AS `CarrierCd`,`start_develop_db`.`claimreinsurancesummarystats`.`CarrierGroupCd` AS `CarrierGroupCd`,`start_develop_db`.`claimreinsurancesummarystats`.`LossYear` AS `LossYear`,`start_develop_db`.`claimreinsurancesummarystats`.`OutstandingAmt` AS `OutstandingAmt`,`start_develop_db`.`claimreinsurancesummarystats`.`MTDReserveChangeAmt` AS `MTDReserveChangeAmt`,`start_develop_db`.`claimreinsurancesummarystats`.`YTDReserveChangeAmt` AS `YTDReserveChangeAmt`,`start_develop_db`.`claimreinsurancesummarystats`.`OutstandingExpectedRecoveryAmt` AS `OutstandingExpectedRecoveryAmt`,`start_develop_db`.`claimreinsurancesummarystats`.`MTDExpectedRecoveryChangeAmt` AS `MTDExpectedRecoveryChangeAmt`,`start_develop_db`.`claimreinsurancesummarystats`.`YTDExpectedRecoveryChangeAmt` AS `YTDExpectedRecoveryChangeAmt`,`start_develop_db`.`claimreinsurancesummarystats`.`ITDPaidAmt` AS `ITDPaidAmt`,`start_develop_db`.`claimreinsurancesummarystats`.`MTDPaidAmt` AS `MTDPaidAmt`,`start_develop_db`.`claimreinsurancesummarystats`.`YTDPaidAmt` AS `YTDPaidAmt`,`start_develop_db`.`claimreinsurancesummarystats`.`ITDPostedRecoveryAmt` AS `ITDPostedRecoveryAmt`,`start_develop_db`.`claimreinsurancesummarystats`.`MTDPostedRecoveryAmt` AS `MTDPostedRecoveryAmt`,`start_develop_db`.`claimreinsurancesummarystats`.`YTDPostedRecoveryAmt` AS `YTDPostedRecoveryAmt`,`start_develop_db`.`claimreinsurancesummarystats`.`ProductVersionIdRef` AS `ProductVersionIdRef`,`start_develop_db`.`claimreinsurancesummarystats`.`LossDt` AS `LossDt`,`start_develop_db`.`claimreinsurancesummarystats`.`ReportDt` AS `ReportDt`,`start_develop_db`.`claimreinsurancesummarystats`.`LossCauseCd` AS `LossCauseCd`,`start_develop_db`.`claimreinsurancesummarystats`.`SubLossCauseCd` AS `SubLossCauseCd`,`start_develop_db`.`claimreinsurancesummarystats`.`ClaimStatusCd` AS `ClaimStatusCd`,`start_develop_db`.`claimreinsurancesummarystats`.`ProductName` AS `ProductName`,`start_develop_db`.`claimreinsurancesummarystats`.`PolicyProductVersionIdRef` AS `PolicyProductVersionIdRef`,`start_develop_db`.`claimreinsurancesummarystats`.`PolicyProductName` AS `PolicyProductName`,`start_develop_db`.`claimreinsurancesummarystats`.`PolicyTypeCd` AS `PolicyTypeCd`,`start_develop_db`.`claimreinsurancesummarystats`.`PolicyGroupCd` AS `PolicyGroupCd`,`start_develop_db`.`claimreinsurancesummarystats`.`MTDIncurredAmt` AS `MTDIncurredAmt`,`start_develop_db`.`claimreinsurancesummarystats`.`YTDIncurredAmt` AS `YTDIncurredAmt`,`start_develop_db`.`claimreinsurancesummarystats`.`ITDIncurredAmt` AS `ITDIncurredAmt`,`start_develop_db`.`claimreinsurancesummarystats`.`MTDIncurredNetRecoveryAmt` AS `MTDIncurredNetRecoveryAmt`,`start_develop_db`.`claimreinsurancesummarystats`.`YTDIncurredNetRecoveryAmt` AS `YTDIncurredNetRecoveryAmt`,`start_develop_db`.`claimreinsurancesummarystats`.`ITDIncurredNetRecoveryAmt` AS `ITDIncurredNetRecoveryAmt`,`start_develop_db`.`claimreinsurancesummarystats`.`CustomerRef` AS `CustomerRef`,`start_develop_db`.`claimreinsurancesummarystats`.`CatastropheRef` AS `CatastropheRef`,`start_develop_db`.`claimreinsurancesummarystats`.`CatastropheNumber` AS `CatastropheNumber`,`start_develop_db`.`claimreinsurancesummarystats`.`HistoricPaidAmt` AS `HistoricPaidAmt`,`start_develop_db`.`claimreinsurancesummarystats`.`ReinsuranceCoverageGroupCd` AS `ReinsuranceCoverageGroupCd`,`start_develop_db`.`claimreinsurancesummarystats`.`ReinsuranceReserveCd` AS `ReinsuranceReserveCd`,`start_develop_db`.`claimreinsurancesummarystats`.`CoverageTriggerCd` AS `CoverageTriggerCd`,`start_develop_db`.`claimreinsurancesummarystats`.`AwarenessDt` AS `AwarenessDt` from `start_develop_db`.`claimreinsurancesummarystats` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `claimstats`
--

/*!50001 DROP VIEW IF EXISTS `claimstats`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `claimstats` AS select `start_develop_db`.`claimstats`.`SystemId` AS `SystemId`,`start_develop_db`.`claimstats`.`id` AS `id`,`start_develop_db`.`claimstats`.`StatSequence` AS `StatSequence`,`start_develop_db`.`claimstats`.`StatSequenceReplace` AS `StatSequenceReplace`,`start_develop_db`.`claimstats`.`CombinedKey` AS `CombinedKey`,`start_develop_db`.`claimstats`.`PolicyNumber` AS `PolicyNumber`,`start_develop_db`.`claimstats`.`PolicyVersion` AS `PolicyVersion`,`start_develop_db`.`claimstats`.`PolicyRef` AS `PolicyRef`,`start_develop_db`.`claimstats`.`LineCd` AS `LineCd`,`start_develop_db`.`claimstats`.`RiskCd` AS `RiskCd`,`start_develop_db`.`claimstats`.`CoverageCd` AS `CoverageCd`,`start_develop_db`.`claimstats`.`CoverageItemCd` AS `CoverageItemCd`,`start_develop_db`.`claimstats`.`PolicyLimit` AS `PolicyLimit`,`start_develop_db`.`claimstats`.`PolicyDeductible` AS `PolicyDeductible`,`start_develop_db`.`claimstats`.`Limit` AS `Limit`,`start_develop_db`.`claimstats`.`Deductible` AS `Deductible`,`start_develop_db`.`claimstats`.`LimitDescription` AS `LimitDescription`,`start_develop_db`.`claimstats`.`DeductibleDescription` AS `DeductibleDescription`,`start_develop_db`.`claimstats`.`CarrierCd` AS `CarrierCd`,`start_develop_db`.`claimstats`.`CarrierGroupCd` AS `CarrierGroupCd`,`start_develop_db`.`claimstats`.`ClaimNumber` AS `ClaimNumber`,`start_develop_db`.`claimstats`.`ClaimRef` AS `ClaimRef`,`start_develop_db`.`claimstats`.`ClaimantCd` AS `ClaimantCd`,`start_develop_db`.`claimstats`.`FeatureCd` AS `FeatureCd`,`start_develop_db`.`claimstats`.`FeatureSubCd` AS `FeatureSubCd`,`start_develop_db`.`claimstats`.`FeatureTypeCd` AS `FeatureTypeCd`,`start_develop_db`.`claimstats`.`ItemNumber` AS `ItemNumber`,`start_develop_db`.`claimstats`.`ReserveCd` AS `ReserveCd`,`start_develop_db`.`claimstats`.`ReserveTypeCd` AS `ReserveTypeCd`,`start_develop_db`.`claimstats`.`TransactionCd` AS `TransactionCd`,`start_develop_db`.`claimstats`.`TransactionNumber` AS `TransactionNumber`,`start_develop_db`.`claimstats`.`ClaimantTransactionCd` AS `ClaimantTransactionCd`,`start_develop_db`.`claimstats`.`ClaimantTransactionNumber` AS `ClaimantTransactionNumber`,`start_develop_db`.`claimstats`.`ClaimantTransactionIdRef` AS `ClaimantTransactionIdRef`,`start_develop_db`.`claimstats`.`EffectiveDt` AS `EffectiveDt`,`start_develop_db`.`claimstats`.`ExpirationDt` AS `ExpirationDt`,`start_develop_db`.`claimstats`.`AddDt` AS `AddDt`,`start_develop_db`.`claimstats`.`BookDt` AS `BookDt`,`start_develop_db`.`claimstats`.`PolicyYear` AS `PolicyYear`,`start_develop_db`.`claimstats`.`LossYear` AS `LossYear`,`start_develop_db`.`claimstats`.`StateCd` AS `StateCd`,`start_develop_db`.`claimstats`.`AnnualStatementLineCd` AS `AnnualStatementLineCd`,`start_develop_db`.`claimstats`.`ProducerProviderCd` AS `ProducerProviderCd`,`start_develop_db`.`claimstats`.`ProducerProviderRef` AS `ProducerProviderRef`,`start_develop_db`.`claimstats`.`InsuranceTypeCd` AS `InsuranceTypeCd`,`start_develop_db`.`claimstats`.`ReserveChangeAmt` AS `ReserveChangeAmt`,`start_develop_db`.`claimstats`.`ExpectedRecoveryChangeAmt` AS `ExpectedRecoveryChangeAmt`,`start_develop_db`.`claimstats`.`PaidAmt` AS `PaidAmt`,`start_develop_db`.`claimstats`.`PostedRecoveryAmt` AS `PostedRecoveryAmt`,`start_develop_db`.`claimstats`.`AdjusterProviderCd` AS `AdjusterProviderCd`,`start_develop_db`.`claimstats`.`AdjusterProviderRef` AS `AdjusterProviderRef`,`start_develop_db`.`claimstats`.`ExaminerProviderCd` AS `ExaminerProviderCd`,`start_develop_db`.`claimstats`.`ExaminerProviderRef` AS `ExaminerProviderRef`,`start_develop_db`.`claimstats`.`BranchCd` AS `BranchCd`,`start_develop_db`.`claimstats`.`PayToName` AS `PayToName`,`start_develop_db`.`claimstats`.`PaymentAccountCd` AS `PaymentAccountCd`,`start_develop_db`.`claimstats`.`StartDt` AS `StartDt`,`start_develop_db`.`claimstats`.`EndDt` AS `EndDt`,`start_develop_db`.`claimstats`.`StatusCd` AS `StatusCd`,`start_develop_db`.`claimstats`.`RateAreaName` AS `RateAreaName`,`start_develop_db`.`claimstats`.`StatData` AS `StatData`,`start_develop_db`.`claimstats`.`ClaimStatusCd` AS `ClaimStatusCd`,`start_develop_db`.`claimstats`.`ClaimantStatusCd` AS `ClaimantStatusCd`,`start_develop_db`.`claimstats`.`FeatureStatusCd` AS `FeatureStatusCd`,`start_develop_db`.`claimstats`.`ReserveStatusCd` AS `ReserveStatusCd`,`start_develop_db`.`claimstats`.`LossDt` AS `LossDt`,`start_develop_db`.`claimstats`.`ReportDt` AS `ReportDt`,`start_develop_db`.`claimstats`.`ClaimStatusChgInd` AS `ClaimStatusChgInd`,`start_develop_db`.`claimstats`.`ClaimantStatusChgInd` AS `ClaimantStatusChgInd`,`start_develop_db`.`claimstats`.`FeatureStatusChgInd` AS `FeatureStatusChgInd`,`start_develop_db`.`claimstats`.`ReserveStatusChgInd` AS `ReserveStatusChgInd`,`start_develop_db`.`claimstats`.`SummaryKey` AS `SummaryKey`,`start_develop_db`.`claimstats`.`ProductVersionIdRef` AS `ProductVersionIdRef`,`start_develop_db`.`claimstats`.`LossCauseCd` AS `LossCauseCd`,`start_develop_db`.`claimstats`.`SubLossCauseCd` AS `SubLossCauseCd`,`start_develop_db`.`claimstats`.`ProductName` AS `ProductName`,`start_develop_db`.`claimstats`.`PolicyProductVersionIdRef` AS `PolicyProductVersionIdRef`,`start_develop_db`.`claimstats`.`PolicyProductName` AS `PolicyProductName`,`start_develop_db`.`claimstats`.`PolicyTypeCd` AS `PolicyTypeCd`,`start_develop_db`.`claimstats`.`PolicyGroupCd` AS `PolicyGroupCd`,`start_develop_db`.`claimstats`.`ReversalStopInd` AS `ReversalStopInd`,`start_develop_db`.`claimstats`.`CustomerRef` AS `CustomerRef`,`start_develop_db`.`claimstats`.`AggregateLimit` AS `AggregateLimit`,`start_develop_db`.`claimstats`.`AggregateLimitDescription` AS `AggregateLimitDescription`,`start_develop_db`.`claimstats`.`CheckDt` AS `CheckDt`,`start_develop_db`.`claimstats`.`CheckNumber` AS `CheckNumber`,`start_develop_db`.`claimstats`.`CheckAmt` AS `CheckAmt`,`start_develop_db`.`claimstats`.`CatastropheRef` AS `CatastropheRef`,`start_develop_db`.`claimstats`.`CatastropheNumber` AS `CatastropheNumber`,`start_develop_db`.`claimstats`.`PropertyDamagedIdRef` AS `PropertyDamagedIdRef`,`start_develop_db`.`claimstats`.`PropertyDamagedNumber` AS `PropertyDamagedNumber`,`start_develop_db`.`claimstats`.`RecordOnly` AS `RecordOnly`,`start_develop_db`.`claimstats`.`FileReasonCd` AS `FileReasonCd`,`start_develop_db`.`claimstats`.`SystemCheckReference` AS `SystemCheckReference`,`start_develop_db`.`claimstats`.`ClaimantLinkIdRef` AS `ClaimantLinkIdRef`,`start_develop_db`.`claimstats`.`ServicePeriodStartDt` AS `ServicePeriodStartDt`,`start_develop_db`.`claimstats`.`ServicePeriodEndDt` AS `ServicePeriodEndDt`,`start_develop_db`.`claimstats`.`HistoricReserveAmt` AS `HistoricReserveAmt`,`start_develop_db`.`claimstats`.`HistoricExpectedRecoveryAmt` AS `HistoricExpectedRecoveryAmt`,`start_develop_db`.`claimstats`.`HistoricPaidAmt` AS `HistoricPaidAmt`,`start_develop_db`.`claimstats`.`HistoricPostedRecoveryAmt` AS `HistoricPostedRecoveryAmt`,`start_develop_db`.`claimstats`.`ReasonCd` AS `ReasonCd`,`start_develop_db`.`claimstats`.`DenyTypeCd` AS `DenyTypeCd`,`start_develop_db`.`claimstats`.`DenyReasonCd` AS `DenyReasonCd`,`start_develop_db`.`claimstats`.`ClaimantMaritalStatusCd` AS `ClaimantMaritalStatusCd`,`start_develop_db`.`claimstats`.`ClaimantNumberOfDependents` AS `ClaimantNumberOfDependents`,`start_develop_db`.`claimstats`.`ClaimantNatureCd` AS `ClaimantNatureCd`,`start_develop_db`.`claimstats`.`ClaimantBodyPartGeneralCd` AS `ClaimantBodyPartGeneralCd`,`start_develop_db`.`claimstats`.`ClaimantBodyPartSpecificCd` AS `ClaimantBodyPartSpecificCd`,`start_develop_db`.`claimstats`.`ClaimantBodyPartIAIABCCd` AS `ClaimantBodyPartIAIABCCd`,`start_develop_db`.`claimstats`.`ClaimantInitialTreatment` AS `ClaimantInitialTreatment`,`start_develop_db`.`claimstats`.`ClaimantDisabilityTypeCd` AS `ClaimantDisabilityTypeCd`,`start_develop_db`.`claimstats`.`ClaimantClassCd` AS `ClaimantClassCd`,`start_develop_db`.`claimstats`.`ClaimantEmploymentStatusCd` AS `ClaimantEmploymentStatusCd`,`start_develop_db`.`claimstats`.`ClaimantShiftCd` AS `ClaimantShiftCd`,`start_develop_db`.`claimstats`.`ClaimantHourlySalaryCd` AS `ClaimantHourlySalaryCd`,`start_develop_db`.`claimstats`.`ClaimantAverageWeeklyWagesAmt` AS `ClaimantAverageWeeklyWagesAmt`,`start_develop_db`.`claimstats`.`ClaimantHireDt` AS `ClaimantHireDt`,`start_develop_db`.`claimstats`.`ClaimantLastWorkedDt` AS `ClaimantLastWorkedDt`,`start_develop_db`.`claimstats`.`CoverageTriggerCd` AS `CoverageTriggerCd`,`start_develop_db`.`claimstats`.`AwarenessDt` AS `AwarenessDt` from `start_develop_db`.`claimstats` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `claimsummarystats`
--

/*!50001 DROP VIEW IF EXISTS `claimsummarystats`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `claimsummarystats` AS select `start_develop_db`.`claimsummarystats`.`SystemId` AS `SystemId`,`start_develop_db`.`claimsummarystats`.`id` AS `id`,`start_develop_db`.`claimsummarystats`.`ReportPeriod` AS `ReportPeriod`,`start_develop_db`.`claimsummarystats`.`UpdateDt` AS `UpdateDt`,`start_develop_db`.`claimsummarystats`.`SummaryKey` AS `SummaryKey`,`start_develop_db`.`claimsummarystats`.`PolicyNumber` AS `PolicyNumber`,`start_develop_db`.`claimsummarystats`.`PolicyVersion` AS `PolicyVersion`,`start_develop_db`.`claimsummarystats`.`PolicyRef` AS `PolicyRef`,`start_develop_db`.`claimsummarystats`.`LineCd` AS `LineCd`,`start_develop_db`.`claimsummarystats`.`RiskCd` AS `RiskCd`,`start_develop_db`.`claimsummarystats`.`CoverageCd` AS `CoverageCd`,`start_develop_db`.`claimsummarystats`.`CoverageItemCd` AS `CoverageItemCd`,`start_develop_db`.`claimsummarystats`.`PolicyLimit` AS `PolicyLimit`,`start_develop_db`.`claimsummarystats`.`PolicyDeductible` AS `PolicyDeductible`,`start_develop_db`.`claimsummarystats`.`Limit` AS `Limit`,`start_develop_db`.`claimsummarystats`.`Deductible` AS `Deductible`,`start_develop_db`.`claimsummarystats`.`LimitDescription` AS `LimitDescription`,`start_develop_db`.`claimsummarystats`.`DeductibleDescription` AS `DeductibleDescription`,`start_develop_db`.`claimsummarystats`.`CarrierCd` AS `CarrierCd`,`start_develop_db`.`claimsummarystats`.`CarrierGroupCd` AS `CarrierGroupCd`,`start_develop_db`.`claimsummarystats`.`ClaimNumber` AS `ClaimNumber`,`start_develop_db`.`claimsummarystats`.`ClaimRef` AS `ClaimRef`,`start_develop_db`.`claimsummarystats`.`ClaimantCd` AS `ClaimantCd`,`start_develop_db`.`claimsummarystats`.`FeatureCd` AS `FeatureCd`,`start_develop_db`.`claimsummarystats`.`FeatureSubCd` AS `FeatureSubCd`,`start_develop_db`.`claimsummarystats`.`FeatureTypeCd` AS `FeatureTypeCd`,`start_develop_db`.`claimsummarystats`.`ItemNumber` AS `ItemNumber`,`start_develop_db`.`claimsummarystats`.`ReserveCd` AS `ReserveCd`,`start_develop_db`.`claimsummarystats`.`SubReserveCd` AS `SubReserveCd`,`start_develop_db`.`claimsummarystats`.`ReserveTypeCd` AS `ReserveTypeCd`,`start_develop_db`.`claimsummarystats`.`StatusCd` AS `StatusCd`,`start_develop_db`.`claimsummarystats`.`EffectiveDt` AS `EffectiveDt`,`start_develop_db`.`claimsummarystats`.`ExpirationDt` AS `ExpirationDt`,`start_develop_db`.`claimsummarystats`.`LossYear` AS `LossYear`,`start_develop_db`.`claimsummarystats`.`PolicyYear` AS `PolicyYear`,`start_develop_db`.`claimsummarystats`.`AnnualStatementLineCd` AS `AnnualStatementLineCd`,`start_develop_db`.`claimsummarystats`.`StateCd` AS `StateCd`,`start_develop_db`.`claimsummarystats`.`InsuranceTypeCd` AS `InsuranceTypeCd`,`start_develop_db`.`claimsummarystats`.`LitigationInd` AS `LitigationInd`,`start_develop_db`.`claimsummarystats`.`LossDt` AS `LossDt`,`start_develop_db`.`claimsummarystats`.`ReportDt` AS `ReportDt`,`start_develop_db`.`claimsummarystats`.`FirstClosedDt` AS `FirstClosedDt`,`start_develop_db`.`claimsummarystats`.`OpenedDt` AS `OpenedDt`,`start_develop_db`.`claimsummarystats`.`ClosedDt` AS `ClosedDt`,`start_develop_db`.`claimsummarystats`.`DenialDt` AS `DenialDt`,`start_develop_db`.`claimsummarystats`.`FirstIndemnityPaymentDt` AS `FirstIndemnityPaymentDt`,`start_develop_db`.`claimsummarystats`.`MTDReserveChangeAmt` AS `MTDReserveChangeAmt`,`start_develop_db`.`claimsummarystats`.`OutstandingAmt` AS `OutstandingAmt`,`start_develop_db`.`claimsummarystats`.`YTDReserveChangeAmt` AS `YTDReserveChangeAmt`,`start_develop_db`.`claimsummarystats`.`IncreaseDecreaseAmt` AS `IncreaseDecreaseAmt`,`start_develop_db`.`claimsummarystats`.`MTDExpectedRecoveryChangeAmt` AS `MTDExpectedRecoveryChangeAmt`,`start_develop_db`.`claimsummarystats`.`OutstandingExpectedRecoveryAmt` AS `OutstandingExpectedRecoveryAmt`,`start_develop_db`.`claimsummarystats`.`YTDExpectedRecoveryChangeAmt` AS `YTDExpectedRecoveryChangeAmt`,`start_develop_db`.`claimsummarystats`.`MTDPaidAmt` AS `MTDPaidAmt`,`start_develop_db`.`claimsummarystats`.`YTDPaidAmt` AS `YTDPaidAmt`,`start_develop_db`.`claimsummarystats`.`ITDPaidAmt` AS `ITDPaidAmt`,`start_develop_db`.`claimsummarystats`.`MTDPostedRecoveryAmt` AS `MTDPostedRecoveryAmt`,`start_develop_db`.`claimsummarystats`.`ITDPostedRecoveryAmt` AS `ITDPostedRecoveryAmt`,`start_develop_db`.`claimsummarystats`.`YTDPostedRecoveryAmt` AS `YTDPostedRecoveryAmt`,`start_develop_db`.`claimsummarystats`.`OpeningReserve` AS `OpeningReserve`,`start_develop_db`.`claimsummarystats`.`OriginalReserve` AS `OriginalReserve`,`start_develop_db`.`claimsummarystats`.`OpenedInPeriodCount` AS `OpenedInPeriodCount`,`start_develop_db`.`claimsummarystats`.`ReOpenedInPeriodCount` AS `ReOpenedInPeriodCount`,`start_develop_db`.`claimsummarystats`.`ClosedInPeriodCount` AS `ClosedInPeriodCount`,`start_develop_db`.`claimsummarystats`.`OpenEndOfPeriodCount` AS `OpenEndOfPeriodCount`,`start_develop_db`.`claimsummarystats`.`ClaimantOpenCount` AS `ClaimantOpenCount`,`start_develop_db`.`claimsummarystats`.`ClaimOpenCount` AS `ClaimOpenCount`,`start_develop_db`.`claimsummarystats`.`FeatureOpenCount` AS `FeatureOpenCount`,`start_develop_db`.`claimsummarystats`.`ClaimLastCloseDt` AS `ClaimLastCloseDt`,`start_develop_db`.`claimsummarystats`.`ClaimantLastCloseDt` AS `ClaimantLastCloseDt`,`start_develop_db`.`claimsummarystats`.`FeatureLastCloseDt` AS `FeatureLastCloseDt`,`start_develop_db`.`claimsummarystats`.`ReserveLastCloseDt` AS `ReserveLastCloseDt`,`start_develop_db`.`claimsummarystats`.`ProductVersionIdRef` AS `ProductVersionIdRef`,`start_develop_db`.`claimsummarystats`.`LossCauseCd` AS `LossCauseCd`,`start_develop_db`.`claimsummarystats`.`SubLossCauseCd` AS `SubLossCauseCd`,`start_develop_db`.`claimsummarystats`.`ProducerProviderCd` AS `ProducerProviderCd`,`start_develop_db`.`claimsummarystats`.`ClaimStatusCd` AS `ClaimStatusCd`,`start_develop_db`.`claimsummarystats`.`ProductName` AS `ProductName`,`start_develop_db`.`claimsummarystats`.`PolicyProductVersionIdRef` AS `PolicyProductVersionIdRef`,`start_develop_db`.`claimsummarystats`.`PolicyProductName` AS `PolicyProductName`,`start_develop_db`.`claimsummarystats`.`PolicyTypeCd` AS `PolicyTypeCd`,`start_develop_db`.`claimsummarystats`.`PolicyGroupCd` AS `PolicyGroupCd`,`start_develop_db`.`claimsummarystats`.`MTDIncurredAmt` AS `MTDIncurredAmt`,`start_develop_db`.`claimsummarystats`.`YTDIncurredAmt` AS `YTDIncurredAmt`,`start_develop_db`.`claimsummarystats`.`ITDIncurredAmt` AS `ITDIncurredAmt`,`start_develop_db`.`claimsummarystats`.`MTDIncurredNetRecoveryAmt` AS `MTDIncurredNetRecoveryAmt`,`start_develop_db`.`claimsummarystats`.`YTDIncurredNetRecoveryAmt` AS `YTDIncurredNetRecoveryAmt`,`start_develop_db`.`claimsummarystats`.`ITDIncurredNetRecoveryAmt` AS `ITDIncurredNetRecoveryAmt`,`start_develop_db`.`claimsummarystats`.`CustomerRef` AS `CustomerRef`,`start_develop_db`.`claimsummarystats`.`AggregateLimit` AS `AggregateLimit`,`start_develop_db`.`claimsummarystats`.`AggregateLimitDescription` AS `AggregateLimitDescription`,`start_develop_db`.`claimsummarystats`.`CatastropheRef` AS `CatastropheRef`,`start_develop_db`.`claimsummarystats`.`CatastropheNumber` AS `CatastropheNumber`,`start_develop_db`.`claimsummarystats`.`PropertyDamagedIdRef` AS `PropertyDamagedIdRef`,`start_develop_db`.`claimsummarystats`.`PropertyDamagedNumber` AS `PropertyDamagedNumber`,`start_develop_db`.`claimsummarystats`.`RecordOnly` AS `RecordOnly`,`start_develop_db`.`claimsummarystats`.`FileReasonCd` AS `FileReasonCd`,`start_develop_db`.`claimsummarystats`.`ClaimantLinkIdRef` AS `ClaimantLinkIdRef`,`start_develop_db`.`claimsummarystats`.`HistoricPaidAmt` AS `HistoricPaidAmt`,`start_develop_db`.`claimsummarystats`.`HistoricPostedRecoveryAmt` AS `HistoricPostedRecoveryAmt`,`start_develop_db`.`claimsummarystats`.`DenyTypeCd` AS `DenyTypeCd`,`start_develop_db`.`claimsummarystats`.`DenyReasonCd` AS `DenyReasonCd`,`start_develop_db`.`claimsummarystats`.`CoverageTriggerCd` AS `CoverageTriggerCd`,`start_develop_db`.`claimsummarystats`.`AwarenessDt` AS `AwarenessDt` from `start_develop_db`.`claimsummarystats` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `payablestats`
--

/*!50001 DROP VIEW IF EXISTS `payablestats`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `payablestats` AS select `start_develop_db`.`payablestats`.`SystemId` AS `SystemId`,`start_develop_db`.`payablestats`.`id` AS `id`,`start_develop_db`.`payablestats`.`StatusCd` AS `StatusCd`,`start_develop_db`.`payablestats`.`CombinedKey` AS `CombinedKey`,`start_develop_db`.`payablestats`.`PaymentSystemId` AS `PaymentSystemId`,`start_develop_db`.`payablestats`.`StatSequence` AS `StatSequence`,`start_develop_db`.`payablestats`.`StatCd` AS `StatCd`,`start_develop_db`.`payablestats`.`StatSequenceReplace` AS `StatSequenceReplace`,`start_develop_db`.`payablestats`.`AddDt` AS `AddDt`,`start_develop_db`.`payablestats`.`PaymentAccountCd` AS `PaymentAccountCd`,`start_develop_db`.`payablestats`.`TypeCd` AS `TypeCd`,`start_develop_db`.`payablestats`.`PayToName` AS `PayToName`,`start_develop_db`.`payablestats`.`ProviderCd` AS `ProviderCd`,`start_develop_db`.`payablestats`.`PaymentStatusCd` AS `PaymentStatusCd`,`start_develop_db`.`payablestats`.`ProviderRef` AS `ProviderRef`,`start_develop_db`.`payablestats`.`ItemAmt` AS `ItemAmt`,`start_develop_db`.`payablestats`.`ItemNumber` AS `ItemNumber`,`start_develop_db`.`payablestats`.`ItemDt` AS `ItemDt`,`start_develop_db`.`payablestats`.`PrinterTemplateIdRef` AS `PrinterTemplateIdRef`,`start_develop_db`.`payablestats`.`ClassificationCd` AS `ClassificationCd`,`start_develop_db`.`payablestats`.`RequestDt` AS `RequestDt`,`start_develop_db`.`payablestats`.`BookDt` AS `BookDt`,`start_develop_db`.`payablestats`.`SourceCd` AS `SourceCd`,`start_develop_db`.`payablestats`.`SourceRef` AS `SourceRef`,`start_develop_db`.`payablestats`.`AllocationAmt` AS `AllocationAmt`,`start_develop_db`.`payablestats`.`TransactionTypeCd` AS `TransactionTypeCd`,`start_develop_db`.`payablestats`.`TransactionDt` AS `TransactionDt`,`start_develop_db`.`payablestats`.`TransactionTm` AS `TransactionTm`,`start_develop_db`.`payablestats`.`TransactionUser` AS `TransactionUser`,`start_develop_db`.`payablestats`.`TransactionAmt` AS `TransactionAmt`,`start_develop_db`.`payablestats`.`TransactionNumber` AS `TransactionNumber`,`start_develop_db`.`payablestats`.`AccountNumber` AS `AccountNumber`,`start_develop_db`.`payablestats`.`AccountTransReference` AS `AccountTransReference`,`start_develop_db`.`payablestats`.`AccountTransIdRef` AS `AccountTransIdRef`,`start_develop_db`.`payablestats`.`PolicyNumber` AS `PolicyNumber`,`start_develop_db`.`payablestats`.`PolicyProviderRef` AS `PolicyProviderRef`,`start_develop_db`.`payablestats`.`LossDt` AS `LossDt`,`start_develop_db`.`payablestats`.`CombinePaymentInd` AS `CombinePaymentInd`,`start_develop_db`.`payablestats`.`ClaimantNumber` AS `ClaimantNumber`,`start_develop_db`.`payablestats`.`ClaimNumber` AS `ClaimNumber`,`start_develop_db`.`payablestats`.`SequenceNumber` AS `SequenceNumber`,`start_develop_db`.`payablestats`.`ClaimantTransactionIdRef` AS `ClaimantTransactionIdRef`,`start_develop_db`.`payablestats`.`ServicePeriodStartDt` AS `ServicePeriodStartDt`,`start_develop_db`.`payablestats`.`ServicePeriodEndDt` AS `ServicePeriodEndDt` from `start_develop_db`.`payablestats` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `policyreinsrenewalofferstats`
--

/*!50001 DROP VIEW IF EXISTS `policyreinsrenewalofferstats`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `policyreinsrenewalofferstats` AS select `start_develop_db`.`policyreinsrenewalofferstats`.`SystemId` AS `SystemId`,`start_develop_db`.`policyreinsrenewalofferstats`.`id` AS `id`,`start_develop_db`.`policyreinsrenewalofferstats`.`StatSequence` AS `StatSequence`,`start_develop_db`.`policyreinsrenewalofferstats`.`StatSequenceReplace` AS `StatSequenceReplace`,`start_develop_db`.`policyreinsrenewalofferstats`.`PolicyRef` AS `PolicyRef`,`start_develop_db`.`policyreinsrenewalofferstats`.`PolicyNumber` AS `PolicyNumber`,`start_develop_db`.`policyreinsrenewalofferstats`.`PolicyVersion` AS `PolicyVersion`,`start_develop_db`.`policyreinsrenewalofferstats`.`TransactionNumber` AS `TransactionNumber`,`start_develop_db`.`policyreinsrenewalofferstats`.`EffectiveDt` AS `EffectiveDt`,`start_develop_db`.`policyreinsrenewalofferstats`.`ExpirationDt` AS `ExpirationDt`,`start_develop_db`.`policyreinsrenewalofferstats`.`CombinedKey` AS `CombinedKey`,`start_develop_db`.`policyreinsrenewalofferstats`.`AddDt` AS `AddDt`,`start_develop_db`.`policyreinsrenewalofferstats`.`BookDt` AS `BookDt`,`start_develop_db`.`policyreinsrenewalofferstats`.`TransactionEffectiveDt` AS `TransactionEffectiveDt`,`start_develop_db`.`policyreinsrenewalofferstats`.`AccountingDt` AS `AccountingDt`,`start_develop_db`.`policyreinsrenewalofferstats`.`WrittenPremiumAmt` AS `WrittenPremiumAmt`,`start_develop_db`.`policyreinsrenewalofferstats`.`WrittenCommissionAmt` AS `WrittenCommissionAmt`,`start_develop_db`.`policyreinsrenewalofferstats`.`EarnDays` AS `EarnDays`,`start_develop_db`.`policyreinsrenewalofferstats`.`InforceChangeAmt` AS `InforceChangeAmt`,`start_develop_db`.`policyreinsrenewalofferstats`.`PolicyYear` AS `PolicyYear`,`start_develop_db`.`policyreinsrenewalofferstats`.`TermDays` AS `TermDays`,`start_develop_db`.`policyreinsrenewalofferstats`.`InsuranceTypeCd` AS `InsuranceTypeCd`,`start_develop_db`.`policyreinsrenewalofferstats`.`StartDt` AS `StartDt`,`start_develop_db`.`policyreinsrenewalofferstats`.`EndDt` AS `EndDt`,`start_develop_db`.`policyreinsrenewalofferstats`.`StatusCd` AS `StatusCd`,`start_develop_db`.`policyreinsrenewalofferstats`.`NewRenewalCd` AS `NewRenewalCd`,`start_develop_db`.`policyreinsrenewalofferstats`.`AnnualStatementLineCd` AS `AnnualStatementLineCd`,`start_develop_db`.`policyreinsrenewalofferstats`.`ReinsuranceName` AS `ReinsuranceName`,`start_develop_db`.`policyreinsrenewalofferstats`.`MasterSubInd` AS `MasterSubInd`,`start_develop_db`.`policyreinsrenewalofferstats`.`ReinsuranceItemName` AS `ReinsuranceItemName`,`start_develop_db`.`policyreinsrenewalofferstats`.`ProviderCd` AS `ProviderCd`,`start_develop_db`.`policyreinsrenewalofferstats`.`TransactionCd` AS `TransactionCd`,`start_develop_db`.`policyreinsrenewalofferstats`.`StateCd` AS `StateCd`,`start_develop_db`.`policyreinsrenewalofferstats`.`ReinsuranceGroupId` AS `ReinsuranceGroupId`,`start_develop_db`.`policyreinsrenewalofferstats`.`ProductVersionIdRef` AS `ProductVersionIdRef`,`start_develop_db`.`policyreinsrenewalofferstats`.`CarrierGroupCd` AS `CarrierGroupCd`,`start_develop_db`.`policyreinsrenewalofferstats`.`CarrierCd` AS `CarrierCd`,`start_develop_db`.`policyreinsrenewalofferstats`.`PolicyStatusCd` AS `PolicyStatusCd`,`start_develop_db`.`policyreinsrenewalofferstats`.`ProductName` AS `ProductName`,`start_develop_db`.`policyreinsrenewalofferstats`.`PolicyTypeCd` AS `PolicyTypeCd`,`start_develop_db`.`policyreinsrenewalofferstats`.`PolicyGroupCd` AS `PolicyGroupCd`,`start_develop_db`.`policyreinsrenewalofferstats`.`CustomerRef` AS `CustomerRef`,`start_develop_db`.`policyreinsrenewalofferstats`.`ReinsuranceCoverageGroupCd` AS `ReinsuranceCoverageGroupCd` from `start_develop_db`.`policyreinsrenewalofferstats` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `policyreinsurancestats`
--

/*!50001 DROP VIEW IF EXISTS `policyreinsurancestats`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `policyreinsurancestats` AS select `start_develop_db`.`policyreinsurancestats`.`SystemId` AS `SystemId`,`start_develop_db`.`policyreinsurancestats`.`id` AS `id`,`start_develop_db`.`policyreinsurancestats`.`StatSequence` AS `StatSequence`,`start_develop_db`.`policyreinsurancestats`.`StatSequenceReplace` AS `StatSequenceReplace`,`start_develop_db`.`policyreinsurancestats`.`PolicyRef` AS `PolicyRef`,`start_develop_db`.`policyreinsurancestats`.`PolicyNumber` AS `PolicyNumber`,`start_develop_db`.`policyreinsurancestats`.`PolicyVersion` AS `PolicyVersion`,`start_develop_db`.`policyreinsurancestats`.`TransactionNumber` AS `TransactionNumber`,`start_develop_db`.`policyreinsurancestats`.`EffectiveDt` AS `EffectiveDt`,`start_develop_db`.`policyreinsurancestats`.`ExpirationDt` AS `ExpirationDt`,`start_develop_db`.`policyreinsurancestats`.`CombinedKey` AS `CombinedKey`,`start_develop_db`.`policyreinsurancestats`.`AddDt` AS `AddDt`,`start_develop_db`.`policyreinsurancestats`.`BookDt` AS `BookDt`,`start_develop_db`.`policyreinsurancestats`.`TransactionEffectiveDt` AS `TransactionEffectiveDt`,`start_develop_db`.`policyreinsurancestats`.`AccountingDt` AS `AccountingDt`,`start_develop_db`.`policyreinsurancestats`.`WrittenPremiumAmt` AS `WrittenPremiumAmt`,`start_develop_db`.`policyreinsurancestats`.`WrittenCommissionAmt` AS `WrittenCommissionAmt`,`start_develop_db`.`policyreinsurancestats`.`EarnDays` AS `EarnDays`,`start_develop_db`.`policyreinsurancestats`.`InforceChangeAmt` AS `InforceChangeAmt`,`start_develop_db`.`policyreinsurancestats`.`PolicyYear` AS `PolicyYear`,`start_develop_db`.`policyreinsurancestats`.`TermDays` AS `TermDays`,`start_develop_db`.`policyreinsurancestats`.`InsuranceTypeCd` AS `InsuranceTypeCd`,`start_develop_db`.`policyreinsurancestats`.`StartDt` AS `StartDt`,`start_develop_db`.`policyreinsurancestats`.`EndDt` AS `EndDt`,`start_develop_db`.`policyreinsurancestats`.`StatusCd` AS `StatusCd`,`start_develop_db`.`policyreinsurancestats`.`NewRenewalCd` AS `NewRenewalCd`,`start_develop_db`.`policyreinsurancestats`.`AnnualStatementLineCd` AS `AnnualStatementLineCd`,`start_develop_db`.`policyreinsurancestats`.`ReinsuranceName` AS `ReinsuranceName`,`start_develop_db`.`policyreinsurancestats`.`MasterSubInd` AS `MasterSubInd`,`start_develop_db`.`policyreinsurancestats`.`ReinsuranceItemName` AS `ReinsuranceItemName`,`start_develop_db`.`policyreinsurancestats`.`ProviderCd` AS `ProviderCd`,`start_develop_db`.`policyreinsurancestats`.`TransactionCd` AS `TransactionCd`,`start_develop_db`.`policyreinsurancestats`.`StateCd` AS `StateCd`,`start_develop_db`.`policyreinsurancestats`.`ReinsuranceGroupId` AS `ReinsuranceGroupId`,`start_develop_db`.`policyreinsurancestats`.`ProductVersionIdRef` AS `ProductVersionIdRef`,`start_develop_db`.`policyreinsurancestats`.`CarrierGroupCd` AS `CarrierGroupCd`,`start_develop_db`.`policyreinsurancestats`.`CarrierCd` AS `CarrierCd`,`start_develop_db`.`policyreinsurancestats`.`PolicyStatusCd` AS `PolicyStatusCd`,`start_develop_db`.`policyreinsurancestats`.`ProductName` AS `ProductName`,`start_develop_db`.`policyreinsurancestats`.`PolicyTypeCd` AS `PolicyTypeCd`,`start_develop_db`.`policyreinsurancestats`.`PolicyGroupCd` AS `PolicyGroupCd`,`start_develop_db`.`policyreinsurancestats`.`CustomerRef` AS `CustomerRef`,`start_develop_db`.`policyreinsurancestats`.`ReinsuranceCoverageGroupCd` AS `ReinsuranceCoverageGroupCd` from `start_develop_db`.`policyreinsurancestats` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `policyreinsurancesummarystats`
--

/*!50001 DROP VIEW IF EXISTS `policyreinsurancesummarystats`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `policyreinsurancesummarystats` AS select `start_develop_db`.`policyreinsurancesummarystats`.`SystemId` AS `SystemId`,`start_develop_db`.`policyreinsurancesummarystats`.`id` AS `id`,`start_develop_db`.`policyreinsurancesummarystats`.`StatSequence` AS `StatSequence`,`start_develop_db`.`policyreinsurancesummarystats`.`StatSequenceReplace` AS `StatSequenceReplace`,`start_develop_db`.`policyreinsurancesummarystats`.`ReportPeriod` AS `ReportPeriod`,`start_develop_db`.`policyreinsurancesummarystats`.`UpdateDt` AS `UpdateDt`,`start_develop_db`.`policyreinsurancesummarystats`.`PolicyRef` AS `PolicyRef`,`start_develop_db`.`policyreinsurancesummarystats`.`PolicyNumber` AS `PolicyNumber`,`start_develop_db`.`policyreinsurancesummarystats`.`PolicyVersion` AS `PolicyVersion`,`start_develop_db`.`policyreinsurancesummarystats`.`TransactionNumber` AS `TransactionNumber`,`start_develop_db`.`policyreinsurancesummarystats`.`EffectiveDt` AS `EffectiveDt`,`start_develop_db`.`policyreinsurancesummarystats`.`ExpirationDt` AS `ExpirationDt`,`start_develop_db`.`policyreinsurancesummarystats`.`CombinedKey` AS `CombinedKey`,`start_develop_db`.`policyreinsurancesummarystats`.`AccountingDt` AS `AccountingDt`,`start_develop_db`.`policyreinsurancesummarystats`.`PolicyYear` AS `PolicyYear`,`start_develop_db`.`policyreinsurancesummarystats`.`InsuranceTypeCd` AS `InsuranceTypeCd`,`start_develop_db`.`policyreinsurancesummarystats`.`StatusCd` AS `StatusCd`,`start_develop_db`.`policyreinsurancesummarystats`.`NewRenewalCd` AS `NewRenewalCd`,`start_develop_db`.`policyreinsurancesummarystats`.`AnnualStatementLineCd` AS `AnnualStatementLineCd`,`start_develop_db`.`policyreinsurancesummarystats`.`ReinsuranceName` AS `ReinsuranceName`,`start_develop_db`.`policyreinsurancesummarystats`.`MasterSubInd` AS `MasterSubInd`,`start_develop_db`.`policyreinsurancesummarystats`.`ReinsuranceItemName` AS `ReinsuranceItemName`,`start_develop_db`.`policyreinsurancesummarystats`.`ProviderCd` AS `ProviderCd`,`start_develop_db`.`policyreinsurancesummarystats`.`TransactionCd` AS `TransactionCd`,`start_develop_db`.`policyreinsurancesummarystats`.`StateCd` AS `StateCd`,`start_develop_db`.`policyreinsurancesummarystats`.`ReinsuranceGroupId` AS `ReinsuranceGroupId`,`start_develop_db`.`policyreinsurancesummarystats`.`MTDWrittenPremiumAmt` AS `MTDWrittenPremiumAmt`,`start_develop_db`.`policyreinsurancesummarystats`.`TTDWrittenPremiumAmt` AS `TTDWrittenPremiumAmt`,`start_develop_db`.`policyreinsurancesummarystats`.`YTDWrittenPremiumAmt` AS `YTDWrittenPremiumAmt`,`start_develop_db`.`policyreinsurancesummarystats`.`MTDWrittenCommissionAmt` AS `MTDWrittenCommissionAmt`,`start_develop_db`.`policyreinsurancesummarystats`.`TTDWrittenCommissionAmt` AS `TTDWrittenCommissionAmt`,`start_develop_db`.`policyreinsurancesummarystats`.`YTDWrittenCommissionAmt` AS `YTDWrittenCommissionAmt`,`start_develop_db`.`policyreinsurancesummarystats`.`InforceAmt` AS `InforceAmt`,`start_develop_db`.`policyreinsurancesummarystats`.`LastInforceAmt` AS `LastInforceAmt`,`start_develop_db`.`policyreinsurancesummarystats`.`MonthEarnedPremiumAmt` AS `MonthEarnedPremiumAmt`,`start_develop_db`.`policyreinsurancesummarystats`.`MTDEarnedPremiumAmt` AS `MTDEarnedPremiumAmt`,`start_develop_db`.`policyreinsurancesummarystats`.`TTDEarnedPremiumAmt` AS `TTDEarnedPremiumAmt`,`start_develop_db`.`policyreinsurancesummarystats`.`YTDEarnedPremiumAmt` AS `YTDEarnedPremiumAmt`,`start_develop_db`.`policyreinsurancesummarystats`.`MonthUnearnedAmt` AS `MonthUnearnedAmt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedAmt` AS `UnearnedAmt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM00Amt` AS `UnearnedM00Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM01Amt` AS `UnearnedM01Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM02Amt` AS `UnearnedM02Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM03Amt` AS `UnearnedM03Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM04Amt` AS `UnearnedM04Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM05Amt` AS `UnearnedM05Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM06Amt` AS `UnearnedM06Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM07Amt` AS `UnearnedM07Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM08Amt` AS `UnearnedM08Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM09Amt` AS `UnearnedM09Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM10Amt` AS `UnearnedM10Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM11Amt` AS `UnearnedM11Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM12Amt` AS `UnearnedM12Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM13Amt` AS `UnearnedM13Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM14Amt` AS `UnearnedM14Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM15Amt` AS `UnearnedM15Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM16Amt` AS `UnearnedM16Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM17Amt` AS `UnearnedM17Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM18Amt` AS `UnearnedM18Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM19Amt` AS `UnearnedM19Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM20Amt` AS `UnearnedM20Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM21Amt` AS `UnearnedM21Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM22Amt` AS `UnearnedM22Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM23Amt` AS `UnearnedM23Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM24Amt` AS `UnearnedM24Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM25Amt` AS `UnearnedM25Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM26Amt` AS `UnearnedM26Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM27Amt` AS `UnearnedM27Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM28Amt` AS `UnearnedM28Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM29Amt` AS `UnearnedM29Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM30Amt` AS `UnearnedM30Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM31Amt` AS `UnearnedM31Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM32Amt` AS `UnearnedM32Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM33Amt` AS `UnearnedM33Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM34Amt` AS `UnearnedM34Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM35Amt` AS `UnearnedM35Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM36Amt` AS `UnearnedM36Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM37Amt` AS `UnearnedM37Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM38Amt` AS `UnearnedM38Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM39Amt` AS `UnearnedM39Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM40Amt` AS `UnearnedM40Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM41Amt` AS `UnearnedM41Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM42Amt` AS `UnearnedM42Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM43Amt` AS `UnearnedM43Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM44Amt` AS `UnearnedM44Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM45Amt` AS `UnearnedM45Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM46Amt` AS `UnearnedM46Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM47Amt` AS `UnearnedM47Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`UnearnedM48Amt` AS `UnearnedM48Amt`,`start_develop_db`.`policyreinsurancesummarystats`.`ProductVersionIdRef` AS `ProductVersionIdRef`,`start_develop_db`.`policyreinsurancesummarystats`.`CarrierGroupCd` AS `CarrierGroupCd`,`start_develop_db`.`policyreinsurancesummarystats`.`CarrierCd` AS `CarrierCd`,`start_develop_db`.`policyreinsurancesummarystats`.`PolicyStatusCd` AS `PolicyStatusCd`,`start_develop_db`.`policyreinsurancesummarystats`.`ProductName` AS `ProductName`,`start_develop_db`.`policyreinsurancesummarystats`.`PolicyTypeCd` AS `PolicyTypeCd`,`start_develop_db`.`policyreinsurancesummarystats`.`PolicyGroupCd` AS `PolicyGroupCd`,`start_develop_db`.`policyreinsurancesummarystats`.`CustomerRef` AS `CustomerRef`,`start_develop_db`.`policyreinsurancesummarystats`.`ContractReinsuranceInd` AS `ContractReinsuranceInd`,`start_develop_db`.`policyreinsurancesummarystats`.`ReinsuranceCoverageGroupCd` AS `ReinsuranceCoverageGroupCd` from `start_develop_db`.`policyreinsurancesummarystats` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `policyrenewalofferstats`
--

/*!50001 DROP VIEW IF EXISTS `policyrenewalofferstats`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `policyrenewalofferstats` AS select `start_develop_db`.`policyrenewalofferstats`.`SystemId` AS `SystemId`,`start_develop_db`.`policyrenewalofferstats`.`id` AS `id`,`start_develop_db`.`policyrenewalofferstats`.`StatSequence` AS `StatSequence`,`start_develop_db`.`policyrenewalofferstats`.`StatSequenceReplace` AS `StatSequenceReplace`,`start_develop_db`.`policyrenewalofferstats`.`PolicyRef` AS `PolicyRef`,`start_develop_db`.`policyrenewalofferstats`.`PolicyNumber` AS `PolicyNumber`,`start_develop_db`.`policyrenewalofferstats`.`PolicyVersion` AS `PolicyVersion`,`start_develop_db`.`policyrenewalofferstats`.`LineCd` AS `LineCd`,`start_develop_db`.`policyrenewalofferstats`.`TransactionNumber` AS `TransactionNumber`,`start_develop_db`.`policyrenewalofferstats`.`CoverageCd` AS `CoverageCd`,`start_develop_db`.`policyrenewalofferstats`.`RiskCd` AS `RiskCd`,`start_develop_db`.`policyrenewalofferstats`.`RiskTypeCd` AS `RiskTypeCd`,`start_develop_db`.`policyrenewalofferstats`.`SubTypeCd` AS `SubTypeCd`,`start_develop_db`.`policyrenewalofferstats`.`ProductVersionIdRef` AS `ProductVersionIdRef`,`start_develop_db`.`policyrenewalofferstats`.`CoverageItemCd` AS `CoverageItemCd`,`start_develop_db`.`policyrenewalofferstats`.`FeeCd` AS `FeeCd`,`start_develop_db`.`policyrenewalofferstats`.`EffectiveDt` AS `EffectiveDt`,`start_develop_db`.`policyrenewalofferstats`.`ExpirationDt` AS `ExpirationDt`,`start_develop_db`.`policyrenewalofferstats`.`CombinedKey` AS `CombinedKey`,`start_develop_db`.`policyrenewalofferstats`.`AddDt` AS `AddDt`,`start_develop_db`.`policyrenewalofferstats`.`BookDt` AS `BookDt`,`start_develop_db`.`policyrenewalofferstats`.`TransactionEffectiveDt` AS `TransactionEffectiveDt`,`start_develop_db`.`policyrenewalofferstats`.`AccountingDt` AS `AccountingDt`,`start_develop_db`.`policyrenewalofferstats`.`WrittenPremiumAmt` AS `WrittenPremiumAmt`,`start_develop_db`.`policyrenewalofferstats`.`WrittenCommissionAmt` AS `WrittenCommissionAmt`,`start_develop_db`.`policyrenewalofferstats`.`WrittenPremiumFeeAmt` AS `WrittenPremiumFeeAmt`,`start_develop_db`.`policyrenewalofferstats`.`WrittenCommissionFeeAmt` AS `WrittenCommissionFeeAmt`,`start_develop_db`.`policyrenewalofferstats`.`EarnDays` AS `EarnDays`,`start_develop_db`.`policyrenewalofferstats`.`InforceChangeAmt` AS `InforceChangeAmt`,`start_develop_db`.`policyrenewalofferstats`.`PolicyYear` AS `PolicyYear`,`start_develop_db`.`policyrenewalofferstats`.`TermDays` AS `TermDays`,`start_develop_db`.`policyrenewalofferstats`.`InsuranceTypeCd` AS `InsuranceTypeCd`,`start_develop_db`.`policyrenewalofferstats`.`StartDt` AS `StartDt`,`start_develop_db`.`policyrenewalofferstats`.`EndDt` AS `EndDt`,`start_develop_db`.`policyrenewalofferstats`.`StatusCd` AS `StatusCd`,`start_develop_db`.`policyrenewalofferstats`.`NewRenewalCd` AS `NewRenewalCd`,`start_develop_db`.`policyrenewalofferstats`.`Limit1` AS `Limit1`,`start_develop_db`.`policyrenewalofferstats`.`Limit2` AS `Limit2`,`start_develop_db`.`policyrenewalofferstats`.`Limit3` AS `Limit3`,`start_develop_db`.`policyrenewalofferstats`.`Deductible1` AS `Deductible1`,`start_develop_db`.`policyrenewalofferstats`.`Deductible2` AS `Deductible2`,`start_develop_db`.`policyrenewalofferstats`.`AnnualStatementLineCd` AS `AnnualStatementLineCd`,`start_develop_db`.`policyrenewalofferstats`.`CoinsurancePct` AS `CoinsurancePct`,`start_develop_db`.`policyrenewalofferstats`.`CoveredPerilsCd` AS `CoveredPerilsCd`,`start_develop_db`.`policyrenewalofferstats`.`CommissionKey` AS `CommissionKey`,`start_develop_db`.`policyrenewalofferstats`.`CommissionAreaCd` AS `CommissionAreaCd`,`start_develop_db`.`policyrenewalofferstats`.`RateAreaName` AS `RateAreaName`,`start_develop_db`.`policyrenewalofferstats`.`StatData` AS `StatData`,`start_develop_db`.`policyrenewalofferstats`.`CustomerRef` AS `CustomerRef`,`start_develop_db`.`policyrenewalofferstats`.`ShortRateStatInd` AS `ShortRateStatInd`,`start_develop_db`.`policyrenewalofferstats`.`TransactionCd` AS `TransactionCd`,`start_develop_db`.`policyrenewalofferstats`.`StateCd` AS `StateCd`,`start_develop_db`.`policyrenewalofferstats`.`StatementAccountNumber` AS `StatementAccountNumber`,`start_develop_db`.`policyrenewalofferstats`.`StatementAccountRef` AS `StatementAccountRef`,`start_develop_db`.`policyrenewalofferstats`.`CarrierGroupCd` AS `CarrierGroupCd`,`start_develop_db`.`policyrenewalofferstats`.`CarrierCd` AS `CarrierCd`,`start_develop_db`.`policyrenewalofferstats`.`ProducerCd` AS `ProducerCd`,`start_develop_db`.`policyrenewalofferstats`.`BusinessSourceCd` AS `BusinessSourceCd`,`start_develop_db`.`policyrenewalofferstats`.`PreviousCarrierCd` AS `PreviousCarrierCd`,`start_develop_db`.`policyrenewalofferstats`.`ConstructionCd` AS `ConstructionCd`,`start_develop_db`.`policyrenewalofferstats`.`YearBuilt` AS `YearBuilt`,`start_develop_db`.`policyrenewalofferstats`.`SqFt` AS `SqFt`,`start_develop_db`.`policyrenewalofferstats`.`Stories` AS `Stories`,`start_develop_db`.`policyrenewalofferstats`.`OccupancyCd` AS `OccupancyCd`,`start_develop_db`.`policyrenewalofferstats`.`Units` AS `Units`,`start_develop_db`.`policyrenewalofferstats`.`ProtectionClass` AS `ProtectionClass`,`start_develop_db`.`policyrenewalofferstats`.`TerritoryCd` AS `TerritoryCd`,`start_develop_db`.`policyrenewalofferstats`.`PolicyTypeCd` AS `PolicyTypeCd`,`start_develop_db`.`policyrenewalofferstats`.`PolicyStatusCd` AS `PolicyStatusCd`,`start_develop_db`.`policyrenewalofferstats`.`PolicyGroupCd` AS `PolicyGroupCd`,`start_develop_db`.`policyrenewalofferstats`.`CancelReason` AS `CancelReason`,`start_develop_db`.`policyrenewalofferstats`.`PayPlanCd` AS `PayPlanCd`,`start_develop_db`.`policyrenewalofferstats`.`ClassCd` AS `ClassCd`,`start_develop_db`.`policyrenewalofferstats`.`AgeOfHome` AS `AgeOfHome`,`start_develop_db`.`policyrenewalofferstats`.`LocationAddress1` AS `LocationAddress1`,`start_develop_db`.`policyrenewalofferstats`.`LocationAddress2` AS `LocationAddress2`,`start_develop_db`.`policyrenewalofferstats`.`LocationCity` AS `LocationCity`,`start_develop_db`.`policyrenewalofferstats`.`LocationCounty` AS `LocationCounty`,`start_develop_db`.`policyrenewalofferstats`.`LocationState` AS `LocationState`,`start_develop_db`.`policyrenewalofferstats`.`LocationZip` AS `LocationZip`,`start_develop_db`.`policyrenewalofferstats`.`VehicleNumber` AS `VehicleNumber`,`start_develop_db`.`policyrenewalofferstats`.`VehicleYear` AS `VehicleYear`,`start_develop_db`.`policyrenewalofferstats`.`VehicleManufacturer` AS `VehicleManufacturer`,`start_develop_db`.`policyrenewalofferstats`.`VehicleModel` AS `VehicleModel`,`start_develop_db`.`policyrenewalofferstats`.`VehicleType` AS `VehicleType`,`start_develop_db`.`policyrenewalofferstats`.`DriverNumber` AS `DriverNumber`,`start_develop_db`.`policyrenewalofferstats`.`DriverFirst` AS `DriverFirst`,`start_develop_db`.`policyrenewalofferstats`.`DriverMI` AS `DriverMI`,`start_develop_db`.`policyrenewalofferstats`.`DriverAge` AS `DriverAge`,`start_develop_db`.`policyrenewalofferstats`.`DriverLast` AS `DriverLast`,`start_develop_db`.`policyrenewalofferstats`.`DriverGenderCd` AS `DriverGenderCd`,`start_develop_db`.`policyrenewalofferstats`.`RelationshipToInsuredCd` AS `RelationshipToInsuredCd`,`start_develop_db`.`policyrenewalofferstats`.`DriverStartDt` AS `DriverStartDt`,`start_develop_db`.`policyrenewalofferstats`.`PointsChargeable` AS `PointsChargeable`,`start_develop_db`.`policyrenewalofferstats`.`PointsCharged` AS `PointsCharged`,`start_develop_db`.`policyrenewalofferstats`.`Limit4` AS `Limit4`,`start_develop_db`.`policyrenewalofferstats`.`Limit5` AS `Limit5`,`start_develop_db`.`policyrenewalofferstats`.`Limit6` AS `Limit6`,`start_develop_db`.`policyrenewalofferstats`.`ControllingProductVersionIdRef` AS `ControllingProductVersionIdRef`,`start_develop_db`.`policyrenewalofferstats`.`ControllingStateCd` AS `ControllingStateCd`,`start_develop_db`.`policyrenewalofferstats`.`DividendPlanCd` AS `DividendPlanCd` from `start_develop_db`.`policyrenewalofferstats` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `policystats`
--

/*!50001 DROP VIEW IF EXISTS `policystats`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `policystats` AS select `start_develop_db`.`policystats`.`SystemId` AS `SystemId`,`start_develop_db`.`policystats`.`id` AS `id`,`start_develop_db`.`policystats`.`StatSequence` AS `StatSequence`,`start_develop_db`.`policystats`.`StatSequenceReplace` AS `StatSequenceReplace`,`start_develop_db`.`policystats`.`PolicyRef` AS `PolicyRef`,`start_develop_db`.`policystats`.`ProductVersionIdRef` AS `ProductVersionIdRef`,`start_develop_db`.`policystats`.`PolicyNumber` AS `PolicyNumber`,`start_develop_db`.`policystats`.`PolicyVersion` AS `PolicyVersion`,`start_develop_db`.`policystats`.`LineCd` AS `LineCd`,`start_develop_db`.`policystats`.`TransactionNumber` AS `TransactionNumber`,`start_develop_db`.`policystats`.`CoverageCd` AS `CoverageCd`,`start_develop_db`.`policystats`.`RiskCd` AS `RiskCd`,`start_develop_db`.`policystats`.`RiskTypeCd` AS `RiskTypeCd`,`start_develop_db`.`policystats`.`SubTypeCd` AS `SubTypeCd`,`start_develop_db`.`policystats`.`CoverageItemCd` AS `CoverageItemCd`,`start_develop_db`.`policystats`.`FeeCd` AS `FeeCd`,`start_develop_db`.`policystats`.`EffectiveDt` AS `EffectiveDt`,`start_develop_db`.`policystats`.`ExpirationDt` AS `ExpirationDt`,`start_develop_db`.`policystats`.`CombinedKey` AS `CombinedKey`,`start_develop_db`.`policystats`.`AddDt` AS `AddDt`,`start_develop_db`.`policystats`.`BookDt` AS `BookDt`,`start_develop_db`.`policystats`.`TransactionEffectiveDt` AS `TransactionEffectiveDt`,`start_develop_db`.`policystats`.`AccountingDt` AS `AccountingDt`,`start_develop_db`.`policystats`.`WrittenPremiumAmt` AS `WrittenPremiumAmt`,`start_develop_db`.`policystats`.`WrittenCommissionAmt` AS `WrittenCommissionAmt`,`start_develop_db`.`policystats`.`WrittenPremiumFeeAmt` AS `WrittenPremiumFeeAmt`,`start_develop_db`.`policystats`.`WrittenCommissionFeeAmt` AS `WrittenCommissionFeeAmt`,`start_develop_db`.`policystats`.`EarnDays` AS `EarnDays`,`start_develop_db`.`policystats`.`InforceChangeAmt` AS `InforceChangeAmt`,`start_develop_db`.`policystats`.`PolicyYear` AS `PolicyYear`,`start_develop_db`.`policystats`.`TermDays` AS `TermDays`,`start_develop_db`.`policystats`.`InsuranceTypeCd` AS `InsuranceTypeCd`,`start_develop_db`.`policystats`.`StartDt` AS `StartDt`,`start_develop_db`.`policystats`.`EndDt` AS `EndDt`,`start_develop_db`.`policystats`.`StatusCd` AS `StatusCd`,`start_develop_db`.`policystats`.`NewRenewalCd` AS `NewRenewalCd`,`start_develop_db`.`policystats`.`Limit1` AS `Limit1`,`start_develop_db`.`policystats`.`Limit2` AS `Limit2`,`start_develop_db`.`policystats`.`Limit3` AS `Limit3`,`start_develop_db`.`policystats`.`Deductible1` AS `Deductible1`,`start_develop_db`.`policystats`.`Deductible2` AS `Deductible2`,`start_develop_db`.`policystats`.`AnnualStatementLineCd` AS `AnnualStatementLineCd`,`start_develop_db`.`policystats`.`CoinsurancePct` AS `CoinsurancePct`,`start_develop_db`.`policystats`.`CoveredPerilsCd` AS `CoveredPerilsCd`,`start_develop_db`.`policystats`.`CommissionKey` AS `CommissionKey`,`start_develop_db`.`policystats`.`CommissionAreaCd` AS `CommissionAreaCd`,`start_develop_db`.`policystats`.`RateAreaName` AS `RateAreaName`,`start_develop_db`.`policystats`.`StatData` AS `StatData`,`start_develop_db`.`policystats`.`CustomerRef` AS `CustomerRef`,`start_develop_db`.`policystats`.`ShortRateStatInd` AS `ShortRateStatInd`,`start_develop_db`.`policystats`.`TransactionCd` AS `TransactionCd`,`start_develop_db`.`policystats`.`StateCd` AS `StateCd`,`start_develop_db`.`policystats`.`StatementAccountNumber` AS `StatementAccountNumber`,`start_develop_db`.`policystats`.`StatementAccountRef` AS `StatementAccountRef`,`start_develop_db`.`policystats`.`CarrierGroupCd` AS `CarrierGroupCd`,`start_develop_db`.`policystats`.`CarrierCd` AS `CarrierCd`,`start_develop_db`.`policystats`.`ProducerCd` AS `ProducerCd`,`start_develop_db`.`policystats`.`BusinessSourceCd` AS `BusinessSourceCd`,`start_develop_db`.`policystats`.`PreviousCarrierCd` AS `PreviousCarrierCd`,`start_develop_db`.`policystats`.`ConstructionCd` AS `ConstructionCd`,`start_develop_db`.`policystats`.`YearBuilt` AS `YearBuilt`,`start_develop_db`.`policystats`.`SqFt` AS `SqFt`,`start_develop_db`.`policystats`.`Stories` AS `Stories`,`start_develop_db`.`policystats`.`OccupancyCd` AS `OccupancyCd`,`start_develop_db`.`policystats`.`Units` AS `Units`,`start_develop_db`.`policystats`.`ProtectionClass` AS `ProtectionClass`,`start_develop_db`.`policystats`.`TerritoryCd` AS `TerritoryCd`,`start_develop_db`.`policystats`.`PolicyTypeCd` AS `PolicyTypeCd`,`start_develop_db`.`policystats`.`PolicyStatusCd` AS `PolicyStatusCd`,`start_develop_db`.`policystats`.`PolicyGroupCd` AS `PolicyGroupCd`,`start_develop_db`.`policystats`.`CancelReason` AS `CancelReason`,`start_develop_db`.`policystats`.`PayPlanCd` AS `PayPlanCd`,`start_develop_db`.`policystats`.`ClassCd` AS `ClassCd`,`start_develop_db`.`policystats`.`AgeOfHome` AS `AgeOfHome`,`start_develop_db`.`policystats`.`LocationAddress1` AS `LocationAddress1`,`start_develop_db`.`policystats`.`LocationAddress2` AS `LocationAddress2`,`start_develop_db`.`policystats`.`LocationCity` AS `LocationCity`,`start_develop_db`.`policystats`.`LocationCounty` AS `LocationCounty`,`start_develop_db`.`policystats`.`LocationState` AS `LocationState`,`start_develop_db`.`policystats`.`LocationZip` AS `LocationZip`,`start_develop_db`.`policystats`.`VehicleNumber` AS `VehicleNumber`,`start_develop_db`.`policystats`.`VehicleYear` AS `VehicleYear`,`start_develop_db`.`policystats`.`VehicleManufacturer` AS `VehicleManufacturer`,`start_develop_db`.`policystats`.`VehicleModel` AS `VehicleModel`,`start_develop_db`.`policystats`.`VehicleType` AS `VehicleType`,`start_develop_db`.`policystats`.`DriverNumber` AS `DriverNumber`,`start_develop_db`.`policystats`.`DriverFirst` AS `DriverFirst`,`start_develop_db`.`policystats`.`DriverMI` AS `DriverMI`,`start_develop_db`.`policystats`.`DriverAge` AS `DriverAge`,`start_develop_db`.`policystats`.`DriverLast` AS `DriverLast`,`start_develop_db`.`policystats`.`DriverGenderCd` AS `DriverGenderCd`,`start_develop_db`.`policystats`.`RelationshipToInsuredCd` AS `RelationshipToInsuredCd`,`start_develop_db`.`policystats`.`DriverStartDt` AS `DriverStartDt`,`start_develop_db`.`policystats`.`PointsChargeable` AS `PointsChargeable`,`start_develop_db`.`policystats`.`PointsCharged` AS `PointsCharged`,`start_develop_db`.`policystats`.`Limit4` AS `Limit4`,`start_develop_db`.`policystats`.`Limit5` AS `Limit5`,`start_develop_db`.`policystats`.`Limit6` AS `Limit6`,`start_develop_db`.`policystats`.`ControllingStateCd` AS `ControllingStateCd`,`start_develop_db`.`policystats`.`ControllingProductVersionIdRef` AS `ControllingProductVersionIdRef`,`start_develop_db`.`policystats`.`DividendPlanCd` AS `DividendPlanCd`,`start_develop_db`.`policystats`.`ConversionTemplateIdRef` AS `ConversionTemplateIdRef`,`start_develop_db`.`policystats`.`ConversionGroup` AS `ConversionGroup`,`start_develop_db`.`policystats`.`ConversionJobRef` AS `ConversionJobRef`,`start_develop_db`.`policystats`.`ConversionFileName` AS `ConversionFileName` from `start_develop_db`.`policystats` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `policysummarystats`
--

/*!50001 DROP VIEW IF EXISTS `policysummarystats`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `policysummarystats` AS select `start_develop_db`.`policysummarystats`.`SystemId` AS `SystemId`,`start_develop_db`.`policysummarystats`.`id` AS `id`,`start_develop_db`.`policysummarystats`.`StatSequence` AS `StatSequence`,`start_develop_db`.`policysummarystats`.`StatSequenceReplace` AS `StatSequenceReplace`,`start_develop_db`.`policysummarystats`.`ReportPeriod` AS `ReportPeriod`,`start_develop_db`.`policysummarystats`.`UpdateDt` AS `UpdateDt`,`start_develop_db`.`policysummarystats`.`PolicyRef` AS `PolicyRef`,`start_develop_db`.`policysummarystats`.`ProductVersionIdRef` AS `ProductVersionIdRef`,`start_develop_db`.`policysummarystats`.`PolicyNumber` AS `PolicyNumber`,`start_develop_db`.`policysummarystats`.`PolicyVersion` AS `PolicyVersion`,`start_develop_db`.`policysummarystats`.`TransactionNumber` AS `TransactionNumber`,`start_develop_db`.`policysummarystats`.`LineCd` AS `LineCd`,`start_develop_db`.`policysummarystats`.`RiskCd` AS `RiskCd`,`start_develop_db`.`policysummarystats`.`RiskTypeCd` AS `RiskTypeCd`,`start_develop_db`.`policysummarystats`.`SubTypeCd` AS `SubTypeCd`,`start_develop_db`.`policysummarystats`.`CoverageCd` AS `CoverageCd`,`start_develop_db`.`policysummarystats`.`CoverageItemCd` AS `CoverageItemCd`,`start_develop_db`.`policysummarystats`.`FeeCd` AS `FeeCd`,`start_develop_db`.`policysummarystats`.`EffectiveDt` AS `EffectiveDt`,`start_develop_db`.`policysummarystats`.`ExpirationDt` AS `ExpirationDt`,`start_develop_db`.`policysummarystats`.`CombinedKey` AS `CombinedKey`,`start_develop_db`.`policysummarystats`.`AccountingDt` AS `AccountingDt`,`start_develop_db`.`policysummarystats`.`PolicyYear` AS `PolicyYear`,`start_develop_db`.`policysummarystats`.`InsuranceTypeCd` AS `InsuranceTypeCd`,`start_develop_db`.`policysummarystats`.`StatusCd` AS `StatusCd`,`start_develop_db`.`policysummarystats`.`NewRenewalCd` AS `NewRenewalCd`,`start_develop_db`.`policysummarystats`.`Limit1` AS `Limit1`,`start_develop_db`.`policysummarystats`.`Limit2` AS `Limit2`,`start_develop_db`.`policysummarystats`.`Limit3` AS `Limit3`,`start_develop_db`.`policysummarystats`.`Limit4` AS `Limit4`,`start_develop_db`.`policysummarystats`.`Limit5` AS `Limit5`,`start_develop_db`.`policysummarystats`.`Limit6` AS `Limit6`,`start_develop_db`.`policysummarystats`.`Limit7` AS `Limit7`,`start_develop_db`.`policysummarystats`.`Limit8` AS `Limit8`,`start_develop_db`.`policysummarystats`.`Limit9` AS `Limit9`,`start_develop_db`.`policysummarystats`.`Deductible1` AS `Deductible1`,`start_develop_db`.`policysummarystats`.`Deductible2` AS `Deductible2`,`start_develop_db`.`policysummarystats`.`AnnualStatementLineCd` AS `AnnualStatementLineCd`,`start_develop_db`.`policysummarystats`.`CoinsurancePct` AS `CoinsurancePct`,`start_develop_db`.`policysummarystats`.`CoveredPerilsCd` AS `CoveredPerilsCd`,`start_develop_db`.`policysummarystats`.`CommissionAreaCd` AS `CommissionAreaCd`,`start_develop_db`.`policysummarystats`.`MTDWrittenPremiumAmt` AS `MTDWrittenPremiumAmt`,`start_develop_db`.`policysummarystats`.`TTDWrittenPremiumAmt` AS `TTDWrittenPremiumAmt`,`start_develop_db`.`policysummarystats`.`YTDWrittenPremiumAmt` AS `YTDWrittenPremiumAmt`,`start_develop_db`.`policysummarystats`.`MTDWrittenCommissionAmt` AS `MTDWrittenCommissionAmt`,`start_develop_db`.`policysummarystats`.`YTDWrittenCommissionAmt` AS `YTDWrittenCommissionAmt`,`start_develop_db`.`policysummarystats`.`TTDWrittenCommissionAmt` AS `TTDWrittenCommissionAmt`,`start_develop_db`.`policysummarystats`.`MTDWrittenPremiumFeeAmt` AS `MTDWrittenPremiumFeeAmt`,`start_develop_db`.`policysummarystats`.`YTDWrittenPremiumFeeAmt` AS `YTDWrittenPremiumFeeAmt`,`start_develop_db`.`policysummarystats`.`TTDWrittenPremiumFeeAmt` AS `TTDWrittenPremiumFeeAmt`,`start_develop_db`.`policysummarystats`.`MTDWrittenCommissionFeeAmt` AS `MTDWrittenCommissionFeeAmt`,`start_develop_db`.`policysummarystats`.`YTDWrittenCommissionFeeAmt` AS `YTDWrittenCommissionFeeAmt`,`start_develop_db`.`policysummarystats`.`TTDWrittenCommissionFeeAmt` AS `TTDWrittenCommissionFeeAmt`,`start_develop_db`.`policysummarystats`.`InforceAmt` AS `InforceAmt`,`start_develop_db`.`policysummarystats`.`LastInforceAmt` AS `LastInforceAmt`,`start_develop_db`.`policysummarystats`.`MonthEarnedPremiumAmt` AS `MonthEarnedPremiumAmt`,`start_develop_db`.`policysummarystats`.`MTDEarnedPremiumAmt` AS `MTDEarnedPremiumAmt`,`start_develop_db`.`policysummarystats`.`YTDEarnedPremiumAmt` AS `YTDEarnedPremiumAmt`,`start_develop_db`.`policysummarystats`.`TTDEarnedPremiumAmt` AS `TTDEarnedPremiumAmt`,`start_develop_db`.`policysummarystats`.`PolicyInforceCount` AS `PolicyInforceCount`,`start_develop_db`.`policysummarystats`.`LocationInforceCount` AS `LocationInforceCount`,`start_develop_db`.`policysummarystats`.`RiskInforceCount` AS `RiskInforceCount`,`start_develop_db`.`policysummarystats`.`MonthUnearnedAmt` AS `MonthUnearnedAmt`,`start_develop_db`.`policysummarystats`.`UnearnedAmt` AS `UnearnedAmt`,`start_develop_db`.`policysummarystats`.`UnearnedM00Amt` AS `UnearnedM00Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM01Amt` AS `UnearnedM01Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM02Amt` AS `UnearnedM02Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM03Amt` AS `UnearnedM03Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM04Amt` AS `UnearnedM04Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM05Amt` AS `UnearnedM05Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM06Amt` AS `UnearnedM06Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM07Amt` AS `UnearnedM07Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM08Amt` AS `UnearnedM08Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM09Amt` AS `UnearnedM09Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM10Amt` AS `UnearnedM10Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM11Amt` AS `UnearnedM11Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM12Amt` AS `UnearnedM12Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM13Amt` AS `UnearnedM13Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM14Amt` AS `UnearnedM14Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM15Amt` AS `UnearnedM15Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM16Amt` AS `UnearnedM16Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM17Amt` AS `UnearnedM17Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM18Amt` AS `UnearnedM18Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM19Amt` AS `UnearnedM19Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM20Amt` AS `UnearnedM20Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM21Amt` AS `UnearnedM21Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM22Amt` AS `UnearnedM22Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM23Amt` AS `UnearnedM23Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM24Amt` AS `UnearnedM24Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM25Amt` AS `UnearnedM25Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM26Amt` AS `UnearnedM26Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM27Amt` AS `UnearnedM27Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM28Amt` AS `UnearnedM28Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM29Amt` AS `UnearnedM29Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM30Amt` AS `UnearnedM30Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM31Amt` AS `UnearnedM31Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM32Amt` AS `UnearnedM32Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM33Amt` AS `UnearnedM33Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM34Amt` AS `UnearnedM34Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM35Amt` AS `UnearnedM35Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM36Amt` AS `UnearnedM36Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM37Amt` AS `UnearnedM37Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM38Amt` AS `UnearnedM38Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM39Amt` AS `UnearnedM39Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM40Amt` AS `UnearnedM40Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM41Amt` AS `UnearnedM41Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM42Amt` AS `UnearnedM42Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM43Amt` AS `UnearnedM43Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM44Amt` AS `UnearnedM44Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM45Amt` AS `UnearnedM45Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM46Amt` AS `UnearnedM46Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM47Amt` AS `UnearnedM47Amt`,`start_develop_db`.`policysummarystats`.`UnearnedM48Amt` AS `UnearnedM48Amt`,`start_develop_db`.`policysummarystats`.`RateAreaName` AS `RateAreaName`,`start_develop_db`.`policysummarystats`.`StatData` AS `StatData`,`start_develop_db`.`policysummarystats`.`CustomerRef` AS `CustomerRef`,`start_develop_db`.`policysummarystats`.`StateCd` AS `StateCd`,`start_develop_db`.`policysummarystats`.`CarrierGroupCd` AS `CarrierGroupCd`,`start_develop_db`.`policysummarystats`.`CarrierCd` AS `CarrierCd`,`start_develop_db`.`policysummarystats`.`ProducerCd` AS `ProducerCd`,`start_develop_db`.`policysummarystats`.`BusinessSourceCd` AS `BusinessSourceCd`,`start_develop_db`.`policysummarystats`.`PreviousCarrierCd` AS `PreviousCarrierCd`,`start_develop_db`.`policysummarystats`.`ConstructionCd` AS `ConstructionCd`,`start_develop_db`.`policysummarystats`.`YearBuilt` AS `YearBuilt`,`start_develop_db`.`policysummarystats`.`SqFt` AS `SqFt`,`start_develop_db`.`policysummarystats`.`Stories` AS `Stories`,`start_develop_db`.`policysummarystats`.`OccupancyCd` AS `OccupancyCd`,`start_develop_db`.`policysummarystats`.`Units` AS `Units`,`start_develop_db`.`policysummarystats`.`ProtectionClass` AS `ProtectionClass`,`start_develop_db`.`policysummarystats`.`TerritoryCd` AS `TerritoryCd`,`start_develop_db`.`policysummarystats`.`TransactionCd` AS `TransactionCd`,`start_develop_db`.`policysummarystats`.`ControllingStateCd` AS `ControllingStateCd`,`start_develop_db`.`policysummarystats`.`ControllingProductVersionIdRef` AS `ControllingProductVersionIdRef`,`start_develop_db`.`policysummarystats`.`DividendPlanCd` AS `DividendPlanCd`,`start_develop_db`.`policysummarystats`.`ConversionTemplateIdRef` AS `ConversionTemplateIdRef`,`start_develop_db`.`policysummarystats`.`ConversionGroup` AS `ConversionGroup`,`start_develop_db`.`policysummarystats`.`ConversionJobRef` AS `ConversionJobRef`,`start_develop_db`.`policysummarystats`.`ConversionFileName` AS `ConversionFileName` from `start_develop_db`.`policysummarystats` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-16 13:00:56
