::
:: setenv - Set Environment Variables for Guidewire InsuranceNow script
::
:: Note that if these parameters are changes, be sure to run the "spi install" script to update the windows service
::
:: Parameters:
::     none
::
IF "%SPI_TRACE%"=="TRUE" (
	ECHO -- 
	ECHO -- setenv.cmd - begin
)

IF NOT DEFINED SPI_HOME (
	ECHO --
	ECHO -- Environment variable SPI_HOME is not set.
	EXIT /b 1
)

IF NOT DEFINED SPI_BASE (
	ECHO --
	ECHO -- Environment variable SPI_BASE is not set.
	EXIT /b 1
)

:: SPI server management script directory
SET SPI_SCRIPTS=%SPI_HOME%\a\scripts_v3.4.0

IF "%SPI_DEBUG%"=="TRUE" (
	ECHO --
	ECHO -- SPI_HOME    [%SPI_HOME%]
	ECHO -- SPI_BASE    [%SPI_BASE%]
	ECHO -- SPI_SCRIPTS [%SPI_SCRIPTS%]
)

:: Load properties.config into the environment
ECHO --
ECHO -- Setting Variables from Configuration File
IF EXIST %SPI_HOME%\scripts\config.properties (
	FOR /f %%a IN (%SPI_HOME%\scripts\config.properties) DO (
		SET TKN=%%a
		REM strip all comments # and remove " and ' wrappers.
		IF NOT "!TKN:~0,1!" == "#" (
			SET !TKN:'=!
		)
	)
)

:: Set UMASK to override default settings of Tomcat8.5
export UMASK=0000

:: Location of Ant install
SET ANT_HOME=%SPI_HOME%\a\apache-ant-1.9.3

:: Location of the Java install
SET JAVA_HOME=%SPI_HOME%\a\jdk1.8.0_60

:: Location of the Tomcat install (aka Catalina)
SET CATALINA_HOME=%SPI_HOME%\a\apache-tomcat-8.5.23

:: Run under any user
SET CHECK_USER=0

:: Location of environment specific Tomcat setup
SET CATALINA_BASE=%SPI_BASE%\tomcat

:: Settings to allow for JMX monitoring of Tomcat. NOTE: does not work behind firewall
SET JMX_PORT=9099

:: Settings to allow for source level debugging the runtime Tomcat environment
SET JPDA_ADDRESS=9009
SET JPDA_TRANSPORT=dt_socket

:: Additional Java Options, typically memory settings.
SET SPI_JAVA_OPTS="-Xmx1024m;-XX:MaxPermSize=256m;-XX:+UseCompressedOops;-XX:+UseConcMarkSweepGC;-XX:+PrintCommandLineFlags"
 
:: SPI Administration Script Overrides:

:: Override valid script user permitted to run SPI Admin Scripts
:: Default user is "innovation" - Must be a valid system user.
SET VALID_SCRIPT_USER=

:: Override default SPI_BASE/deployments war file path used during deployments 
:: Path relative to $SPI_BASE (i.e WAR_PATH=data) 
SET WAR_PATH=

:: Override login page check
:: 0 will disable login page check
SET CHECK_LOGIN_PAGE=

:: Override User Authentication 
:: 0 will do Authentication
SET CHECK_USER=0

:: Override default login page query string.
:: String to search for in the login page body. i.e."Innovation Login Page"
SET LOGIN_PAGE_QUERY_STRING=

:: Override default query response timeout. (typically increase)
:: Amount of time to query before test aborts, 15 seconds by default.
SET LOGIN_PAGE_QUERY_RESPONSE_ATTEMPTS=15

:: Override default login page URL.
:: URL of the server login page as seen from the server.
:: Do not include SERVERNAME:PROXYPORT
:: i.e. only /innovation?rq=COUserStartPage
::

IF "%SPI_TRACE%"=="TRUE" (
	ECHO -- 
	ECHO -- setenv.cmd - complete
)

