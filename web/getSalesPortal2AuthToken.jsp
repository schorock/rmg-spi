<%@ page import="net.inov.tec.security.SecurityManager" %>
<%@ page import="net.inov.tec.beans.ModelBean" %>

<%@ page session="false" %>

<%
  // Make sure the client doesn't cache the response to this
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
  response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
  response.setDateHeader("Expires", 0); // Proxies.

  // Add CORS response headers to the response.
  // See {@link http://www.html5rocks.com/en/tutorials/cors/} for more information on CORS.
  // Just allow any requesting origin for now
  String origin = request.getHeader("Origin");
  if (origin != null) {
      response.setHeader("Access-Control-Allow-Origin", origin);
  }

  String userId = "directportal";
  ModelBean loginToken = SecurityManager.getSecurityManager().createLoginToken(userId, null);
  String loginTokenStr = loginToken.gets("LoginToken");
%>
<%=loginTokenStr%>

