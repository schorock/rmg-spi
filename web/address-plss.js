// This is a plug-in to address.js, which must also be included
function TRSInfo(){
	
	var self = this;
	
	self.latitude;
	self.longitude;
	self.description;
	self.verificationMsg;

}

function createTownshipControlGroup(rootName, separator){
	
	if(!separator){
		separator = ".";
	}
	
	var grp = new TownshipGroup();
	grp.initialize(rootName, separator);
	
	return grp;
	
}

function TownshipGroup(){
	
	var self = this;
	
	var __townshipFields = ["State", "Meridian", "Township", "TownshipDir", 
	              "Range", "RangeDir", "Section", 
	              "Latitude", "Longitude", "Description", 
	              "VerificationHash", "VerificationMsg", 
	              "VerifyImg", "StatusImg" ];
	
	self.controlGroupRootName;
	self.controlGroupSeparator;
	self.addressVerificationFailedImgSrc = "../img/escalated-exclamation.gif";
	self.addressVerifiedImgSrc = "../img/green-check.gif";
	self.processingImgSrc = "../img/indicator.gif";
	self.addressVerifyImgSrc = "../img/address-verify.gif";
	
	this.initialize = function(controlGroupRootName, controlGroupSeparator){
		self.controlGroupRootName = controlGroupRootName;
		self.controlGroupSeparator = controlGroupSeparator;
	}

	this.getControl = function(name){
		return document.getElementById(self.controlGroupRootName + self.controlGroupSeparator + name);
	}
	
	this.getControlValue = function(name){
		return self.getControl(name) ? self.getControl(name).value : null;
	}
	
	this.setControlValue = function(name, value){
		if(self.getControl(name)){
			self.getControl(name).value = value;
		}
		
	}
	
	this.getStateProvCd = function(){
		return self.getControlValue("StateProvCd");
	}
	
	this.setStateProvCd = function(value){
		self.setControlValue("StateProvCd", value);
	}
	
	this.getMeridian = function(){
		return self.getControlValue("Meridian");
	}
	
	this.setMeridian = function(value){
		self.setControlValue("Meridian", value);
	}

	this.getTownship = function(){
		return self.getControlValue("Township");
	}
	
	this.setTownship = function(value){
		self.setControlValue("Township", value);
	}

	this.getTownshipDir = function(){
		return self.getControlValue("TownshipDir");
	}
	
	this.setTownshipDir = function(value){
		self.setControlValue("TownshipDir", value);
	}
	
	this.getRange = function(){
		return self.getControlValue("Range");
	}
	
	this.setRange = function(value){
		self.setControlValue("Range", value);
	}

	this.getRangeDir = function(){
		return self.getControlValue("RangeDir");
	}
	
	this.setRangeDir = function(value){
		self.setControlValue("RangeDir", value);
	}

	this.getSection = function(){
		return self.getControlValue("Section");
	}
	
	this.setSection = function(value){
		self.setControlValue("Section", value);
	}
	
	this.getLatitude = function(){
		return self.getControlValue("Latitude");
	}
	
	this.setLatitude = function(value){
		self.setControlValue("Latitude", value);
	}
	
	this.getLongitude = function(){
		return self.getControlValue("Longitude");
	}
	
	this.setLongitude = function(value){
		self.setControlValue("Longitude", value);
	}
	
	this.getDescription = function(){
		return self.getControlValue("Description");
	}
	
	this.setDescription = function(value){
		self.setControlValue("Description", value);
	}
	
	this.getStoredVerificationHash = function(){
		return self.getControlValue("VerificationHash");
	}
	
	this.setStoredVerificationHash = function(value){
		self.setControlValue("VerificationHash", value);
	}
	
	this.getVerificationMsg = function(){
		return self.getControlValue("VerificationMsg");
	}
	
	this.setVerificationMsg = function(value){
		self.setControlValue("VerificationMsg", value);
	}
		
	this.verify = function(){
		
		self.setLatitude("");
		self.setLongitude("");
		self.setDescription("");
		self.setVerificationMsg("");
		
		if(self.getControl("StatusImg")){
			self.getControl("StatusImg").style.visibility = "hidden";
		}
		
		var securityId = document.getElementById("SecurityId") ? document.getElementById("SecurityId").value : null;
		
		self.getControl("VerifyImg").src = self.processingImgSrc;
		var xform = "<?xml version='1.0' ?><xfdf><fields>";    	
	    xform = xform += "<SafeCopy>true</SafeCopy>";
	    xform = xform += "<rq>COAddressVerificationPLSS</rq>";
	    xform = xform += "<SecurityId>" + securityId + "</SecurityId>";
	    xform = xform += "<ResponseAsXml>Yes</ResponseAsXml>";
	    xform = xform += "<StateProvCd>" + self.getStateProvCd() + "</StateProvCd>";
	    xform = xform += "<Meridian>" + self.getMeridian() + "</Meridian>";
	    xform = xform += "<Township>" + self.getTownship() + "</Township>";
	    xform = xform += "<TownshipDir>" + self.getTownshipDir() + "</TownshipDir>";
	    xform = xform += "<Range>" + self.getRange() + "</Range>";
	    xform = xform += "<RangeDir>" + self.getRangeDir() + "</RangeDir>";
	    xform = xform += "<Section>" + self.getSection() + "</Section>";
	   	xform = xform += "</fields></xfdf>";
	
		// alert(xform);
		var params = new Object();
		params.XForm = xform;

		analyticsTrackPageview("address-plss-verify");
		
		window.seleniumPageLoadOutstanding++;

		$.post("innovation", $.param(params), function(data) {
			handleReturnedPLSS(data, self);
			window.seleniumPageLoadOutstanding--;
		}, "xml");	
		
		avlog(xform);
	
	}
	
	this.updateStatusImg = function(){
		
		if(self.getControl("StatusImg") && self.getControl("VerificationHash")){
		
			if(self.getControlValue("VerificationMsg") && self.getControlValue("VerificationMsg").length > 0){
				alert(self.getControl("VerificationMsg").value);
				self.getControl("StatusImg").src = self.addressVerificationFailedImgSrc;
				self.getControl("StatusImg").style.cursor = "help";
				self.getControl("StatusImg").style.visibility = "visible";
				self.getControl("StatusImg").onclick = "";
				self.getControl("StatusImg").title = self.getControlValue("VerificationMsg");
			}
			else if(self.getVerificationHash() == self.getControlValue("VerificationHash") && self.getVerificationHash().length > 0){
				self.getControl("StatusImg").src = self.addressVerifiedImgSrc;	
				self.getControl("StatusImg").style.visibility = "visible";
				self.getControl("StatusImg").style.cursor = "";
				self.getControl("StatusImg").title = "Valid";

			}
			else{
				self.getControl("StatusImg").style.visibility = "hidden";
				self.getControl("StatusImg").onclick = "";
			}
		}				
	}
	
	this.setTRSSInfo = function(trsInfo){
	
		self.setControlValue("Latitude", trsInfo.latitude);
		self.setControlValue("Longitude", trsInfo.longitude);
		self.setControlValue("Description", trsInfo.description);
		
		self.setControlValue("VerificationHash", self.getVerificationHash() );
		if(trsInfo.verificationMsg){
			self.setControlValue("VerificationMsg", trsInfo.verificationMsg);		
		}

		self.updateStatusImg();
		self.onSuccess(self);
		
		
	}
	
	
	this.onSuccess = function(obj) {
		// Point this function to your own function if you want your code to be called after
		// verification
	}
	
	this.onFailure = function(obj) {
		// Point this function to your own function if you want your code to be called after
		// verification
	}
	
	this.getVerificationHash = function(){
	
		var hash;
		hash += self.getControlValue("State") ? (self.getControlValue("State") + " ") : "";
		hash += self.getControlValue("Meridian") ? (self.getControlValue("Meridian") + " ") : "";
		hash += self.getControlValue("Township") ? (self.getControlValue("Township") + " ") : "";
		hash += self.getControlValue("Range") ? (self.getControlValue("Range") + " ") : "";
		hash += self.getControlValue("RangeDir") ? (self.getControlValue("RangeDir") + " ") : "";
		hash += self.getControlValue("Section") ? (self.getControlValue("Section") + " ") : "";
    	
		return hash.toUpperCase();
		
	}

	
}

function handleReturnedPLSS( data, obj ) {
	
	// Set the image back to normal
	obj.getControl("VerifyImg").src = obj.addressVerifyImgSrc;
	
	// First, make sure we got a COAddressVerificationRs.  If not, something bad happened
	var errorMsg = "";
	var rs = data.getElementsByTagName("COAddressVerificationPLSSRs");
	if(!rs || rs.length == 0){
		var errorMsg = "Unexpected error getting PLSS informatino. \nYour session may have expired.  Please relogin and try again.";
		obj.updateStatusImg();
		alert(errorMsg);
		obj.onFailure(obj);
		return;
	}
	
	avlog(XMLtoString(data));
	
	// Check for a verification error message	
	var errors = data.getElementsByTagName("Error");
	if(errors.length > 0){
		var errorMsg = "Could not perform PLSS:\n";
		for(i=0; i<errors.length; i++){
			errorMsg = errorMsg + errors[i].getAttribute("Msg");
			if(i< errors.length - 1){
				errorMsg = errorMsg + "\n";
			}
		}
		obj.setControlValue("VerificationMsg", errorMsg.replace(/\n/g, "<BR>"));
		obj.updateStatusImg();
		alert(errorMsg);
		obj.onFailure(obj);
		return;
	}
	
	// Update the fields
	var trsInfoBean = data.getElementsByTagName("TRSInfo")[0];
	
	var trsInfo = new TRSInfo();
	trsInfo.latitude = trsInfoBean.getAttribute("Latitude");
	trsInfo.longitude = trsInfoBean.getAttribute("Longitude");
	trsInfo.description = trsInfoBean.getAttribute("Description");
	trsInfo.verificationMsg = trsInfoBean.getAttribute("VerificationMsg");
	
	// Update the object
	obj.setTRSSInfo(trsInfo);
	
}

