var _verificationMsgElementID;
var _verificationMsgIframeElementID;
var _xOffset = 0;
var _yOffset = 20;
var _currentAddressCtlGrp;
var _currentAddresses;
var _addrFields = [ 'Addr1', 'Addr2', 'Addr3', 'Addr4', 'City', 'StateProvCd', 
                    'PostalCode', 'RegionCd', 'RegionISOCd', 'County', 'Latitude', 
                    'Longitude', 'VerificationHash', 'VerificationMsg',
                    'PrimaryNumber', 'StreetName', 'PreDirectional', 'Suffix', 'PostDirectional', 
                    'SecondaryDesignator', 'SecondaryNumber', 'Latitude', 'Longitude',
                    'Score', 'DPV', 'DPVDesc', 'DPVNotes', 'DPVNotesDesc',
                    'Attention', 'BarcodeDigits', 'CarrierRoute', 'CongressCode', 'CountyCode' ];
                   
function CombinedAddress(){

	var self = this;  
	
	self.Address1;
	self.Address2;
	self.Address3;
	self.Address4;
	self.StreetNum;
	self.StreetDir;
	self.StreetName;
	self.StreetType;
	self.StreetPostDir;
	self.UnitType;
	self.Unit;
	self.City;
	self.County;
	self.StateProvCd;
	self.PostalCode;
	self.RegionCd;
	self.RegionISOCd;
	self.Latitude;
	self.Longitude;
	self.VerificationHash;
	self.VerificationMsg;
	self.Score;
	self.DPV;
	self.DPVDesc;
	self.DPVNotes;
	self.DPVNotesDesc;
	self.AddressType;
	self.BarcodeDigits;
	self.CarrierRoute;
	self.CongressCode;
	self.CountyCode;	

	this.toString = function(){
	
		var s = "";
		
		s += self.Address1 ? (self.Address1 + "<BR/>") : "";
		s += self.Address2 ? (self.Address2 + "<BR/>") : "";
		s += self.Address3 ? (self.Address3 + "<BR/>") : "";
		s += self.Address4 ? (self.Address4 + "<BR/>") : "";
		s += self.City ? (self.City + " ") : "";
		s += self.StateProvCd ? (self.StateProvCd + " ") : "";
		s += self.PostalCode ? (self.PostalCode + " ") : "";
		
		return s;
	
	}
	
	this.getMapLink = function(){
	
		var link = googleMapsLink(
			self.Address1, 
			self.Address2,
			self.City,
			self.StateProvCd
		);
		
		return link;
		
	}
	

}

function AddressControlGroup(){
	
	var self = this;  // workaround for scope errors: see
						// http://www.crockford.com/javascript/private.html
	
	self.Address1ControlId;
	self.Address2ControlId;
	self.Address3ControlId;
	self.Address4ControlId;
	self.StreetNumControlId;
	self.StreetDirControlId;
	self.StreetNameControlId;
	self.StreetPostDirControlId;
	self.StreetTypeControlId;
	self.UnitTypeControlId;
	self.UnitControlId;
	self.CityControlId;
	self.StateProvCdControlId;
	self.CountyControlId;
	self.PostalCodeControlId;
	self.RegionCdControlId;
	self.RegionISOCdControlId;
	self.LatitudeControlId;
	self.LongitudeControlId;
	self.VerifyImgId;
	self.StatusImgId;
	self.VerificationHashControlId;
	self.VerificationMsgControlId;
	self.DPVControlId;
	self.DPVDescControlId;
	self.DPVNotesControlId;
	self.DPVNotesDescControlId;	
	self.BarcodeDigitsControlId;
	self.CarrierRouteControlId;
	self.CongressCodeControlId;
	self.CountyCodeControlId;		
	self.VerificationMsgDivId;
	self.VerificationMsgIframeShieldId;
	self.LinkedAddressControlGroups = new Array();

	self.addressVerificationFailedImgSrc = "img/escalated-exclamation.gif";
	self.addressVerifiedImgSrc = "img/green-check.gif";
	self.processingImgSrc = "img/indicator.gif";
	self.addressVerifyImgSrc = "img/address-verify.gif";
	
	this.initializeUnparsed = function(address1, address2, city, county, stateProvCd, postalCode, country, countryCd, latitude, longitude, verificationHash, verificationMsg, verifyImg, statusImg){
		
		self.Address1ControlId = address1;
		self.Address2ControlId = address2;
		
		self.initializeCommon(city, county, stateProvCd, postalCode, country, countryCd, latitude, longitude, verificationHash, verificationMsg, verifyImg, statusImg);
		
	}

	this.initializeParsed = function(streetNum, streetDir, street, streetType, streetPostDir, unitType, unit, city, county, stateProvCd, postalCode, country, countryCd, latitude, longitude, verificationHash, verificationMsg, verifyImg, statusImg){
	
		self.StreetNumControlId = streetNum;
		self.StreetDirControlId = streetDir;
		self.StreetNameControlId = street;
		self.StreetPostDirControlId = streetPostDir;
		self.StreetTypeControlId = streetType;
		self.UnitTypeControlId = unitType;
		self.UnitControlId = unit;
		
		self.initializeCommon(city, county, stateProvCd, postalCode, country, countryCd, latitude, longitude, verificationHash, verificationMsg, verifyImg, statusImg);
		
	}
	
	this.initializeCommon = function(city, county, stateProvCd, postalCode, country, countryCd, latitude, longitude, verificationHash, verificationMsg, verifyImg, statusImg){
	
		self.CityControlId = city;
		self.StateProvCdControlId = stateProvCd;
		self.PostalCodeControlId = postalCode;
		self.CountyControlId = county;
		self.RegionCdControlId = country;
		self.RegionISOCdControlId = countryCd;
		self.LatitudeControlId = latitude;
		self.LongitudeControlId = longitude;
		self.VerificationHashControlId = verificationHash;
		self.VerifyImgId = verifyImg;
		self.StatusImgId = statusImg;
		self.VerificationMsgControlId = verificationMsg;

		self.initialize();
		
	
	}
	
	this.initialize = function(){
	
		self.VerificationMsgDivId = "div" + self.StatusImgId;
		self.VerificationMsgIframeShieldId = "iframeShield" + self.StatusImgId;
		
		var elemToReplace = document.getElementById(self.VerificationMsgDivId);
		if( elemToReplace ) 
			$( elemToReplace ).replaceWith("<div id='" + self.VerificationMsgDivId + "' class='ui-dialog ui-widget ui-widget-content ui-corner-all' style='visibility: hidden; z-index: 12'></div>");
		else
			$("body").append("<div id='" + self.VerificationMsgDivId + "' class='ui-dialog ui-widget ui-widget-content ui-corner-all' style='visibility: hidden; z-index: 12'></div>");
			
		elemToReplace = document.getElementById(self.VerificationMsgIframeShieldId);
		if( elemToReplace ) 
			$( elemToReplace ).replaceWith("<iframe id='" + self.VerificationMsgIframeShieldId + "' src='javascript:false;' style='position:absolute; top:0px; left:0px; height=0px; width=0px; visibility:hidden; border:0;'></iframe>");
		else
			$("body").append("<iframe id='" + self.VerificationMsgIframeShieldId + "' src='javascript:false;' style='position:absolute; top:0px; left:0px; height=0px; width=0px; visibility:hidden; border:0;'></iframe>");		
			
		if(self.getAddress1Control()){
			self.getAddress1Control().onkeyup = self.addressEdited;
		}
		if(self.getAddress2Control()){
			self.getAddress2Control().onkeyup = self.addressEdited;
		}
		if(self.getAddress3Control()){
			self.getAddress3Control().onkeyup = self.addressEdited;
		}
		if(self.getAddress4Control()){
			self.getAddress4Control().onkeyup = self.addressEdited;
		}
		if(self.getStreetNumControl()){
			self.getStreetNumControl().onkeyup = self.addressEdited;		
		}
		if(self.getStreetDirControl()){
			self.getStreetDirControl().onkeyup = self.addressEdited;		
		}
		if(self.getStreetNameControl()){
			self.getStreetNameControl().onkeyup = self.addressEdited;		
		}
		if(self.getStreetTypeControl()){
			self.getStreetTypeControl().onchange = self.addressEdited;		
		}
		if(self.getStreetPostDirControl()){
			self.getStreetPostDirControl().onkeyup = self.addressEdited;		
		}
		if(self.getUnitTypeControl()){
			self.getUnitTypeControl().onchange = self.addressEdited;		
		}
		if(self.getUnitControl()){
			self.getUnitControl().onkeyup = self.addressEdited;		
		}
		if(self.getCityControl()){
			self.getCityControl().onkeyup = self.addressEdited;
		}
		if(self.getStateProvCdControl()){
			self.getStateProvCdControl().onchange = self.addressEdited;
		}
		if(self.getPostalCodeControl()){
			self.getPostalCodeControl().onkeyup = 
				function(){
					self.addressEdited;
					toUpperCase(self.getPostalCodeControl());//Convert all PostalCode key inputs to upper case
				};
		}
		if(self.getCountyControl()){
			self.getCountyControl().onkeyup = self.addressEdited;
		}
		if(self.getRegionCdControl()){
			self.getRegionCdControl().onkeyup = self.addressEdited;
		}
		if(self.getRegionISOCdControl()){
			self.getRegionISOCdControl().onkeyup = self.addressEdited;
		}
		if(self.getLatitudeControl()){
			self.getLatitudeControl().onkeyup = self.addressEdited;
		}		
		if(self.getLongitudeControl()){
			self.getLongitudeControl().onkeyup = self.addressEdited;
		}		
		
		if(self.getStatusImgControl()){
			self.getStatusImgControl().onmouseover = self.onStatusImgControlMouseOver;
			self.getStatusImgControl().onmouseout = self.onStatusImgControlMouseOut;
		}
		
		if(self.getVerifyImgControl()){
			self.getVerifyImgControl().style.cursor = "pointer";
		}

		self.updateStatusImg();
	
	}
		
	this.getAddress1Control = function(){
		return document.getElementById(self.Address1ControlId);
	}
	this.getAddress1 = function(){
		return self.getAddress1Control() ? self.getAddress1Control().value : null;
	}
	
	this.setAddress1 = function(address1){
		if(self.getAddress1Control()){
			self.getAddress1Control().value = ( address1 ? address1 : "" );
		}
	}	
	
	this.getAddress2Control = function(){
		return document.getElementById(self.Address2ControlId);
	}
	
	this.getAddress2 = function(){
		return self.getAddress2Control() ? self.getAddress2Control().value : null;
	}
	
	this.setAddress2 = function(address2){
		if(self.getAddress2Control()){
			self.getAddress2Control().value = ( address2 ? address2 : "" );
		}
	}	
	
	this.getAddress3Control = function(){
		return document.getElementById(self.Address3ControlId);
	}
	
	this.getAddress3 = function(){
		return self.getAddress3Control() ? self.getAddress3Control().value : null;
	}
	
	this.setAddress3 = function(address3){
		if(self.getAddress3Control()){
			self.getAddress3Control().value = ( address3 ? address3 : "" );
		}
	}	
	
	this.getAddress4Control = function(){
		return document.getElementById(self.Address4ControlId);
	}
	
	this.getAddress4 = function(){
		return self.getAddress4Control() ? self.getAddress4Control().value : null;
	}
	
	this.setAddress4 = function(address4){
		if(self.getAddress4Control()){
			self.getAddress4Control().value = ( address4 ? address4 : "" );
		}
	}	
	
	this.getStreetNumControl = function(){
		return document.getElementById(self.StreetNumControlId);
	}
	
	this.getStreetNum = function(){
		return self.getStreetNumControl() ? self.getStreetNumControl().value : null;
	}
	
	this.setStreetNum = function(streetNum){
		if(self.getStreetNumControl()){
			self.getStreetNumControl().value = ( streetNum ? streetNum : "" );
		}
	}	
	
	this.getStreetDirControl = function(){
		return document.getElementById(self.StreetDirControlId);
	}
	
	this.getStreetDir = function(){
		return self.getStreetDirControl() ? self.getStreetDirControl().value : null;
	}
	
	this.setStreetDir = function(streetDir){
		if(self.getStreetDirControl()){
			self.getStreetDirControl().value = ( streetDir ? streetDir : "" );
		}
	}	
	
	this.getStreetNameControl = function(){
		return document.getElementById(self.StreetNameControlId);
	}
	
	this.getStreetName = function(){
		return self.getStreetNameControl() ? self.getStreetNameControl().value : null;
	}
	
	this.setStreetName = function(streetName){
		if(self.getStreetNameControl()){
			self.getStreetNameControl().value = ( streetName ? streetName : "" );
		}
	}	
	
	this.getStreetTypeControl = function(){
		return document.getElementById(self.StreetTypeControlId);
	}
	
	this.getStreetType = function(){
		return self.getStreetTypeControl() ? self.getStreetTypeControl().value : null;
	}
	
	this.setStreetType = function(streetType){
		if(self.getStreetTypeControl()){
			if(self.getStreetTypeControl().type == "select-one"){
				setSelectedItem(self.StreetTypeControlId, streetType);				
			}
			else{
				self.getStreetTypeControl().value = ( streetType ? streetType : "" );
			}
		}
	}	
	
	this.getStreetPostDirControl = function(){
		return document.getElementById(self.StreetPostDirControlId);
	}
	
	this.getStreetPostDir = function(){
		return self.getStreetPostDirControl() ? self.getStreetPostDirControl().value : null;
	}
	
	this.setStreetPostDir = function(streetPostDir){
		if(self.getStreetPostDirControl()){
			self.getStreetPostDirControl().value = ( streetPostDir ? streetPostDir : "" );
		}
	}

	this.getUnitTypeControl = function(){
		return document.getElementById(self.UnitTypeControlId);
	}
	
	this.getUnitType = function(){
		return self.getUnitTypeControl() ? self.getUnitTypeControl().value : null;
	}
	
	this.setUnitType = function(unitType){
		if(self.getUnitTypeControl()){
			
			// Convert STE to SUITE
			if(unitType && unitType.toUpperCase() == "STE"){
				unitType = "Suite";
			}
			
			if(self.getStreetTypeControl().type == "select-one"){
				setSelectedItem(self.UnitTypeControlId, unitType);			
			}
			else{
				self.getUnitTypeControl().value = ( unitType ? unitType : "" );
			}
		}
	}	

	this.getUnitControl = function(){
		return document.getElementById(self.UnitControlId);
	}
	
	this.getUnit = function(){
		return self.getUnitControl() ? self.getUnitControl().value : null;
	}
	
	this.setUnit = function(unit){
		if(self.getUnitControl()){
			self.getUnitControl().value = ( unit ? unit : "" );
		}
	}	

	this.getCityControl = function(){
		return document.getElementById(self.CityControlId);
	}
	
	this.getCity = function(){
		return self.getCityControl() ? self.getCityControl().value : null;
	}
	
	this.setCity = function(city){
		if(self.getCityControl()){
			self.getCityControl().value = ( city ? city : "" );
		}
	}	
	
	this.getStateProvCdControl = function(){
		return document.getElementById(self.StateProvCdControlId);
	}
	
	this.getStateProvCd = function(){
		return self.getStateProvCdControl() ? self.getStateProvCdControl().value : null;
	}
	
	this.setStateProvCd = function(stateProvCd){
		if(self.getStateProvCdControl()){
			self.getStateProvCdControl().value = stateProvCd;
		}
	}	
	
	this.getCountyControl = function(){
		return document.getElementById(self.CountyControlId);
	}
	
	this.getCounty = function(){
		return self.getCountyControl() ? self.getCountyControl().value : null;	
	}
	
	this.setCounty = function(county){
		if(self.getCountyControl()){
			self.getCountyControl().value = ( county ? county : "" );
		}
	}	
	
	this.getPostalCodeControl = function(){
		return document.getElementById(self.PostalCodeControlId);				
	}
	
	this.getPostalCode = function(){
		return self.getPostalCodeControl() ? self.getPostalCodeControl().value : null;				
	}
	
	this.setPostalCode = function(postalCode){
		if(self.getPostalCodeControl()){
			self.getPostalCodeControl().value = ( postalCode ? postalCode : "" );
		}
	}	
		
	this.getRegionCdControl = function(){
		return document.getElementById(self.RegionCdControlId);	
	}
	
	this.getRegionCd = function(){
		return self.getRegionCdControl() ? self.getRegionCdControl().value : null;	
	}
	
	this.setRegionCd = function(country){
		if(self.getRegionCdControl()){
			self.getRegionCdControl().value = ( country ? country : "" );
		}
	}
	
	this.getRegionISOCdControl = function(){
		return document.getElementById(self.RegionISOCdControlId);	
	}
	
	this.getRegionISOCd = function(){
		return self.getRegionISOCdControl() ? self.getRegionISOCdControl().value : null;	
	}
	
	this.setRegionISOCd = function(countryCd){
		if(self.getRegionISOCdControl()){
			self.getRegionISOCdControl().value = ( countryCd ? countryCd : "" );
		}
	}
	
	this.getLatitudeControl = function(){
		return document.getElementById(self.LatitudeControlId);	
	}
	
	this.getLatitude = function(){
		return self.getLatitudeControl() ? self.getLatitudeControl().value : null;	
	}
	
	this.setLatitude = function(latitude){
		if(self.getLatitudeControl()){
			self.getLatitudeControl().value = ( latitude ? latitude : "" );
		}
	}
	
	this.getLongitudeControl = function(){
		return document.getElementById(self.LongitudeControlId);	
	}
	
	this.getLongitude = function(){
		return self.getLongitudeControl() ? self.getLongitudeControl().value : null;	
	}
	
	this.setLongitude = function(longitude){
		if(self.getLongitudeControl()){
			self.getLongitudeControl().value = ( longitude ? longitude : "" );
		}
	}
	
	this.getDPVControl = function(){
		return document.getElementById(self.DPVControlId);	
	}
	
	this.getDPV = function(){
		return self.getDPVControl() ? self.getDPVControl().value : null;	
	}
	
	this.setDPV = function(DPV){
		if(self.getDPVControl()){
			self.getDPVControl().value = ( DPV ? DPV : "" );
		}
	}
	
	this.getDPVDescControl = function(){
		return document.getElementById(self.DPVDescControlId);	
	}
	
	this.getDPVDesc = function(){
		return self.getDPVDescControl() ? self.getDPVDescControl().value : null;	
	}
	
	this.setDPVDesc = function(DPVDesc){
		if(self.getDPVDescControl()){
			self.getDPVDescControl().value = ( DPVDesc ? DPVDesc : "" );
		}
	}
	
	this.getDPVNotesControl = function(){
		return document.getElementById(self.DPVNotesControlId);	
	}
	
	this.getDPVNotes = function(){
		return self.getDPVNotesControl() ? self.getDPVNotesControl().value : null;	
	}
	
	this.setDPVNotes = function(DPVNotes){
		if(self.getDPVNotesControl()){
			self.getDPVNotesControl().value = ( DPVNotes ? DPVNotes : "" );
		}
	}
	
	this.getDPVNotesDescControl = function(){
		return document.getElementById(self.DPVNotesDescControlId);	
	}
	
	this.getDPVNotesDesc = function(){
		return self.getDPVNotesDescControl() ? self.getDPVNotesDescControl().value : null;	
	}
	
	this.setDPVNotesDesc = function(DPVNotesDesc){
		if(self.getDPVNotesDescControl()){
			self.getDPVNotesDescControl().value = ( DPVNotesDesc ? DPVNotesDesc : "" );
		}
	}	
	
	this.getBarcodeDigitsControl = function(){
		return document.getElementById(self.BarcodeDigitsControlId);	
	}
	
	this.getBarcodeDigits = function(){
		return self.getBarcodeDigitsControl() ? self.getBarcodeDigitsControl().value : null;	
	}
	
	this.setBarcodeDigits = function(barcodeDigits){
		if(self.getBarcodeDigitsControl()){
			self.getBarcodeDigitsControl().value = ( barcodeDigits ? barcodeDigits : "" );
		}
	}	
	
	this.getCarrierRouteControl = function(){
		return document.getElementById(self.CarrierRouteControlId);	
	}
	
	this.getCarrierRoute = function(){
		return self.getCarrierRouteControl() ? self.getCarrierRouteControl().value : null;	
	}
	
	this.setCarrierRoute = function(carrierRoute){
		if(self.getCarrierRouteControl()){
			self.getCarrierRouteControl().value = ( carrierRoute ? carrierRoute : "" );
		}
	}

	this.getCongressCodeControl = function(){
		return document.getElementById(self.CongressCodeControlId);	
	}
	
	this.getCongressCode = function(){
		return self.getCongressCodeControl() ? self.getCongressCodeControl().value : null;	
	}
	
	this.setCongressCode = function(congressCode){
		if(self.getCongressCodeControl()){
			self.getCongressCodeControl().value = ( congressCode ? congressCode : "" );
		}
	}
	
	this.getCountyCodeControl = function(){
		return document.getElementById(self.CountyCodeControlId);	
	}
	
	this.getCountyCode = function(){
		return self.getCountyCodeControl() ? self.getCountyCodeControl().value : null;	
	}
	
	this.setCountyCode = function(countyCode){
		if(self.getCountyCodeControl()){
			self.getCountyCodeControl().value = ( countyCode ? countyCode : "" );
		}
	}
	
	this.getVerifyImgControl = function(){
		return document.getElementById(self.VerifyImgId);
	}
	
	this.getStatusImgControl = function(){
		return document.getElementById(self.StatusImgId);
	}
	
	this.onStatusImgControlMouseOver = function(){
		if(self.getVerificationMsg()){
			verificationMsgDivShow( self.VerificationMsgDivId, self.VerificationMsgIframeShieldId, self.getVerificationMsg() );
		}
	}
	
	this.onStatusImgControlMouseOut = function(){
		verificationMsgDivHide( self.VerificationMsgDivId, self.VerificationMsgIframeShieldId );
		
	}
	
	this.getVerificationHashControl = function(){
		return document.getElementById(self.VerificationHashControlId);
	}
	
	this.getStoredVerificationHash = function(){
		return self.getVerificationHashControl() ? self.getVerificationHashControl().value : null;
	}
	
	this.setStoredVerificationHash = function(hash){
		if(self.getVerificationHashControl()){
			self.getVerificationHashControl().value = ( hash ? hash : "");
		}
	}
	
	this.addressEdited = function(e){			
		self.updateStatusImg();
	}
	
	this.getVerificationMsg = function() {
		return self.getVerificationMsgControl() ? self.getVerificationMsgControl().value : null;
	}
	
	this.setVerificationMsg = function(msg) {
		if(self.getVerificationMsgControl()){
			self.getVerificationMsgControl().value = ( msg ? msg : "" );
		}
	}
	
	this.getVerificationMsgControl = function() {
		return document.getElementById(self.VerificationMsgControlId);
	}
	
	// This computes an address hash using the latest/current algorithm.
	// Note that other hash algorithms may still be consider valid by addressHashMatches()
	// for existing data (see getVerificationHashLegacy* functions).
	// This function must match the behavior of AddressTools.getAddressHash(). A change to one must be made to the other.
	this.getVerificationHash = function(){
    	
    	var combined = "";
    	combined += self.getAddress1() ? (self.getAddress1() + " ") : "";
    	combined += self.getAddress2() ? (self.getAddress2() + " ") : "";
    	combined += self.getAddress3() ? (self.getAddress3() + " ") : "";
    	combined += self.getAddress4() ? (self.getAddress4() + " ") : "";
    	combined += self.getStreetNum() ? (self.getStreetNum() + " ") : "";
    	combined += self.getStreetDir() ? (self.getStreetDir() + " ") : "";
    	combined += self.getStreetName() ? (self.getStreetName() + " ") : "";
    	combined += self.getStreetType() ? (self.getStreetType() + " ") : "";
    	combined += self.getStreetPostDir() ? (self.getStreetPostDir() + " ") : "";
    	combined += self.getUnitType() ? (self.getUnitType() + " ") : "";
    	combined += self.getUnit() ? (self.getUnit() + " ") : "";
    	combined += self.getCity() ? (self.getCity() + ", ") : "";
    	combined += self.getCounty() ? (self.getCounty()  + " ") : "";
    	combined += self.getStateProvCd() ? (self.getStateProvCd()  + " ") : "";
    	combined += self.getPostalCode() ? (self.getPostalCode() + " "): "";
    	combined += self.getRegionCd() ? (self.getRegionCd()  + " "): "";
    	combined += self.getRegionISOCd() ? (self.getRegionISOCd() + " "): "";
    	
    	combined = combined.toUpperCase();
	
		var hash = "";
		for(i=0; i<combined.length; i++){
			var c = combined.charCodeAt(i);
			if(( c > 47 && c<58) || (c > 64 && c<91) || (c>96 && c<123) || (c==32)){
				// Check for double spaces
				if(!(hash.length > 0 && (hash.charCodeAt(hash.length - 1) == 32) && (c==32))){
					hash = hash + combined.charAt(i);
				}
			}
		}
		
		// Trim the string
		while (hash.substring(0,1) == ' '){
			hash = hash.substring(1, hash.length);
		}
		while(hash.substring(hash.length-1, hash.length) == ' '){
			hash = hash.substring(0,hash.length-1);
		}
		
		return hash;
		
	}
	
	// This computes an address hash using the algorithm in use prior to 3.1.1.0 (INC-2911)
	// basically without RegionCd or RegionISOCd. RegionCd was actually included - called CountryCd then -
	// but because it was unlikely to contain any data this algorithm represents the common condition
	// where there was no data in RegionCd so it was not included in the resulting hash.
	// Note: The lack of Addr3 and Addr4 in the hash, which was added in INC-1714, which was also
	// introduced in 3.1.1.0, is not considered a legacy hash algorithm because Addr3 and Addr4 should
	// have been included. In other words, this is considered a bug and we want addresses with 3 and 4
	// to no longer be considered verified.
	// This function must match the behavior of AddressTools.getAddressHashLegacy1(). A change to one must be made to the other.
	this.getVerificationHashLegacy1 = function(){
    	
    	var combined = "";
    	combined += self.getAddress1() ? (self.getAddress1() + " ") : "";
    	combined += self.getAddress2() ? (self.getAddress2() + " ") : "";
    	combined += self.getAddress3() ? (self.getAddress3() + " ") : "";
    	combined += self.getAddress4() ? (self.getAddress4() + " ") : "";
    	combined += self.getStreetNum() ? (self.getStreetNum() + " ") : "";
    	combined += self.getStreetDir() ? (self.getStreetDir() + " ") : "";
    	combined += self.getStreetName() ? (self.getStreetName() + " ") : "";
    	combined += self.getStreetType() ? (self.getStreetType() + " ") : "";
    	combined += self.getStreetPostDir() ? (self.getStreetPostDir() + " ") : "";
    	combined += self.getUnitType() ? (self.getUnitType() + " ") : "";
    	combined += self.getUnit() ? (self.getUnit() + " ") : "";
    	combined += self.getCity() ? (self.getCity() + ", ") : "";
    	combined += self.getCounty() ? (self.getCounty()  + " ") : "";
    	combined += self.getStateProvCd() ? (self.getStateProvCd()  + " ") : "";
    	combined += self.getPostalCode() ? (self.getPostalCode() + " "): "";
    	
    	combined = combined.toUpperCase();
	
		var hash = "";
		for(i=0; i<combined.length; i++){
			var c = combined.charCodeAt(i);
			if(( c > 47 && c<58) || (c > 64 && c<91) || (c>96 && c<123) || (c==32)){
				// Check for double spaces
				if(!(hash.length > 0 && (hash.charCodeAt(hash.length - 1) == 32) && (c==32))){
					hash = hash + combined.charAt(i);
				}
			}
		}
		
		// Trim the string
		while (hash.substring(0,1) == ' '){
			hash = hash.substring(1, hash.length);
		}
		while(hash.substring(hash.length-1, hash.length) == ' '){
			hash = hash.substring(0,hash.length-1);
		}
		
		return hash;
		
	}
	
	// This computes an address hash using the algorithm in use prior to 3.1.1.0 (INC-2911)
	// basically without RegionISOCd.
	// This function must match the behavior of AddressTools.getAddressHashLegacy2(). A change to one must be made to the other.
	this.getVerificationHashLegacy2 = function(){
    	
    	var combined = "";
    	combined += self.getAddress1() ? (self.getAddress1() + " ") : "";
    	combined += self.getAddress2() ? (self.getAddress2() + " ") : "";
    	combined += self.getAddress3() ? (self.getAddress3() + " ") : "";
    	combined += self.getAddress4() ? (self.getAddress4() + " ") : "";
    	combined += self.getStreetNum() ? (self.getStreetNum() + " ") : "";
    	combined += self.getStreetDir() ? (self.getStreetDir() + " ") : "";
    	combined += self.getStreetName() ? (self.getStreetName() + " ") : "";
    	combined += self.getStreetType() ? (self.getStreetType() + " ") : "";
    	combined += self.getStreetPostDir() ? (self.getStreetPostDir() + " ") : "";
    	combined += self.getUnitType() ? (self.getUnitType() + " ") : "";
    	combined += self.getUnit() ? (self.getUnit() + " ") : "";
    	combined += self.getCity() ? (self.getCity() + ", ") : "";
    	combined += self.getCounty() ? (self.getCounty()  + " ") : "";
    	combined += self.getStateProvCd() ? (self.getStateProvCd()  + " ") : "";
    	combined += self.getPostalCode() ? (self.getPostalCode() + " "): "";
    	combined += self.getRegionCd() ? (self.getRegionCd()  + " "): "";
    	
    	combined = combined.toUpperCase();
	
		var hash = "";
		for(i=0; i<combined.length; i++){
			var c = combined.charCodeAt(i);
			if(( c > 47 && c<58) || (c > 64 && c<91) || (c>96 && c<123) || (c==32)){
				// Check for double spaces
				if(!(hash.length > 0 && (hash.charCodeAt(hash.length - 1) == 32) && (c==32))){
					hash = hash + combined.charAt(i);
				}
			}
		}
		
		// Trim the string
		while (hash.substring(0,1) == ' '){
			hash = hash.substring(1, hash.length);
		}
		while(hash.substring(hash.length-1, hash.length) == ' '){
			hash = hash.substring(0,hash.length-1);
		}
		
		return hash;
		
	}
	
	// Use all known hash algorithms from current to oldest to verify if this address still matches
	// the given hash value (usually the one returned by self.getStoredVerificationHash).
	// This function must match the behavior of AddressTools.isAddressVerified(). A change to one must be made to the other.
	this.addressHashMatches = function(storedVerificationHash) {
		// Try the current hash algorithm first
		if (self.getVerificationHash() == storedVerificationHash && self.getVerificationHash().length > 0) {
			return true;
		}
		// If that didn't match, try the next oldest algorithm
		if (self.getVerificationHashLegacy2() == storedVerificationHash && self.getVerificationHashLegacy2().length > 0) {
			return true;
		}
		// If that didn't match, try the next oldest algorithm
		if (self.getVerificationHashLegacy1() == storedVerificationHash && self.getVerificationHashLegacy1().length > 0) {
			return true;
		}
		// If we get here, no algorithm matched
		return false;
	}
	
	this.updateStatusImg = function(){
	
		if(self.getStatusImgControl() && self.getVerificationHashControl()){
		
			if(self.getVerificationMsg() && self.getVerificationMsg().length > 0){			
				self.getStatusImgControl().src = self.addressVerificationFailedImgSrc;
				self.getStatusImgControl().style.cursor = "help";
				self.getStatusImgControl().style.visibility = "visible";
				self.getStatusImgControl().onclick = "";
				self.getStatusImgControl().title = "";
			}
			else if(self.addressHashMatches(self.getStoredVerificationHash())){
				self.getStatusImgControl().src = self.addressVerifiedImgSrc;	
				self.getStatusImgControl().style.cursor = "pointer";
				self.getStatusImgControl().style.visibility = "visible";
				self.getStatusImgControl().title = "Click to open map";
				
				var onClickCmd = "analyticsTrackPageview('address-maplink');window.open('"+self.getMapLink()+"')";
				self.getStatusImgControl().onclick = new Function(onClickCmd); 
			}
			else{
				self.getStatusImgControl().style.visibility = "hidden";
				self.getStatusImgControl().onclick = "";
			}

		}	
				
	}
	
	this.verify = function(overrideServices){
		
		if(self.getStatusImgControl()){
			self.getStatusImgControl().style.visibility = "hidden";
		}
		
		var securityId = document.getElementById("SecurityId") ? document.getElementById("SecurityId").value : null;
		
		self.getVerifyImgControl().src = self.processingImgSrc;
		var xform = "<?xml version='1.0' ?><xfdf><fields>";    	
	    xform = xform += "<SafeCopy>true</SafeCopy>";
	    xform = xform += "<rq>COAddressVerification</rq>";
	    xform = xform += "<SecurityId>" + securityId + "</SecurityId>";
	    xform = xform += "<ResponseAsXml>Yes</ResponseAsXml>";
	    xform = xform += "<Addr1>" + (self.getAddress1() ? escapeXml(self.getAddress1()) : "")  + "</Addr1>";
	    xform = xform += "<Addr2>" + (self.getAddress2() ? escapeXml(self.getAddress2()) : "") + "</Addr2>";
	    xform = xform += "<Addr3>" + (self.getAddress3() ? escapeXml(self.getAddress3()) : "")  + "</Addr3>";
	    xform = xform += "<Addr4>" + (self.getAddress4() ? escapeXml(self.getAddress4()) : "") + "</Addr4>";
		xform = xform += "<PrimaryNumber>" + (self.getStreetNum() ? escapeXml(self.getStreetNum()) : "") + "</PrimaryNumber>";
		xform = xform += "<PreDirectional>" + (self.getStreetDir() ? escapeXml(self.getStreetDir()) : "") + "</PreDirectional>";
		xform = xform += "<StreetName>" + (self.getStreetName() ? escapeXml(self.getStreetName()) : "") + "</StreetName>";
		xform = xform += "<Suffix>" + (self.getStreetType() ? escapeXml(self.getStreetType()) : "") + "</Suffix>";
		xform = xform += "<PostDirectional>" + (self.getStreetPostDir() ? escapeXml(self.getStreetPostDir()) : "") + "</PostDirectional>";
		xform = xform += "<SecondaryDesignator>" + (self.getUnitType() ? escapeXml(self.getUnitType()) : "") + "</SecondaryDesignator>";
		xform = xform += "<SecondaryNumber>" + (self.getUnit() ? escapeXml(self.getUnit()) : "") + "</SecondaryNumber>";
	    xform = xform += "<City>" + (self.getCity() ? escapeXml(self.getCity()) : "") + "</City>";
	    xform = xform += "<County>" + (self.getCounty () ? escapeXml(self.getCounty()) : "") + "</County>";
	    xform = xform += "<StateProvCd>" + (self.getStateProvCd() ? escapeXml(self.getStateProvCd()) : "") + "</StateProvCd>";
	    xform = xform += "<PostalCode>" + (self.getPostalCode() ? escapeXml(self.getPostalCode()) : "") + "</PostalCode>";
	   	xform = xform += "<RegionCd>" + (self.getRegionCd () ? escapeXml(self.getRegionCd()) : "") + "</RegionCd>";
	   	xform = xform += "<RegionISOCd>" + (self.getRegionISOCd () ? escapeXml(self.getRegionISOCd()) : "") + "</RegionISOCd>";
	   	
	   	var i;
	   	var thisItem;
	   	var include = false;
	   	var serviceName;
	   	var serviceNamesAnalytics = "";
	   	
	   	// If the caller provided the services to run, only run those
	   	if(overrideServices){
	   		for(i=0; i<overrideServices.length; i++){
	   			serviceName = overrideServices[i];
	   			xform = xform += "<" + serviceName + ">Yes</" + serviceName + ">";
	   			var serviceNameAnalytics = serviceName.replace("AddressVerificationService.", "");
	   			if(i==0) {
	   				serviceNamesAnalytics = "?ServiceNames=" + serviceNameAnalytics;
	   			} else {
	   				serviceNamesAnalytics = serviceNamesAnalytics + "," + serviceNameAnalytics;
	   			}
	   		}
	   	}
	   	// Otherwise, loop through all items in the form that start with
		// "AddressVerificationService"
	   	else{
		   	for(i=0; i<document.getElementById("frmCMM").elements.length; i++){
		   		thisItem = document.getElementById("frmCMM").elements[i];
		   		if(thisItem.name.indexOf("AddressVerificationService") == 0){
		   			serviceName = thisItem.name;
		   				   			
		   			xform = xform += "<" + serviceName + ">";
		   		   	
		   		   	var included = false;
		   		   	
		   			// If this is a checkbox, and it is checked, add the service
		   			if(thisItem.type == "checkbox" && thisItem.checked == true){
		   				xform += "Yes";
		   				included = true;
		   			}
		   			// If this is a hidden field, and the value is "True" or
					// "Yes", add the service
		   			else if(thisItem.type == "hidden" && (thisItem.value.toLowerCase() == "yes" || thisItem.value.toLowerCase() == "true")){
		   				xform += "Yes";	   				
		   				included = true;
		   			}
		   			else{
		   				xform += "No";
		   				included = false;
		   			}
		   			xform += "</" + serviceName + ">";
		   			
		   			if(included) {
		   				var serviceNameAnalytics = serviceName.replace("AddressVerificationService.", "");
			   			if(serviceNamesAnalytics == "") {
			   				serviceNamesAnalytics = "?ServiceNames=" + serviceNameAnalytics;
			   			} else {
			   				serviceNamesAnalytics = serviceNamesAnalytics + "," + serviceNameAnalytics;
			   			}
		   			}
		   		}
		   	}	   		
	   	}

	   	xform = xform += "</fields></xfdf>";
	
	   	// Clear the Parsed Address fields for CombinedAddress Group
	   	if(self.AddressType == "Combined") {
			self.setStreetNum("");
			self.setStreetDir("");
			self.setStreetName("");
			self.setStreetType("");
			self.setStreetPostDir("");
			self.setUnitType("");
			self.setUnit("");
	    	self.setCounty("");
	    	self.setStoredVerificationHash("");
	    	self.setLatitude("");
	    	self.setLongitude("");
		}
		
		// alert(xform);
		var params = new Object();
		params.XForm = xform;

		analyticsTrackPageview("address-verify" + serviceNamesAnalytics);
		
		window.seleniumPageLoadOutstanding++;

		var deferred = $.post("innovation", $.param(params), function(data) {
			handleReturnedAddressVerification(data, self);
			window.seleniumPageLoadOutstanding--;
		}, "xml");	
		
		avlog(xform);

		return deferred;
	}
	
	this.setAddress = function(address, previouslyUpdated, manuallySelected){
	
		self.setAddress1(address.Address1);
		self.setAddress2(address.Address2);

		self.setStreetNum(address.StreetNum);
		self.setStreetDir(address.StreetDir);
		self.setStreetName(address.StreetName);
		self.setStreetType(address.StreetType);
		self.setStreetPostDir(address.StreetPostDir);
		
		// If the address has address2, but this is a parsed control group,
		// parse the address2
		// as best we can
		if(address.Address2 && !address.UnitType && !self.Address2Control ){
			
			var address2collapsed = address.Address2.replace(/\s+/g," "); // reduce all whitespace to single spaces
			var tokens = address2collapsed.split(" ");
			if(tokens.length == 2){
				var unitType = tokens[0].toUpperCase().replace(/[^A-Z0-9]/g,"");   // uppercase and remove all special characters
				
				self.setUnit(tokens[1]);
				// Since unitType is often a dropdown, if we weren't able to set
				// anything because the dropdown
				// didn't have the value, just force it to a default
				if(self.getUnitType() != unitType){
					self.setUnitType("UNIT");
				}
			} else if (tokens.length == 1 && tokens[0].substring(0,1) == "#") {
				// Special case of "#n". Convert "#" to "UNIT" and n to the unit
				// number
				self.setUnitType("UNIT");
				self.setUnit(tokens[0].substring(1));
			}
			
		} 
		else{
			self.setUnitType(address.UnitType);
			self.setUnit(address.Unit);
		}
		
		self.setCity(address.City);
		self.setStateProvCd(address.StateProvCd);
		self.setPostalCode(address.PostalCode);
		self.setCounty(address.County);
		self.setRegionCd(address.RegionCd);
		self.setRegionISOCd(address.RegionISOCd);
		self.setLatitude(address.Latitude);
		self.setLongitude(address.Longitude);
		
		self.setDPV(address.DPV);
		self.setDPVDesc(address.DPVDesc);
		self.setDPVNotes(address.DPVNotes);
		self.setDPVNotesDesc(address.DPVNotesDesc);
		
		self.setBarcodeDigits(address.BarcodeDigits);
		self.setCarrierRoute(address.CarrierRoute);
		self.setCongressCode(address.CongressCode);
		self.setCountyCode(address.CountyCode);		
			
		
		// Verify and force revalidation. This will deal with geocoding and DPV that may not be available from the popup data
		if(manuallySelected && (!address.Latitude || address.Latitude == "" || !address.DPV || address.DPV == "") ){
			self.verify();
		}
		else{
			self.updateLinkedAddresses(address, previouslyUpdated);
			self.setStoredVerificationHash( self.getVerificationHash() );
			self.setVerificationMsg("");
			self.updateStatusImg();
			self.onSuccess(self);	
		}
		
		
	}
	
	this.getAddress = function(){
		
		var address = new CombinedAddress();
		
		address.Address1 = self.getAddress1();
		address.Address2 = self.getAddress2();
		address.Address3 = self.getAddress3();
		address.Address4 = self.getAddress4();
		address.StreetNum = self.getStreetNum();
		address.StreetDir = self.getStreetDir();
		address.StreetName = self.getStreetName();
		address.StreetType = self.getStreetType();
		address.StreetPostDir = self.getStreetPostDir();
		address.UnitType =  self.getUnitType();
		address.Unit =  self.getUnit();
		address.City = self.getCity();
		address.StateProvCd = self.getStateProvCd();
		address.PostalCode = self.getPostalCode();
		address.County = self.getCounty();
		address.Latitude = self.getLatitude();
		address.Longitude = self.getLongitude();
		address.RegionCd = self.getRegionCd();
		address.RegionISOCd = self.getRegionISOCd();
		
		return address;
	
	}
	
	this.getMapLink = function(){
	
		var address1 = self.getAddress1();
		if(!address1){
			address1 = "";
			address1 += self.getStreetNum() ? (self.getStreetNum() + " ") : "";
			address1 += self.getStreetDir() ? (self.getStreetDir() + " ") : "";
			address1 += self.getStreetName() ? (self.getStreetName() + " ") : "";
			address1 += self.getStreetType() ? (self.getStreetType() + " ") : "";
			address1 += self.getStreetPostDir() ? (self.getStreetPostDir() + "") : "";
		}
		
		var address2 = self.getAddress2();
		if(!address2){
			address2 = "";
			address2 += self.getUnitType() ? (self.getUnitType() + " ") : "";
			address2 += self.getUnit() ? (self.getUnit()) : "";
		}
		
		var link = googleMapsLink(
			address1, 
			address2,
			self.getCity(),
			self.getStateProvCd()
		);
		
		return link;
	}
	
	this.updateLinkedAddresses = function(address, previouslyUpdated){
	
		// Create the previously updated Array
		if(!previouslyUpdated){
			previouslyUpdated = new Array();
		}
		
		// Add this object to the previously updated array
		previouslyUpdated.push(self);
		
		for(i=0; i<self.LinkedAddressControlGroups.length; i++){
			var group = self.LinkedAddressControlGroups[i];
			var alreadyUpdated = false;
			
			for(j=0; j<previouslyUpdated.length; j++){
				if(group == previouslyUpdated[j]){
					alreadyUpdated = true;
					break;		
				}
			}			
			
			if(!alreadyUpdated){
				group.setAddress( address, previouslyUpdated );		
			}	
		}

	}
	
	this.addLinkedAddressControlGroup = function(group){
		
		this.LinkedAddressControlGroups.push(group);
	
	}
	
	this.onSuccess = function(obj) {
		// Point this function to your own function if you want your code to be called after verification
	}
	
	this.onFailure = function(obj) {
		// Point this function to your own function if you want your code to be called after verification
	}
}

function handleReturnedAddressVerification( data, obj ) {
	
	// Set the image back to normal
	obj.getVerifyImgControl().src = obj.addressVerifyImgSrc;
	
	// First, make sure we got a COAddressVerificationRs. If not, something bad
	// happened
	var errorMsg = "";
	var rs = data.getElementsByTagName("COAddressVerificationRs");
	if(!rs || rs.length == 0){
		var errorMsg = "Unexpected error validating address. \nYour session may have expired.  Please relogin and try again.";
		obj.setVerificationMsg(errorMsg);
		obj.updateStatusImg();
		alert(errorMsg);
		obj.onFailure(obj);
		return;
	}

	avlog(XMLtoString(data));
	
	// Check for a verification error message
	var errors = data.getElementsByTagName("Error");
	if(errors.length > 0){
		var errorMsg = "Could not match address:\n";
		for(i=0; i<errors.length; i++){
			errorMsg = errorMsg + errors[i].getAttribute("Msg");
			if(i< errors.length - 1){
				errorMsg = errorMsg + "\n";
			}
		}
		obj.setVerificationMsg(errorMsg.replace(/\n/g, "<BR>"));
		obj.updateStatusImg();
		alert(errorMsg);
		obj.onFailure(obj);
		return;
	}
	
	// Get the address
	var validatedAddress;
	var addressesBeans = data.getElementsByTagName("Addresses");
	var addresses = [];
	for(j=0;j<addressesBeans.length;j++){
		var addrBeans = addressesBeans[j].getElementsByTagName("Addr");
		for(i=0; i<addrBeans.length;i++){
			var address = new CombinedAddress();
			address.Address1 = addrBeans[i].getAttribute("Addr1"); 
			address.Address2 = addrBeans[i].getAttribute("Addr2");
			address.Address3 = addrBeans[i].getAttribute("Addr3");
			address.Address4 = addrBeans[i].getAttribute("Addr4");
			address.City = addrBeans[i].getAttribute("City");
			address.StateProvCd = addrBeans[i].getAttribute("StateProvCd");
			address.PostalCode = addrBeans[i].getAttribute("PostalCode");
			address.RegionCd = addrBeans[i].getAttribute("RegionCd");
			address.RegionISOCd = addrBeans[i].getAttribute("RegionISOCd");
			address.County = addrBeans[i].getAttribute("County");
			address.StreetNum = addrBeans[i].getAttribute("PrimaryNumber");
			address.StreetName = addrBeans[i].getAttribute("StreetName");
			address.StreetDir = addrBeans[i].getAttribute("PreDirectional");
			address.StreetType = addrBeans[i].getAttribute("Suffix");
			address.StreetPostDir = addrBeans[i].getAttribute("PostDirectional");
			address.UnitType = addrBeans[i].getAttribute("SecondaryDesignator");
			address.Unit = addrBeans[i].getAttribute("SecondaryNumber");
			address.Latitude = addrBeans[i].getAttribute("Latitude");
			address.Longitude = addrBeans[i].getAttribute("Longitude");
			address.Score = addrBeans[i].getAttribute("Score");
			address.DPV = addrBeans[i].getAttribute("DPV");
			address.DPVDesc = addrBeans[i].getAttribute("DPVDesc");
			address.DPVNotes = addrBeans[i].getAttribute("DPVNotes");
			address.DPVNotesDesc = addrBeans[i].getAttribute("DPVNotesDesc");
			address.BarcodeDigits = addrBeans[i].getAttribute("BarcodeDigits");
			address.CarrierRoute = addrBeans[i].getAttribute("CarrierRoute");
			address.CongressCode = addrBeans[i].getAttribute("CongressCode");
			address.CountyCode = addrBeans[i].getAttribute("CountyCode");			
			
			if(addrBeans[i].getAttribute("AddrTypeCd") == "ValidatedAddress"){
				validatedAddress = address;
			}
			else{			
				addresses.push(address);
			}
		}
	}	
	
	if(validatedAddress){
		// If there is only one address, with a score of 100, or no score,
		// immediately update...
		if(validatedAddress.Score == "" || validatedAddress.Score == "100" || validatedAddress.Score == "100.00"){
			obj.setAddress(validatedAddress);			
		}
		else{
			// Pop up the address
			chooseAddress(obj, [validatedAddress]);
		}
	}
	else if(addresses != null && addresses.length > 0){
		// Choose from among the available address matches
		chooseAddress(obj, addresses);
	}
	else{
		alert("No addresses returned");
	}
	
}


function chooseAddress(addrControlGrp, addresses){

	_currentAddressCtlGrp = addrControlGrp;
	_currentAddresses = addresses;
	analyticsTrackPageview("address-chooser.html");
	var windowObj = window.open("address-chooser.html","addressChooser","height=300,width=430,status=yes,toolbar=no,menubar=no,location=no");
	if(!windowObj.opener){
		windowObj.opener = this.window; 
	}
	else{
		windowObj.focus();
    }
	
}

function addressServiceRequested(serviceName){
	
	for(i=0; i<document.getElementById("frmCMM").elements.length; i++){
   		thisItem = document.getElementById("frmCMM").elements[i];
   		if(thisItem.name == "AddressVerificationService." + serviceName){
   			if(thisItem.type == "checkbox" && thisItem.checked == true){
   				return true;
   			}
   			else if(thisItem.type == "hidden" && (thisItem.value.toLowerCase() == "yes" || thisItem.value.toLowerCase() == "true")){
   				return true;				
   			}
   		}
	}
	
	return false;
	
}

function getCurrentAddressCtlGrp(){
	return _currentAddressCtlGrp;
}

function getCurrentAddresses(){
	return _currentAddresses;
}


function googleMapsLink( address1, address2, city, stateProvCd ){

	var baseUrl = "http://www.google.com/maps?q=";
	
	var link = address1 ? (address1 + " ") : "";
    link += address2 ? (address2 + " ") : "";
    link += city ? (city + " ") : "";
    link += stateProvCd ? (stateProvCd) : "";
    
    link = escape(link);
    
    return baseUrl + link;

}

function verificationMsgGetMouseX( event )
{
    if ( !event ){
        event = window.event;
    }
      
    if ( event.pageX ){
        return event.pageX;
    }
    else if ( event.clientX ){
        return event.clientX + ( document.documentElement.scrollLeft ?  document.documentElement.scrollLeft : document.body.scrollLeft );
    }
    else{
        return 0;
    }
}
 
function verificationMsgGetMouseY( event ) 
{
    if ( !event ) {
        event = window.event;
    }
      
    if ( event.pageY ){
        return event.pageY;
    }
    else if ( event.clientY ){
        return event.clientY + ( document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop );
    }
    else
    {
        return 0;
    }
}

function verificationMsgDivFollow( event )
{
    if ( document.getElementById ){                  
        var div = document.getElementById( _verificationMsgElementID );
        var iframe = document.getElementById( _verificationMsgIframeElementID );
        
        if ( div != null ){
            var style = div.style; 
            style.left = ( parseInt(verificationMsgGetMouseX(event)) - $(div).offsetParent().offset().left + _xOffset ) + 'px';
            style.top = ( parseInt(verificationMsgGetMouseY(event)) - $(div).offsetParent().offset().top + _yOffset ) + 'px'; 
            style.visibility = 'visible';
            if ( iframe != null && _ie6orLower ) {
            	var iframeStyle = iframe.style;
            	iframeStyle.top = style.top;
            	iframeStyle.left = style.left;
            	iframeStyle.height = div.offsetHeight;
            	iframeStyle.width = div.offsetWidth;
            	// give it the same zindex as the div. Since the div comes
				// latter in the doc it will be on top
            	iframeStyle.zindex = style.zindex;
            	iframeStyle.visibility = 'visible';
            }
        }
    }
}
 
function verificationMsgDivShow( elementID, iframeId, msg ){

	if ( document.getElementById ){     
        _verificationMsgElementID = elementID;
        _verificationMsgIframeElementID = iframeId;
        document.getElementById(elementID).innerHTML  = "<table class='ui-dialog-content ui-widget-content'><tr><td><img src='img/escalated-exclamation.gif'></td><td class='error'>" + msg + "</td></tr></table>";
// var iframeStyle = document.getElementById( _verificationMsgIframeElementID
// ).style;
// iframeStyle.visibility = 'visible';
        document.onmousemove = verificationMsgDivFollow;
    }
    
}
 
function verificationMsgDivHide( elementID, iframeId ){

    if ( document.getElementById ){     
        _verificationMsgElementID = elementID;
        _verificationMsgIframeElementID = iframeId;
        var divStyle = document.getElementById( _verificationMsgElementID ).style;
        divStyle.visibility = 'hidden';
        var iframeStyle = document.getElementById( _verificationMsgIframeElementID ).style;
        iframeStyle.visibility = 'hidden';
        document.onmousemove = '';
    }
}


// Detect IE6 or lower
var _ie6orLower = false;
if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){ // test for MSIE x.x;
	var ieversion=new Number(RegExp.$1) // capture x.x portion and store as a
										// number
	if (ieversion < 7) {
		_ie6orLower = true;
	}
}
// alert(_ie6orLower);

// Creates a Combined control group to accommodate both parsed and unparsed
// address
function createCombinedControlGroup(rootName,separator){

	// Default the separator to . if it is not set
	if(separator==null || separator=='undefined'){
		separator='.';
	}

	var grp = new AddressControlGroup();
	addCommonAddrControls(grp, rootName, separator);
	addParsedAddrControls(grp, rootName, separator);
	addUnparsedAddrControls(grp, rootName, separator);
	grp.AddressType = "Combined";
	grp.initialize();
	
	return grp;

}

// Creates a parsed control group following the pattern of
// rootName.PrimaryNumber, rootName.PreDirectiona, etc...
function createParsedControlGroup(rootName, separator){

	// Default the separator to . if it is not set
	if(separator==null || separator=='undefined'){
		separator='.';
	}
	var grp = new AddressControlGroup();
	addCommonAddrControls(grp, rootName, separator);
	addParsedAddrControls(grp, rootName, separator);
	grp.AddressType = "Parsed";
	grp.initialize();
	return grp;

}

// Creates a parsed control group following the pattern of
// rootName.PrimaryNumber, rootName.PreDirectiona, etc...
function createUnparsedControlGroup(rootName, separator){

	// Default the separator to . if it is not set
	if(separator==null || separator=='undefined'){
		separator='.';
	}
	var grp = new AddressControlGroup();
	addCommonAddrControls(grp, rootName, separator);
	addUnparsedAddrControls(grp, rootName, separator);
	grp.AddressType = "Unparsed";
	grp.initialize();
	return grp;
}


function addCommonAddrControls(grp, rootName, separator){

	grp.CityControlId = rootName+separator+"City";
	grp.StateProvCdControlId = rootName+separator+"StateProvCd";
	grp.CountyControlId = rootName+separator+"County";
	grp.PostalCodeControlId = rootName+separator+"PostalCode";
	grp.RegionCdControlId = rootName+separator+"RegionCd";
	grp.RegionISOCdControlId = rootName+separator+"RegionISOCd";
	grp.LatitudeControlId = rootName+separator+"Latitude";
	grp.LongitudeControlId = rootName+separator+"Longitude";
	grp.VerifyImgId = rootName+separator+"addrVerifyImg";
	grp.StatusImgId = rootName+separator+"verifyStatusImg";
	grp.VerificationHashControlId = rootName+separator+"VerificationHash";
	grp.VerificationMsgControlId = rootName+separator+"VerificationMsg";
	grp.DPVControlId = rootName+separator+"DPV";
	grp.DPVDescControlId = rootName+separator+"DPVDesc";
	grp.DPVNotesControlId = rootName+separator+"DPVNotes";	
	grp.DPVNotesDescControlId = rootName+separator+"DPVNotesDesc";
	grp.BarcodeDigitsControlId = rootName+separator+"BarcodeDigits";
	grp.CarrierRouteControlId = rootName+separator+"CarrierRoute";
	grp.CongressCodeControlId = rootName+separator+"CongressCode";	
	grp.CountyCodeControlId = rootName+separator+"CountyCode";	

	
}

function addParsedAddrControls(grp, rootName, separator){
	
	grp.StreetNumControlId = rootName+separator+"PrimaryNumber";
	grp.StreetDirControlId = rootName+separator+"PreDirectional";
	grp.StreetNameControlId = rootName+separator+"StreetName";
	grp.StreetTypeControlId = rootName+separator+"Suffix";
	grp.StreetPostDirControlId = rootName+separator+"PostDirectional";
	grp.UnitTypeControlId = rootName+separator+"SecondaryDesignator";
	grp.UnitControlId = rootName+separator+"SecondaryNumber";
	
}

function addUnparsedAddrControls(grp, rootName, separator){
	
	grp.Address1ControlId = rootName+separator+"Addr1";
	grp.Address2ControlId = rootName+separator+"Addr2";
	
}

function toUpperCase(element){
	var start = element.selectionStart;
	var end = element.selectionEnd;
	element.value = element.value.toUpperCase();
	/**Move cursor back to where it was originally positioned*/
	element.selectionStart = start
	element.selectionEnd = end; 
}


// Concatenate the Separated Lookup Address and Copy to Standard Address
function copyLookupAddressToAddress(lookupAddrName, addrName, separator) {
	
	// Pass 'true' to build address1 and address1 in the target address
	copyAddress(lookupAddrName, addrName, separator, true);
	
}

// If Blank - Concatenate the Separated Lookup Address and Copy to Standard
// Address
function copyLookupAddressToAddressIfBlank(lookupAddrName, addrName) {
	fieldValue = stripTrailingSpaces(stripLeadingSpaces(document.getElementById(addrName+".Addr1").value));
	if( fieldValue == "") {
		copyLookupAddressToAddress(lookupAddrName, addrName);
	}
}

// Deprecated!! Just use copyaddress
function copyAddressAll(from, to, separator, buildUnparsedFieldsFromParsed) {
	copyAddress(from, to, separator, buildUnparsedFieldsFromParsed)
}

// Copy address fields from an address to another address.
function copyAddress(from, to, separator, buildUnparsedFieldsFromParsed, skipCheckForCountry) {
	if(skipCheckForCountry === undefined) {
		skipCheckForCountry = false;
	}
	if(!separator){
		separator = ".";
	}
	
	if(!skipCheckForCountry) {
		//check if countries are different. if so change the country on the 'to' address and ajax to change it's fields
		var fromCountry = document.getElementById(from+separator+"RegionCd");
		if (fromCountry != null) {
			checkForCountryDifferenceAndSwitch(from, to, separator, buildUnparsedFieldsFromParsed);
		}
	}
	
	for(var i in _addrFields){
		copyAddressField(from, to, _addrFields[i], separator);
	}
	
	// Build Addr1 and Addr2 from a parsed address to an unparsed address
	if(buildUnparsedFieldsFromParsed && document.getElementById(from+separator+"PrimaryNumber")){
		
		var number = stripTrailingSpaces(stripLeadingSpaces(document.getElementById(from+separator+"PrimaryNumber").value)); 
		var directional = stripTrailingSpaces(stripLeadingSpaces(document.getElementById(from+separator+"PreDirectional").value)); 
		var street = stripTrailingSpaces(stripLeadingSpaces(document.getElementById(from+separator+"StreetName").value)); 
		var suffix = stripTrailingSpaces(selectListText(from+separator+"Suffix"));
		var designator = stripTrailingSpaces(selectListText(from+separator+"SecondaryDesignator")); 
		var unit = stripTrailingSpaces(stripLeadingSpaces(document.getElementById(from+separator+"SecondaryNumber").value)); 
		var postDir = "";
		if( document.getElementById(from+separator+"PostDirectional") ) {
			postDir = stripTrailingSpaces(stripLeadingSpaces(document.getElementById(from+separator+"PostDirectional").value));
		} 
			
		var address1 = number;
		if( directional != "") {
			address1 = address1 + " " + directional;
		}
		var address1 = address1 + " " + street;				
		if( suffix != "" ) {
			address1 = address1 + " " + suffix;
		}
		
		if( postDir != "" ) {
			address1 = address1 + " " + postDir;
		}
		
		var address2 = "";
		if( designator != "" || unit != "" ) {
			address2 = designator;
			address2 = stripTrailingSpaces(address2) + " " + unit;
		}
					
		document.getElementById(to+separator+"Addr1").value = address1;
		document.getElementById(to+separator+"Addr2").value = address2;
				
	}
	
}

function copyAddressIfBlank(from, to, separator) {
	
	if(!separator){
		separator = ".";
	}
	
	if (document.getElementById(to+separator+"Addr1").value == "" &&
	    document.getElementById(to+separator+"Addr2").value == "" &&
	    document.getElementById(to+separator+"City").value == "" &&
	    document.getElementById(to+separator+"StateProvCd").value == "" &&
	    document.getElementById(to+separator+"PostalCode").value == "") {
	    copyAddress(from, to, separator);
	}
}

function copyAddressField(from, to, fieldName, separator){
	
	if(!separator){
		separator = ".";
	}
	
	if(document.getElementById(from+separator+fieldName) && document.getElementById(to+separator+fieldName)){
		var value = stripTrailingSpaces(stripLeadingSpaces(document.getElementById(from+separator+fieldName).value));
		document.getElementById(to+separator+fieldName).value = value;
	}
		
}

/*
 * Runs ajax replace on address if countries are different
 */
function checkForCountryDifferenceAndSwitch(from, to, separator, buildUnparsedFieldsFromParsed) {
	var fromCountry = document.getElementById(from+separator+"RegionCd").value;
	var toCountry = document.getElementById(to+separator+"RegionCd").value;
	if (fromCountry != toCountry) {
		//change to 'toCountry' value to match from
		var index = document.getElementById(from+separator+"RegionCd").selectedIndex;
		var toRegion = document.getElementById(to+separator+"RegionCd");
		toRegion.selectedIndex = index;
		var arguments = document.getElementById(to+"RegionCdArgs").value;
		var argSplit = arguments.split(", ");
		for(var i=0; i<argSplit.length;i++) {
			argSplit[i] = argSplit[i].replace('\'','');//replace left and right single quotes from string (since they're now surrounded by ")
			argSplit[i] = argSplit[i].replace('\'','');
		}
		var code = document.getElementById(from+separator+"RegionISOCd").value;
		ajaxCMMXForm(argSplit[0]+"ajaxReplaceAddress","COAddressReplace", "", "", "", "RegionISOCd="+code+"&AddrAlias="+argSplit[0]+"&TemplateName="+argSplit[2]+argSplit[1]).done(function() {
			//copy the address again after reloading the template (so all the fields are displayed correctly)
			copyAddress(from, to, separator, buildUnparsedFieldsFromParsed, true)
			});

	}
}

function ajaxReplaceAddress(addrAlias) {
	var fieldNamePrefix = "";
	if (addrAlias == "") {
		fieldNamePrefix = "";
	} else {
		fieldNamePrefix = addrAlias + ".";
	}
	//get ajax args
	var arguments = document.getElementById(addrAlias+"RegionCdArgs").value;
	var argSplit = arguments.split(", ");
	for(var i=0; i<argSplit.length;i++) {
		argSplit[i] = argSplit[i].replace('\'','');//replace left and right single quotes from string (since they're now surrounded by ")
		argSplit[i] = argSplit[i].replace('\'','');
	}
	
	//copy value to RegionISOCd hidden select field
	var from = fieldNamePrefix + "RegionCd";
	var to = fieldNamePrefix + "RegionISOCd";
	var index = document.getElementById(from).selectedIndex;
	var toElement = document.getElementById(to);
	toElement.selectedIndex = index;
	
	//ajax replace
	var codeField = fieldNamePrefix + "RegionISOCd";
	var code = document.getElementById(codeField).value;
	ajaxCMMXForm(argSplit[0]+"ajaxReplaceAddress","COAddressReplace", "", "", "", "RegionISOCd="+code+"&AddrAlias="+argSplit[0]+"&TemplateName="+argSplit[2]+argSplit[1]).done(function() {
		// refocus on RegionCd field
		var input = document.getElementById(fieldNamePrefix+"RegionCd");
		input.focus();
	});
}

var addressAutocompleteInputs = {};
var possibleComponents = {
        street_number: 'short_name',
        route: 'short_name',
        locality: 'long_name',
        post_town: 'long_name',
        postal_town: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'short_name',
        postal_code: 'short_name',
        sublocality_level_1: 'long_name',
        sublocality_level_2: 'long_name'
      };
var componentMap = {
		country: 'RegionCd',
		postal_code: 'PostalCode',
		administrative_area_level_1: 'StateProvCd',
		locality: 'City',
		post_town: 'City',
		postal_town: 'City',
		route: 'Addr1',
		street_number: 'Addr1',
		sublocality_level_1: 'Addr1',
        sublocality_level_2: 'Addr2'
};

function initAutocomplete(addrAlias) {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    var autocomplete = new google.maps.places.Autocomplete(document.getElementById(addrAlias+'.Addr1'),{types: ['address']});

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocomplete.addListener('place_changed', function() {
    	fillInAddress(addrAlias);
    });
    
    //add it to array(declared in address.js)
    addressAutocompleteInputs[addrAlias] = autocomplete;
}

function fillInAddress(addrAlias) {
    // Get the place details from the autocomplete object.
    var place = addressAutocompleteInputs[addrAlias].getPlace();
    
    var fieldNamePrefix = "";
	if (addrAlias == "") {
		fieldNamePrefix = "";
	} else {
		fieldNamePrefix = addrAlias + ".";
	}
	
    for (var component in possibleComponents) {
    	var field = componentMap[component];
    	if(component != 'country') {
    		document.getElementById(addrAlias + '.' + field).value = '';
    	}
    }

    //first loop through and check for country, compare it to current and switch if necessary
    for (var i = 0; i < place.address_components.length; i++) {
      var addressType = place.address_components[i].types[0];
      if (addressType==='country') {
        var val = place.address_components[i][possibleComponents[addressType]];
        var currentCountry = document.getElementById(fieldNamePrefix + 'RegionISOCd').value;
        if (val != currentCountry) {
        	ajaxReplaceGooglePlace(addrAlias, place, val);
        } else {
        	//copy everything but country
        	copyPlaceToAddress(place, addrAlias, val);
        }
      }
    }
    //remove address verification checkmark if hashes no longer match
    if(!window[addrAlias].addressHashMatches(document.getElementById(addrAlias+'.VerificationHash').value)) {
    	window[addrAlias].addressEdited();
    }
  }

//Ajax function for replacing address with google place, similar to ajaxReplaceAddress, but tailored for google places api functionality
function ajaxReplaceGooglePlace(addrAlias, place, selectedCountry) {
	var fieldNamePrefix = "";
	if (addrAlias == "") {
		fieldNamePrefix = "";
	} else {
		fieldNamePrefix = addrAlias + ".";
	}
	
	if(document.getElementById(fieldNamePrefix + 'RegionISOCd').value != selectedCountry) {
		//change current country selection to new country
		var selectList = document.getElementById(fieldNamePrefix + 'RegionISOCd');
		for(var i=0; i<selectList.options.length; i++) {
			if(selectList.options[i].value === selectedCountry) {
				selectList.selectedIndex = i;
				break;
			}
		}
	}
	
	//get ajax args
	var arguments = document.getElementById(addrAlias+"RegionCdArgs").value;
	var argSplit = arguments.split(", ");
	for(var i=0; i<argSplit.length;i++) {
		argSplit[i] = argSplit[i].replace('\'','');//replace left and right single quotes from string (since they're now surrounded by ")
		argSplit[i] = argSplit[i].replace('\'','');
	}
	
	//copy value to RegionISOCd hidden select field
	var from = fieldNamePrefix + "RegionISOCd";
	var to = fieldNamePrefix + "RegionCd";
	var index = document.getElementById(from).selectedIndex;
	var toElement = document.getElementById(to);
	toElement.selectedIndex = index;
	
	//ajax replace
	var codeField = fieldNamePrefix + "RegionISOCd";
	var code = document.getElementById(codeField).value;
	ajaxCMMXForm(argSplit[0]+"ajaxReplaceAddress","COAddressReplace", "", "", "", "RegionISOCd="+code+"&AddrAlias="+argSplit[0]+"&TemplateName="+argSplit[2]+argSplit[1]).done(function() {
		//replace fields for google place
		copyPlaceToAddress(place, addrAlias, selectedCountry);
		
		// refocus on RegionCd field
		var input = document.getElementById(fieldNamePrefix+"RegionCd");
		input.focus();
	});
}

//Copy the google place result to the designated address alias (without copying country)
function copyPlaceToAddress(place, addrAlias, selectedCountry) {
	//handle addr1 after since we need to put two fields together and the order of the place result is not guaranteed
	var streetNumber = "";
	var route = "";
	
	for (var i = 0; i < place.address_components.length; i++) {
		var addressType = place.address_components[i].types[0];
		if (addressType != 'country') {
			var val = place.address_components[i][possibleComponents[addressType]];
			if (!val) {
				continue;//value is undefined for this address type
			}
			var field = componentMap[addressType];
			
			//if field is hidden, continue
			var element = document.getElementById(addrAlias + '.' + field);
			if (element.offsetParent === null) {
				continue;
			}
			
			//set addr1 variables to handle after
			if(field === 'Addr1' && addressType != 'sublocality_level_1') {
				if(addressType === 'route') {
					route = val;
				} else {
					streetNumber = val;
				}
				continue;
			} else if (field === 'StateProvCd') {
				//if it's a select list, try to select the element
				var tagType = document.getElementById(addrAlias + '.' + field).tagName;
				if (tagType === 'SELECT') {
					//Select the new value if possible
					var shortName = place.address_components[i]['short_name'];
					var longName = place.address_components[i]['long_name'];
					
					var stateOptions = document.getElementById(addrAlias + '.' + field).options;
					for(var j=0; j<stateOptions.length; j++) {
						if (stateOptions[j].value === shortName || stateOptions[j].value === longName || stateOptions[j].label === shortName || stateOptions[j].label === longName) {
							document.getElementById(addrAlias + '.' + field).selectedIndex = j;
							break;
						}
					}
					
					//if state not in list try administrative area level 2
					if (document.getElementById(addrAlias + '.' + field).value === '') {
						//try admin area level 2
						var level2Short = "";
						var level2Long = "";
						for (var j = 0; j < place.address_components.length; j++) {
							if (place.address_components[j].types[0] == "administrative_area_level_2") {
								level2Short = place.address_components[j]['short_name'];
								level2Long = place.address_components[j]['long_name'];
							}
						}
						for(var j=0; j<stateOptions.length; j++) {
							if (stateOptions[j].value === level2Short || stateOptions[j].value === level2Long 
									|| stateOptions[j].label === level2Short || stateOptions[j].label === level2Long) {
								document.getElementById(addrAlias + '.' + field).selectedIndex = j;
								break;
							}
						}
					}
					
					//if state not in list try getting english version
					if (document.getElementById(addrAlias + '.' + field).value === '') {
						//try english version
						updateStateProvCdWithEnglishGooglePlace(place, addrAlias, selectedCountry);
					}
					continue;
				}
			} 
	        document.getElementById(addrAlias + '.' + field).value = val;
		}
	}
	
	//try using "name" field in place as address 1 first 
	if (place.name != "") {
		document.getElementById(addrAlias + '.Addr1').value = place.name;
	} else if (streetNumber != "" || route != "") {//if street number and route exist, use that for the addr1
		var formattedAddress = place.formatted_address.toLowerCase();
		var addr1 = "";
		//determine which part to put first in addr1
		if (formattedAddress.startsWith(route.toLowerCase())) {
			//route first
			var addr1 = route + " " + streetNumber;
		} else {
			//street number first
			var addr1 = streetNumber + " " + route;
		}
		addr1 = addr1.trim();
		document.getElementById(addrAlias + '.Addr1').value = addr1;
	}
}

function updateStateProvCdWithEnglishGooglePlace(place, addrAlias, selectedCountry) {
	//Call service to get english value for StateProvCd Select list if needed
	ajaxCMMXFormXml("COGooglePlacesAddress", null, null, null, "GooglePlaceId="+place.place_id+"&RegionISOCd="+selectedCountry, function(data) {
		//Gets each param from additional params (should only be necessary values since service clears out other params)
		$(data).find("AdditionalParams").children().each(function(){	
			//Get values and copy to the fields
			var addressFieldId = $(this).attr("Name");
			var addressFieldValue = $(this).attr("Value");
			//Handle state and region code separately because they are select lists (state sometimes, region code always)
			if (addressFieldId === 'StateProvCd') {
				//if it's a select list, try to select the element
				var tagType = document.getElementById(addrAlias + '.' + addressFieldId).tagName;
				if (tagType === 'SELECT') {
					//Select the new value if possible
					var stateOptions = document.getElementById(addrAlias + '.' + addressFieldId).options;
					for(var j=0; j<stateOptions.length; j++) {
						if (stateOptions[j].value === addressFieldValue) {
							document.getElementById(addrAlias + '.' + addressFieldId).selectedIndex = j;
							break;
						}
					}
					
					//if state not in list try administrative area level 2
					if (document.getElementById(addrAlias + '.' + field).value === '') {
						//try admin area level 2
						var level2Short = "";
						var level2Long = "";
						for (var j = 0; j < place.address_components.length; j++) {
							if (place.address_components[j].types[0] == "administrative_area_level_2") {
								level2Short = place.address_components[j]['short_name'];
								level2Long = place.address_components[j]['long_name'];
							}
						}
						for(var j=0; j<stateOptions.length; j++) {
							if (stateOptions[j].value === level2Short || stateOptions[j].value === level2Long 
									|| stateOptions[j].label === level2Short || stateOptions[j].label === level2Long) {
								document.getElementById(addrAlias + '.' + field).selectedIndex = j;
								break;
							}
						}
					}
					
					//if state not in list, set select to 'Select...'
					if (document.getElementById(addrAlias + '.' + addressFieldId).value === '') {
						document.getElementById(addrAlias + '.' + addressFieldId).selectedIndex = 0;
					}
				}
			}
		});	
	});	
}

  // Bias the autocomplete object to the user's geographical location,
  // as supplied by the browser's 'navigator.geolocation' object.
  function geolocate(fieldIDPrefix) {
	  addrAlias = fieldIDPrefix.replace(".","");
	  //TODO: Maybe priorities selected regioncd over users location?
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var geolocation = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        var circle = new google.maps.Circle({
          center: geolocation,
          radius: position.coords.accuracy
        });
        addressAutocompleteInputs[addrAlias].setBounds(circle.getBounds());
      });
    }
  }

//Toggles the required param for all address fields
function toggleAddressFieldRequired(fieldNamePrefix, setRequired) {
	var requiredFieldsListElement = document.getElementById(fieldNamePrefix+"RequiredFields");
	if(requiredFieldsListElement) {
		var requiredFieldsList = requiredFieldsListElement.value.split(",");
		for(i = 0; i < requiredFieldsList.length; i++) {
			if (setRequired) {
				appendStringToValue("METAFIELD__"+fieldNamePrefix+"___"+requiredFieldsList[i]+"__EXT", "Required=True|");	
			} else {
				removeStringFromValue("METAFIELD__"+fieldNamePrefix+"___"+requiredFieldsList[i]+"__EXT", "Required=True|");
			}
		}
	}
}

function clearAddress(addr, separator){
	
	if(!separator){
		separator = ".";
	}
	
	for(var i in _addrFields){
		if(document.getElementById(addr+separator+_addrFields[i])){
			document.getElementById(addr+separator+_addrFields[i]).value = "";
		}	
	}

}

function avlog(str){
	var obj = document.getElementById("_console");
	if(obj){	
		var txt=document.createTextNode(str + "\n");
		obj.appendChild(txt);
		obj.scrollTop = obj.scrollHeight;
	}
}

function XMLtoString(elem){
	
	var serialized;
	
	try {
		// XMLSerializer exists in current Mozilla browsers
		serializer = new XMLSerializer();
		serialized = serializer.serializeToString(elem);
	} 
	catch (e) {
		// Internet Explorer has a different approach to serializing XML
		serialized = elem.xml;
	}
	
	return serialized;
}
