var time = 3000;
var numofitems = 5;
var fieldItems;

// Show a document in an IFRAME
function showTheDocument( docName ) {
	var iframe = document.getElementById("showDoc");
	
	if( iframe )		
		iframe.src = docName;
	
	var el = document.getElementById("showDocDiv");
	
	if( el )
		el.style.display = "inline";
}

// Toggle the visibility of an element
function toggleVisible( elname ) {
	var el = document.getElementById(elname);
	if( el ) {
		if( el.style.display != "none" )
			el.style.display = "none";
		else
			el.style.display = "inline";
	}
}

//menu constructor
function menu(allitems,thisitem,startstate){ 
  callname= "gl"+thisitem;
  divname="subglobal"+thisitem;  
  this.numberofmenuitems = 5;
  this.caller = document.getElementById(callname);
  this.thediv = document.getElementById(divname);
  this.thediv.style.visibility = startstate;
}

//menu methods
function ehandler(event,theobj){
  for (var i=1; i<= theobj.numberofmenuitems; i++){
    var shutdiv =eval( "menuitem"+i+".thediv");
    shutdiv.style.visibility="hidden";
  }
  theobj.thediv.style.visibility="visible";
}
				
function closesubnav(event){
  if ((event.clientY <48)||(event.clientY > 107)){
    for (var i=1; i<= numofitems; i++){
      var shutdiv =eval('menuitem'+i+'.thediv');
      shutdiv.style.visibility='hidden';
    }
  }
}

function selectValue( elname, v ) {
	var el = document.getElementById(elname);
	if( el ) {
		el.value = v;
	}
}

// Functions related to CSS popups

var elSelect;
var elPopup;
	
function selectValue( v ) {
	if( elSelect ) {
		elSelect.value = v;
	}
	if( elPopup ) {
		elPopup.style.visibility = "hidden";
		elPopup = null;
	}
}
	
function setSelect( pname, elname, left, top, width, height ) {
	if( elPopup ) {
		elPopup.style.visibility = "hidden";
		elPopup = null;
	}
	elSelect = document.getElementById(elname);
	elPopup = document.getElementById(pname);
	if( left )
		elPopup.style.left = left;
	if( top )
		elPopup.style.top = top;
	if( width )
		elPopup.style.width = width;
	if( height )
		elPopup.style.height = height;
	elPopup.style.visibility = "visible";
}

function getAjaxMethod() {
	if( document.all )
		return _GET;
	return _POST;
}

// Changed to use jQuery but no where is this being called from
function ajaxDocument( id, uid ) {

	// Form backwards compatibility with CMM/2, pass the following fields
	var xform = "<?xml version='1.0' ?><xfdf><fields>";    	
	
	xform = xform + "<SafeCopy>true</SafeCopy>";
	xform = xform + "<rq>WCDocumentSync</rq>";
	xform = xform + "<SystemId></SystemId>";
	xform = xform + "<IdRef></IdRef>";
	xform = xform + "<ParentIdRef></ParentIdRef>";
    xform = xform + "<DocumentId>" + uid + "</DocumentId>";
    xform = xform + "</fields></xfdf>";

	var postInd = true;
	if( document.all )
		postInd = false;
		
	var params = new Object();
	params.rq = "WCDocumentSync";
	params.XForm = xform;
	
	if( uid )
		params.DocumentId = uid;

	var elemToReplace = document.getElementById(id);
	if( postInd ) {
		$.post("innovation", $.param(params), function(data) {
			$( elemToReplace ).html(data);
		});		
	} else {
		$.get("innovation", $.param(params), function(data) {
			$( elemToReplace ).html(data);
		});		
	}		
}

// Changed to use jQuery but no where is this being called from
function ajax( id, url ) {
	var elemToReplace = document.getElementById(id);
	$( elemToReplace ).load(url);
}

// Changed to use jQuery but no where is this being called from
function ajaxWebPath( id, rq, uid ) {
	var elemToReplace = document.getElementById(id);

	if( rq && !uid ) {
		var params = new Object();
		params.rq = rq;
	
		$.get("innovation", $.param(params), function(data) {
			$( elemToReplace ).html(data);
		});		
	} 
	
	if ( uid ) {
		var params = new Object();
		params.SystemId = uid;
	
		$.post("innovation", $.param(params), function(data) {
			$( elemToReplace ).html(data);
		});		
	}
	
}


function ajaxCMMXFormXml( serviceRq, systemId, parentIdRef, idRef, urlParams, onComplete ) {

	if (urlParams == null || urlParams == "") {
		urlParams = "ResponseAsXml=Yes";
	} else {
		urlParams += "&ResponseAsXml=Yes";
	}
	
	// Get all the additional optional arguments (beyond the named ones in the signature) from the arguments aray.
	// Because arguments is not a "real" array and altering it actually alters the named parameters too, copy it first (minus 1st 5 elements) with the Array.slice method.
	var additionalArgs = [].slice.call(arguments, 6);
	
	var xform = setXFormFields(serviceRq, systemId, parentIdRef, idRef, ["","","","","",urlParams].concat(additionalArgs));
	if (xform == null)
		return;
    
	var params = new Object();
	params.rq = serviceRq;
	params.XForm = xform;
	
	window.seleniumPageLoadOutstanding++;
	
	var deferred = $.post("innovation", $.param(params), function(data) {
		if(onComplete){
			onComplete(data);
		}
		window.seleniumPageLoadOutstanding--;
	}, "xml");	
	return deferred;
}


function ajaxCMMXForm( id, serviceRq, systemId, parentIdRef, idRef, urlParams) {

	var xform = setXFormFields(serviceRq, systemId, parentIdRef, idRef, arguments);
	if (xform == null)
		return;
    
	var params = new Object();
	params.rq = serviceRq;
	params.XForm = xform;
		
	window.seleniumPageLoadOutstanding++;
	
	var elemToReplace = document.getElementById(id);
	var deferred = $.post("innovation", $.param(params), function(data) {
		$( elemToReplace ).html(data);
		window.seleniumPageLoadOutstanding--;
	});	
	return deferred;
}

function ajaxCMMXFormShowBusy( id, serviceRq, systemId, parentIdRef, idRef, urlParams ) {
    showBusyBox();
        
	var xform = setXFormFields(serviceRq, systemId, parentIdRef, idRef, arguments);
	if (xform == null) {
		hideBusyBox();
		return;
	}
    
	var params = new Object();
	params.rq = serviceRq;
	params.XForm = xform;
		
	window.seleniumPageLoadOutstanding++;
	
	var elemToReplace = document.getElementById(id);
	var deferred = $.post("innovation", $.param(params), function(data) {
		hideBusyBox();
		$( elemToReplace ).html(data);
		window.seleniumPageLoadOutstanding--;
	});	
	return deferred;
}

function setXFormField (field) {
    var n = field.name;
    var xform = "";

    // Error if trying to reset a CMM parameter
    if( ";rq;SystemId;IdRef;ParentIdRef;SafeCopy;".search(";"+n+";") != -1 ) {
        alert("XForms cannot have an element called "+n);
        return;
    }

    if( n != "" ) {
        if( (n != "XForm") && !(n.substr(0, 21) == "HiddenValueMetaField.") ) {
        	
        	var v = "";
        	// If there is a hidden field associate with this field, use its value instead
        	if(document.getElementById("HiddenValueMetaField." + n)){
        		if(document.getElementById("HiddenValueMetaField." + n).value != "__UNSET9999__"){
            		v = escapeXml(document.getElementById("HiddenValueMetaField." + n).value);			
        		}
        		else{
        			// If the hidden value metafield is blank, then the user didn't change the value, so don't
        			// send up a value
        			return null;
        		}
        	}
        	
        	if (field.type == "hidden" && field.disabled) {
        		// Don't include hidden disabled fields (see HiddenNoPost FieldTemplate
        		return null;
        	}
        	
        	if(v.length == 0){
        		v = escapeXml(field.value);
        	}
        	
            if( field.type == "radio" ) {
				if(isFirstRadioButtonInGroup(field)){
                	v = getRadioButtonGroupValue( field );
                	xform = xform + "<" + n + ">" + v + "</" + n + ">";
                }
            }
            else{
	            if( field.type == "checkbox" ) {
    	            if( !field.checked )
        	            v = "";
            	}    
            	if( field.type == "select-multiple" ) {
					// loop through selected
				    var selectedArray = new Array();
				    var count = 0;
				    for( var j=0; j<field.options.length; j++) {
				    	if( field.options[j].selected ) {
				        	selectedArray[count] = field.options[j].value;
				            count++;
				        }
				    }
				    v = selectedArray;    
				}
	            xform = xform + "<" + n + ">" + v + "</" + n + ">";
			}
        }
    }
    else {
    	// Ignore dialog close buttons, jQuery UI 1.12.0 now use the HTML <button> instead of anchors <a> as before. Therefore we need to ignore specifically these buttons.
    	if (field.nodeName == "BUTTON" && $(field).hasClass("ui-dialog-titlebar-close")) {
    		return;
    	}
        alert("Form cannot be submitted because\na form control was found without a name." + field.nodeName);
        return;
    }
    return xform;
}

function setXFormFields(serviceRq, systemId, parentIdRef, idRef, paramList ) {
    // Form backwards compatibility with CMM/2, pass the following fields
    var xform = "<?xml version='1.0' ?><xfdf><fields>";    	

    xform = xform + "<SafeCopy>true</SafeCopy>";
    xform = xform + "<rq>" + (serviceRq ? serviceRq : "") + "</rq>";
    xform = xform + "<SystemId>" + (systemId ? systemId : "") + "</SystemId>";
    xform = xform + "<ParentIdRef>" + (parentIdRef ? parentIdRef : "") + "</ParentIdRef>";
    xform = xform + "<IdRef>" + (idRef ? idRef : "") + "</IdRef>";

    // Always pass the security id
    var securityId = document.getElementById("SecurityId");
    xform = xform + "<SecurityId>" + (securityId.value ? securityId.value : "") + "</SecurityId>";
    
    // Always pass the ConversationId if it exists. This is needed to be able to correctly read the conversation variables on an Ajax request.
    var conversationId = document.getElementById("ConversationId");
    if (conversationId){
    	xFormField = setXFormField(conversationId);
        if (xFormField != null) {
            xform = xform + xFormField;
        }
    }

    if( paramList[5] ) {
        try {
            xform = xform + urlXForm(paramList[5]);
        }
        catch(ie) {
            alert(ie.message);
            return; // returns null
        }
    }
    
    // Determine if all params should be sent back or just a select few.
    if (paramList[6] == "all" ) {
        var elems = document.frmCMM.elements;
        for( i=0; i<elems.length; i++ ) {
            xFormField = setXFormField(elems[i]);
            if (xFormField != null) {
                xform = xform + xFormField;
            }
        }
    } else {
        for (i=6; i<paramList.length; i++ ) {
            var elem = document.getElementById(paramList[i]);
            if (elem ) {
                xFormField = setXFormField(elem);
                if (xFormField != null) {
                    xform = xform + xFormField;
                }
            }
        }
    }
    xform = xform + "</fields></xfdf>";
    return xform;
}

//This function is a commonly used to set the RqUID if it is lower than the new RqUID to solve Duplicate request issue
function replaceCurrentRqUID(reponseParamRqUID){
	if ( parseInt($("#RqUID").val(), 10) < parseInt(reponseParamRqUID) ) {
		//Update RqUID field if it's lower than the new RQUID
		$("#RqUID").val(reponseParamRqUID);
	}
}