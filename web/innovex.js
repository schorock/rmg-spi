//var isIE  = $.browser.msie;
var isIE = false;
var submitDisabled = false;

// Primary service used to submit CMM WebPath II Service Requests
function cmmService( serviceRq, sid, pid, idref  ) {

	if(submitDisabled)
		return false;
	disableForm();
	
    if( serviceRq ){
        document.frmCMM.rq.value = serviceRq;
        }
    if( sid ){
        document.frmCMM.SystemId.value = sid;
    }
    if( pid ) {
        document.frmCMM.ParentIdRef.value = pid;
    }
    if( idref ){
        document.frmCMM.IdRef.value = idref;
        }
    document.frmCMM.submit();
}

// General Exception
function InternalException (message) {
   this.message=message;
   this.name="InternalException";
}


// Tokenizes a URL style parameters string into XForm format
function urlXForm( urlParams ) {
    var params = urlParams.split("&");
    var s = "";
    try {
        for( var i=0; i<params.length; i++ ) {
            var p = params[i].split("=");
            var pValue = escapeXml(p[1])
            s = s + "<" + p[0] + ">" + pValue + "</" + p[0] + ">";
        }
        return s;
    }
    catch(e) {
        alert("JavaScript Error: "+e);
        throw new InternalException("URL parameter format incorrect");
    }
}

// The busyBox innerHTML Code is to Keep the Gif Moving During the Load [IE Bug]
function showBusyBox( data, obj ) {
	try {
		if( isIE ) {
	    	busyBox = document.getElementById("BusyBox1");
	    	if( busyBox ) {
	       		busyBox.innerHTML = "<img id='BusyBoxImg' src='img/progress-bar-animated.gif' height='22px;' width='270px;' />"; 
	       	}
       	}
       	$("#BusyBox1").dialog({ modal: true,
       							beforeClose: function(event, ui) { return false; },
        						resizable: false, 
        						height: 73
							 });		
		$("#BusyBox1").closest(".ui-dialog").find(".ui-dialog-titlebar-close").hide();					 
	} catch(e) {
    	// do nothing on error. This avoids hanging requests if busybox didn't init for some reason
    }	
}

function hideBusyBox( data, obj ) {
	$("#BusyBox1").dialog({ beforeClose: function(event, ui) { return true; } });		
	$("#BusyBox1").dialog("close");	
}

// Function to submit a CMM service via an XML based form parameter
function cmmXForm(  serviceRq, systemId, parentIdRef, idRef, urlParams, showBusy, newWindow ) {
	
	window.seleniumPageLoadOutstanding++;
	
	if( showBusy == 'true') {
        showBusyBox();
    } else if(newWindow==null || !newWindow) {
    	disableForm();
    }

    if( !document.frmCMM.XForm ) {
        alert("CMM XForm Param Not Present");
        return;
    }

    var s = new StringBuilder("<?xml version='1.0' ?><xfdf><fields>");

    // Save the conversation id as we will clear it temporarily to open a new window
    var convo = "";
    if(document.getElementById("ConversationId")){
    	var convo = document.getElementById("ConversationId").value;	
    }
    if(typeof newWindow == 'boolean' && newWindow) {
    	document.getElementById("ConversationId").value = "";
    	document.getElementById("frmCMM").setAttribute("target", "_blank");
    } else if(typeof newWindow == 'string' && newWindow != null) {
    	document.getElementById("frmCMM").setAttribute("target", newWindow);
    }
    
    
    // Always set standard CMM parameters passed by the caller even if the form has the field on it
    s.append("<SafeCopy>true</SafeCopy>");
    s.append("<rq>").append(serviceRq ? serviceRq : "").append("</rq>");
    s.append("<SystemId>").append(systemId ? systemId : "").append("</SystemId>");
    s.append("<ParentIdRef>").append(parentIdRef ? parentIdRef : "").append("</ParentIdRef>");
    s.append("<IdRef>").append(idRef ? idRef : "").append("</IdRef>");

    if( urlParams ) {
        try {
            s.append(urlXForm(urlParams));
        }
        catch(ie) {
            alert(ie.message);
            return;
        }
    }

    var elems = document.frmCMM.elements;

    for( var i=0, elemsLength=elems.length; i<elemsLength; i++ ) {
        var elem = elems[i];
        var n = elem.name;

		//alert(index);
        // Error if trying to reset a CMM parameter
        if( ";rq;SystemId;IdRef;ParentIdRef;SafeCopy;".search(";"+n+";") != -1 ) {
            alert("XForms cannot have an element called "+n);
            return;
        }
        if( n != "" ) {
            if( (n != "XForm") && !(n.substr(0, 21) == "HiddenValueMetaField.") ) {
            	
            	var v = "";
            	// If there is a hidden field associate with this field, use its value instead
            	if(document.getElementById("HiddenValueMetaField." + n)){
            		if(document.getElementById("HiddenValueMetaField." + n).value != "__UNSET9999__"){
                		v = escapeXml(document.getElementById("HiddenValueMetaField." + n).value);			
            		}
            		else{
            			// If the hidden value metafield is blank, then the user didn't change the value, so don't
            			// send up a value
            			continue;
            		}
            	}
            	
            	if (elem.type == "hidden" && elem.disabled) {
            		// Don't include hidden disabled fields (see HiddenNoPost FieldTemplate
            		continue;
            	}
            	
            	if(v.length == 0){
            		v = escapeXml(elem.value);
            	}
            	
            	if( elem.type == "checkbox" ) {
                    if( !elem.checked )
                        v = "";
                }
                if( elem.type == "select-multiple" ) {
					// loop through selected
				    var selectedArray = new Array();
				    var count = 0;
				    for( var j=0; j<elem.options.length; j++) {
				    	if( elem.options[j].selected ) {
				        	selectedArray[count] = elem.options[j].value;
				            count++;
				        }
				    }
				    v = escapeXml(selectedArray.toString());
				}
                
                if( elem.type == "radio" ) {
                	if(isFirstRadioButtonInGroup(elem)){  // Only add once
                		var radioGroupVal = getRadioButtonGroupValue( elem );
                		s.append("<").append(n).append(">").append(escapeXml(radioGroupVal)).append("</").append(n).append(">");
                	}
                } else {
                	// all other types of fields always emit an element, one per field
                	s.append("<").append(n).append(">").append(v).append("</").append(n).append(">");
                }
            }
        }
        else {
        	// Ignore dialog close buttons, jQuery UI 1.12.0 now use the HTML <button> instead of anchors <a> as before. Therefore we need to ignore specifically these buttons.
        	if (elem.nodeName == "BUTTON" && $(elem).hasClass("ui-dialog-titlebar-close")) {
        		continue;
        	}
            alert("Form cannot be submitted because\na form control was found without a name." + elem.nodeName);
            return;
        }
    }

    s.append("</fields></xfdf>");
    
    document.frmCMM.XForm.value = s.toString();
    try {  
    	document.frmCMM.submit();
	} catch ( e ) {
		alert( "Invalid file specified . Please try again." );
	} 
	
	// Restore conversation id and clear target in case these values were temporarily altered by new window functionality
	if(document.getElementById("ConversationId")){
		document.getElementById("ConversationId").value = convo;
	}
	document.getElementById("frmCMM").setAttribute("target", "");	
}


 // Gets the valud for a radio button group 	
 function getRadioButtonGroupValue(radioObj){
 	
 	if(!radioObj)
		return "";
	
	var radioButtonItems = document.getElementsByName(radioObj.name);
	for(var i = 0, radioButtonItemsLength = radioButtonItems.length; i < radioButtonItemsLength; i++) {
		if(radioButtonItems[i].checked) {
			return radioButtonItems[i].value;
		}
	}
	return "";
 	
 }
 
 function isFirstRadioButtonInGroup(radioObj){
 	
 	var radioButtonItems = document.getElementsByName(radioObj.name);
	if(radioButtonItems[0] == radioObj){
		return true;
	}
	return false;
 
 }
 
 

// Highlight form element- Dynamic Drive (www.dynamicdrive.com)
// For full source code, 100's more DHTML scripts, and TOS,
// visit http://www.dynamicdrive.com

var highlightcolor="lightyellow"

var ns6=document.getElementById&&!document.all
var previous=''
var eventobj

//Regular expression to highlight only form elements
var intended=/INPUT|TEXTAREA|SELECT|OPTION/

//Function to check whether element clicked is form element
function checkel(which){
    if (which.style&&intended.test(which.tagName)){
        if (ns6&&eventobj.nodeType==3)
        eventobj=eventobj.parentNode.parentNode
        return true
    }
    else
        return false
}

//Function to highlight form element
function highlight(e){
    // eventobj=ns6? e.target : event.srcElement
    // if (previous!=''){
    //    if (checkel(previous))
    //        previous.style.backgroundColor=''
    //    previous=eventobj
    //    if (checkel(eventobj))
    //        eventobj.style.backgroundColor=highlightcolor
    //}
    //else{
    //    if (checkel(eventobj))
    //        eventobj.style.backgroundColor=highlightcolor
    //    previous=eventobj
    //}
}

// Submit a WebPath I Business Service Request
function submitService( service ) {
	if(submitDisabled)
		return false;
	disableForm();
	
    document.frmCMM.BusinessSvcRq.value = service;
    document.frmCMM.submit();
}

// Submit a WebPath I Business Service Request
function submitServiceCMM( service, container, model, position, systemid, idref ) {
	
	if(submitDisabled)
		return false;
	disableForm();
	
    if( service )
        document.frmCMM.BusinessSvcRq.value = service;
    if( systemid )
        document.frmCMM.SystemId.value = systemid;
    if( position )
        document.frmCMM.Position.value = position;
    if( container )
        document.frmCMM.Container.value = container;
    if( model )
        document.frmCMM.ModelName.value = model;
    if( idref )
        document.frmCMM.IdRef.value = idref;
    document.frmCMM.submit();
}

// Deprecated
// Submit a WebPath I Business Service Request
function submitDefaultService() {
	if(submitDisabled)
		return false;
	disableForm();
    document.frmCMM.submit();
}

// Popup a window that selects a value and pokes it back into an input
// field in the calling window
function selectWindow(url,w,h,tb,stb,l,mb,sb,rs,x,y) {
    var t=(document.layers)? ',screenX='+x+',screenY='+y: ',left='+x+',top='+y; //A LITTLE CROSS-BROWSER CODE FOR WINDOW POSITIONING
    tb=(tb)?'yes':'no'; stb=(stb)?'yes':'no'; l=(l)?'yes':'no'; mb=(mb)?'yes':'no'; sb=(sb)?'yes':'no'; rs=(rs)?'yes':'no';
    var x=window.open('/lookup/lookup_'+url+'.html', 'newWin'+new Date().getTime(), 'scrollbars=yes'+',width='+w+',height='+h+',toolbar='+tb+',status='+stb+',menubar='+mb+',links='+l+',resizable='+rs+t);
    x.focus();
}

// Generic, easy to use function to open a window
function openWindow(url,w,h,tb,stb,l,mb,sb,rs,x,y) {
    var t=(document.layers)? ',screenX='+x+',screenY='+y: ',left='+x+',top='+y; //A LITTLE CROSS-BROWSER CODE FOR WINDOW POSITIONING
    tb=(tb)?'yes':'no'; stb=(stb)?'yes':'no'; l=(l)?'yes':'no'; mb=(mb)?'yes':'no'; sb=(sb)?'yes':'no'; rs=(rs)?'yes':'no';
    var x=window.open(url, 'newWin'+new Date().getTime(), 'scrollbars='+sb+',width='+w+',height='+h+',toolbar='+tb+',status='+stb+',menubar='+mb+',links='+l+',resizable='+rs+t);
    x.focus();
}

// Generic function to open a "help" window as a popup
function helpWindow( text ) {
    msg=window.open("","Help","height=200,width=200,left=80,top=80");
    msg.document.write("<html><title>Help Window</title>");
    msg.document.write("<body bgcolor='white' onblur=window.close()>");
    msg.document.write("<center>" + text + "</center>");
    msg.document.write("</body></html><p>");

    // If you just want to open an existing HTML page in the 
    // new window, you can replace win()'s coding above with:
    // window.open("page.html","","height=200,width=200,left=80,top=80");
}

// Changes cursor style
function setPointer(theRow, thePointerColor) { 
    if (typeof(theRow.style) == 'undefined' || typeof(theRow.cells) == 'undefined') { 
        return false; 
    } 
    var row_cells_cnt = theRow.cells.length; 
    for (var c = 0; c < row_cells_cnt; c++) { 
        theRow.cells[c].bgColor = thePointerColor; 
    } 
    return true; 
}

// Removes all leading spaces from myString
function stripLeadingSpaces(myString) {
    while(myString.substring(0,1)==" "){
        myString = myString.substring(1, (myString.length));
    }
    return(myString);
}

// Removes all trailing spaces from myString
function stripTrailingSpaces(myString) {
    while( myString.substring((myString.length-1),myString.length) ==" "){                
        myString = myString.substring(0, (myString.length-1));        
    }    
    return(myString);
}

// Toggles a checkbox's value
function toggleCheckbox(checkboxName){
    
    hiddenName = checkboxName.substring(9, (checkboxName.length));            
    
    if(document.getElementById(checkboxName).checked)
        document.getElementById(hiddenName).value = "Yes";
    else
        document.getElementById(hiddenName).value = "No";            
}


// Deprecated
function claimPlaceOfLossChange(selectObj){
    if(selectObj.options[selectObj.selectedIndex].text == "Insured Address"){
        //Default location address to Claim->PolicyInfo->Insured1 address    
        document.getElementById("Addr.Addr1").value = document.getElementById("Insured1.Addr1").value;
        document.getElementById("Addr.Addr2").value = document.getElementById("Insured1.Addr2").value;
        document.getElementById("Addr.City").value = document.getElementById("Insured1.City").value;
        document.getElementById("Addr.StateProvCd").value = document.getElementById("Insured1.StateProvCd").value;
        document.getElementById("Addr.PostalCode").value = document.getElementById("Insured1.PostalCode").value;
    }

    changeLinkedField(selectObj)
}

// Deprecated
function changeLinkedField(selectObj){
    //Special work-around code to handle "Diary.TaskTypeCd" select field.    
    if(selectObj.name == "Diary.TaskTypeCd"){
        document.getElementById("Diary.TaskTypeDesc").value = selectObj.options[selectObj.selectedIndex].text;
    }
    else{
        inputName = selectObj.name.substring(0, (selectObj.name.length-2));
        document.getElementById(inputName).value = selectObj.options[selectObj.selectedIndex].text;
    }    
}

// Changes a field's color during client-side "required" checking
function reqF(x){
    theid = document.getElementById(x);
    if (theid.value == '' ){
        theid.style.background = "#6487dc";
        theid.parentNode.previousSibling.style.color = "#FF0033";
        return false;
    }
    else{ 
        theid.style.background = "#FFFFFF"
        theid.parentNode.previousSibling.style.color = "#000000";
        return true;
    }
}

// Changes a field's color during client-side "required" checking to not required
function reqFOff(x){
    theid = document.getElementById(x);
    theid.style.background = "#FFFFFF"
    theid.parentNode.previousSibling.style.color = "#000000";
    return true;
}

// Generic phone lookup function
function lookupPhone( ac, num ) {
    var areaCode = document.getElementById(ac);
    var number = document.getElementById(num);

    if( !areaCode || !number )
        alert('Missing area code or phone number');
    else {
        var url = "http://www.anywho.com/qry/wp_rl?npa="+areaCode.value+"&amp;telephone="+number.value+"&amp;btnsubmit.x=18&amp;btnsubmit.y=3";
        openWindow(url);
    }
}

// Generic address lookup function
function lookupAddress( a, c, s, z ) {
    var addr = document.getElementById(a);
    var city = document.getElementById(c);
    var state = document.getElementById(s);
    var zip = document.getElementById(z);

    if( !addr || !city || !state || !zip )
        alert('Part of the requested address is missing');
    else {
        var url = "http://www.mapquest.com/maps/map.adp?country=US&amp;addtohistory=" +
                "&amp;address=" + addr.value + 
                "&amp;city=" + city.value + 
                "&amp;state=" + state.value + 
                "&amp;zipcode=" + zip.value + 
                "&amp;homesubmit=Get+Map";
        openWindow(url);
    }
}

// Show and Hide Span Function brcm2003
function spanMizer(theH){
	if(theH.parentNode.nextSibling.style.display != 'none'){
            theH.firstChild.src="img/triright.gif";
            theH.parentNode.nextSibling.style.display = "none";
            }
	else{
            theH.firstChild.src="img/tridown.gif";
            theH.parentNode.nextSibling.style.display="inline";
	}
}
// Show and Hide Next Table Span Function brcm2004
function carTDMizer(thisone){
	if(thisone.parentNode.parentNode.nextSibling.firstChild.firstChild.style.display != 'none'){
            thisone.firstChild.src="img/triright.gif";
            thisone.parentNode.parentNode.nextSibling.firstChild.firstChild.style.display = "none";
            }
	else{
            thisone.firstChild.src="img/tridown.gif";
            thisone.parentNode.parentNode.nextSibling.firstChild.firstChild.style.display="inline";
	}
}

// idCol -- Collapses and expands an element by it's id.
// requires two vars:
// x = id of image to alternate
// y = id of element to collapse/expand.
function idCol(x,y){
    var z = document.getElementById(y);
    var imageId = document.getElementById(x);
    if(z.style.display != 'none'){
        imageId.src="img/triright.gif";
        z.style.display = "none";
    }
    else{
        imageId.src="img/tridown.gif";
        z.style.display = "";
    }
}

// Parse an input field and put it in currency format
function parseelement( thisone ) {
    var prefix=""
    var wd
    if( thisone.value.charAt(0)=="$" )
        return
    wd="w"
    var tempnum=thisone.value
    for( i=0; i<tempnum.length; i++ ) {
        if( tempnum.charAt(i)=="." ){
            wd="d"
            break
        }
    }
    if( wd=="w" )
        thisone.value=prefix+tempnum+".00"
    else {
        if( tempnum.charAt(tempnum.length-2)=="." ) {
            thisone.value=prefix+tempnum+"0"
        }
        else {
            tempnum=Math.round(tempnum*100)/100
            thisone.value=prefix+tempnum
        }
    }
}

// Form input box enabler/disabler for drop down menus 
// *****************************************************
// function optSet is called via an onClick within the <select> tags.
// function optSet requires 3 variables in DOM terms:
// var x is normally "this" .
// var ident is set to the options value that triggers the the box.
// var boxDo is set to the DOM location of the box to be enabled/disabled.
function optSet(x,ident,boxDo){
        var e = document.getElementById(boxDo)
        if( e ) {
            if( x.options.value == ident ) {
		e.disabled = false;
		e.style.background = "#FFFFFF";
            }   
            else {
		e.value = "";
		e.disabled = true;
		e.style.background = "#CCCCCC";
            }   
        }
        else
            alert("Element "+boxDo+" not found");
}


// Form Input, Select Element Enabler/Disabler elEnabler(obj,checkval) and elDisabler(obj,checkval)
// +++++++++++++++++++++++++++++++++++++++++++++++
// More browsers supported, easier use
// Replaces optCheck !
// obj = string id of input element to enable/disable
// val = string value to compare against


function elEnabler(obj,val){
  obj = document.getElementById(obj);
  checkval = window.event.srcElement.value;
  if( val == checkval){
    obj.disabled = false;
    obj.style.background = "#FFFFFF";
  }
  else{
    obj.disabled = true;
    obj.value = "";
    obj.style.background = "#CCCCCC";
  }

}
function elDisabler(obj,val){
  obj = document.getElementById(obj);
  checkval = window.event.srcElement.value;
  if( val == checkval){
    obj.disabled = true;
    obj.value = "";
    obj.style.background = "#CCCCCC";
  }
  else{
    obj.disabled = false;
    obj.style.background = "#FFFFFF";
  }

}




// Form input box enabler/disabler for drop down menus 
// *****************************************************
// function optSet is called via an onClick within the <select> tags.
// function optSet requires 3 variables in DOM terms:
// var x is normally "this" .
// var ident is set to the options value that triggers the the box.
// var boxDo is set to the DOM location of the box to be enabled/disabled.
function optCheck( x,ident,boxDo ){
        var e = document.getElementById(boxDo)
        if( e ) {
            if( x.options.value == ident ) {
		e.disabled = false;
		e.style.background = "#FFFFFF";
            }   
            else {
		e.value="";
                e.diabled = true;
		e.style.background = "#CCCCCC";
            }   
        }
        else
            alert("Element "+boxDo+" not found");
}

//
function optCheckVersa( x,ident,boxDo ){
        var e = document.getElementById(boxDo)
        if( e ) {
            if( x.options.value != ident ) {
		e.disabled = false;
		e.style.background = "#FFFFFF";
            }   
            else {
		e.value="";
                e.disabled = true;
		e.style.background = "#CCCCCC";
            }   
        }
        else
            alert("Element "+boxDo+" not found");
}

// 
function setElVal(x,y){
    var e = document.getElementById(x);
    e.value = y;
}

function setElValIf(x,y,z){
    var sel = window.event.srcElement.options.value;
    var ely = document.getElementById(y);
    if( sel == x){
        ely.value = z;
    }
    else{
        ely.value = "";
    }
}
// Div Floater For Menu
// variable required: whatDiv
// whatDiv defines the id of the Div to float.
function divFloater(whatDiv)
{
	var startX = 0, startY = 0;
	var ns = (navigator.appName.indexOf("Netscape") != -1);
	var d = document;
	var px = document.layers ? "" : "px";
	function ml(id)
	{
		var el=d.getElementById?d.getElementById(id):d.all?d.all[id]:d.layers[id];
		if(d.layers)el.style=el;
		el.sP=function(x,y){this.style.left=x+px;this.style.top=y+px;};
		el.x = startX; el.y = startY;
		return el;
	}
	window.stayTopLeft=function()
	{
		var pY = ns ? pageYOffset : document.documentElement && document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop;
		ftlObj.y += (pY + startY - ftlObj.y)/8;
		ftlObj.sP(ftlObj.x, ftlObj.y);
		setTimeout("stayTopLeft()",0);
	}
	ftlObj = ml(whatDiv);
	stayTopLeft();
}

// Check's a dates value against the preferred format of mm/dd/ccyy
function check_date(field){
    formatDate(field);
}

// Mask Input Function.
// Best if used with onkeyup.
// Takes an input string and compares it 
// with the input element's value.
// example: SS number mask would be "nnn-nn-nnnn"
// first character in mask must be "n"
// author: Brad Mooneyham ? Innovex Technology, LLC

function maskInput(s){
x = window.event.srcElement;
key_code = window.event.keyCode;
xlen = x.value.length;
xray = x.value.split("");
slen = s.length;
sray = s.split("");
if(xlen > slen){
    newstr = x.value.substring(0,slen);
    x.value = newstr;
    return;
}
    if(sray[xlen] && sray[xlen] == "n"){
        if(key_code != 8){
            if(!((key_code > 47 && key_code < 58) || (key_code > 95 && key_code < 106))) {
                strloc = xlen-1;
                newstr = x.value.substring(0,strloc);
                x.value = newstr; 
            }
        }
    }
    if(sray[xlen] && sray[xlen] != "n"){
        if(key_code != 8){
            x.value+= sray[xlen];
            if(sray[xlen+1] && sray[xlen+1] != "n"){
                x.value+= sray[xlen+1];
            }
        }
        else{
            strloc = xlen-1;
            newstr = x.value.substring(0,strloc);
            x.value = newstr;
        }
    }

}

function replaceAll( str, from, to ) {
    var idx = str.indexOf( from );
    while ( idx > -1 ) {
        str = str.replace( from, to ); 
        idx = str.indexOf( from );
    }
    return str;
}

// nulls a specified element's value
function clearElVal(el) {
	if( document.getElementById(el) ) {
		document.getElementById(el).value = "";
	}
}

//Function to escape a string using XML escaping rules
function escapeXml(val) {
    var apos_pos, quot_pos, amp_pos, lt_pos, gt_pos, cr_pos, lf_pos;
    var APOS = "'";
    var QUOTE = '"';
    var AMP = "&";
    var LT = "<";
    var GT = ">";
    var CR = "\r";
    var LF = "\n";
    var result = '';
    
    if( val==null)
    	return result;
    
    // Find first special characters if any
    apos_pos = val.indexOf(APOS);
    quot_pos = val.indexOf(QUOTE);
    amp_pos = val.indexOf(AMP);
    lt_pos = val.indexOf(LT);
    gt_pos = val.indexOf(GT);
    cr_pos = val.indexOf(CR);
    lf_pos = val.indexOf(LF);
    
    if (apos_pos == -1 && quot_pos == -1 && amp_pos == -1 && lt_pos == -1 && gt_pos == -1 && cr_pos == -1 && lf_pos == -1) {
        result = val;
    } else { 
        //replace '&' first so we don't replace escaped characters
        val = val.replace(/&/g, '&amp;'); 
        //escape less than and greater than 
        val = val.replace(/</g, '&lt;');
        val = val.replace(/>/g, '&gt;');
        //escape single and double quotes
        val = val.replace(/\'/g, '&apos;');
        val = val.replace(/\"/g, '&quot;');
        //escape carriage-returns and newlines using xml character references
        val = val.replace(/\r/g, '&#xD;');
        val = val.replace(/\n/g, '&#xA;');
        
        result = val;
    }
    
    return result;
}

function disableForm(){
	
	submitDisabled = true;
	
	// Disable the new Anchor style buttons
	var anchors = document.frmCMM.getElementsByTagName("a");
	
	for( var i = 0, anchorsLength = anchors.length; i < anchorsLength; i++) {
		
		var tempobj = anchors[i];
		tempobj.onclick = null;
		
		//below JQuery code was added to solve sql concurrency exception when user click on actionLink twice. Only those link that does an update to database(MT#8953)
		var tempjq = $(tempobj);
		if (tempjq.hasClass("actionLinkUpdate")) {
			tempjq.on("click.disable", function(e) {
				e.preventDefault();
			})
		}		
	}
	
	// Disable the old button elements
	var elements = document.frmCMM.elements;
	
	for( var i = 0, elementsLength = elements.length; i < elementsLength; i++) {
		
		var tempobj = elements[i];
		var type = tempobj.type.toLowerCase();
		
		if (type == "submit" || type == "button"){
			tempobj.disabled = true;
		}
	}	
	return true;
}

//*****
// StringBuilder class to speed string concatenation.
//
// From: http://www.codeproject.com/KB/scripting/stringbuilder.aspx (with modifications from suggestions)
//*****

// Initializes a new instance of the StringBuilder class
// and appends the given value if supplied
function StringBuilder(value) {
    this.strings = new Array("");
    this.append(value);
}

// Appends the given value to the end of this instance.
StringBuilder.prototype.append = function(value) {
    if (value)
    {
        // Add the value to the end of the strings array
        this.strings[this.strings.length] = value;
    }
    return this;
}

// Clears the string buffer
StringBuilder.prototype.clear = function() {
    this.strings.length = 1;
}

// Converts this instance to a String.
StringBuilder.prototype.toString = function() {
    return this.strings.join("");
}