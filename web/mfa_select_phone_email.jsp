<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.iscs.interfaces.mfa.MFAAuthentication" %>
<%@page import="java.util.ArrayList"%>
<%@page import="com.iscs.interfaces.mfa.MFATools" %>
<%@page import="net.inov.tec.beans.ModelBean" %>
<%@page import="net.inov.tec.security.SecurityManager" %>
<%@page import="com.iscs.common.utility.StringTools" %>
<%@page import="com.iscs.common.tech.log.Log" %>
<%@page import="com.iscs.common.tech.web.servlet.ServletServiceContextInitializer"%>
<%@page import="net.inov.tec.web.webpath.renderer.TileSetRenderer"%>
<%@page import="com.iscs.common.render.VelocityTools"%>
<%@page import="net.inov.biz.server.ServiceContext"%>
<%@page import="net.inov.tec.security.authenticator.AuthenticationAlert"%>
<%@page import="com.iscs.interfaces.mfa.MFASecurity" %>
<%@page import="com.iscs.interfaces.mfa.MFASettings" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
// Initialize a ServiceContext
ServletServiceContextInitializer ctxInitializer = null;
try {
	ServiceContext context = ServiceContext.getServiceContext();
	ctxInitializer = new ServletServiceContextInitializer();
    Exception initializerException = null;
    try {
		ctxInitializer.initialize(new Object[] {request, response});
    } catch (Exception e) {
    	// Swallow any exception since the login page knows how to deal with system unavailability issues
    	// Save it for logging once we start the thread log, though
    	initializerException = e;
    }
    
    context.getLogContext().setLogContextAttribute("RequestType", "WEB");
    context.getLogContext().setLogContextAttribute("RequestName", "MFASelectPhoneEmail");
    			
    Log.start(ServiceContext.getServiceContext().getLogfile());
    // Start a stopwatch for the request response time. This can be picked up later in a rendering phase (e.g. analytics-tracking-body-end.html)
    Log.startStopwatch("responseTime");
    
    if (initializerException != null) {
    	// We had an exception during service context initialization. Log it now that we've started the thread log
    	Log.error("Exception initializing service context", initializerException);
    }
	Log.debug("JSP service context initialized");

	// Verify the application has initialized
	Boolean initialized = (Boolean) application.getAttribute("initialized");
	if (initialized == null || !initialized) {
		String msg = "Innovation has not initialized properly! Is web.xml missing the AppContextLoaderListener?";
		Log.error(msg);
		response.setStatus(500);
		throw new ServletException(msg);
	}
	AuthenticationAlert[] alerts = new AuthenticationAlert[0];
	//alerts = AuthenticationAlertCache.getCache().getAuthenticationAlerts(userName);		

	if (alerts.length > 0){
		pageContext.setAttribute("hasErrors", "true");
		context.getLogContext().setAuthenticationAlertErrors(alerts);	
	}
	//Header necessary to make sure that IE browser does not default to compatibility mode when connected to Intranet.
	//Note that even though IE reports that it is in Compatibility Mode, it actually renders pages correctly.
    response.addHeader("X-UA-Compatible", "IE=edge");
	if (!new MFATools().allowPageAccess(request, session, new String[]{"mfa_select_phone_email.jsp", "mfa_enter_pin.jsp"})){
		%> <jsp:forward page = "/login.jsp" /> <%
	}
%>
<html>
<head>
<!-- Page load timing -->
<script type="text/javascript">
    var pageLoadStartTimestamp = (new Date()).getTime();
	window.seleniumPageLoadOutstanding = 1;

	function submitSendCode() {
		window.seleniumPageLoadOutstanding++;
		if (validateForm()){
			if (document.getElementsByName("GoogleAuthenticatorOption")[0] && document.getElementsByName("GoogleAuthenticatorOption")[0].checked){
				document.SendCodeGAForm.submit();
			} else {
				document.SendCodeForm.submit();
			}	
		}	
	}

	function validateForm() {
		var radios = document.getElementsByName('Destination')
	    for (var i = 0; i < radios.length; i++) {
			if (radios[i].checked) {
				return true;
			}
		}; 
		if (document.getElementById("GoogleAuthenticatorOption").checked){
			return true;
		}
		alert("Please select one method to send the verification code");
	    return false;
	}
	
	function selectRadioButton(button){
		if (button.value && button.value == "GoogleAuthenticatorOption"){
			document.getElementById("SubmitSendCode").innerHTML="<span>&nbsp;</span>Continue";
			var radios = document.getElementsByName('Destination')
		    for (var i = 0; i < radios.length; i++) {
				radios[i].checked = false;
			}; 
		} else {	
			document.getElementById("SubmitSendCode").innerHTML="<span>&nbsp;</span>Send code";
			document.getElementById("GoogleAuthenticatorOption").checked = false;
		}
		document.getElementById("SubmitSendCode").enable;
	}
</script>

     <title>Guidewire InsuranceNow&trade; Select Phone or Email</title>
     <style type="text/css" media="screen">
     <!--
     @import url(innovation.css);
     @import url(color1.css);
     @import url(css/ui-lightness/jquery-ui-1.12.1.css);
     @import url(css/dynaskin-vista/ui.dynatree.css);
     -->
     </style>
     <style type="text/css" media="print">
     <!--
     @import url(innovation.css);
     @import url(print.css);
     @import url(css/ui-lightness/jquery-ui-1.12.1.css);
     @import url(css/dynaskin-vista/ui.dynatree.css);
     -->
     </style>
     <meta http-equiv="content-type" content="text/html" />
     <meta name="author" content="Guidewire" />
     <meta name="generator" content="InsuranceNow" />
     <meta name="viewport" content="width=1137" />
     <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png"/>
     <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32"/>
     <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16"/>
     <link rel="manifest" href="manifest.json"/>
     <link rel="mask-icon" href="safari-pinned-tab.svg" color="#5bbad5"/>
     <meta name="theme-color" content="#ffffff"/>
     <link rel="alternate stylesheet" type="text/css" href="color2.css" title="color2" />
	 <link rel="stylesheet" type="text/css" href="css/ui-lightness/jquery-ui-1.12.1.css" title="color1" />	
	 <link rel="alternate stylesheet" type="text/css" href="css/redmond/jquery-ui-1.12.1.css" title="color2" />	
	 <link rel="stylesheet" type="text/css" href="css/dynaskin-vista/ui.dynatree.css" title="dynatree" id="skinSheet"/>	
     <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
     <script type="text/javascript" src="js/jquery-ui-1.12.1.min.js"></script>
     <script type="text/javascript" src="innovex.js"></script>
     <script type="text/javascript" src="innovation.js"></script>
     <script type="text/javascript" src="style-switcher.js"></script>
     <script type="text/javascript" src="news-banner.js"></script>
     <script language="JavaScript" type="text/javascript" src="authorize-net.js"></script>
<%
    TileSetRenderer rend = new TileSetRenderer();
    out.println(rend.renderPage(VelocityTools.tile("COAnalytics::tileset::form::analytics-tracking-head-end"))); 
%>

</head>
<%
	String userName = (String)session.getAttribute("MFAUser");
	ModelBean userInfo = SecurityManager.getSecurityManager().getUser(userName);
	MFATools tools = new MFATools();
	ArrayList<String> phoneDestinations = tools.getPhoneDestinations(userInfo);
	ArrayList<String> emailDestinations = tools.getEmailDestinations(userInfo);
	
	if (phoneDestinations.isEmpty() && emailDestinations.isEmpty()){
%>
	<jsp:forward page = "/mfa_enter_phone_email.jsp" />
<%	
	}
	StringTools maskingTools = new StringTools();
%>
<body>
	<div class='pageBackground'>
		<div class='banner'></div>
		<div class='menu'>
			<ul>
				<li><span></span></li>
			</ul>
			<div class='clear'></div>
		</div>
		<div class='subMenu'>    
			<div class="wiz_bottom_left"></div>
			<div class="wiz_bottom_right"></div>
		</div>
		<c:if test='${hasErrors == "true"}'>
			<div class='errorMsg'>
				<div class='error_top_left'></div>
				<div class='error_top_right'></div>
				<div class='clear'></div>
				<div class='error_content'>
					<div class='error_content_left'>
					<div class='error_icon'></div>
					<span>ATTENTION!</span>
				</div>
				<div class='error_content_right'>
<%
	for (int i=0; i<alerts.length; i++){
		out.println(alerts[i].getMessage());
		out.println("<br>");
	}
%>
				</div>
				<div class='clear'></div>
				</div>
				<div class='error_bottom_left'></div>
				<div class='error_bottom_right'></div>
			</div>
		</c:if>
		<div class='mfaTile'>
			<div class='r_tile'> <!-- Start r_tile -->
			<div class="r_tile_top_left"></div>
			<div class="r_tile_top_right">Multi Factor Authentication</div>
			<div class="clear"></div>
			<div class="r_tile_content_right">
				<div class="r_tile_content_left">
					<h3>First time signing in with this device?</h3>
					<h4>
						We haven't seen you sign in from this device before.
						<br>
						Choose a method below to verify that this is your account.
					</h4>
					<form action="/mfa_enter_pin.jsp" method="post" id="SendCodeForm" name="SendCodeForm">
						<input type="hidden" name="NavigationPage" value=<%=MFASecurity.encryptPageNavigationToken(userName, "mfa_enter_pin.jsp")%> />
						<table>
<%  for (int i=0; i < phoneDestinations.size(); i++) { %>	
							<tr>
								<td><input type="radio" id="PhoneDestination_<%=i%>" name="Destination" value="PhoneDestination_<%=i%>" onclick='javascript:selectRadioButton(this);'></td> 
								<td>Send text message to phone <%=maskingTools.maskDestinationValue("Phone", phoneDestinations.get(i))%></td>
							</tr>
<% 	} 
	for (int i=0; i < emailDestinations.size(); i++) { %>
							<tr>	
								<td><input type="radio" id="EmailDestination_<%=i%>" name="Destination" value="EmailDestination_<%=i%>" onclick='javascript:selectRadioButton(this);'></td> 
								<td>Send email to <%=maskingTools.maskDestinationValue("Email", emailDestinations.get(i))%></td>
							</tr>
<% 	} %>
						</table>
						<br>
					</form>
<% if (MFASettings.INSTANCE.isGoogleAuthenticatorEnabled() && tools.isRegisterdToGoogleAuthenticatorVerified(userInfo)){ %>	
					<form action="/mfa_ga.jsp" method="post" id="SendCodeGAForm" name="SendCodeGAForm">
						<input type="hidden" name="NavigationPage" value=<%=MFASecurity.encryptPageNavigationToken(userName, "mfa_ga.jsp")%> />
						<table>
						<tr>	
							<td><input type="radio" id="GoogleAuthenticatorOption" name="GoogleAuthenticatorOption" value="GoogleAuthenticatorOption" onclick='javascript:selectRadioButton(this);' ></td> 
							<td>Use Google Authenticator
								<input type="hidden" name="GoogleAuthenticatorFlag" value="Yes"></td>
						</tr>
						</table>
					</form>	
<% } %>	
					<br>		
					<div><a class='button' name="SubmitSendCode" id='SubmitSendCode' href='javascript:submitSendCode();' disabled><span>&nbsp;</span>Continue</a></div>
				</div>
			</div>
			<div class="clear"></div>
			<div class="r_tile_bottom_left"></div>
			<div class="r_tile_bottom_right"></div>
			<div class="clear"></div>
			</div> <!-- End r_tile -->
		</div>
		<div class='tile'>
			<div class='clear'></div>
			<div id='FooterTile' class='footer'>
				<div class="footer_left"></div>
				<div class="footer_content">
					<img src="img/powered_by.png" class="powered_by" />
					&nbsp;&nbsp;<a href="#" onclick="setActiveStyleSheet('color1'); return false;"><img src="img/color1_selection.png" alt="Turn Site Orange" border='0'/></a>
					&nbsp;&nbsp;<a href="#" onclick="setActiveStyleSheet('color2'); return false;"><img src="img/color2_selection.png" alt="Turn Site Blue" border='0'/></a>
				</div>
			</div>  <!-- End of <div class='footer'> -->
		</div> <!-- End of <div class='tile'> -->
	</div><!-- End pageBackground -->
<%
    out.println(rend.renderPage(VelocityTools.tile("COAnalytics::tileset::form::analytics-tracking-body-end"))); 
%>

<script type="text/javascript">
	window.seleniumPageLoadOutstanding--;
</script>	
</body>
<%
} catch (Exception e){
	Log.error(e);
} finally {
	Log.debug("Finalizing JSP service context");
	try {
		// close the transaction
		ctxInitializer.finalizeServiceContext();			
	} catch(Exception e) {
		Log.error(e);
	}
	
	// Export the logs
	if (Log.isLogThreads()) {
		Log.export();
	}
}
%>	
</html>