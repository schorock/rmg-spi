/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */

window.locale = {
    "fileupload": {
        "errors": {
            "maxFileSize": "File is too big",
            "minFileSize": "File is too small",
            "acceptFileTypes": "Filetype not allowed",
            "maxNumberOfFiles": "Max number of files exceeded",
            "uploadedBytes": "Uploaded bytes exceed file size",
            "emptyResult": "Empty file upload result"
        },
        "error": "ERROR",
        "start": "START",
        "cancel": "CANCEL",
        "destroy": "DELETE"
    }
};
$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget
    $('#fileupload').fileupload();

    // Set Options
    $('#fileupload').fileupload('option', {
        url: 'multifileupload/v2',
        maxFileSize: 500000000 // 500MB
    });
    
    // Add Additional Form Data
    $('#fileupload').fileupload({
        formData: {ThumbnailSize: '70x70'}
    });
    
    var idAttr = $('#MultiFileStorageButton a');
    var classAttr = idAttr.attr('class');
    $('#fileupload').fileupload({
    		
    	// Disable Add Attachment/Note Button
    	start: function(){
	    	idAttr.attr('class', classAttr + ' disabled');
	    	idAttr.css('cursor', 'default');
	    	idAttr.attr('disabled', '');
		}, 
		
		// Enable Add Attachment/Note Button
		stop: function(){
	    	idAttr.attr('class', classAttr);
	    	idAttr.css('cursor', 'pointer');
			idAttr.removeAttr('disabled');
			$("#fileupload-list tbody.files").sortable( { handle: ".preview", cursor: "n-resize" } );
			$("#fileupload-list tbody.files").disableSelection();
		}
    });
});