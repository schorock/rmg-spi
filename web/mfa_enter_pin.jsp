<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="net.inov.tec.security.authenticator.AuthenticationAlertCache"%>
<%@page import="com.iscs.interfaces.mfa.MFAAuthentication" %>
<%@page import="com.iscs.interfaces.mfa.MFATools" %>
<%@page import="com.iscs.interfaces.mfa.MFASecurity" %>
<%@page import="com.iscs.interfaces.mfa.MFASettings" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map" %>
<%@page import="java.lang.String" %>
<%@page import="net.inov.tec.beans.ModelBean" %>
<%@page import="net.inov.tec.security.SecurityManager" %>
<%@page import="com.iscs.common.tech.log.Log" %>
<%@page import="com.iscs.common.tech.web.servlet.ServletServiceContextInitializer"%>
<%@page import="net.inov.tec.web.webpath.renderer.TileSetRenderer"%>
<%@page import="com.iscs.common.render.VelocityTools"%>
<%@page import="net.inov.biz.server.ServiceContext"%>
<%@page import="net.inov.tec.security.authenticator.AuthenticationAlert"%>
<%@page import="net.inov.biz.server.IBIZException"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
// Initialize a ServiceContext
ServletServiceContextInitializer ctxInitializer = null;
try {
	ServiceContext context = ServiceContext.getServiceContext();
	ctxInitializer = new ServletServiceContextInitializer();
    Exception initializerException = null;
    try {
		ctxInitializer.initialize(new Object[] {request, response});
    } catch (Exception e) {
    	// Swallow any exception since the login page knows how to deal with system unavailability issues
    	// Save it for logging once we start the thread log, though
    	initializerException = e;
    }
    context.getLogContext().setLogContextAttribute("RequestType", "WEB");
    context.getLogContext().setLogContextAttribute("RequestName", "MFAEnterPin");
    			
    Log.start(ServiceContext.getServiceContext().getLogfile());
    // Start a stopwatch for the request response time. This can be picked up later in a rendering phase (e.g. analytics-tracking-body-end.html)
    Log.startStopwatch("responseTime");
    
    if (initializerException != null) {
    	// We had an exception during service context initialization. Log it now that we've started the thread log
    	Log.error("Exception initializing service context", initializerException);
    }
	Log.debug("JSP service context initialized");

	// Verify the application has initialized
	Boolean initialized = (Boolean) application.getAttribute("initialized");
	if (initialized == null || !initialized) {
		String msg = "Innovation has not initialized properly! Is web.xml missing the AppContextLoaderListener?";
		Log.error(msg);
		response.setStatus(500);
		throw new ServletException(msg);
	}
	String userName = (String)session.getAttribute("MFAUser");
	AuthenticationAlert[] alerts = AuthenticationAlertCache.getCache().getAuthenticationAlerts(userName);		

	if (alerts.length > 0){
		pageContext.setAttribute("hasErrors", "true");
		context.getLogContext().setAuthenticationAlertErrors(alerts);	
	}
	//Header necessary to make sure that IE browser does not default to compatibility mode when connected to Intranet.
	//Note that even though IE reports that it is in Compatibility Mode, it actually renders pages correctly.
    response.addHeader("X-UA-Compatible", "IE=edge");
	if (!new MFATools().allowPageAccess(request, session, new String[]{"mfa_enter_pin.jsp"})){
		%> <jsp:forward page = "/login.jsp" /> <%
	}
%>
<html>
<head>
<!-- Page load timing -->
<script type="text/javascript">
    var pageLoadStartTimestamp = (new Date()).getTime();
	window.seleniumPageLoadOutstanding = 1;

	function submitVerifyCode() {
		window.seleniumPageLoadOutstanding++;
		document.VerifyCodeForm.submit();
	}
	
	function submitReturn() {
		window.seleniumPageLoadOutstanding++;
		document.ReturnForm.submit();
	}
</script>

     <title>Guidewire InsuranceNow&trade; Enter Verification Code</title>
     <style type="text/css" media="screen">
     <!--
     @import url(innovation.css);
     @import url(color1.css);
     @import url(css/ui-lightness/jquery-ui-1.12.1.css);
     @import url(css/dynaskin-vista/ui.dynatree.css);
     -->
     </style>
     <style type="text/css" media="print">
     <!--
     @import url(innovation.css);
     @import url(print.css);
     @import url(css/ui-lightness/jquery-ui-1.12.1.css);
     @import url(css/dynaskin-vista/ui.dynatree.css);
     -->
     </style>
     <meta http-equiv="content-type" content="text/html" />
     <meta name="author" content="Guidewire" />
     <meta name="generator" content="InsuranceNow" />
     <meta name="viewport" content="width=1137" />
     <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png"/>
     <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32"/>
     <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16"/>
     <link rel="manifest" href="manifest.json"/>
     <link rel="mask-icon" href="safari-pinned-tab.svg" color="#5bbad5"/>
     <meta name="theme-color" content="#ffffff"/>
     <link rel="alternate stylesheet" type="text/css" href="color2.css" title="color2" />
	 <link rel="stylesheet" type="text/css" href="css/ui-lightness/jquery-ui-1.12.1.css" title="color1" />	
	 <link rel="alternate stylesheet" type="text/css" href="css/redmond/jquery-ui-1.12.1.css" title="color2" />	
	 <link rel="stylesheet" type="text/css" href="css/dynaskin-vista/ui.dynatree.css" title="dynatree" id="skinSheet"/>	
     <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
     <script type="text/javascript" src="js/jquery-ui-1.12.1.min.js"></script>
     <script type="text/javascript" src="innovex.js"></script>
     <script type="text/javascript" src="innovation.js"></script>
     <script type="text/javascript" src="style-switcher.js"></script>
     <script type="text/javascript" src="news-banner.js"></script>
     <script language="JavaScript" type="text/javascript" src="authorize-net.js"></script>
<%
    TileSetRenderer rend = new TileSetRenderer();
    out.println(rend.renderPage(VelocityTools.tile("COAnalytics::tileset::form::analytics-tracking-head-end"))); 
%>

</head>
<body>
	<div class='pageBackground'>
		<div class='banner'></div>
		<div class='menu'>
				<ul>
					<li><span></span></li>
				</ul>
				<div class='clear'></div>
		</div>
		<div class='subMenu'>    
			<div class="wiz_bottom_left"></div>
			<div class="wiz_bottom_right"></div>
		</div>
		<c:if test='${hasErrors == "true"}'>
			<div class='errorMsg'>
				<div class='error_top_left'></div>
				<div class='error_top_right'></div>
				<div class='clear'></div>
				<div class='error_content'>
					<div class='error_content_left'>
					<div class='error_icon'></div>
					<span>ATTENTION!</span>
				</div>
				<div class='error_content_right'>
					<%
					for(int i=0; i<alerts.length; i++){
						out.println(alerts[i].getMessage());
						out.println("<br>");
					}
					%>
				</div>
				<div class='clear'></div>
				</div>
				<div class='error_bottom_left'></div>
				<div class='error_bottom_right'></div>
			</div>
		</c:if>
		<div class='mfaTile'>
			
	<div class='r_tile'> <!-- Start r_tile-->
	<div class="r_tile_top_left"></div>
	<div class="r_tile_top_right">Multi Factor Authentication</div>
	<div class="clear"></div>
	<div class="r_tile_content_right">
		<div class="r_tile_content_left">
<% 
	MFATools tools = new MFATools();
	ModelBean userInfo = SecurityManager.getSecurityManager().getUser(userName);
	String pinDestination = "";
	String[] destinations = request.getParameterValues("Destination");
	if (destinations != null && destinations.length > 0){
		String pinDestinationIndex = destinations[0];
		if (pinDestinationIndex != null && !pinDestinationIndex.isEmpty()){
			pinDestination = tools.getClearPinDestination(pinDestinationIndex, userInfo);
		}
	} else {
		pinDestination = request.getParameter("PinDestination");
	}

	String manualEnteredPinDestination = request.getParameter("ManualEnteredPinDestination");
	boolean sendAnotherPinInd = new MFATools().isParameterTrue(request, "sendanotherpin");;
	boolean isRestrictedManualEnteredPinDestination = false;
	if (pinDestination == null || pinDestination.isEmpty()){
		if (new MFATools().getAllowManualEnteredPinDestination(userName)){
			if (!sendAnotherPinInd && !tools.validatePinDestinationFormat(manualEnteredPinDestination)){
				String forwardPage = "/mfa_enter_phone_email.jsp?InvalidFormat=" + manualEnteredPinDestination;
%>
				<jsp:forward page='<%=forwardPage%>' />
<%		
			}
		} else {
			isRestrictedManualEnteredPinDestination = true;
		}
	}
	boolean selfVerified = false;
	if (isRestrictedManualEnteredPinDestination && (pinDestination == null || pinDestination.isEmpty())){
		int maxAttempts = MFASettings.INSTANCE.getMaxSelfAuthenticationFailureCount();
%>
	<div>
		<h4>We are sorry, for security reasons you are only allowed	<%=maxAttempts%> attempts to provide a valid phone or email.
		</h4> 
		<h4>Please	contact your support team or help desk.</h4>
	</div>
	<form action="/login.jsp" method="post" id="ReturnForm" name="ReturnForm">
		<a class='button' name="submitReturn" id='submitReturn' href='javascript:submitReturn();' ><span>&nbsp;</span>Return</a>
	</form>
<%
	} else {
		if (pinDestination == null || pinDestination.isEmpty()){
			if (manualEnteredPinDestination != null && !manualEnteredPinDestination.isEmpty()){
				pinDestination = manualEnteredPinDestination;
				selfVerified = true;
				sendAnotherPinInd = true;
			}
		} else {
			sendAnotherPinInd = true;
		}
		if (sendAnotherPinInd){
			try {
				new MFAAuthentication().sendAuthenticationCode(request, session, userName, pinDestination, false);
			} catch (IBIZException e){
				Log.error(e);
				if (e.getMessage().equalsIgnoreCase("SmsException")){
					AuthenticationAlertCache.getCache().addAuthenticationAlert(userName, "SmsException", "Error sending text message to " + pinDestination);
					pageContext.setAttribute("hasErrors", "true");
					ArrayList<String> phoneDestinations = tools.getPhoneDestinations(userInfo);
					ArrayList<String> emailDestinations = tools.getEmailDestinations(userInfo);
					String pageDestination = "/mfa_select_phone_email.jsp";
					if (phoneDestinations.isEmpty() && emailDestinations.isEmpty()){
						pageDestination = "/mfa_enter_phone_email.jsp";
					}
%>
					<jsp:forward page="<%=pageDestination%>" />
<%						
				}
			}
		}
%>
	<div>
		<h4>Enter the code we sent to <%=tools.maskDestinationValue(pinDestination)%></h4>
	</div>
	<form id="loginFormSuccess" action="/login.jsp" method="post" id="VerifyCodeForm" name="VerifyCodeForm">
		<input type="hidden" name="PinDestination" value=<%=pinDestination%> >
<%
	if (selfVerified){
%>		<input type="hidden" name="MFASelfVerified" value="Yes" /> <%
	}
%>		
		<div>
			<span><input type="text" name="mfa_pin" size="6" autofocus></span>
			<span><a class='button' name="submitVerifyCode" id='submitVerifyCode' href='javascript:submitVerifyCode();' ><span>&nbsp;</span>Verify</a></span>
		</div>
		<br>
		<div><input type="checkbox" id="remember_device" name="remember_device" value="Yes">Remember me on this device</div>
	</form>
	<br>
	<form id="SendAnotherCodeForm" action="/mfa_select_phone_email.jsp" method="post">
		<input type="hidden" name="NavigationPage" value=<%=MFASecurity.encryptPageNavigationToken(userName, "mfa_select_phone_email.jsp")%> />
		<input type="hidden" name="sendanotherpin" value="Yes">
		<input type="hidden" name="ManualEnteredPinDestination" value=<%=manualEnteredPinDestination%> >
		<input type="hidden" name="PinDestination" value=<%=pinDestination%> >
		<table>
			<tr>
				<td colspan="2"><a href='javascript:document.getElementById("SendAnotherCodeForm").submit();'>Send another code.</a></td>
			</tr>
		</table>
	</form>
<%
	}
%>
	</div>
	</div>
	<div class="clear"></div>
	<div class="r_tile_bottom_left"></div>
	<div class="r_tile_bottom_right"></div>
	<div class="clear"></div>
	</div> <!-- End r_tile-->
	</div> <!-- End class='mfaTile' -->
	<div class='tile'>
		<div class='clear'></div>
		<div id='FooterTile' class='footer'>
			<div class="footer_left"></div>
			<div class="footer_content">
				<img src="img/powered_by.png" class="powered_by" />
					&nbsp;&nbsp;<a href="#" onclick="setActiveStyleSheet('color1'); return false;"><img src="img/color1_selection.png" alt="Turn Site Orange" border='0'/></a>
					&nbsp;&nbsp;<a href="#" onclick="setActiveStyleSheet('color2'); return false;"><img src="img/color2_selection.png" alt="Turn Site Blue" border='0'/></a>
			</div>
		</div>  <!-- End of <div class='footer'> -->
	</div> <!-- End of <div class='tile'> -->
<%
    out.println(rend.renderPage(VelocityTools.tile("COAnalytics::tileset::form::analytics-tracking-body-end"))); 
%>
	</div> <!-- End of pageBaeckground -->
<script type="text/javascript">
	window.seleniumPageLoadOutstanding--;
</script>		
</body>
<%
} catch (Exception e){
	Log.error(e);
} finally {
	Log.debug("Finalizing JSP service context");
	try {
		// close the transaction
		ctxInitializer.finalizeServiceContext();			
	} catch(Exception e) {
		Log.error(e);
	}
	
	// Export the logs
	if (Log.isLogThreads()) {
		Log.export();
	}
}
%>	
</html>