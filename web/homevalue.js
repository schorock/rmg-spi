var winHV;
var __homeValueRq;

var airSingleValPropertyInfoTypes = [
	"BasementDaylight", 
	"BasementSquareFeet",
	"Bedrooms",
	"BuildingStoriesRes",
	"ConstructionMethod",
	"GarageParkingSquareFeet",
	"LivingSquareFeet",
	"PercentBasementFinished",
	"PercentCathedral",
	"Perimeter",
	"ResidentialShape",
	"ResidentialUse",
	"SiteAccess",
	"SiteSlope",
	"TotalRooms",
	"TotalUnitsInBldg",
	"YearBuilt"
	];
	
var airMultiValPropertyInfoTypes = [
	"AirConditioningType", 
	"AttachedStructures", 
	"Bathrooms",
	"Builtins",
	"Ceiling",
	"CustomFeatures",
	"DetachedStructures",
	"ExteriorFeatures",
	"ExteriorWallType",
	"FlooringType",
	"FoundationMaterial",
	"FoundationMix",
	"FramingOverride",
	"GarageTypeMulti",
	"HeatingFuelType",
	"HeatingType",
	"InteriorFeatures",
	"InteriorWallFinish",
	"InteriorWallMaterial",
	"RoofCover",
	"RoofShape",
	"WallHeightOverride"
	];


function SingleValPropertyInfoElement(){
	
	var self = this;
	self.TypeName;
	self.Estimate;
	
}

function MultiValPropertyInfoElement(){
	
	var self = this;
	self.TypeName;
	self.Items = new Array();
	
}

function MultiValItem(){
	
	var self = this;
	self.Value;
	self.Estimate;
	
}

function HomeValueRq(){

	var self = this;  
	
	self.User;
	self.Password;
	self.CompanyKey;
	self.CompanyToken;
	self.URL;
	self.UserType;
	self.Parent;
	self.Peer;
	self.DefaultPolicyNumber;
	self.Agent;
	self.Agency;
	self.PolicyNumber;
	self.QuoteNumber;
	self.ReferenceNumber;
	self.InsuredFirstName;
	self.InsuredLastName;
	self.SecondInsuredFirstName;
	self.SecondInsuredLastName;
	self.InsuredHomePhone;
	self.InsuredWorkPhone;
	self.CoverageALimit;
	self.CoverageADeductible;
	self.Addr1;
	self.StreetNumber;
	self.StreetPrefix;
	self.StreetName;
	self.StreetType;
	self.StreetSuffix;
	self.UnitNumber;
	self.BuildingName;
	self.BuildingFloor;
	self.CondoTemplate;
	self.City;
	self.StateProvCd;
	self.PostalCode;
	self.Country;
	self.MultiValPropertyInfoElements = new Array();
	self.SingleValPropertyInfoElements = new Array();
	self.onReturn;
	
	this.setSingleValPropertyInfoElement = function(typeName, est){
	
		// Validate the multiplicity of the property info type
		var multiplicity = getPropertyInfoTypeMultiplicity(typeName);
		if(!multiplicity){
			alert("Invalid property info type: '" + typeName + "'");
			return;
		}
		if(multiplicity == "MULTI"){
			alert("Cannot add a multiple value property info type to the single value items array: '" + typeName + "'");
			return;
		}
		
		// Find the element if we already have it in memory
		var element;
		for(var i=0; i<self.SingleValPropertyInfoElements.length; i++){
			if(self.SingleValPropertyInfoElements[i].TypeName == typeName){
				element = self.SingleValPropertyInfoElements[i];
				break;
			}
		}
		
		// Create the element if we do not have it in memory
		if(!element){
			element = new SingleValPropertyInfoElement();
			self.SingleValPropertyInfoElements.push(element);
			element.TypeName = typeName;
		}
		
		element.Estimate = est;
		
	}
	
	
	
	// Add a property info element
	this.addMultiValPropertyInfoItem = function(typeName, value, est){
	
		// Validate the multiplicity of the property info type
		var multiplicity = getPropertyInfoTypeMultiplicity(typeName);
		if(!multiplicity){
			alert("Invalid property info type: '" + typeName + "'");
			return;
		}
		if(multiplicity == "SINGLE"){
			alert("Cannot add a single valut property info type to the multi value items array: '" + typeName + "'");
			return;
		}
		
		// Find the element if we already have it in memory
		var element;
		for(var i=0; i<self.MultiValPropertyInfoElements.length; i++){
			if(self.MultiValPropertyInfoElements[i].TypeName == typeName){
				element = self.MultiValPropertyInfoElements[i];
				break;
			}
		}
		
		// Create the element if we do not have it in memory
		if(!element){
			element = new MultiValPropertyInfoElement();
			self.MultiValPropertyInfoElements.push(element);
			element.TypeName = typeName;
		}
		
		// Add to the element's itens
		var item = new MultiValItem();
		item.Value = value;
		item.Estimate = est;
		element.Items.push(item);
		
	}
	
	
	this.submit = function(){
		
		__homeValueRq = self;
		launchHomeValue(self.User, self.Password, self.getXml());
		monitorHVWindow();

	}
	
	this.getXml = function(){
	
		var xml = "<AIRRequest>";
		
		// Build the HomeValue element
		xml +="<HomeValue>";
		
		xml += self.Parent ? "<Parent>" + self.Parent + "</Parent>" : "";
		xml += self.Peer ? "<Peer>" + self.Peer + "</Peer>" : "";
		xml += self.UserType ? "<UserType>" + self.UserType + "</UserType>" : "";
		xml += self.CompanyKey ? "<CompanyKey>" + self.CompanyKey + "</CompanyKey>" : "";
		xml += self.CompanyToken ? "<CompanyToken>" + self.CompanyToken + "</CompanyToken>" : "";
		
		// Build the property defaults
		if(self.Agent || self.Agency || self.PolicyNumber){
			xml += "<PropertyDefaults>";
			xml += self.DefaultPolicyNumber ? "<PolicyNumber>" + self.DefaultPolicyNumber + "</PolicyNumber>" : "";
			xml += "</PropertyDefaults>";
		}
		
		// Add the address
		if(self.Addr1){
		
			xml += "<Addresses>";
			xml += "<Address>";
			
			xml += self.Addr1 ? "<Type>UnparsedRiskAddress</Type>" : "<Type>ParsedRiskAddress</Type>";
			xml += self.Addr1 ? "<Addr1>" + self.Addr1 + "</Addr1>" : "";
			xml += self.StreetNumber ? "<StreetNumber>" + self.StreetNumber + "</StreetNumber>" : "";
			xml += self.StreetPrefix ? "<StreetPrefix>" + self.StreetPrefix + "</StreetPrefix>" : "";
			xml += self.StreetName ? "<StreetName>" + self.StreetName + "</StreetName>" : "";
			xml += self.StreetType ? "<StreetType>" + self.StreetType + "</StreetType>" : "";
			xml += self.StreetSuffix ? "<StreetSuffix>" + self.StreetSuffix + "</StreetSuffix>" : "";
			xml += self.UnitNumber ? "<UnitNumber>" + self.UnitNumber + "</UnitNumber>" : "";
			xml += self.BuildingName ? "<BuildingName>" + self.BuildingName + "</BuildingName>" : "";
			xml += self.BuildingFloor ? "<BuildingFloor>" + self.BuildingFloor + "</BuildingFloor>" : "";
			xml += self.City ? "<City>" + self.City + "</City>" : "";
			xml += self.StateProvCd ? "<StateProv>" + self.StateProvCd + "</StateProv>" : "";
			xml += self.PostalCode ? "<PostalCode>" + self.PostalCode + "</PostalCode>" : "";
			xml += self.Country ? "<Country>" + self.Country + "</Country>" : "";

			xml +="<PropertyInfo>";
			xml += self.Agent ? "<Agent>" + self.Agent + "</Agent>" : "";
			xml += self.Agency ? "<Agency>" + self.Agency + "</Agency>" : "";
			xml += self.ReferenceNumber ? "<LocationName>" + self.ReferenceNumber + "</LocationName>" : "";
			xml += self.PolicyNumber ? "<PolicyNumber>" + self.PolicyNumber + "</PolicyNumber>" : "";	
			xml += self.QuoteNumber ? "<QuoteNumberID>" + self.QuoteNumber + "</QuoteNumberID>" : "";	
			xml += self.InsuredFirstName ? "<FirstInsuredFirstName>" + self.InsuredFirstName + "</FirstInsuredFirstName>" : "";	
			xml += self.InsuredLastName ? "<FirstInsuredLastName>" + self.InsuredLastName + "</FirstInsuredLastName>" : "";	
			xml += self.SecondInsuredFirstName ? "<SecondInsuredFirstName>" + self.SecondInsuredFirstName + "</SecondInsuredFirstName>" : "";	
			xml += self.SecondInsuredLastName ? "<SecondInsuredLastName>" + self.SecondInsuredLastName + "</SecondInsuredLastName>" : "";	
			xml += self.InsuredHomePhone ? "<HomePhone>" + self.InsuredHomePhone + "</HomePhone>" : "";	
			xml += self.InsuredWorkPhone ? "<WorkPhone>" + self.InsuredWorkPhone + "</WorkPhone>" : "";	
			xml += self.CoverageALimit ? "<CoverageALimit>" + self.CoverageALimit + "</CoverageALimit>" : "";	
			xml += self.CoverageADeductible ? "<CoverageADeductible>" + self.CoverageADeductible + "</CoverageADeductible>" : "";	
			xml += self.CondoTemplate ? "<ResidentialUse>2</ResidentialUse>" : "";
			xml += self.CondoTemplate ? "<CondoTemplate>" + self.CondoTemplate + "</CondoTemplate>" : "";
			
			// Add the PropertyInfo
			if(self.SingleValPropertyInfoElements.length > 0 || self.MultiValPropertyInfoElements.length > 0){
				
				for(var i=0; i<self.SingleValPropertyInfoElements.length; i++){
					var element = self.SingleValPropertyInfoElements[i];
					xml += "<" + element.TypeName + " est='" + element.Estimate + "' />";
				}
				for(var i=0; i<self.MultiValPropertyInfoElements.length; i++){
					var element = self.MultiValPropertyInfoElements[i];
					xml += "<" + element.TypeName + ">";
					for(var j=0; j<element.Items.length; j++){
						var item = element.Items[j];
						xml += "<Type val='" + item.Value + "' est='" + item.Estimate + "' />";
					}
					xml +="</" + element.TypeName + ">";
				}
				
			}
			xml +="</PropertyInfo>";
			
			xml += "</Address>";
			xml += "</Addresses>";
		
		}
		
		self.handleHVWindowClose = function() {
			alert("Home value window close");
		}

		xml += "</HomeValue>";
		xml += "</AIRRequest>";
		// alert(xml);
		return xml;
	
	}
	
	this.handleDataReportReceived = function( data, obj ) {
	
		// alert("Data report received for Home Value Request\n" + data.xml);
		if(self.onReturn){
			var dataReports = data.getElementsByTagName("DataReport");
			if(dataReports.length == 0){
				self.onReturn( null, self);
			}
			else if(dataReports.length == 1){
				self.onReturn( dataReports[0], self);
			}
			else{
				alert("Warning! Multiple data reports found!");
				self.onReturn( dataReports[0], self);
			}
			
		}
	
	}

}


function airCheckBrowser()
{
	//Check OS

	if (navigator.userAgent.indexOf('Windows') == -1)
	{
		alert("ISO HomeValue requires a Microsoft Windows operating system.");		
		return false;
	}

	//Check browser

	var intIndex = navigator.appVersion.indexOf('MSIE');
	var dblVersion = 0;

	if (intIndex != -1)
	{
		dblVersion = navigator.appVersion.substring(intIndex + 5, intIndex + 8);
	}

	if (dblVersion < 5.5)
	{
		alert("ISO HomeValue requires Microsoft Internet Explorer 5.5 or later. Please obtain the latest version of Internet Explorer from your administrator and try again.");
		return false;
	}

	//Check cookies are enabled

	var blnCookiesOK = true;

	if (navigator.cookieEnabled == false)
	{
		blnCookiesOK = false
	}
	else
	{
		document.cookie = 'HVCookieTest'
		if (document.cookie == "") blnCookiesOK = false;
	}

	if (blnCookiesOK == false)
	{
		alert("ISO HomeValue requires the use of cookies. Please enable cookies in Internet Explorer by clicking on Tools, Internet Options. Then, refresh this page and try again.");
		return false;
	}
}

function airGetBrowserInfo()
{
	var strComponentVersion;

	if (typeof(objClientCaps) == 'undefined')
	{
		if (document.appendChild)
		{
			document.appendChild(document.createElement('<IE:clientCaps id="objClientCaps" style="behavior:url(#default#clientcaps)"/>'));
			strComponentVersion = objClientCaps.getComponentVersion("{89820200-ECBD-11CF-8B85-00AA005B4383}", "ComponentID");
		} else
		{
			strComponentVersion = '';
		}
	}
	
	return navigator.userAgent + ' Build ' +  strComponentVersion + ' Res.: ' + screen.width + 'x' + screen.height; 
}

function launchHomeValue(strUserName, strPassword, strXML)
{
	if (strUserName == '') return;
	// if (airCheckBrowser() == false) return;	

	var strBrowserInfo = airGetBrowserInfo();

	if (strPassword == null)
	{
		strPassword = '';
	}

	if (strXML == null)
	{
		strXML = '';
	}
	// Enter the URL of the HomeValue system here
	//
	// This is for the PRODUCTION environment
	//
	//var strURL = 'www.isohomevalue.com';
	//
	// This is for the Development environment
	//

	var strURL = 'demo.isohomevalue.com';

	//
	// Set to false to use a popup window
	// Set to true to launch HV in the same window
	//

	var blnSameWindow = false;

	if (!blnSameWindow)
	{
		if(typeof(winHV) == 'undefined')
		{	
			winHV = window.open();
		}
		else if(winHV.closed == true)
		{
			winHV = window.open();
		}
		else
		{
			winHV.focus();
			return;
		}
	}
	else
	{
		winHV = window;
	}
	
	analyticsTrackPageview("iso-homevalue");

	with(winHV.document)
	{
		write('<html><body><form name="frmAuth" method="POST" action="https://' + strURL + '/homevaluenet/_login.aspx">');
		write('<input type="hidden" name="strUserName" value="' + strUserName + '"><input type="hidden" name="strPassword" value="' + strPassword + '">');
		write('<input type="hidden" name="hv" value="hv">');
		write('<input type="hidden" name="AutoLoginXML" value="' + strXML + '">');
//		write('<input type="hidden" name="CompanyToken" value="' + strCompanyToken + '">');
		write('</form></body><script type="text/javascript" src="https://' + strURL + '/homevaluenet/_autologin.aspx?hv=homevalue"></script></html>');
	}
	

}

// Returns "SINGLE" if the property info type has just one "est" value, "MULTI" if it has multiple, and null if it doesn't exist
function getPropertyInfoTypeMultiplicity(typeName){

	for(var i=0; i<airSingleValPropertyInfoTypes.length; i++){
		if(typeName == airSingleValPropertyInfoTypes[i]){
			return "SINGLE";
		}
	}
	
	for(var i=0; i<airMultiValPropertyInfoTypes.length; i++){
		if(typeName == airMultiValPropertyInfoTypes[i]){
			return "MULTI";
		}
	}
	
	return null;
	
}


function monitorHVWindow(){

	if(winHV.closed){
		handleHVWindowClose();
	}
	else{
		setTimeout(	monitorHVWindow, 500 );
	}
}


function handleHVWindowClose(){

	var securityId = document.getElementById("SecurityId") ? document.getElementById("SecurityId").value : null;
	//alert("Handling  hv data report response from Innovation:\n" + data.xml);
	if(__homeValueRq.onReturn){
		__homeValueRq.onReturn(__homeValueRq);
	}	
	
}
