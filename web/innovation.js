var hideTimer;
var table = '';
var menuCell = '';
var prefixForInactiveCell = '';
var forceDirty = false;

function HideVisibility(){
    if( '' != table )
        document.getElementById(table).style.visibility = 'hidden';
    if( '' != menuCell ) {
    	var mc = document.getElementById(menuCell) 
    	if( 'mouseOver' == mc.className ) mc.className = prefixForInactiveCell + 'active';
    }
}

function HidePT(PTC){
    if(PTC != null) {	
		var mc = document.getElementById(PTC) 
		if( mc.className == 'mouseOver' ) mc.className = '';
    }    
    hideTimer = setTimeout("HideVisibility();", '25');
}

function HideOtherPT(PT, MID){
	// Hide the other Menu Options
	var optionElems = document.getElementsByTagName('table');
	for( i = 0; i < optionElems.length; i++) {
        if( optionElems[i].id == PT )
            continue;
        var optionId = optionElems[i].id;
        if( optionId.substring(0,3) == PT ) {
            optionElems[i].style.visibility = 'hidden';
        } 
	}
}

function ShowPT(PT, PMC,MID){
	HideVisibility();
    clearTimeout(hideTimer);    
    HideOtherPT(PT, MID);
    if(document.getElementById(PT).style.visibility != 'visible'){
        table = PT;     
        var t  = document.getElementById(table);
        menuCell = PMC; 
        var mc = document.getElementById(menuCell);
        t.style.left = findPosX(mc) + 'px';
        t.style.top = '30px';
        t.style.visibility = 'visible';
   		prefixForInactiveCell = ( 'inactive'==mc.className ? 'in' : '' ); 
   		mc.className = 'mouseOver';
   		// Last resort fix for IE7 menuDropDown double margin problem
		if( document.compatMode && document.all && (!document.documentMode || document.documentMode < 8)) {
	   		var bg = document.getElementById('pageBackground');
	   		var offsetSize = findPosX(mc) - bg.offsetLeft;
	   		t.style.left = offsetSize + 'px';
		}   		
    }
}

function StopHide(PTC){
    if(hideTimer != null)
        clearTimeout(hideTimer);
	document.getElementById(PTC).className = 'mouseOver';
}

function findPosX(obj) {
    var curleft = 0;
    if(obj.offsetParent)
        while(1) 
        {
          curleft += obj.offsetLeft;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.x)
        curleft += obj.x;
    return curleft;
  }

  function findPosY(obj)
  {
    var curtop = 0;
    if(obj.offsetParent)
        while(1)
        {
          curtop += obj.offsetTop;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.y)
        curtop += obj.y;
    return curtop;
  }

function spanMizerId(imgId, tileId){
    if(document.getElementById(tileId).style.display != 'none'){
        document.getElementById(imgId).src="img/arrow-down.gif";
        document.getElementById(tileId).style.display = "none";
        }
    else{
        document.getElementById(imgId).src="img/arrow-right.gif";
        document.getElementById(tileId).style.display="inline";
    }
}

var lineCount = 0;

function toggle(rowNumber) {
    var elementId = 'row' + rowNumber;
    var imageId = 'img' + rowNumber;       

	var elem = document.getElementById(elementId);
	if (elem != null) {
	    if( document.getElementById(elementId).style.display=='none' ){            
	        expand(rowNumber);
	    }else{            
	        collapse(rowNumber);
	    }
	}
}

function expand(rowNumber){
    var elementId = 'row' + rowNumber;
    var imageId = 'img' + rowNumber;       

    if(document.getElementById(elementId))
        document.getElementById(elementId).style.display = '';
    if(document.getElementById(imageId))    
        document.getElementById(imageId).src = 'img/collapse.gif';
}

function collapse(rowNumber){
    var elementId = 'row' + rowNumber;
    var imageId = 'img' + rowNumber;       

    if(document.getElementById(elementId))
        document.getElementById(elementId).style.display = 'none';
    if(document.getElementById(imageId))        
        document.getElementById(imageId).src = 'img/expand.gif';
}

function expandAll(){
    for(var i=0; i < lineCount; i++){
        expand(i);
    }
}

function collapseAll(){
    for(var i=0; i < lineCount; i++){
        collapse(i);
    }
}

function expandRow(id) {
	if( document.getElementById(id) )
    	document.getElementById(id).style.display = '';
}

function collapseRow(id) {
	if( document.getElementById(id) )
    	document.getElementById(id).style.display = 'none';
}

function expandImage(id){
	var ele = document.getElementById(id);
	if (ele) {
		if (ele.onclick != null) {
			ele.click();
	    }	
		ele.src = 'img/collapse.gif';
	}
}

function collapseImage(id){
    document.getElementById(id).src = 'img/expand.gif';
}

function setImageSrc(id, src){
	if( document.getElementById(id) )
	    document.getElementById(id).src = src;
}

function setImageAlt(id, alt){
	if( document.getElementById(id) )
	    document.getElementById(id).alt = alt;
}

function toggleAllId(listId) {

    var elems = document.getElementsByTagName('*');

    var listExp = "row" + listId + "|" + "img" + listId;
    for( i=0; i<elems.length; i++ ) {
        var elemName = elems[i].id;
        if (elemName.search(listExp) == 0 ) {
            if (elemName.substring(0,3) == "row" ) {
                if (document.getElementById(elemName).style.display=='none' ) {
                    expand(elemName.substring(3));
                } else {
                    collapse(elemName.substring(3));
                }
            }
        }
    }

}

// Note that listId must be unique between lists on the same page, else it will expand all lists
function expandAllId(listId) {
    var elems = document.getElementsByTagName('*');

    var listExp = "row" + listId + "|" + "img" + listId;
    for( i=0; i<elems.length; i++ ) {
        var elemName = elems[i].id;
        if (elemName.search(listExp) == 0 ) {
            if (elemName.substring(0,3) == "row" ) 
                expandRow(elemName);
            else
                expandImage(elemName);
        }
    }
}

// Note that listId must be unique between lists on the same page, else it will collapse all lists
function collapseAllId(listId) {

    var elems = document.getElementsByTagName('*');

    var listExp = "row" + listId + "|" + "img" + listId;
    for( i=0; i<elems.length; i++ ) {
        var elemName = elems[i].id;
        if (elemName.search(listExp) == 0 ) {
            if (elemName.substring(0,3) == "row" ) 
                collapseRow(elemName);
            else
                collapseImage(elemName);
        }
    }
}

function notePreview(str){
    str = fixNoteLineBreaks(str);
    return str;
}

function fixNoteLineBreaks(str){
    str = str.replace(/#xD#xA/gi, '<BR>');
    return str;
}

/**
 * The format pattern mapping from Java format to momentjs.
 *
 * @property momentFormatMapping
 * @type {Object}
 */
momentFormatMapping = {
        d: 'D',
        dd: 'DD',
        y: 'YYYY',
        yy: 'YY',
        yyy: 'YYYY',
        yyyy: 'YYYY',
        a: 'a',
        A: 'A',
        M: 'M',
        MM: 'MM',
        MMM: 'MMM',
        MMMM: 'MMMM',
        h: 'h',
        hh: 'hh',
        H: 'H',
        HH: 'HH',
        m: 'm',
        mm: 'mm',
        s: 's',
        ss: 'ss',
        S: 'SSS',
        SS: 'SSS',
        SSS: 'SSS',
        E: 'ddd',
        EE: 'ddd',
        EEE: 'ddd',
        EEEE: 'dddd',
        EEEEE: 'dddd',
        EEEEEE: 'dddd',
        D: 'DDD',
        w: 'W',
        ww: 'WW',
        z: 'ZZ',
        zzzz: 'Z',
        Z: 'ZZ',
        X: 'ZZ',
        XX: 'ZZ',
        XXX: 'Z',
        u: 'E'
      };

/**
 * Translates the java date format String to a momentjs format String.
 *
 * @function translateFormat
 * @param {String}  formatString    The unmodified format string
 * @param {Object}  mapping         The date format mapping object
 * @returns {String}
 */
function translateFormat(formatString, mapping) {
	var i = 0;
	var beginIndex = -1;
	var lastChar = null;
	var currentChar = "";
	var resultString = "";
	var escapeStart = "'";
	var escapeEnd = "'";
	var esc1 = escapeStart.charAt(0);
	var esc2 = escapeEnd.charAt(0);
	var targetEscapeStart = "[";
	var targetEscapeEnd = "]";

	for (; i < formatString.length; i++) {
		currentChar = formatString.charAt(i);
		if(i > 0 && lastChar != currentChar){
			resultString += _appendMappedString(formatString, mapping, beginIndex, i, escapeStart, escapeEnd, targetEscapeStart, targetEscapeEnd);
			beginIndex = i;
		}
		lastChar = currentChar;
		if (currentChar === esc1) {
			i++;
			while (i< formatString.length && formatString.charAt(i) !== esc2) {
				i++;
			}
			resultString += targetEscapeStart;
			resultString += formatString.substring(beginIndex+1, i);
			resultString += targetEscapeEnd;
			i++;
			if (i < formatString.length) {
				lastChar = formatString.charAt(i);
			}
			beginIndex = i;
		}
	}
	if (beginIndex < formatString.length && i <= formatString.length) {
		resultString += _appendMappedString(formatString, mapping, beginIndex, i, escapeStart, escapeEnd, targetEscapeStart, targetEscapeEnd);
		return resultString;
	} else {
		return resultString;
	}
}

/**
 * Checks if the substring is a mapped date format pattern and adds it to the result format String.
 *
 * @function _appendMappedString
 * @param {String}  formatString    The unmodified format String.
 * @param {Object}  mapping         The date format mapping Object.
 * @param {Number}  beginIndex      The begin index of the continuous format characters.
 * @param {Number}  currentIndex    The last index of the continuous format characters.
 * @param {String}  resultString    The result format String.
 * @returns {String}
 * @private
 */

function _appendMappedString(formatString, mapping, beginIndex, currentIndex, escapeStart, escapeEnd, targetEscapeStart, targetEscapeEnd) {
	var tempString;
	var errorString = "";
	tempString = formatString.substring(beginIndex, currentIndex);
	if(tempString == escapeStart || tempString == escapeEnd){
		errorString =  targetEscapeStart;
		errorString += "(error: cannot escape escape characters)";
		errorString += targetEscapeEnd;
		return errorString;
	}
	if(mapping[tempString]){
		var result = mapping[tempString];
		if(result === null || result.length == 0){
			errorString = targetEscapeStart;
			errorString += "(error: ";
			errorString += result;
			errorString += " cannot be converted";
			errorString += targetEscapeEnd;
			return errorString;
		}
		return result;
	}
	return tempString;
}
		
// Format Date
function formatDate(vDateName) {
	// this function assumes one of the following date formats:
	//   MMddyy, MMddyyyy, M/d/y, M/d/yy, M/d/yyyy, MM/d/y, MM/d/yy, MM/d/yyyy, M/dd/y, M/dd/yy, M/dd/yyyy
	//   where the separator in each format above is either "/" or "-"

	//Add locale capability for different countries standard date format
	if (vDateName.value.length == 0) {
		return true;
	}
	if(isGlobalizationLegacyMode){
		// if there are separators, strip them and zero-pad month and day
		if ((vDateName.value.indexOf("-") >= 1) || (vDateName.value.indexOf("/") >= 1)){
			if (vDateName.value.indexOf("-") >= 1) 
				var arrDate = vDateName.value.split("-");
			if (vDateName.value.indexOf("/") >= 1) 
				var arrDate = vDateName.value.split("/");
			var MONTH = (arrDate.length > 0) ? arrDate[0] : "";
			var DAY = (arrDate.length > 1) ? arrDate[1] : "";
			var YEAR = (arrDate.length > 2) ? arrDate[2] : "";
			if (MONTH.length == 1)
				MONTH = "0" + MONTH;
			if (DAY.length == 1)
				DAY = "0" + DAY;
			if (YEAR.length == 1)
				YEAR = "0" + YEAR;
			vDateName.value = MONTH + DAY + YEAR;
		}

		var mMonth = vDateName.value.substr(0,2);
		var mDay = vDateName.value.substr(2,2);
		var mYear = vDateName.value.substr(4,4);        
		var strSeperator = "/";
		var error = false;

		if ((vDateName.value.length < 6 || vDateName.value.indexOf("un") > 0) || (vDateName.value.length == 7 || vDateName.value.length > 8)) {
			error = true;
		}

		if (!error && vDateName.value.length == 6 ) {
			//Turn a two digit year into a 4 digit year
			if (mYear.length == 2) {
				var mToday = new Date();
				//If the year is greater than 30 years from now use 19, otherwise use 20
				var checkYear = mToday.getFullYear() + 30; 
				var mCheckYear = '20' + mYear;
				if (mCheckYear >= checkYear)
					mYear = '19' + mYear;
				else
					mYear = '20' + mYear;
			}
		}

		// check that the components are actually numbers
		if (isNaN(mMonth) || isNaN(mDay) || isNaN(mYear)) {
			error = true;
		}

		// check validity of month and day components
		if (error) {
			// do nothing
		} else if ((mMonth < 1) || (mMonth > 12)) {
			error = true;
		} else if (mDay < 1) {
			error = true;
		} else if (mDay > 31 && (mMonth == 1 || mMonth == 3 || mMonth == 5 || mMonth == 7 || mMonth == 8 || mMonth == 10 || mMonth == 12)) {
			error = true;
		} else if (mDay > 30 && (mMonth == 4 || mMonth == 6 || mMonth == 9 || mMonth == 11)) {
			error = true;
		} else if (mMonth == 2) {
			if (mYear % 400 == 0 || (mYear % 4 == 0 && mYear % 100 != 0)) {
				// leap year
				if (mDay > 29) {
					error = true
				}
			} else {
				// not a leap year
				if (mDay > 28) {
					error = true
				}
			}
		}

		if (error) {
			alert("Invalid Date.\n\nPlease Re-Enter.");
			vDateName.value = "";
			setFocus(vDateName);
			return false;
		}

		// Assemble final reformatted date
		vDateName.value = mMonth+strSeperator+mDay+strSeperator+mYear;
		return true;
	}else{		
		moment.locale(locale);
		moment.updateLocale(locale, {
			longDateFormat:{
				L: translateFormat(dateFormatLocale, momentFormatMapping)
			}
		});
		var format = moment.localeData().longDateFormat('L');
		var localLocale = moment(vDateName.value, format);		
		var d = localLocale.format('L');
		if(d === "Invalid date"){
			alert("Invalid Date.\n\nPlease Re-Enter.");
			vDateName.value = "";
			setFocus(vDateName);
			return false;
		}else{
			vDateName.value = d;		
			return true
		}
	}
}

//Parse Locale specific(by ServiceContext) Date String into Date object
function parseDate(date) {
	moment.locale(locale);
	moment.updateLocale(locale, {
		longDateFormat:{
			L: translateFormat(dateFormatLocale, momentFormatMapping)
		}
	});
	var format = moment.localeData().longDateFormat('L');
	return moment(date, format).toDate();		
}

//Format Currency
//If currencySymbol is not being passed(i.e unglobalized code) then currency symbol will be "$" dollar sign
function formatCurrency(vCurrencyName, currencySymbol) {
	if(typeof currencySymbol == "undefined"){
		currencySymbol = "$";
	}
	num = vCurrencyName.value;
	if( num == "" )
		return;
	var symbol = "\\"+currencySymbol+"|\\,";	
	var symbolRegex = new RegExp(symbol, "g");	
	num = num.toString().replace(symbolRegex,"");

	if( isNaN(num) )
		num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if( cents < 10 )
		cents = "0" + cents;
	for( var i = 0; i < Math.floor((num.length-(1+i))/3); i++ )
		num = num.substring(0,num.length-(4*i+3))+','+ num.substring(num.length-(4*i+3));

	vCurrencyName.value = (((sign)?'':'-') + currencySymbol + num + '.' + cents);
}

//Format Currency Value
//If currencySymbol is not being passed(i.e unglobalized code) then currency symbol will be "$" dollar sign
function formatCurrencyValue(num, centsInd, currencySymbol) {
	if(typeof currencySymbol == "undefined"){
		currencySymbol = "$";
	}
	var symbol = "\\"+currencySymbol+"|\\,";	
	var symbolRegex = new RegExp(symbol, "g");
	num = num.toString().replace(symbolRegex,"");

	if( isNaN(num) || num == "" )
		num = "0";	
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if( cents < 10 )
		cents = "0" + cents;
	for( var i = 0; i < Math.floor((num.length-(1+i))/3); i++ )
		num = num.substring(0,num.length-(4*i+3))+','+ num.substring(num.length-(4*i+3));

	if( centsInd ){
		return (((sign)?'':'-') + currencySymbol + num + '.' + cents);
	}else{
		return (((sign)?'':'-') + currencySymbol + num);
	}
}

//Compare Currency 
//If currencySymbol is not being passed(i.e unglobalized code) then currency symbol will be "$" dollar sign
function compareCurrency(vCurrencyName1, vCurrencyName2, compareOperator, currencySymbol) {
	if(typeof currencySymbol == "undefined"){
		currencySymbol = "$";
	}
	str1 = vCurrencyName1.value;
	str2 = vCurrencyName2.value;
	var symbol = "\\"+currencySymbol+"|\\,";	
	var symbolRegex = new RegExp(symbol, "g");

	str1 = str1.toString().replace(symbolRegex,"");
	str2 = str2.toString().replace(symbolRegex,"");

	num1 = parseFloat(str1);	
	num2 = parseFloat(str2);

	if( compareOperator == "<" ) {
		return ( num1 < num2 );
	} else if( compareOperator == "<=" ) {
		return ( num1 <= num2 );
	} else if( compareOperator == ">" ) {
		return ( num1 > num2 );
	} else if( compareOperator == ">=" ) {
		return ( num1 >= num2 );
	} else if( compareOperator == "==" ) {
		return ( num1 == num2 );
	} else if( compareOperator == "!=" ) {
		return ( num1 != num2 );
	} 

	return false;
}

function copyValue(from, to) {
    document.getElementById(to).value = document.getElementById(from).value;
}
function copyValueIfBlank(from, to) {
    if( document.getElementById(to).value == "" )
        document.getElementById(to).value = document.getElementById(from).value;
}
// Toggles a checkbox's value
function toggleCheckBox(checkboxName){
    if(document.getElementById(checkboxName).checked)
        document.getElementById(checkboxName).value = "Yes";
    else
        document.getElementById(checkboxName).value = "No";            
}

function replaceAll( str, from, to ) {
    var idx = str.indexOf( from );
    while ( idx > -1 ) {
        str = str.replace( from, to ); 
        idx = str.indexOf( from );
    }
    return str;
}

function hide(id){
	if( document.getElementById(id) )
    	document.getElementById(id).style.visibility = 'hidden';
}

function show(id){
	if( document.getElementById(id) )
    	document.getElementById(id).style.visibility = 'visible';
}

function escapeQuotes(value){
    value = replaceAll(value, "'", "&#39;");
    value = replaceAll(value, "\"", "&quot;");
    return value;
}

// Cross-browser function to replace the .focus() method on form elements
// Simply using .focus() doesn't work in FireFox. The following works in all browsers.
// See http://developer.mozilla.org/en/docs/Key-navigable_custom_DHTML_widgets for more info
// Since not all elements support the select() method, that is surrounded by a try/catch
// to eat any exception produced.
function setFocus(elem) {
	if (elem.select) {
		setTimeout(function(){elem.focus();elem.select();}, 1);
	} else {
		setTimeout(function(){elem.focus();}, 1);
	}
}

// Place the focus on the first available text-type field.  If the CurrentFieldId is set then place focus on the 
// next available text-type field after it.  If there is an error, and the ErrorFieldId is set, then place focus 
// on that field.
function placeFocus() {
	main = document.getElementById("Main");
    placeFocusInside(main);
}

//Scrolls the page to a specific location
function scrollToLocation(id){
	if (id && id != ""){
		window.location='#' + id;
	}	
}

//Adds the location where the page should scroll to when next page is loaded. Typically, it is used to set the location for the scroll when returning to the page.
function addScrollToLocation(location){
	addHiddenFormField("ScrollToLocation", location)
}

//Creates a hidden input form field and adds it to frmCMM.
function addHiddenFormField(name, value){
	var frmCMM = document.forms["frmCMM"];
	if (frmCMM){
		var param = document.createElement( "INPUT" );
		param.TYPE = "HIDDEN";
		param.name = name;
		param.value = value;
		frmCMM.appendChild( param );
	}	
}

// Place the focus on the first available text-type field inside the specified element.
// This functions like placeFocus in all other respects
function placeFocusInside(insideElem) {
    if (document.forms.length > 0) {
        var currentFieldId = document.getElementById("CurrentFieldId");
		var errorFieldId = document.getElementById("ErrorFieldId");
		
        var fields = document.forms[0].elements;
        var fieldToFocus = null;
        var isAfterFocus = false;
        for (var i = 0, fieldLength = fields.length; i < fieldLength; i++) {
        	if (insideElem == null || containsElem(insideElem, fields[i])) {
	            if (isFocusableFormElement(fields[i], false)) {
                    if (fieldToFocus == null) {
                        fieldToFocus = fields[i];
                    }
                    if (isAfterFocus == true) {
                        fieldToFocus = fields[i];
                        break;
                    }
	            }
            }
            // Error field set, so just focus on that field
            if (errorFieldId != null && errorFieldId.value == fields[i].name) {
            	fieldToFocus = fields[i];
            	break;
            }
            // Set the field focus on the next field if current field is defined
            if (currentFieldId != null && currentFieldId.value == fields[i].name) {
                isAfterFocus = true;
            }            
        }
        if (fieldToFocus != null) {
        	if (!$(fieldToFocus).hasClass("autocompleteAddress")) {
                // set the focus to the field, but only if its not a field with address auto complete enabled (to avoid a "instant" address auto complete dropdown, which is jarring and confusing to a user)
        		setFocus(fieldToFocus);
        	}
        }
    }
}

// Returns true if the specified elem is a descendant of the specified ancestor
// (i.e. elem is inside ancestorElem)
function containsElem(ancestorElem, elem) {
	var isInside = false;
	if (elem) {
		var parent = elem.parentNode;
		while (parent) {
			if (parent == ancestorElem) {
				isInside = true;
				break;
			}
			parent = parent.parentNode;
		}
	}
	return isInside;
}

// Returns true if the element in question is a "focusable" form element
function isFocusableFormElement(elem, includeButtons) {
    var type = elem.type;
    if ((type == "text") || (type == "textarea") || (type.toString().charAt(0) == "s") || (type == "button" && includeButtons) ) {
        if ((elem.style.display != "none") && (elem.disabled != true) && (elem.readOnly != true) ) {
        	// TODO - cpp - this should check if any parent element is hidden or display none
        	return true;
        }
    }
    return false;
}

// Returns the ids of the first and last tab stop elements inside the given element (e.g. a div)
// The return is an array of the form [firstElemId,lastElemId]
function findFirstAndLastTabStops(insideElem, includeButtons) {
	var focusableElems = new Array();
	var firstStop = null;
	var lastStop = null;
	var lastTabIndex = null;
	var sortByTabIndex = false;
	
	// Gather up all the focusable elements on the form that are inside the given element
    var fields = document.forms[0];
    for (i = 0; i < fields.length; i++) {
    	if (containsElem(insideElem, document.forms[0].elements[i])) {
            if (isFocusableFormElement(fields.elements[i], includeButtons)) {
            	focusableElems[focusableElems.length] = fields.elements[i];
            	if (lastTabIndex) {
            		if (lastTabIndex != fields.elements[i].tabIndex) {
            			sortByTabIndex = true;
            		}
            	}
            	lastTabIndex = fields.elements[i].tabIndex;
            }
        }
    }
//    var debugOut = '';
//    for (i = 0; i < focusableElems.length; i++) {
//    	debugOut += focusableElems[i].id + '[' + focusableElems[i].tabIndex + '],';
//    }
//    alert(debugOut);
    
    // Sort the elements by tabIndex (if necessary)
    if (sortByTabIndex) {
	    // JS Array.sort method in Firefox(2) doesn't seem to preserve existing order for
	    // equal elements (i.e. a.tabIndex == b.tabIndex). It shuffles them. IE is fine.
	    // So if no tabIndexes are used (all the same value), just rely on the order of the
	    // fields as retieved from the form.
    	focusableElems.sort(sortByTabIndex);
    }
//    debugOut = '';
//    for (i = 0; i < focusableElems.length; i++) {
//    	debugOut += focusableElems[i].id + '[' + focusableElems[i].tabIndex + '],';
//    }
//    alert(debugOut);
    
    // get the first and last one
    if (focusableElems.length > 0) {
	    firstStop = focusableElems[0].id;
	    lastStop = focusableElems[focusableElems.length - 1].id;
    }
    
    return [firstStop, lastStop];
}

// array sort function to sort elements by tabIndex
function sortByTabIndex(a, b) {
	aTabIndex = a.tabIndex;
	bTabIndex = b.tabIndex;
	return aTabIndex - bTabIndex;
}

// This version of CMMXForm checks for changed data in the form
// and confirms that the user wished to discard data and continue
// prior to continuing
function cmmXFormSafe(serviceRq, systemId, parentIdRef, idRef, urlParams, showBusy, newWindow ) {
 
 		if(isDirty() && !newWindow ){
			if(window.confirm("Do you wish to leave this screen without saving?")){
				cmmXForm( serviceRq, systemId, parentIdRef, idRef, urlParams, showBusy, newWindow );
			}
		}
		else{
			cmmXForm( serviceRq, systemId, parentIdRef, idRef, urlParams, showBusy, newWindow );		
		}
		

}
 


// Returns TRUE if the form is dirty    	
function isDirty(){
 		
 		if(getChangedFields().length > 0 || forceDirty){
 			return true;
 		}
 		else{
 			return false;
 		}
}

  
 // Returns an array of the changed fields   	
 function getChangedFields(){
 		
 		// Declare the array of changed fields
 		var changedFields = new Array();
 		
 		// Get the metafields
 		var metafields = getMetafields();
 		
 		// Loop through the metafields
 		for(var i=0, metafieldsLength=metafields.length; i<metafieldsLength; i++){	
 				
 			// Get the original value of this field
 			var originalvalue = getTupleValue("OriginalValue", metafields[i].value);
 			
 			if(originalvalue != null){
 						
     			// Get the corresponding field
				var fieldname = getCorrespondingFieldName(metafields[i].name);			
				var field = document.getElementById(fieldname);
				if (field == null) {
					// Double check to obtain Radio Button fields
					var radioFields = document.getElementsByName(fieldname);
					if (radioFields.length > 0) {
						field = radioFields[0];
					}					
				}
				
				if(field != null){					

					var fieldvalue = null;

					// Exit if the field name is undefined				
					if(field.name == undefined){
						continue;					
					}
					
					//if(field.name.indexOf("undef") > 0){
					//	continue;
					//}
					
					// Special processing for checkbox
					if(field.type == "checkbox"){
						if(field.checked){
							fieldvalue = "Yes";
						}
						else{
							fieldvalue = "No";
						}
					}
					else if(field.type == "radio"){
						fieldvalue = getRadioButtonGroupValue(field);
					}
					else if(field.type == "select-multiple"){
						// loop through selected
					    var selectedArray = new Array();
					    var count = 0;
					    for( var j=0; j<field.options.length; j++) {
					    	if( field.options[j].selected ) {
					        	selectedArray[count] = field.options[j].value;
					            count++;
					        }
					    }
					    fieldvalue = selectedArray;    
					}
					else{
						fieldvalue = field.value;
					}													
										
					// Filter some changes					
					if(originalvalue != fieldvalue){
						if(field.type.indexOf("select") > -1){
							if(originalvalue == "0" && fieldvalue == ""){
								originalvalue = fieldvalue;
							}		
						}
						else if(field.type == "checkbox"	){
							if(originalvalue == "" && fieldvalue == "No"){
								originalvalue = fieldvalue;
							}							
						}
						else if(field.type == "radio" ){
							if(originalvalue.toUpperCase() == fieldvalue.toUpperCase()){
								fieldvalue = originalvalue;
							} 
						}
					}
					
					// Add the change to the changes list
					// TODO : Currently, this will falsely detect a change of a multiline
					//        textarea (i.e. that contains newlines or carriage-returns)
					//        where the original value was created in one browser family (e.g.
					//        IE or Firefox) and the field is being displayed in the other
					//        browser family (e.g. Firefox or IE, repsectively). This is because
					//        IE writes a \r\n combination when the Enter key is pressed in a
					//        textarea and Firefox writes just a \n (and converts \r\n to \n in
					//        textarea values even if the user doesn't edit it).
					//        Since most companies stick to one browser family or another,
					//        we'll ignore this problem for now.
					if(originalvalue != fieldvalue){						
						console.log("Field '" + fieldname + "' original value='" + originalvalue + "' current value='" + fieldvalue + "'");						
						changedFields.push(field);
					}
				}			
			}
			
		}
		
		return changedFields;
 		
}
    
// Returns an array of the metafields in the form
function getMetafields(){
     	
 		// Metafields array
 		var metafields = new Array();
 		
 		// Loop through all the forms elements
 		var cmmForm = document.getElementById("frmCMM");
        var elements = cmmForm.elements;
 		
 		for(var i=0, elementsLength=elements.length; i<elementsLength; i++){
 		
 		
 			var element = elements[i];
 			
 			// If this is a hidden field
 			if(element.type == "hidden"){
 			
 				// If the hidden field starts with "METAFIELD"
 				if(element.name.indexOf("METAFIELD") == 0){	
 				
 					// Add this to the array		
 					metafields.push(element);
 				}
 			}		
 		}    
 		
 		// Return the metafields array
 		return metafields; 	
}
     	
// Returns a tuple's value 	     	
function getTupleValue(tuplename, tuples){
 		
		// Replace any escaped backslash so as not to mistake it for an escaped delimiter later
		var escapedBackslash = "@$^*)(&%#!!#%&()*^$@";
		while(tuples.indexOf("\\\\") > -1){
			var index = tuples.indexOf("\\\\");
			tuples = tuples.substring(0, index) + escapedBackslash + tuples.substring(index + 2);
		}
	
		// Replace any escaped delimiter so as not to mistake it for a real delimiter
		var escapedDelimiter =  "!@#$%^&*())(*&^%$#@!";
		while(tuples.indexOf("\\|") > -1){
			var index = tuples.indexOf("\\|");
			tuples = tuples.substring(0, index) + escapedDelimiter + tuples.substring(index + 2);
		}
 		
 		// Split the string into token
 		var tokens = tuples.split("|");
 		
 		// Loop through the tokens
 		for(var i=0; i<tokens.length; i++){
 			
 			// If the token has an "=" sign

 			//MT 9011: Save Dialog Box Pops Up even after the changes where saved (if the changes contained "=" equals sign, 
 			//so instead of splitting at every equal sign, split at the first occurrence.
 			
 			var equalIndex = tokens[i].indexOf("=");
 			if(equalIndex > 0){
 			
 				// Split this token into two tokens
 				 var tupletokens = [ tokens[i].substr(0, equalIndex), tokens[i].substr(equalIndex+1) ];
 				
 				// If this token matches the tuple name parameter
 				if(tuplename == tupletokens[0]){
 				
 					// Restore any escaped delimiters
 					while(tupletokens[1].indexOf(escapedDelimiter) > -1){
 						var index = tupletokens[1].indexOf(escapedDelimiter);
 						tupletokens[1] = tupletokens[1].substring(0, index) + "|" + tupletokens[1].substring(index + escapedDelimiter.length);
 					}
 					// Restore any escaped backslashes
 					while(tupletokens[1].indexOf(escapedBackslash) > -1){
 						var index = tupletokens[1].indexOf(escapedBackslash);
 						tupletokens[1] = tupletokens[1].substring(0, index) + "\\" + tupletokens[1].substring(index + escapedBackslash.length);
 					}
 					
 					// Return the value
					return tupletokens[1];  
					
					 			     				
 				}
 			}     							     			     			
 		}
 		
 		// Return null if the tuple name isn't found in the tuples string
 		return null;	
}
     
// Returns the corresponding field's name
function getCorrespondingFieldName(metafieldName){
		
		// Extract the field name
		var fieldName = metafieldName.substring(11, metafieldName.length - 5);
		
		// Replace three underscores with a period
		// Use split+join instead of replace for speed (since we don't need the regex power of replace)
		var fieldNameSplit = fieldName.split("___");
		if(fieldNameSplit.length > 1) {
			fieldName = fieldNameSplit.join(".");
		}
		//fieldName = fieldName.replace(/___/, ".");
		
		// Return the field name
		return fieldName;
		
    		
} 	

// prepareReplyXml() is called on numerous tiles before cmmXForm() to do any
// pre-submit preparation of form data. All a tile has to do to add a function
// is call registerPrepareReplyXml() with its function and it will be called
// before cmmXForm() as part of the prepareReplyXml() process.

var prepareReplyXmlFunc = null;

// prepareReplyXml dispatch function.
// This is the only function tiles/forms need to call before cmmXForm(). It will
// call any registered tile-specific prepareReplyXml() functions.
function prepareReplyXml() {
	if (prepareReplyXmlFunc) {
		prepareReplyXmlFunc();
	}
}

// Tiles should call this to register a new tile-specific prepareReplyXml() function
// to be called by the main prepareReplyXml() dispatch function.
function registerPrepareReplyXml(func) {
	if (prepareReplyXmlFunc) {
		var oldPrepareReplyXmlFunc = prepareReplyXmlFunc;
		prepareReplyXmlFunc = function() {
				if (oldPrepareReplyXmlFunc) {
					oldPrepareReplyXmlFunc();
				}
				func();
			}
	} else {
		prepareReplyXmlFunc = func;
	}
}

// Placeholder for loadProvider, function is meant to be overridden locally
function loadProvider(){

}

// Get the corresponding select option text for the select option value 
function getOptionText(id) {
	var value = document.getElementById(id).value;
	if( value == "" )
		return "";
	
	if( document.getElementById(id).options ) {	
		var length = document.getElementById(id).options.length;
		for( i = 0; i < length; i++ ) {
			if( document.getElementById(id).options[i].value == value )
				return document.getElementById(id).options[i].text;
		}
	}
}

// Add new option to the select list
function addOption(id, text, value) {
	if( document.getElementById(id).options ) {	
		var length = document.getElementById(id).options.length;
		document.getElementById(id).options[length] = new Option(text, value);
	}	
}

// Field enabler/disabler 
// var optId is id of the option field that triggers the box.
// var optValue is set to the options value that triggers the the box.
// var inputId id of the input box to be enabled/disabled.
function optCheckControl(optId, optValue, inputId){
    if( document.getElementById(inputId) ) {
    	var optValueList = optValue.split("|");
    	var isInList = false;
	    for( x in optValueList ) {
	    	if(document.getElementById(optId).value == optValueList[x]) {
	    		isInList = true;
	    	}	    	
    	}
        if(!isInList){
            document.getElementById(inputId).disabled = true;
            document.getElementById(inputId).value = "";
        } else {
            document.getElementById(inputId).disabled = false;
        }    
    }
}

//Since trim() is not supported in IE8, below is a simple function to overcome that problem
	if(typeof String.prototype.trim !== 'function') {
		String.prototype.trim = function() {
			return this.replace(/^\s+|\s+$/g, '');
		}
	}
	
// Trim
// var string is the string to trim of spaces
function trim(string) {
	return string.replace(/^\s+|\s+$/, '');
		
}

// Add Load Event
// func string is the function to add to the window load event
function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      if (oldonload) {
        oldonload();
      }
      func();
    }
  }
}

//Round Value to Scale Specified
//If currencySymbol is not being passed(i.e unglobalized code) then currency symbol will be "$" dollar sign
function roundToScale(fieldId, scale, currencySymbol) {
	if(typeof currencySymbol == "undefined"){
		currencySymbol = "$";
	}
	value = document.getElementById(fieldId).value;	

	var symbol = "\\"+currencySymbol+"|\\,";	
	var symbolRegex = new RegExp(symbol, "g");	
	value = value.toString().replace(symbolRegex,"");

	if( !isNaN(value) ) {
		document.getElementById(fieldId).value = Math.round(value / scale) * scale;
	}
}

//Round Value up to Scale Specified
function roundUpToScale(fieldId, scale, currencySymbol) {
	if(typeof currencySymbol == "undefined"){
		currencySymbol = "$";
	}
	value = document.getElementById(fieldId).value;
	var symbol = "\\"+currencySymbol+"|\\,";	
	var symbolRegex = new RegExp(symbol, "g");
	value = value.toString().replace(symbolRegex,"");
	if( !isNaN(value) ) {
		document.getElementById(fieldId).value = Math.ceil(value / scale) * scale;
	}
}

// Set Current Field
function setCurrentFieldId(fieldObj) {
	document.getElementById("CurrentFieldId").value = fieldObj.name;
}

// Checks if string is numeric
function isNumeric(sText)
{
   var validChars = "0123456789.";
   var isNumber=true;
   var ch;

 
   for (i = 0; i < sText.length && isNumber == true; i++) 
      { 
      ch = sText.charAt(i); 
      if (validChars.indexOf(ch) == -1) 
         {
         isNumber = false;
         }
      }
   return isNumber;
   
}

// Select All Checkboxes
function selectAll(idPrefix) {
	// Loop through each field in the form
   	var xform = document.forms["frmCMM"];          
   	for( i = 0; i < xform.elements.length; i++ ){
		var field = xform.elements[i];
	   	if( field.name.indexOf(idPrefix) == 0 ) {
			document.getElementById(field.name).checked = true;
        	document.getElementById(field.name).value = "Yes";
	    }    
   	}  
}

// Unselect All Checkboxes
function unselectAll(idPrefix) {
	// Loop through each field in the form
   	var xform = document.forms["frmCMM"];          
   	for( i = 0; i < xform.elements.length; i++ ){
		var field = xform.elements[i];
	   	if( field.name.indexOf(idPrefix) == 0 ) {
			document.getElementById(field.name).checked = false;
        	document.getElementById(field.name).value = "No";
	    }    
   	}  
}   

// Builds a URL queryString (HTTP GET) from component parameters
// This can be used to avoid embedding special characters like & in javascript
// which some browsers treat differently.
// This takes a variable number of parameters. The first is the base url (e.g. innovation)
// while the remainder are pairs of querystring parameters (e.g. "file", "filename"...)
function buildURLqueryString(baseUrl) {
    if (arguments.length % 2 != 1) {
        throw new InternalException("buildURLqueryString: Odd number of QueryString parameters (they must come in pairs)."); 
    }
    var url = baseUrl;
    if (arguments.length > 1) {
        url = url + "?" + encodeURIComponent(arguments[1]) + "=" + encodeURIComponent(arguments[2]);
        for (i = 3; i < arguments.length; i=i+2) {
            url = url + "&" + encodeURIComponent(arguments[i]) + "=" + encodeURIComponent(arguments[i+1]);
        }
    }
    return url;
}

// Toggle the Display of the Combined Address
function toggleCombinedAddr() {
	if( document.getElementById("CombinedAddr").style.display=='none' ){            
        document.getElementById("CombinedAddr").style.display = '';
        document.getElementById("CombinedAddrButton").value = 'Hide Combined Address';
    }else{            
        document.getElementById("CombinedAddr").style.display = 'none';
        document.getElementById("CombinedAddrButton").value = 'Show Combined Address';
    }
}

// Set the Margin Top on the Main Div Below the FixedHeader
function setMainMarginTop(resetHeight) {
	if( document.getElementById("FixedHeader") ) {
		var height = document.getElementById("FixedHeader").offsetHeight + "px";
		document.getElementById("Main").style.marginTop = height;
		
		// Safari doesn't properly read the height attribute, so you have to reset the height after the document is loaded
		if( resetHeight )
			document.getElementById("FixedHeader").style.height = height;
	}
}

// This version of CMMXForm checks for changed data in the form
// and confirms that the user wished to discard data and continue
// prior to continuing. 
// It also passes blank Container, ModelName, and Operation parameters 
// to clear out the form variables. 
function cmmXFormSiteMenuSafe(serviceRq, systemId, parentIdRef, idRef, urlParams, showBusy ) {
 		
 		if( urlParams ) {
 			urlParams = urlParams + "&Container=&ModelName=&Operation=&TaskSystemId=";
 		} else {
 			urlParams = "Container=&ModelName=&Operation=&TaskSystemId=";
 		}
 		if( isDirty() ) {
			if( window.confirm("Do you wish to leave this screen without saving?") ) {
				cmmXForm( serviceRq, systemId, parentIdRef, idRef, urlParams, showBusy );
			}
		} else {
			cmmXForm( serviceRq, systemId, parentIdRef, idRef, urlParams, showBusy );		
		}
}

function createCookie(name, value, days) {
	if( days ) {
		var date = new Date();
		date.setTime(date.getTime() + (days*24*60*60*1000));
		var expires = "; expires=" + date.toGMTString();
  	}
  	else expires = "";
  	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEquals = name + "=";
	var cookies = document.cookie.split(';');
	for( var i = 0; i < cookies.length; i++) {
		var cookie = cookies[i];
    	while( cookie.charAt(0) == ' ' ) 
    		cookie = cookie.substring(1, cookie.length);
    	if( cookie.indexOf(nameEquals) == 0 ) 
    		return cookie.substring(nameEquals.length, cookie.length);
  	}
	return null;
}

function deleteCookie(name) {
	if( readCookie(name) ) 
		createCookie(name, "", -1);
}

// Get Currency Value
function getCurrencyValue(vCurrencyName, currencySymbol) {
	if( 'string' == typeof vCurrencyName || 'number' == typeof vCurrencyName) {      
		num = vCurrencyName;
	} else {                               
		try { num = vCurrencyName.value; }
		catch(e) { num = '0'; }
	}
	if(typeof currencySymbol == "undefined"){
		currencySymbol = "$";
	}
	var symbol = "\\"+currencySymbol+"|\\,";	
	var symbolRegex = new RegExp(symbol, "g");
	
	num = num.toString().replace(symbolRegex,"");
	if( isNaN(num) || num == "" )
		num = "0";	
	return parseFloat(num);	
}
     
// Format Percent Value
function formatPercentValue(num, decimals) {

	// Adjust Number to Decimals Specified 
	num *= Math.pow(10, decimals);
	num = Math.round(Math.abs(num))
	num /= Math.pow(10, decimals);
	
	// Format Number
	str = num.toString();
	if( str.indexOf(".") == -1 ) {
		for( var i = 0; i < decimals; i++ ) {
			if( i == 0 )
				str += ".0";
			else	
				str += "0";
		}
	}
	return str + "%";
}

// Strip Number Format Characters
function stripNumberFormatChars(value) {
	value = value.toString().replace(/\$|\,|\%/g,'');
	
	return value;
}

// Checks if a Value is True (i.e. True, Yes)
function isTrue(value) {
	
	// If Value is Blank
	if( value == null ) {
		return false;
	} else if( value.length == 0 ) {
		return false;
	} 
	
	// Uppercase Value so We Only Need to Check One Case
	value = value.toUpperCase();
	
	// If Value is No
	if( value == "N" ) {
		return false;
	} else if ( value == "NO" ) {
		return false;
	}
	
	// If Value is False	
	if( value == "F" ) {
		return false;
	} else if( value == "FALSE" ) {
		return false;
	} else if( value == "0" ) {
		return false;
	}
	
	// Otherwise True
	return true;
	
}

// Case insensitive way to set the value of a select box
function setSelectedItem(selectBoxId, value){
	
	var list = document.getElementById(selectBoxId);
	
	if(list && value){
		for(var i = 0; i < list.options.length; ++i){
			if(list.options[i].value.toUpperCase() == value.toUpperCase()){
				list.value = list.options[i].value;
			}
		}
	}

}

// Get the displayed text value of a select box.
// If the value is blank, a blank will be returned regardless of the text.
function selectListText(selectBoxId){

	var w = document.getElementById(selectBoxId).selectedIndex;
	var value = document.getElementById(selectBoxId).options[w].value;
	if (value == "") {
		return "";
	} else {
		return document.getElementById(selectBoxId).options[w].text;
	}

}

// Calculate the Number of Years Between Two Dates
function calculateYearDiff(date1, date2) {
	startDt = new Date(date1);
	endDt = new Date(date2);
	years = Math.floor((endDt.getTime() - startDt.getTime()) / (365.25 * 24 * 60 * 60 * 1000));
	return years;
}


// Track a page view in analytics
function analyticsTrackPageview(opt_pageURL) {
    try {
        var pageName = "/innovation/" + opt_pageURL;
        _gaq.push(['_trackPageview', pageName]);
    } catch(err) {}
}

// Track an event in analytics
function analyticsTrackEvent(category, action, opt_label, opt_value) {
    try {
        _gaq.push(['_trackEvent', category, action, opt_label, opt_value]);
    } catch(err) {}
}

function maskValue( value,  maskChar, visibleChars, maskLeft, ignoreChar ){

	// Mask the string
	var masked = "";
	
	for(i=0; i<value.length; i++){
		
		// Get the current character in the string
		var currentChar = value.charAt(i);
		
		// Determine whether or not to mask it
		var mask = maskLeft ? i < value.length - visibleChars : i > visibleChars - 1;
		
		// Determine whether to ignore this character
		if(mask && ignoreChar.length > 0 && ignoreChar.charAt(0) == currentChar){
			mask = false;
		}
		
		// Append the appropriate masked or unmasked character 
		masked += ( mask ? maskChar : currentChar );
	}
	
	return masked;
	
}


// Create a masked field control group
function createMaskedControlGroup( primaryFieldId, helperFieldId, maskChar, visibleChars, maskLeft, ignoreChar ){

	var primaryField = document.getElementById(primaryFieldId);
	if(!primaryField){
		alert("Primary field " + primaryFieldId + " not found!");
	}

	var helperField = document.getElementById(helperFieldId);
	if(!helperFieldId){
		alert("Secondary field " + helperFieldId + " not found!");
	}
	
	primaryField.onfocus = function() {
		primaryField.select();
		return true; 
	};
	
	primaryField.onchange = function() { 
		helperField.value = primaryField.value;
		primaryField.value = maskValue( primaryField.value, maskChar, visibleChars, maskLeft, ignoreChar );
		return true; 
	};
	
	
}

// Select All Elements Between
function getAllBetween(firstEl,lastEl) {
    var firstElement = $(firstEl); // First Element
    var lastElement = $(lastEl); // Last Element
    var collection = new Array(); // Collection of Elements
    $(firstEl).nextAll().each(function(){ // Traverse all siblings
        var siblingID  = $(this).attr("id"); // Get Sibling ID
        if (siblingID != $(lastElement).attr("id")) { // If Sib is not LastElement
        	collection.push($(this)); // Add Sibling to Collection
        } else { // Else, if Sib is LastElement
            return false; // Break Loop
        }
    });         
    return collection; // Return Collection
}

// Toggle Dialog Box
function toggleDialogBox(dialogId, imageId, counter, width) {
	var dialog = "#" + dialogId + counter;
	var image = "#" + imageId + counter;
	
	if( $(image).attr("src") == "img/expand.gif" ) {
		
		// Open Dialog
		$(image).attr("src", "img/collapse.gif");
		
		$(dialog).dialog({ 
			position: { 
				my: "left top",
			    at: "right bottom",
    		    of: $(image)
    		},
    		beforeClose: function(event, ui) { return false; },
    		autoOpen: false,
    		height: 0,
    		width: width,
    		resizable: false
		 });	
		$(dialog).css({'background-color': '#FFFFFF'});
		$(dialog).closest(".ui-dialog").css({'border-color': '#FAAB19', 'background': 'none', 'background-color': '#FFFFFF'});
    	$(dialog).closest(".ui-dialog").find(".ui-dialog-titlebar").hide();
		$(dialog).dialog("open");
		
		// Collapse Other Dialogs
		$('tr[id*="' + dialogId + '"]:visible').each(
	   		function(i) {
	   			var id = this.id;
	   			var hideDialogCounter = id.replace(dialogId, '');
	   			if( hideDialogCounter != counter ) {
	   				$("#" + dialogId + hideDialogCounter).dialog({ 
						beforeClose: function(event, ui) { return true; } 
					});
	   				$("#" + dialogId + hideDialogCounter).dialog("close");
					
					if( document.getElementById(imageId + hideDialogCounter) )        
				        document.getElementById(imageId + hideDialogCounter).src = 'img/expand.gif';
	   			}
	   		}
	   	);    	
		
		
	} else if( $(image).attr("src") == "img/collapse.gif" ) {
		$(image).attr("src", "img/expand.gif");
		
		$(dialog).dialog({ 
			beforeClose: function(event, ui) { return true; } 
		});	
		$(dialog).dialog("close");
	}
}

//creates the proper 'meta name' depending upon presence of '.' 
function makeFieldMetaName(field) {
	if(field.indexOf('.') > -1) 
		return field.replace( /(\w+)\.(\w+)/gi, "METAFIELD__$1___$2__EXT");
	else 
		return "METAFIELD__"+field+"__EXT";
}
//gets a value from the meta field 
function getFieldMeta(field, key) {
	var meta = makeFieldMetaName(field);
	//console.log("makeFieldMetaName = '"+meta+"'");
	var element = $('[id=\"'+ meta + '\"]');

	if( !element.length ) return null;
	
	var val = element.val();
	if (val.trim().length == 0) return null;
	
	var rg = new RegExp(key+"=([^|$]*)","gi");
	var i;
	if( i = rg.exec(val) ) {
		return i[1];
	}
	return null;
}
//sets a value on a field's hidden 'meta field' based upon name and key
//does not modify if key is already equal to passed in value
function setFieldOption(field, key, value) {
	if(getFieldMeta(field,key) === value) return;
	var meta = makeFieldMetaName(field);
	//console.log("makeFieldMetaName = '"+meta+"'");
	var element = $('[id=\"'+ meta + '\"]');
	//console.log('prior:  ' + meta + "='"+ element.val() + "'");
	var reKeyVal = new RegExp(key+"=[^|]*[\|]","gi");
	if( reKeyVal.test(element.val())) {
    element.val(function(i,v){return v.replace(reKeyVal,key+"="+value+"|")});
	} else {
		element.val(function(i,v){return v+'|'+key+"="+value+"|";});
	}
	//console.log('after:  ' + meta + "='"+ element.val() + "'");
}

//helper function set multiple required fields (comma-separated) or single required field
function setRequired(field, when) {
	if( field.indexOf(',') > -1 ){
		var fields = field.split(',');
		for( var i = 0; i < fields.length; i++ )
			setRequiredIndividually(fields[i].trim(), when);
	} else {
		setRequiredIndividually(field, when);
	}
}

//helper function to set meta value to required and insert '*' on label field
function setRequiredIndividually(field, when) {
	var meta = makeFieldMetaName(field);
	if(when === true) {
		setFieldOption(field,"Required", "True");
		$('.label label[for=\"'+ field + '\"]').text(function(i,t){return t.replace(/(\*)*$/,"")+"*"});
		//console.log("required is True and field '"+field+"' text = '"+$('.label label[for=\"'+ field + '\"]').text()+"'");
	} else {
		setFieldOption(field,"Required", "False");
		$('.label label[for=\"'+ field + '\"]').text(function(i,t){return t.replace(/(\*)*$/,"")});
		//console.log("required is False and field '"+field+"' text = '"+$('.label label[for=\"'+ field + '\"]').text()+"'");
	} 
}

// Convert a Java Locale to the jquery Datepicker locale. The Datepicker uses the hyphen instead of underscore for separation of country and language in the locale
// Additionally it only supports certain regional codes in the ICU locale, so most languages will just have the language portion and not the country portion.
function getJqueryUII18nLocale(locale) {
	locale = locale.replace('_', '-');
	var regions = jQuery.datepicker.regional;

	// If it does not support the language and region, then just see if it supports thed language only
	if (!regions.hasOwnProperty(locale)) {		
    	locale = locale.substring(0, locale.indexOf('-'));
    }
	if (!regions.hasOwnProperty(locale)) {
		// Default to English US (en_US) if it doesn't seem to support the language at all
    	locale = "";
    }
    return locale;
}

// Convert a Java Date Format to the jquery Datepicker equivalent.
function getJqueryDateFormat(javaFormat) {
	var formatMap = {'d':'d', 'D':'o', 'F':'', 'E':'', 'u':'', 'M':'m', 'w':'', 'W':'', 'y':'y', 'Y':'', 'G':'', 'a':'', 'H':'', 'k':'', 'K':'', 'h':'', 'm':'', 's':'', 'S':'', 'z':'', 'Z':'', 'X':''};
	
	var jqueryFormat = '';
	for (i=0; i<javaFormat.length; i++) {
		var char = javaFormat[i];
		if (formatMap[char]) {
			var pickerChar = formatMap[char];
			jqueryFormat += pickerChar;
		} else {
			jqueryFormat += char;
		}
	}	
	return jqueryFormat;
}

function asYouTypePhoneNumberPreventKeys(e) {
	//get keycode pressed
	if(e) {            
		keynum = e.keyCode;
    } else if(e.which){                   
    	keynum = e.which;
    }
	if (keynum == 32 || keynum == 41 || keynum == 40 || keynum == 45) {
		//don't allow '(' ')' '-' ' ' to be pressed
		e.preventDefault();
		return false;
	}
}

// As-you-type formatting for phone number input fields
function asYouTypePhoneNumber(phoneNumber, defaultCountry, inputField, e) {
	var oldValue = phoneNumber;
	var start = inputField.selectionStart;
	
	//"import" library
	var libphonenumber = window['libphonenumber-js'];
	// initialize formatter with default country
	var formatter;
	if (defaultCountry === "ZZ") {
		formatter = new libphonenumber.as_you_type();
		if (!phoneNumber.includes("+")) {
			phoneNumber = "+" + phoneNumber;
		}
	} else {
		var formatter = new libphonenumber.as_you_type(defaultCountry);
	}
	
	//check which key was pressed
	if(e) {                   
		keynum = e.keyCode;
    } else if(e.which){                
    	keynum = e.which;
    }
	if (keynum == 8 || ( keynum >= 37 && keynum <=40 )) {
		//do nothing, arrow keys or backspace pressed
		return;
	} else if ( /[a-z#]/.test(phoneNumber.toLowerCase()) ) {
		//check if a letter or '#' exists in the phone number string (treat these chars as extension prefixes)
		//If so, no longer format the string as formatting the input now will just remove these characters
		return;
	}
	
	//format the number
	var out = formatter.input(phoneNumber)
	var end = inputField.selectionEnd + out.length - phoneNumber.length;
	
	//trim whitespace on edge
	out = out.trim();
	
	//set new value
	inputField.value = out;
	
	//calculate new cursor postion if needed
	if (oldValue.length < out.length) {
		start = start + (out.length - oldValue.length);
	}
	if (oldValue.length > out.length + 1 ) {
		start = start - (out.length - oldValue.length);
	}
	//set cursor position
	inputField.selectionStart = start;
	inputField.selectionEnd = end < start ? start : end;
}

//Since startsWith does not work on IE11 this is a polyfill to overcome this problem
	if (!String.prototype.startsWith) {
		String.prototype.startsWith = function(searchString, position) {
			position = position || 0;
			return this.indexOf(searchString, position) === position;
		};
	}

	//This function supports the Birt Report call with POST to hide parameters being passed.
	function windowOpenUsingPOST(path, params) {
	    method = "post"; // Set method to post by default.	    	    
	    var form = document.createElement("form");
	    form.setAttribute("method", method);
	    form.setAttribute("action", path);
	    form.setAttribute("target", "GenericBrowse");
	    for(var key in params) {
	        if(params.hasOwnProperty(key)) {
	            var hiddenField = document.createElement("input");
	            hiddenField.setAttribute("type", "hidden");
	            hiddenField.setAttribute("name", key);
	            hiddenField.setAttribute("value", params[key]);
	            form.appendChild(hiddenField);
	         }
	    }
	    document.body.appendChild(form);
	    
	    newWindow = window.open("", "GenericBrowse","height=500,width=890,left=200,top=200,scrollbars=yes,resizable=yes");
	    
	    if (newWindow) {
	    	form.submit();
	    } else {
	        alert('You must allow popups for this report to work.');
	    }
	    setTimeout(function(){newWindow.focus();}, 1000);
	}
	
	function windowOpenBirtReportUsingPOST(reportName, params) {
		analyticsTrackPageview("report/" + reportName);
		params["__report"] = reportName;
		params["__showtitle"] = false;
		params["__title"] = "InsuranceNow Reports";
		windowOpenUsingPOST("birt-viewer/frameset", params);
	}
	
	/*
	creates the cartesian product of any number of passed in arrays
	and exposes them as an iterable set via the do function
	i.e.:  if two passed arrays are:  
	[["red","green",blue"], [1,2,3]]
	The resulting iterations will be:
	[red,1],[red,2],[red,3],[green,1],[green,2],[green3],[blue,1],[blue,2],[blue,3]
	the number of passed in arrays will dictate the number of parameters populated
	by the 'do' callback iterator, so the in this case the 'do' function 
	would look like:  .do(function(color, number)) {
	A full example:
			var colorNumbers = crossProduct([["red","green",blue"], [1,2,3]]);
			
			while (colorNumbers.next()) {
				colorNumbers.do(function(color, number){
					console.log("color = " + color + " and number = " + number);
				}
			}
	(this could be much cleaner as a generator but this approach is compatible 
	with IE)
	*/
	function crossProduct(sets) {
		var n = sets.length,
			carets = [],
			args = [];

		function init() {
			for (var i = 0; i < n; i++) {
				carets[i] = 0;
				args[i] = sets[i][0];
			}
		}

		function next() {
			if (!args.length) {
				init();
				return true;
			}
			var i = n - 1;
			carets[i]++;
			if (carets[i] < sets[i].length) {
				args[i] = sets[i][carets[i]];
				return true;
			}
			while (carets[i] >= sets[i].length) {
				if (i == 0) {
					return false;
				}
				carets[i] = 0;
				args[i] = sets[i][0];
				carets[--i]++;
			}
			args[i] = sets[i][carets[i]];
			return true;
		}

		return {
			next: next,
			do: function (block, _context) {
				return block.apply(_context, args);
			}
		}
	}
	
	//checks to see if a 'key' event is the enter key
	//but not being pressed with in a 'textbox' field
	function checkEnter(e){
		 e = e || event;
		 var txtArea = /textarea/i.test((e.target || e.srcElement).tagName);
		 return txtArea || (e.keyCode || e.which || e.charCode || 0) !== 13;
	}	
	
	function tagNameWithId(node) { 
		return node.hasAttribute("id") ? node.tagName+"[id='"+node.getAttribute("id")+"']" : node.tagName; 
	}

	function pathToNode(node) {
		return $(node).parents().map(function() {return tagNameWithId(this);}).get().reverse().join("/")+"/"+tagNameWithId(node);
	}

	//capture all key presses
	$(document).keypress(function(e){
		//is this the enter key in a context we concerned about?
		if (!checkEnter()) {
			//stop form submission
			e.preventDefault();
			
			var attrToSearch = ['href','onclick'];
			var jquerySelectorsToSearch = ['a#Save','button#Save','#Save',":contains('Save')"]

			//iterate of cartesian product of possible 'save' action locations
			var variations = crossProduct([attrToSearch, jquerySelectorsToSearch]);
			
			var done = false;
			while (!done && variations.next()) {
				variations.do(function(attr, selector){
					var nodes = $(selector+"["+attr+"*='cmmXForm']");
					//if we find a matching node, trigger that event on that node
					if(nodes.length > 0) {
						var node = nodes[0];
						console.log("Found 'Save' action at: "+pathToNode(node));
						console.log("Save action = "+node.getAttribute(attr));
						$(node).trigger(attr);
						done = true;
					}
				});	
			}
		}
	});