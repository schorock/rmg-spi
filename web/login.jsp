<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="net.inov.biz.server.ServiceContext"%>
<%@page import="net.inov.tec.security.authenticator.AuthenticationAlertCache"%>
<%@page import="net.inov.tec.security.authenticator.AuthenticationAlert"%>
<%@page import="net.inov.tec.security.SecurityManager"%>
<%@page import="com.iscs.common.render.VelocityTools" %>
<%@page import="com.iscs.common.tech.log.Log" %>
<%@page import="com.iscs.common.tech.web.servlet.ServletServiceContextInitializer"%>
<%@page import="com.iscs.interfaces.saml.handler.SAMLRequestInitiator" %>
<%@page import="com.iscs.interfaces.saml.handler.SAMLSettings" %>
<%@page import="com.iscs.interfaces.saml.handler.SAMLSecurity" %>
<%@page import="com.iscs.interfaces.saml.handler.SAMLServlet" %>
<%@page import="net.inov.tec.web.webpath.renderer.TileSetRenderer" %>
<%@page import="com.iscs.interfaces.mfa.MFASettings" %>
<%@page import="com.iscs.interfaces.mfa.MFASecurity" %>
<%@page import="com.iscs.interfaces.mfa.MFAAuthentication" %>
<%@page import="com.iscs.interfaces.mfa.MFATools" %>
<%@page import="com.iscs.common.utility.StringTools" %>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Arrays"%>
<%@page import="javax.servlet.http.Cookie"%>

<%@page contentType="text/html; charset=utf-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
// Redirect to the main application page for a successful login following the HTTP 400 direct reference issue
String referrer = request.getHeader("Referer");
if (referrer != null && referrer.endsWith("j_security_check")) {
	response.sendRedirect("/innovation?rq=COUserStartPage");
}

// Initialize a ServiceContext
ServletServiceContextInitializer ctxInitializer = new ServletServiceContextInitializer();
try {
	ServiceContext context = ServiceContext.getServiceContext();
	ctxInitializer = new ServletServiceContextInitializer();
    Exception initializerException = null;
    try {
		ctxInitializer.initialize(new Object[] {request, response});
    } catch (Exception e) {
    	// Swallow any exception since the login page knows how to deal with system unavailability issues
    	// Save it for logging once we start the thread log, though
    	initializerException = e;
    }    
    context.getLogContext().setLogContextAttribute("RequestType", "WEB");
    context.getLogContext().setLogContextAttribute("RequestName", "Login");
    
    Log.start(ServiceContext.getServiceContext().getLogfile());
    // Start a stopwatch for the request response time. This can be picked up later in a rendering phase (e.g. analytics-tracking-body-end.html)
    Log.startStopwatch("responseTime");
    
    if (initializerException != null) {
    	// We had an exception during service context initialization. Log it now that we've started the thread log
    	Log.error("Exception initializing service context", initializerException);
    }
	Log.debug("JSP service context initialized");

	// Verify the application has initialized
	Boolean initialized = (Boolean) application.getAttribute("initialized");
	if (initialized == null || !initialized) {
		String msg = "Innovation has not initialized properly! Is web.xml missing the AppContextLoaderListener?";
		Log.error(msg);
		response.setStatus(500);
		throw new ServletException(msg);
	}
%>

<%

	// Read the cookies we're interested in
	String sessionId = pageContext.getSession().getId();
	String expectedSessionId = null;
	String logoutMessage = null;
	String passwordChangeUserName = null;
	String loggedInUser = null;
	Cookie cookies [] = request.getCookies();
	if (cookies != null){
		for (int i = 0; i < cookies.length; i++) {
			if (cookies[i].getName().equals("ExpectedSessionId")){
				expectedSessionId = cookies[i].getValue();
			}
			else if (cookies[i].getName().equals("LogoutMessage")){
				logoutMessage = cookies[i].getValue();
			}
			else if (cookies[i].getName().equals("PasswordChangeUserName")){
				passwordChangeUserName = cookies[i].getValue();
			}
			else if (cookies[i].getName().equals("ServiceUser")){
				loggedInUser = cookies[i].getValue();
			}
		}
	}

	List<AuthenticationAlert> alerts = new ArrayList<AuthenticationAlert>();

	// Get the user name that was previously submitted
	String userName = pageContext.getRequest().getParameter("j_username");
	
	// If the user name is null, it's possible we are coming from the password change page
	if( (userName == null || userName.length() == 0) && passwordChangeUserName != null && passwordChangeUserName.length() > 0){
		userName = passwordChangeUserName;
	}
		
	// Get the authentication alerts from the server
	List<AuthenticationAlert> serverAlerts = new ArrayList<AuthenticationAlert>();
	if(userName != null){
		 serverAlerts = Arrays.asList(AuthenticationAlertCache.getCache().getAuthenticationAlerts(userName));	
	}
	
	// Set the alerts
	if(pageContext.getSession().getAttribute("HttpErrorMessage") != null){
		String message = (String) pageContext.getSession().getAttribute("HttpErrorMessage");
		AuthenticationAlert alert = new AuthenticationAlert("HttpErrorMessage", message);
		alerts.add(alert);
		
		// Clear the http error message
		pageContext.getSession().setAttribute("HttpErrorMessage", null);
	}
	else if(logoutMessage != null && logoutMessage.length() > 0){
		AuthenticationAlert alert = new AuthenticationAlert("LogoutMessage", logoutMessage);
		alerts.add(alert);
	}
	else{		
		// If this session isn't the expected session id and there is no logout message, the user has been timed out or kicked out
		if(expectedSessionId != null && !(sessionId.equalsIgnoreCase(expectedSessionId))){
			
			List<AuthenticationAlert> kickedOutAlerts = null;
			if(loggedInUser != null){
				kickedOutAlerts = Arrays.asList(AuthenticationAlertCache.getCache().getAuthenticationAlerts(expectedSessionId));
				alerts.addAll(kickedOutAlerts);
			}
			
			// If there are no alerts, the session timed out
			if(kickedOutAlerts != null && kickedOutAlerts.size() == 0){
				AuthenticationAlert alert = new AuthenticationAlert("SessionTimedOut", "Your session timed out");
				alerts.add(alert);			
			}
			
		}
		// If there was an expected session and no server alerts, the server restarted
		else if(loggedInUser != null && loggedInUser.length() > 0 && !loggedInUser.equalsIgnoreCase("Anonymous") && serverAlerts.size() == 0){
			AuthenticationAlert alert = new AuthenticationAlert("ServerRestart", "The server restarted. Please re-supply your username and password");
			alerts.add(alert);			
		}
	}
	
	// Add the defined server alerts, for unknown type, display basic error message
	for (int i=0; i<serverAlerts.size(); i++) {
		if (((AuthenticationAlert) serverAlerts.get(i)).getCode().equals("UnexpectedError")) {
			AuthenticationAlert alert = new AuthenticationAlert("ConnectionError", "System data unavailable. Try again later.");
			alerts.add(alert);			
		} else {
			alerts.add(serverAlerts.get(i));
		}
	}
	
	// Add SAML related alerts
	if( SAMLSettings.INSTANCE.getIdPEnableFlag() ) { 
		String samlParameter = SAMLServlet.getSAMLParameter(request);
		//SAML Single Sign On fails
		if( samlParameter.equalsIgnoreCase(SAMLServlet.SAMLCode.FAILED) ) {
			AuthenticationAlert alert = new AuthenticationAlert("SAMLMessage", "Sorry, something went wrong. Please try again. If you continue to experience this issue, please contact your system administrator");
			alerts.add(alert);
		} else if( !samlParameter.equals(SAMLServlet.SAMLCode.ON) && !samlParameter.equalsIgnoreCase(SAMLServlet.SAMLCode.OFF) ) {
			AuthenticationAlert alert = new AuthenticationAlert("SAMLMessage", "Unrecognized SAML Parameter");
			alerts.add(alert);
		}
	}
	
	boolean isMFAAuthenticated = false; 
	boolean isMfaSelfVerifiedInd = false;
	if (MFASettings.INSTANCE.isMFAEnabled()){
		
		if (userName != null && !userName.isEmpty()){
			session.setAttribute("MFAUser", userName);
		} else {
			userName = (String)session.getAttribute("MFAUser");
		}
		
		String mfaPin = request.getParameter("mfa_pin");
		isMfaSelfVerifiedInd = new MFATools().isParameterTrue(request, "MFASelfVerified");
		boolean isGoogleAuthenticatorInd = MFASettings.INSTANCE.isGoogleAuthenticatorEnabled() && new MFATools().isParameterTrue(request, "GoogleAuthenticatorFlag");
		if (mfaPin != null){
			if (new MFAAuthentication().authenticateMFA(request, session, userName, mfaPin, isMfaSelfVerifiedInd, isGoogleAuthenticatorInd)) {
				isMFAAuthenticated = true;
			} else {
				AuthenticationAlert alert = new AuthenticationAlert("MFAMessage", "Authentication code is incorrect");
				alerts.add(alert);
			}
		}

		if (!isMFAAuthenticated){
			if (userName != null && !userName.isEmpty()){
				session.setAttribute("MFAUser", userName);
			} else {
				userName = (String)session.getAttribute("MFAUser");
			}
			
			boolean requiresMFA = false;
			for (int i=0; i<serverAlerts.size(); i++) {
				AuthenticationAlert authenticationAlert = ((AuthenticationAlert) serverAlerts.get(i));
				if (authenticationAlert.getCode().equals("SecurityException") && authenticationAlert.getMessage().equals("MFAAuthenticationRequired")) {
					requiresMFA = true;
				}
			}
			if (requiresMFA){
				if (new MFATools().isMFARememberedDevice(request, userName)){
					isMFAAuthenticated = true;
				} else {
					MFASecurity.init();
					String navigationPageToken = MFASecurity.encryptPageNavigationToken(userName, "mfa_select_phone_email.jsp");
					RequestDispatcher RequetsDispatcherObj = request.getRequestDispatcher("/mfa_select_phone_email.jsp?NavigationPage=" + navigationPageToken);
					RequetsDispatcherObj.forward(request, response);
				}	
			}
		}	
	}
	
	if(alerts.size() > 0){				
		pageContext.setAttribute("hasErrors", "true");
		context.getLogContext().setAuthenticationAlertErrors(alerts);
	}
	
	// Set the expected session id cookie
	response.addCookie(new Cookie("ExpectedSessionId", sessionId));
	
	// Clear the logout message
	response.addCookie(new Cookie("LogoutMessage", ""));

	// Clear the password change user name
	response.addCookie(new Cookie("PasswordChangeUserName", ""));
	
	// Clear the logged in user
	response.addCookie(new Cookie("ServiceUser", ""));
	
	//Header necessary to make sure that IE browser does not default to compatibility mode when connected to Intranet.
	//Note that even though IE reports that it is in Compatibility Mode, it actually renders pages correctly.
    response.addHeader("X-UA-Compatible", "IE=edge");

	// Determine the relative path for the images and scripts
	String servletPath = (String) request.getAttribute("javax.servlet.forward.request_uri");
	if (servletPath == null || servletPath.isEmpty()){
		servletPath = "/innovation";
	}
	String relativePath = "";
	int indexOf = servletPath.indexOf("/", 0);
	while(indexOf > -1){
		relativePath += "../";
		indexOf = servletPath.indexOf("/", indexOf + 1);
	}

%>
	
<html>

<head>

<!-- Page load timing -->
<script type="text/javascript">
    var pageLoadStartTimestamp = (new Date()).getTime();
</script>

<!-- Selenium automation page load waiting -->
<script type="text/javascript">
	window.seleniumPageLoadOutstanding = 1;
</script>

     <title>Guidewire InsuranceNow&trade; Login</title>
     <style type="text/css" media="screen">
     <!--
     @import url(<%=relativePath%>innovation.css);
     @import url(<%=relativePath%>color1.css);
     @import url(<%=relativePath%>css/ui-lightness/jquery-ui-1.12.1.css);
     @import url(<%=relativePath%>css/dynaskin-vista/ui.dynatree.css);
     -->
     </style>
     <style type="text/css" media="print">
     <!--
     @import url(<%=relativePath%>innovation.css);
     @import url(<%=relativePath%>print.css);
     @import url(<%=relativePath%>css/ui-lightness/jquery-ui-1.12.1.css);
     @import url(<%=relativePath%>css/dynaskin-vista/ui.dynatree.css);
     -->
     </style>
     <meta http-equiv="content-type" content="text/html" />
     <meta name="author" content="Guidewire" />
     <meta name="generator" content="InsuranceNow" />
     <meta name="viewport" content="width=1137" />
     <link rel="apple-touch-icon" sizes="180x180" href="<%=relativePath%>/apple-touch-icon.png"/>
     <link rel="icon" type="image/png" href="<%=relativePath%>/favicon-32x32.png" sizes="32x32"/>
     <link rel="icon" type="image/png" href="<%=relativePath%>/favicon-16x16.png" sizes="16x16"/>
     <link rel="manifest" href="manifest.json"/>
     <link rel="mask-icon" href="safari-pinned-tab.svg" color="#5bbad5"/>
     <meta name="theme-color" content="#ffffff"/>
     <link rel="alternate stylesheet" type="text/css" href="<%=relativePath%>color2.css" title="color2" />
	 <link rel="stylesheet" type="text/css" href="<%=relativePath%>css/ui-lightness/jquery-ui-1.12.1.css" title="color1" />	
	 <link rel="alternate stylesheet" type="text/css" href="<%=relativePath%>css/redmond/jquery-ui-1.12.1.css" title="color2" />	
	 <link rel="stylesheet" type="text/css" href="<%=relativePath%>css/dynaskin-vista/ui.dynatree.css" title="dynatree" id="skinSheet"/>	
     <script type="text/javascript" src="<%=relativePath%>js/jquery-3.1.1.min.js"></script>
     <script type="text/javascript" src="<%=relativePath%>js/jquery-ui-1.12.1.min.js"></script>
     <script type="text/javascript" src="<%=relativePath%>innovex.js"></script>
     <script type="text/javascript" src="<%=relativePath%>innovation.js"></script>
     <script type="text/javascript" src="<%=relativePath%>style-switcher.js"></script>
     <script type="text/javascript" src="<%=relativePath%>news-banner.js"></script>
     <script type="text/javascript" src="<%=relativePath%>authorize-net.js"></script>
     
<%
    TileSetRenderer rend = new TileSetRenderer();
    out.println(rend.renderPage(VelocityTools.tile("COAnalytics::tileset::form::analytics-tracking-head-end"))); 
%>

</head>
<% 
boolean samlSSOInd = false; //flag to indicate that SAML SSO was initiated. SAML could be on but manually bypassed
if( SAMLSettings.INSTANCE.getIdPEnableFlag() ) { //Shibboleth Library needs to run on atleast Java 7. Make sure not to hit Shibboleth Library if SAML is disabled so Java 6 environments can still work
	if( SAMLRequestInitiator.sendSAMLRequestInd(request) ) {
		try {
			samlSSOInd = true;
			new SAMLRequestInitiator().sendSAMLAuthnRequest(request, response);
			
		} catch( Exception e ) {
			Log.error("Failed to generate and send SAML AuthnRequest", e);
			response.sendRedirect("/innovation?rq=COUserStartPage&saml=failed");
		}
	} else if( SAMLRequestInitiator.SAMLUserSessionAttributeInd(request) ) { 
		samlSSOInd = true; 
		String userId = SAMLRequestInitiator.getValidSAMLUserSessionAttribute(request);
%>
	<body onload="document.forms[0].submit()">
		<form name="frmLogin" id="frmLogin" method="post" action="<%=relativePath%>j_security_check">
			<input type="hidden" name="j_username" id="j_username" value="<%=userId%>" />
			<input type="hidden" name="j_password" id="j_password" value="<%=SAMLSecurity.generateSAMLPassword(userId)%>" />
		</form>
	</body>
<%	
	}
}

if (isMFAAuthenticated){
	
	MFATools tools = new MFATools();
	MFASecurity.init();
	boolean rememberDeviceInd = tools.isParameterTrue(request, "remember_device");
	tools.updateRememberDeviceCookie(request, response, userName, (rememberDeviceInd) );
%>
	<body onload="document.forms[0].submit()">
		<form name="frmLogin" id="frmLogin" method="post" action="innovation/j_security_check">
			<input type="hidden" name="j_username" id="j_username" value="<%=userName%>" />
			<input type="hidden" name="j_password" id="j_password" value="<%=MFASecurity.generateMFAPassword(userName)%>" />
		</form>
	</body>
<%
}

if( !samlSSOInd && !isMFAAuthenticated) {
%>
<body onload="document.getElementById('j_username').focus(); ">
	
	<script type="text/javascript">
	
	var timerId = 0;
	
	 function initiateDelayedPopup(){
	 	timerId = setTimeout("popupPDF()", 500);
	 }
	 
	
	 function popupPDF(){
	 	clearTimeout(timerId);
	 	window.open("popupPDF.pdf");
	 }
	 
	 
     function login() {
    	window.seleniumPageLoadOutstanding++;
     	document.frmLogin.submit();
     }
     
     function loginIdKeyPress(event) {
         if (getKeyNum(event) == 13) {
             login();
             return false;
         }
     }
     
     function passwordKeyPress(event) {
         if (getKeyNum(event) == 13) {
             login();
             return false;
         }
     }
     
     function signInKeyPress(event) {
         if (getKeyNum(event) == 32) {
             login();
         }
     }
     
     function getKeyNum(event) {
         if (window.event) return event.keyCode;	//IE
         if (event.which) return event.which;		//Netscape/Firefox/Opera
     }
	</script>
	
	<form name="frmLogin" id="frmLogin" method="post" action="<%=relativePath%>j_security_check">
		<div class='pageBackground'>
			<div class='banner'></div>
			<div class='menu'>
				<ul>
					<li><span></span></li>
				</ul>
				<div class='clear'></div>
			</div>
			<div class='subMenu'>    
			    <div class="wiz_bottom_left"></div>
				<div class="wiz_bottom_right"></div>
			</div>
			<% try { %>
			<%     SecurityManager.getSecurityManager();%>
			<% } catch(Exception e) { %>
			<div align='center'><span class='error'>THE SYSTEM IS CURRENTLY UNAVAILABLE</span></div>
			<% } %>
			
			<c:if test='${hasErrors == "true"}'>
				<div class='errorMsg'>
					<div class='error_top_left'></div>
					<div class='error_top_right'></div>
					<div class='clear'></div>
					<div class='error_content'>
						<div class='error_content_left'>
						<div class='error_icon'></div>
							<span>ATTENTION!</span>
						</div>
						<div class='error_content_right'>
						<%
						 
				      		for(int i=0; i<alerts.size(); i++){
				      			out.println(((AuthenticationAlert) alerts.get(i)).getMessage());
				      			out.println("<br>");
				      		}
						
			      		%>
						</div>
						<div class='clear'></div>
					</div>
					<div class='error_bottom_left'></div>
					<div class='error_bottom_right'></div>
				</div>
			</c:if>
			
			<%if( !SAMLSettings.INSTANCE.getIdPEnableFlag() || SAMLRequestInitiator.displaySPILogin(request) ) { //Shibboleth Library needs to run on atleast Java 7. Make sure not to hit Shibboleth Library if SAML is disabled so Java 6 environments can still work%> 
			<div class='signInTile'>
				<div class='r_tile'> <!-- Start Tile-->
					<div class="r_tile_top_left"></div>
					<div class="r_tile_top_right">Please Sign In</div>
					<div class="clear"></div>
					<div class="r_tile_content_right">
						<div class="r_tile_content_left">
							<label>User Name</label>
							<input type='text' id="j_username" name='j_username' style='text-align: Left;' MaxLength='100' size='20' tabindex='1' onkeypress='javascript:return loginIdKeyPress(event);' />
							<br/>	
							<label>Password</label>
							<input type='password' id="j_password" name='j_password' value='' Align='Left' size='20' tabindex='2' onkeypress='javascript:return passwordKeyPress(event);' />
							<br/>
							<a class='button' id='SignIn' name='SignIn' href='javascript:login();' ><span>&nbsp;</span>Sign In</a>
							<div class="clear"></div>
							<% try { %>
								<% if(SecurityManager.getSecurityManager().canChangePasswords()) { %>
								<br/>
								<div>		
									<span><a class="actionLink" onclick="javascript:window.seleniumPageLoadOutstanding++;" href="<%=relativePath%>forgot_password.jsp">Forgot Password</a></span>
									&nbsp;&nbsp;
									<span><a class="actionLink" onclick="javascript:window.seleniumPageLoadOutstanding++;" href="<%=relativePath%>change_password.jsp">Change Password</a></span>
								</div>
								<br>
							<% }		
							} catch(Exception e) { 
								Log.error(e);
							} 
							%>				
						</div>
					</div>
					<div class="clear"></div>
					<div class="r_tile_bottom_left"></div>
					<div class="r_tile_bottom_right"></div>
					<div class="clear"></div>
				</div> <!-- End Tile-->
			</div>
			<% } %>
			<% 
			out.println(rend.renderPage(VelocityTools.tile("CONewsBanner::tileset::form::news-banner-login"))); 
			%>			
			<div class='tile'>
				<div class='clear'></div>
				<div id='FooterTile' class='footer'>
					<div class="footer_left"></div>
					<div class="footer_content">
						<img src="<%=relativePath%>img/powered_by.png" class="powered_by" />
						&nbsp;&nbsp;<a href="#" onclick="setActiveStyleSheet('color1'); return false;"><img src="<%=relativePath%>img/color1_selection.png" alt="Turn Site Orange" border='0'/></a>
						&nbsp;&nbsp;<a href="#" onclick="setActiveStyleSheet('color2'); return false;"><img src="<%=relativePath%>img/color2_selection.png" alt="Turn Site Blue" border='0'/></a>
					</div>
				</div>  <!-- End of <div class='footer'> -->
			</div> <!-- End of <div class='tile'> -->
		
		</div>
	</form>

<%
    out.println(rend.renderPage(VelocityTools.tile("COAnalytics::tileset::form::analytics-tracking-body-end"))); 
%>

<script type="text/javascript">
	window.seleniumPageLoadOutstanding--;
</script>

</body>
<% } %>
</html>

<%
} finally {
	Log.debug("Finalizing JSP service context");
	try {
		// close the transaction		
		ctxInitializer.finalizeServiceContext();
	} catch(Exception e) {
		Log.error(e);
	}
	
	// Export the logs
	if (Log.isLogThreads()) {
		Log.finalizeLogs();
	}
}
%>
