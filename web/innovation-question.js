/* Innovation Question Show On Parent Value Comparison Library
*
*       evalEquals(parentValue, value)
*       evalEqualsIgnoreCase(parentValue, value)
*       evalNotEquals(parentValue, value)
*       evalGreaterThan(parentValue, value)
*       evalGreaterThanOrEquals(parentValue, value)
*       evalLessThan(parentValue, value)
*       evalLessThanOrEquals(parentValue, value)
*       evalInList(parentValue, list)	// List is Comma Delimited String
*       evalBetween(parentValue, lowerBoundary, upperBoundary)
*/ 

function evalEquals(parentValue, refValue) {

	// Strip Number Format Characters
	parentValue = stripNumberFormatChars(parentValue);
	refValue = stripNumberFormatChars(refValue);
	
	if (isNaN(parentValue) || isNaN(refValue)) {
		// String Compare
		if(parentValue == refValue) {
			return true;
		}
	} else { 
		// Numeric Compare
    	var parentValueNum = parseFloat(parentValue);
    	var refValueNum = parseFloat(refValue);

    	if(parentValueNum == refValueNum) {
	        return true;
    	}
    }
    return false;
}

function evalEqualsIgnoreCase(parentValue, refValue) {
	// Call the normal evalEquals, but uppercase the parameters first
	return evalEquals(parentValue.toUpperCase(), refValue.toUpperCase());
}

function evalNotEquals(parentValue, refValue) {
	
	// Strip Number Format Characters
	parentValue = stripNumberFormatChars(parentValue);
	refValue = stripNumberFormatChars(refValue);
	
	if( refValue == "" && parentValue == "" ) {
		return false;
	} else if (isNaN(parentValue) || isNaN(refValue)) {
		// String Compare
		if(parentValue != refValue) {
			return true;
		}
	} else { 
		// Numeric Compare
    	var parentValueNum = parseFloat(parentValue);
    	var refValueNum = parseFloat(refValue);

    	if(parentValueNum != refValueNum) {
	        return true;
    	}
    }
    return false;
}

function evalEqualsIgnoreCase(parentValue, refValue) {
	// Call the normal evalEquals, but uppercase the parameters first
	return evalEquals(parentValue.toUpperCase(), refValue.toUpperCase());
}

//function returns an array
function evalInList(parentValue, list)
{
	// Split the list into Array
	var dataArray = list.split(",");
	var length = dataArray.length;
	if(length > 0) {
		// compare with each data in the array
		for(i = 0; i < length; i++)	{
			if (typeof(parentValue) === "string") {
				if (dataArray[i] == parentValue){
					return true;
				}	
			} else if (parentValue && $.isArray(parentValue)){
				//It is probably from a multi select list
				for (j = 0; j< parentValue.length; j++){
					if (dataArray[i] == parentValue[j].value){
						return true;
					}
				}
			} 
		}
	}
	return false;
}

function evalBetween(parentValue, lowerBoundary, upperBoundary){
	
	// Strip Number Format Characters
	parentValue = stripNumberFormatChars(parentValue);
	lowerBoundary = stripNumberFormatChars(lowerBoundary);
	upperBoundary = stripNumberFormatChars(upperBoundary);
	
	if (isNaN(parentValue) || isNaN(lowerBoundary) || isNaN(upperBoundary)){
			return false;
	} else { 
		// Numeric Compare
	    var parentValueNum = parseFloat(parentValue);
	    var lowerBoundaryNum = parseFloat(lowerBoundary);
	    var upperBoundaryNum = parseFloat(upperBoundary);
			
	    if(lowerBoundaryNum <= parentValueNum && parentValueNum  <= upperBoundaryNum) {
	        return true;
	    }
	}
    return false;
}

function evalLessThan(parentValue, refValue) {

	// Strip Number Format Characters
	parentValue = stripNumberFormatChars(parentValue);
	refValue = stripNumberFormatChars(refValue);
	
	if (isNaN(parentValue) || isNaN(refValue)) {
		// String Compare
		if(parentValue < refValue) {
			return true;
		}
	} else { 
		// Numeric Compare
	    var parentValueNum = parseFloat(parentValue);
	    var refValueNum = parseFloat(refValue);
			
	    if(parentValueNum < refValueNum) {
	        return true;
	    }
	}
    return false;
}

function evalLessThanOrEquals(parentValue, refValue) {

	// Strip Number Format Characters
	parentValue = stripNumberFormatChars(parentValue);
	refValue = stripNumberFormatChars(refValue);
	
	if (isNaN(parentValue) || isNaN(refValue)) {
		// String Compare
		if(parentValue <= refValue) {
			return true;
		}
	} else { 
		// Numeric Compare
	    var parentValueNum = parseFloat(parentValue);
	    var refValueNum = parseFloat(refValue);
			
	    if(parentValueNum <= refValueNum) {
	        return true;
	    }
	}
    return false;
}

function evalGreaterThan(parentValue, refValue) {

	// Strip Number Format Characters
	parentValue = stripNumberFormatChars(parentValue);
	refValue = stripNumberFormatChars(refValue);
	
	if (isNaN(parentValue) || isNaN(refValue)) {
		// String Compare
		if(parentValue > refValue) {
			return true;
		}
	} else { 
		// Numeric Compare
	    var parentValueNum = parseFloat(parentValue);
	    var refValueNum = parseFloat(refValue);
			
	    if(parentValueNum > refValueNum) {
	        return true;
	    }
	}
    return false;
}

function evalGreaterThanOrEquals(parentValue, refValue) {

	// Strip Number Format Characters
	parentValue = stripNumberFormatChars(parentValue);
	refValue = stripNumberFormatChars(refValue);
	
	if (isNaN(parentValue) || isNaN(refValue)) {
		// String Compare
		if(parentValue >= refValue) {
			return true;
		}
	} else { 
		// Numeric Compare
	    var parentValueNum = parseFloat(parentValue);
	    var refValueNum = parseFloat(refValue);
			
	    if(parentValueNum >= refValueNum) {
	        return true;
	    }
	}
    return false;
}