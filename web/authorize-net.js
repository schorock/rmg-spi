(function () {
	if (!window.AuthorizeNetPopup) window.AuthorizeNetPopup = {};
	if (!AuthorizeNetPopup.options) AuthorizeNetPopup.options = {
			onPopupClosed: null
			,eCheckEnabled: false
			,skipZIndexCheck: false
			,useTestEnvironment: false
		};
	AuthorizeNetPopup.closePopup = function() {
		document.getElementById("divAuthorizeNetPopup").className = "";
		document.getElementById("iframeAuthorizeNet").style.display = "none";
		document.getElementById("AuthorizeNetShadowT").style.display = "none";
		document.getElementById("AuthorizeNetShadowR").style.display = "none";
		document.getElementById("AuthorizeNetShadowB").style.display = "none";
		document.getElementById("AuthorizeNetShadowL").style.display = "none";
		document.getElementById("AuthorizeNetShadowTR").style.display = "none";
		document.getElementById("AuthorizeNetShadowBR").style.display = "none";
		document.getElementById("AuthorizeNetShadowBL").style.display = "none";
		document.getElementById("AuthorizeNetShadowTL").style.display = "none";
		$("#ModalAuthorizeNet").dialog({ beforeClose: function(event, ui) { return true; } });		
  		$("#ModalAuthorizeNet").dialog("close");
	};
	AuthorizeNetPopup.openManagePopup = function() {
		openSpecificPopup({action:"manage"});
	};
	AuthorizeNetPopup.openAddPaymentPopup = function() {
		openSpecificPopup({action:"addPayment", paymentProfileId:"new"});
	};
	AuthorizeNetPopup.openEditPaymentPopup = function(paymentProfileId) {
		openSpecificPopup({action:"editPayment", paymentProfileId:paymentProfileId});
	};
	AuthorizeNetPopup.openAddShippingPopup = function() {
		openSpecificPopup({action:"addShipping", shippingAddressId:"new"});
	};
	AuthorizeNetPopup.openEditShippingPopup = function(shippingAddressId) {
		openSpecificPopup({action:"editShipping", shippingAddressId:shippingAddressId});
	};
	AuthorizeNetPopup.onReceiveCommunication = function(querystr) {
		var params = parseQueryString(querystr);
		switch(params["action"]) {
			case "successfulSave":
				AuthorizeNetPopup.closePopup();
				if( AuthorizeNetPopup.onPopupSaved ) {
					AuthorizeNetPopup.onPopupSaved();
				}
				break;
			case "cancel":
				AuthorizeNetPopup.closePopup();
				if( AuthorizeNetPopup.onPopupCancelled ) {
					AuthorizeNetPopup.onPopupCancelled();
				}
				break;
			case "resizeWindow":
				var w = parseInt(params["width"]);
				var h = parseInt(params["height"]);
				var ifrm = document.getElementById("iframeAuthorizeNet");
				ifrm.style.width = w.toString() + "px";
				ifrm.style.height = h.toString() + "px";
				break;
		}
	};
	function openSpecificPopup(opt) {
		var popup = document.getElementById("divAuthorizeNetPopup");
		var ifrm = document.getElementById("iframeAuthorizeNet");
		var form = document.forms["formAuthorizeNetPopup"];
		switch (opt.action) {
			case "addPayment":
				ifrm.style.width = "435px";
				ifrm.style.height = "508px";
				break;
			case "editPayment":
				ifrm.style.width = "435px";
				ifrm.style.height = "479px";
				break;
			case "addShipping":
				ifrm.style.width = "385px";
				ifrm.style.height = "359px";
				break;
			case "editShipping":
				ifrm.style.width = "385px";
				ifrm.style.height = "359px";
				break;
			case "manage":
				ifrm.style.width = "442px";
				ifrm.style.height = "578px";
				break;
		}
		var zIndexHightest = getHightestZIndex();
		popup.style.zIndex = zIndexHightest + 2;
		if( AuthorizeNetPopup.options.useTestEnvironment ) {
			form.action = "https://test.authorize.net/customer/" + opt.action;
		} else {
			form.action = "https://accept.authorize.net/customer/" + opt.action;
		}
		if( form.elements["PaymentProfileId"] ) {
			form.elements["PaymentProfileId"].value = opt.paymentProfileId;
		} else {
			form.elements["PaymentProfileId"].value = "";
		}
		if( form.elements["ShippingAddressId"] ) {
			form.elements["ShippingAddressId"].value = opt.shippingAddressId;
		} else {
			form.elements["ShippingAddressId"].value = "";
		}
		popup.className = "AuthorizeNetPopupSimpleTheme";
		form.submit();
		try {
			$("#ModalAuthorizeNet").dialog({
				minHeight: 0,
				width: 0,
				modal: true,
				beforeClose: function(event, ui) { return false; },
				autoOpen: true,
				resizable: false
			});
			$("#ModalAuthorizeNet").closest(".ui-dialog").hide();
		} catch(e) {
	    	// do nothing on error. This avoids hanging requests if busybox didn't init for some reason
	    }
	};
	function getHightestZIndex() {
		var max = 0;
		var z = 0;
		var a = document.getElementsByTagName('*');
		for (var i = 0; i < a.length; i++) {
			z = 0;
			if (a[i].currentStyle) {
				var style = a[i].currentStyle;
				if (style.display != "none") {
					z = parseFloat(style.zIndex);
				}
			} else if (window.getComputedStyle) {
				var style = window.getComputedStyle(a[i], null);
				if (style.getPropertyValue("display") != "none") {
					z = parseFloat(style.getPropertyValue("z-index"));
				}
			}
			if (!isNaN(z) && z > max) max = z;
		}
		return Math.ceil(max);
	}
	function parseQueryString(str) {
		var vars = [];
		var arr = str.split('&');
		var pair;
		for (var i = 0; i < arr.length; i++) {
			pair = arr[i].split('=');
			vars.push(pair[0]);
			vars[pair[0]] = unescape(pair[1]);
		}
		return vars;
	}
} ());
