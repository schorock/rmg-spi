<%@page import="net.inov.tec.security.authenticator.AuthenticationAlertCache"%>
<%@page import="net.inov.tec.security.authenticator.AuthenticationAlert"%>
<%@page import="net.inov.tec.security.SecurityManager"%>
<%@page import="com.iscs.common.render.VelocityTools"%>
<%@page import="com.iscs.common.tech.web.analytics.AnalyticsSettings"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.net.InetAddress"%>
<%@page import="javax.servlet.http.Cookie"%>

<%@page contentType="text/html; charset=utf-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%

	String referrer = request.getHeader("Referer");
	if(referrer == null){
		referrer = "unknown page.";
	}

	String errorMessage = "Invalid request for " + referrer;
	
	// Do not show an error message
	if(referrer.endsWith("j_security_check")){
		errorMessage = "You are already logged in. To relogin, please click 'Logout'";		
	}
	

%>
	
<html>

<head>
	<title>HTTP 404 Error</title>
	
	<!-- Internet explorer suppresses custom error pages if they are under a certain number of bytes -->
	<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
	<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
	<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
	<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
	<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
	<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
	<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
	<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
	<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
	<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
	
	<meta http-equiv="refresh" content="5;url=/innovation?rq=COUserStartPage">

<%
    AnalyticsSettings analyticsSettings = AnalyticsSettings.getSettings();
    if (analyticsSettings.isEnabled()) {
    	VelocityTools vt = new VelocityTools();
%>
<script type="text/javascript">

  var analyticsPageName = "error404.jsp/" + document.URL;
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '<%= analyticsSettings.getGoogleAnalyticsSettings().getWebPropertyId() %>'],
            ['_setDomain', '<%= analyticsSettings.getGoogleAnalyticsSettings().getDomainName() %>']);
  _gaq.push(['_setVar', ''],
            ['_setCustomVar', 1, 'UserType', '', 3],
            ['_setCustomVar', 2, 'User', '', 3],
            ['_setCustomVar', 3, 'AppServer', '<%= vt.getHostName().replace(" ","_") %>', 3],
            ['_setCustomVar', 4, 'Version', '<%= vt.getSPIVersion() %>', 3]);
  _gaq.push(['_trackPageview', '/innovation/' + analyticsPageName]);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<%
    }
%>
</head>
<body>
	
	<%=errorMessage%>
	
	<BR><BR>
	Redirecting you to the start page.  If you are not automatically redirected, click <a href="innovation?rq=COUserStartPage"> here </a>
	
</body>
</html>	
     