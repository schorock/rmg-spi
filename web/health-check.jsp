<%@page import="net.inov.tec.date.ThreadSafeSimpleDateFormat"%>
<%@page import="net.inov.mda.MDAObject"%>
<%@page import="net.inov.mda.MDA"%>
<%@page import="net.inov.tec.security.SystemNotAvailableException"%>
<%@page import="java.lang.Thread" %>
<%@page import="java.lang.ThreadGroup" %>
<%@page import="java.net.UnknownHostException"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Enumeration"%>
<%@page import="net.inov.biz.server.ServiceContext"%>
<%@page import="net.inov.tec.thread.MasterThreadManager" %>
<%@page import="net.inov.tec.security.SecurityManager"%>
<%@page import="com.iscs.common.tech.log.Log" %>
<%@page import="com.iscs.common.tech.web.servlet.ServletServiceContextInitializer"%>

<%@page contentType="text/html; charset=utf-8" %>
<%@page buffer="none" %>

<%!

//Use a static thread-safe version of SimpleDateFormat both to avoid thread-safe issues in SimpleDateFormat
//and to gain the performance benefits of not instantiating (and initializing) a new one every time
//and concurrency issues in SimpleDateFormat->DecimalFormat->DigitList
private static final ThreadSafeSimpleDateFormat responseTimestampFmt = new ThreadSafeSimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS z");

%>

<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
%>

<%

// Change these to alter the fixed user used to test authentication
// Note: This user should have its ConcurrentSessions set to 0 to avoid unnecessary updates to the UserInfo bean
final String USER_ID = "health-check";
final String PASSWORD = "Blue19Rock!";

long startTime = System.currentTimeMillis();	// We don't use Log.startStopWatch only because we can't call that until after the log has been started which seems too late

try {
	
	ServiceContext context = ServiceContext.getServiceContext();
	
	// Start a new log that rolls once a day (unlike normal logs that are 1 per request)
	
	String serverName = null;
	try {
		serverName = InetAddress.getLocalHost().getHostName();
	} catch (UnknownHostException e) {
        serverName = request.getServerName();
        if (serverName == null || serverName.trim().equals("")) {
        	serverName = "unknown";
        }
	}
	SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
	String transactionName = serverName + ".health-check." + fmt.format(new Date());
    context.setTransactionName(transactionName);
    context.setLogfile(transactionName + ".log");
	Log.start(ServiceContext.getServiceContext().getLogfile());
    // Save the log file name in the ServletRequest so that it can be accessed by logging (e.g. AccessLogValve)
    request.setAttribute("LogFile", context.getLogfile());
    
    // Log some request data that may prove useful later
    Log.info("Request remote host addr = " + request.getRemoteAddr());
    Log.info("Request server name = " + request.getServerName());
    Log.info("Request user-agent = " + request.getHeader("user-agent"));
    StringBuilder requestHeaders = new StringBuilder("Request headers (all):");
    for (Enumeration headerEnum = request.getHeaderNames(); headerEnum.hasMoreElements();) {
        String headerName = (String)headerEnum.nextElement();
        String headerValue = request.getHeader(headerName);
        requestHeaders.append("\n")
                      .append("    ")
                      .append(headerName)
                      .append(" = ")
                      .append(headerValue);
    }
    Log.info(requestHeaders.toString());
	
    //
    // Test that basic subsystems are fully initialized

	// Verify the application has initialized
	Boolean initialized = (Boolean) application.getAttribute("initialized");
	if (initialized == null || !initialized) {
		String msg = "Innovation has not initialized properly! Is web.xml missing the AppContextLoaderListener?";
		Log.error(msg);
		response.setStatus(500);
		throw new ServletException(msg);
	}
    
    // MDA
    try {
    	MDAObject testMDA = MDA.getModelObject("OFW::Object::Object::Object");
    } catch (Exception e) {
    	// We had an exception during authentication.
    	Log.error("Exception verifying MDA initialized", e);
    	
    	// Rethrow the exception so a proper HTTP error is returned
    	throw e;
    }
    
	//
	// Test database connectivity and general initialization by initializing a ServiceContext
	
    ServletServiceContextInitializer ctxInitializer = new ServletServiceContextInitializer();
    Exception initializerException = null;
    boolean maintenanceMode = false;
    try {
		ctxInitializer.initialize(new Object[] {request, response});
    } catch (SystemNotAvailableException e) {
        maintenanceMode = true;
        Log.warn("Application maintenance mode detected.");
    } catch (Exception e) {
    	initializerException = e;
    }
    if (initializerException != null) {
    	// We had an exception during service context initialization.
    	Log.error("Exception initializing service context", initializerException);
    	
    	// Rethrow the exception so a proper HTTP error is returned
    	throw initializerException;
    }

    // The above check for SystemNotAvailableException can be fooled by some as-yet-unknown initialization problem
    // Double check to make sure the context's connection object wasn't null after ServiceContext initialization
    if (context.getData() == null) {
        // We had a problem initializing something (TransactionFactory?)
        Log.error("ServiceContext database connection should not be null. Something didn't initialize");

        // Throw an exception so a proper HTTP error is returned
        throw new Exception("ServiceContext database connection should not be null. Something didn't initialize");
    }
    
    //
    // Test authentication using a hard-coded user
    // Note: This is important because authentication may use an external directory service (LDAP) so
    //       even if our database is up and running, a user may still not be able to log in successfully
    
	try {
		SecurityManager.getSecurityManager().authenticate(USER_ID, PASSWORD);
	} catch (Exception e) {
    	// We had an exception during authentication.
    	Log.error("Exception authenticating", e);
    	
    	// Rethrow the exception so a proper HTTP error is returned
    	throw e;
	}
%>
Okay
<%
	long endTime = System.currentTimeMillis();
	long responseTime = endTime - startTime;
	Log.info("Response time = " + responseTime);
	
	String responseTimestamp = responseTimestampFmt.format(new Date());
	Log.info("Response timestamp = " + responseTimestamp);
	
	out.println("serverName: " + serverName);
	out.println("responseTime: " + responseTime);
	out.println("responseTimestamp: " + responseTimestamp);
	out.println("remoteAddr: " + request.getRemoteAddr());
	if (maintenanceMode) {
	    out.println("(in maintenance mode)");
	}
	
	ArrayList<String> busyProcesses = new ArrayList<String>();
    try {
    	if( MasterThreadManager.getInstance().getRunningTasks().size() > 0 ){
    		busyProcesses.add("Managed Task");
    	}
    	
    	for( Thread thread : findAllRunningThreads() ){
    		if( thread != null && thread.getName().startsWith("BatchJob") ){
    			busyProcesses.add("Batch Job");
    			break;
    		}
    	}
    	
    } catch( Exception e ){
    	Log.error("Unable to determine if application is busy", e);
    	busyProcesses.add("Unable to determine if application server is busy");
    }	

    if( busyProcesses.size() > 0 ){
    	out.println("Busy:");
    	for( String busyProcess : busyProcesses ){
    		out.println(busyProcess);
    	}
    }
    
} finally {
	try {
		// close the transaction
		ServiceContext.getServiceContext().closeTransaction();				
	} catch(Exception e) {
		Log.error(e);
	}
	
	// Invalidate the session to keep open, untimed-out sessions from piling up
	try {
		request.getSession().invalidate();
	} catch (Exception e) {
		Log.error(e);
	}
	
	// Export the logs
	if (Log.isLogThreads()) {
		Log.export();
	}
}
%>

<%!
//Methods to help detect background BatchJobs threads

// Find all running threads
public Thread[] findAllRunningThreads(){
	ThreadGroup rootGroup = findRootThreadGroup();
	Thread[] threads = new Thread[ rootGroup.activeCount() ];
	while ( rootGroup.enumerate( threads, true ) == threads.length ) {
	    threads = new Thread[ threads.length * 2 ];
	}
	return threads;
}

// Find the root thread group
public ThreadGroup findRootThreadGroup(){
	ThreadGroup rootGroup = Thread.currentThread().getThreadGroup();
	ThreadGroup parentGroup;
	while ( ( parentGroup = rootGroup.getParent() ) != null ) {
	    rootGroup = parentGroup;
	}
	return rootGroup;
}

%>