<%
	String referrer = request.getHeader("Referer");
	if (referrer == null) {
		referrer = "unknown page.";
	}
	
	// Redirect to the main application page for a successful login following the HTTP 400 direct reference issue
	if (referrer.endsWith("j_security_check")) {
		response.sendRedirect("/innovation?rq=COUserStartPage");
	}
%>