<%@page import="net.inov.biz.server.ServiceContext"%>

<%@page contentType="text/html; charset=utf-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% 

	// Get the login error code
	String errorCode = "";
	ServiceContext context = ServiceContext.getServiceContext();
	if(context.getAuthenticationAlerts().size() > 0){
		errorCode = context.getAuthenticationAlertsArray()[0].getCode();
	}
	pageContext.setAttribute("errorCode", errorCode);
			
%>

<c:choose>
	<c:when test="${errorCode == 'ExpiredPassword'}">
		<jsp:include page="change_password.jsp" />
	</c:when>
	<c:otherwise>
		<jsp:include page="login.jsp" />
	</c:otherwise>
</c:choose>

<%
	
	// Clear the context
	context.clear();

%>