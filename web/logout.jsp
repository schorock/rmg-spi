<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.iscs.interfaces.saml.handler.SAMLServlet" %>

<%@page contentType="text/html; charset=utf-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%

//Header necessary to make sure that IE browser does not default to compatibility mode when connected to Intranet.
//Note that even though IE reports that it is in Compatibility Mode, it actually renders pages correctly.
response.addHeader("X-UA-Compatible", "IE=edge");

String logoutMessage = "<p>You have been logged out of InsuranceNow.</p><p>If you are using a shared computer you should also sign out of your identity provider's site.</p>";

String samlParameter = SAMLServlet.getSAMLParameter(request);
if( samlParameter.equalsIgnoreCase(SAMLServlet.SAMLCode.IDP_LOGGED_OUT) ) {
	logoutMessage = "<p>You have been logged out of InsuranceNow and your identity provider's site.</p>";
}
	
%>
<html>
	<head>
	     <title>Guidewire InsuranceNow&trade;</title>
	     <style type="text/css" media="screen">
	     <!--
	     @import url(innovation.css);
	     @import url(color1.css);
	     @import url(css/ui-lightness/jquery-ui-1.12.1.css);
	     @import url(css/dynaskin-vista/ui.dynatree.css);
	     -->
	     </style>
	     <style type="text/css" media="print">
	     <!--
	     @import url(innovation.css);
	     @import url(print.css);
	     @import url(css/ui-lightness/jquery-ui-1.12.1.css);
	     @import url(css/dynaskin-vista/ui.dynatree.css);
	     -->
	     </style>
	     <meta http-equiv="content-type" content="text/html" />
	     <meta name="author" content="Guidewire" />
	     <meta name="generator" content="InsuranceNow" />
	     <meta name="viewport" content="width=1137" />
	     <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png"/>
    	 <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32"/>
    	 <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16"/>
    	 <link rel="manifest" href="manifest.json"/>
    	 <link rel="mask-icon" href="safari-pinned-tab.svg" color="#5bbad5"/>
    	 <meta name="theme-color" content="#ffffff"/>
	     <link rel="alternate stylesheet" type="text/css" href="color2.css" title="color2" />
		 <link rel="stylesheet" type="text/css" href="css/ui-lightness/jquery-ui-1.12.1.css" title="color1" />	
		 <link rel="alternate stylesheet" type="text/css" href="css/redmond/jquery-ui-1.12.1.css" title="color2" />		
	     <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
	     <script type="text/javascript" src="js/jquery-ui-1.12.1.min.js"></script>
	     <script type="text/javascript" src="innovation.js"></script>
	     <script type="text/javascript" src="style-switcher.js"></script>
	</head>
	<body>
		<div class='pageBackground'>
			<div class='banner'></div>
			<div class='menu'>
				<ul>
					<li><span></span></li>
				</ul>
				<div class='clear'></div>
			</div>
			<div class='subMenu'>    
				<div class="wiz_bottom_left"></div>
				<div class="wiz_bottom_right"></div>
			</div>		
			<div class='errorMsg'>
				<div class='error_top_left'></div>
				<div class='error_top_right'></div>
				<div class='clear'></div>
				<div class='error_content'>
					<div class='error_content_right'>
						<%=logoutMessage%>
					</div>		
					<div class="clear"></div>
				</div>			
				<div class="clear"></div>
				<div class='error_bottom_left'></div>
				<div class='error_bottom_right'></div>
				<div align='right'><a class='button' href='/'><span>&nbsp;</span>Sign In</a></div>
			</div> <!-- End Tile-->
			<div class='tile'>
				<div class='clear'></div>
				<div id='FooterTile' class='footer'>
					<div class="footer_left"></div>
					<div class="footer_content">
						<img src="img/powered_by.png" class="powered_by" />
						&nbsp;&nbsp;<a href="#" onclick="setActiveStyleSheet('color1'); return false;"><img src="img/color1_selection.png" alt="Turn Site Orange" border='0'/></a>
						&nbsp;&nbsp;<a href="#" onclick="setActiveStyleSheet('color2'); return false;"><img src="img/color2_selection.png" alt="Turn Site Blue" border='0'/></a>
					</div>
				</div>  <!-- End of <div class='footer'> -->
			</div> <!-- End of <div class='tile'> -->
		</div>
	</body>
</html>