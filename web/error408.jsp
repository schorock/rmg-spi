<%@page import="net.inov.tec.security.authenticator.AuthenticationAlertCache"%>
<%@page import="net.inov.tec.security.authenticator.AuthenticationAlert"%>
<%@page import="net.inov.tec.security.SecurityManager"%>
<%@page import="com.iscs.common.render.VelocityTools"%>
<%@page import="com.iscs.common.tech.web.analytics.AnalyticsSettings"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.net.InetAddress"%>
<%@page import="javax.servlet.http.Cookie"%>

<%@page contentType="text/html; charset=utf-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%

	pageContext.getSession().setAttribute("HttpErrorMessage", "The time allowed for the login process has been exceeded. Please try again");

%>
	
<html>

<head>
	<title>HTTP Error</title>
	
	
	<!-- Internet explorer suppresses custom error pages if they are under a certain number of bytes -->
	<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
	<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
	<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
	<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
	<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
	<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
	<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
	<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
	<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
	<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
	
	
	<meta http-equiv="refresh" content="1;url=/innovation?rq=COUserStartPage">
	
<%
    AnalyticsSettings analyticsSettings = AnalyticsSettings.getSettings();
    if (analyticsSettings.isEnabled()) {
    	VelocityTools vt = new VelocityTools();
%>
<script type="text/javascript">

  var analyticsPageName = "error408.jsp/" + document.URL;
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '<%= analyticsSettings.getGoogleAnalyticsSettings().getWebPropertyId() %>'],
            ['_setDomain', '<%= analyticsSettings.getGoogleAnalyticsSettings().getDomainName() %>']);
  _gaq.push(['_setVar', ''],
            ['_setCustomVar', 1, 'UserType', '', 3],
            ['_setCustomVar', 2, 'User', '', 3],
            ['_setCustomVar', 3, 'AppServer', '<%= vt.getHostName().replace(" ","_") %>', 3],
            ['_setCustomVar', 4, 'Version', '<%= vt.getSPIVersion() %>', 3]);
  _gaq.push(['_trackPageview', '/innovation/' + analyticsPageName]);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<%
    }
%>
</head>
<body />
</html>
     