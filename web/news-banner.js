function displayNewsBanners(activeNewsBanners) {
	var newsBannerArray = activeNewsBanners.split('|');
	for( var i = 0; i < newsBannerArray.length; i++) {
		displayNewsBanner(newsBannerArray[i]);
	}
	displayNoNewsBanner();
}

function displayNewsBanner(id) {
	var value = readCookie(id);
  	if( value )
  		var display = value;
  	else
		var display = "";
		
	if( document.getElementById(id) ) {
		document.getElementById(id).style.display = display;
	}
}

// nodeOrId can be either a DOM node (e.g. this) or the id of an element.  
function dismissNewsBanner(nodeOrId, days) {
	var id = null;
    if (isString(nodeOrId)) {
    	id = nodeOrId;
    } else {
		var parent = nodeOrId.parentNode;
		while( parent ) {
			if( parent.className == "newsBanner" ) {
				id = parent.id;
				break;
			}
			parent = parent.parentNode;
		}
	}
	if (id) {
		createCookie(id, "none", days);
		displayNewsBanner(id);
	}
	displayNoNewsBanner();
}	

// nodeOrId can be either a DOM node (e.g. this) or the id of an element.  
function restoreNewsBanner(nodeOrId) {
	var id = null;
    if (isString(nodeOrId)) {
    	id = nodeOrId;
    } else {
		var parent = nodeOrId.parentNode;
		while( parent ) {
			if( parent.className == "newsBanner" ) {
				id = parent.id;
				break;
			}
			parent = parent.parentNode;
		}
	}
	if (id) {
		deleteCookie(id);
		displayNewsBanner(id);
	}
	displayNoNewsBanner();
}	

function displayNoNewsBanner() {
	if( document.getElementById("NoNewsBanner") ) {
		var newsBannerTile = document.getElementById("NewsBannerTile");
		var newsBannerDivs = newsBannerTile.getElementsByTagName('div');
		var displayNoNewsBanner = true;
		for( var i = 0; i < newsBannerDivs.length; i++ ) {
			var newsBannerId = newsBannerDivs[i].id;
			if( newsBannerId.indexOf("NewsBanner") == 0 ) {
				if( document.getElementById(newsBannerId).style.display != 'none' ) {
					displayNoNewsBanner = false;
					break;
				}
			}
		}
		if( displayNoNewsBanner ) 
			document.getElementById("NoNewsBanner").style.display = "";
		else
			document.getElementById("NoNewsBanner").style.display = "none";
	}	
}

function isString() {
	if( typeof arguments[0] == 'string' ) 
		return true;
	if( typeof arguments[0] == 'object' ) {
		var constructor = arguments[0].constructor;
        if( constructor ) {
			var criterion = constructor.toString().match(/string/i);
            return (criterion != null);
		}
	}
	return false;
}