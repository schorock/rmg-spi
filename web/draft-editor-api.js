/**
 * Bi-directional JavaScript Remote Method Invocation.
 * Depending on browser support uses Cross-Domain Messaging (typically IE8+/FF3+/Chrome)
 * or HTTP Form POST and GET (IE7).
 *
 * @class Thunderhead.RMI
 * @static
 */
if (typeof Thunderhead === 'undefined') { Thunderhead = {}; }
Thunderhead.RMI = {

    /**
     * The timeout (in milliseconds) for calls to return, after this time expires
     * the callback (if exists) will be called with "false" to indicate failure.
     */
    DEFAULT_TIMEOUT: 10000,

    config: {
        container: document.body,
        visible: false,
        remoteProxy: '',
        commandWindowName: '',
        remoteWindowName: '',
        method: 'post',
        thunderheadServer: '',
        clientServer: ''
    },

    initialized: false,

    commands: null,

    init: function (config) {
        if (config && typeof config === 'object') {
            var p;
            for (p in config) {
                if (config.hasOwnProperty(p)) {
                    this.config[p] = config[p];
                }
            }
        }

        this.commands = config.commands || {};

        // Detect availability of HTML5 messaging
        // Use form based if we can't verify origin of messages due to missing configuration
        this._formBased = !this.isMessagingSupported() || !(config.thunderheadServer || config.clientServer);

        // Fall back to form-based if not available
        if (this._formBased) {
            this._initFormBased();
        } else {
            this._expectedMessageOrigin = config.thunderheadServer || config.clientServer;
            this._initMessagingBased();
        }
    },

    /**
     * Change the currently assigned command set.
     *
     * @param {Object} commands Object containing functions that may be executed via RMI
     */
    setCommands: function (commands) {
        this.commands = commands;
    },

    /**
     * Returns true if the browser supports HTML5 cross domain messaging.
     *
     * @return {boolean} True if the browser supports HTML5 messaging
     */
    isMessagingSupported: function () {
        return !!window.postMessage;
    },

    _initFormBased: function () {
        this.config.method = (this.config.method === 'post' || this.config.method === 'get') ? this.config.method : 'post';
        this.config.commandFormName = this.config.commandWindowName + '__Form';
        this._rmiDiv = document.createElement('div');
        var html = '<div id="thunderheadRMI" style="' + ((this.config.visible) ? '' : 'position:absolute;width:1px;height:1px;top:0px;left:-999px') + '">';
        html += '	<iframe name="' + this.config.commandWindowName + '" src="" frameborder="1" width="100%" height="100" tabindex="-1"></iframe>';
        html += '	<form id="' + this.config.commandFormName + '" action="' + this.config.remoteProxy + '" method="' + this.config.method + '" target="' + this.config.commandWindowName + '">';
        html += '		<input type="hidden" name="command" />';
        html += '		<input type="hidden" name="object" />';
        html += '		<input type="hidden" name="callback" />';
        html += '		<input type="hidden" name="windowName" value="' + this.config.remoteWindowName + '" />';
        html += '	</form>';
        html += '</div>';
        this._rmiDiv.innerHTML = html;
        this.config.container = document.getElementById(this.config.container) || document.body;
        if (this.config.container !== null) {
            this.config.container.appendChild(this._rmiDiv);
            this._commandForm = document.getElementById(this.config.commandFormName);
            this.initialized = true;
        }
    },

    _initMessagingBased: function () {
        var self = this;
        self._messageListener = function (event) {
            // Important:  Verify message origin
            event = event || window.event;
            if (event.origin === self._expectedMessageOrigin) {
                var data = self._decodeJSON(event.data);
                self.receiveCommand(data.command, data.args, data.callback);
            }  //else {
            //console.log('Received message from: ' + event.origin + ', ignoring!  (expect ' + self._expectedMessageOrigin + ')');
            //}
        };

        if (window.addEventListener) {
            // all browsers except IE before version 8
            window.addEventListener('message', self._messageListener, false);
        } else {
            // IE before version 8
            window.attachEvent('onmessage', self._messageListener);
        }
        this.initialized = true;
    },

    /**
     * Returns true if the RMI library was properly initialised.
     *
     * @return {boolean} True if this was initialised properly.
     */
    isInitialized: function () {
        return this.initialized;
    },

    /**
     * Returns true if the RMI framework has detected the availability
     * of HTML5 cross domain messaging & is using this for RMI calls.
     *
     * @return {boolean} True if the library is using HTML5 messaging
     */
    isMessagingBased: function () {
        if (!this.initialized) {
            throw new Error('Thunderhead.RMI is not initialized');
        }
        return !this._formBased;
    },

    /*
     * Removes the HTML from the page and the initialized commands 
     */
    destroy: function () {
        if (this.initialized !== true) {
            return;
        }

        if (this._formBased) {
            this._rmiDiv.parentNode.removeChild(this._rmiDiv);
        } else {
            if (window.removeEventListener) {
                window.removeEventListener('message', this._messageListener, false);
            } else {
                window.detachEvent('onmessage', this._messageListener);
            }
        }

        this.initialized = false;
        this.commands = {};
    },

    /**
     * Receives a command.
     *
     * @param {String} command Command Name received
     * @param {String} param Command parameter
     * @param {String} callback Optional name of a function to callback.
     */
    receiveCommand: function (command, param, callback) {
        var self = this;
        if (!self.initialized) {
            //alert('Thunderhead.RMI not initialized!');
            return false;
        }
        var fnCommand = self.commands[command];
        if (Object.prototype.toString.apply(fnCommand) === '[object Function]') {
            var args;
            // only accept primitive types, pure JSON object forbidden for JavaScript Hijacking prevention
            if (self._formBased && self._isString(param)) {
                args = self._decodeJSON(param);
                if (args.__isPrimitive === true) {
                    args = this._isString(args.primitive) ? decodeURIComponent(args.primitive) : args.primitive;
                }
            } else if (!self._formBased) {
                args = param;
            }
            // NOTE: It is currently intentional that only one argument is processed here..
            // Changing this will require changing docEditor.api.API - This relies on the fact only one
            // argument is passed back by RMI (for unit testing).
            fnCommand.call(self.commands, args, function (returnVal) {
                if (self._isString(callback) && callback !== 'undefined') {
                    self.sendCommand(callback, returnVal);
                }
            });
        }

        return true;
    },

    /**
     * Sends a command.
     *
     * @param {String} command Command Name to send
     * @param {Object} object The parameters to pass
     * @param {Function} callback The callback function
     * @param {Object} scope Optional callback scope
     */
    sendCommand: function (command, object, callback, scope) {
        var self = this;
        if (!self.initialized || (self._formBased && self._commandForm === null)) {
            //alert('Thunderhead.RMI not initialized!');
            return false;
        }

        var callbackName = callback;
        if (!this._isString(callbackName)) { // SF support using old DraftEditor/DocEditor API JS (legacy will pass string)...
            callbackName = this._storeCallbackCommand(command, callback, scope);
        }

        if (self._formBased) {
            var form = self._commandForm;
            form.elements.command.value = command;
            if (this._isPrimitive(object)) {
                object = {
                    __isPrimitive: true,
                    primitive: this._isString(object) ? encodeURIComponent(object) : object
                };
            }
            form.elements.object.value = self._encodeJSON(object);
            form.elements.callback.value = this._isString(callbackName) ? callbackName : 'undefined';
            form.submit();
        } else {
            // Message based:
            var targetName = this.config.remoteWindowName;
            var targetWindow;
            if (targetName === '') {
                // If no remote target specified then send to parent
                targetWindow = window.parent;
            } else {
                targetWindow = window.frames[targetName];
            }
            var message = {
                command: command,
                args: object,
                callback: callbackName
            };
            targetWindow.postMessage(self._encodeJSON(message), '*');
        }

        return true;
    },

    // Private/utility functions:

    /**
     * Stores a callback function into the RMI helper, to be executed when the RMI command returns.
     *
     * @param {String} name The name of the function
     * @param {Function} callbackFcn The function to execute
     * @param {Object} scope (Optional) The scope to execute the callback in
     * @return {String} The name the callback has been stored under
     */
    _storeCallbackCommand: function (name, callbackFcn, scope) {
        if (callbackFcn) {
            var callbackFunctionName;
            do {
                callbackFunctionName = name + '_' + this._getUniqueStamp();
                // Just in-case our unique stamp has recently been generated (very extreme case)
            } while (typeof Thunderhead.RMI.commands[callbackFunctionName] !== 'undefined');
            Thunderhead.RMI.commands[callbackFunctionName] = function (params) {
                delete Thunderhead.RMI.commands[callbackFunctionName];
                callbackFcn.call(scope, true, params);
            };
            // If the server doesn't return within DEFAULT_TIMEOUT ms, we cleanup the callback & let the user know
            setTimeout(function () {
                if (typeof Thunderhead.RMI.commands[callbackFunctionName] !== 'undefined') {
                    delete Thunderhead.RMI.commands[callbackFunctionName];
                    callbackFcn.call(scope, false);
                }
            }, this.DEFAULT_TIMEOUT);
            return callbackFunctionName;
        }
    },

    /**
     * On each invocation should return a unique string
     * (used as part of the name for storing callback functions).
     *
     * @return {String} A unique string
     */
    _getUniqueStamp: function () {
        return Math.floor(Math.random() * 1048576).toString();
    },

    _isPrimitive: function (item) {
        return (typeof item === 'number' && isFinite(item)) || typeof item === 'boolean' || typeof item === 'string';
    },

    _isString: function (obj) {
        return typeof obj === 'string';
    },

    _decodeJSON: function (json) {
        if (typeof Ext !== 'undefined' && typeof Ext.decode !== 'undefined') {
            return Ext.decode(json);
        }
        if (json.length === 0) {
            return json;
        }
        // Check JSON is/was protected
        // We've extended our Ext decode/encode for this, so RMI should behave in the same way
        else {
            if (json.substr(0, 2) === '/*') {
                json = json.substring(2, json.length - 2);
            }
            else if (json.substr(0, 9) === 'while(1);') {
                json = json.substring(9);
            }
            else {
                throw new SyntaxError('This JSON data is not protected against JavaScript Hijacks.');
            }
        }
        if (json !== 'undefined') {
            return JSON.parse(json);
        }
    },

    _encodeJSON: function (obj) {
        var value;
        if (typeof Ext !== 'undefined' && typeof Ext.encode !== 'undefined') {
            value = Ext.encode(obj);
            if (value.indexOf('while(1);') !== 0) {
                value = 'while(1);' + value;
            }
        }
        else {
            value = 'while(1);' + JSON.stringify(obj);
        }
        return value;
    }
};
Thunderhead = window.Thunderhead || {};
/**
 * Thunderhead DraftEditor API.<br/>
 *
 * This class is technically a "remote" interface to the API within the DraftEditor application.<br/>
 *
 * Draft Editor configuration options (should be returned as an object from a <b><u>loadStartupConfig function</u></b>):
 * <hr/>
 * <ul>
 *   <li>{Object} <b>preview</b>     - (Optional) Preview configuration
 *     <ul>
 *       <li>{Boolean} <b>disabled</b>   - (Optional) Defaults to false, true to disable the preview button</li>
 *       <li>{String} <b>forceFormat</b> - (Optional) The file format for which document previews should be loaded.</li>
 *     </ul>
 *   </li>
 *   <li>{Boolean} <b>forceToggleShowChoices</b> - (Optional) If defined, this will force toggle the 'Show Choices' button on (true), or off (false).</li>
 *   <li>{Object} <b>sidePanel</b> - (Optional) A configuration object containing properties to define the side panel:
 *     <ul>
 *       <li>{String} <b>location</b> - (Optional) The location of the side panel left or right (defaults to right)</li>
 *       <li>{Array}  <b>tabs</b>     - (Optional) List of tabs that should be shown, for default do not specify this option.</li>
 *       <li>{String} <b>open</b>     - (Optional) The tab that should be open on starting the application, if not specified will use default ('Documents').</li>
 *       <li>{Number} <b>width</b>    - (Optional) The width (px) of the side panel (defaults to 500)</li>
 *       <li>{Number} <b>minWidth</b> - (Optional) The minimum expandable width (px) of the side panel (defaults to 200)</li>
 *       <li>{Number} <b>maxWidth</b> - (Optional) The maximum expandable width (px) of the side panel (defaults to 700)</li>
 *     </ul>
 *   </li>
 *   <li>{Object}  <b>tools</b>    - (Optional) A configuration object for enabling/disabling available tools
 *     <ul>
 *       <li>{Array} <b>enabled</b>   - (Optional) A list of enabled tools (if specified, tools not listed will be disabled)</li>
 *       <li>{Array} <b>disabled</b>  - (Optional) A list of disabled tools (if specified, tools not listed will be enabled)</li>
 *     </ul>
 *   </li>
 *   <li>{Boolean} <b>openInPreview</b>         - (Optional, defaults to false) - Will open the preview of the document once loaded.</li>
 *   <li>{Boolean} <b>focusOnFirstEditPoint</b> - (Optional, defaults to true)  - Will place the cursor at the first edit point in the document.</li>
 *   <li>{Boolean} <b>showTrackChanges</b>      - (Optional, defaults to false) - Whether to open the draft with track changes displayed, or not.</li>
 * </ul>
 * <hr/>
 *
 * @remarks Omit specifying clientServer and thunderheadServer if you are on the same host/port as Draft-Editor, this will enable "local mode".
 *
 * @param {Object} config Configuration for the Draft-Editor
 *   @config {String} thunderheadServer (optional) The protocol (http/https), Thunderhead Server host, & port if not 80/443
 *   @config {String} clientServer (optional) The protocol (http/https), Client Server host, & port if not 80/443 - used to validate origin of API messages
 *   @config {String} targetElementID (optional) The ID of the element which is to contain the Thunderhead iFrame (defaults to document.body)
 *   @config {String} authUrl (optional) Used to authenticate the user before loading Draft editor (URL format "authUrl?state=draftEditoAppUrl&authParams")
 *   @config {String} authParams (optional) Used to provide additional information to the above authentication URL
 *   @config {Function} loadStartupConfig (optional) A function that should return the Draft-Editor UI configuration, see class JS-Doc.
 *   @config {Function} onReady (optional) A function to execute once the Draft-Editor is loaded, e.g. to contain initial API calls
 *
 * @class
 */
Thunderhead.DraftEditor = function(config) {
    this.initialize(config);
};

// Legacy
Thunderhead.DocEditor = Thunderhead.DraftEditor;

Thunderhead.DraftEditor.prototype = {

    WINDOW_NAME: 'thunderheadDraftEditorWindow',
    // TODO - *** We should probably have the users specify the context?
    DRAFTEDITOR_CONTEXT: '/draft-editor',
    DRAFTEDITOR_PAGE_NAME: 'draft-editor.jsp',

    // Legacy
    DOCEDITOR_CONTEXT: null,

    /**
     * Set of listener functions added by the user for events that may be
     * fired by the draft-editor via the RMI framework.
     * @type {Object} (set of EventName -> Array)
     */
    _listeners: null,

    EVENTS: {
        /**
         * Fired when unposted comments have been found
         */
        'unpostedCommentFound': function() {},
        /**
         * Fired when the dirty state of the draft changes (i.e. has been modified, or
         * dirty state has been cleared/reset).
         * @param {Boolean} newState The new state (dirty/not-dirty)
         */
        'dirty': function(newState) {},
        /**
         * Fired when a new comment is added by the user
         */
        'commentsDirty': function () {},
        /**
         * Fired when an attempt to edit the document is raised.
         */
        'editingAttempted': function() {}
    },

    /**
     * Initialise the DraftEditor API for use, MUST be called before using this API.
     *
     * @remarks This method uses the same config object than Thunderhead.DraftEditor.constructor.
     */
    initialize: function(config) {

        config = config || {};
        // Legacy support
        this.DRAFTEDITOR_CONTEXT = this.DOCEDITOR_CONTEXT || this.DRAFTEDITOR_CONTEXT;

        this._listeners = {};

        this._thunderheadServer = config.thunderheadServer;
        this._thunderheadServerDomain = (this._thunderheadServer ? this._thunderheadServer : '');

        config.isLocal = this._local = !config.clientServer;

        if (!config.isLocal) {

            // Ensure the user has specified both client & server, we only support HTML5 messaging so they are both required
            if (!config.thunderheadServer) {
                throw new Error('Draft-Editor configuration is missing the "thunderheadServer" parameter. This parameter is required when the "clientServer" parameter is provided.');
            }
            var thunderheadServer = config.thunderheadServer;

            Thunderhead.RMI.init({
                    // The container where the hidden command form should be located
                    container: document.body,
                    // The name of the window containing the DraftEditor (Review Case Editor) application
                    remoteWindowName: this.WINDOW_NAME,
                    // The name used for the form that will be posting the commands
                    commandWindowName: 'thunderheadRMICommandForm',
                    // Used for HTML5 message origin verification (when the server calls back to us):
                    thunderheadServer: thunderheadServer,
                    // Will be used to store temporary callback functions (this object is used when receiving commands from the draft-editor side)
                    commands: this._getClientCommands(config)
            });
            this.initialized = Thunderhead.RMI.isInitialized();
            if (!this.initialized) {
                throw new Error('Could not initialize RMI framework.');
            }

        } else {

            // In local style access, the iFrame (Draft-Editor server) can access our window
            // and will just call these commands directly.
            Thunderhead.RMI.setCommands(this._getClientCommands(config));
            this.initialized = true;

        }

        this._createApplicationWindow(config);
    },

    destroy: function () {

        Thunderhead.RMI.destroy();

        var iFrame = document.getElementById(this.WINDOW_NAME);

        if (iFrame) {
            iFrame.parentNode.removeChild(iFrame);
        }
    },

    /**
     * Fetches the object to use as the initial set of client-side command functions.
     *
     * @param {Object} config The draft-editor initialization config object
     * @return {Object} The command set to initialize the RMI framework with
     */
    _getClientCommands: function(config) {
        var scope = this;
        return {
            'onReady': function() {
                if (config.onReady) {
                    config.onReady();
                }
            },
            'loadStartupConfig': function(args, callback) {
                if (config.loadStartupConfig) {
                    callback(config.loadStartupConfig());
                }
            },
            'fireEvent': function(args, callback) {
                var listeners = scope._listeners[args.eventName];
                if (listeners) {
                    var i, ii;
                    for (i = 0, ii = listeners.length; i < ii; i++) {
                        if (listeners[i](args.params) === false) {
                            return;
                        }
                    }
                }
            }
        };
    },

    /**
     * Creates an iFrame containing the Thunderhead Draft-Editor application in the specified element.
     *
     * @param {Object} config Configuration object to be passed to the draft-editor, must contain targetElementID property.
     *   @config {String} targetElementID The ID of the element which is to contain the Thunderhead iFrame.
     */
    _createApplicationWindow: function(config) {
        this._canUseAPI();
        var targetEl;

        if (config.hasOwnProperty('targetElementID')) {
            targetEl = document.getElementById(config.targetElementID);
            if (!targetEl) {
                throw new Error('Cannot find element with ID "' + config.targetElementID + '"');
            }
            delete config.targetElementID;
        }
        else  {
            targetEl = document.body;
        }

        var iFrame = document.createElement('iframe');
        iFrame.setAttribute('name', this.WINDOW_NAME);
        iFrame.setAttribute('id', this.WINDOW_NAME);
        iFrame.setAttribute('title', 'Draft-Editor');
        iFrame.setAttribute('frameborder', 0);
        iFrame.setAttribute('width', '100%');
        iFrame.setAttribute('height', '100%');
        
        var startupConfig = '';
        if (config.debugMode === true) {
            startupConfig = 'debug=true&';
        }
        if (!config.isLocal) {
            startupConfig += 'clientServer=' + encodeURIComponent(config.clientServer);
        } else {
            startupConfig += 'isLocal=true';
        }

        // It isn't nice having to fetch this to check, but does allow us to deduce this information with no changes
        // to existing deployments (legacy is now required to be known on the JSP to initialise the resources, see draft-editor.jsp).
        var fullConfig = config.loadStartupConfig ? config.loadStartupConfig() : null;
        if (fullConfig && fullConfig.webServiceRoot) {
            if (fullConfig.webServiceRoot.indexOf('/thunderhead-doceditor-rest') === 0) {
                startupConfig += '&legacy=true';
            }
        }

        var appUrl = this._thunderheadServerDomain + this.DRAFTEDITOR_CONTEXT + '/' + this.DRAFTEDITOR_PAGE_NAME + '?' + startupConfig;

        if (config.authUrl && config.authParams){
			iFrame.setAttribute('src', config.authUrl + '?state=' + encodeURIComponent(appUrl) + '&' + config.authParams);
		} else {
            iFrame.setAttribute('src', appUrl);
        }

        targetEl.appendChild(iFrame);
    },

    /**
     * Returns true if the API was properly initialized.
     *
     * @return {boolean} True if this was initialized properly.
     */
    isInitialized: function() {
        return this.initialized === true;
    },

    // TODO - Consider refactoring out either the below code, or above if the API becomes richer, to keep this class size down.
    
    /* -- Public API, to interact with created application window --

          All API calls are asynchronous and will return immediately, a callback function should be used
          to if you require to know when/if the call completes, or if data is to be returned.

          The first parameter given to ALL callback functions is "success", if this is false it means the
          API call timed out before returning from the server (see DEFAULT_TIMEOUT) - if required the
          default timeout can be changed using .setTimeout(milliseconds).
     */

    /**
     * Sets the default timeout used when performing API calls.
     * Must be greater than 100ms.
     *
     * @param {Number} timeout The timeout to use (in milliseconds)
     */
    setTimeout: function(timeout) {
        if (timeout > 100) {
            Thunderhead.RMI.DEFAULT_TIMEOUT = timeout;
        }
    },

    /*
        Current event list:

          dirty(newState):  Fired when the Draft-Editor changes state from NotDirty[False] <--> Dirty[True].
                            Can be reset using .clearDirtyState(..) API call.
     */

    /**
     * Adds a listener for the given event name.
     *
     * @param {String} eventName The event name to listen for e.g. "dirty"
     * @param {Function} listener The function to call when the event occurs
     * @param {Object} scope Optional scope to call the listener within
     */
    addListener: function(eventName, listener, scope) {
        if (!this._listeners[eventName]) {
            this._listeners[eventName] = [];
        }
        scope = scope || this;
        this._listeners[eventName].push(function() {
            return listener.apply(scope, arguments);
        });
    },

    /**
     * Loads and serialize to string a review case at given URL.
     *
     * @param {String} url the local URL of the review case XML.
     * @param callback
     */
    _getSerializedReviewCaseFromURL: function (url, callback) {
        var me = this;
        var xmlObj = me.getXMLHttpRequest();
        xmlObj.open('GET', url, true);
        xmlObj.onreadystatechange = function() {
            if (xmlObj.readyState === 4 && xmlObj.status === 200) {
                me._processSerializedReviewCaseResponse(xmlObj.responseXML, callback);
            }
        };     
        xmlObj.send();
    },

    /**
     * Process response and call callback appropriately
     * @param xmlDoc
     * @param callback
     * @private
     */
    _processSerializedReviewCaseResponse: function(xmlDoc, callback) {
        if (callback) {
            if (xmlDoc && xmlDoc.documentElement.tagName === 'review-case') {
                var xml = xmlDoc.outerXML || xmlDoc.xml;
                if (xml) {
                    callback(xml);
                } else if (window.XMLSerializer) {
                    callback(new XMLSerializer().serializeToString(xmlDoc));
                }
            }
            else {
                callback(null);
            }
        }
    },

    /**
     * Loads a ReviewCase from the given URL.
     * (currently must be on the same domain).
     *
     * @param {String} url The URL of the ReviewCase XML
     * @param {Boolean} openInPreview Optional, default is false -Whether to open doc in preview mode.
     * @param {Function} callback (Optional) Optional callback to execute when the load is completed (if call times out, will be called with false)
     * @param {Object} scope (Optional) The scope to execute the callback in
     * @throws (Exception) Error If the API has not been initialized (or failed to initialize)
     */
    loadURL: function(url, openInPreview, callback, scope) {
        var me = this;
        if (this._local) {
            this._sendCommand('loadURL', {
                url: url,
                openInPreview: openInPreview
            }, callback, scope);
        }
        else {
            this._getSerializedReviewCaseFromURL(url, function(rc) {
                if (rc) {
                    me.loadString(rc, openInPreview, callback, scope);
                }
            });
        }
    },

    /**
     * Loads a ReviewCase from the given URL.
     * (currently must be on the same domain).
     *
     * @param {String} url The URL of the ReviewCase XML
     * @param {Boolean} openInPreview Optional, default is false -Whether to open doc in preview mode.
     * @param {Number} projectId The id of the project to open the review case within
     * @param {Function} callback (Optional) Optional callback to execute when the load is completed (if call times out, will be called with false)
     * @param {Object} scope (Optional) The scope to execute the callback in
     * @throws (Exception) Error If the API has not been initialized (or failed to initialize)
     */
    loadURLInProjectScope: function(url, openInPreview, projectId, callback, scope) {
        var me = this;
        if (this._local) {
            this._sendCommand('loadURL', {
                url: url,
                openInPreview: openInPreview,
                projectId: projectId
            }, callback, scope);
        }
        else {
            this._getSerializedReviewCaseFromURL(url, function(rc) {
                if (rc) {
                    me.loadStringInProjectScope(rc, openInPreview, projectId, callback, scope);
                }
            });
        }
    },

    /**
     * Loads a ReviewCase using the given string.
     *
     * @param {String} reviewCase The Review Case XML as a String.
     * @param {Boolean} openInPreview Optional, default is false -Whether to open doc in preview mode.
     * @param {Function} callback (Optional) Optional callback to execute when the load is completed (if call times out, will be called with false)
     * @param {Object} scope (Optional) The scope to execute the callback in
     * @throws (Exception) Error If the API has not been initialized (or failed to initialize)
     */
    loadString: function(reviewCase, openInPreview, callback, scope) {
        this._sendCommand('loadString', {
                reviewCase: reviewCase,
                openInPreview: openInPreview
            }, callback, scope);
    },

    /**
     * Loads a ReviewCase in project scope using the given string.
     *
     * @param {String} reviewCase The Review Case XML as a String.
     * @param {Boolean} openInPreview Optional, default is false -Whether to open doc in preview mode.
     * @param {Number} projectId The id of the project to open the review case within
     * @param {Function} callback (Optional) Optional callback to execute when the load is completed (if call times out, will be called with false)
     * @param {Object} scope (Optional) The scope to execute the callback in
     * @throws (Exception) Error If the API has not been initialized (or failed to initialize)
     */
    loadStringInProjectScope: function(reviewCase, openInPreview, projectId, callback, scope) {
        this._sendCommand('loadString', {
                reviewCase: reviewCase,
                openInPreview: openInPreview,
                projectId: projectId
            }, callback, scope);
    },

    /**
     * Saves the ReviewCase by posting the data to the URL given, will be POSTed to the URL.
     *
     * Callback parameters:  success, postSuccess
     *
     * @param {String} url The URL to POST the Review Case to.
     * @param {Function} callback (Optional) Optional callback to execute when the post is completed (if call times out, will be called with just false), if post or review case save fails will be called with true, false
     * @param {Object} scope (Optional) The scope to execute the callback in
     * @throws (Exception) Error If the API has not been initialized (or failed to initialize)
     */
    saveURL: function(url, callback, scope) {
        var me = this;
        if (this._local) {
            this._sendCommand('saveURL', url, callback, scope);
        } else {
            this.getString(function(success, reviewCase) {
                if (success) {
                    me._sendRequest('POST', url, reviewCase, function (status, responseXML) {
                        callback.call(scope || window, true, status === 200, responseXML);
                    });
                }
            });
        }
    },

    /**
     * Safely gets request object depending on browser
     * @return {*}
     */
    getXMLHttpRequest: function () {
        if (!window.XMLHttpRequest) {
            // IE
            return this._getMicrosoftXMLDOM();
        }
        else {
            //Other Browsers
            return new XMLHttpRequest();
        }
    },

    /**
     * Sends XML to the specified URL.
     *
     * @param {String} type 'GET'/'SET'
     * @param {String} url The URL for the XML
     * @param reviewCase
     * @param callback
     */
    _sendRequest: function (type, url, reviewCase, callback) {
        var xmlObj;
        xmlObj = this.getXMLHttpRequest();

        xmlObj.onreadystatechange = function() {
            if (xmlObj.readyState === 4 && xmlObj.status === 200) {
                callback(xmlObj.status, xmlObj.responseXML);
            }
        };
        xmlObj.open(type, url, true);
        xmlObj.setRequestHeader("Content-Type", "application/xml; charset=utf-8");
        xmlObj.send(reviewCase);
    },

    /**
     * Creates and initialises a Microsoft XML DOM (ActiveXObject).
     *
     * @return {ActiveXObject} Microsoft DOM
     */
    _getMicrosoftXMLDOM: function() {
        var progIDs = ['Microsoft.XMLDOM', 'Msxml2.DOMDocument.6.0', 'Msxml2.DOMDocument.3.0'];
        var xmlDoc, i;
        for (i = 0; i < progIDs.length; i++) {
            try {
                xmlDoc = new ActiveXObject(progIDs[i]);
                xmlDoc.async = true;
                xmlDoc.preserveWhiteSpace = true;
                break;
            }
            catch (ex) {
            }
        }
        return xmlDoc;
    },

    /**
     * Fetches the Review Case XML as a string.
     *
     * Callback parameters: success (i.e. call didn't time out), reviewCase
     *
     * @param {Function} callback The callback to execute when the API call returns, (if call times out, will be called with just false), if review case save fails will be called with true, false
     * @param {Object} scope (Optional) The scope to execute the callback in
     * @throws (Exception) Error If the API has not been initialized (or failed to initialize)
     */
    getString: function(callback, scope) {
        this._sendCommand('getString', {}, callback, scope);
    },

    /**
     * Resets the Draft-Editor "dirty" state back to "not dirty" [false].
     *
     * @param {Function} callback Optional callback to execute once completed
     * @param {Object} scope (Optional) The scope to execute the callback in
     */
    clearDirtyState: function(callback, scope) {
        this._sendCommand('clearDirtyState', {}, callback, scope);
    },

    /**
     * Sets/clears read-only mode for the Draft.
     *
     * @param {Function} callback Optional callback to execute once completed
     * @param {Object} scope (Optional) The scope to execute the callback in
     */
    setReadOnly: function(callback, scope) {
        this._sendCommand('setReadOnly', {}, callback, scope);
    },

    /**
     * Fetches the current state of "show track changes",
     * true if being displayed, otherwise false.
     *
     * @param {Function} callback Optional callback to execute (with the track change state true/false)
     * @param {Object} scope (Optional) The scope to execute the callback in
     */
    isShowTrackChanges: function(callback, scope) {
        this._sendCommand('isShowTrackChanges', {}, callback, scope);
    },

    /**
     * Turns on/off the display of track changes within the draft document.
     *
     * @param {Boolean} show True to display, false to hide track changes
     * @param {Function} callback Optional callback to execute once completed
     * @param {Object} scope (Optional) The scope to execute the callback in
     */
    setShowTrackChanges: function(show, callback, scope) {
        this._sendCommand('setShowTrackChanges', show, callback, scope);
    },

    /**
     * Send a command to refresh the comments list in the user interface.
     *
     * @param {Function} callback Optional callback to execute once completed
     * @param {Object} scope (Optional) The scope to execute the callback in
     */
    refreshReviewComments: function(callback, scope) {
         this._sendCommand('refreshReviewComments', {}, callback, scope);
    },

    /**
     * Activates the content editor and enables blurring
     *
     * @param {Boolean} preventCaretRefresh (optional) Prevents scroll to the previous caret position on the frag (when true)
     */
    activate: function(preventCaretRefresh, callback, scope) {
        this._sendCommand('activate', {preventCaretRefresh: preventCaretRefresh}, callback, scope);
    },

    /**
     * Activates the content editor and enables blurring
     */
    deactivate: function(callback, scope) {
        this._sendCommand('deactivate', {}, callback, scope);
    },

    /**
     * Deactivates CE and prevents blurring
     *
     */
    deactivateAndPreventBlur: function(callback, scope){
        this._sendCommand('deactivateAndPreventBlur', {}, callback, scope);
    },

    /**
     * Gets the Audit report containing the user changes for the loaded review case.
     *
     * @param {String} user The user ID used to filter the audit report.
     * If not "null", only the changes from this user will be reported.
     * @param {Function} callback Optional callback to execute once completed
     * @param {Object} scope (Optional) The scope to execute the callback in
     */
    getAuditXML: function (user, callback, scope) {
        this._sendCommand('getAuditXML', user, callback, scope);
    },

    /* -- Private Functions -- */

    /**
     * If using remote invocation will send the command via the RMI framework,
     * else will access the API function directly.
     *
     * @param {String} commandName The name of the command to invoke
     * @param {String} args The parameters to send with the command
     * @param {Function} callback The callback to execute once finished
     * @param {Object} scope (Optional) The scope to execute the callback in
     */
    _sendCommand: function(commandName, args, callback, scope) {
        scope = scope || this;
        if (this._local) {
            // Invoke command directly (we can only do this if we're on the same domain & port)
            var remoteCommands = window.frames[this.WINDOW_NAME].Thunderhead.RMI.commands;
            remoteCommands[commandName].call(remoteCommands, args, function(params) {
                if (callback) {
                    callback.call(scope, true, params);
                }
            });
        } else {
            Thunderhead.RMI.sendCommand(commandName, args, callback, scope);
        }
    },

    /**
     * Checks whether the API was initialized properly and throws an Exception if not.
     *
     * @throws {Error} Exception If the API was not initialised
     */
    _canUseAPI: function() {
        if (!this.isInitialized()) {
            throw new Error('API not initialized.');
        }
    }

};
 
/**
 * @coverage-exclude
 */

/*jslint evil: true, strict: false, regexp: false */

/*members "", "\b", "\t", "\n", "\f", "\r", "\"", JSON, "\\", apply,
    call, charCodeAt, getUTCDate, getUTCFullYear, getUTCHours,
    getUTCMinutes, getUTCMonth, getUTCSeconds, hasOwnProperty, join,
    lastIndex, length, parse, prototype, push, replace, slice, stringify,
    test, toJSON, toString, valueOf
*/


// Create a JSON object only if one does not already exist. We create the
// methods in a closure to avoid creating global variables.

if (typeof JSON === 'undefined') {
    JSON = {};
}

(function () {
    "use strict";

    function f(n) {
        // Format integers to have at least two digits.
        return n < 10 ? '0' + n : n;
    }

    if (typeof Date.prototype.toJSON !== 'function') {

        Date.prototype.toJSON = function (key) {

            return isFinite(this.valueOf()) ?
                this.getUTCFullYear()     + '-' +
                f(this.getUTCMonth() + 1) + '-' +
                f(this.getUTCDate())      + 'T' +
                f(this.getUTCHours())     + ':' +
                f(this.getUTCMinutes())   + ':' +
                f(this.getUTCSeconds())   + 'Z' : null;
        };

        String.prototype.toJSON      =
            Number.prototype.toJSON  =
            Boolean.prototype.toJSON = function (key) {
                return this.valueOf();
            };
    }

    var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        gap,
        indent,
        meta = {    // table of character substitutions
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"' : '\\"',
            '\\': '\\\\'
        },
        rep;


    function quote(string) {

// If the string contains no control characters, no quote characters, and no
// backslash characters, then we can safely slap some quotes around it.
// Otherwise we must also replace the offending characters with safe escape
// sequences.

        escapable.lastIndex = 0;
        return escapable.test(string) ? '"' + string.replace(escapable, function (a) {
            var c = meta[a];
            return typeof c === 'string' ? c :
                '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
        }) + '"' : '"' + string + '"';
    }


    function str(key, holder) {

// Produce a string from holder[key].

        var i,          // The loop counter.
            k,          // The member key.
            v,          // The member value.
            length,
            mind = gap,
            partial,
            value = holder[key];

// If the value has a toJSON method, call it to obtain a replacement value.

        if (value && typeof value === 'object' &&
                typeof value.toJSON === 'function') {
            value = value.toJSON(key);
        }

// If we were called with a replacer function, then call the replacer to
// obtain a replacement value.

        if (typeof rep === 'function') {
            value = rep.call(holder, key, value);
        }

// What happens next depends on the value's type.

        switch (typeof value) {
        case 'string':
            return quote(value);

        case 'number':

// JSON numbers must be finite. Encode non-finite numbers as null.

            return isFinite(value) ? String(value) : 'null';

        case 'boolean':
        case 'null':

// If the value is a boolean or null, convert it to a string. Note:
// typeof null does not produce 'null'. The case is included here in
// the remote chance that this gets fixed someday.

            return String(value);

// If the type is 'object', we might be dealing with an object or an array or
// null.

        case 'object':

// Due to a specification blunder in ECMAScript, typeof null is 'object',
// so watch out for that case.

            if (!value) {
                return 'null';
            }

// Make an array to hold the partial results of stringifying this object value.

            gap += indent;
            partial = [];

// Is the value an array?

            if (Object.prototype.toString.apply(value) === '[object Array]') {

// The value is an array. Stringify every element. Use null as a placeholder
// for non-JSON values.

                length = value.length;
                for (i = 0; i < length; i += 1) {
                    partial[i] = str(i, value) || 'null';
                }

// Join all of the elements together, separated with commas, and wrap them in
// brackets.

                v = partial.length === 0 ? '[]' : gap ?
                    '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']' :
                    '[' + partial.join(',') + ']';
                gap = mind;
                return v;
            }

// If the replacer is an array, use it to select the members to be stringified.

            if (rep && typeof rep === 'object') {
                length = rep.length;
                for (i = 0; i < length; i += 1) {
                    if (typeof rep[i] === 'string') {
                        k = rep[i];
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            } else {

// Otherwise, iterate through all of the keys in the object.

                for (k in value) {
                    if (Object.prototype.hasOwnProperty.call(value, k)) {
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            }

// Join all of the member texts together, separated with commas,
// and wrap them in braces.

            v = partial.length === 0 ? '{}' : gap ?
                '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}' :
                '{' + partial.join(',') + '}';
            gap = mind;
            return v;
        }
    }

// If the JSON object does not yet have a stringify method, give it one.

    if (typeof JSON.stringify !== 'function') {
        JSON.stringify = function (value, replacer, space) {

// The stringify method takes a value and an optional replacer, and an optional
// space parameter, and returns a JSON text. The replacer can be a function
// that can replace values, or an array of strings that will select the keys.
// A default replacer method can be provided. Use of the space parameter can
// produce text that is more easily readable.

            var i;
            gap = '';
            indent = '';

// If the space parameter is a number, make an indent string containing that
// many spaces.

            if (typeof space === 'number') {
                for (i = 0; i < space; i += 1) {
                    indent += ' ';
                }

// If the space parameter is a string, it will be used as the indent string.

            } else if (typeof space === 'string') {
                indent = space;
            }

// If there is a replacer, it must be a function or an array.
// Otherwise, throw an error.

            rep = replacer;
            if (replacer && typeof replacer !== 'function' &&
                    (typeof replacer !== 'object' ||
                    typeof replacer.length !== 'number')) {
                throw new Error('JSON.stringify');
            }

// Make a fake root object containing our value under the key of ''.
// Return the result of stringifying the value.

            return str('', {'': value});
        };
    }


// If the JSON object does not yet have a parse method, give it one.

    if (typeof JSON.parse !== 'function') {
        JSON.parse = function (text, reviver) {

// The parse method takes a text and an optional reviver function, and returns
// a JavaScript value if the text is a valid JSON text.

            var j;

            function walk(holder, key) {

// The walk method is used to recursively walk the resulting structure so
// that modifications can be made.

                var k, v, value = holder[key];
                if (value && typeof value === 'object') {
                    for (k in value) {
                        if (Object.prototype.hasOwnProperty.call(value, k)) {
                            v = walk(value, k);
                            if (v !== undefined) {
                                value[k] = v;
                            } else {
                                delete value[k];
                            }
                        }
                    }
                }
                return reviver.call(holder, key, value);
            }


// Parsing happens in four stages. In the first stage, we replace certain
// Unicode characters with escape sequences. JavaScript handles many characters
// incorrectly, either silently deleting them, or treating them as line endings.

            text = String(text);
            cx.lastIndex = 0;
            if (cx.test(text)) {
                text = text.replace(cx, function (a) {
                    return '\\u' +
                        ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                });
            }

// In the second stage, we run the text against regular expressions that look
// for non-JSON patterns. We are especially concerned with '()' and 'new'
// because they can cause invocation, and '=' because it can cause mutation.
// But just to be safe, we want to reject all unexpected forms.

// We split the second stage into 4 regexp operations in order to work around
// crippling inefficiencies in IE's and Safari's regexp engines. First we
// replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
// replace all simple value tokens with ']' characters. Third, we delete all
// open brackets that follow a colon or comma or that begin the text. Finally,
// we look to see that the remaining characters are only whitespace or ']' or
// ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

            if (/^[\],:{}\s]*$/
                    .test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
                        .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
                        .replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

// In the third stage we use the eval function to compile the text into a
// JavaScript structure. The '{' operator is subject to a syntactic ambiguity
// in JavaScript: it can begin a block or an object literal. We wrap the text
// in parens to eliminate the ambiguity.

                j = eval('(' + text + ')');

// In the optional fourth stage, we recursively walk the new structure, passing
// each name/value pair to a reviver function for possible transformation.

                return typeof reviver === 'function' ?
                    walk({'': j}, '') : j;
            }

// If the text is not JSON parseable, then a SyntaxError is thrown.

            throw new SyntaxError('JSON.parse');
        };
    }
}());
