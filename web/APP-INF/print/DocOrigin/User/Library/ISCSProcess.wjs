/*
	ISCSProcess.wjs

		1. Combine all xatw files from the -form argument into a single, combined xatw
		2. Make the following updates to the combined xatw:
			- rename all panes to include the original name of the underlying form
			- add a field to each pane with the name of the original form
			- remove the global flag for all fields within all repeating panes
			- update all references to overflow header and footer panes with the new pane names
		3. Make the following updates to the data file passed in the -data argument
			- remove surrounding quotes from the ^form "formName" records
			- insert a ^field record after the ^form record with the new form name
			- remove all ^define records from the field nominated file
		4. Run Merge using the updated, combined XATW and the updated field nominated file

*/

var noGood = false;

var formList = _job.command.form;

if (typeof formList == 'undefined') {
	_logf('No formList file name was given. Script terminated');
	noGood = true;
}

var prtFile = _job.command.prtFile;


if (typeof prtFile== 'undefined') {
	_logf('No .prt file name was given. Script terminated');
	noGood = true;
}

// Make sure that the .prt file exists
if (!_file.exists(prtFile)) {
	_logf('The .prt file %s was not found. Script terminated', prtFile);
	noGood = true;
}

var data = _job.command.data;

if (typeof data == 'undefined') {
	_logf('No data file name was given. Script terminated');
	noGood = true;
}

var output = _job.command.output;
_logf("Output = %s", output);

if (typeof output == 'undefined') {
	_logf('No output file name was given. Script terminated');
	noGood = true;
}

var logfile = _job.command.logfile;

if (typeof logfile == 'undefined') {
	_logf('No log file name was given. Script terminated');
	noGood = true;
}

var ellipsis = _job.command.ellipsis;

if (typeof ellipsis == 'undefined') {
	_logf('No ellipsis option was specified');	
  	ellipsis = 20;
}

var autoResolve = _job.command.FragmentAutoResolve;

if (typeof autoResolve == 'undefined') {
	_logf('No autoResolve option was specified');
	autoResolve = 'Yes';	
}

// Missing mandatory argument[s] - terminate script
if (noGood) {
	_printf('Usage:  runscript -script=ISCSPreProcess.wjs \n\nMake sure that all parameters are correct\n\nScript terminated.');
	return 200;
}

// Names of the updated XATW and FNF files
var newXatwName = '';
var newFnf = '';

/*
	Step 1: Combine the forms into temporary xatw file
*/

// temporary file name
var combXatw = _file.tempname('');
_logf('Using temporay file %s for combined xatw', combXatw);

var exe = $E + '/DocOriginCombineForms';

_logf('exe name: %s', exe);

var cmdArgs = '-in=';
var allForms = formList.split(';');

// Get the path of the first form
var formPath = _file.getFileNamePart(allForms[0], "DrivePath");

for (var i = 0; i < allForms.length; i++) {
	if (i == 0) {
		cmdArgs += allForms[i];
	}
	else {
		cmdArgs += ';' + allForms[i];
	}
}
cmdArgs += ' '
cmdArgs += '-out=' + combXatw;

_logf('cmdArgs: %s', cmdArgs);

// run DocOriginCombineForms
var combRC = _run(exe, cmdArgs);
if (combRC != 0) {
	_logf('Caution:\nDocOriginCombineForms returned %s.\nScript terminated. ', combRC);
	return combRC;
}


/*
	Step 2:	Update the combined xatw file
*/
updateXatw(combXatw);

/*
	Step 3:	Update the field-nominated file
*/

_logf('data: %s', data);

updateFNF(data);

/*
	Step 4: Run Merge
*/
var mergeArgs = {};
mergeArgs.form = newXatwName;
mergeArgs.data = newFnf;
mergeArgs.config = prtFile;
mergeArgs.output = output;
mergeArgs.FragmentAutoResolve = autoResolve;
mergeArgs.logfile = logfile;
mergeArgs.ellipsis = ellipsis;

mergeRC = _merge(mergeArgs);

return mergeRC;

function updateXatw(xatw) {

	// Arrays to hold original and updated pane names
	var oldPanes = [];
	var newPanes = [];

	var fieldNode = '<field name="##fieldName##" type="text" dataType="string" backgroundColor="transparent" foregroundColor="#000000" visible="no" shading="none" singleLine="yes" allowSplit="no" global="no" resizableWidth="yes" resizableHeight="no" maxHeight="0.0 in" lineSpacing="0.166667 in" angle="0" justify="left top" caseShift="none"><boundingRect left="1.913 in" top="0.0 in" width="2.1628 in" height="0.1667 in"/><border visible="no" color="#000000" thickness="0.008333 in" lineStyle="solid" edges="all" radius="0.0 in"/><font typeface="Arial" size="10.000" italic="0" bold="1000"/></field>';

	
	//newXatwName = xatw.replace('.xatw', '_upd.xatw');
        newXatwName = xatw + '_upd.xatw';
	
	
	var xatwRec = '';
	var formName = '';
	var paneName = '';

	// Open the original xatw for input
	var xatwFp = _file.fopen(xatw, 'r');
	// Open the new xatw for output
	var xatwOut = _file.fopen(newXatwName, 'w');

	while (true) {
		xatwRec = xatwFp.fgets();
		if (!xatwRec) break;
		// Page ?
		if (xatwRec.indexOf('<page ') > 1) {
			formName = getAttrValue('_originalForm', xatwRec);
			xatwOut.fputs(xatwRec);
			continue;
		}
		// Pane?
		if (xatwRec.indexOf('<pane ') > 1) {
			paneName = getAttrValue('name', xatwRec);
			
			// If the pane allows multiple, fields cannot be global
			var removeGlobals = (xatwRec.indexOf('allowMultiple="yes"') > -1);

			// Remember old and new pane names
			if (oldPanes.indexOf(paneName) == -1) {
				oldPanes.push(paneName);
				newPanes.push(paneName + '_' + formName);
			}

			var oldPaneString = 'name="' + paneName + '"';
			var newPaneString = 'name="' + paneName + '_' + formName + '"';
			xatwOut.fputs(xatwRec.replace(oldPaneString, newPaneString));
			xatwOut.fputs(fieldNode.replace('##fieldName##', formName));
			continue;
		}
		// Remove "Global" flag for all fields in a "allowMultiple" pane
		if (removeGlobals) {
			if (xatwRec.indexOf('<field ') > 1) {
				xatwOut.fputs(xatwRec.replace('global="yes"', 'global="no"'));
				continue;
			}
		}
		xatwOut.fputs(xatwRec);
	}
	xatwFp.fclose();
	xatwOut.fclose();

	// We need to replace the pane names of all overflow header and footer panes
	// with their new name
	var xatwFp = _file.fopen(newXatwName, 'r');
	var updNeeded = false;
	var xatwString = '';
	while (true) {
		xatwRec = xatwFp.fgets();
		if (!xatwRec) break;
		if (xatwRec.indexOf('<pane ') > 1) {
			if (xatwRec.indexOf(' overflow') > -1) {
				updNeeded = true;
				// Overflow Header
				var hPane = getAttrValue('overflowHeader', xatwRec);
				var paneIdx = oldPanes.indexOf(hPane);
				var hSrch = ' overflowHeader="' + oldPanes[paneIdx] + '"'; 
				var hRpl =  ' overflowHeader="' + newPanes[paneIdx] + '"'; 
				xatwRec = xatwRec.replace(hSrch, hRpl);
				// Overflow Footer
				var fPane = getAttrValue('overflowFooter', xatwRec);
				paneIdx = oldPanes.indexOf(fPane);
				var fSrch = ' overflowFooter="' + oldPanes[paneIdx] + '"'; 
				var fRpl =  ' overflowFooter="' + newPanes[paneIdx] + '"'; 
				xatwRec = xatwRec.replace(fSrch, fRpl);
				xatwString += xatwRec;
				continue;
			}
		}
		xatwString += xatwRec;
	}
	xatwFp.fclose();
	if (updNeeded) _file.writeFile(newXatwName, xatwString);
	return;
}

function getAttrValue(attr, xml) {
	var srch = attr + '="';
	var strPos = xml.indexOf(srch) + srch.length;
	var endPos = xml.indexOf('"', strPos);
	return xml.substring(strPos, endPos);
}


function updateFNF(dataFile) {
	var fnf = dataFile;

        newFnf  = _file.tempname('') + '_updated.dat';

	_logf('fnf value %s', fnf);
	_logf('new fnf value %s', newFnf);
	var inpFnf = _file.fopen(fnf, 'r');
	var outFnf = _file.fopen(newFnf, 'w');
	var fnfRec = '';
	var formName = '';

        if (outFnf == undefined || outFnf == null) {
	   _logf('outFnf is null');
	}
        else {
	   _logf('out Fnf is not null');
	}
     
	while(true) {
		fnfRec = inpFnf.fgets();
		if (!fnfRec) break;

		// ^form record
		if (fnfRec.toLowerCase().indexOf('^form') > -1) {
			var parts = fnfRec.trim().split(' ');
			var fName = _file.getFileNamePart(parts[1], "Name");
			var rplChar= '"';
			formName = fName.replace(rplChar, '', 'g');
			outFnf.fputs('^form ' + formName + '\n');
			outFnf.fputs('^field ' + formName + '\n');
			outFnf.fputs(' \n');
			continue;
		}
		
		// ignore ^define records
		if (fnfRec.toLowerCase().indexOf('^define') > -1) continue;

		// ignore "empty" records
		//if (fnfRec.trim().length == 0) continue;

		// write all other records to the new FNF
		outFnf.fputs(fnfRec);
	}

	inpFnf.fclose();
	outFnf.fclose();
	return;
}
