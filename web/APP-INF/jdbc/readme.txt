This directory contains jdbc configuration files to be used with Install Assist by customer implementation teams and the CTG deployment team.  
The files contain configurations for MySql, SQLServer and Oracle with all but one of those commented out.

When creating a product template for delivery to an implementation team, copy these XML files into the parent directory, "web\APP-INF\jdbc"

*** The files contained in this directory WILL NOT deploy in the DevOps environments! ***