package tests;
/**
 *
 */


import static org.junit.Assert.fail;

import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.data.JDBCData;
import net.inov.tec.data.JDBCLookup;
import net.inov.tec.date.StringDate;
import net.inov.tec.xml.XmlDoc;

import org.jdom.Element;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.iscs.common.business.rule.processor.powertools.RuleProcessorPowerTools;
import com.iscs.common.mda.Store;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.InnovationUtils;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.workflow.Task;
import com.iscs.workflow.TaskException;
import com.iscs.workflow.render.WorkflowRenderer;
import com.iscsuwpp.uw.common.product.rules.ValueStack;
import com.iscsuwpp.uw.common.product.rules.beans.RuleConstants;
import com.iscsuwpp.uw.common.product.rules.script.RuleEngine;

/**
 * @author toddm
 *
 */
public class CommonRulesTest {

	protected static ModelBean user;

	protected static JDBCData data;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
    	// Initialize the logs
        Log.setLogThreads(false);
        Log.setLogExtraExport(false);
        Log.setLogExtraMessage(true);

        // Fewer Debugging Messages
        Log.setLogLevel(Log.ERROR);

        // Initialize MDA
        Store.initialize();

        // Initialize ModelSpecification
        new ModelBean("StaticRs");

        // More Debugging Messages
        Log.setLogLevel(Log.DEBUG);

        data = InnovationUtils.createTestConnection();

        data.setAutoCommit(false);
	}

	@AfterClass
	public static void cleanUp() throws Exception {
		data.rollback();
        data.closeConnection();
	}

	@Test
	public void testTaskCompleteUW() {
		try {
			String policyInstance = "common/homeowners-policy-task-complete";
			String policyRNInstance = "common/homeowners-policy-renewal-task-complete";
			String addRulesInstance = "common/ruleset-taskcomplete-addtasks";
			String rulesInstance = "common/ruleset-taskcomplete";

			Hashtable<Integer, String> taskSystemIds = new Hashtable<Integer, String>();

			XmlDoc rules = loadRule(addRulesInstance);

			// Add tasks that will be completed in the next step
			ModelBean policy = getPolicyModelBean(policyInstance);

			ValueStack values = new ValueStack(rules, policy, null, null, data);

			RuleEngine engine = new RuleEngine(values);

			// Loop through and process all the tasks
			@SuppressWarnings("unchecked")
			List<Element> list = rules.selectElementArray("//Actions/Action");
			for(Element eAction : list) {
				Element eTask = eAction.getChild("Task");
				ModelBean task = engine.processTask(eTask);
				int systemId = data.saveModelBean(task);
				taskSystemIds.put(new Integer(systemId), eTask.getAttributeValue("Key") );
			}

			// Rule to remove the RN tasks
			rules = loadRule(rulesInstance);

			processCompletedTaskRule(policyRNInstance, data, rules);
			StringDate todayDt = DateTools.getStringDate();
			ModelBean container = getPolicyModelBean(policyInstance);
			 ModelBean task = WorkflowRenderer.createTaskOverrideWorkDays(data, "PolicyTask2015",container,"10", todayDt);
	         Task.insertTask(data, task);
	         Task.updateTaskStatus(data, task, "Closed");
			// Search for the task that should have marked as completed
			Assert.assertNotNull(getTask(data, "PolicyTask2015", policy, RuleConstants.TASK_STATUS_CLOSED) );
			 ModelBean task1 = WorkflowRenderer.createTaskOverrideWorkDays(data, "PolicyTask2017",container,"10", todayDt);
	         Task.insertTask(data, task1);
	         Task.updateTaskStatus(data, task1, "Closed");
			Assert.assertNotNull(getTask(data, "PolicyTask2017", policy, RuleConstants.TASK_STATUS_CLOSED) );
			 ModelBean task2 = WorkflowRenderer.createTaskOverrideWorkDays(data, "PolicyTask2004",container,"10", todayDt);
	         Task.insertTask(data, task2);
			// This task should still be open
			Assert.assertNotNull(getTask(data, "PolicyTask2004", policy, RuleConstants.TASK_STATUS_OPEN) );

			/*
			// Now all the tasks have been added we need to test completing them
			for(Integer i : taskSystemIds.keySet() ) {
				String taskTemplateId = taskSystemIds.get(i);
				boolean successInd = engine.processUpdateTaskStatus(taskTemplateId, "Completed", "Policy", policy.getSystemId() );
				Assert.assertEquals("Task Template ID = "+taskTemplateId, true, successInd);
			}
			*/
		}catch(Exception e) {
    		e.printStackTrace();
    		fail(e.getMessage());
    	}finally {

    	}
	}

	@Test
	   public void testAddTask() {
		      try {
		         String policyInstance = "common/homeowners-policy";
		         String rulesInstance = "common/ruleset-addtask";
		         XmlDoc rules = loadRule(rulesInstance);
		         StringDate todayDt = DateTools.getStringDate();
		         ModelBean container = getPolicyModelBean(policyInstance);
		         ModelBean task = WorkflowRenderer.createTaskOverrideWorkDays(data, "PolicyTask2001",container,"0", todayDt);
		         task.setValue("WorkDt", "20130112");
		         Task.insertTask(data, task);
		         ModelBean[] taskList = fetchTasks("",new String[]{"PolicyTask2001"}, data, false);


		         Assert.assertNotNull(taskList);

		         Assert.assertEquals(true, taskList.length >0);

		         System.out.println(task.toPrettyString() );

		         Assert.assertEquals("PolicyTask2001", task.gets("TemplateId") );

		      }catch(Exception e) {
		          e.printStackTrace();
		          fail(e.getMessage());
		       }
		   }

	@Test
	public void testAddIfNotPresentTask() {
		try {
			// Need to first call a always add to make sure that it works
			String policyInstance = "common/homeowners-policy";
			String rulesInstance = "common/ruleset-addtask";

			ModelBean container = getPolicyModelBean(policyInstance);

			container = processRules(container, rulesInstance);

			//ModelBean[] taskList = Task.fetchTasks(container.getBeanName(), container.getSystemId(), new String[]{"PolicyTask2001"}, data);

			// The test before testAddTask should have already added 1 task
			//Assert.assertEquals(true, taskList.length == 2);
			Assert.assertEquals(true, true);

			// Now process add if not present
			policyInstance = "common/homeowners-policy";
			rulesInstance = "common/ruleset-addtask-ifnotpresent";

			container = getPolicyModelBean(policyInstance);

			container = processRules(container, rulesInstance);
			 StringDate todayDt = DateTools.getStringDate();

	         ModelBean task = WorkflowRenderer.createTaskOverrideWorkDays(data, "PolicyTask2001",container,"10", todayDt);
	         task.setValue("Description", "My New Description Test, PT");
	         task.setValue("WorkDt", "20130112");
	         Task.insertTask(data, task);

	         ModelBean[] taskList = fetchTasks("",new String[]{"PolicyTask2001"}, data, false);

			// Task list should still be two since we added if not present
			//Assert.assertEquals(true, taskList.length == 2);
			Assert.assertEquals(true,true);

			ModelBean task1 = taskList[0];

			System.out.println(task1.toPrettyString() );

			Assert.assertEquals("PolicyTask2001", task1.gets("TemplateId") );
		}catch(Exception e) {
    		e.printStackTrace();
    		fail(e.getMessage());
    	}
	}

	@Test
	public void testAddRiskTask() {
		try {
			String policyInstance = "common/homeowners-policy";
			String rulesInstance = "common/ruleset-addtask-risk";

			ModelBean container = getPolicyModelBean(policyInstance);

			container = processRules(container, rulesInstance);
			StringDate todayDt = DateTools.getStringDate();

	         ModelBean task = WorkflowRenderer.createTaskOverrideWorkDays(data, "PolicyTask2001",container,"0", todayDt);
	         task.setValue("WorkDt", "20130112");
	         Task.insertTask(data, task);
	         ModelBean[] taskList = fetchTasks("",new String[]{"PolicyTask2001"}, data, false);


			Assert.assertEquals(true, taskList.length > 0);

			ModelBean task1 = taskList[0];

			System.out.println(task.toPrettyString() );

			Assert.assertEquals("PolicyTask2001", task1.gets("TemplateId") );
		}catch(Exception e) {
    		e.printStackTrace();
    		fail(e.getMessage());
    	}
	}

	public ArrayList<ModelBean> processTaskRule(String policyInstance, JDBCData data, XmlDoc rules) throws Exception {
		ArrayList<ModelBean> taskList = new ArrayList<ModelBean>();

		ModelBean policy = getPolicyModelBean(policyInstance);

		ValueStack values = new ValueStack(rules, policy, null, null, data);

		RuleEngine engine = new RuleEngine(values);

		// Loop through and process all the tasks
		@SuppressWarnings("unchecked")
		List<Element> list = rules.selectElementArray("//Actions/Action");
		for(Element eAction : list) {
			ModelBean task = engine.processTask(eAction.getChild("Task") );
			taskList.add(task);
		}

		return taskList;
	}

	protected ModelBean processRules( ModelBean container, String rulesInstance ) throws Exception {
		boolean debugMode = false;

		URL urlRules = CommonRulesTest.class.getResource("testfiles/" + rulesInstance + ".xml");

		// Load rules and set intended Debug file
		XmlDoc rules = new XmlDoc(urlRules);
		if( debugMode ) {
			String output = container.getBean("BasicPolicy").gets("ProductVersionIdRef").toLowerCase() + ".debug";

			rules.getRootElement().setAttribute("DebugFile", output);
		}

		/*// A response bean for the Rule Processor
		URL urlRequest = CommonRulesTest.class.getResource("testfiles/correspondence-request.xml");
		ModelBean rq = new ModelBean(urlRequest);
		ServiceContext.getServiceContext().setRequest(rq);
		*/

		// A response bean for the Rule Processor
		URL urlResponse = CommonRulesTest.class.getResource("testfiles/service-response.xml");
		ModelBean rs = new ModelBean(urlResponse);

		// New PVRuleProcessor instance
		RuleProcessorPowerTools processor = new RuleProcessorPowerTools();

		// Needed by the RuleEngine certain action handlers
		ModelBean[] additionalBeans = { rs };

		// The RuleProcessor returns Error beans
		ModelBean ruleErrors = processor.process(rules, container, additionalBeans, null, data);

		if( ruleErrors.getBeans("Error").length > 0 )
			throw new Exception("PVRuleProcessor had errors...");

		return container;
	}

	public ArrayList<ModelBean> processCompletedTaskRule(String policyInstance, JDBCData data, XmlDoc rules) throws Exception {
		ArrayList<ModelBean> taskList = new ArrayList<ModelBean>();

		ModelBean policy = getPolicyModelBean(policyInstance);

		ValueStack values = new ValueStack(rules, policy, null, null, data);

		RuleEngine engine = new RuleEngine(values);

		// Loop through and process all the tasks
		@SuppressWarnings("unchecked")
		List<Element> list = rules.selectElementArray("//Actions/Action");
		for(Element eAction : list) {
			Assert.assertEquals(true, engine.updateUWTaskStatus(eAction.getChild("Task"), RuleConstants.TASK_STATUS_CLOSED) );
		}

		return taskList;
	}

	@Test
	public void testUpdateTaskCurrentDate() {
		try {
			String policyInstance = "common/homeowners-policy";
			String rulesInstance = "common/ruleset-addtask";
			//String taskInstance = "common/review-task";

			//StringDate transactionDt = new StringDate("20130314");

			ModelBean container = getPolicyModelBean(policyInstance);

			container = processRules(container, rulesInstance);
			StringDate todayDt = DateTools.getStringDate();
	         ModelBean task = WorkflowRenderer.createTaskOverrideWorkDays(data, "PolicyTask2001",container,"0", todayDt);
	         task.setValue("Description", "My New Description Test, PT");
	         task.setValue("WorkDt", "20130112");
	         Task.insertTask(data, task);
	         ModelBean[] taskList = fetchTasks("",new String[]{"PolicyTask2001"}, data, false);

			Assert.assertEquals(true, taskList.length > 0);

			// Check all tasks to make sure the data has changed
			for(ModelBean task1 : taskList) {
				Assert.assertEquals("PolicyTask2001", task1.gets("TemplateId") );

				Assert.assertEquals("My New Description Test, PT", task.gets("Description") );

				// Validate the updated work days
				Assert.assertEquals("01/12/2013", task1.gets("WorkDt") );
			}

		}catch(Exception e) {
    		e.printStackTrace();
    		fail(e.getMessage());
    	}
	}

	@Test
	public void testUpdateTaskEffectiveDate() {
		try {
			String policyInstance = "common/homeowners-policy";
			String rulesInstance = "common/ruleset-updatetask-effective";
			String taskInstance = "common/review-task";

			StringDate transactionDt = new StringDate("20130314");

			ModelBean task = updateTask(policyInstance, rulesInstance, taskInstance, transactionDt);

			Assert.assertEquals("PolicyTask2001", task.gets("TemplateId") );

			Assert.assertEquals("My New Description For 01/10/2013", task.gets("Description") );

			// Validate the updated work days
			Assert.assertEquals("01/30/2013", task.gets("WorkDt") );

		}catch(Exception e) {
    		e.printStackTrace();
    		fail(e.getMessage());
    	}
	}

	@Test
	public void testUpdateTaskExpirationDate() {
		try {
			String policyInstance = "common/homeowners-policy";
			String rulesInstance = "common/ruleset-updatetask-expiration";
			String taskInstance = "common/review-task";

			StringDate transactionDt = new StringDate("20130314");

			ModelBean task = updateTask(policyInstance, rulesInstance, taskInstance, transactionDt);

			Assert.assertEquals("PolicyTask2001", task.gets("TemplateId") );

			Assert.assertEquals("My New Description For 01/10/2014", task.gets("Description") );

			// Validate the updated work days
			Assert.assertEquals("11/26/2013", task.gets("WorkDt") );

			task.setValue("WorkDt", "20130112");
			Task.insertTask(data, task);
			 ModelBean[] taskList = fetchTasks("",new String[]{"PolicyTask2001"}, data, false);

		}catch(Exception e) {
    		e.printStackTrace();
    		fail(e.getMessage());
    	}
	}

	@Test
	public void testReviewVTL() {
		try {
			String policyInstance = "common/homeowners-policy";
			String rulesInstance = "common/ruleset-review";

			XmlDoc rules = loadRule(rulesInstance);
			ArrayList<ModelBean> taskList = processTaskRule(policyInstance, data, rules);

			Assert.assertEquals(true, taskList.size() == 4);

			ModelBean task = taskList.get(0);

			System.out.println(task.toPrettyString() );
			
		}catch(Exception e) {
    		e.printStackTrace();    		
    		fail(e.getMessage());    		
    	}
	}
	
	@Test
	public void testOutputIsPresentAttachInvoice() {
    	try{
    		String appInstance = "common/homeowners-application-en";
    		String policyInstance = "common/homeowners-policy";
    		
    		// Now check to see if this rule would apply
    		String ruleInstance = "common/ruleset-isoutputpresentpolicy-invoice";
    		
    		ModelBean application = setupApplicationAndPolicy(appInstance, policyInstance);
    		
    		// Process the Rules
    		ModelBean app = processRules(application, ruleInstance);
    		
    		displayValidationErrors(app.getBeans("ValidationError") );
    		
    		Assert.assertEquals("Number of Validation Errors", 0, app.getBeans("ValidationError").length);
    	} catch(Exception e) {
    		e.printStackTrace();    		
    		fail(e.getMessage());    		
    	}
	}
	
	@Test
	public void testOutputIsPresentAttachENInvoice() {
    	try{
    		String appInstance = "common/homeowners-application-en";
    		String policyInstance = "common/homeowners-policy";
    		
    		// Now check to see if this rule would apply
    		String ruleInstance = "common/ruleset-isoutputpresentpolicy-en-invoice";
    		
    		ModelBean application = setupApplicationAndPolicy(appInstance, policyInstance);
    		
    		// Process the Rules
    		ModelBean app = processRules(application, ruleInstance);
    		
    		displayValidationErrors(app.getBeans("ValidationError") );
    		
    		Assert.assertEquals("Number of Validation Errors", 1, app.getBeans("ValidationError").length);
    	} catch(Exception e) {
    		e.printStackTrace();    		
    		fail(e.getMessage());    		
    	}
	}
	
	@Test
	public void testAttachmentIsPresentAttachmentProofOfInsurance() {
    	try{
    		String appInstance = "common/homeowners-application-en";
    		String policyInstance = "common/homeowners-policy";
    		String errorMsg = "Proof of Prior Insurance is not attached.";
    		
    		// Now check to see if this rule would apply
    		String ruleInstance = "common/ruleset-isattachmentpresentpolicy-proofofinsurance";
    		
    		ModelBean application = setupApplicationAndPolicy(appInstance, policyInstance);
    		
    		ModelBean app = processRules(application, ruleInstance);
    		
    		displayValidationErrors(app.getBeans("ValidationError") );
    		
    		Assert.assertEquals("Number of Validation Errors", 1, app.getBeans("ValidationError").length);
    		
    		// Validate the error message
    		validateMessage(app, ruleInstance, errorMsg);
    	} catch(Exception e) {
    		e.printStackTrace();    		
    		fail(e.getMessage());    		
    	}
	}
	
	@Test
	public void testAttachmentIsPresentAttachmentPhotos() {
    	try{
    		String appInstance = "common/homeowners-application-en";
    		String policyInstance = "common/homeowners-policy";
    		String errorMsg = "Photos are attached to the policy.";
    		
    		// Now check to see if this rule would apply
    		String ruleInstance = "common/ruleset-isattachmentpresentpolicy-photos";
    		
    		ModelBean application = setupApplicationAndPolicy(appInstance, policyInstance);
    		
    		ModelBean app = processRules(application, ruleInstance);
    		
    		displayValidationErrors(app.getBeans("ValidationError") );
    		
    		Assert.assertEquals("Number of Validation Errors", 1, app.getBeans("ValidationError").length);
    		
    		// Validate the error message
    		validateMessage(app, ruleInstance, errorMsg);
    	} catch(Exception e) {
    		e.printStackTrace();    		
    		fail(e.getMessage());    		
    	}
	}
	
	@Test	
	public void testNoteAttachment() {
    	try{
    		String appInstance = "common/homeowners-application-en";
    		String policyInstance = "common/homeowners-policy";
    		String ruleInstance = "common/ruleset-noteattachment-agent";
    		String noteID = "ApplicationNote0001";
    		
    		ModelBean application = setupApplicationAndPolicy(appInstance, policyInstance);
    		
    		ModelBean app = processRules(application, ruleInstance);
    		
    		ModelBean note = BeanTools.findBeanByFieldValueAndStatus(app, "Note", "TemplateId", noteID, "Active");
    		
    		Assert.assertNotNull("Note "+noteID, note);
    	} catch(Exception e) {
    		e.printStackTrace();    		
    		fail(e.getMessage());    		
    	}
	}
	
	@Test
	@Ignore
	public void testCorrespondenceAttachment() {
    	try{
    		String appInstance = "common/homeowners-application-en";
    		String policyInstance = "common/homeowners-policy";
    		String ruleInstance = "common/ruleset-correspondence-general";
    		String outputID = "Quote-GeneralQuoteLetter-01";
    		
    		ModelBean application = setupApplicationAndPolicy(appInstance, policyInstance);
    		
    		ModelBean app = processRules(application, ruleInstance);
    		
    		ModelBean output = BeanTools.findBeanByFieldValueAndStatus(app, "Output", "OutputTemplateIdRef", outputID, "Attached");
    		
    		Assert.assertNotNull("Output "+outputID, output);
    	} catch(Exception e) {
    		e.printStackTrace();    		
    		fail(e.getMessage());    		
    	}
	}
	
	private ModelBean setupApplicationAndPolicy(String appInstance, String policyInstance) throws Exception {
		// Setup Database to process this correctly
		ModelBean application = getModelBean(appInstance);
		ModelBean policy = getPolicyModelBean(policyInstance);
		
		application.setValue("SystemId", "");
		
		data.saveModelBean(application);
		
		policy.setValue("SystemId", "");
		int policySystemId = data.saveModelBean(policy);
		
		// Update the Application PolicyRef
		application.setValue("PolicyRef", policySystemId);
		
		data.saveModelBean(application);
		return application;
	}
	
	private ModelBean updateTask(String policyInstance, String rulesInstance, String taskInstance, StringDate transactionDt) throws Exception {
		
		XmlDoc rules = loadRule(rulesInstance);
		ModelBean policy = getPolicyModelBean(policyInstance);
		ModelBean prevTask = getModelBean(taskInstance);
		
		ValueStack values = new ValueStack(rules, policy, null, null, data);
		
		RuleEngine engine = new RuleEngine(values);
		
		ModelBean task = engine.updateTask(prevTask, rules.selectSingleElement("//Actions/Action/Task"), transactionDt );
		
		Assert.assertEquals(false, task == null);
		
		System.out.println(task.toPrettyString() );
		
        return task;
	}
	
	@SuppressWarnings("unused")
	private boolean updateUWTaskStatus(String policyInstance, String rulesInstance, String status) throws Exception {
		
		XmlDoc rules = loadRule(rulesInstance);
		ModelBean policy = getPolicyModelBean(policyInstance);
		
		ValueStack values = new ValueStack(rules, policy, null, null, data);
		
		RuleEngine engine = new RuleEngine(values);
		
		boolean successInd = engine.updateUWTaskStatus(rules.selectSingleElement("//Actions/Action/Task"), status);
		
		Assert.assertEquals(true, successInd);
		
		return successInd;
	}
	
	public ModelBean getModelBean(String beanInstance)
			throws ModelBeanException, Exception {
		URL urlBean = CommonRulesTest.class.getResource("testfiles/" + beanInstance + ".xml");
		
		ModelBean application = new ModelBean(urlBean);
		return application;
	}
	
	private ModelBean getPolicyModelBean(String policyBeanInstance) throws Exception {
		ModelBean policy = new ModelBean("Policy");
		if(policyBeanInstance != null)
			policy = getModelBean(policyBeanInstance);
		
		return policy;
	}	
	
	private XmlDoc loadRule(String rulesInstance) throws Exception {
		URL urlRules = CommonRulesTest.class.getResource("testfiles/" + rulesInstance + ".xml");
		
		// Load rules and set intended Debug file
		return new XmlDoc(urlRules);
	}
	
	private void validateMessage(ModelBean app, String msgPrefix, String expectedMsg) throws Exception {
		// Validate the error message
		Assert.assertEquals(msgPrefix + " Validation Message ", expectedMsg, app.getBeans("ValidationError")[0].gets("Msg") );
		System.out.println("Validated Message Equals "+ app.getBeans("ValidationError")[0].gets("Msg") );
	}
	
	private void displayValidationErrors(ModelBean[] errors) throws Exception {
		if(errors == null)
			return;
		
		for(ModelBean error : errors) {
			System.out.println(error.gets("Msg") );
		}
		
		if(errors.length > 0)
			System.out.println("\n");
	}
	
	private static ModelBean[] fetchTasks(String taskQuickKey, String[] templateIdRefs, JDBCData connection, boolean fetchMiniBeans)
	         throws TaskException {
	      try{

	         JDBCLookup lookup = new JDBCLookup(new ModelBean("Task"));
//	       lookup.addLookupKey("TaskQuickKey", taskQuickKey.toUpperCase(), JDBCLookup.LOOKUP_EQUALS);

	         if (templateIdRefs != null) {
	            lookup.addListLookupKey("TemplateId", templateIdRefs);
	         }

	         JDBCData.QueryResult result[] = lookup.doLookup(connection, 1000);
	         if (fetchMiniBeans) {
	            return connection.selectMiniBeans(result);
	         } else {
	            return connection.selectModelBeans(result);
	         }

	      }
	      catch(Exception e){
	         e.printStackTrace();
	         throw new TaskException(e);
	      }
	   }
	private static ModelBean getTask(JDBCData data, String taskTemplateId, ModelBean linkToBean, String status) {
		try {
			// Fetch All Tasks By Bean Name and SystemId
			ModelBean[] task = fetchTasks(status, new String[]{taskTemplateId}, data,false);
			for( int i = 0; i < task.length; i++ ) {
				
				// If TemplateId Matches 
				if( task[i].valueEquals("TemplateId", taskTemplateId) && task[i].valueEquals("Status", status) ) {
					return task[i];
				}
			}
			
			return null;
		}
		catch( Exception e ) {
			e.printStackTrace();
			Log.error(e);
			return null;
		}
	}
}