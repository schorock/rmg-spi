package com.ric.uw.policy.handler;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.lang.StringUtils;
import com.iscs.common.admin.operation.render.OperationRenderer;
import com.iscs.common.business.provider.render.ProviderRenderer;
import com.iscs.common.render.DateRenderer;
import com.iscs.common.render.DynamicString;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.InnovationUtils;
import com.iscs.common.utility.bean.render.BeanRenderer;
import com.iscs.common.utility.io.FileTools;
import com.iscs.workflow.Task;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;

/**This class is used to write/append the latest transaction record of the policy bean to a csv file,if file already exists,
 * else creates new file and writes into it,if BasicPolicy.SurveyReceivedDt - BasicPolicy.ExpirationDt >= 2 years. 
 * 
 */

public class DQIDataExportPolicyFileGeneration implements RuleProcessor {
    public DQIDataExportPolicyFileGeneration() {
    }

	public String getContextVariableName() {
		return "DQIDataExportPolicyFileGeneration";
	}

	@Override
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user,
			JDBCData data) throws RuleException {
		try {
			// Update the Application Status
			ModelBean taskLink = bean.getBean("TaskLink","ModelName","Policy");
	        ModelBean policyBean = BeanRenderer.selectModelBean(data,"Policy", taskLink.gets("IdRef"));
	        processFiles(policyBean);
	        // Close the current task
            Task.updateTaskStatus(bean, "Completed");
            ModelBean uwRuleRs = new ModelBean("UWRuleRs");
	        return uwRuleRs;
		}
		catch (Exception e) {
			Log.error(e.getMessage(), e);
            throw new RuleException(e);
		}
	}
	
	/**This method writes/appends the latest transaction record of the policy bean to a csv file,if file already exists,
	 * else creates new file and writes into it,if BasicPolicy.SurveyReceivedDt - BasicPolicy.ExpirationDt >= 2 years. 
     * 
     * @param policyBean ModelBean
     */
	public void processFiles(ModelBean policyBean) {
		try {
				String directoryPath = DynamicString.render(new ModelBean("Company"),InnovationUtils.getEnvironmentVariable("DQIDataPolicyExport", "directoryPath", ""));
				String dailyFilename = InnovationUtils.getEnvironmentVariable("DQIDataPolicyExport", "dailyFilename","");
				String year = "";
				String month = "";
				String day = "";
				FileTools.createPath(directoryPath);
				// Run Date
				String runDtStr = OperationRenderer.getBookDate();
				DateFormat dateFormatter=null;
				Date convertedDate;
				dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
				convertedDate = dateFormatter.parse(runDtStr);
				StringDate runDt = DateRenderer.getStringDate(convertedDate);
				if (runDt != null) {
					year = runDt.toString().substring(2, 4);
					month = runDt.toString().substring(4, 6);
					day = runDt.toString().substring(6, 8);
				}
				// Run Date
				String fullPathName = directoryPath + dailyFilename + month + day + year + ".csv";
				File saveFile = new java.io.File(fullPathName);
				StringBuilder sb = new StringBuilder();
				ModelBean basicPolicy = policyBean.getBean("BasicPolicy");
				ModelBean insured = policyBean.getBean("Insured");
				ModelBean line = policyBean.getBean("Line");
				// PolicyNumber
				sb.append(basicPolicy.gets("PolicyDisplayNumber"));
				sb.append(",");
				// BasicPolicy.ExpirationDt
				sb.append(basicPolicy.gets("ExpirationDt"));
				sb.append(",");
				// get the insured party bean
				ModelBean insuredPartyInfo = insured.getBean("PartyInfo", "PartyTypeCd", "InsuredParty");
				// if(insuredPartyInfo!=null){
				ModelBean insuredNameInfo = insuredPartyInfo.getBean("NameInfo", "NameTypeCd", "InsuredName");
				// InsuredName.GivenName
				String insuredGivenName = insuredNameInfo.gets("GivenName");
				sb.append(insuredGivenName);
				sb.append(",");
				// InsuredName.OtherGivenName
				String insuredOtherGivenName = insuredNameInfo.gets("OtherGivenName");
				sb.append(insuredOtherGivenName);
				sb.append(",");
				// InsuredName.Surname
				String insuredSurCommName = insuredNameInfo.gets("Surname");
				if (StringUtils.isBlank(insuredSurCommName) && StringUtils.isEmpty(insuredSurCommName)) {
					// InsuredName.CommercialName
					insuredSurCommName = insuredNameInfo.gets("CommercialName");
				}
				sb.append(insuredSurCommName);
				sb.append(",");
				String entityTypeCd = insured.gets("EntityTypeCd");
				if (StringRenderer.equal(entityTypeCd, "Joint")) {
					// InsuredPartyJoint
					// get the InsuredPartyJoint bean
					ModelBean insuredPartyInfoJoint = insured.findBeanByFieldValue("PartyInfo", "PartyTypecd","InsuredPartyJoint");
					ModelBean insuredJointName = insuredPartyInfoJoint.findBeanByFieldValue("NameInfo", "NameTypeCd","InsuredNameJoint");
					//InsuredNameJoint.GivenName
					String insuredNameJointGivenName = insuredJointName.gets("GivenName");
					sb.append(insuredNameJointGivenName);
					sb.append(",");
					// InsuredContactName.OtherGivenName
					String insuredNameJointOtherGivenName = insuredJointName.gets("OtherGivenName");
					sb.append(insuredNameJointOtherGivenName);
					sb.append(",");
					// InsuredContactName.Surname
					String insuredNameJointSurName = insuredJointName.gets("Surname");
					sb.append(insuredNameJointSurName);
					sb.append(",");
				} else if (StringRenderer.in(entityTypeCd, "Business,Trust,Estate")) {
						// InsuredContactName
						ModelBean insuredContactNameInfo = insuredPartyInfo.getBean("NameInfo", "NameTypeCd","InsuredContactName");
						// InsuredContactName.GivenName
						String insuredContactGivenName = insuredContactNameInfo.gets("GivenName");
						sb.append(insuredContactGivenName);
						sb.append(",");
						// InsuredContactName.OtherGivenName
						String insuredContactotherGivenName = insuredContactNameInfo.gets("OtherGivenName");
						sb.append(insuredContactotherGivenName);
						sb.append(",");
						// InsuredContactName.Surname
						String insuredContactSurName = insuredContactNameInfo.gets("Surname");
						sb.append(insuredContactSurName);
						sb.append(",");
				
				} else {
					sb.append("");
					sb.append(",");
					sb.append("");
					sb.append(",");
					sb.append("");
					sb.append(",");
				}
				// InsuredPhonePrimary.PhoneNumber
				String phoneNumber = insuredPartyInfo.getBean("PhoneInfo", "PhoneTypeCd", "InsuredPhonePrimary").gets("PhoneNumber");
				sb.append(phoneNumber);
				sb.append(",");
				// InsuredMailingAddr.Addr1
				ModelBean insuredMailingAddr = insuredPartyInfo.getBean("Addr", "AddrTypeCd", "InsuredMailingAddr");
				String insuredAddr1 = insuredMailingAddr.gets("Addr1");
				sb.append(insuredAddr1);
				sb.append(",");
				// InsuredMailingAddr.Addr2
				String insuredAddr2 = insuredMailingAddr.gets("Addr2");
				sb.append(insuredAddr2);
				sb.append(",");
				// InsuredMailingAddr.City
				String insuredCity = insuredMailingAddr.gets("city");
				sb.append(insuredCity);
				sb.append(",");
				// InsuredMailingAddr.StateProvCd
				String insuredStateProvCd = insuredMailingAddr.gets("StateProvCd");
				sb.append(insuredStateProvCd);
				sb.append(",");
				// InsuredMailingAddr.PostalCode
				String insuredPostalCode = insuredMailingAddr.gets("PostalCode");
				sb.append(insuredPostalCode);
				sb.append(",");
				String productCode = line.gets("LineCd");
				ModelBean riskBean = line.getBean("Risk", "TypeCd", productCode);
				ModelBean buildingBean = riskBean.getBean("Building");
				// Risk Address
				ModelBean riskLookupAddrBean = buildingBean.getBean("Addr", "AddrTypeCd", "RiskLookupAddr");
				String riskAddress = getRiskAddress(riskLookupAddrBean);
				sb.append(riskAddress);
				sb.append(",");
				// Risk Address 2
				String secondaryRiskAddress = getSecondaryRiskAddress(riskLookupAddrBean);
				sb.append(secondaryRiskAddress);
				sb.append(",");
				// RiskLookupAddr.City
				String riskCity = riskLookupAddrBean.gets("City");
				sb.append(riskCity);
				sb.append(",");
				// RiskLookupAddr.StateProvCd
				String riskStateProvCd = riskLookupAddrBean.gets("StateProvCd");
				sb.append(riskStateProvCd);
				sb.append(",");
				// RiskLookupAddr.PostalCode
				String riskPostalCode = riskLookupAddrBean.gets("PostalCode");
				sb.append(riskPostalCode);
				sb.append(",");
				// Producer Name
				String providerRef = basicPolicy.gets("ProviderRef");
				ModelBean providerBean = ProviderRenderer.getProviderBySystemId(providerRef);
				String producerName = ProviderRenderer.getProviderName(providerBean.gets("ProviderNumber"),providerBean.gets("ProviderTypeCd"));
				sb.append(producerName);
				sb.append(",");
				// Producer Code
				sb.append(providerBean.gets("ProviderNumber"));
				sb.append(",");
				// Producer Primary Phone
				ModelBean producerPhone = providerBean.findBeanByFieldValue("PhoneInfo", "PhoneTypeCd","ProviderPrimaryPhone");
				if(producerPhone!=null){
				    sb.append(producerPhone.gets("PhoneNumber"));
				}else{
					sb.append("");
				}
				sb.append(",");
				sb.append(System.getProperty("line.separator"));
				Charset charset = Charset.forName("UTF-8");
				CharsetEncoder encoder = charset.newEncoder();
				FileOutputStream fos = new FileOutputStream(saveFile, true);// false,used for file appending
				FileChannel channel = fos.getChannel();
				try {
					channel.write(encoder.encode(CharBuffer.wrap(sb.toString().toCharArray())));
				} catch (Exception ex) {
					Log.debug("File locked. Retrying in 100ms " + ex);
					throw (ex);
				} finally {
					if (channel != null)
						channel.close();
					if (fos != null)
						fos.close();
				}
			
		} catch (Exception e) {
			Log.debug("Exception occurred while processing the csv file for DQI Data Export");
		}

	}
    
	/** Returns the RiskAddress
     * @return String
     * @param riskLookupAddressBean ModelBean
     */
	private String getRiskAddress(ModelBean riskLookupAddressBean) {
		String riskAddress = "";
		try {
			String primaryNumber = riskLookupAddressBean.gets("PrimaryNumber");
			String preDirectional = riskLookupAddressBean.gets("PreDirectional");
			String streetName = riskLookupAddressBean.gets("StreetName");
			String suffix = riskLookupAddressBean.gets("Suffix");
			String postDirectional = riskLookupAddressBean.gets("PostDirectional");
			if (StringUtils.isNotBlank(primaryNumber) && StringUtils.isNotEmpty(primaryNumber)) {
				riskAddress = primaryNumber;
			}
			if (StringUtils.isNotBlank(preDirectional) && StringUtils.isNotEmpty(preDirectional)) {
				riskAddress = riskAddress + " " + preDirectional;
			}
			if (StringUtils.isNotBlank(streetName) && StringUtils.isNotEmpty(streetName)) {
				riskAddress = riskAddress + " " + streetName;
			}
			if (StringUtils.isNotBlank(suffix) && StringUtils.isNotEmpty(suffix)) {
				riskAddress = riskAddress + " " + suffix;
			}
			if (StringUtils.isNotBlank(postDirectional) && StringUtils.isNotEmpty(postDirectional)) {
				riskAddress = riskAddress + " " + postDirectional;
			}
		} catch (ModelBeanException e) {
			Log.debug("Exception occurred while getting the RiskAddress");
		}
		return riskAddress;
	}
    
	/** Returns the Secondary RiskAddress
     * @return String
     * @param riskLookupAddressBean ModelBean
     */
	private String getSecondaryRiskAddress(ModelBean riskLookupAddressBean) {
		String secondaryRiskAddress = "";
		try {
			String secondaryDesignator = riskLookupAddressBean.gets("SecondaryDesignator");
			String secondaryNumber = riskLookupAddressBean.gets("SecondaryNumber");
			if (StringUtils.isNotBlank(secondaryDesignator) && StringUtils.isNotEmpty(secondaryDesignator)) {
				secondaryRiskAddress = secondaryDesignator;
			}
			if (StringUtils.isNotEmpty(secondaryRiskAddress) && (StringUtils.isNotBlank(secondaryNumber) && StringUtils.isNotEmpty(secondaryNumber))) {
				secondaryRiskAddress = secondaryRiskAddress + " " + secondaryNumber;
			}
			if (StringUtils.isEmpty(secondaryRiskAddress) && (StringUtils.isNotBlank(secondaryNumber) && StringUtils.isNotEmpty(secondaryNumber))) {
				secondaryRiskAddress = secondaryNumber;
			}
		} catch (ModelBeanException e) {
			Log.debug("Exception occurred while getting the Secondary RiskAddress");
		}
		return secondaryRiskAddress;
	} 
   
	
}

