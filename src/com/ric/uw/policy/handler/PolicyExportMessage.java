package com.ric.uw.policy.handler;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceContext;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import com.iscs.common.business.provider.Provider;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.error.ErrorTools;

/**
 * Displays export message
 *
 */
public class PolicyExportMessage extends InnovationIBIZHandler {

	/**
	 * Creates a new instance of PolicyExportMessage
	 * 
	 * @throws Exception
	 *             never
	 */
	public PolicyExportMessage() throws Exception {
	}

	/**
	 * Processes a generic service request.
	 * 
	 * @return the current response bean
	 * @throws IBIZException
	 *             when an error requiring user attention occurs
	 * @throws ServiceHandlerException
	 *             when a critical error occurs
	 */
	public ModelBean process() throws IBIZException, ServiceHandlerException {
		try {
			// Log a Greeting
			Log.debug("Processing PolicyExportMessage...");
			ModelBean rs = this.getHandlerData().getResponse();
			ModelBean policy = rs.getBean("Policy");
			JDBCData data = ServiceContext.getServiceContext().getData();
			int providerRef = policy.getBean("BasicPolicy").getValueInt("ProviderRef");
			ModelBean provider = Provider.getProviderBySystemId(data, providerRef);
			String ivansParticipationInd = provider.getBean("ProducerInfo").gets("PolicyExportInd");
			String ivansValidTransDate = provider.getBean("ProducerInfo").gets("PolicyExptValidDtInd");
			StringDate policyExportStartDt = provider.getBean("ProducerInfo").getValueDate("PolicyExportStartDt");
			StringDate policyExportEndDt = provider.getBean("ProducerInfo").getValueDate("PolicyExportEndDt");
			if(ivansParticipationInd.equalsIgnoreCase("Yes") && !ivansValidTransDate.equalsIgnoreCase("Yes")){
				addErrorMsg("General",
						"Policy " + policy.getBean("BasicPolicy").gets("PolicyDisplayNumber")
								+ " was not exported, because this Agency/Agent should have at least one transaction within the Provider Start Date " + policyExportStartDt 
								 + " and End Date " + policyExportEndDt,
						ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_INFO);
				}	
			 else if (ivansParticipationInd.equalsIgnoreCase("Yes")) {
					addErrorMsg("General",
							"Policy " + policy.getBean("BasicPolicy").gets("PolicyDisplayNumber")
									+ " was successfully exported.",
							ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_INFO);
				}
				else {
			
				addErrorMsg("General",
						"Policy " + policy.getBean("BasicPolicy").gets("PolicyDisplayNumber")
								+ " was not exported, because this Agency/Agent is not eligible to Policy Export.",
						ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_INFO);
			}
			// Return the Response ModelBean
			return rs;
		} catch (Exception e) {
			throw new ServiceHandlerException(e);
		}
	}

}
