package com.ric.uw.policy.handler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.web.cmm.AdditionalParams;
import com.iscs.common.render.DynamicString;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.InnovationUtils;
import com.iscs.common.utility.io.FileTools;
import net.inov.tec.date.StringDate;

public class FormatIvansFile extends InnovationIBIZHandler{
	
	/** Wraps all the DTOPolicy beans in the Policy Export folder with a DTORoot node
     * @throws Exception never
     */
    public FormatIvansFile() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a Greeting
            if (Log.isDebugEnabled()) {
            	Log.debug("Processing FormatIvansFile...");
            }
            
            ModelBean rs = this.getHandlerData().getResponse();
            
	       	AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest() );
	        String runDt = ap.gets("RunDt");	        
	        String fullPathName = "";      
       		String directoryPath = DynamicString.render(new ModelBean("Company"), InnovationUtils.getEnvironmentVariable("IVANS-PolicyExport", "directoryPath", ""));
       		String dailyFilename = InnovationUtils.getEnvironmentVariable("IVANS-PolicyExport", "dailyFilename", "");	
       		 
       		//Make sure path exists
       		FileTools.createPath( directoryPath );
       		// extract the run date instead of book date
       		fullPathName = directoryPath + dailyFilename + '_' + runDt;	       		
       		String txtFilename = fullPathName + ".txt";
       		String xmlFilename = fullPathName + ".xml"; 
       		String xmlNoCountFilename = fullPathName + "-no-count.xml";   
       		String line = null;
       		
       		BufferedReader reader = null;
	        BufferedWriter writer = null;
       		
       		if( FileTools.exists(txtFilename) ) {
    			try {
	       			//Create XML file with <DTORoot> element
	       			// Retrieve the original txt file
			        File textFile = new File(txtFilename);      
		    		// Make sure the new xml file is empty by deleting it
			        File xmlFile = new File(xmlNoCountFilename);
			        xmlFile.delete();
			        // Create the new xml file 	        
			        xmlFile = new File(xmlNoCountFilename);
			        reader = new BufferedReader(new FileReader(textFile));
			        writer = new BufferedWriter(new FileWriter(xmlNoCountFilename));
			        // Insert the root node along with the original file contents
			        writer.write("<?xml version='1.0' encoding='UTF-8'?>");
			        writer.newLine();
			        writer.write("<DTORoot>");
			        writer.newLine();
		    		while ((line = reader.readLine()) != null) {
		    			writer.write(line);
		    		}
		    		writer.newLine();
		    		writer.write("</DTORoot>");
		    		writer.flush();
    			}
    			finally {
		    		// Close Buffers
		    		reader.close();
		    		writer.close();
    			}
    			
    			Double totalPremium = new Double("0.00");
				Integer dtoCount = 0;

				//Write final XML file with the TotalPolicies and TotalPremium values as attributes of the <DTORoot> element
				try {
					File textFile = new File(txtFilename);
					reader = new BufferedReader(new FileReader(textFile));
			        writer = new BufferedWriter(new FileWriter(xmlFilename));
			        line = null;
			        
			        // Insert the root node along with the original file contents
			        writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			        writer.newLine();
			        writer.write("<DTORoot RunDate=\""+ runDt +"\" TotalPolicies=\"" + dtoCount.toString() + "\" TotalPremium=\""+ totalPremium.toString() +"\">");
			        writer.newLine();
		    		while ((line = reader.readLine()) != null) {
		    			writer.write(line);
		    		}
		    		writer.newLine();
		    		writer.write("</DTORoot>");
		    		writer.flush();
				}
				finally {	
		    		// Close Buffers
		    		reader.close();
		    		writer.close();
				}
				
				try {					
					//Delete no count file
					File deleteFile = new File(xmlNoCountFilename); 
					deleteFile.delete();
				}
				catch(Exception e) {
					Log.error(e);
					throw new Exception(e);
				}
       		}
       		StringDate runDtStrDt = StringDate.advanceDate(runDt, -1);
       		runDt = runDtStrDt.toString();
	        
       		if (Log.isDebugEnabled()) {
       			Log.debug(String.format("[DTOPolicyExportFileFormatter] Successfully created %1s", xmlFilename));
       		}
       		
            // Return the Response ModelBean
            return rs;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
}
