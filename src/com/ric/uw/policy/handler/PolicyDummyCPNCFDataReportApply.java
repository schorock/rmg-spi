package com.ric.uw.policy.handler;

/*
 * PolicyDummyCPNCFDataReportApply.java
 *
 */

import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.tech.log.Log;

public class PolicyDummyCPNCFDataReportApply implements RuleProcessor {

	/**
	 * Creates a new instance of PolicyCPNCFDataReportApply
	 * 
	 * @throws Exception
	 */
	public PolicyDummyCPNCFDataReportApply() throws Exception {
	}

	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user,
			JDBCData data) throws RuleException {
		ModelBean errors = null;
		try {
			errors = new ModelBean("Errors");
			return errors;
		} catch (Exception e) {
			Log.error(e.getMessage(), e);
			throw new RuleException(e);
		}
	}

}