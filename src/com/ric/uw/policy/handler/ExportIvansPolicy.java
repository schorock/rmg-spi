package com.ric.uw.policy.handler;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceContext;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;
import net.inov.tec.xml.XmlDoc;

import com.iscs.common.business.provider.Provider;
import com.iscs.common.render.DynamicString;
import com.iscs.common.shared.SystemData;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.InnovationUtils;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.io.FileTools;
import com.iscs.uw.common.shared.Form;
import java.text.SimpleDateFormat;
import java.sql.Timestamp;

public class ExportIvansPolicy extends InnovationIBIZHandler {
	
	 /** Exports Policy as DTOPolicy to an ASCII file
     * @throws Exception never
     */
    public ExportIvansPolicy() throws Exception {
    }
    
    public ModelBean process() throws IBIZException, ServiceHandlerException{
    	ModelBean rs = null;
    	String onDemand = null;
        try {
        	
            // Log a Greeting
            Log.debug("Processing ExportDTOPolicy...");
            rs = this.getHandlerData().getResponse();
            AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest() );
            onDemand = ap.gets("OnDemand");
            boolean isOndemandRequired= false;
			// Get Document Archive EnvSettings
			String enableIVANS = InnovationUtils.getEnvironmentVariable("IVANS-PolicyExport", "enable", "");
			if( enableIVANS.length() == 0 || !enableIVANS.equals("Yes")) {		
				Log.debug("IVANS policy download disabled.");
			}	
			else {            
	            // Get the Current Policy ModelBean
	            ModelBean policy = rs.getBean("Policy");
	            
	            JDBCData data = ServiceContext.getServiceContext().getData();
	            
	            int providerRef= policy.getBean("BasicPolicy").getValueInt("ProviderRef");                     
	            ModelBean provider = Provider.getProviderBySystemId(data, providerRef);
	            String ivansParticipationInd = provider.getBean("ProducerInfo").gets("PolicyExportInd");	           
	            StringDate ivansStartDt = provider.getBean("ProducerInfo").getValueDate("PolicyExportStartDt");
    			StringDate ivansEndDt = provider.getBean("ProducerInfo").getValueDate("PolicyExportEndDt");
    			
    			ModelBean basicPolicy=policy.getBean("BasicPolicy");
    			String transactionCd=basicPolicy.gets("TransactionCd");
    			
    			int priorTransactionNumber = basicPolicy.getValueInt("TransactionNumber") - 1;
				ModelBean priorTransaction = basicPolicy.getBean("TransactionHistory","TransactionNumber",String.valueOf(priorTransactionNumber));
				
    			boolean transactionExportInd=true;
    			if(transactionCd.equalsIgnoreCase("Cancellation Notice")){
					transactionExportInd=false;
    			}
    			
    			if(transactionCd.equalsIgnoreCase("Reinstatement")){
    				String previousTransactionCd=priorTransaction.gets("TransactionCd");
    				if(previousTransactionCd.equalsIgnoreCase("Cancellation Notice")){
    					transactionExportInd=false;
    				}
    			}
    			
	            if( ivansParticipationInd.equalsIgnoreCase("Yes") ) {
	              if(transactionExportInd || onDemand.equalsIgnoreCase("Yes")){
	            	boolean participationInd = false;
	            	
	    			ModelBean[] transactionHistory = policy.getBean("BasicPolicy").getBeans("TransactionHistory");
	    			transactionHistory = BeanTools.sortBeansBy(transactionHistory, new String[] {"TransactionNumber"}, new String[] {"Descending"});
	    			
	    			for( int i=0; i<transactionHistory.length; i++ ) {
	    				if( transactionHistory[i].getValueDate("TransactionDt").compareTo(ivansStartDt) >= 0  ) {
	    					if ( transactionHistory[i].getValueDate("TransactionDt").compareTo(ivansEndDt) <= 0 || ivansEndDt.equals("") ) {
		    					participationInd = true;
		    					provider.getBean("ProducerInfo").setValue("PolicyExptValidDtInd", "Yes");
	    						break;
	    					}
	    				}
	    				
	    			}
	    			
	    			if (participationInd) {
	            

			            String fullPathName = "";	            
			       		
			       		String directoryPath = DynamicString.render(new ModelBean("Company"), InnovationUtils.getEnvironmentVariable("IVANS-PolicyExport", "directoryPath", ""));
			       		String dailyFilename = InnovationUtils.getEnvironmentVariable("IVANS-PolicyExport", "dailyFilename", "");	       		
			       		String onDemandFilename = InnovationUtils.getEnvironmentVariable("IVANS-PolicyExport", "onDemandFilename", "");
			       		String policyNumber = policy.getBean("BasicPolicy").gets("PolicyDisplayNumber");
			       		    		
			       		if (onDemand.equals("Yes")) {
			       			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
							Timestamp timestamp = new Timestamp(System.currentTimeMillis());
							String timeNow = sdf.format(timestamp);
			       			FileTools.createPath( directoryPath );        		
			       			fullPathName = directoryPath + onDemandFilename + '_' + timeNow + '_' + policyNumber + ".xml";
						}
						else {
							FileTools.createPath( directoryPath );        		
							fullPathName = directoryPath + dailyFilename + '_' +   DateTools.getStringDate() + ".txt";
						}
			       		
						// Save the file in the location specified
				        File saveFile = new java.io.File(fullPathName);      
				        synchronized (this)
					        {
					        	
						        // Create an empty dto policy bean
					            ModelBean dtoPolicy = new ModelBean("DTOPolicy");
					            	
					        	// Copy all the values from the dto to the application
					        	BeanTools.copyAllValuesToDTO(ModelSpecification.getSharedModel(), policy, dtoPolicy);	
					        	
					        	// Copy the Provider Number into the dtoPolicy
					        	dtoPolicy.getBean("DTOBasicPolicy").setValue("ProviderNumber", provider.gets("ProviderNumber"));
					            
					            // Copy the Product Form Names into the dtoPolicy
					            ModelBean[] forms = policy.getBeans("Form");
					            for( int i = 0; i < forms.length; i++ ) {
					            	if( !forms[i].gets("Status").equals("Deleted") ) {
						            	ModelBean productForm = Form.getProductForm( policy, forms[i].gets("Name"), dtoPolicy.getBean("DTOBasicPolicy").getDate("EffectiveDt") );
						            	dtoPolicy.getBean("DTOForm", "Name", forms[i].gets("Name")).setValue("DisplayDesc", productForm.gets("DisplayDesc"));
					            	}
					            }
	
				            	XmlDoc doc = dtoPolicy.toStandardXml(); 
			            		doc.setOmitDeclaration(true);	
					            
				            	if (onDemand.equals("Yes")) {
				            		FileTools.appendText(saveFile, "<?xml version='1.0' encoding='UTF-8'?>", true);
				            		FileTools.appendText(saveFile, "<DTORoot>", true);
				            		FileTools.appendText(saveFile, doc.toString(), true);
				            		FileTools.appendText(saveFile, "</DTORoot>");
				            	}
				            	else {
				            		// new code
				            		Charset charset = Charset.forName("UTF-8"); 
				            		CharsetEncoder encoder = charset.newEncoder(); 
				            		FileOutputStream fos = new FileOutputStream(saveFile, true);
				            		FileChannel channel = fos.getChannel();
				            		FileLock lock = null;
				            		int count = 0;
				            		try {
				            			while ((lock = performFileLock(channel)) == null && count<=100)  {    						                
				            				Thread.sleep(100);
				            				count++;
				            				if(count == 100)
				            					isOndemandRequired = true;				            				
				            			}    						            
				            			channel.write(encoder.encode(CharBuffer.wrap(doc.toString().toCharArray())));
				            		} catch(Exception ex){
			            				if(isOndemandRequired)
			            				{
				            				Log.debug("IVANS export to daiy file for policy number " +policyNumber + "failed so writing to ondemand file - " + fullPathName);
				            				fullPathName = directoryPath + onDemandFilename + '_' + policyNumber + ".xml";
				            				saveFile = new java.io.File(fullPathName);
						            		FileTools.appendText(saveFile, "<?xml version='1.0' encoding='UTF-8'?>", true);
						            		FileTools.appendText(saveFile, "<DTORoot>", true);
						            		FileTools.appendText(saveFile, doc.toString(), true);
						            		FileTools.appendText(saveFile, "</DTORoot>");
			            				}				            			
				            			Log.debug("File locked. Retrying in 100ms " +ex );
				            			throw(ex);
				            		} finally {
				            			if (lock !=null)
				            				lock.release();
				            			if (channel!=null)
				            				channel.close();
				            			if (fos!=null)
				            				fos.close();
				            		}
				            	}	
					        }
			        
	    			}
	              }
	            }
			}
            
            // Return the Response ModelBean
            return rs;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
    public FileLock performFileLock(FileChannel channel) {
	FileLock lock = null;
	try{
		lock = channel.tryLock();
	} catch(Exception ex){
		Log.error("Failed to lock IVANS file...wait" +ex );
		return null;
	}
	return lock;
}  
    


     
	
	

}
