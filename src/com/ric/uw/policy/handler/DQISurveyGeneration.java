/*
 * DQISurveyGeneration.java
 *
 */

package com.ric.uw.policy.handler;

import com.iscs.common.render.DateRenderer;
import com.iscs.common.tech.service.ExternalServiceBase;
import com.iscs.common.tech.service.ExternalServiceHandler;
import com.iscs.document.render.DCRenderer;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

/** Set up and call the process DQI related outputs and tasks 
 *
 *
 */
public class DQISurveyGeneration extends ExternalServiceBase implements ExternalServiceHandler {
    
    /** Creates a new instance of DQISurveyGeneration */
    public DQISurveyGeneration() throws Exception {
        super();
    }
    
    /** Obtain the Webpath service name that should be called associated with this external service call
     */
    public String getServiceName() {
        return "UWDQISurveyGeneration";
    }
    
    /** Process the results from the external webpath service call
     * @param response The response ModelBean
     * @param bean The container bean associated with this service call
     * @param user The user bean
     * @param data The JDBC data repository connection
     * @return ModelBean response ModelBean
     * @throws Exception when an unexpected error occurs
     * @throws IBIZException when a business error occurs
     * @throws ServiceHandlerException when a service handler error occurs
     */ 
    public ModelBean processEnd(ModelBean response, ModelBean bean, ModelBean user, JDBCData data)
    throws Exception, IBIZException, ServiceHandlerException {
        return response;
    }
    
    /** Process the setup information required to call the external webpath service.
     * @param request The request ModelBean from the current external service call
     * @param bean The container bean associated with this service call
     * @param user The user bean
     * @param data The JDBC data repository connection
     * @return ModelBean response ModelBean
     * @throws Exception when an unexpected error occurs
     * @throws IBIZException when a business error occurs
     * @throws ServiceHandlerException when a service handler error occurs
     */
    public ModelBean processStart(ModelBean request, ModelBean bean, ModelBean user, JDBCData data) 
    throws Exception, IBIZException, ServiceHandlerException {
        ModelBean rq = createRequest(request, getServiceName() + "Rq");
        
        setCMM(rq, null, "Policy", "Policy", bean.getSystemId(), null, null);
        
        addAdditionalParam(rq, "TransactionCd", "Renewal");
        
        // Obtain the application from the previous response
        rq.addValue(bean);
        
        //Add logic to generate Survey and Cover Letter 
        
       	ModelBean responseParams = request.getBean("ResponseParams");
       	String userId = responseParams.gets("UserId");
       	ModelBean policy = bean;
       	String todayDt = DateRenderer.getDate(responseParams);
        String todayTm = DateRenderer.getTime(responseParams);
        String securityId = responseParams.gets("SecurityId");
        String sessionId = responseParams.gets("SessionId");
        String conversationId = responseParams.gets("ConversationId");
        String submissionNumber = "";
		String logKey = "";
		StringDate todayStringDt = DateRenderer.getStringDate(responseParams);
		
		ModelBean output = DCRenderer.addCorrespondence(data, policy, "UnderwritingQuestionnaire", "UWPolicy", submissionNumber, userId, todayStringDt, logKey);
	    output.setValue("AddUser","System");
	    output.setValue("AddDt",todayDt);
	    output.setValue("AddTm",todayTm);
		DCRenderer.processOutput(data,userId,securityId, sessionId, conversationId,policy,output,todayStringDt,todayTm);
        
        return rq;
    }
    
}
