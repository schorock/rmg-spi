/*
 * RICProductRenderer.java
 *
 */

package com.ric.uw.policy.render;

import java.util.ArrayList;

import com.iscs.common.render.FieldRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.NameValuePair;
import com.iscs.uw.app.Application;
import com.iscs.uw.policy.render.PolicyRenderer;

import net.inov.tec.beans.ModelBean;


public class RICPolicyRenderer extends PolicyRenderer {
	
    /** Creates a New Instance of RICProductRenderer */
    public RICPolicyRenderer() {
    }

	public String getContextVariableName() {
		return "RICPolicyRenderer";
	}

	
	/**
	 * Override PolicyRenderer termSelectField method. Removes select.. option from the Term field. 
	 * Generates a HTML SELECT Tag with all the Product Term Options Populated
     * @param fieldId SELECT Field Name and ID
     * @param defaultValue Select List Default Value
     * @param rs Response ModelBean
     * @param onChangeAction SELECT OnChange Action
     * @param extra SELECT Tag Attributes
     * @return HTML SELECT Tag
     */
    public static String termSelectField( String fieldId, String defaultValue, ModelBean rs, String onChangeAction, String extra ) {
        try {
            ModelBean productSetup = Application.getProductSetup(rs);
            ModelBean productTerms = productSetup.getBean("ProductTerms");
            ModelBean[] productTerm = productTerms.getBeans("ProductTerm");
            
            ArrayList<NameValuePair> options = new ArrayList<NameValuePair>();
            for( int i = 0; i < productTerm.length; i++ ) {
                String shortDesc = productTerm[i].gets("ShortDesc");
                NameValuePair selOption = new NameValuePair(shortDesc, shortDesc);
                options.add(selOption);
            }
            
            FieldRenderer renderer = new FieldRenderer();
            return renderer.select(fieldId, defaultValue, options, onChangeAction, extra, false );
        } catch (Exception e) {
            e.printStackTrace();
            Log.error("Error", e);
            return "";
        }
    }
    
    

}
