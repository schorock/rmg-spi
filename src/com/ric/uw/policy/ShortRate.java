/*
 * ShortRate.java
 *
 */

package com.ric.uw.policy;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.math.Money;

import com.ibm.math.BigDecimal;
import com.iscs.uw.common.rule.ShortRateRule;
import com.iscs.uw.policy.PM;

/** ShortRate implements ShortRateRule
 *
 * @author  Benjamin S. Olsen
 */
public class ShortRate implements ShortRateRule {
    // Create an object for logging messages
    
    
    /** Creates a new instance of ShortRate */
    public ShortRate() {
    }
    
	public boolean process(ModelBean bean, String transactionCd, ModelBean productSubType, double proRataFactor) throws Exception {

		String writtenField = "WrittenPremiumAmt";
		if( bean.getBeanName().equals("Fee") )
			writtenField = "WrittenAmt";
		
		if( transactionCd.equals(PM.TX_CANCELLATION) ) {
			// Short Rate Factor
			BigDecimal shortRateFactor = new BigDecimal(productSubType.gets("ShortRateData"));
			
			// Calculate new written amount			
			Money written = new Money(bean.gets(writtenField));
			Money shortRateWritten = written.multiply(shortRateFactor);
	        Money shortRateAmt = shortRateWritten.subtract(written);
	        
	        // Save new amounts
	        bean.setValue("ShortRateAmt",shortRateAmt);
	        bean.setValue(writtenField, shortRateWritten);
		}
		else if( transactionCd.equals(PM.TX_REINSTATEMENT) || transactionCd.equals(PM.TX_REINSTATEMENT_WITH_LAPSE) ) {
			Money written = new Money(bean.gets(writtenField));
			written = written.subtract(new Money(bean.gets("ShortRateAmt")));
			bean.setValue(writtenField,written);
		}
		
		return false;
	}
    
}
