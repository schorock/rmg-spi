package com.ric.uw.policy.conversion;

import java.util.Hashtable;

import org.jdom.Element;

import com.iscs.common.business.provider.Provider;
import com.iscs.common.business.rule.RuleManager;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.conversion.DataConversionHandler;
import com.iscs.conversion.DataConversionHelper;
import com.iscs.conversion.DataConversionSimple;
import com.iscs.uw.common.coverage.Coverage;
import com.iscs.uw.common.risk.Risk;
import com.iscs.uw.policy.PM;
import com.iscs.workflow.Task;

import net.inov.biz.server.ServiceContext;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanField;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;
import net.inov.tec.math.Money;
import net.inov.tec.xml.XmlDoc;


/** Convert the Application ModelBean from the original system into a Policy processing.  
 * All Date Fields must contain the format of MM/dd/yyyy.  
 * All Phone Number fields must contain the format 999-999-9999 x9999
 * 
 * @author ashutoshs
 *
 */
public class PolicyDataConversion extends com.iscs.uw.policy.conversion.PolicyDataConversion {

	//Removed Rate area from the premium bean check
	protected String[] premiumBeans = new String[] {"Line", "Risk", "Coverage", "CoverageItem", "Fee", "BasicPolicy", "TransactionHistory", "CommissionArea"};
	protected String[] existBeans = new String[] {"Line", "Risk", "Coverage", "CoverageItem", "Fee"};
	protected String[] fixedAmts = new String[] {"WrittenPremiumAmt", "FullTermAmt", "FullAmt", "WrittenAmt", "CommissionAmt", "TransactionCommissionAmt", "FinalPremiumAmt", "FinalAmt", "FullTermFeeAmt", "FinalFeeAmt"};
	protected ModelBean policyBean = null;
	protected boolean policyRead = false;
	protected boolean allowPartialCommitInd = false;
	protected ModelBean committedApp = null;
	protected boolean failInd = false;
	
	public PolicyDataConversion() throws Exception {
		super();
	}
		
    
    /** Determine if the current transaction passed in is allowable to the current system.  The following rules
     * determine if a transaction is allowed:
     * 1) The policy number passed in from BasicPolicy.PolicyNumber and BasicPolicy.PolicyVersion does not 
     * exist in the system current.
     * 2) The policy number exists in the system, and the current transaction number is greater than what is 
     * in the system
     * @param dtoBean The DTOApplication ModelBean
     * @return true/false if the Application should continue to process
     * @throws Exception when an error occurs
     */
    protected boolean validateTransaction(ModelBean dtoBean)
    throws Exception {
    	ModelBean basicPolicy = dtoBean.getBean("DTOBasicPolicy");
    	boolean isValid = true;
    	// Validate the current Transaction
    	ModelBean transactionInfo = dtoBean.getBean("DTOTransactionInfo");
    	if (!DataConversionHelper.validateField(transactionInfo, "SourceCd", null, true, getErrors())) {
    		isValid = false;
    	}
    	    	
    	// Validate the basic policy information if Source is different than Innovation
    	if (!transactionInfo.gets("SourceCd").equals(PM.SOURCE_CODE)) {
//        	if (!DataConversionHelper.validateField(basicPolicy, "PolicyNumber", null, true, getErrors()))
//        		isValid = false;
//        	if (!DataConversionHelper.validateField(basicPolicy, "PolicyDisplayNumber", null, true, getErrors()))
//        		isValid = false;
        	if (!DataConversionHelper.validateField(basicPolicy, "PolicyVersion", null, true, getErrors()))
        		isValid = false;
        	if (isValid) {
        		JDBCData data = ServiceContext.getServiceContext().getData();
        		ModelBean policy = findBean(data, basicPolicy.gets("PolicyDisplayNumber"));
        		if (policy != null) {
        			// Determine if the current Transaction to convert is the next valid transaction in Innovation
        			ModelBean basicP = policy.getBean("BasicPolicy");
        			int sysTransNo = Integer.parseInt(basicP.gets("TransactionNumber"));
        			int convTransNo = Integer.parseInt(basicPolicy.gets("TransactionNumber"));
        			if (convTransNo != sysTransNo + 1) { 
        				isValid = false;
        				String msg = "The current transaction to convert, " + convTransNo + " is not the next transaction in Innovation, which should be " + (sysTransNo+1);
        				addErrorMsg("ValidationError", msg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
        			}
        			// Make sure the policy has not been touched/updated in Innovation
        			ModelBean[] transactions = basicP.findBeansByFieldValue("TransactionHistory", "SourceCd", PM.SOURCE_CODE);
        			if (transactions.length > 0) {
        				isValid = false;
        				String msg = "The current transaction to convert, " + convTransNo + " for policy " + basicPolicy.gets("PolicyDisplayNumber") + " is not valid because the policy has been updated within Innovation";
        				addErrorMsg("ValidationError", msg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);        				
        			}        			
        			
        			// If this is a Reapply transaction, only reapply in order
        	    	String replaceTransNo = transactionInfo.gets("ReplacementOfTransactionNumber");
        	    	if (!replaceTransNo.equals("")) {
        	    		ModelBean[] changes = BeanTools.sortBeansBy(policy.getBeans("ChangeInfo"), new String[] {"TransactionNumber"}, new String[] {"Ascending"});
        	    		for (int i=0; i<changes.length; i++) {
        	    			if (changes[i].gets("Status").equals("Unapplied")) {
        	    				if (!changes[i].gets("TransactionNumber").equals(replaceTransNo)) {
        	    					isValid = false;
        	    					String msg = "The current transaction " + replaceTransNo + " to Reapply, must be done in order off all reapplies required.  The first reapply transaction is transaction number " + changes[i].gets("TransactionNumber");
        	        				addErrorMsg("ValidationError", msg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);        	    					
        	    				}
        	    				break;
        	    			}
        	    		}
        	    	}        	    	
        		} else {
        			// If the policy cannot be found, make sure its the first Transaction.
        			int convTransNo = Integer.parseInt(basicPolicy.gets("TransactionNumber"));
        			if (convTransNo > 1) {
        				isValid = false;
        				String msg = "The current transaction number of " + convTransNo + " indicates it is not the first transaction.  However the policy cannot be located in the system.";
        				addErrorMsg("ValidationError", msg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
        			}        				
        		}
        	}   

        	// Validate if the Transaction is a renewal or rewrite it must have a policy reference number
        	if ((transactionInfo.gets("TransactionCd").equals(PM.TX_RENEWAL) || transactionInfo.gets("TransactionCd").equals(PM.TX_RENEWAL_START)) 
        			&& basicPolicy.gets("RenewedFromPolicyNumber").equals("")) {
				isValid = false;
				String msg = "The current renewal transaction must reference a previous Policy Number in the DTOBasicPolicy.RenewedFromPolicyNumber node/attribute";
				addErrorMsg("ValidationError", msg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
        	} else if ((!transactionInfo.gets("TransactionCd").equals(PM.TX_RENEWAL) && !transactionInfo.gets("TransactionCd").equals(PM.TX_RENEWAL_START)) 
        			&& !basicPolicy.gets("RenewedFromPolicyNumber").equals("")) {
				isValid = false;
				String msg = "The DTOBasicPolicy.RenewedFromPolicyNumber node/attribute can only be specified on Renewal and Renewal Start transactions";
				addErrorMsg("ValidationError", msg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);        		
        	} else if ((transactionInfo.gets("TransactionCd").equals(PM.TX_REWRITE_NEW) || transactionInfo.gets("TransactionCd").equals(PM.TX_REWRITE_RENEWAL)) 
        			    && basicPolicy.gets("RewriteFromPolicyNumber").equals("")) {
				isValid = false;
				String msg = "The current rewrite transaction must reference a previous Policy Number in the DTOBasicPolicy.RewriteFromPolicyNumber node/attribute";
				addErrorMsg("ValidationError", msg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
        	} else if ((!transactionInfo.gets("TransactionCd").equals(PM.TX_REWRITE_NEW) && !transactionInfo.gets("TransactionCd").equals(PM.TX_REWRITE_RENEWAL)) 
        			    && !basicPolicy.gets("RewriteFromPolicyNumber").equals("")) {
				isValid = false;
				String msg = "The DTOBasicPolicy.RewriteFromPolicyNumber node/attribute can only be specified on Rewrite New and Rewrite Renewal transactions";
				addErrorMsg("ValidationError", msg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
        	}
        	
        	// Validate that prior term pending changes fields cannot be set
        	if (!basicPolicy.gets("PriorTermPendingChangeTransactionNumber").equals("") || !basicPolicy.gets("PriorTermPendingChangePolicyRef").equals("")) {
				isValid = false;
				String msg = "The DTOBasicPolicy.PriorTermPendingChangeTransactionNumber and/or DTOBasicPolicy.PriorTermPendingChangePolicyRef node/attribute can not be set directly.";
				addErrorMsg("ValidationError", msg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);        		
        	}
    	}
    	return isValid;
    }
    
    /** Compare all of the existence ModelBeans to make sure no new Beans are created, or possibly were not 
     * reported on a future transaction.  (i.e. The continuing transactions did not report a ModelBean that was
     * deleted, deleted in the old system but not reported as deleted in this conversion) 
     * @param dtoBean The DTO ModelBean
     * @param originalBean The original converted ModelBean
     * @param response The response from the service chain call
     * @return true/false if the existence check is valid
     * @throws Exception when an error occurs
     */
    public boolean validateBeanExist(ModelBean dtoBean, ModelBean originalBean, ModelBean response) 
    throws Exception {
    	boolean isValid = true;
    	for (int i=0; i<existBeans.length; i++) {
    		ModelBean[] beans = response.getAllBeans(premiumBeans[i]);
    		for (int k=0; k<beans.length; k++) {
    			// For this type of Bean, find the matching Bean in the processed bean and validate its status
    			// if present in the original Bean
    			String statusField = null;
    			if (!beans[k].hasBeanField("Status") && !beans[k].hasBeanField("StatusCd")) {
    				continue;    				
    			} else {
    				if (beans[k].hasBeanField("Status"))
    					statusField = "Status";
    				else 
    					statusField = "StatusCd";    						
    			}
    			String statusCd = beans[k].gets(statusField);
    			if (statusCd.equals("Active")) {
        			ModelBean foundBean = findMatchingBean(beans[k], originalBean);
        			if (foundBean != null) {
        				if (!statusCd.equals(foundBean.gets(statusField))) {
                			addErrorMsg("ValidationError", "The bean " + foundBean.getBeanName() + " with id of " + foundBean.getId() + " has a status of " + foundBean.gets(statusField) + " which does not match the current processed status of " + statusCd, ErrorTools.FIELD_CONSTRAINT_ERROR);
                			isValid = false;
        				}
        			} else {
            			addErrorMsg("ValidationError", "The bean " + beans[k].getBeanName() + " with id of " + beans[k].getId() + " should exist in the incoming DTO model, but does not at all. ", ErrorTools.FIELD_CONSTRAINT_ERROR);
            			isValid = false;
        			}    				
    			} else {
    				ModelBean foundBean = originalBean.getBeanById(beans[k].getId());
    				if (foundBean != null && foundBean.gets(statusField).equals("Active")) {
            			addErrorMsg("ValidationError", "The bean " + foundBean.getBeanName() + " with id of " + foundBean.getId() + " has a status of " + foundBean.gets(statusField) + " which does not match the current processed status of " + statusCd, ErrorTools.FIELD_CONSTRAINT_ERROR);    					
            			isValid = false;
    				}
    			}
    		}    		
    	}
    	return isValid;
    	
    }
    
    public boolean validatePremium(ModelBean dtoBean, ModelBean originalBean, ModelBean response) throws Exception {
		    boolean isValid = true;
		    for (int i=0; i<premiumBeans.length; i++) {
		    	ModelBean[] beans = originalBean.getAllBeans(premiumBeans[i]);
		    	for (int k=0; k<beans.length; k++) {
		    		// For this type of Bean, find the matching Bean in the processed bean and validate its value
		    		// if present in the original Bean
		    		for (int x=0; x<fixedAmts.length; x++) {
		        		if (!beans[k].hasBeanField(fixedAmts[x]))
		        			continue;
		        		String value = beans[k].gets(fixedAmts[x], null);
		        		if (value != null) {
		            		Money oldAmt = new Money(value);
		            		ModelBean foundBean = findMatchingBean(beans[k], response);
		            		if (foundBean != null) {
		                		Money newAmt = new Money(foundBean.gets(fixedAmts[x]));
		                		if (oldAmt.compareTo(newAmt) != 0) {
		                    		addErrorMsg("ValidationError", "There is a difference in the amounts for node DTO" + beans[k].getBeanName() + " and the attribute " + fixedAmts[x] + ".  Old value is " + oldAmt + " and new value is " + newAmt, ErrorTools.FIELD_CONSTRAINT_ERROR);
		                    		isValid = false;
		                		}
		            		}
		        		}    				
		    		}
		    	}
		    }
		    return isValid;
	}
    
    /** Add one row of the 'Policy' specific data to the audit report
	 * @param xmlDoc Original XML conversion document
	 * @param resultBean Result of the conversion process
	 * @return String array of Container specific audit data
	 */
	public String[] getAuditData(XmlDoc xmlDoc, ModelBean resultBean) throws Exception {
		
		// Initialize return values
		String module = "";
		String policyNumber = "";
		String policyVersion = "";
		String transactionNumber = "";
		String transactionCd = "";
		String applicationNumber = "";
		String conversionContext = "";
		
		// Populate return vales
		Element container = xmlDoc.getRootElement();
		if( container != null ) {
			
			if( container.getName().equals("DTOAccount") ) {

				module = "AR";
				policyNumber = container.getAttributeValue("AccountDisplayNumber");
				policyVersion = "-";
				
				if( resultBean != null ) {
					// Get the last transaction
					ModelBean[] arTrans = resultBean.getBeans("ARTrans");	
					if( arTrans.length > 0 ) {
						transactionNumber = arTrans[arTrans.length - 1].gets("Reference");
						transactionCd = arTrans[arTrans.length - 1].gets("TypeCd");
					}
				}
			}
			else {

				module = "UW";
				Element basicPolicy = container.getChild("DTOBasicPolicy");
						
				if( basicPolicy != null ) {
					policyNumber = basicPolicy.getAttributeValue("PolicyNumber");
					// Added the code for COIC - to get the policy number from the result bean
					// because the DTO bean won't have the policy number
					if(policyNumber == null){
						if(resultBean != null && resultBean.getBean("BasicPolicy") != null){
							ModelBean convertedBasicPolicy = resultBean.getBean("BasicPolicy");
							if(convertedBasicPolicy != null){
								policyNumber = convertedBasicPolicy.gets("PolicyDisplayNumber");
							}
						}

					}
					policyVersion = basicPolicy.getAttributeValue("ProductVersionIdRef");
					if( policyVersion.equals("") ) {
						policyVersion = "-";
					}
					transactionNumber = basicPolicy.getAttributeValue("TransactionNumber");
					transactionCd = basicPolicy.getAttributeValue("TransactionCd");
					conversionContext =  basicPolicy.getAttributeValue("ConversionContext");
					if(conversionContext == null)
					conversionContext = "";
				}
				
				if (policyNumber.isEmpty()) {
					// The conversion is sourced from Innovation, therefore it is a conversion of renewal in Innovation
			    	Element transactionInfo = container.getChild("DTOTransactionInfo");
			    	String sourceCd = transactionInfo.getAttributeValue("SourceCd");
			    	if (sourceCd.equals(PM.SOURCE_CODE)) {
			    		ModelBean basicPolicyBean = resultBean.getBean("BasicPolicy"); 
			    		policyNumber = basicPolicyBean.gets("PolicyNumber");
						policyVersion = basicPolicyBean.gets("ProductVersionIdRef");
						transactionNumber = basicPolicyBean.gets("TransactionNumber");
						transactionCd = basicPolicyBean.gets("TransactionCd");
			    	}

				}
				
				if( allowPartialCommitInd && failInd ) {		
					// Read from a saved copy of the Application since the 'resultBean' may be null after a failure during policy issue
					if( committedApp != null && !committedApp.getSystemId().equals("") ) {
						applicationNumber = committedApp.gets("ApplicationNumber");
					}
				}
			}
		}
		
		// Return data
		return new String[] {module, policyNumber, policyVersion, transactionNumber, transactionCd, applicationNumber, conversionContext};
	}

    
	/** Set up the audit report with the 'Policy' specific column headers
	 * @return String array of Container specific audit headers
	 */
	public String[] getAuditHeader() throws Exception {
		
		return new String[] {"Module","Policy#","Version","Txn#","TxnCd","App#","ConversionContext"};
	}
	
	/** Pre Validate the DTO ModelBean before it becomes one.  This processes the XmlDoc version of the ModelBean.
	 * This is done for validation of the incoming data types.  For validation of data types such as dates, or numeric 
	 * data such as SSN, or other types.
	 * @param xmlDoc The Xml Document object
	 * @return true/false if the validation was successful
	 * @throws Exception when an error occurs
	 */
	protected boolean preDTOValidate(XmlDoc xmlDoc)
	throws Exception {
		// Only process DTOApplication 
		if (!xmlDoc.getRootElement().getName().equals("DTOApplication"))
			return true;
		
		boolean isValid = true;
		
		// Only process the current DTO tied to this processor 
		if (!xmlDoc.getRootElement().getName().equals(getDTOBeanName()))
			return true;
				
		// Obtain the list of date fields
		if (dateFields == null)
			dateFields = DataConversionHelper.findDateFields(getBeanName());
				
		if (!DataConversionHelper.hasValidDates(xmlDoc, dateFields, errorList))
			return false;

		// Validate the Phone numbers
    	Element[] fieldElems = xmlDoc.selectElements("//PhoneInfo");
    	for (int k=0; k<fieldElems.length; k++) {
    		String phoneStr = fieldElems[k].getAttributeValue("PhoneNumber");
    		if (phoneStr != null && !super.isValidPhone(phoneStr)) {
    			String msg = "Field PhoneInfo.PhoneNumber with the value of " + phoneStr + " has an incorrect phone format. It should be " + super.getValidPhone();
    			addErrorMsg("ValidationError", msg, ErrorTools.FIELD_CONSTRAINT_ERROR);
    			isValid = false;        			    			
    		}
    	}    								

    	// Validate email addresses
    	fieldElems = xmlDoc.selectElements("//EmailInfo");
    	for (int k=0; k<fieldElems.length; k++) {
    		String emailAddr = fieldElems[k].getAttributeValue("EmailAddr");
    		if (emailAddr != null && !super.isValidEmail(emailAddr)) {
    			String msg = "Field EmailInfo.EmailAddr with the value of " + emailAddr + " has an incorrect email address format. It should be in the form of " + super.getValidEmail();
    			addErrorMsg("ValidationError", msg, ErrorTools.FIELD_CONSTRAINT_ERROR);
    			isValid = false;        			    			    			
    		}
    	}
    	    	
    	// Validate SSN 
    	fieldElems = xmlDoc.selectElements("//TaxInfo");
    	for (int k=0; k<fieldElems.length; k++) {
    		String ssn = fieldElems[k].getAttributeValue("SSN");
    		if (ssn == null){
    			continue;
    		}
    		if (super.isMaskedField(ssn)){
    			addToRestorationList("TaxInfo", fieldElems[k].getAttributeValue("id"), "SSN" );
    		} else if (!super.isValidSSN(ssn)) {
    			String msg = "Field TaxInfo.SSN with the value of " + ssn + " has an incorrect Social Security Number format. It should be in the form of " + super.getValidSSN();
    			addErrorMsg("ValidationError", msg, ErrorTools.FIELD_CONSTRAINT_ERROR);
    			isValid = false;        			    			    			        			    			
    		}
    	}
    	
    	// Validate FEIN
    	for (int k=0; k<fieldElems.length; k++) {
    		String fein = fieldElems[k].getAttributeValue("FEIN");
    		if (fein == null) {
    			continue;
    		}
    		if (super.isMaskedField(fein)){
    			addToRestorationList("TaxInfo", fieldElems[k].getAttributeValue("id"), "FEIN" );
    		} else if (!super.isValidFEIN(fein)) {
    			String msg = "Field TaxInfo.FEIN with the value of " + fein + " has an incorrect Federal Employer Identification Number format. It should be in the form of " + super.getValidFEIN();
    			addErrorMsg("ValidationError", msg, ErrorTools.FIELD_CONSTRAINT_ERROR);
    			isValid = false;        			    			
    		}
    	}
    	
    	fieldElems = xmlDoc.selectElements("//Addr");
    	for (int k=0; k<fieldElems.length; k++) {
    		//map regioncd to regionisocd and vice versa
    		String[] regionCdArray = new String[]{fieldElems[k].getAttributeValue("RegionCd"), fieldElems[k].getAttributeValue("RegionISOCd")};
    		regionCdArray = DataConversionHelper.validateAndMapRegionCd(regionCdArray);
    		fieldElems[k].setAttribute("RegionCd", regionCdArray[0]);
    		fieldElems[k].setAttribute("RegionISOCd", regionCdArray[1]);
    		String regionISOCd = fieldElems[k].getAttributeValue("RegionISOCd");
        	// Validate State Codes    		
    		String stateCd = fieldElems[k].getAttributeValue("StateProvCd");
    		if (stateCd != null && !super.isValidStateCode(stateCd, regionISOCd)) {
    			String msg = "Field Addr.StateProvCd with the value of " + stateCd + " does not have the valid format";
    			addErrorMsg("ValidationError", msg, ErrorTools.FIELD_CONSTRAINT_ERROR);
    			isValid = false;        			    			    			
    		}
        	// Validate Postal Zip Codes
    		String zipCd = fieldElems[k].getAttributeValue("PostalCode");
    		if (zipCd != null && !super.isValidZipCode(zipCd, regionISOCd)) {
    			String msg = "Field Addr.PostalCode with the value of " + zipCd + " does not have the valid format";
    			addErrorMsg("ValidationError", msg, ErrorTools.FIELD_CONSTRAINT_ERROR);
    			isValid = false;        			    			    			
    		}
    	}    	    	
    	
    	// Validate ACHBankAccountNumber 
    	fieldElems = xmlDoc.selectElements("//ElectronicPaymentSource");
    	for (int k=0; k<fieldElems.length; k++) {
    		String achBankAccountNumber = fieldElems[k].getAttributeValue("ACHBankAccountNumber");
    		if (achBankAccountNumber == null) {
    			continue;
    		}	
    		if (super.isMaskedField(achBankAccountNumber)){
    			addToRestorationList("ElectronicPaymentSource", fieldElems[k].getAttributeValue("id"), "ACHBankAccountNumber" );
    		} else if (!super.isValidACHBankAccountNumber(achBankAccountNumber)) {
    			String msg = "Field ElectronicPaymentSource.ACHBankAccountNumber with the value of " + achBankAccountNumber + " has an incorrect ACH Bank Account Number format. It should be in the form of " + super.getValidACHBankAccountNumber();
    			addErrorMsg("ValidationError", msg, ErrorTools.FIELD_CONSTRAINT_ERROR);
    			isValid = false;        			    			    			        			    			
    		}
    	}
    	    	
		return isValid;
	}
}
