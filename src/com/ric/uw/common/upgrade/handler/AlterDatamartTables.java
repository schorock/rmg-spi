package com.ric.uw.common.upgrade.handler;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCConfig;
import net.inov.tec.data.JDBCData;

public class AlterDatamartTables extends InnovationIBIZHandler{
	
	/** Creates a new instance of AlterDatamartTables
	 * @throws Exception never
	 */
	public AlterDatamartTables() throws Exception {
	}
	
	public ModelBean process() throws IBIZException, ServiceHandlerException {	
		try {
			Log.debug("Processing AlterDatamartTables...");
			//Add new columns
			addColumntoDataMartTables();
		}
		catch(Exception e){
			Log.error("Unable to create datamart tabled for XactProcess");		
		}
		
		return null;
		}


	protected ModelBean addColumntoDataMartTables() throws IBIZException, ServiceHandlerException {
		JDBCData dataMartConnection = null;
		try{
			Log.debug("Processing AlterDatamartTables...");
			dataMartConnection = new JDBCData(new JDBCConfig("jdbc/jdbc_datamart"));
			//Code to create XactListenerProcessor table
			String tableName = "";
			String columnName = "";
			createXactProcessorTable(dataMartConnection);
			tableName = "XactListenerProcessor";
			columnName = "UpdateCount";	

			if(!columnExists(dataMartConnection, tableName, columnName)){
				addIntegerColumn(dataMartConnection, tableName, columnName);
				dataMartConnection.commit();
			}
			
			tableName = "XactListenerProcessor";
			columnName = "UpdateUser";
			if(!columnExists(dataMartConnection, tableName, columnName)){
				addColumn(dataMartConnection, tableName, columnName);
				dataMartConnection.commit();
			}
			
			tableName = "XactListenerProcessor";
			columnName = "UpdateTimestamp";
			if(!columnExists(dataMartConnection, tableName, columnName)){
				addColumn(dataMartConnection, tableName, columnName);
				dataMartConnection.commit();
			}
			
			tableName = "XactListenerProcessor";
			columnName = "StatusCd";
			if(!columnExists(dataMartConnection, tableName, columnName)){
				addColumn(dataMartConnection, tableName, columnName);
				dataMartConnection.commit();
			}
			
			tableName = "XactListenerProcessor";
			columnName = "ProcessingStatusCd";
			if(!columnExists(dataMartConnection, tableName, columnName)){
				addColumn(dataMartConnection, tableName, columnName);
				dataMartConnection.commit();
			}
			
			tableName = "XactListenerProcessor";
			columnName = "ZipFileName";
			if(!columnExists(dataMartConnection, tableName, columnName)){
				addColumn(dataMartConnection, tableName, columnName);
				dataMartConnection.commit();
			}
			
			tableName = "XactListenerProcessor";
			columnName = "ClaimRef";
			if(!columnExists(dataMartConnection, tableName, columnName)){
				addColumn(dataMartConnection, tableName, columnName);
				dataMartConnection.commit();
			}
			
			tableName = "XactListenerProcessor";
			columnName = "ReplacedByRef";
			if(!columnExists(dataMartConnection, tableName, columnName)){
				addColumn(dataMartConnection, tableName, columnName);
				dataMartConnection.commit();
			}
			
			tableName = "XactListenerProcessor";
			columnName = "ProcessedDt";
			if(!columnExists(dataMartConnection, tableName, columnName)){
				addDateTimeColumn(dataMartConnection, tableName, columnName);
				dataMartConnection.commit();
			}
			
			tableName = "XactListenerProcessor";
			columnName = "ProcessedTm";
			if(!columnExists(dataMartConnection, tableName, columnName)){
				addColumn(dataMartConnection, tableName, columnName);
				dataMartConnection.commit();
			}
			
			tableName = "XactListenerProcessor";
			columnName = "LogFile";
			if(!columnExists(dataMartConnection, tableName, columnName)){
				addColumn(dataMartConnection, tableName, columnName);
				dataMartConnection.commit();
			}
			
			tableName = "XactListenerProcessor";
			columnName = "FileTransferStatusCd";
			if(!columnExists(dataMartConnection, tableName, columnName)){
				addColumn(dataMartConnection, tableName, columnName);
				dataMartConnection.commit();
			}
			
			//End of changes create XactListenerProcessor table
			
			//Code to create XactMatchCriteria table
			
			createXactMatchCriteriaTable(dataMartConnection);			
			tableName="XactMatchCriteria";
			columnName ="MatchInd";		
			if(!columnExists(dataMartConnection, tableName, columnName)){
				addColumn(dataMartConnection, tableName, columnName);
				dataMartConnection.commit();
			}
			
			
			tableName="XactMatchCriteria";
			columnName ="TypeCd";		
			if(!columnExists(dataMartConnection, tableName, columnName)){
				addColumn(dataMartConnection, tableName, columnName);
				dataMartConnection.commit();
			}
			
			tableName="XactMatchCriteria";
			columnName ="Value";		
			if(!columnExists(dataMartConnection, tableName, columnName)){
				addColumn(dataMartConnection, tableName, columnName);
				dataMartConnection.commit();
			}
			
			//End of changes to create XactMatchCriteria table
		}
		catch( Exception e ){
			try {
				Log.error(e);
				if( dataMartConnection!=null) dataMartConnection.rollback();
			} catch( SQLException sqle){
				Log.error(sqle);
			}
			throw new ServiceHandlerException(e);

		} finally {
			if (dataMartConnection != null) {
				dataMartConnection.closeConnection();
			}
		}
		
	return null;
	}


	public void createXactProcessorTable(JDBCData data) throws SQLException {
		try {
			String sql = null;
			sql = "create table XactListenerProcessor(SystemId int(11),ParentId varchar(150),CMMContainer varchar(150),Id varchar(150),RowId bigInt(20) NOT NULL AUTO_INCREMENT PRIMARY KEY);";
			data.getDelegate().prepareAndExecStmt(data, sql);
		} catch (SQLException sqle) {
			Log.error("Table XactListenerProcessor already exists");
		}
	}

	public void createXactMatchCriteriaTable(JDBCData data) throws SQLException {
		try {
			String sql = null;
			sql = "create table XactMatchCriteria(SystemId int(11),ParentId varchar(150),CMMContainer varchar(150),Id varchar(150),RowId bigInt(20) NOT NULL AUTO_INCREMENT PRIMARY KEY);";
			data.getDelegate().prepareAndExecStmt(data, sql);
		} catch (SQLException sqle) {
			Log.error("Table XactMatchCriteria already exists");
		}
	}
	public boolean columnExists(JDBCData data, String tableName, String columnName) {
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT " + columnName+" FROM ")
		   .append(data.getDelegate().tableName(data, tableName))
		   .append(" where 0=1");

		try {
			//data.getDelegate().prepareAndExecStmt(data, sql.toString());
			rs = data.doSQL(sql.toString());
			return true;
		} catch (SQLException sqle) {
			return false;
		}
	}



		public void addColumn(JDBCData data, String tableName, String columnName) throws SQLException{

			String sql = null;
				sql =
					"ALTER TABLE " + data.getDelegate().tableName(data, tableName) + " " +
					"ADD (" + columnName + " VARCHAR(255) NULL) ";

			data.getDelegate().prepareAndExecStmt(data, sql);

		}
		
		public void addIntegerColumn(JDBCData data, String tableName, String columnName) throws SQLException{

			String sql = null;
				sql =
					"ALTER TABLE " + data.getDelegate().tableName(data, tableName) + " " +
					"ADD (" + columnName + " INT(11) NULL) ";

			data.getDelegate().prepareAndExecStmt(data, sql);

		}
		
		public void addDateColumn(JDBCData data, String tableName, String columnName) throws SQLException{

			String sql = null;
				sql =
					"ALTER TABLE " + data.getDelegate().tableName(data, tableName) + " " +
					"ADD (" + columnName + " DATE NULL) ";

			data.getDelegate().prepareAndExecStmt(data, sql);

		}
		
		public void addMultipleColumns(JDBCData data, String sql) throws SQLException{
			data.getDelegate().prepareAndExecStmt(data, sql);
		}


		public void addTimeStampColumn(JDBCData data, String tableName, String columnName) throws SQLException{

			String sql = null;
				sql =
					"ALTER TABLE " + data.getDelegate().tableName(data, tableName) + " " +
					"ADD (" + columnName + " timestamp NULL) ";

			data.getDelegate().prepareAndExecStmt(data, sql);

		}
		public void addDateTimeColumn(JDBCData data, String tableName, String columnName) throws SQLException{

			String sql = null;
				sql =
					"ALTER TABLE " + data.getDelegate().tableName(data, tableName) + " " +
					"ADD (" + columnName + " DATETIME NULL) ";

			data.getDelegate().prepareAndExecStmt(data, sql);

		}
		


}
