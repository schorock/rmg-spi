/*
 * PolicyTransactionValidate.java
 *
 */

package com.ric.uw.common.transaction.handler;

import java.util.ArrayList;

import com.iscs.common.admin.user.User;
import com.iscs.common.render.DateRenderer;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.shared.SystemBookDt;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.uw.common.transaction.Transaction;
import com.iscs.uw.policy.PM;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

/** Validates the Current Policy Transaction
 *
 * @author  moniquef
 */
public class PolicyTransactionValidate extends InnovationIBIZHandler {
    
    /** Creates a new instance of PolicyTransactionValidate
     * @throws Exception never
     */
    public PolicyTransactionValidate() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a Greeting
            Log.debug("Processing PolicyTransactionValidate...");
            ModelBean rs = this.getHandlerData().getResponse();
            JDBCData data = this.getHandlerData().getConnection();
            ModelBean responseParams = rs.getBean("ResponseParams");
            ModelBean cmm = responseParams.getBean("CMMParams");
            StringDate todayDt = DateTools.getStringDate(responseParams);
            ModelBean user = responseParams.getBean("UserInfo");
            
            // Add Policy ModelBean to the Response
            ModelBean policy;
            if (rs.gets("Policy") != null){
            	policy = rs.getBean("Policy");
            } else {
                String systemId = cmm.gets("SystemId");
                policy = new ModelBean("Policy");
                data.selectModelBean(policy, Integer.parseInt(systemId) );
                rs.setValue(policy);
            }
            
            // Get TransactionHistory ModelBean
            ModelBean transactionHistory = rs.getBean("TransactionHistory");
            String transactionCd = transactionHistory.gets("TransactionCd");
            
            // Determine Non-Renewal/Non-Renewal Request
            if( transactionCd.equals(PM.TX_NON_RENEWAL) ) {
               
                ArrayList<ModelBean> errors = new ArrayList<ModelBean>();
            	Transaction.validateFieldsNonRenewal(policy, transactionHistory, errors, todayDt);
            	
            	if(errors.size() > 0 && !errors.get(0).gets("Name").equals("ReasonCd")) {            		
            		transactionCd = PM.TX_NON_RENEWAL_REQUEST;   
            		transactionHistory.setValue("TransactionCd", transactionCd);
                    transactionHistory.setValue("TransactionShortDescription", "Non-Renewal Requested on " + PM.getTransactionEffectiveDt(policy, transactionHistory).toStringDisplay());
                     
            	}
            }
            
            // Do not validate Certain Transactions
            if( transactionCd.equals(PM.TX_AUDIT_FINALIZE) || transactionCd.equals(PM.TX_COMMISSION_REVERSAL) || transactionCd.equals(PM.TX_BILLING_ENTITY_CHANGE) || transactionCd.equals(PM.TX_LAPSE_START) || transactionCd.equals(PM.TX_LAPSE_END) )
            	return rs;
            
            // Set Posting Date
            transactionHistory.setValue("BookDt", SystemBookDt.getBookDt(data).toString());
            
            // Get Prior Associated TransactionHistory ModelBean
            ModelBean transactionBean = Transaction.getPriorTransaction(policy, transactionHistory.getString("TransactionCd"));
            if( transactionBean != null )
                rs.addValue(transactionBean);
            
            // Validate transaction rules
            ArrayList<ModelBean> errors = new ArrayList<ModelBean>();
            ModelBean[] transactions = rs.getBeans("TransactionHistory");
            Transaction.validateTransactionCd(data, policy, transactionHistory, user, todayDt, errors);
            Transaction.validate(data, policy, transactions, user, todayDt, errors);
            validateAdditionalAuthority(policy, transactions, user, todayDt, errors);
                        
            // Loop Through Validation Errors and Add Them to the Response
            for( int i = 0; i < errors.size(); i++ ) {
                ModelBean errorBean = (ModelBean) errors.get(i);
                addErrorMsg(errorBean.gets("Name"), errorBean.gets("Msg"), errorBean.gets("Type") );
            }
                                    
            // If the Response Has Errors
            if( hasErrors() ) {
                throw new IBIZException();
            }
            
            // Return the Response ModelBean
            return rs;
        }
        catch ( IBIZException ibe ) {
            throw ibe;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
    
    /**
     * Validate Additional Authority RMGUAT-363
     * @param policy
     * @param transactions
     * @param user
     * @param todayDt
     * @param errors
     * @throws Exception
     */
    private void validateAdditionalAuthority( ModelBean policy, ModelBean[] transactions, ModelBean user, StringDate todayDt, ArrayList<ModelBean> errors ) 
    		throws Exception {
    	try {
            Log.debug("Validating additional authority...");
            ModelBean transactionHistory = transactions[0];
            String transactionCd = transactionHistory.getString("TransactionCd");
            
            if( transactionCd.equals( PM.TX_REINSTATEMENT ) ) {
            	ModelBean cancellationTransactionHistory = null;
            	
            	for( int i = 0; i < transactions.length; i++ ) {
            		if( transactions[i].valueEquals("TransactionCd", PM.TX_CANCELLATION) ) {
            			cancellationTransactionHistory = transactions[i];
            			break;
            		}
            	}
            	
            	ModelBean[] losses = policy.findBeansByFieldValue("LossHistory", "StatusCd", "Active");
            	Integer numberOfPriorPaidLoss = 0;
            	StringDate fiveYrsAgoToday = StringDate.advanceYear( new StringDate(), -5 );
           
            	// Cancellation date >= 30 days prior to the reinstatement request 
            	if( cancellationTransactionHistory != null ) {
            		StringDate cancellationEffectiveDt = cancellationTransactionHistory.getDate("TransactionEffectiveDt");
                	StringDate reinstatementThresholdDt = DateRenderer.advanceDate(cancellationEffectiveDt, "30");
            		
            		if( !User.hasAuthority(user, "AllowReinstatement30DaysAfterCancellation", todayDt) && DateRenderer.greaterThanEqual(todayDt, reinstatementThresholdDt) ) {
                		ErrorTools.addError("General", "You are not allowed to create a reinstatement transaction 30 or more days after cancellation date", ErrorTools.AUTHORITY_ERROR, errors);
                	}
            	}
            	
            	//>= 2 paid losses in last 5 years
            	for( ModelBean loss : losses ) {
            		StringDate lossDt = loss.getDate("LossDt");
            		
    				if( !StringRenderer.isTrue(loss.gets("IgnoreInd")) && StringRenderer.greaterThan(loss.gets("PaidAmt"), "0") && DateRenderer.greaterThanEqual(lossDt, fiveYrsAgoToday) )
    					numberOfPriorPaidLoss++;
    			}

    			if( !User.hasAuthority(user, "AllowReinstatementWithPaidLossesWithin5Yrs", todayDt) && numberOfPriorPaidLoss >= 2 ){
    				ErrorTools.addError("General", "You are not allowed to create a reinstatement transaction when there are two or more paid losses in the last 5 years", ErrorTools.AUTHORITY_ERROR, errors);
    			}
            	
            	//If "No Reinstatements" selection from special options is selected "Yes"
            	if( !User.hasAuthority(user, "AllowReinstatementWithNoReinstatementFlag", todayDt) && policy.getBean("BasicPolicy").gets("NoReinstatements").equals("Yes") ) {
            		ErrorTools.addError("General", "You are not allowed to create a reinstatement transaction for this policy", ErrorTools.AUTHORITY_ERROR, errors);
            	}
            }
            
        } catch (Exception e) {
            Log.error(e);
            throw e;
        }
    }
    
}