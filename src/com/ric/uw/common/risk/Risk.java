/*
 * Risk.java
 *
 */

package com.ric.uw.common.risk;

import net.inov.tec.beans.ModelBean;


public class Risk extends com.iscs.uw.common.risk.Risk {

    /** Set the default address of a risk to match the location address referenced by this risk.  Should be called
     * on creation of a new Risk, or when attaching to a new location.
     * @param container ModelBean holding the container of the Risk
     * @param risk ModelBean holding the Risk being added
     * @throws Exception when an unexpected error occurs
     */
    public static void setDefaultAddress(ModelBean container, ModelBean risk)
    throws Exception {
    	
        // Get Location
        ModelBean location = container.getBeanById(risk.gets("LocationRef"));
        if( location != null ) {
        	
        	// Get Building
        	ModelBean building = risk.getBean("Building");
        	
	        ModelBean riskAddr = building.getBean("Addr");
	        riskAddr.setValue("AddrTypeCd", "RiskAddr");
	        
	        ModelBean locationLookupAddr = location.getBean("Addr", "AddrTypeCd", "LocationLookupAddr");
	        if( locationLookupAddr != null ) {
	        	ModelBean riskLookupAddr = building.getBean("Addr", "AddrTypeCd", "RiskLookupAddr");
	        	if( riskLookupAddr == null ) {
	        		riskLookupAddr = new ModelBean("Addr");
	        		building.addValue(riskLookupAddr);
	        	}
	        	riskLookupAddr.setValue("AddrTypeCd", "RiskLookupAddr");
	        }
        }            
    }
}
