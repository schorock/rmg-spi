package com.ric.uw.common.risk.handler;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.web.cmm.AdditionalParams;

import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.uw.common.risk.Risk;
import com.iscs.uw.policy.Policy;

/** Copies an existing Risk
 *
 * @author Julie and Marcus
 */
public class RiskCopy extends InnovationIBIZHandler {
    
    /** Creates a new instance of RiskCopy
     * @throws Exception never
     */
    public RiskCopy() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a Greeting
            Log.debug("Processing RiskCopy...");
            ModelBean rs = this.getHandlerData().getResponse();
            ModelBean responseParams = rs.getBean("ResponseParams");
            ModelBean cmm = responseParams.getBean("CMMParams");
            AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest());
            
            // Get the Container ModelBean
            ModelBean container = rs.getBean(cmm.gets("Container"));
                        
            // Get Line ModelBean
            ModelBean line = rs.getBeanById(cmm.gets("ParentIdRef"));        
              
            // Get the Original Risk (Copy From)
            ModelBean origRisk = null;
            String riskIdRef = ap.gets("RiskIdRef");
            if( !riskIdRef.equals("") ) {
            	origRisk = container.getBeanById(riskIdRef);
            } else {
            	origRisk = container.getBeanById(cmm.gets("IdRef"));
            }
            
            // Copy Risk
            ModelBean newRisk = Risk.copyRisk(container, line, origRisk);
        	if (Policy.isWorkersCompensation(container)) {
                String riskIndex = Risk.nextIndex(container, line, newRisk);
                Risk.setRiskIndex(newRisk.getBean("GLClass"), riskIndex);
        	}
        	
        	// Clear Building Field in Risk
        	ModelBean building = newRisk.getBean("Building");
        	if( building != null && building.hasBeanField("MeetFortifiedMapReq") ) {
        		building.setValue("MeetFortifiedMapReq", "");
        	}
        	
            // Add the New Risk to the Response
            line.addValue(newRisk);
            
            // Update CMM Params with New Risk Information
            cmm.setValue("Operation", "New");
            String newRiskId = newRisk.getId();
            cmm.setValue("IdRef", newRiskId);
            
            // Force Dirty Field Indicator
            ModelBean additionalParams = responseParams.getBean("AdditionalParams");
            ModelBean param = additionalParams.getBean("Param", "Name", "ForceDirtyFieldInd");
            param.setValue("Value", "Yes");
            
            addErrorMsg("General", "This new copy of Risk '" + origRisk.gets("Description") + "' has not been saved and may be edited.", ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_INFO);
            
            // Return the Response ModelBean
            return rs;
            
        }
        catch( IBIZException e ) {
            throw new IBIZException();
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
}
