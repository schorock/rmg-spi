package com.ric.uw.common.shared.render;

import java.util.ArrayList;

import net.inov.mda.MDAOption;
import net.inov.tec.beans.ModelBean;

import com.iscs.common.render.Renderer;
import com.iscs.common.tech.form.field.FormFieldRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.uw.common.risk.Risk;

public class RICLossHistoryRenderer implements Renderer{
	
	/** Creates a new instance of LossHistoryRenderer */
    public RICLossHistoryRenderer() {
    }
    
    /** Context Variable Getter
      * @return LossHistoryRenderer
      */     
    public String getContextVariableName() {
         return "RICLossHistoryRenderer";
    }
    

    /** Get Property Select List
     * @param fieldId Field Identifier
     * @param defaultValue Select List Default Value
     * @param container ModelBean Containing the Risks
     * @param meta Parameters
     * @return String HTML Select Tag
     * @throws Exception if an Unexpected Error Occurs
     */
    public static String propertySelectField(String fieldId, String defaultValue, ModelBean container, String meta)
    throws Exception {
        try {
        	ArrayList<MDAOption> array = new ArrayList<MDAOption>();
            ModelBean[] riskArray = container.findBeansByFieldValue("Risk","Status","Active"); 
            for( ModelBean risk : riskArray ) {
            	String riskBeanName = Risk.getRiskBeanName(container, risk.getParentBean());
            	if( riskBeanName.equals("Building") ) {
            		ModelBean riskAddr = risk.getBean("Building").getBean("Addr", "AddrTypeCd", "RiskAddr");
            		String addr =  riskAddr.gets("Addr1") + riskAddr.gets("Addr2") + " " + riskAddr.gets("City") + " " +  riskAddr.gets("StateProvCd") + " " +  riskAddr.gets("PostalCode"); 
	        		MDAOption option = new MDAOption();
	        		option.setValue(risk.getId());
	        		option.setLabel(addr);
	        		array.add(option);
            	}
            }
            
            MDAOption[] options = array.toArray(new MDAOption[array.size()]);
            return FormFieldRenderer.renderSelect(fieldId, defaultValue, options, "SelectList", meta);
        }
        catch( Exception e ) {
            e.printStackTrace();
            Log.error(e);
            return "";
        }
    }
    
    

    /** Get Property Select List including Deleted Risks
     * @param fieldId Field Identifier
     * @param defaultValue Select List Default Value
     * @param container ModelBean Containing the Risks
     * @param meta Parameters
     * @return String HTML Select Tag
     * @throws Exception if an Unexpected Error Occurs
     */
    public static String propertySelectAllRisksField(String fieldId, String defaultValue, ModelBean container, String meta)
    throws Exception {
        try {
        	ArrayList<MDAOption> array = new ArrayList<MDAOption>();
            ModelBean[] riskArray = container.getAllBeans("Risk");
            for( ModelBean risk : riskArray ) {
            	String riskBeanName = Risk.getRiskBeanName(container, risk.getParentBean());
            	if( riskBeanName.equals("Building") ) {
            		ModelBean riskAddr = risk.getBean("Building").getBean("Addr", "AddrTypeCd", "RiskAddr");
            		String addr =  riskAddr.gets("Addr1") + riskAddr.gets("Addr2") + " " + riskAddr.gets("City") + " " +  riskAddr.gets("StateProvCd") + " " +  riskAddr.gets("PostalCode"); 
            		MDAOption option = new MDAOption();
	        		option.setValue(risk.getId());
	        		option.setLabel(addr);
	        		array.add(option);
            	}
            }
            
            MDAOption[] options = array.toArray(new MDAOption[array.size()]);
            return FormFieldRenderer.renderSelect(fieldId, defaultValue, options, "SelectList", meta);
        }
        catch( Exception e ) {
            e.printStackTrace();
            Log.error(e);
            return "";
        }
    }
    
    /** Update Loss Histories that are associated with a Deleted Risk 
     * @param application
     * @param deletedRisks
     * @throws Exception if an Unexpected Error Occurs
     */
    public static void UpdateDeletedRiskLossHistories(ModelBean application, ModelBean [] deletedRisks)
    throws Exception {

		for(ModelBean deletedRisk : deletedRisks ) {
	    	String riskId = deletedRisk.gets("id"); 
	    	ModelBean[] lossHistories = application.findBeansByFieldValue("LossHistory" , "StatusCd", "Active"); 
        	for( ModelBean lossHistory : lossHistories ) {
        		String lossRiskId = lossHistory.gets("RiskIdRef");
        		if(riskId.equals(lossRiskId) ){
        			lossHistory.setValue("IgnoreInd", "Yes");
        			
        			//Note if the following comment needs to be modified, please update the following files accordingly
        			//loss-history-detail-button.html
        			//loss-history-property-detail.html
        			lossHistory.setValue("Comment", "This Loss History has been set to Disputed because it is associated to a risk that is no longer active");
        		}
        	}
		}
    }
    
}
