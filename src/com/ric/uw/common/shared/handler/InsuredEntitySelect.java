/*
 * InsuredEntitySelect.java
 *
 */

package com.ric.uw.common.shared.handler;

import java.util.ArrayList;
import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import com.iscs.common.tech.log.Log;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.common.utility.validation.render.ValidationRenderer;
import com.iscs.uw.common.shared.Insured;

public class InsuredEntitySelect extends InnovationIBIZHandler {
    // Create an object for logging messages
    
    
    /** Creates a new instance of InsuredEntitySelect
     * @throws Exception never
     */
    public InsuredEntitySelect() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    
    
    
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a greeting
            Log.debug("Processing...");
            ModelBean rs = this.getHandlerData().getResponse();

            // Fetch the insured
            ModelBean insured = rs.getBean("Application").getBean("Insured");

         // Get the previous entity type
            String previousEntityType = null;
            ModelBean xfdf = rs.getBean("ResponseParams").getBean("XFDF");
            if(xfdf != null){
            	ModelBean param = xfdf.findBeanByFieldValue("Param", "Name", "PreviousEntityType");
            	if(param != null){
            		previousEntityType = param.gets("Value");
            	}
            }
            
            // If the previous entity type isn't null, then check the current entity type
            if(previousEntityType != null && !previousEntityType.equals("")){     	
            	String currentEntityType = insured.gets("EntityTypeCd");
            	
            	// If the current entity type and the previous entity type are different, then do some field copying
            	if(!currentEntityType.equalsIgnoreCase(previousEntityType)){
            		
            		Insured.switchEntityType(insured, previousEntityType);
            		ValidationRenderer.clearValidationErrors(rs.getBean("Application"),"Validation"); 
            	}
            	
            }

            // Return the response bean
            return rs;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
    
}

