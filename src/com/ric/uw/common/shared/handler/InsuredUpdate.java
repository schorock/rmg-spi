/*
 * InsuredUpdate.java
 *
 */

package com.ric.uw.common.shared.handler;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;

import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.uw.common.risk.Location;
import com.ric.uw.common.risk.Risk;

/** Insured Update
 *  
 * @author  moniquef
 */
public class InsuredUpdate extends InnovationIBIZHandler {
    
    /** Creates a new instance of InsuredUpdate
     * @throws Exception never
     */
    public InsuredUpdate() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a greeting
            Log.debug("Processing InsuredUpdate...");
            ModelBean rs = this.getHandlerData().getResponse();
            
            // Get the Application ModelBean
            ModelBean application = rs.getBean("Application");
            
            // Get Insured Lookup Address
            ModelBean insuredLookupAddr = application.getBeanByAlias("InsuredLookupAddr");
            
            // Check if Locations Need to be Updated With Insured Address
            ModelBean[] locationArray = application.getAllBeans("Location");
            for( ModelBean location : locationArray ) {
            	
            	// Check if Location Addresses are Blank
            	ModelBean locationLookupAddr = location.getBean("Addr", "AddrTypeCd", "LocationLookupAddr");
            	ModelBean locationAddr = location.getBean("Addr", "AddrTypeCd", "LocationAddr");
            	if( locationLookupAddr == null || locationAddr == null )
            		continue;
            	
            	if( BeanTools.isBlank(locationLookupAddr) && BeanTools.isBlank(locationAddr) ) {
            		Location.setDefaultAddress(application, location);
            	} else if( BeanTools.isBlank(locationLookupAddr, new String[] {"PostalCode"}) ) {
            		
            		// If Postal Codes Match Set Default Address
            		if( hasMatchingPostalCode(insuredLookupAddr, locationLookupAddr) ) 
            			Location.setDefaultAddress(application, location);
            		
            	} else if( BeanTools.isBlank(locationAddr, new String[] {"PostalCode"}) ) {
            		
            		// If Postal Codes Match Set Default Address
            		if( hasMatchingPostalCode(insuredLookupAddr, locationAddr) ) 
            			Location.setDefaultAddress(application, location);
            	}
            	
            }

            // Check if Risks Need to be Updated With Insured Address
            ModelBean[] riskArray = application.getAllBeans("Risk");
            for( ModelBean risk : riskArray ) {
            	
            	// Check if Risk Addresses are Blank
            	ModelBean riskLookupAddr = risk.getBean("Addr", "AddrTypeCd", "RiskLookupAddr");
            	ModelBean riskAddr = risk.getBean("Addr", "AddrTypeCd", "RiskAddr");
            	if( riskLookupAddr == null || riskAddr == null )
            		continue;
            	
            	if( BeanTools.isBlank(riskLookupAddr) && BeanTools.isBlank(riskAddr) ) {
            		Risk.setDefaultAddress(application, risk);
            	} else if( BeanTools.isBlank(riskLookupAddr, new String[] {"PostalCode"}) ) {
            		
            		// If Postal Codes Match Set Default Address
            		if( hasMatchingPostalCode(insuredLookupAddr, riskLookupAddr) ) {
            			Risk.setDefaultAddress(application, risk);
            		}
            	} else if( BeanTools.isBlank(riskAddr, new String[] {"PostalCode"}) ) {
            		
            		// If Postal Codes Match Set Default Address
            		if( hasMatchingPostalCode(insuredLookupAddr, riskAddr) ) 
            			Risk.setDefaultAddress(application, risk);
            	}
            }
            
            // If Response has Errors, Throw IBIZException
            if( hasErrors() )
                throw new IBIZException();
            
            // Return the response bean
            return rs;
        }
        catch( IBIZException e ) {
            throw new IBIZException();
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
    
    /** Check if Addresses Have Matching Postal Codes
     * @param addr1 Address 1
     * @param addr2 Address 2
     * @return true/false 
     * @throws Exception if an Unexpected Error Occurs
     */
    public static boolean hasMatchingPostalCode(ModelBean addr1, ModelBean addr2)
    throws Exception {

    	if( addr1 == null || addr2 == null ) 
    		return false;
    	
    	// Get Postal Codes
    	String postalCode1 = addr1.gets("PostalCode");
		String postalCode2 = addr2.gets("PostalCode");
			
		// Trim Postal Codes
		if( postalCode1.length() >= 5 ) 
			postalCode1 = postalCode1.substring(0, 5);	
		if( postalCode2.length() >= 5 ) 
			postalCode2 = postalCode2.substring(0, 5);

		if( postalCode1.equals(postalCode2) && postalCode1.length() > 0 )
			return true;
		else
			return false;
    }
}