/*
 * InsuredNew.java
 *
 */
package com.ric.uw.common.shared.handler;

import net.inov.biz.server.*;
import net.inov.tec.beans.*;
import net.inov.tec.data.JDBCData;
import net.inov.tec.web.cmm.AdditionalParams;

import com.iscs.common.tech.log.Log;
import com.iscs.common.tech.biz.*;

import com.ric.uw.common.shared.Insured;

/** Default Customer Values into New Insured ModelBean
 *
 * @author  moniquef
 */
public class InsuredNew extends InnovationIBIZHandler {
    
    /** Creates a new instance of InsuredNew
     * @throws Exception never
     */
    public InsuredNew() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a greeting
            Log.debug("Processing InsuredNew...");
            ModelBean rs = this.getHandlerData().getResponse();
            JDBCData data = this.getHandlerData().getConnection();
            AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest() );
                        
            // Get Customer ModelBean
            String customerSystemId = ap.gets("CustomerSystemId");
            if( !customerSystemId.equals("") ) {
	            ModelBean customer = new ModelBean("Customer");
	            data.selectModelBean(customer, Integer.parseInt(customerSystemId) );
	            
	            // Get Insured ModelBean
	            ModelBean application = rs.getBean("Application");
	            ModelBean insured = application.getBean("Insured");
	            
	            // Copy Customer ModelBean Field Values to the Insured ModelBean Fields
	            Insured.copyCustomerFieldValues(customer, insured);
            }
            
            // Return the response bean
            return rs;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
    
}

