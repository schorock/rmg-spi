/*
 * AIValidate.java
 *
 */

package com.ric.uw.common.shared.handler;

import java.util.ArrayList;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;

import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.uw.common.shared.AI;

    /** Validate AI Field Data
    *
    * @author  moniquef
    */
   public class AIValidate extends InnovationIBIZHandler {
   	// Create an object for logging messages


   	/** Creates a new instance of AIValidate
   	 * @throws Exception never
   	 */
   	public AIValidate() throws Exception {
   	}

   	/** Processes a generic service request.
   	 * @return the current response bean
   	 * @throws IBIZException when an error requiring user attention occurs
   	 * @throws ServiceHandlerException when a critical error occurs
   	 */
   	public ModelBean process() throws IBIZException, ServiceHandlerException {
   		try {
   			// Log a greeting
   			Log.debug("Processing AIValidate...");
   			ModelBean rs = this.getHandlerData().getResponse();
   			JDBCData data = this.getHandlerData().getConnection();
   			ModelBean cmm = rs.getBean("ResponseParams").getBean("CMMParams");

   			// Get the Application ModelBean
   			ModelBean application = rs.getBean("Application");
   			ModelBean basicPolicy = application.getBean("BasicPolicy");

   			// Get the AI ModelBean
   			ModelBean ai = rs.getBeanById(cmm.gets("IdRef"));

   			// Check for Form Required Field
   			ModelBean[] productFormRef = AI.getProductFormRefs(basicPolicy.gets("ProductVersionIdRef"), ai.gets("InterestTypeCd"));
   			for( int i = 0; i < productFormRef.length; i++ ) {
   				if( StringTools.isTrue(productFormRef[i].gets("RequiredInd")) ) {
   					if( ai.valueEquals("InterestFormCd", "") ) {
   						//RMGPROD-232. Add check so that error does not trigger when AI Interest Type is Additional Named Insured since RIC does not want this field available on the UI
   						if( !ai.valueEquals("InterestTypeCd", "Additional Named Insured") ) {
   							addErrorMsg("AI.InterestFormCd", "Forms Required", ErrorTools.MISSING_FIELD_ERROR);
   							break;
   						}
   					}    
   				}	
   			}

   			// Validate Field Values
   			ArrayList<ModelBean> errors = new ArrayList<ModelBean>();
   			AI.validate(data, ai, errors);

   			validateNoDuplicateAI(application, ai, null);

   			// Loop Through Validation Errors and Add Them to the Response
   			for( int i = 0; i < errors.size(); i++ ) {
   				ModelBean errorBean = (ModelBean) errors.get(i);
   				addErrorMsg(errorBean.gets("Name"), errorBean.gets("Msg"), errorBean.gets("Type"));
   			}

   			// If Response has Errors, Throw IBIZException
   			if( hasErrors() )
   				throw new IBIZException();

   			// Return the Response ModelBean
   			return rs;
   		}
   		catch( IBIZException e ) {
   			throw new IBIZException();
   		}
   		catch( Exception e ) {
   			throw new ServiceHandlerException(e);
   		}
   	}

   	/**Validate current AI is not a duplicate of any other AI
   	 * 
   	 * @param application ModelBean
   	 * @param ai ModelBean
   	 * @throws Exception
   	 */

   	public void validateNoDuplicateAI(ModelBean application, ModelBean ai, ArrayList<ModelBean> errors) throws Exception{
   		// Make sure this is not a duplicate of any other AI
   		ModelBean[] ais = application.findBeansByFieldValue("AI", "Status", "Active");
   		for( int i=0; i<ais.length; i++ ) {
   			if( !ais[i].gets("SequenceNumber").equals(ai.gets("SequenceNumber"))) {   

   				boolean changeFound = false;

   				// Test the AI bean
   				String[] newFields = ai.getFields();
   				String[] oldFields = ais[i].getFields();
   				if( newFields.length != oldFields.length ) {            			
   					continue;
   				}

   				for( int fieldCnt=0; fieldCnt<newFields.length; fieldCnt++ ) {            			
   					if( !newFields[fieldCnt].equals("id") && !newFields[fieldCnt].equals("SequenceNumber") && !newFields[fieldCnt].equals("Status")) {
   						String field = newFields[fieldCnt];
   						String newField = ai.gets(field);
   						String oldField = ais[i].gets(field);
   						if( !newField.equals(oldField) ) {
   							changeFound = true;
   							break;
   						}
   					}
   				}            		
   				if( changeFound )
   					continue;

   				// Test the PartyInfo bean
   				ModelBean newPartyInfo = ai.getBean("PartyInfo");
   				ModelBean oldPartyInfo = ais[i].getBean("PartyInfo");

   				newFields = newPartyInfo.getFields();
   				oldFields = oldPartyInfo.getFields();
   				if( newFields.length != oldFields.length ) {
   					continue;
   				}

   				for( int fieldCnt=0; fieldCnt<newFields.length; fieldCnt++ ) {
   					if( !newFields[fieldCnt].equals("id") ) {
   						String field = newFields[fieldCnt];
   						String newField = newPartyInfo.gets(field);
   						String oldField = oldPartyInfo.gets(field);
   						if( !newField.equals(oldField) ) {
   							changeFound = true;
   						}
   					}
   				}
   				if( changeFound )
   					continue;

   				// Test the PartyInfo sub beans
   				ModelBean[] newPartyInfoBeans = newPartyInfo.getBeans();
   				for( int beanCnt=0; beanCnt<newPartyInfoBeans.length; beanCnt++ ) {
   					String alias = newPartyInfoBeans[beanCnt].getAliasName();
   					ModelBean newPartyInfoBean = newPartyInfoBeans[beanCnt];
   					ModelBean oldPartyInfoBean = oldPartyInfo.getBeanByAlias(alias);

   					if( oldPartyInfoBean == null ) {
   						continue;
   					}

   					newFields = newPartyInfoBean.getFields();
   					oldFields = oldPartyInfoBean.getFields();
   					if( newFields.length != oldFields.length ) {
   						continue;
   					}

   					for( int fieldCnt=0; fieldCnt<newFields.length; fieldCnt++ ) {
   						if( !newFields[fieldCnt].equals("id") ) {
   							String field = newFields[fieldCnt];
   							String newField = newPartyInfoBean.gets(field);
   							String oldField = oldPartyInfoBean.gets(field);
   							if( !newField.equals(oldField) ) {
   								changeFound = true;
   								break;
   							}
   						}
   					}
   					if( changeFound )
   						break;                		
   				}
   				if( changeFound )
   					continue;

   				// Test the LinkReference beans
   				ModelBean[] newLinkReferences = ai.findBeansByFieldValue("LinkReference","Status","Active");
   				ModelBean[] oldLinkReferences = ais[i].findBeansByFieldValue("LinkReference","Status","Active");
   				if( newLinkReferences.length != oldLinkReferences.length ) { 
   					continue;
   				}

   				for( int beanCnt=0; beanCnt<newLinkReferences.length; beanCnt++ ) {
   					String idRef = newLinkReferences[beanCnt].gets("IdRef");
   					ModelBean oldLinkReference = ais[i].getBean("LinkReference", "IdRef", idRef);
   					if( oldLinkReference == null || !oldLinkReference.gets("Status").equals("Active") ) {
   						changeFound = true;
   						break;
   					}
   				}
   				if( !changeFound ) {                     
   					// Add error if no change has been detected
   					if(errors != null){
   						ErrorTools.addError("General", "Invalid - this AI is a duplicate of AI #" + ais[i].gets("SequenceNumber"),ErrorTools.GENERIC_BUSINESS_ERROR, errors);
   					}else{
   						addErrorMsg("General", "Invalid - this AI is a duplicate of AI #" + ais[i].gets("SequenceNumber"),ErrorTools.GENERIC_BUSINESS_ERROR);
   					}					
   					break;
   				}
   			}           	

   		}
   	}
   }
