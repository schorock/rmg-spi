package com.ric.uw.common.shared.handler;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.insurance.customer.Customer;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;

/** Copy Insured field values to Customer if necessary
 *
 * @author  beno
 */
public class InsuredToCustomer extends InnovationIBIZHandler {

    /** Creates a new instance of InsuredToCustomer
     * @throws Exception never
     */
    public InsuredToCustomer() throws Exception {
    }
    
    /** Copy Insured field values to Customer if necessary
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
        	
            // Log a Greeting
            Log.debug("Processing InsuredToCustomer...");
            ModelBean rs = this.getHandlerData().getResponse();
            JDBCData data = this.getHandlerData().getConnection();
                        
            // Update customer only from bound quote
            ModelBean application = rs.getBean("Application");
            if( application.gets("TypeCd").equals("Quote") )
            	return rs;
            
            // Get Customer
            String customerRef = application.gets("CustomerRef");
            if( customerRef.equals("") )
            	return rs;
            
			ModelBean customer = Customer.getCustomerBySystemId(data, Integer.parseInt(customerRef));

			// Get Insured
			ModelBean insured = application.getBean("Insured");
			
			// Update customer only if Entity Types match
			String customerEntityType = customer.gets("EntityTypeCd");
			String insuredEntityType = insured.gets("EntityTypeCd");
			if( !customerEntityType.equals(insuredEntityType) ) 
				return rs;
						
			// Copy Billing Address
			ModelBean customerBillingAddr = customer.getBeanByAlias("CustomerBillingAddr");
			if( customerBillingAddr.gets("Addr1").equals("") ) {
				BeanTools.copyNonAliasValues(BeanTools.getBeanWithAlias(insured,"InsuredBillingAddr"),BeanTools.getBeanWithAlias(customer,"CustomerBillingAddr"));
			}
            
			if( !customer.gets("PreferredDeliveryMethod").equals(insured.gets("PreferredDeliveryMethod")) )
				customer.setValue("PreferredDeliveryMethod",insured.gets("PreferredDeliveryMethod"));
			
			//Copy the Opt-Out of Marketing preference (RGUAT-167)
			customer.setValue("OptOutMarketing",insured.gets("OptOutMarketing"));
			
			BeanTools.copyNonAliasValuesIfBlank(insured.getBeanByAlias("InsuredPersonal"), customer.getBeanByAlias("CustomerPersonal"));
			BeanTools.copyNonAliasValuesIfBlank(insured.getBeanByAlias("InsuredEmail"), customer.getBeanByAlias("CustomerEmail"));
			BeanTools.copyNonAliasValuesIfBlank(insured.getBeanByAlias("InsuredTaxInfo"), customer.getBeanByAlias("CustomerTaxInfo"));
			BeanTools.copyNonAliasValuesIfBlank(insured.getBeanByAlias("InsuredPhonePrimary"), customer.getBeanByAlias("CustomerPhonePrimary"));
			BeanTools.copyNonAliasValuesIfBlank(insured.getBeanByAlias("InsuredPhoneSecondary"), customer.getBeanByAlias("CustomerPhoneSecondary"));
			BeanTools.copyNonAliasValuesIfBlank(insured.getBeanByAlias("InsuredFax"), customer.getBeanByAlias("CustomerFax"));
			 			
			BeanTools.saveBean(customer, data);
			
            return null;
        }
        catch( IBIZException e ) {
            throw e;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
    
}

