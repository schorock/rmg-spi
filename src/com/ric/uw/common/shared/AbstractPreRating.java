/*
 * 
 * PreRating.java
 */
package com.ric.uw.common.shared;

import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.data.JDBCData;

/**
 * Businessowners Policy PreRating example
 * 
 * Calculates number of risks that apply to certain coverages depending on rules specific to the coverage
 * 
 * @author Scott Ferguson
 */

public abstract class AbstractPreRating implements RuleProcessor {
	private static final Object DIRECTPORTAL_USER = "directportal";
	private static final String WEBPORTAL_USER = "webportal";
	private static final String API_USER = "api";


	@Override
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user, JDBCData data) throws RuleException {
		ModelBean errors = null;
		try{
			Log.debug("Processing PreRating...");

			// Build the Errors ModelBean
			errors = new ModelBean("Errors");	

			setRatingIndicator(bean, additionalBeans);
		}
		catch(Exception e){	
			throw new RuleException(e);
		}
		return errors;		
	}

	protected void setRatingIndicator(ModelBean application, ModelBean[] additionalBeans) throws Exception {
		if (isRatingRequired(application, additionalBeans)) {
			application.setValue("ReadyToRateInd", "Yes");
		} else {
			application.setValue("ReadyToRateInd", "No");
		}	
	}

	/**Decides whether the Rating to the called
	 * Check for Netrate Param in Additional Param
	 * @param application 
	 * @return boolean
	 */
	protected boolean isRatingRequired(ModelBean application, ModelBean[] additionalBeans) throws Exception {
		try{
			ModelBean response = additionalBeans[0];
			ModelBean responseParams = response.getBean("ResponseParams");
			ModelBean additionalParams = responseParams.getBean("AdditionalParams");
			ModelBean xfdf = responseParams.getBean("XFDF");

			boolean useNetRate = getUseNetRate(additionalParams, xfdf);
			String transactionSource = getTransactionSource(application);
			String transactionCd = getTransactionCd(application);
			String transactionName = getTransactionName(response, xfdf);
			String updateUser = getUpdateUser(application);
			boolean isAutoRenewal = transactionSource.equals("Innovation") && 
									transactionName.equals("UWExternalPolicyTransaction") && 
									transactionCd.equals("Renewal");

			// Do not rate certain transactions
			if (useNetRate || 
					!transactionSource.equals("Innovation") || 
					StringRenderer.in(updateUser, DIRECTPORTAL_USER+","+WEBPORTAL_USER+","+API_USER) || 
					StringRenderer.in(transactionName, "UWApplicationUpdateCloseout,UWRiskUpdateCloseout,UWPolicyReApply") ||
					isAutoRenewal)  
			{
				return true;
			} else {
				return false;
			}
		}
		catch( Exception e ) {
			Log.error(e.getMessage(), e);
			e.printStackTrace();
			return false;
		}
	}

	protected String getTransactionSource(ModelBean application) throws ModelBeanException {
		ModelBean transactionInfo = null;
		if (application.hasBeanField("DTOTransactionInfo")) {
			transactionInfo = application.getBean("DTOTransactionInfo");
		} else if (application.hasBeanField("TransactionInfo")) {
			transactionInfo = application.getBean("TransactionInfo");
		}

		String transactionSource = "Innovation";
		if (transactionInfo != null) {
			transactionSource = transactionInfo.gets("SourceCd");
			if (transactionSource == null) {
				transactionSource = "Innovation";
			}
		}
		return transactionSource;
	}
	
	protected String getTransactionCd(ModelBean application) throws ModelBeanException {
		ModelBean transactionInfo = null;
		if (application.hasBeanField("DTOTransactionInfo")) {
			transactionInfo = application.getBean("DTOTransactionInfo");
		} else if (application.hasBeanField("TransactionInfo")) {
			transactionInfo = application.getBean("TransactionInfo");
		}
		
		return transactionInfo.gets("TransactionCd");
	}
	
	protected String getUpdateUser(ModelBean application) throws ModelBeanException {
		ModelBean appBean = null;
		if (application.getBeanName().equals("Application") || application.getBeanName().equals("DTOApplication")) {
			appBean = application;
		} else if (application.hasBeanField("Application")) {
			appBean = application.getBean("Application");
		} else if (application.hasBeanField("DTOApplication")) {
			appBean = application.getBean("DTOApplication");
		}

		String updateUser = "";
		if (appBean != null) {
			updateUser = appBean.gets("UpdateUser");
			if (updateUser == null) {
				updateUser = "";
			}
		}
		return updateUser;
	}
	
	protected String getTransactionName(ModelBean request, ModelBean xfdf) throws ModelBeanException {
		String transactionName = "";
		String transactionBeanName = request.getBeanName();
		if (transactionBeanName.toUpperCase().endsWith("RS") || transactionBeanName.toUpperCase().endsWith("RQ")) {
			transactionName = transactionBeanName.substring(0, transactionBeanName.length() - 2);
		} else {
			ModelBean rq = xfdf.findBeanByFieldValue("Param", "Name", "rq");
			if (rq != null) {
				transactionName = rq.getValue("Value").toString();
			} 			
		}
		
		return transactionName;
	}

	protected boolean getUseNetRate(ModelBean additionalParams, ModelBean xfdf) throws ModelBeanException {
		boolean useNetRate = false;
		ModelBean netrateParam = additionalParams.getBean("Param", "Name", "UseNetRate");
		if(netrateParam == null || netrateParam.equals("")){
			netrateParam = xfdf.getBean("Param", "Name", "UseNetRate");
		}

		// Check Netrate flag in Additional Param
		if(netrateParam != null) {
			String netrateParamVal = netrateParam.getValue("Value").toString();
			if(netrateParamVal!= "") {
				useNetRate = StringTools.isTrue(netrateParam.getValue("Value").toString());        	   
			}
		} 

		return useNetRate;
	}
}