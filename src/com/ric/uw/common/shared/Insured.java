package com.ric.uw.common.shared;

import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.bean.BeanTools;

import net.inov.tec.beans.ModelBean;

public class Insured extends com.iscs.uw.common.shared.Insured {
	
	/** Copy Customer Field Values to the Insured
     * @param customer ModelBean
     * @param insured ModelBean
     * @throws Exception if an Error Occurs
     */    
    public static void copyCustomerFieldValues(ModelBean customer, ModelBean insured) throws Exception {
        try {
        	insured.setValue("IndexName", customer.gets("IndexName"));
            insured.setValue("EntityTypeCd", customer.gets("EntityTypeCd"));
                        
            BeanTools.copyNonAliasValues(BeanTools.getBeanWithAlias(customer,"CustomerName"), BeanTools.getBeanWithAlias(insured,"InsuredName"));
            BeanTools.copyNonAliasValues(BeanTools.getBeanWithAlias(customer,"CustomerTaxInfo"), BeanTools.getBeanWithAlias(insured,"InsuredTaxInfo"));
            BeanTools.copyNonAliasValues(BeanTools.getBeanWithAlias(customer,"CustomerPersonal"), BeanTools.getBeanWithAlias(insured,"InsuredPersonal"));
            BeanTools.copyNonAliasValues(BeanTools.getBeanWithAlias(customer,"CustomerEmail"), BeanTools.getBeanWithAlias(insured,"InsuredEmail"));
            BeanTools.copyNonAliasValues(BeanTools.getBeanWithAlias(customer,"CustomerPhonePrimary"), BeanTools.getBeanWithAlias(insured,"InsuredPhonePrimary"));
            BeanTools.copyNonAliasValues(BeanTools.getBeanWithAlias(customer,"CustomerPhoneSecondary"), BeanTools.getBeanWithAlias(insured,"InsuredPhoneSecondary"));
            BeanTools.copyNonAliasValues(BeanTools.getBeanWithAlias(customer,"CustomerFax"), BeanTools.getBeanWithAlias(insured,"InsuredFax"));
            BeanTools.copyNonAliasValues(BeanTools.getBeanWithAlias(customer,"CustomerMailingAddr"), BeanTools.getBeanWithAlias(insured,"InsuredMailingAddr"));
            BeanTools.copyNonAliasValues(BeanTools.getBeanWithAlias(customer,"CustomerBillingAddr"), BeanTools.getBeanWithAlias(insured,"InsuredBillingAddr"));
            BeanTools.copyNonAliasValues(BeanTools.getBeanWithAlias(customer,"CustomerLookupAddr"), BeanTools.getBeanWithAlias(insured,"InsuredLookupAddr"));
            
            if( insured.valueEquals("EntityTypeCd", "Joint") ) {
                BeanTools.copyNonAliasValues(BeanTools.getBeanWithAlias(customer,"CustomerNameJoint"), BeanTools.getBeanWithAlias(insured,"InsuredNameJoint"));
                BeanTools.copyNonAliasValues(BeanTools.getBeanWithAlias(customer,"CustomerTaxInfoJoint"), BeanTools.getBeanWithAlias(insured,"InsuredTaxInfoJoint"));
                BeanTools.copyNonAliasValues(BeanTools.getBeanWithAlias(customer,"CustomerPersonalJoint"), BeanTools.getBeanWithAlias(insured,"InsuredPersonalJoint"));
            } else if( insured.valueEquals("EntityTypeCd", "Trust") ) {
                BeanTools.copyNonAliasValues(BeanTools.getBeanWithAlias(customer,"CustomerTaxInfoTrust"), BeanTools.getBeanWithAlias(insured,"InsuredTaxInfoTrust"));
            } else if( insured.valueEquals("EntityTypeCd", "Estate") ) {
                BeanTools.copyNonAliasValues(BeanTools.getBeanWithAlias(customer,"CustomerTaxInfoEstate"), BeanTools.getBeanWithAlias(insured,"InsuredTaxInfoEstate"));
            }

            // Copy the Ouput Delivery Method preference
            insured.setValue("PreferredDeliveryMethod", customer.gets("PreferredDeliveryMethod"));
            
            //Copy the Opt-Out of Marketing preference (RGUAT-167)
            insured.setValue("OptOutMarketing", customer.gets("OptOutMarketing"));
                        
        } catch( Exception e ) {
            e.printStackTrace();
            Log.error(e);
            throw e;
        }
    }
}
