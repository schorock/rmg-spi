package com.ric.uw.common.commission;

import java.util.ArrayList;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.math.Money;

import com.iscs.common.business.provider.Provider;
import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.tech.bean.data.SQLRelationDelegate;
import com.iscs.common.tech.bean.data.SQLRelationHelper;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.insurance.product.ProductException;
import com.iscs.insurance.product.ProductMaster;
import com.iscs.uw.policy.Policy;

/** Loads Producer Commission 
 * 
 * @author moniquef
 */
public class CommissionBuilder implements RuleProcessor {

	// Rule processor interface
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user, JDBCData data) 
			throws RuleException {	
		ModelBean errors = null;
		try{
			Log.debug("Processing CommissionBuilder...");
			
			// Build the Errors ModelBean
			errors = new ModelBean("Errors");	
			
			boolean updateOnlyCommissionAreas = false;
			
			ModelBean param = ruleTemplate.findBeanByFieldValue("Param", "Name", "UpdateOnlyCommisionAreas");
			if (param != null && StringTools.isTrue(param.gets("Value"))){
				updateOnlyCommissionAreas = true;
			}
	
			// Set Commission Percent
			setCommissionPercent(data, bean, updateOnlyCommissionAreas); 
		}
		catch(Exception e){	
			throw new RuleException(e.getMessage(),e);
		}
		return errors;		
	}
	
	/** Set the Coverage Commission Percent to the Producer's Commission Percent
     * @param data JDBCData connection to use to obtain the commission percents from the Producer table.
     * @param application ModelBean containing the application/quote
	 * @throws Exception 
     */    
    protected void setCommissionPercent(JDBCData data, ModelBean application) throws Exception {
    	 setCommissionPercent(data, application, false); 
    }
    
	/** Set the Coverage Commission Percent to the Producer's Commission Percent
     * @param data JDBCData connection to use to obtain the commission percents from the Producer table.
     * @param application ModelBean containing the application/quote
     * @param updateOnlyCommissionAreas Flag that indicating that the commission percentage should only be updated for commission areas and NOT in the BasicPolicy
     */    
    protected void setCommissionPercent(JDBCData data, ModelBean application, boolean updateOnlyCommissionAreas) 
    		throws Exception {
    	ModelBean basicPolicy = application.getBean("BasicPolicy");
    	if( !basicPolicy.valueEquals("ProviderRef", "") ) {
    		
    		// Get the Application's Producer Commission Percent
            String commissionPct = getProducerCommission(data, application);
            
    		if (!updateOnlyCommissionAreas){
                // Update the Application ModelBean with the New Commission Percent only if it is not restricted
                basicPolicy.setValue("Commission", commissionPct);
    		}

            // Loop Through All the CommissionArea ModelBeans and Update the Percent
            ModelBean commissionAreas[] = basicPolicy.getAllBeans("CommissionArea");  
            
            updateCommissionAreas(data, application, commissionPct, commissionAreas);
    	}
    }

	/** Update the commission area beans
	 * @param data The JDBCData
	 * @param application The Application bean
	 * @param commissionPct The commission percentage
	 * @param commissionAreas The commission area beans
	 * @throws ModelBeanException
	 * @throws Exception
	 */
	protected void updateCommissionAreas(JDBCData data, ModelBean application, String commissionPct,	ModelBean[] commissionAreas) 
				throws ModelBeanException, Exception {
		ModelBean basicPolicy = application.getBean("BasicPolicy");
		int providerRef = basicPolicy.getValueInt("ProviderRef");
		ModelBean provider = Provider.getProviderBySystemId(data, providerRef);
		boolean providerOnCommissionPlan = StringTools.isTrue(provider.gets("ProviderOnCommissionPlan"));  
		
		for (int i = 0; i < commissionAreas.length; i++ ) {
		    if (providerOnCommissionPlan){
		    	String commissionAreaCd = commissionAreas[i].gets("CommissionAreaCd");
		    	String code = createCodeFromApplication(application);
				String commissionFromPlanPct = getCommisionFromPlan(data, application, provider, commissionAreaCd, "", code);
				commissionAreas[i].setValue("CommissionPct", commissionFromPlanPct);
		    } else {
		    	commissionAreas[i].setValue("CommissionPct", commissionPct);
		    }	
		    Money finalCommissionPct = new Money("0.00");
		    if( commissionAreas[i].gets("OverrideInd").equals("Yes") ) {
		        finalCommissionPct = new Money(commissionAreas[i].gets("OverridePct")).subtract( new Money(commissionAreas[i].gets("ContributionPct")) );
		    }
		    else {
		    	finalCommissionPct = new Money(commissionPct).subtract( new Money(commissionAreas[i].gets("ContributionPct")) );
		    }
		    commissionAreas[i].setValue("FinalCommissionPct", finalCommissionPct);
		}
	}

    /** Get the Commission Percent from the Application's Producer
     * @param data JDBCData
     * @param application ModelBean
     * @return Producer Commission Percent
     */
    protected String getProducerCommission(JDBCData data, ModelBean application) 
	throws Exception {
        String result = "";    
        ModelBean basicPolicy = application.getBean("BasicPolicy");
        int providerRef = basicPolicy.getValueInt("ProviderRef");
        
        ModelBean provider = Provider.getProviderBySystemId(data, providerRef);
        
        boolean providerOnCommissionPlan = StringTools.isTrue(provider.gets("ProviderOnCommissionPlan"));
        if (providerOnCommissionPlan){
        	String code = createCodeFromApplication(application);
        	result = getCommisionFromPlan(data, application, provider, "", "", code);
        } else {
        	result = getProducerCommissionFromLicensedProduct(application, provider);
        }	
        if (!result.isEmpty()){
        	return result;
        }
        throw new Exception("Unable to set commission using the selected producer");
    }

    /** Get the Commission Plan from the DB
     * It finds only the latest plans that satisfy the criteria
     * 
     * SELECT * FROM `CommissionPlan` WHERE `EffectiveDt` IN (SELECT MAX(`EffectiveDt`) 
     * FROM `CommissionPlan` WHERE `EffectiveDt` <= '2015-01-15') AND `PlanCd` = 'Gold' 
     * AND `Status` = 'Active' AND `LicenceClass` = 'PP' AND `State` IN ('Default', 'CA') 
     * ORDER BY CASE WHEN `State` = 'CA' THEN `State` END DESC
     * 
     * @param data The JDBCData
     * @param application The Application bean
     * @param provider The Provider bean
     * @param commissionAreaCd The commission area code
     * @param businessSource The Business Source code
     * @param codeCd The code cd
     * @return The commission plan percentage
     * @throws Exception
     */
    
    public String getCommisionFromPlan(JDBCData data, ModelBean application, ModelBean provider, String commissionAreaCd, String businessSource, String codeCd) throws Exception{
    	boolean providerOnCommissionPlan = StringTools.isTrue(provider.gets("ProviderOnCommissionPlan"));
        if (!providerOnCommissionPlan){
        	throw new Exception("Trying to get a Commission Plan from a provider, but the provider is not on a Commission Plan.");
        }
        
        ModelBean basicPolicy = application.getBean("BasicPolicy");
        StringDate appEffectiveDate = basicPolicy.getDate("EffectiveDt");
        
        String providerPlanCd = getInEffectCommisionPlan(provider, appEffectiveDate);
        
        // Get the ProductMaster so we can find the associated LicenseClassCd's
        ModelBean productMaster = (ModelBean) ProductMaster.getProductMaster("UW").getBean();
        
        // Identify the LicenseClassCd this application's product is associated with
        ModelBean productVersion = productMaster.findBeanByFieldValue("ProductVersion","id", basicPolicy.gets("ProductVersionIdRef"));
        String productLicenseClassCd = productVersion.getParentBean().gets("LicenseClassCd");
        String controllingState = basicPolicy.gets("ControllingStateCd");
        
		// Set up the sql helpers
        SQLRelationDelegate delegate = SQLRelationHelper.getDelegate(data);
		SQLRelationHelper sqlHelp = new SQLRelationHelper(data, delegate); 
		
		String dbTableName = SQLRelationHelper.getDelegate(data).tableName("CommissionPlan");
		String query = "SELECT * FROM " + dbTableName;
		
		query = query + " WHERE " + delegate.columnName("EffectiveDt") + " IN (SELECT MAX(" + delegate.columnName("EffectiveDt") + ") FROM " + dbTableName;
		query = query + " WHERE " + delegate.columnName("EffectiveDt") + " <= " + data.getDelegate().formatForSQL(appEffectiveDate) + ")";
		
		query = query + " AND " + delegate.columnName("PlanCd") + " = '" + providerPlanCd + "'";
		query = query + " AND " + delegate.columnName("Status") + " = 'Active'";
		query = query + " AND " + delegate.columnName("LicenceClass") + " = '" + productLicenseClassCd + "'";
		query = query + " AND " + delegate.columnName("State") + " IN ('Default', '" + controllingState + "')";
		if (commissionAreaCd != null && !commissionAreaCd.isEmpty()){
			query = query + " AND " + delegate.columnName("CommissionArea") + " = '" + commissionAreaCd + "'";
		}
		query = query + " ORDER BY CASE WHEN " + delegate.columnName("State") + " = '" + controllingState + "' THEN " + delegate.columnName("State") + " END DESC";
		
		ModelBean[] commissionPlans = sqlHelp.doQuery("CommissionPlan", query, 0);
		ModelBean commissionPlan = null;

		commissionPlans = filterByBusinessSourceAndCode(commissionPlans, businessSource, codeCd);
		if (commissionPlans.length > 0){
			commissionPlan = commissionPlans[0];
		}
		if (commissionPlan == null){
			String msg = "No commission plan has been found for Effective Date: " + appEffectiveDate.toStringDisplay()
			+ ", State: " + controllingState
			+ ", Licence Class: " + productLicenseClassCd
			+ ", Commission Area: " + commissionAreaCd
			+ ", Business Source: " + businessSource
			+ ", Code: " + codeCd;
			throw new Exception(msg);
		}	

		if (!isPolicyRenewal(application)){
        	Money commission = new Money(commissionPlan.gets("NewBusiness"));
            return commission.toString();
        } else {
            Money commission = new Money(commissionPlan.gets("Renewal"));
            return commission.toString();
        }
    }
    
    /** Perform additional filtering on the retrieved CommissionPlans as needed
     * The method should return an Array of Commission Plans with the first element being the most suited to be picked.
     * In this example the BusinessSource takes precedence over the Code
     * 
     * @param commissionPlans The retrieved Commission Plans
     * @param businessSource The Basic Source
     * @return An Array of Commission Plans sorted by relevance
     * @throws ModelBeanException 
     */
    protected ModelBean[] filterByBusinessSourceAndCode(ModelBean[] commissionPlans, String businessSource, String codeCd) throws ModelBeanException{
    	
    	ModelBean[] sortedResult = null;
    	ArrayList<ModelBean> resultList = new ArrayList<ModelBean>();
    	
    	//In this example the BusinessSource takes precedence over the Code
    	
		//Filter the Commission Plans by Code. First take the values that exactly match and then the Default or empty values
    	//Find by specific Code
		for (ModelBean plan: commissionPlans){
	    	if (plan.gets("CodeCd").equalsIgnoreCase(codeCd)){
	    		resultList.add(plan);
	    	}
		}
			
	    //Find by Default or empty value
		for (ModelBean plan: commissionPlans){
	    	if (plan.gets("CodeCd").equalsIgnoreCase("Default") || plan.gets("CodeCd").isEmpty()){
	    		if (!resultList.contains(plan)){
	    			resultList.add(plan);
	    		}
	    	}
		}
		sortedResult = (ModelBean[]) resultList.toArray(new ModelBean[resultList.size()]);
		
		//Filter the Commission Plans by BusinessSource. First take the values that exactly match and then the Default or empty values
    	//Find by specific Business Source
		resultList = new ArrayList<ModelBean>();
		for (ModelBean plan: sortedResult){
	    	if (plan.gets("BusinessSource").equalsIgnoreCase(businessSource)){
	    		resultList.add(plan);
	    	}
		}
			
	    //Find by Default or empty value
		for (ModelBean plan: sortedResult){
	    	if (plan.gets("BusinessSource").equalsIgnoreCase("Default") || plan.gets("BusinessSource").isEmpty()){
	    		if (!resultList.contains(plan)){
	    			resultList.add(plan);
	    		}
	    	}
		}
		sortedResult = (ModelBean[]) resultList.toArray(new ModelBean[resultList.size()]);
		
		//Do any additional filtering, making sure that the first values in the list are the most suitable ones
    	
    	sortedResult = (ModelBean[]) resultList.toArray(new ModelBean[resultList.size()]);
    	return sortedResult;
     }
    
    /** Return the Commission in effect Commission Plan form the provider
     * This will return the plan that has the latest effective date but is before the application effective date
     * @param provider The Provider bean
     * @param appEffectiveDate The application's effective date
     * @return The in effect plan
     * @throws Exception
     */
    protected String getInEffectCommisionPlan(ModelBean provider, StringDate appEffectiveDate) throws Exception{
    	ModelBean[] commissionPlans = provider.findBeansByFieldValue("CommissionPlan", "Status", "Active");
    	if (commissionPlans == null || commissionPlans.length == 0){
    		throw new Exception("Trying to get a Commission Plan from a provider, but the provider is not on a Commission Plan.");
    	}
    	
    	ModelBean resultPlan = null;
    	for (ModelBean plan: commissionPlans){
    		if (plan.getDate("EffectiveDt").compareTo(appEffectiveDate) <= 0){
    			if (resultPlan == null || resultPlan.getDate("EffectiveDt").compareTo(plan.getDate("EffectiveDt")) < 0){
    				resultPlan = plan;
    			}
    		}
    	}
    	if (resultPlan == null ){
    		throw new Exception("The provider: " + provider.gets("IndexName") + " does not have a Commission Plan effective on " + appEffectiveDate.toString() );
    	}
    	return resultPlan.gets("PlanCd");
    }
    
    /** Create the code from the Application
     * This needs to be implemented to suit specific business needs
     * @param application The Application bean
     * @return The created code
     * @throws Exception 
     */
    protected String createCodeFromApplication(ModelBean application) throws Exception{
    	return application.getBean("BasicPolicy").gets("CarrierGroupCd");
    }
    
    /** Verify if the policy is a renewal 
     * @param application The Application bean
     * @return true if if it is a policy renewal
     * @throws Exception 
     */
    protected boolean isPolicyRenewal(ModelBean application) throws Exception{
    	ModelBean basicPolicy = application.getBean("BasicPolicy");
		return Policy.isRenewal(basicPolicy);
    }
    
	/** Get the producer's commission from the Licensed products
	 * @param application The Application bean 
	 * @param providerThe Provider bean
	 * @return the producer's commission
	 * @throws ModelBeanException
	 * @throws ProductException
	 * @throws Exception
	 */
	protected String getProducerCommissionFromLicensedProduct(ModelBean application, ModelBean provider)
			throws ModelBeanException, ProductException, Exception {
		
		String result = "";
		ModelBean basicPolicy = application.getBean("BasicPolicy");
		String carrierGroupCd = basicPolicy.gets("CarrierGroupCd");
		String productVersionIdRef = basicPolicy.gets("ProductVersionIdRef");
        
        // Get the ProductMaster so we can find the associated LicenseClassCd's
        ModelBean productMaster = (ModelBean) ProductMaster.getProductMaster("UW").getBean();
	
		// Identify the LicenseClassCd this application's product is associated with
		ModelBean productVersion = null;
		ModelBean[] products = null;
		if (!carrierGroupCd.isEmpty()){
			ModelBean carrierGroup = productMaster.findBeanByFieldValue("ProductCarrierGroup", "CarrierGroupIdRef", carrierGroupCd);
			productVersion = carrierGroup.findBeanByFieldValue("ProductVersion","id", productVersionIdRef);
			products = provider.findBeansByFieldValue("LicensedProduct", "CarrierGroup", carrierGroupCd);
		} else {
			productVersion = productMaster.findBeanByFieldValue("ProductVersion","id", productVersionIdRef);
			products = provider.getAllBeans("LicensedProduct");
		}
		
		// Sort products to get the most correct commission by effective date
        ModelBean[] sorted = BeanTools.sortBeansBy(products, new String[] {"EffectiveDt"}, new String[] {"Descending"});
        
        String productLicenseClassCd = productVersion.getParentBean().gets("LicenseClassCd");
        
        for( int i = 0; i < sorted.length; i++ ) {
            if( sorted[i].gets("LicenseClassCd").equals(productLicenseClassCd)){
                if( sorted[i].gets("StateProvCd").equals(basicPolicy.gets("ControllingStateCd")) ) {
                    if( sorted[i].getValueDate("EffectiveDt").compareTo(basicPolicy.getValueDate("EffectiveDt")) <= 0) {
                        if( !Policy.isRenewal(basicPolicy) ) {
                        	Money commission = new Money(sorted[i].gets("CommissionNewPct"));
                            result = commission.toString();
                        } else {
                            Money commission = new Money(sorted[i].gets("CommissionRenewalPct"));
                            result = commission.toString();
                        }
                        break;
                    }
                }
            }
        }
		return result;
	}
}