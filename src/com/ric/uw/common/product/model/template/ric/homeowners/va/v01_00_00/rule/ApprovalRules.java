package com.ric.uw.common.product.model.template.ric.homeowners.va.v01_00_00.rule;

import com.iscs.common.admin.user.render.UserRenderer;
import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.bean.BeanDiff;
import com.iscs.common.utility.bean.render.BeanRenderer;
import com.iscs.common.utility.validation.render.ValidationRenderer;
import com.iscs.uw.common.transaction.render.TransactionRenderer;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;


public class ApprovalRules implements RuleProcessor{

	private String policyTabParam = "CodeRefOptionsKey=application-product&TabOption=Policy&ShowRequiredFieldsInd=Yes";
	private String dwellingTabParam = "CodeRefOptionsKey=application-product&TabOption=Risks&ShowRequiredFieldsInd=Yes";
	private String genericTabLink = "javascript:cmmXFormSafe('UWApplicationSync','%s','%s','%s','%s')";
	private ModelBean application;
	private ModelBean dwellingQuestionReplies;
	private ModelBean basicPolicy;
	private ModelBean insured;
	private ModelBean line;
	private ModelBean risk;
	private ModelBean[] losses;
	private ModelBean oldPolicy;
	private BeanDiff applicationDiff;
	private StringDate todayDt;
	private ModelBean transactionInfo;
	private ModelBean response;
	private ModelBean errors;
	private ModelBean user;
	private String transactionCd;
	private String todayYear;

	@Override
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user,
			JDBCData data) throws RuleException {
		try {
			this.user = user;
			response = additionalBeans[0];
			errors = response.getBean("ResponseParams").getBean("Errors");
			application = bean;
			basicPolicy = application.getBean("BasicPolicy");
			insured = application.getBean("Insured");
			line = application.getBean("Line");
			risk = line.getBean("Risk", "Status", "Active");
			losses = application.findBeansByFieldValue("LossHistory", "StatusCd", "Active");
			transactionInfo = application.getBean("TransactionInfo");
			dwellingQuestionReplies = application.getBean("QuestionReplies");
			transactionCd = transactionInfo.gets("TransactionCd");
			todayDt = new StringDate(DateTools.getDate());
			todayYear = todayDt.toString().substring(0,4);
			
			//Generate Diffs, this is used for detecting changes to the policy when transaction cd is not "New Business" and "Rewrite-New"
			if( !transactionCd.equals("New Business") && !transactionCd.equals("Rewrite-New") ) {
				oldPolicy = TransactionRenderer.getApplicationStart(data, application);
				applicationDiff = BeanRenderer.getBeanDiff(oldPolicy, application, true, "ChangeInfo,Output,AuditData");
			}

			//Trigger Rules for "New Business" and "Rewrite-New"
			if( transactionCd.equals("New Business") || transactionCd.equals("Rewrite-New") ){
				triggerContructionCdEIFSApproval();
			}

			//Trigger Rules for "Endorsement"
			if( transactionCd.equals("Endorsement") ){
				triggerContructionCdEIFSApproval();
			}

			//Trigger Rules for "Renewal" and "Rewrite-Renewal"
			if( transactionCd.equals("Renewal") || transactionCd.equals("Rewrite-Renewal") ){
				triggerContructionCdEIFSApproval();
			}

			//Trigger Rules for "Cancellation"
			if( transactionCd.equals("Cancellation") ){
				
			}

			return errors;
		} catch ( Exception e ) {
			throw new RuleException( e );
		}
	}
	
	private void triggerContructionCdEIFSApproval() throws Exception {
		//Declare Variables and check authority
		boolean canApproveConstructionCdEIFS = UserRenderer.hasAuthority(user, "HOApproveConstructionCdEIFS", todayDt);
		String constructionCd = risk.getBean("Building").gets("ConstructionCd");
		
		//Check to see if any variables have been changed. This is only applicable for any transaction cds besides "New Business" and "Rewrite-New" 
		//If no changes made, leave method.
		if( applicationDiff != null && !applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@ConstructionCd"))
			return;
		
		//Approval Rule itself - Check for authority first. If they have authority, it will short circuit the rule.
		if( !canApproveConstructionCdEIFS && constructionCd.equals("EIFS") ) {
			ValidationRenderer.addValidationError(application, "Approval", "APPROVAL", "Construction type requires approval");
		}
	}
}

