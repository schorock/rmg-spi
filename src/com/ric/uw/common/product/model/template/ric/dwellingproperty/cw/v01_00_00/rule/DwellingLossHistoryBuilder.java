package com.ric.uw.common.product.model.template.ric.dwellingproperty.cw.v01_00_00.rule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.webpath.FrontController;

import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.uw.common.shared.LossHistory;
import com.iscs.uw.policy.PM;
import com.iscsuwpp.uw.common.product.LossHistoryBuilderBase;

/** Loads Loss History Information for the Application or Policy
 * 
 * @author moniquef
 */
public class DwellingLossHistoryBuilder extends LossHistoryBuilderBase implements RuleProcessor {

	// Rule processor interface
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user, JDBCData data) 
	throws RuleException {	
		ModelBean errors = null;
		try{
			Log.debug("Processing DwellingLossHistoryBuilder...");
			
			// Build the Errors ModelBean
			errors = new ModelBean("Errors");	
			
			// Get Transaction Code
			String transactionCd = "";
			if( bean.getBeanName().equals("Application") ) {
				ModelBean transactionInfo = bean.getBean("TransactionInfo");            
	            transactionCd = transactionInfo.gets("TransactionCd");
			} else if( bean.getBeanName().equals("Policy") ) {
				ModelBean param = ruleTemplate.getBean("Param", "Name", "TransactionCd");
				if( param != null )
					transactionCd = param.gets("Value");
			}
			if( transactionCd.equals(PM.TX_RENEWAL_START) || transactionCd.equals(PM.TX_RENEWAL) || transactionCd.equals(PM.TX_REWRITE_RENEWAL) ) {
				
				// Build Service Request ModelBean
	            ModelBean serviceRq = buildRequest(ruleTemplate, bean);
	            
	            // Turn Off Debug Messages
	            Log.setLoggingLevelError();
	            
	            // Send Webpath Request
				FrontController controller = new FrontController();
		        ModelBean response = controller.sendRequest(serviceRq, data);
		        ModelBean[] dtoClaim = response.getBeans("DTOClaim");
		        				
				// Load the Loss History Information
				load(dtoClaim, bean);
				
				Log.debug("Bean: " + bean.readableDoc());
			}
		}
		catch(Exception e){	
			throw new RuleException(e);
		} finally {
			// Set Log Level back after processing the FrontController request
			Log.setLoggingLevelDefault();
		}
		return errors;		
	}
    
    /** Check for Matching Loss History, Then Update or Load New Loss History
	 * @param dtoClaim ModelBean Array
	 * @param bean ModelBean
	 * @throws Exception if an Unexpected Error Occurs
	 */
	public static void load(ModelBean[] dtoClaims, ModelBean bean) 
	throws Exception{
		try {
			// Loop Through Claims
			for(ModelBean dtoClaim:dtoClaims){
				
				if(dtoClaim.gets("ForRecordOnlyInd").equalsIgnoreCase("Yes")){
					continue;
				}
				// Check for Matching Loss
				ModelBean lossHistory = getLossHistoryMatch(dtoClaim, bean);
				if( lossHistory == null ) {
					lossHistory = new ModelBean("LossHistory");
					lossHistory.setValue("StatusCd", "New");
					bean.addValue(lossHistory);
				}
				
				// Load Loss History
				loadLossHistory(dtoClaim, bean, lossHistory); 
			}
			
		}
		catch(Exception e){
			throw new Exception(e);
		}
	}
	
	/** Attempt to Get Matching Loss History
	 * @param dtoClaim ModelBean 
	 * @param bean ModelBean
	 * @throws Exception if an Unexpected Error Occurs
	 */
	public static ModelBean getLossHistoryMatch(ModelBean dtoClaim, ModelBean bean) 
	throws Exception{
		try {
			ModelBean[] lossHistory = BeanTools.findBeansByFieldValueAndStatus(bean, "LossHistory", "SourceCd", dtoClaim.gets("SourceCd"), "Active");
			for( int i = 0; i < lossHistory.length; i++ ) {
				if( lossHistory[i].valueEquals("ClaimNumber", dtoClaim.gets("ClaimNumber")) ) {
					return lossHistory[i];
				}
			}
			
			return null;
		}
		catch(Exception e){
			throw new Exception(e);
		}
	}
	
	/** Load Loss History Information from DTOClaim
	 * @param dtoClaim ModelBean 
	 * @param bean ModelBean
	 * @param lossHistory ModelBean
	 * @throws Exception if an Unexpected Error Occurs
	 */
	public static void loadLossHistory(ModelBean dtoClaim, ModelBean bean, ModelBean lossHistory) 
	throws Exception{
		try {
			if( lossHistory.valueEquals("LossHistoryNumber", "") )
				lossHistory.setValue("LossHistoryNumber", Integer.toString(LossHistory.nextNumber(bean)));
			
			lossHistory.setValue("StatusCd", "Active");
			lossHistory.setValue("SourceCd", dtoClaim.gets("SourceCd"));
			lossHistory.setValue("LossDt", dtoClaim.getDate("LossDt"));
			lossHistory.setValue("LossCauseCd", dtoClaim.gets("LossCauseCd"));
			lossHistory.setValue("LossAmt", dtoClaim.gets("DamageEstimateAmt"));
			if( lossHistory.valueEquals("LossDesc", "") )
				lossHistory.setValue("LossDesc", dtoClaim.gets("Description"));
			
			lossHistory.setValue("AtFaultCd", dtoClaim.gets("AtFaultCd"));
			lossHistory.setValue("CatastropheNumber", dtoClaim.gets("CatastropheNumber"));
			lossHistory.setValue("ClaimNumber", dtoClaim.gets("ClaimNumber"));
			lossHistory.setValue("ClaimStatusCd", dtoClaim.gets("StatusCd"));
			
			ModelBean dtoClaimPolicyInfo = dtoClaim.getBean("DTOClaimPolicyInfo");
			lossHistory.setValue("PolicyNumber", dtoClaimPolicyInfo.gets("PolicyNumber"));
			lossHistory.setValue("PolicyTypeCd", "Dwelling");
			lossHistory.setValue("CarrierName", dtoClaimPolicyInfo.gets("CarrierName"));
			
			if( lossHistory.valueEquals("Comment", "") )
				lossHistory.setValue("Comment", dtoClaim.gets("Comment"));
			
			String totalImdemnityReserveAmt = "0.00";
			String totalImdemnityPaidAmt = "0.00";
			ModelBean[] dtoReserveAllocation = dtoClaim.getAllBeans("DTOReserveAllocation");
			for( int i = 0; i < dtoReserveAllocation.length; i++ ) {
				if(dtoReserveAllocation[i].gets("ReserveCd").equalsIgnoreCase("Indemnity")){
					totalImdemnityReserveAmt = StringTools.addMoney(dtoReserveAllocation[i].gets("ReserveAmt"), totalImdemnityReserveAmt);
					totalImdemnityPaidAmt = StringTools.addMoney(dtoReserveAllocation[i].gets("PaidAmt"), totalImdemnityPaidAmt);
				}
			}
			lossHistory.setValue("LossAmt", StringTools.addMoney(totalImdemnityReserveAmt, totalImdemnityPaidAmt));
			lossHistory.setValue("PaidAmt", totalImdemnityPaidAmt);
			
			lossHistory.setValue("TypeCd", "Property");
			
			lossHistory.setValue("LossType",dtoClaim.gets("CategoryCd"));
			if(dtoClaim.valueEquals("ClaimLossTemplateIdRef", "Weather")){
				lossHistory.setValue("WeatherInd","Yes");
			}else if(dtoClaim.valueEquals("ClaimLossTemplateIdRef", "NonWeather")){
				lossHistory.setValue("WeatherInd","No");
			}
			if(!dtoClaim.valueEquals("CatastropheNumber", "")){
				lossHistory.setValue("CATInd","Yes");
			}else{
				lossHistory.setValue("CATInd","No");
			}
			ModelBean [] claimantTransactions = dtoClaim.getAllBeans("DTOClaimantTransaction");
			List <ModelBean> sortList = new ArrayList<ModelBean>();
			for(ModelBean claimantTransaction : claimantTransactions){
				if(claimantTransaction.valueEquals("TransactionCd", "Payment")){
					sortList.add(claimantTransaction);
				}					
			}
			if(sortList.size() > 1){
				Collections.sort(sortList, new Comparator<ModelBean>(){
					public int compare(ModelBean bean1, ModelBean bean2) throws ClassCastException {
						try{
							StringDate date1 = bean1.getDate("TransactionDt");
							StringDate date2 = bean2.getDate("TransactionDt");
							int dateDiff = date2.compareTo(date1);
							if(dateDiff == 0){
								String time1 = bean1.gets("TransactionTm");
								String time2 = bean2.gets("TransactionTm");
								return time2.compareTo(time1);
							}else{
								return dateDiff;
							}
						}catch(Exception e){
							throw new ClassCastException("Error occurred trying to compare fields for sorting." + e.toString() );
						}
					}
				});
			}
			if(sortList.size() > 0){
				lossHistory.setValue("LastPaymentDt",sortList.get(0).getDate("TransactionDt"));
			}
		}
		catch(Exception e){
			throw new Exception(e);
		}
	}
}