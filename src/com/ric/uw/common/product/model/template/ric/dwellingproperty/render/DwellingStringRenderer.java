/**
 * 
 */
package com.ric.uw.common.product.model.template.ric.dwellingproperty.render;

import java.math.BigDecimal;

import net.inov.tec.beans.ModelBean;

import com.iscs.common.render.Renderer;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanDiff;

/**
 * @author karthick.gopalakrish
 *
 */
public class DwellingStringRenderer implements Renderer {

	/**
	 * 
	 */
	public DwellingStringRenderer() {
	}

	/* (non-Javadoc)
	 * @see com.iscs.common.render.Renderer#getContextVariableName()
	 */
	@Override
	public String getContextVariableName() {
		return "DwellingStringRenderer";
	}	

	/**
	 * Divide Money Value1 by Money Value2 and find Percent
	 * 
	 * @param value1
	 *            Dividend 
	 * @param value2
	 *            Divisor 
	 * @return Calculated Value
	 */
	public static String divideReturnPercent(String value1, String value2) {

		BigDecimal moneyValue1 = stringToNumber(value1);
		BigDecimal moneyValue2 = stringToNumber(value2);
		if (moneyValue2 == null || moneyValue1 == null)
			return null;

		BigDecimal pctVal = moneyValue2.multiply(new BigDecimal(100)).divide(moneyValue1, 2, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP);
		return pctVal.toString();

	}	

	private static BigDecimal stringToNumber(String value) {

		if (value == null) {
			return null;
		}
		value = value.trim();
		value = StringTools.replaceAll(value, "\\$", "");
		value = value.replaceAll(",", "");

		return new BigDecimal(value);

	}

	/** Creates a new instance of Date from a String in the compact
	 * date format ccyymmdd and returns the Year
	 * @param ccyymmdd compact String representation of a date
	 */
	public static String getYear(String ccyymmdd) {
		java.util.Calendar calendar = new java.util.GregorianCalendar();
		// Assures that the time is set to 00:00:00.
		calendar.clear();

		String datestr = ccyymmdd;

		if (datestr == null) {
			throw new java.lang.IllegalArgumentException("ccyymmdd is null");
		}

		if (datestr.length() != 8) {
			throw new java.lang.IllegalArgumentException("ccyymmdd should be 8 digits.");
		}

		// fmtstring is assumed to be in the format ccyymmdd
		int year = Integer.parseInt( datestr.substring(0,4) );
		int month = Integer.parseInt( datestr.substring(4,6) ) - 1;
		int day = Integer.parseInt( datestr.substring(6,8) );

		calendar.set(year,month,day);   // New calendar instance
		return ""+year;
	}

	/** input date format MM/DD/YYYY and returns the Year
	 * @param d is String representation of a date
	 */
	public static String getYearGivenDate(String d) {
		int year=0;
		if (d.length() == 10) {
			year = Integer.parseInt( d.substring(6,10));
		}
		return ""+year;
	}

	/**
	 * Returns code for coverage associated with updated coverage item 
	 * @param app Current application ModelBean
	 * @param changedCoverageItem BeanDiff for single changed coverage item
	 * @return coverage code
	 */
	public static String getCoverageCd(ModelBean app, BeanDiff changedCoverageItem){
		try {
			ModelBean oldVar = changedCoverageItem.getOldBean();
			String idVar = oldVar.gets("id");
			ModelBean covItem = app.findBeanById("CoverageItem", idVar);
			ModelBean cov = covItem.getParentBean();
			String covCd = cov.gets("CoverageCd");
			return covCd;
		} catch (Exception e) {
			return "";    		
		}

	}

}
