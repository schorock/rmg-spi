package com.ric.uw.common.product.model.template.ric.dwellingproperty.va.v01_00_00.rule;

import net.inov.tec.beans.ModelBean;

import com.ibm.math.BigDecimal;
import com.iscs.uw.common.coverage.Coverage;
import com.iscs.uw.common.rating.RatingBase;
import com.iscs.uw.common.rating.RatingException;


public class DwellingRating extends com.iscs.rules.rating.DwellingRating {
	
	@Override
	protected void calcMinimumPremium(ModelBean application) throws RatingException {
		try {
			BigDecimal minPremiumAmt = getMinPremiumAmt();
			
			ModelBean basicPolicy = application.getBean("BasicPolicy");
			
			// First check to see if we need to apply it
			if(basicPolicy.getDecimal("FullTermAmt").compareTo(minPremiumAmt) < 0 ) {
				// Determine the difference that we need to add for min premium
				BigDecimal adjustedAmt = minPremiumAmt.subtract(application.getBean("BasicPolicy").getDecimal("FullTermAmt") );
				basicPolicy.setValue("MinPremiumInd", "Yes");
				
				// Find the first Active Building
				ModelBean line = application.findBeanByFieldValue("Line", "LineCd", "Dwelling");
				if(line != null) {
					ModelBean[] active = active(line, "Risk");
					if(active.length > 0) {
						ModelBean risk = active[0];
						ModelBean[] coverages;
						
						coverages = Coverage.getActiveCoverages(risk, "CovA");
						
						if(coverages.length > 0) {
							// Find the "Fire Rate Area"
							ModelBean[] rateAreas = coverages[0].getBeans("RateArea");
							for( ModelBean rateArea : rateAreas) {
								if(rateArea.gets("Description").contains("Fire") ) {
									if(rateArea.gets("Status").equals("Active") ) {
										BigDecimal currentPremium = rateArea.getDecimal("FullTermAmt");
										// Add the min premium as the final rating step
										RatingBase.addRatingStep(rateArea, "Minimum Premium", "Minimum Premium", "+", adjustedAmt.toString(), currentPremium.add(adjustedAmt).toString() );
										rateArea.setValue("FullTermAmt", currentPremium.add(adjustedAmt) );
										
										// Finalize the application again since we have changed the premium
										finalizeRating(application);
									}
								}
							}					
						}
					}
				}
			} else {
				basicPolicy.setValue("MinPremiumInd", "No"); 
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RatingException(e);
		}
	}	
}
