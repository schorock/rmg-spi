package com.ric.uw.common.product.model.template.ric.dwellingproperty.handler;

import java.util.Hashtable;

import com.innovextechnology.service.Log;
import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDAUrl;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.utility.table.Row;
import com.iscs.common.utility.table.excel.ExcelReader;
import com.iscs.insurance.product.ProductSetup;
import com.iscs.uw.common.rating.RatingTable;
import com.iscs.uw.common.rating.RatingTables;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;


public class TerritoryCodeUpdate extends InnovationIBIZHandler {
	private static Hashtable<String, RatingTables> TerritoryCodeCache = new Hashtable<String, RatingTables>(); 
	private RatingTables terrTables = null;
	private RatingTables premiumGroupTables = null;
	public TerritoryCodeUpdate() throws Exception {
		super();
	}

	public ModelBean process() throws IBIZException, ServiceHandlerException {
		try {
			Log.debug("Processing " + this.getClass().getName() + "...");
			ModelBean rs = getResponse();
			ModelBean application = rs.getBean("Application");
			ModelBean basicPolicy = application.getBean("BasicPolicy");
			String productVersionIdRef = basicPolicy.gets("ProductVersionIdRef");
			ModelBean productSetup = ProductSetup.getProductSetup(productVersionIdRef);
	        MDAUrl mdaTerritoryUrl = (MDAUrl) Store.getModelObject(productSetup.gets("TerritoryCodeListSource"));
	        ExcelReader excelReader = new ExcelReader();
	        terrTables = new RatingTables(excelReader.read(mdaTerritoryUrl.getURL()));
			RatingTable territoryTable = terrTables.get("territory-codes");
			
			
			MDAUrl mdaUrl = (MDAUrl) Store.getModelObject(productSetup.gets("RatingSource"));
	        ExcelReader premiumGroupExcelReader = new ExcelReader();
	        premiumGroupTables = new RatingTables(premiumGroupExcelReader.read(mdaUrl.getURL()));
			RatingTable table = premiumGroupTables.get("PremGroupSelection");
		
			ModelBean[] risks = application.findBeansByFieldValue("Risk","Status","Active");
			for (ModelBean risk : risks ){
				String status = risk.gets("Status");
				if(status != "Deleted") {
					ModelBean building = risk.getBean("Building");
					ModelBean addr = risk.getBean("Addr", "AddrTypeCd", "RiskLookupAddr");
					if( addr.gets("StateProvCd").equalsIgnoreCase("VA") ) {
						String beachRisk = building.gets("BeachRisk"); 
						String county = addr.gets("County");			
						if(!beachRisk.equals("")) {
							Row[] rows = territoryTable.lookup(new String[] {"County","Beach"}, new String[] {county,beachRisk});
							if(rows !=null && rows.length>0) {
								Row row = rows[0];
								building.setValue("TerritoryCd", row.getCell("TerritoryNumber").getValue());
							} else {						
								Row[] rows1 = territoryTable.lookup(new String[] {"County","Beach"}, new String[] {county,"No"});
								Row row = rows1[0];
								building.setValue("TerritoryCd", row.getCell("TerritoryNumber").getValue());
							}
						}
						if(!county.contains("Beach")) {
							Row[] rows2 = territoryTable.lookup(new String[] {"County","Beach"}, new String[] {county,"No"});
							Row row = rows2[0];
							building.setValue("TerritoryCd", row.getCell("TerritoryNumber").getValue());
						}
					} else {
						building.setValue("TerritoryCd", "0");
					}
					
					if( risk.gets("TypeCd").equals("Dwelling") ) {
						String territoryCd = building.gets("TerritoryCd"); 
						String protectionClass = building.gets("ProtectionClass");
						String constructionCd = building.gets("ConstructionCd");
						String dwellingStyle = building.gets("DwellingStyle");	
						String dwProgramTypeCd = basicPolicy.gets("DwProgramTypeCd");						

						// Premium Group Table only has Masonry and not "Masonry, Brick", so we will be using Masonry instead for the lookup
						// Premium Group Table does not contain options for "Veneer, Brick" and "Veneer, Stone", so we will be using Masonry instead for the lookup as per BA 
						if(constructionCd.contains("Masonry") || constructionCd.contains("Veneer") ){
							constructionCd = "Masonry";
						
						// Premium Group Table only has Frame and not "Framing, Wood", so we will be using Frame instead for the lookup
						// Premium Group Table does not contain options for "Other", so we will be using Frame instead for the lookup as per BA 
						} else if(constructionCd.contains("Framing") || constructionCd.equals("Other") ){
							constructionCd = "Frame";
						}
						
						if( !territoryCd.equals("") && !territoryCd.equals("0") && !protectionClass.equals("") && !constructionCd.equals("") ) {
							if( !dwProgramTypeCd.equals("DP") ) {
								RatingTable tableLLP = premiumGroupTables.get("LLPremGroup");
								String lookupLLPKey = constructionCd + protectionClass + territoryCd;
								Row[] rowsLLP = tableLLP.lookup("Lookup", lookupLLPKey);
								
								if(rowsLLP !=null && rowsLLP.length>0) {
									Row row = rowsLLP[0];
									building.setValue("LLPremiumGroup", row.getCell("Group").getValue());
								} else {
									String lookupLLPKeyOther = "Other" + protectionClass + constructionCd; 
									Row[] rowsLLPOther = table.lookup("Lookup", lookupLLPKeyOther);
									if(rowsLLPOther !=null && rowsLLPOther.length>0) {
										Row row = rowsLLPOther[0];
										building.setValue("LLPremiumGroup", row.getCell("PremGroup").getValue());
									}
								}

								if(dwellingStyle.equalsIgnoreCase("Mobile Home") ) {
									constructionCd = "MobileHome";
								}
								
								String lookupKey = territoryCd + protectionClass + constructionCd; 
								Row[] rows = table.lookup("Lookup", lookupKey);
								if(rows !=null && rows.length>0) {
									Row row = rows[0];
									building.setValue("PremiumGroup", row.getCell("PremGroup").getValue());
								} else {
									String lookupKeyOther = "Other" + protectionClass + constructionCd; 
									Row[] rowsOther = table.lookup("Lookup", lookupKeyOther);
									if(rowsOther !=null && rowsOther.length>0) {
										Row row = rowsOther[0];
										building.setValue("PremiumGroup", row.getCell("PremGroup").getValue());
									}
								}						
							} else {
								if(dwellingStyle.equalsIgnoreCase("Mobile Home") ) {
									constructionCd = "MobileHome";
								}
								String lookupKey = territoryCd + protectionClass + constructionCd; 
								Row[] rows = table.lookup("Lookup", lookupKey);
								if(rows !=null && rows.length>0) {
									Row row = rows[0];
									building.setValue("PremiumGroup", row.getCell("PremGroup").getValue());
								} else {
									String lookupKeyOther = "Other" + protectionClass + constructionCd; 
									Row[] rowsOther = table.lookup("Lookup", lookupKeyOther);
									if(rowsOther !=null && rowsOther.length>0) {
										Row row = rowsOther[0];
										building.setValue("PremiumGroup", row.getCell("PremGroup").getValue());
									}
								}
							}
						}
					}
				}			
			}

			/*
			 * Always check for errors in the response and throw an IBIZException
			 * if any are found if even this handler did not generate them,
			 */
			if (hasErrors()) {
				throw new IBIZException("Error: Cannot assign TerritoryBeachCd");
			}
			return rs;
		} catch (IBIZException e) {
			Log.error(e);
			throw e;
		} catch (Exception e) {
			throw new ServiceHandlerException(e);
		}
	}
}