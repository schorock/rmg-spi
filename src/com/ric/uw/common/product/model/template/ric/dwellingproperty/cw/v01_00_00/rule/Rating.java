/*
 * DFMDRating010000.java
 *
 */

package com.ric.uw.common.product.model.template.ric.dwellingproperty.cw.v01_00_00.rule;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.date.StringDate;

import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDAUrl;
import com.iscs.common.render.DateRenderer;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.NumberTools;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.table.Row;
import com.iscs.common.utility.table.Table;
import com.iscs.common.utility.table.excel.ExcelReader;
import com.iscs.insurance.product.ProductMaster;
import com.iscs.insurance.product.ProductSetup;
import com.iscs.insurance.product.ProductVersion;
import com.iscs.uw.common.coverage.render.CoverageRenderer;
import com.iscs.uw.common.product.UWProductSetup;
import com.iscs.uw.common.rating.RatingBase;
import com.iscs.uw.common.rating.RatingException;
import com.iscs.uw.common.rating.RatingTable;
import com.iscs.uw.common.rating.RatingTables;

/** Rate Dwelling Application
 *
 * @author  Srirams	
 */
public class Rating extends RatingBase  {

	// --------------------------------------------------
	// Static variables (must be thread-safe)

	private static final BigDecimal zero = new BigDecimal("0.00");

	// Cache of rating tables by product version id
	private static Hashtable<String, RatingTables> ratingTablesCache = new Hashtable<String, RatingTables>(); 

	// --------------------------------------------------
	// Instance variables (do not need to be thread-safe)

	// Current rating tables for this instance of rating
	private RatingTables tables = null;

	private ModelBean application = null;
	private ModelBean basicPolicy = null;
	private ModelBean[] lossHistory = null;
	private ModelBean[] preInceptionLossHistory = null;

	private BigDecimal totalPremium = new BigDecimal("0.00");
	private boolean migratedPolicies=false;
	private BigDecimal basicPolicyPremium = zero;
	private BigDecimal Minus_One= new BigDecimal("-1");

	private BigDecimal covALimit;
	private BigDecimal covABasePremium;
	private BigDecimal covAPremFireOrdinance  = new BigDecimal("0.00");
	private BigDecimal covBPremFireOrdinance  = new BigDecimal("0.00");
	private BigDecimal covDPremFireOrdinance  = new BigDecimal("0.00");
	private BigDecimal covEPremFireOrdinance  = new BigDecimal("0.00");
	private BigDecimal covCPremFireOrdinance  = new BigDecimal("0.00");
	private BigDecimal covPremFireOrdinance  = new BigDecimal("0.00");
	private BigDecimal covAPremECOrdinance  = new BigDecimal("0.00");
	private BigDecimal covBPremECOrdinance  = new BigDecimal("0.00");
	private BigDecimal covDPremECOrdinance  = new BigDecimal("0.00");
	private BigDecimal covEPremECOrdinance  = new BigDecimal("0.00");
	private BigDecimal covCPremECOrdinance  = new BigDecimal("0.00");
	private BigDecimal covPremECOrdinance  = new BigDecimal("0.00");
	private BigDecimal covAAOPPremFire  = new BigDecimal("0.00");
	public BigDecimal getCovAAOPPremFire() {
		return covAAOPPremFire;
	}


	public void setCovAAOPPremFire(BigDecimal covAAOPPremFire) {
		this.covAAOPPremFire = covAAOPPremFire;
	}


	public BigDecimal getCovBAOPPremFire() {
		return covBAOPPremFire;
	}


	public void setCovBAOPPremFire(BigDecimal covBAOPPremFire) {
		this.covBAOPPremFire = covBAOPPremFire;
	}


	public BigDecimal getCovCAOPPremFire() {
		return covCAOPPremFire;
	}


	public void setCovCAOPPremFire(BigDecimal covCAOPPremFire) {
		this.covCAOPPremFire = covCAOPPremFire;
	}


	public BigDecimal getCovDAOPPremFire() {
		return covDAOPPremFire;
	}


	public void setCovDAOPPremFire(BigDecimal covDAOPPremFire) {
		this.covDAOPPremFire = covDAOPPremFire;
	}


	public BigDecimal getCovEAOPPremFire() {
		return covEAOPPremFire;
	}


	public void setCovEAOPPremFire(BigDecimal covEAOPPremFire) {
		this.covEAOPPremFire = covEAOPPremFire;
	}


	public BigDecimal getCovOrdAOPPremFire() {
		return covOrdAOPPremFire;
	}


	public void setCovOrdAOPPremFire(BigDecimal covOrdAOPPremFire) {
		this.covOrdAOPPremFire = covOrdAOPPremFire;
	}


	public BigDecimal getCovAddlAOPPremFire() {
		return covAddlAOPPremFire;
	}


	public void setCovAddlAOPPremFire(BigDecimal covAddlAOPPremFire) {
		this.covAddlAOPPremFire = covAddlAOPPremFire;
	}


	public BigDecimal getCovAAOPPremEc() {
		return covAAOPPremEc;
	}


	public void setCovAAOPPremEc(BigDecimal covAAOPPremEc) {
		this.covAAOPPremEc = covAAOPPremEc;
	}


	public BigDecimal getCovBAOPPremEc() {
		return covBAOPPremEc;
	}


	public void setCovBAOPPremEc(BigDecimal covBAOPPremEc) {
		this.covBAOPPremEc = covBAOPPremEc;
	}


	public BigDecimal getCovCAOPPremEc() {
		return covCAOPPremEc;
	}


	public void setCovCAOPPremEc(BigDecimal covCAOPPremEc) {
		this.covCAOPPremEc = covCAOPPremEc;
	}


	public BigDecimal getCovDAOPPremEc() {
		return covDAOPPremEc;
	}


	public void setCovDAOPPremEc(BigDecimal covDAOPPremEc) {
		this.covDAOPPremEc = covDAOPPremEc;
	}


	public BigDecimal getCovEAOPPremEc() {
		return covEAOPPremEc;
	}


	public void setCovEAOPPremEc(BigDecimal covEAOPPremEc) {
		this.covEAOPPremEc = covEAOPPremEc;
	}


	public BigDecimal getCovOrdAOPPremEc() {
		return covOrdAOPPremEc;
	}


	public void setCovOrdAOPPremEc(BigDecimal covOrdAOPPremEc) {
		this.covOrdAOPPremEc = covOrdAOPPremEc;
	}


	public BigDecimal getCovAddlAOPPremEc() {
		return covAddlAOPPremEc;
	}


	public void setCovAddlAOPPremEc(BigDecimal covAddlAOPPremEc) {
		this.covAddlAOPPremEc = covAddlAOPPremEc;
	}


	public BigDecimal getCovAAOPPremVmm() {
		return covAAOPPremVmm;
	}


	public void setCovAAOPPremVmm(BigDecimal covAAOPPremVmm) {
		this.covAAOPPremVmm = covAAOPPremVmm;
	}


	public BigDecimal getCovBAOPPremVmm() {
		return covBAOPPremVmm;
	}


	public void setCovBAOPPremVmm(BigDecimal covBAOPPremVmm) {
		this.covBAOPPremVmm = covBAOPPremVmm;
	}


	public BigDecimal getCovCAOPPremVmm() {
		return covCAOPPremVmm;
	}


	public void setCovCAOPPremVmm(BigDecimal covCAOPPremVmm) {
		this.covCAOPPremVmm = covCAOPPremVmm;
	}


	public BigDecimal getCovDAOPPremVmm() {
		return covDAOPPremVmm;
	}


	public void setCovDAOPPremVmm(BigDecimal covDAOPPremVmm) {
		this.covDAOPPremVmm = covDAOPPremVmm;
	}


	public BigDecimal getCovEAOPPremVmm() {
		return covEAOPPremVmm;
	}


	public void setCovEAOPPremVmm(BigDecimal covEAOPPremVmm) {
		this.covEAOPPremVmm = covEAOPPremVmm;
	}


	public BigDecimal getCovOrdAOPPremVmm() {
		return covOrdAOPPremVmm;
	}


	public void setCovOrdAOPPremVmm(BigDecimal covOrdAOPPremVmm) {
		this.covOrdAOPPremVmm = covOrdAOPPremVmm;
	}


	public BigDecimal getCovAddlAOPPremVmm() {
		return covAddlAOPPremVmm;
	}


	public void setCovAddlAOPPremVmm(BigDecimal covAddlAOPPremVmm) {
		this.covAddlAOPPremVmm = covAddlAOPPremVmm;
	}

	private BigDecimal covBAOPPremFire  = new BigDecimal("0.00");
	private BigDecimal covCAOPPremFire  = new BigDecimal("0.00");
	private BigDecimal covDAOPPremFire  = new BigDecimal("0.00");
	private BigDecimal covEAOPPremFire  = new BigDecimal("0.00");
	private BigDecimal covOrdAOPPremFire  = new BigDecimal("0.00");
	private BigDecimal covAddlAOPPremFire  = new BigDecimal("0.00");
	private BigDecimal covAAOPPremEc  = new BigDecimal("0.00");
	private BigDecimal covBAOPPremEc  = new BigDecimal("0.00");
	private BigDecimal covCAOPPremEc  = new BigDecimal("0.00");
	private BigDecimal covDAOPPremEc  = new BigDecimal("0.00");
	private BigDecimal covEAOPPremEc  = new BigDecimal("0.00");
	private BigDecimal covOrdAOPPremEc  = new BigDecimal("0.00");
	private BigDecimal covAddlAOPPremEc  = new BigDecimal("0.00");
	private BigDecimal covAAOPPremVmm  = new BigDecimal("0.00");
	private BigDecimal covBAOPPremVmm  = new BigDecimal("0.00");
	private BigDecimal covCAOPPremVmm  = new BigDecimal("0.00");
	private BigDecimal covDAOPPremVmm  = new BigDecimal("0.00");
	private BigDecimal covEAOPPremVmm  = new BigDecimal("0.00");
	private BigDecimal covOrdAOPPremVmm  = new BigDecimal("0.00");
	private BigDecimal covAddlAOPPremVmm  = new BigDecimal("0.00");

	DecimalFormat fmt = new DecimalFormat("#,###");

	private BigDecimal uwScore ;


	public BigDecimal getUwScore() {
		return uwScore;
	}


	public void setUwScore(BigDecimal uwScore) {
		this.uwScore = uwScore;
	}


	public BigDecimal getCovABasePremium() {
		return covABasePremium;
	}


	public void setCovABasePremium(BigDecimal covABasePremium) {
		this.covABasePremium = covABasePremium;
	}


	public BigDecimal getCovALimit() {
		return covALimit;
	}


	public void setCovALimit(BigDecimal bigDecimal) {
		this.covALimit = bigDecimal;
	}


	/** Creates a new instance of Dwelling Rate */
	public Rating() {
	}


	/** Overrides the base's preparation routine
	 * @param application ModelBean
	 * @throws RatingException for errors
	 * 
	 */
	public void prepare(ModelBean application) throws RatingException {
		try {

			// Call the super function's prepare method
			super.prepare(application);


			// Attempt to Get Cached Rating Tables
			String productVersionIdRef = application.getBean("BasicPolicy").gets("ProductVersionIdRef");
			if( ratingTablesCache.containsKey(productVersionIdRef) ) {
				tables = (RatingTables) ratingTablesCache.get(productVersionIdRef);
			} else {
				ModelBean productSetup = ProductSetup.getProductSetup(productVersionIdRef);

				MDAUrl mdaUrl = (MDAUrl) Store.getModelObject(productSetup.gets("RatingSource"));
				ExcelReader excelReader = new ExcelReader();
				tables = new RatingTables(excelReader.read(mdaUrl.getURL()));

				ratingTablesCache.put(productVersionIdRef, tables);
			}

			// Save commonly used beans
			this.application = application;
			basicPolicy = application.getBean("BasicPolicy");

			if(!basicPolicy.gets("LegacyPolicyNumber").equals("")){
				migratedPolicies=true;
			}
			if(migratedPolicies){
				lossHistory = getLossHistory(getAllActiveMigratedNonIgnoredLossesPostInception());
				preInceptionLossHistory = getLossHistory(getAllActiveMigratedNonIgnoredLosses());
			}else{
				lossHistory = getLossHistory(getAllActiveNonIgnoredLossesPostInception());
				preInceptionLossHistory = getLossHistory(getAllActiveNonIgnoredLosses());
			}
		}
		catch( Exception e ) {
			Log.error("General Error: " + e.getMessage());
			throw new RatingException(e);
		}

	}

	/**
	 * Rates each coverage in the line of business
	 * @param application ModelBean
	 * @param line Line of business
	 * @throws RatingException for errors
	 */
	public void rate(ModelBean application, ModelBean line) throws RatingException {
		try {
			ModelBean basicPolicy = application.getBean("BasicPolicy");

			// Get ProductSetup
			String productVersionIdRef = basicPolicy.gets("ProductVersionIdRef");
			ProductVersion productVersion = ProductMaster.getProductMaster("UW").getProductVersion(productVersionIdRef); 
			UWProductSetup uwProductSetup = new UWProductSetup(productVersion.getProductSetup());

			if (Log.isLoggingDebug()) {
				Log.debug("Line: " + line.readableDoc());
			}

			// Get ProductLine
			String subTypeCd = basicPolicy.gets("SubTypeCd");
			ModelBean productLine = uwProductSetup.getProductLine(subTypeCd, line.gets("LineCd"));
			/*if (productLine == null) {
       			throw new RatingException("Unable to rate. ProductLine '" + line.gets("LineCd") + "' not found in product '" + productVersionIdRef + "', sub-type '" + subTypeCd + "'");
       		}*/
			if (isApplicationReadyForRating(application)) {
				// Rate Line Coverages
				ModelBean[] productLineCoverage = productLine.getBean("ProductCoverages").getBeans("ProductCoverage");
				for( int i = 0; i < productLineCoverage.length; i++ ) {
					ModelBean coverage = BeanTools.findBeanByFieldValueAndStatus(line,"Coverage","CoverageCd",productLineCoverage[i].gets("CoverageCd"),"Active");
					if( coverage != null ) {
						// Ensure the parent bean (line or risk) is an active bean - not deleted				
						if( BeanTools.isActive(coverage, true) ){
							try {
								rateCoverage(coverage);
							} catch( RatingException re ) {
								coverageWarning(application,coverage,re.getMessage());
							}
						}
					}	
				}

				// Get ProductRisk
				ModelBean[] risk = line.findBeansByFieldValue("Risk","Status","Active");
				for( int i = 0; i < risk.length; i++ ) {

					if (Log.isLoggingDebug()) {
						Log.debug("Risk: " + risk[i].readableDoc());
					}

					String typeCd = risk[i].gets("TypeCd");
					ModelBean productRiskType = productLine.getBean("ProductRiskTypes").getBean("ProductRiskType", "RiskTypeCd", typeCd);
					if (productRiskType == null) {
						throw new RatingException("Unable to rate. ProductRiskType '" + typeCd + "' not found in product '" + productVersionIdRef + "', line '" + line.gets("LineCd") + "'");
					}

					// Rate Risk Coverages
					ModelBean[] productRiskCoverage = productRiskType.getBean("ProductCoverages").getBeans("ProductCoverage");
					for( int j = 0; j < productRiskCoverage.length; j++ ) {
						ModelBean coverage = BeanTools.findBeanByFieldValueAndStatus(risk[i],"Coverage","CoverageCd",productRiskCoverage[j].gets("CoverageCd"),"Active");
						if( coverage != null ) {
							// Ensure the parent bean (line or risk) is an active bean - not deleted				
							if( BeanTools.isActive(coverage, true) ){
								try {
									rateCoverage(coverage);
								} catch( RatingException re ) {
									coverageWarning(application,coverage,re.getMessage());
								}
							}
						}
					}
				}

				// Rate Fees
				ModelBean[] fees = application.findBeansByFieldValue("Fee","Status","Active");
				for( int i=0; i<fees.length; i++ ) {
					rateFee(fees[i]);
				}

			}       	 	
		} catch (RatingException e) {
			throw e;
		} catch (Exception e) {
			Log.error("General Error: " + e.getMessage());
			throw new RatingException(e);
		}
	}	

	/** Summarized the premiums and marks the app as rated.  Calls applicationSummary and maskAsRated
	 * @param application ModelBean
	 * @throws RatingException for errors
	 * 
	 */
	public void finalize(ModelBean application) throws RatingException {
		try {
//			if (isApplicationReadyForRating(application)) {
//				ModelBean[] risks = application.findBeansByFieldValue("Risk","Status","Active");
//				for (ModelBean risk : risks) {
//					ModelBean building = application.findBeanByFieldValue("Building","Status","Active");
//
//				}
//			}
//			
//			if (building != null ) {
//				String sqFt = building.gets("SqFt");
//				String allPerilDed = building.gets("AllPerilDed");
//				String constructionCd = building.gets("ConstructionCd");
//				if (!subTypeCd.equals("") && !sqFt.equals("") && StringTools.isNumeric(sqFt) && !allPerilDed.equals("") && !constructionCd.equals("") ) {
//
//					if(CoverageRenderer.isActive(application, "CovA")){
//						ModelBean coverage = CoverageRenderer.getActiveCoverage(application, "CovA");
//						rateCoverageA(coverage);
//					}
//					if(CoverageRenderer.isActive(application, "CovB")){
//						ModelBean coverage = CoverageRenderer.getActiveCoverage(application, "CovB");
//						rateCoverageB(coverage);
//					}
//					if(CoverageRenderer.isActive(application, "CovC")){
//						ModelBean coverage = CoverageRenderer.getActiveCoverage(application, "CovC");
//						rateCoverageC(coverage);
//					}
//					if(CoverageRenderer.isActive(application, "CovD")){
//						ModelBean coverage = CoverageRenderer.getActiveCoverage(application, "CovD");
//						rateCoverageD(coverage);
//					}
//					if(CoverageRenderer.isActive(application, "CovE")){
//						ModelBean coverage = CoverageRenderer.getActiveCoverage(application, "CovE");
//						rateCoverageE(coverage);
//					}
//					if(CoverageRenderer.isActive(application, "DP0471")){
//						ModelBean coverage = CoverageRenderer.getActiveCoverage(application, "DP0471");
//						rateOrdinanceCoverage(coverage);
//					}
//					if(CoverageRenderer.isActive(application, "DP0414")){
//						ModelBean coverage = CoverageRenderer.getActiveCoverage(application, "DP0414");
//						rateCoverageDP0414(coverage);
//					}
//					// Call the super function's finalize method
//					applicationSummary(application, true);
//					applyMinimumPremium(application);
//				}
//			}
			super.finalize(application);

		}
		catch( Exception e ) {
			Log.error("General Error: " + e.getMessage());
			throw new RatingException(e);
		}

	}

	/** Overrides the loadRates function
	 * @param productVersion Product version
	 * @throws RatingException for errors
	 */
	public void loadRates(String productVersion) throws RatingException {
		try {

			// Do nothing


		} catch (Exception e) {
			throw new RatingException(e);
		}

	}

	/** Coverage rating delegator - determines with function to delegate the rating to
	 * @param coverage Coverage modelbaen
	 * @throws RatingException for rating errors
	 */
	public void rateCoverage(ModelBean coverage) throws RatingException{
		try{


			// Delegate the rating to the appropriate function based on coverage code
			String covcode = coverage.gets("CoverageCd");
			if(covcode.equals("CovA")){
				rateCoverageA(coverage);
			}           
			else if(covcode.equals("CovB")){
				rateCoverageB(coverage);
			}
			else if(covcode.equals("CovC")){
				rateCoverageC(coverage);
			}
			else if(covcode.equals("CovD")){
				rateCoverageD(coverage);
			}
			else if(covcode.equals("CovE")){
				rateCoverageE(coverage);
			}
			else if(covcode.equals("CovL")){
				rateCoverageL(coverage);  
			}
			else if(covcode.equals("CovM")){
				rateCoverageM(coverage);  
			}
			else if(covcode.equals("VandalismMaliciousMischief")){
				rateIncludedCoverage(coverage);
			}
			else if(covcode.equals("ExtendedCoverage")){
				rateIncludedCoverage(coverage);
			}
			else if(covcode.equals("WB")){
				rateWaterBackupCoverage(coverage);
			}
			else if(covcode.equals("DP0414")){
				rateCoverageDP0414(coverage);
			}
			else if(covcode.equals("DP0471")){
				rateOrdinanceCoverage(coverage); // Check to add in rating sheet
			}
			else if(covcode.equals("EB")){
				rateEqBreakdownCoverage(coverage);
			}
			else if(covcode.equals("ELFL")){
				rateEscapedLiquidCoverage(coverage);
			}
			else{
				Log.warn("Unknown coverage: '" + covcode + "'");
			}
		}
		catch(Exception e){
			throw new RatingException(e);
		}    
	}

	public void rateIncludedCoverage(ModelBean coverage) throws RatingException, ModelBeanException {
		try {
			Log.debug("Rating " + coverage.gets("CoverageCd") + " coverage...");

			// Initialize Premium
			BigDecimal premium = zero;

			addRatingStep(coverage, coverage.gets("Description"), "", "", "", "Included");
			coverage.setValue("FullTermAmt", premium.toString());

		} catch (RatingException e) {
			Log.error(e);
			throw e;
		} catch (Exception e) {
			Log.error(e);
			throw new RatingException("Unexpected error rating coverage '" + coverage.gets("CoverageCd") + "' : " + e.toString(), e);
		}
	}
	/** Rates coverage A
	 * @param coverage Coverage modelbean
	 * @throws RatingException for rating errors
	 */

	public void rateCoverageA(ModelBean coverage) throws RatingException{
		try{
			Log.debug("Rating coverage A");
			// Initialize the premium to 0
			BigDecimal covApremium = new BigDecimal("0.00");
			covApremium = addCoverageAFireRateArea(coverage,"Building");
			if (basicPolicy.gets("SubTypeCd").equals("DP1")) {
				if(basicPolicy.gets("ExtendedCoverageInd").equals("Yes"))
					covApremium = covApremium.add(addCoverageAExtendedCoverageRateArea(coverage,"Building"));
				if (basicPolicy.gets("VandalismMaliciousMischiefInd").equals("Yes")) 
					covApremium = covApremium.add(addCoverageAVMMCoverageRateArea(coverage,"Building"));
			}else{
				covApremium = covApremium.add(addCoverageAExtendedCoverageRateArea(coverage,"Building"));
			}

			addRatingStep(coverage, "A - Dwelling Total Premium", "A - Dwelling Total Premium", "", "",covApremium);
			//covApremium = NumberTools.roundToDollar(covApremium.setScale(2, BigDecimal.ROUND_HALF_UP));
			coverage.setValue("FullTermAmt", covApremium);
			totalPremium = totalPremium.add(new BigDecimal(coverage.gets("FullTermAmt")));
			setCovABasePremium(covApremium);

		}
		catch(Exception e){
			throw new RatingException(e);
		}
	}

	/** Rates coverage A
	 * @param coverage Coverage modelbean
	 * @throws RatingException for rating errors
	 */

	public void rateCoverageB(ModelBean coverage) throws RatingException{
		try{
			Log.debug("Rating coverage B");
			// Initialize the premium to 0
			BigDecimal covApremium = new BigDecimal("0.00");
			covApremium = addCoverageAFireRateArea(coverage,"Building");
			if (basicPolicy.gets("SubTypeCd").equals("DP1")) {
				if(basicPolicy.gets("ExtendedCoverageInd").equals("Yes"))
					covApremium = covApremium.add(addCoverageAExtendedCoverageRateArea(coverage,"Building"));
				if (basicPolicy.gets("VandalismMaliciousMischiefInd").equals("Yes")) 
					covApremium = covApremium.add(addCoverageAVMMCoverageRateArea(coverage,"Building"));
			}else{
				covApremium = covApremium.add(addCoverageAExtendedCoverageRateArea(coverage,"Building"));
			}
			addRatingStep(coverage, "B - Other Structures Total Premium", "B - Other Structures Total Premium", "", "",covApremium);
			//covApremium = NumberTools.roundToDollar(covApremium.setScale(2, BigDecimal.ROUND_HALF_UP));
			coverage.setValue("FullTermAmt", covApremium);
			totalPremium = totalPremium.add(new BigDecimal(coverage.gets("FullTermAmt")));
			setCovABasePremium(covApremium);

		}
		catch(Exception e){
			throw new RatingException(e);
		}
	}

	public void rateOrdinanceCoverage(ModelBean coverage) throws RatingException{
		try{
			Log.debug("Rating coverage Ordinance");
			// Initialize the premium to 0
			BigDecimal covApremium = new BigDecimal("0.00");
			covApremium = addCoverageOrdinancFireRateArea(coverage,"Building");
			covApremium = covApremium.add(addCoverageOrdinanceExtendedCoverageRateArea(coverage,"Building"));


			addRatingStep(coverage, "Ordinance or Law Total Premium", "Ordinance or Law Total Premium", "", "",covApremium);
			//covApremium = NumberTools.roundToDollar(covApremium.setScale(2, BigDecimal.ROUND_HALF_UP));
			coverage.setValue("FullTermAmt", covApremium);
			totalPremium = totalPremium.add(new BigDecimal(coverage.gets("FullTermAmt")));
			setCovABasePremium(covApremium);

		}
		catch(Exception e){
			throw new RatingException(e);
		}
	}
	/** Rates coverage A
	 * @param coverage Coverage modelbean
	 * @throws RatingException for rating errors
	 */

	public void rateCoverageC(ModelBean coverage) throws RatingException{
		try{
			Log.debug("Rating coverage C");
			// Initialize the premium to 0
			BigDecimal covApremium = new BigDecimal("0.00");
			covApremium = addCoverageAFireRateArea(coverage,"Contents");

			if (basicPolicy.gets("SubTypeCd").equals("DP1")) {
				if(basicPolicy.gets("ExtendedCoverageInd").equals("Yes"))
					covApremium = covApremium.add(addCoverageAExtendedCoverageRateArea(coverage,"Contents"));
				if (basicPolicy.gets("VandalismMaliciousMischiefInd").equals("Yes")) 
					covApremium = covApremium.add(addCoverageAVMMCoverageRateArea(coverage,"Contents"));
			}else{
				covApremium = covApremium.add(addCoverageAExtendedCoverageRateArea(coverage,"Contents"));
			}
			addRatingStep(coverage, "C - Personal Property Total Premium", "C - Personal Property Total Premium", "", "",covApremium);
			//covApremium = NumberTools.roundToDollar(covApremium.setScale(2, BigDecimal.ROUND_HALF_UP));
			coverage.setValue("FullTermAmt", covApremium);
			totalPremium = totalPremium.add(new BigDecimal(coverage.gets("FullTermAmt")));
			setCovABasePremium(covApremium);

		}
		catch(Exception e){
			throw new RatingException(e);
		}
	}
	/** Rates coverage A
	 * @param coverage Coverage modelbean
	 * @throws RatingException for rating errors
	 */

	public void rateCoverageD(ModelBean coverage) throws RatingException{
		try{
			Log.debug("Rating coverage B");
			// Initialize the premium to 0
			BigDecimal covApremium = new BigDecimal("0.00");
			covApremium = addCoverageAFireRateArea(coverage,"Building");
			if (basicPolicy.gets("SubTypeCd").equals("DP1")) {
				if(basicPolicy.gets("ExtendedCoverageInd").equals("Yes"))
					covApremium = covApremium.add(addCoverageAExtendedCoverageRateArea(coverage,"Building"));
				if (basicPolicy.gets("VandalismMaliciousMischiefInd").equals("Yes")) 
					covApremium = covApremium.add(addCoverageAVMMCoverageRateArea(coverage,"Building"));
			}else{
				covApremium = covApremium.add(addCoverageAExtendedCoverageRateArea(coverage,"Building"));
			}

			addRatingStep(coverage, "D - Fair Rental Value Total Premium", "D - Fair Rental Value Total Premium", "", "",covApremium);
			//covApremium = NumberTools.roundToDollar(covApremium.setScale(2, BigDecimal.ROUND_HALF_UP));
			coverage.setValue("FullTermAmt", covApremium);
			totalPremium = totalPremium.add(new BigDecimal(coverage.gets("FullTermAmt")));
			setCovABasePremium(covApremium);

		}
		catch(Exception e){
			throw new RatingException(e);
		}
	}
	/** Rates coverage A
	 * @param coverage Coverage modelbean
	 * @throws RatingException for rating errors
	 */

	public void rateCoverageE(ModelBean coverage) throws RatingException{
		try{
			Log.debug("Rating coverage E");
			// Initialize the premium to 0
			BigDecimal covApremium = new BigDecimal("0.00");
			covApremium = addCoverageAFireRateArea(coverage,"Building");
			if (basicPolicy.gets("SubTypeCd").equals("DP1")) {
				if(basicPolicy.gets("ExtendedCoverageInd").equals("Yes"))
					covApremium = covApremium.add(addCoverageAExtendedCoverageRateArea(coverage,"Building"));
				if (basicPolicy.gets("VandalismMaliciousMischiefInd").equals("Yes")) 
					covApremium = covApremium.add(addCoverageAVMMCoverageRateArea(coverage,"Building"));
			}else{
				covApremium = covApremium.add(addCoverageAExtendedCoverageRateArea(coverage,"Building"));
			}

			addRatingStep(coverage, "E - Additional Living Expenses Total Premium", "E - Additional Living Expenses Total Premium", "", "",covApremium);
			//covApremium = NumberTools.roundToDollar(covApremium.setScale(2, BigDecimal.ROUND_HALF_UP));
			coverage.setValue("FullTermAmt", covApremium);
			totalPremium = totalPremium.add(new BigDecimal(coverage.gets("FullTermAmt")));
			setCovABasePremium(covApremium);

		}
		catch(Exception e){
			throw new RatingException(e);
		}
	}
	/** Rates coverage A
	 * @param coverage Coverage modelbean
	 * @throws RatingException for rating errors
	 */

	public void rateCoverageDP0414(ModelBean coverage) throws RatingException{
		try{
			Log.debug("Rating coverage DP0414");
			// Initialize the premium to 0
			BigDecimal covApremium = new BigDecimal("0.00");
			covApremium = addCoverageAFireRateArea(coverage,"Building");
			if (basicPolicy.gets("SubTypeCd").equals("DP1")) {
				if(basicPolicy.gets("ExtendedCoverageInd").equals("Yes"))
					covApremium = covApremium.add(addCoverageAExtendedCoverageRateArea(coverage,"Building"));
				if (basicPolicy.gets("VandalismMaliciousMischiefInd").equals("Yes")) 
					covApremium = covApremium.add(addCoverageAVMMCoverageRateArea(coverage,"Building"));
			}else{
				covApremium = covApremium.add(addCoverageAExtendedCoverageRateArea(coverage,"Building"));
			}

			addRatingStep(coverage, "Additional Living Expenses Total Premium", "Additional Living Expenses Total Premium", "", "",covApremium);
			//covApremium = NumberTools.roundToDollar(covApremium.setScale(2, BigDecimal.ROUND_HALF_UP));
			coverage.setValue("FullTermAmt", covApremium);
			totalPremium = totalPremium.add(new BigDecimal(coverage.gets("FullTermAmt")));
			setCovABasePremium(covApremium);

		}
		catch(Exception e){
			throw new RatingException(e);
		}
	}
	private BigDecimal addCoverageAFireRateArea(ModelBean coverage, String category) throws Exception, ModelBeanException {
		ModelBean fireRateArea = addRateArea(coverage,"Fire","Fire");
		BigDecimal covApremium = new BigDecimal("0.00");
		BigDecimal covLimit = new BigDecimal("0.00");
		BigDecimal premium = new BigDecimal(0);
		String lookUpZipCode = "";
		RatingTable table = null;
		String policyForm = basicPolicy.gets("SubTypeCd");
		BigDecimal factor = zero;
		BigDecimal coverageAPremium = zero;
		Row[] rows = null;
		ModelBean building = getBuildingFromCoverage(coverage);
		if(coverage.gets("CoverageCd").equals("CovA")){

			String 	fullzipCode =  building.findBeanByFieldValue("Addr", "AddrTypeCd", "RiskAddr").gets("PostalCode");
			if(fullzipCode !=null && fullzipCode.length() > 5){
				lookUpZipCode = fullzipCode.substring(0,5);
			}else{
				lookUpZipCode = fullzipCode;
			}
			//Get the BasePremium Worksheet by RiskFactor 
			table = tables.get("BasePremium");
			Row row = table.lookupSingleRow("RiskFactor","Fire");
			String wsName = row.getCell("WorksheetName").getString();

			// Start Coverage A Premium
			// Get the BasePremium value by Territory, Building/Contents
			table = tables.get(wsName);

			rows = table.lookup(new String[]{"Territory", "Category"}, new String[]{lookUpZipCode, category});    	    	

			BigDecimal keyBasePremium = new BigDecimal(0);
			if (rows!=null && rows.length>0 && rows[0]!=null ) {
				keyBasePremium =  rows[0].getCell(policyForm).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);    		
			}    	
			//Get the Coverage A factor
			table = tables.get("CovAFactor");	
			covLimit = new BigDecimal(building.gets("CovALimit"));
			setCovALimit(covLimit);
			factor = table.getRate(covLimit, "CovALimit", "Fire", RatingTable.RATE_CALC_INTERPOLATE, 4);
			coverageAPremium = keyBasePremium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			premium = premium.add(coverageAPremium).setScale(2, BigDecimal.ROUND_HALF_UP);

			//ModelBean covA = addRatingStep(step, "Coverage A Premium", "", "", "" ,premium.setScale(2, BigDecimal.ROUND_HALF_UP));
			addRatingStep(fireRateArea, "Fire Key Dwelling Premium", "Policy Form: " +policyForm +" Territory: " +lookUpZipCode, "", "", keyBasePremium);
			addRatingStep(fireRateArea, "Coverage A Factor", "Cov A: "+fmt.format(covLimit), "x",factor ,coverageAPremium.setScale(2, BigDecimal.ROUND_HALF_UP));
		}else if(coverage.gets("CoverageCd").equals("CovB")){
			// Start Increased Coverage B Premium
			//Get Increased Cov B Limit Factor - included
			covLimit = new BigDecimal(building.gets("CovBLimitIncrease"));
			factor = new BigDecimal(0.0);
			if (covLimit.doubleValue() > 0) {
				BigDecimal divisorVal = new BigDecimal("1000.00");
				factor = covLimit.divide(divisorVal).setScale(2, BigDecimal.ROUND_HALF_UP);
			} 
			//Get Increased Cov B Limit Rate    
			BigDecimal rate = null;
			table = tables.get("IncBDE");	
			Row row = table.lookupSingleRow("Coverage","Fire");
			rate = row.getCell(policyForm).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
			BigDecimal incrCovBPremium =  rate.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			premium = premium.add(incrCovBPremium).setScale(2, BigDecimal.ROUND_HALF_UP);
			addRatingStep(fireRateArea, "Increased Cov B Limit Factor", "Increased Cov B Limit/1,000: "+fmt.format(covLimit), "", "", factor);
			addRatingStep(fireRateArea, "Increased Cov B Limit Rate ", "Rate: "+rate, "x", rate, incrCovBPremium.setScale(2, BigDecimal.ROUND_HALF_UP));
		}else if(coverage.gets("CoverageCd").equals("CovD")){

			// Start Increased Coverage D Premium
			//Get Increased Cov D Limit Factor - included
			covLimit = new BigDecimal(building.gets("CovDLimitIncrease"));
			factor = new BigDecimal(0.0);
			if (covLimit.doubleValue() > 0) {
				BigDecimal divisorVal = new BigDecimal("1000.00");
				factor = covLimit.divide(divisorVal).setScale(2, BigDecimal.ROUND_HALF_UP);
			} 
			//Get Increased Cov D Limit Rate
			BigDecimal rate = null;
			table = tables.get("IncBDE");	
			Row row = table.lookupSingleRow("Coverage","Fire");
			rate = row.getCell(policyForm).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);

			BigDecimal incrCovDPremium =  rate.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			premium = premium.add(incrCovDPremium).setScale(2, BigDecimal.ROUND_HALF_UP);
			addRatingStep(fireRateArea, "Increased Cov D Limit Factor", "Increased Cov D Limit/1,000: "+fmt.format(covLimit), "", "", factor);
			addRatingStep(fireRateArea, "Increased Cov D Limit Rate ", "Rate: "+rate, "x", rate, incrCovDPremium.setScale(2, BigDecimal.ROUND_HALF_UP));

		}else if(coverage.gets("CoverageCd").equals("CovE") || coverage.gets("CoverageCd").equals("DP0414")){
			// Start Increased Coverage E Premium
			//Get Increased Cov E Limit Factor - included
			covLimit = new BigDecimal(building.gets("CovELimitIncrease"));
			factor = new BigDecimal(0.0);
			if (covLimit.doubleValue() > 0) {
				BigDecimal divisorVal = new BigDecimal("1000.00");
				factor = covLimit.divide(divisorVal).setScale(2, BigDecimal.ROUND_HALF_UP);
			} 
			//Get Increased Cov E Limit Rate 
			BigDecimal rate = null;
			table = tables.get("IncBDE");	
			Row row = table.lookupSingleRow("Coverage","Fire");
			rate = row.getCell(policyForm).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
			BigDecimal incrCovEPremium =  rate.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			premium = premium.add(incrCovEPremium).setScale(2, BigDecimal.ROUND_HALF_UP);
			addRatingStep(fireRateArea, "Increased Cov E Limit Factor", "Increased Cov E Limit/1,000:  "+fmt.format(covLimit), "", "", factor);
			addRatingStep(fireRateArea, "Increased Cov E Limit Rate ", "Rate: "+rate, "x", rate, incrCovEPremium.setScale(2, BigDecimal.ROUND_HALF_UP));

		}else if(coverage.gets("CoverageCd").equals("CovC") ){
			String 	fullzipCode =  building.findBeanByFieldValue("Addr", "AddrTypeCd", "RiskAddr").gets("PostalCode");
			if(fullzipCode !=null && fullzipCode.length() > 5){
				lookUpZipCode = fullzipCode.substring(0,5);
			}else if(fullzipCode !=null){
				lookUpZipCode = fullzipCode;
			}
			//Get the BasePremium Worksheet by RiskFactor 
			table = tables.get("BasePremium");
			Row row = table.lookupSingleRow("RiskFactor","Fire");
			String wsName = row.getCell("WorksheetName").getString();

			// Start Coverage A Premium
			// Get the BasePremium value by Territory, Building/Contents
			table = tables.get(wsName);
			rows = new Row[1];
			rows = table.lookup(new String[]{"Territory", "Category"}, new String[]{lookUpZipCode, category});    	    	
			BigDecimal keyBasePremium = new BigDecimal(0);
			if(rows!=null && rows.length>0 && rows[0]!=null){
				keyBasePremium =  rows[0].getCell(policyForm).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
			}    	
			premium = keyBasePremium;
			addRatingStep(fireRateArea, "Fire" + " Key " + category + " Premium ", "Policy Form: " +policyForm +" Territory: " +lookUpZipCode, "", "", keyBasePremium);

			//Get the Coverage C factor
			table = tables.get("CovCFactor");	
			covLimit = new BigDecimal(building.gets("CovCLimit"));
			factor = new BigDecimal(0.00);
			if (covLimit.doubleValue() != 0 ) 
				factor = table.getRate(covLimit, "CovCLimit", "Fire", RatingTable.RATE_CALC_INTERPOLATE, 4);
			premium = premium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			addRatingStep(fireRateArea, "Coverage C Factor", "Cov C: "+fmt.format(covLimit), "x",factor, premium.setScale(2, BigDecimal.ROUND_HALF_UP));

		}


		// Get Num of Units/Families Factor
		String numOfUnits = building.gets("Units");
		table = tables.get("NumFamily");
		String lookupStr = "Fire"+category;
		String riskFactorVal = "Fire";
		rows = table.lookup(new String[]{"Category", "NumOfUnits"}, new String[]{riskFactorVal, numOfUnits});    	
		factor  = rows[0].getCell(category).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
		premium = premium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
		addRatingStep(fireRateArea, "# of Units/Families Factor", "# of Units/Families: "+numOfUnits, "x", factor, premium.setScale(2, BigDecimal.ROUND_HALF_UP) );

		// Get Seasonal/Non-Seasonal Factor
		String residence = building.gets("Residence");
		table = tables.get("SeasonalNon");
		if (!residence.equalsIgnoreCase("Seasonal")) {
			residence = "NonSeasonal";
		} else {
			residence = "Seasonal";
		}
		rows = table.lookup(new String[]{"Category", "Form"}, new String[]{residence, policyForm});        	
		factor  = rows[0].getCell(lookupStr).getBigDecimal().setScale(3,BigDecimal.ROUND_HALF_UP);
		premium = premium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
		addRatingStep(fireRateArea, "Seasonal/Non-Seasonal Factor", "Residence: "+residence, "x", factor, premium.setScale(2, BigDecimal.ROUND_HALF_UP) );

		// Get Occupancy Factor
		String occupancyCd = building.gets("OccupancyCd");
		table = tables.get("Occupancy");
		Row row = table.lookupSingleRow("Category",riskFactorVal);

		factor  = row.getCell(occupancyCd).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
		premium = premium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
		addRatingStep(fireRateArea, "Occupancy Factor", "Occupancy: "+occupancyCd, "x", factor, premium.setScale(2, BigDecimal.ROUND_HALF_UP) );

		// Get Protection Class/Construction Type Factor
		String protectionClass = building.gets("ProtectionClass");
		String constructionCd = building.gets("ConstructionCd");
		String lookUpCode = protectionClass+constructionCd;
		table = tables.get("ProtConstClass"+"Fire");
		rows = table.lookup(new String[]{"PROTCONST", "Category"}, new String[]{lookUpCode, category});    
		factor  = rows[0].getCell("Factor").getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
		premium = premium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
		addRatingStep(fireRateArea, "Protection Class/Construction Type Factor", "Protection Class: "+protectionClass +" Construction Type: "+constructionCd, "x",factor.setScale(2, BigDecimal.ROUND_HALF_UP ) ,premium.setScale(2, BigDecimal.ROUND_HALF_UP ) );
		if(policyForm.equalsIgnoreCase("DP1")  && !coverage.gets("CoverageCd").equals("CovC")){
			table = tables.get("DwellingUnderConst");
			if(building.gets("UnderConstructionOrRenovation").equals("") || building.gets("UnderConstructionOrRenovation").equals("No")){
				row = table.lookupSingleRow("Coverage","No");
			}else{
				row = table.lookupSingleRow("Coverage","Yes");
			}
			if(row!=null){
				factor  = row.getCell("Factor").getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
				premium = premium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
				addRatingStep(fireRateArea, "Dwelling Under Construction Coverage Factor", "", "x", factor, premium.setScale(2, BigDecimal.ROUND_HALF_UP) );
			}
		}
		if(coverage.gets("CoverageCd").equals("CovA")){
			setCovAPremFireOrdinance(premium);
		}else if(coverage.gets("CoverageCd").equals("CovB")){
			setCovBPremFireOrdinance(premium);
		}else if(coverage.gets("CoverageCd").equals("CovD")){
			setCovDPremFireOrdinance(premium);
		}else if(coverage.gets("CoverageCd").equals("CovE")){
			setCovEPremFireOrdinance(premium);
		}else if(coverage.gets("CoverageCd").equals("CovC")){
			setCovCPremFireOrdinance(premium);
		}

		BigDecimal subTotal = premium;
		addRatingStep(fireRateArea, "Premium Subtotal" , "", "", "", subTotal);
		covApremium = applyDeductiblePremium(fireRateArea,subTotal, "Fire", building.gets("AllPerilDed"));
		if(coverage.gets("CoverageCd").equals("CovA")){
			setCovAAOPPremFire(covApremium);
		}else if(coverage.gets("CoverageCd").equals("CovB")){
			setCovBAOPPremFire(covApremium);
		}else if(coverage.gets("CoverageCd").equals("CovD")){
			setCovDAOPPremFire(covApremium);
		}else if(coverage.gets("CoverageCd").equals("CovE")){
			setCovEAOPPremFire(covApremium);
		}else if(coverage.gets("CoverageCd").equals("CovC")){
			setCovCAOPPremFire(covApremium);
		}else if(coverage.gets("CoverageCd").equals("DP0414")){
			setCovAddlAOPPremFire(covApremium); //DP0414
		}

		covApremium = applyMaximiumFireAllOtherPerilsDeductibleCredit(fireRateArea, "FIRE", subTotal,coverage.gets("CoverageCd"),covApremium);
		ModelBean totalCreditstep = addRatingStep(fireRateArea, "Total Credits", "", "x", "","");
		covApremium = applyCreditsPremium(totalCreditstep, covApremium, true, building);

		covApremium = applyLossSurchargePremium(fireRateArea, covApremium);
		covApremium = applyGroupDiscountPremium(fireRateArea, covApremium);
		if( StringRenderer.in(coverage.gets("CoverageCd"), "CovA,CovB,CovC,CovD,CovE,DP1914,DP0471,DP0414,DL2480")){
			fireRateArea.setValue("AnnualStatementLineCd","010");
		}		
		fireRateArea.setValue("FullTermAmt",covApremium.toString());
		return covApremium;
	}


	private ModelBean getBuildingFromCoverage(ModelBean coverage)
			throws ModelBeanException {
		ModelBean risk = coverage.getParentBean();
		ModelBean building = risk.findBeanByFieldValue("Building", "Status", "Active");
		return building;
	}

	private BigDecimal addCoverageAExtendedCoverageRateArea(ModelBean coverage, String category) throws Exception, ModelBeanException {
		ModelBean extendedCoverageRateArea = addRateArea(coverage,"Extended Coverage","Extended Coverage");
		BigDecimal covApremium = new BigDecimal("0.00");
		BigDecimal covLimit = new BigDecimal("0.00");
		BigDecimal premium = new BigDecimal(0);
		String lookUpZipCode = "";
		RatingTable table = null;
		String policyForm = basicPolicy.gets("SubTypeCd");
		BigDecimal factor = zero;
		BigDecimal coverageAPremium = zero;
		Row[] rows = null;
		ModelBean building = getBuildingFromCoverage(coverage);
		if(coverage.gets("CoverageCd").equals("CovA")){
			String 	fullzipCode =  building.findBeanByFieldValue("Addr", "AddrTypeCd", "RiskAddr").gets("PostalCode");
			if(fullzipCode !=null && fullzipCode.length() > 5){
				lookUpZipCode = fullzipCode.substring(0,5);
			}else{
				lookUpZipCode = fullzipCode;
			}
			//Get the BasePremium Worksheet by RiskFactor 
			table = tables.get("BasePremium");
			Row row = table.lookupSingleRow("RiskFactor","EC");
			String wsName = row.getCell("WorksheetName").getString();

			// Start Coverage A Premium
			// Get the BasePremium value by Territory, Building/Contents
			table = tables.get(wsName);
			rows = null;
			rows = table.lookup(new String[]{"Territory", "Category"}, new String[]{lookUpZipCode, category});    	    	
			BigDecimal keyBasePremium = new BigDecimal(0);
			if (rows!=null && rows.length>0 && rows[0]!=null ) {
				keyBasePremium =  rows[0].getCell(policyForm).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);    		
			}    	
			//Get the Coverage A factor
			table = tables.get("CovAFactor");	
			covLimit = new BigDecimal(building.gets("CovALimit"));
			setCovALimit(covLimit);
			factor = table.getRate(covLimit, "CovALimit", "EC", RatingTable.RATE_CALC_INTERPOLATE, 4);
			coverageAPremium = keyBasePremium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			premium = premium.add(coverageAPremium).setScale(2, BigDecimal.ROUND_HALF_UP);
			//ModelBean covA = addRatingStep(step, "Coverage A Premium", "", "", "" ,premium.setScale(2, BigDecimal.ROUND_HALF_UP));
			addRatingStep(extendedCoverageRateArea, "Extended Coverage Key Dwelling Premium", "Policy Form: " +policyForm +" Territory: " +lookUpZipCode, "", "", keyBasePremium);
			addRatingStep(extendedCoverageRateArea, "Coverage A Factor", "Cov A: "+fmt.format(covLimit), "x",factor ,coverageAPremium.setScale(2, BigDecimal.ROUND_HALF_UP));
		}else if(coverage.gets("CoverageCd").equals("CovB")){
			// Start Increased Coverage B Premium
			//Get Increased Cov B Limit Factor - included
			covLimit = new BigDecimal(building.gets("CovBLimitIncrease"));
			factor = new BigDecimal(0.0);
			if (covLimit.doubleValue() > 0) {
				BigDecimal divisorVal = new BigDecimal("1000.00");
				factor = covLimit.divide(divisorVal).setScale(2, BigDecimal.ROUND_HALF_UP);
			} 
			//Get Increased Cov B Limit Rate    
			BigDecimal rate = null;
			table = tables.get("IncBDE");	
			Row row = table.lookupSingleRow("Coverage","EC");
			rate = row.getCell(policyForm).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
			BigDecimal incrCovBPremium =  rate.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			premium = premium.add(incrCovBPremium).setScale(2, BigDecimal.ROUND_HALF_UP);
			addRatingStep(extendedCoverageRateArea, "Increased Cov B Limit Factor", "Increased Cov B Limit/1,000: "+fmt.format(covLimit), "", "", factor);
			addRatingStep(extendedCoverageRateArea, "Increased Cov B Limit Rate ", "Rate: "+rate, "x", rate, incrCovBPremium.setScale(2, BigDecimal.ROUND_HALF_UP));
		}else if(coverage.gets("CoverageCd").equals("CovD")){

			// Start Increased Coverage D Premium
			//Get Increased Cov D Limit Factor - included
			covLimit = new BigDecimal(building.gets("CovDLimitIncrease"));
			factor = new BigDecimal(0.0);
			if (covLimit.doubleValue() > 0) {
				BigDecimal divisorVal = new BigDecimal("1000.00");
				factor = covLimit.divide(divisorVal).setScale(2, BigDecimal.ROUND_HALF_UP);
			} 
			//Get Increased Cov D Limit Rate
			BigDecimal rate = null;
			table = tables.get("IncBDE");	
			Row row = table.lookupSingleRow("Coverage","EC");
			rate = row.getCell(policyForm).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);

			BigDecimal incrCovDPremium =  rate.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			premium = premium.add(incrCovDPremium).setScale(2, BigDecimal.ROUND_HALF_UP);
			addRatingStep(extendedCoverageRateArea, "Increased Cov D Limit Factor", "Increased Cov D Limit/1,000: "+fmt.format(covLimit), "", "", factor);
			addRatingStep(extendedCoverageRateArea, "Increased Cov D Limit Rate ", "Rate: "+rate, "x", rate, incrCovDPremium.setScale(2, BigDecimal.ROUND_HALF_UP));

		}else if(coverage.gets("CoverageCd").equals("CovE") || coverage.gets("CoverageCd").equals("DP0414")){
			// Start Increased Coverage E Premium
			//Get Increased Cov E Limit Factor - included
			covLimit = new BigDecimal(building.gets("CovELimitIncrease"));
			factor = new BigDecimal(0.0);
			if (covLimit.doubleValue() > 0) {
				BigDecimal divisorVal = new BigDecimal("1000.00");
				factor = covLimit.divide(divisorVal).setScale(2, BigDecimal.ROUND_HALF_UP);
			} 
			//Get Increased Cov E Limit Rate 
			BigDecimal rate = null;
			table = tables.get("IncBDE");	
			Row row = table.lookupSingleRow("Coverage","EC");
			rate = row.getCell(policyForm).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
			BigDecimal incrCovEPremium =  rate.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			premium = premium.add(incrCovEPremium).setScale(2, BigDecimal.ROUND_HALF_UP);
			addRatingStep(extendedCoverageRateArea, "Increased Cov E Limit Factor", "Increased Cov E Limit/1,000: "+fmt.format(covLimit), "", "", factor);
			addRatingStep(extendedCoverageRateArea, "Increased Cov E Limit Rate ", "Rate: "+rate, "x", rate, incrCovEPremium.setScale(2, BigDecimal.ROUND_HALF_UP));

		}else if(coverage.gets("CoverageCd").equals("CovC") ){
			String 	fullzipCode =  building.findBeanByFieldValue("Addr", "AddrTypeCd", "RiskAddr").gets("PostalCode");
			if(fullzipCode !=null && fullzipCode.length() > 5){
				lookUpZipCode = fullzipCode.substring(0,5);
			}else if(fullzipCode !=null){
				lookUpZipCode = fullzipCode;
			}
			//Get the BasePremium Worksheet by RiskFactor 
			table = tables.get("BasePremium");
			Row row = table.lookupSingleRow("RiskFactor","EC");
			String wsName = row.getCell("WorksheetName").getString();

			// Start Coverage A Premium
			// Get the BasePremium value by Territory, Building/Contents
			table = tables.get(wsName);
			rows = new Row[1];
			rows = table.lookup(new String[]{"Territory", "Category"}, new String[]{lookUpZipCode, category});    	    	
			BigDecimal keyBasePremium = new BigDecimal(0);
			if(rows!=null && rows.length>0 && rows[0]!=null){
				keyBasePremium =  rows[0].getCell(policyForm).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
			}    	
			premium = keyBasePremium;
			addRatingStep(extendedCoverageRateArea, "EC" + " Key " + category + " Premium ", "Policy Form: " +policyForm +" Territory: " +lookUpZipCode, "", "", keyBasePremium);

			//Get the Coverage C factor
			table = tables.get("CovCFactor");	
			covLimit = new BigDecimal(building.gets("CovCLimit"));
			factor = new BigDecimal(0.00);
			if (covLimit.doubleValue() != 0 ) 
				factor = table.getRate(covLimit, "CovCLimit", "EC", RatingTable.RATE_CALC_INTERPOLATE, 4);
			premium = premium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			addRatingStep(extendedCoverageRateArea, "Coverage C Factor", "Cov C: "+fmt.format(covLimit), "x",factor, premium.setScale(2, BigDecimal.ROUND_HALF_UP));

		}
		// Get Num of Units/Families Factor
		String numOfUnits = building.gets("Units");
		table = tables.get("NumFamily");
		String lookupStr = "EC"+category;
		String riskFactorVal = "ECVMM";
		rows = table.lookup(new String[]{"Category", "NumOfUnits"}, new String[]{riskFactorVal, numOfUnits});    	
		factor  = rows[0].getCell(category).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
		premium = premium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
		addRatingStep(extendedCoverageRateArea, "# of Units/Families Factor", "# of Units/Families: "+numOfUnits, "x", factor, premium.setScale(2, BigDecimal.ROUND_HALF_UP) );

		// Get Seasonal/Non-Seasonal Factor
		String residence = building.gets("Residence");
		table = tables.get("SeasonalNon");
		if (!residence.equalsIgnoreCase("Seasonal")) {
			residence = "NonSeasonal";
		} else {
			residence = "Seasonal";
		}
		rows = table.lookup(new String[]{"Category", "Form"}, new String[]{residence, policyForm});        	
		factor  = rows[0].getCell(lookupStr).getBigDecimal().setScale(3,BigDecimal.ROUND_HALF_UP);
		premium = premium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
		addRatingStep(extendedCoverageRateArea, "Seasonal/Non-Seasonal Factor", "Residence: "+residence, "x", factor, premium.setScale(2, BigDecimal.ROUND_HALF_UP) );

		// Get Occupancy Factor
		String occupancyCd = building.gets("OccupancyCd");
		table = tables.get("Occupancy");
		Row row = table.lookupSingleRow("Category",riskFactorVal);

		factor  = row.getCell(occupancyCd).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
		premium = premium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
		addRatingStep(extendedCoverageRateArea, "Occupancy Factor", "Occupancy: "+occupancyCd, "x", factor, premium.setScale(2, BigDecimal.ROUND_HALF_UP) );

		// Get Protection Class/Construction Type Factor
		String protectionClass = building.gets("ProtectionClass");
		String constructionCd = building.gets("ConstructionCd");
		String lookUpCode = protectionClass+constructionCd;
		table = tables.get("ProtConstClass"+"ECVMM");
		rows = table.lookup(new String[]{"PROTCONST", "Category"}, new String[]{lookUpCode, category});    
		factor  = rows[0].getCell("Factor").getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
		premium = premium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
		addRatingStep(extendedCoverageRateArea, "Protection Class/Construction Type Factor", "Protection Class: "+protectionClass +" Construction Type: "+constructionCd, "x",factor.setScale(2, BigDecimal.ROUND_HALF_UP ) ,premium.setScale(2, BigDecimal.ROUND_HALF_UP ) );

		if(coverage.gets("CoverageCd").equals("CovA")){
			setCovAPremECOrdinance(premium);
		}else if(coverage.gets("CoverageCd").equals("CovB")){
			setCovBPremECOrdinance(premium);
		}else if(coverage.gets("CoverageCd").equals("CovD")){
			setCovDPremECOrdinance(premium);
		}else if(coverage.gets("CoverageCd").equals("CovE")){
			setCovEPremECOrdinance(premium);
		}else if(coverage.gets("CoverageCd").equals("CovC")){
			setCovCPremECOrdinance(premium);
		}

		if(!building.gets("WindHailDed").equals("None") && !building.gets("WindHailDed").equals("")){
			covApremium = applyWindHailDeductiblePremium(extendedCoverageRateArea, premium,"PerilWinDed",building.gets("WindHailDed"), building.gets("AllPerilDed"));
		}else{
			covApremium=premium;
		}
		BigDecimal subTotal = covApremium;
		addRatingStep(extendedCoverageRateArea, "Premium Subtotal" , "", "", "", subTotal);
		covApremium = applyDeductiblePremium(extendedCoverageRateArea,covApremium, "ECVMM", building.gets("AllPerilDed"));
		if(coverage.gets("CoverageCd").equals("CovA")){
			setCovAAOPPremEc(covApremium);
		}else if(coverage.gets("CoverageCd").equals("CovB")){
			setCovBAOPPremEc(covApremium);
		}else if(coverage.gets("CoverageCd").equals("CovD")){
			setCovDAOPPremEc(covApremium);
		}else if(coverage.gets("CoverageCd").equals("CovE")){
			setCovEAOPPremEc(covApremium);
		}else if(coverage.gets("CoverageCd").equals("CovC")){
			setCovCAOPPremEc(covApremium);
		}else if(coverage.gets("CoverageCd").equals("DP0414")){
			setCovAddlAOPPremEc(covApremium); //DP0414
		}
		covApremium = applyMaximiumFireAllOtherPerilsDeductibleCredit(extendedCoverageRateArea, "EC", subTotal, coverage.gets("CoverageCd"),covApremium);
		ModelBean totalCreditstep = addRatingStep(extendedCoverageRateArea, "Total Credits", "", "x", "","");
		covApremium = applyCreditsPremium(totalCreditstep, covApremium, false, building);
		String 	fullZipCode =  building.findBeanByFieldValue("Addr", "AddrTypeCd", "RiskAddr").gets("PostalCode");
		covApremium = applyCoastalReinsuranceFactor(extendedCoverageRateArea, covApremium, fullZipCode);
		covApremium = applyLossSurchargePremium(extendedCoverageRateArea, covApremium);
		covApremium = applyGroupDiscountPremium(extendedCoverageRateArea, covApremium);
		extendedCoverageRateArea.setValue("FullTermAmt",covApremium.toString());
		if( StringRenderer.in(coverage.gets("CoverageCd"), "CovA,CovB,CovC,CovD,CovE,DP1914,DP0471,DP0414,DL2480")){
			extendedCoverageRateArea.setValue("AnnualStatementLineCd","021");
		}
		return covApremium;
	}
	private BigDecimal addCoverageAVMMCoverageRateArea(ModelBean coverage, String category) throws Exception, ModelBeanException {
		ModelBean vmmRateArea = addRateArea(coverage,"VMM","VMM");
		BigDecimal covApremium = new BigDecimal("0.00");
		BigDecimal covLimit = new BigDecimal("0.00");
		BigDecimal premium = new BigDecimal(0);
		String lookUpZipCode = "";
		RatingTable table = null;
		String policyForm = basicPolicy.gets("SubTypeCd");
		BigDecimal factor = zero;
		BigDecimal coverageAPremium = zero;
		Row[] rows = null;
		ModelBean building = getBuildingFromCoverage(coverage);
		if(coverage.gets("CoverageCd").equals("CovA")){
			String 	fullzipCode =  building.findBeanByFieldValue("Addr", "AddrTypeCd", "RiskAddr").gets("PostalCode");
			if(fullzipCode !=null && fullzipCode.length() > 5){
				lookUpZipCode = fullzipCode.substring(0,5);
			}else{
				lookUpZipCode = fullzipCode;
			}
			//Get the BasePremium Worksheet by RiskFactor 
			table = tables.get("BasePremium");
			Row row = table.lookupSingleRow("RiskFactor","VMM");
			String wsName = row.getCell("WorksheetName").getString();

			// Start Coverage A Premium
			// Get the BasePremium value by Territory, Building/Contents
			table = tables.get(wsName);
			rows = null;
			rows = table.lookup(new String[]{"Territory", "Category"}, new String[]{lookUpZipCode, category});    	    	
			BigDecimal keyBasePremium = new BigDecimal(0);
			if (rows!=null && rows.length>0 && rows[0]!=null ) {
				keyBasePremium =  rows[0].getCell(policyForm).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);    		
			}    	
			//Get the Coverage A factor
			table = tables.get("CovAFactor");	
			covLimit = new BigDecimal(building.gets("CovALimit"));
			setCovALimit(covLimit);
			factor = table.getRate(covLimit, "CovALimit", "VMM", RatingTable.RATE_CALC_INTERPOLATE, 4);
			coverageAPremium = keyBasePremium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			premium = premium.add(coverageAPremium).setScale(2, BigDecimal.ROUND_HALF_UP);
			//ModelBean covA = addRatingStep(step, "Coverage A Premium", "", "", "" ,premium.setScale(2, BigDecimal.ROUND_HALF_UP));
			addRatingStep(vmmRateArea, "VMM Key Dwelling Premium", "Policy Form: " +policyForm +" Territory: " +lookUpZipCode, "", "", keyBasePremium);
			addRatingStep(vmmRateArea, "Coverage A Factor", "Cov A: "+fmt.format(covLimit), "x",factor ,coverageAPremium.setScale(2, BigDecimal.ROUND_HALF_UP));
		}else if(coverage.gets("CoverageCd").equals("CovB")){
			// Start Increased Coverage B Premium
			//Get Increased Cov B Limit Factor - included
			covLimit = new BigDecimal(building.gets("CovBLimitIncrease"));
			factor = new BigDecimal(0.0);
			if (covLimit.doubleValue() > 0) {
				BigDecimal divisorVal = new BigDecimal("1000.00");
				factor = covLimit.divide(divisorVal).setScale(2, BigDecimal.ROUND_HALF_UP);
			} 
			//Get Increased Cov B Limit Rate    
			BigDecimal rate = null;
			table = tables.get("IncBDE");	
			Row row = table.lookupSingleRow("Coverage","VMM");
			rate = row.getCell(policyForm).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
			BigDecimal incrCovBPremium =  rate.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			premium = premium.add(incrCovBPremium).setScale(2, BigDecimal.ROUND_HALF_UP);
			addRatingStep(vmmRateArea, "Increased Cov B Limit Factor", "Increased Cov B Limit/1,000: "+fmt.format(covLimit), "", "", factor);
			addRatingStep(vmmRateArea, "Increased Cov B Limit Rate ", "Rate: "+rate, "x", rate, incrCovBPremium.setScale(2, BigDecimal.ROUND_HALF_UP));
		}else if(coverage.gets("CoverageCd").equals("CovD")){

			// Start Increased Coverage D Premium
			//Get Increased Cov D Limit Factor - included
			covLimit = new BigDecimal(building.gets("CovDLimitIncrease"));
			factor = new BigDecimal(0.0);
			if (covLimit.doubleValue() > 0) {
				BigDecimal divisorVal = new BigDecimal("1000.00");
				factor = covLimit.divide(divisorVal).setScale(2, BigDecimal.ROUND_HALF_UP);
			} 
			//Get Increased Cov D Limit Rate
			BigDecimal rate = null;
			table = tables.get("IncBDE");	
			Row row = table.lookupSingleRow("Coverage","VMM");
			rate = row.getCell(policyForm).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);

			BigDecimal incrCovDPremium =  rate.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			premium = premium.add(incrCovDPremium).setScale(2, BigDecimal.ROUND_HALF_UP);
			addRatingStep(vmmRateArea, "Increased Cov D Limit Factor", "Increased Cov D Limit/1,000: "+fmt.format(covLimit), "", "", factor);
			addRatingStep(vmmRateArea, "Increased Cov D Limit Rate ", "Rate: "+rate, "x", rate, incrCovDPremium.setScale(2, BigDecimal.ROUND_HALF_UP));

		}else if(coverage.gets("CoverageCd").equals("CovE") || coverage.gets("CoverageCd").equals("DP0414")){
			// Start Increased Coverage E Premium
			//Get Increased Cov E Limit Factor - included
			covLimit = new BigDecimal(building.gets("CovELimitIncrease"));
			factor = new BigDecimal(0.0);
			if (covLimit.doubleValue() > 0) {
				BigDecimal divisorVal = new BigDecimal("1000.00");
				factor = covLimit.divide(divisorVal).setScale(2, BigDecimal.ROUND_HALF_UP);
			} 
			//Get Increased Cov E Limit Rate 
			BigDecimal rate = null;
			table = tables.get("IncBDE");	
			Row row = table.lookupSingleRow("Coverage","VMM");
			rate = row.getCell(policyForm).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
			BigDecimal incrCovEPremium =  rate.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			premium = premium.add(incrCovEPremium).setScale(2, BigDecimal.ROUND_HALF_UP);
			addRatingStep(vmmRateArea, "Increased Cov E Limit Factor", "Increased Cov E Limit/1,000: "+fmt.format(covLimit), "", "", factor);
			addRatingStep(vmmRateArea, "Increased Cov E Limit Rate ", "Rate: "+rate, "x", rate, incrCovEPremium.setScale(2, BigDecimal.ROUND_HALF_UP));

		}else if(coverage.gets("CoverageCd").equals("CovC") ){
			String 	fullzipCode =  building.findBeanByFieldValue("Addr", "AddrTypeCd", "RiskAddr").gets("PostalCode");
			if(fullzipCode !=null && fullzipCode.length() > 5){
				lookUpZipCode = fullzipCode.substring(0,5);
			}else if(fullzipCode !=null){
				lookUpZipCode = fullzipCode;
			}
			//Get the BasePremium Worksheet by RiskFactor 
			table = tables.get("BasePremium");
			Row row = table.lookupSingleRow("RiskFactor","VMM");
			String wsName = row.getCell("WorksheetName").getString();

			// Start Coverage A Premium
			// Get the BasePremium value by Territory, Building/Contents
			table = tables.get(wsName);
			rows = new Row[1];
			rows = table.lookup(new String[]{"Territory", "Category"}, new String[]{lookUpZipCode, category});    	    	
			BigDecimal keyBasePremium = new BigDecimal(0);
			if(rows!=null && rows.length>0 && rows[0]!=null){
				keyBasePremium =  rows[0].getCell(policyForm).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
			}    	
			premium = keyBasePremium;
			addRatingStep(vmmRateArea, "VMM" + " Key " + category + " Premium ", "Policy Form: " +policyForm +" Territory: " +lookUpZipCode, "", "", keyBasePremium);

			//Get the Coverage C factor
			table = tables.get("CovCFactor");	
			covLimit = new BigDecimal(building.gets("CovCLimit"));
			factor = new BigDecimal(0.00);
			if (covLimit.doubleValue() != 0 ) 
				factor = table.getRate(covLimit, "CovCLimit", "VMM", RatingTable.RATE_CALC_INTERPOLATE, 4);
			premium = premium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			addRatingStep(vmmRateArea, "Coverage C Factor", "Cov C: "+fmt.format(covLimit), "x",factor, premium.setScale(2, BigDecimal.ROUND_HALF_UP));

		}
		// Get Num of Units/Families Factor
		String numOfUnits = building.gets("Units");
		table = tables.get("NumFamily");
		String lookupStr = "VMM"+category;
		String riskFactorVal = "ECVMM";
		rows = table.lookup(new String[]{"Category", "NumOfUnits"}, new String[]{riskFactorVal, numOfUnits});    	
		factor  = rows[0].getCell(category).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
		premium = premium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
		addRatingStep(vmmRateArea, "# of Units/Families Factor", "# of Units/Families: "+numOfUnits, "x", factor, premium.setScale(2, BigDecimal.ROUND_HALF_UP) );

		// Get Seasonal/Non-Seasonal Factor
		String residence = building.gets("Residence");
		table = tables.get("SeasonalNon");
		if (!residence.equalsIgnoreCase("Seasonal")) {
			residence = "NonSeasonal";
		} else {
			residence = "Seasonal";
		}
		rows = table.lookup(new String[]{"Category", "Form"}, new String[]{residence, policyForm});        	
		factor  = rows[0].getCell(lookupStr).getBigDecimal().setScale(3,BigDecimal.ROUND_HALF_UP);
		premium = premium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
		addRatingStep(vmmRateArea, "Seasonal/Non-Seasonal Factor", "Residence: "+residence, "x", factor, premium.setScale(2, BigDecimal.ROUND_HALF_UP) );

		// Get Occupancy Factor
		String occupancyCd = building.gets("OccupancyCd");
		table = tables.get("Occupancy");
		Row row = table.lookupSingleRow("Category",riskFactorVal);

		factor  = row.getCell(occupancyCd).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
		premium = premium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
		addRatingStep(vmmRateArea, "Occupancy Factor", "Occupancy: "+occupancyCd, "x", factor, premium.setScale(2, BigDecimal.ROUND_HALF_UP) );

		// Get Protection Class/Construction Type Factor
		String protectionClass = building.gets("ProtectionClass");
		String constructionCd = building.gets("ConstructionCd");
		String lookUpCode = protectionClass+constructionCd;
		table = tables.get("ProtConstClass"+"ECVMM");
		rows = table.lookup(new String[]{"PROTCONST", "Category"}, new String[]{lookUpCode, category});    
		factor  = rows[0].getCell("Factor").getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
		premium = premium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
		addRatingStep(vmmRateArea, "Protection Class/Construction Type Factor", "Protection Class: "+protectionClass +" Construction Type: "+constructionCd, "x",factor.setScale(2, BigDecimal.ROUND_HALF_UP ) ,premium.setScale(2, BigDecimal.ROUND_HALF_UP ) );
		BigDecimal subTotal = premium;
		addRatingStep(vmmRateArea, "Premium Subtotal" , "", "", "", subTotal);			
		covApremium = applyDeductiblePremium(vmmRateArea,premium, "ECVMM", building.gets("AllPerilDed"));
		if(coverage.gets("CoverageCd").equals("CovA")){
			setCovAAOPPremVmm(covApremium);
		}else if(coverage.gets("CoverageCd").equals("CovB")){
			setCovBAOPPremVmm(covApremium);
		}else if(coverage.gets("CoverageCd").equals("CovD")){
			setCovDAOPPremVmm(covApremium);
		}else if(coverage.gets("CoverageCd").equals("CovE")){
			setCovEAOPPremVmm(covApremium);
		}else if(coverage.gets("CoverageCd").equals("CovC")){
			setCovCAOPPremVmm(covApremium);
		}else if(coverage.gets("CoverageCd").equals("DP0414")){
			setCovAddlAOPPremVmm(covApremium); //DP0414
		}
		covApremium = applyMaximiumFireAllOtherPerilsDeductibleCredit(vmmRateArea, "VMM", subTotal, coverage.gets("CoverageCd"),covApremium);
		ModelBean totalCreditstep = addRatingStep(vmmRateArea, "Total Credits", "", "x", "","");
		covApremium = applyCreditsPremium(totalCreditstep, covApremium, false, building);
		String 	fullZipCode =  building.findBeanByFieldValue("Addr", "AddrTypeCd", "RiskAddr").gets("PostalCode");
		covApremium = applyCoastalReinsuranceFactor(vmmRateArea, covApremium, fullZipCode);
		covApremium = applyLossSurchargePremium(vmmRateArea, covApremium);
		covApremium = applyGroupDiscountPremium(vmmRateArea, covApremium);
		vmmRateArea.setValue("FullTermAmt",covApremium.toString());
		if( StringRenderer.in(coverage.gets("CoverageCd"), "CovA,CovB,CovC,CovD,CovE,DP1914,DP0471,DP0414,DL2480")){
			vmmRateArea.setValue("AnnualStatementLineCd","021");
		}
		return covApremium;
	}

	private BigDecimal addCoverageOrdinancFireRateArea(ModelBean coverage, String category) throws Exception, ModelBeanException {
		ModelBean fireRateArea = addRateArea(coverage,"Fire","Fire");
		BigDecimal covApremium = new BigDecimal("0.00");
		BigDecimal premium = new BigDecimal(0);
		RatingTable table = null;
		String policyForm = basicPolicy.gets("SubTypeCd");
		BigDecimal factor = zero;
		premium =  premium.add(getCovAPremFireOrdinance()).add(getCovBPremFireOrdinance()).add(getCovDPremFireOrdinance()).add(getCovEPremFireOrdinance());
		ModelBean building = getBuildingFromCoverage(coverage);
		addRatingStep(fireRateArea, "Premium Subtotal", "", "", "", premium.setScale(2, BigDecimal.ROUND_HALF_UP) );
		ModelBean line = coverage.getParentBean().getParentBean();
		if(!policyForm.equalsIgnoreCase("DP1") && CoverageRenderer.isActive(line,"DP0471")){
			ModelBean risk = coverage.getParentBean();
			ModelBean covOrd = BeanTools.findBeanByFieldValueAndStatus(risk,"Coverage","CoverageCd","DP0471","Active");
			String limit = covOrd.gets("OrdOrLawPercentage");
			table = tables.get("OrdinanceCoverage");
			factor = table.lookupSingleRow("TotalAmount", limit , Table.MATCH_TYPE_NUMERIC).getCell("Factor").getBigDecimal().setScale(2, BigDecimal.ROUND_HALF_UP);
			factor=factor.subtract(new BigDecimal("1")).setScale(2, BigDecimal.ROUND_HALF_UP);
			premium = premium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			addRatingStep(fireRateArea, "Ordinance or Law Factor", "Increase in Coverage A %: "+limit, "x", factor, premium.setScale(2, BigDecimal.ROUND_HALF_UP) );
		}
		setCovPremFireOrdinance(premium);

		BigDecimal subTotal = premium;
		addRatingStep(fireRateArea, "Premium Subtotal" , "", "", "", subTotal);			
		covApremium = applyDeductiblePremium(fireRateArea,premium, "Fire", building.gets("AllPerilDed"));
		setCovOrdAOPPremFire(covApremium);
		covApremium = applyMaximiumFireAllOtherPerilsDeductibleCredit(fireRateArea, "FIRE", subTotal, coverage.gets("CoverageCd"),covApremium);
		ModelBean totalCreditstep = addRatingStep(fireRateArea, "Total Credits", "", "x", "","");
		covApremium = applyCreditsPremium(totalCreditstep, covApremium, true, building);
		covApremium = applyLossSurchargePremium(fireRateArea, covApremium);
		covApremium = applyGroupDiscountPremium(fireRateArea, covApremium);
		fireRateArea.setValue("FullTermAmt",covApremium.toString());
		if( StringRenderer.in(coverage.gets("CoverageCd"), "CovA,CovB,CovC,CovD,CovE,DP1914,DP0471,DP0414,DL2480")){
			fireRateArea.setValue("AnnualStatementLineCd","010");
		}
		return covApremium;
	}

	private BigDecimal addCoverageOrdinanceExtendedCoverageRateArea(ModelBean coverage, String category) throws Exception, ModelBeanException {
		ModelBean extendedCoverageRateArea = addRateArea(coverage,"Extended Coverage","Extended Coverage");
		BigDecimal covApremium = new BigDecimal("0.00");
		BigDecimal premium = new BigDecimal(0);
		RatingTable table = null;
		String policyForm = basicPolicy.gets("SubTypeCd");
		BigDecimal factor = zero;
		ModelBean line = coverage.getParentBean().getParentBean();
		premium =  premium.add(getCovAPremECOrdinance()).add(getCovBPremECOrdinance()).add(getCovDPremECOrdinance()).add(getCovEPremECOrdinance());
		addRatingStep(extendedCoverageRateArea, "Premium Subtotal", "", "", "", premium.setScale(2, BigDecimal.ROUND_HALF_UP) );
		if(!policyForm.equalsIgnoreCase("DP1") && CoverageRenderer.isActive(line,"DP0471")){
			ModelBean risk = coverage.getParentBean();
			ModelBean covOrd = BeanTools.findBeanByFieldValueAndStatus(risk,"Coverage","CoverageCd","DP0471","Active");
			String limit = covOrd.gets("OrdOrLawPercentage");
			table = tables.get("OrdinanceCoverage");
			factor = table.lookupSingleRow("TotalAmount", limit , Table.MATCH_TYPE_NUMERIC).getCell("Factor").getBigDecimal().setScale(2, BigDecimal.ROUND_HALF_UP);
			factor=factor.subtract(new BigDecimal("1")).setScale(2, BigDecimal.ROUND_HALF_UP);
			premium = premium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			addRatingStep(extendedCoverageRateArea, "Ordinance or Law Factor", "Increase in Coverage A %: "+limit, "x", factor, premium.setScale(2, BigDecimal.ROUND_HALF_UP) );
		}
		setCovPremECOrdinance(premium);

		ModelBean building = getBuildingFromCoverage(coverage);
		if(!building.gets("WindHailDed").equals("None") && !building.gets("WindHailDed").equals("")){
			covApremium = applyWindHailDeductiblePremium(extendedCoverageRateArea, premium,"PerilWinDed",building.gets("WindHailDed"), building.gets("AllPerilDed"));
		}else{
			covApremium=premium;
		}
		BigDecimal subTotal = covApremium;
		addRatingStep(extendedCoverageRateArea, "Premium Subtotal" , "", "", "", subTotal);			
		covApremium = applyDeductiblePremium(extendedCoverageRateArea,covApremium, "ECVMM", building.gets("AllPerilDed"));
		setCovOrdAOPPremEc(covApremium);
		covApremium = applyMaximiumFireAllOtherPerilsDeductibleCredit(extendedCoverageRateArea, "EC", subTotal, coverage.gets("CoverageCd"),covApremium);
		ModelBean totalCreditstep = addRatingStep(extendedCoverageRateArea, "Total Credits", "", "x", "","");
		covApremium = applyCreditsPremium(totalCreditstep, covApremium, false, building);
		String 	fullZipCode =  building.findBeanByFieldValue("Addr", "AddrTypeCd", "RiskAddr").gets("PostalCode");
		covApremium = applyCoastalReinsuranceFactor(extendedCoverageRateArea, covApremium, fullZipCode);
		covApremium = applyLossSurchargePremium(extendedCoverageRateArea, covApremium);
		covApremium = applyGroupDiscountPremium(extendedCoverageRateArea, covApremium);
		extendedCoverageRateArea.setValue("FullTermAmt",covApremium.toString());
		if( StringRenderer.in(coverage.gets("CoverageCd"), "CovA,CovB,CovC,CovD,CovE,DP1914,DP0471,DP0414,DL2480")){
			extendedCoverageRateArea.setValue("AnnualStatementLineCd","021");
		}
		return covApremium;
	}

	private void rateCoverageL( ModelBean coverage) throws Exception {
		try{
			Log.debug("Rating coverage L");

			String riskFactor = "Liability";
			String category = "Building";
			ModelBean building = getBuildingFromCoverage(coverage);
			if(building.gets("OccupancyCd").equals("Owner")){
				category = "Building";
			}else{
				category = "Contents";
			}
			String lookUpZipCode = "";
			String policyForm = basicPolicy.gets("SubTypeCd");
			String 	fullzipCode =  building.findBeanByFieldValue("Addr", "AddrTypeCd", "RiskAddr").gets("PostalCode");
			if(fullzipCode !=null && fullzipCode.length() > 5){
				lookUpZipCode = fullzipCode.substring(0,5);
			} else if(fullzipCode != null && fullzipCode.length() == 5){
				lookUpZipCode = fullzipCode;
			}
			//Get the BasePremium Worksheet by RiskFactor 
			RatingTable table = tables.get("BasePremium");
			Row row = table.lookupSingleRow("RiskFactor",riskFactor);
			String wsName = row.getCell("WorksheetName").getString();

			// Start Coverage A Premium
			// Get the BasePremium value by Territory
			table = tables.get(wsName);
			row = table.lookupSingleRow("Territory", lookUpZipCode);    	    	
			BigDecimal premium = new BigDecimal(0);
			BigDecimal keyBasePremium = null;
			if(row !=null){
				keyBasePremium =  row.getCell("Factor").getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
			}else{   		
				keyBasePremium = new BigDecimal("100.00"); //Check value for null if not needed DELETE
			}    	
			premium = keyBasePremium;
			addRatingStep(coverage, riskFactor + " Key Premium ", "Policy Form: " +policyForm +" Territory: " +lookUpZipCode, "", "", keyBasePremium);

			Row[] rows = new Row[1];

			// Get Num of Units/Families Factor
			String numOfUnits = building.gets("Units");
			table = tables.get("NumFamily");
			rows = table.lookup(new String[]{"Category", "NumOfUnits"}, new String[]{riskFactor, numOfUnits});    	
			BigDecimal factor  = rows[0].getCell(category).getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
			premium = premium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			addRatingStep(coverage, "# of Units/Families Factor", "# of Units/Families: "+numOfUnits, "x", factor, premium.setScale(2, BigDecimal.ROUND_HALF_UP) );

			// Get Liability Limit
			String covLLimit = building.gets("CovLLimit");
			BigDecimal covLLimitAsBigDecimal = new BigDecimal(covLLimit);
			table = tables.get("LiabilityCoverageLimits");
			row = table.lookupSingleRow("LiabilityLimit", covLLimit);   
			factor  = row.getCell("Factor").getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
			premium = premium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			addRatingStep(coverage, "Limit of Liability", "Liability Limit: "+fmt.format(covLLimitAsBigDecimal), "x", factor, premium.setScale(2, BigDecimal.ROUND_HALF_UP) );

			String transactionCd = null;
			if(basicPolicy.gets("TransactionCd").equals("New Business") || basicPolicy.gets("TransactionCd").equals("Renewal") || basicPolicy.gets("TransactionCd").equals("Rewrite-New")){
				transactionCd = basicPolicy.gets("TransactionCd");
			} else {
				ModelBean transactionHistory = basicPolicy.findBeanByFieldValue("TransactionHistory", "TransactionNumber", "1");
				if(transactionHistory != null){
					transactionCd = transactionHistory.gets("TransactionCd");
				}
			}
			if (lossHistory != null  && lossHistory.length >0 && transactionCd != null && transactionCd.equals("Renewal")){ //sort when more than 1 element and go inside
				if(lossHistory.length >1 ){
					lossHistory = BeanTools.sortBeansBy(lossHistory,new String[]{"LastPaymentDt"},new String[]{"Descending"});
					// Sorted as per the last Payment date on the claim
				}	
				String lastPaymentDt = lossHistory[0].gets("LastPaymentDt");
				if(!lastPaymentDt.equals("")){
					StringDate lastPaymentDtValue = new StringDate(lastPaymentDt);					
					//After sorting , need the date of the first loss
					int age = DateTools.getAge(lastPaymentDtValue,basicPolicy.getDate("EffectiveDt")); 
					table = tables.get("LossFreeCredit");
					BigDecimal lossFreePostFactor = table.lookupSingleRow("YearsLossFreeAfterInception", Integer.toString(age)).getCell("Credit").getBigDecimal().setScale(3, BigDecimal.ROUND_HALF_UP);
					lossFreePostFactor =  new BigDecimal("1.00").subtract(lossFreePostFactor);
					premium = premium.multiply(lossFreePostFactor);
					addRatingStep(coverage, "Loss Free Credit Factor", "Years Loss Free: "+Integer.toString(age) , "x",lossFreePostFactor.setScale(3, BigDecimal.ROUND_HALF_UP ) ,premium.setScale(2, BigDecimal.ROUND_HALF_UP ) );
				}
			}
			if(lossHistory == null || lossHistory.length == 0){
				if(transactionCd != null && transactionCd.equals("Renewal")){	
					String policyVersion = basicPolicy.gets("PolicyVersion");
					policyVersion = StringTools.subtractInt(policyVersion, "1");
					table = tables.get("LossFreeCredit");
					Row lossFreePostFactorRow = table.lookupSingleRow("YearsLossFreeAfterInception", policyVersion);
					if(lossFreePostFactorRow!=null){
						BigDecimal lossFreePostFactor = table.lookupSingleRow("YearsLossFreeAfterInception", policyVersion).getCell("Credit").getBigDecimal().setScale(3, BigDecimal.ROUND_HALF_UP);
						lossFreePostFactor =  new BigDecimal("1.00").subtract(lossFreePostFactor);
						premium = premium.multiply(lossFreePostFactor);
						addRatingStep(coverage, "Loss Free Credit Factor", "Years Loss Free: "+policyVersion , "x",lossFreePostFactor.setScale(3, BigDecimal.ROUND_HALF_UP ) ,premium.setScale(2, BigDecimal.ROUND_HALF_UP ) );
					}
				}
			}
			premium = applyLossSurchargePremium(coverage, premium);
			premium = applyGroupDiscountPremium(coverage, premium);
			addRatingStep(coverage, "L - Personal Liability Premium", "L - Personal Liability Premium", "", "",premium);
			//covApremium = NumberTools.roundToDollar(covApremium.setScale(2, BigDecimal.ROUND_HALF_UP));
			coverage.setValue("FullTermAmt", premium);
			totalPremium = totalPremium.add(new BigDecimal(coverage.gets("FullTermAmt")));
		}
		catch(Exception e){
			throw new RatingException(e);
		}

	}
	private void rateCoverageM( ModelBean coverage) throws Exception {
		try{
			Log.debug("Rating coverage M");
			BigDecimal premium = zero;

			ModelBean building = getBuildingFromCoverage(coverage);
			String covMLimit = building.gets("CovMLimit");
			BigDecimal covMLimitAsBigDecimal = new BigDecimal(covMLimit);
			RatingTable table = tables.get("MedicalPayments");
			Row row = table.lookupSingleRow("MedicalPayLimit", covMLimit);   
			BigDecimal factor  = row.getCell("Premium").getBigDecimal().setScale(2,BigDecimal.ROUND_HALF_UP);
			premium = premium.add(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			addRatingStep(coverage, "Medical Payments Coverage", "Medical Payments Limit: "+fmt.format(covMLimitAsBigDecimal), "", "", premium.setScale(2, BigDecimal.ROUND_HALF_UP) );


			// Get Loss Free Credit Premium
			String transactionCd = null;
			if(basicPolicy.gets("TransactionCd").equals("New Business") || basicPolicy.gets("TransactionCd").equals("Renewal") || basicPolicy.gets("TransactionCd").equals("Rewrite-New")){
				transactionCd = basicPolicy.gets("TransactionCd");
			} else {
				ModelBean transactionHistory = basicPolicy.findBeanByFieldValue("TransactionHistory", "TransactionNumber", "1");
				if(transactionHistory != null){
					transactionCd = transactionHistory.gets("TransactionCd");
				}
			}
			if (lossHistory != null  && lossHistory.length >0 && transactionCd != null && transactionCd.equals("Renewal")){ //sort when more than 1 element and go inside
				if(lossHistory.length >1 ){
					lossHistory = BeanTools.sortBeansBy(lossHistory,new String[]{"LastPaymentDt"},new String[]{"Descending"});
					// Sorted as per the last payment date on the claim
				}	
				String lastPaymentDt = lossHistory[0].gets("LastPaymentDt");
				if(!lastPaymentDt.equals("")){
					StringDate lastPaymentDtValue = new StringDate(lastPaymentDt);					
					//After sorting , need the date of the first loss
					int age = DateTools.getAge(lastPaymentDtValue,basicPolicy.getDate("EffectiveDt"));				 
					table = tables.get("LossFreeCredit");
					BigDecimal lossFreePostFactor = table.lookupSingleRow("YearsLossFreeAfterInception", Integer.toString(age)).getCell("Credit").getBigDecimal().setScale(3, BigDecimal.ROUND_HALF_UP);
					lossFreePostFactor =  new BigDecimal("1.00").subtract(lossFreePostFactor);
					premium = premium.multiply(lossFreePostFactor);
					addRatingStep(coverage, "Loss Free Credit Factor", "Years Loss Free: "+Integer.toString(age) , "x",lossFreePostFactor.setScale(3, BigDecimal.ROUND_HALF_UP ) ,premium.setScale(2, BigDecimal.ROUND_HALF_UP ) );
				}
			}
			if(lossHistory == null || lossHistory.length == 0){
				if(transactionCd != null && transactionCd.equals("Renewal")){	
					String policyVersion = basicPolicy.gets("PolicyVersion");
					policyVersion = StringTools.subtractInt(policyVersion, "1");
					table = tables.get("LossFreeCredit");
					Row lossFreePostFactorRow = table.lookupSingleRow("YearsLossFreeAfterInception", policyVersion);
					if(lossFreePostFactorRow!=null){
						BigDecimal lossFreePostFactor = table.lookupSingleRow("YearsLossFreeAfterInception", policyVersion).getCell("Credit").getBigDecimal().setScale(3, BigDecimal.ROUND_HALF_UP);
						lossFreePostFactor =  new BigDecimal("1.00").subtract(lossFreePostFactor);
						premium = premium.multiply(lossFreePostFactor);
						addRatingStep(coverage, "Loss Free Credit Factor", "Years Loss Free: "+policyVersion , "x",lossFreePostFactor.setScale(3, BigDecimal.ROUND_HALF_UP ) ,premium.setScale(2, BigDecimal.ROUND_HALF_UP ) );
					}
				}
			}
			premium = applyLossSurchargePremium(coverage, premium);
			premium = applyGroupDiscountPremium(coverage, premium);
			addRatingStep(coverage, "M - Medical Payment to Others Premium", "M - Medical Payment to Others Premium", "", "",premium);
			//covApremium = NumberTools.roundToDollar(covApremium.setScale(2, BigDecimal.ROUND_HALF_UP));
			coverage.setValue("FullTermAmt", premium);
			totalPremium = totalPremium.add(new BigDecimal(coverage.gets("FullTermAmt")));
		}
		catch(Exception e){
			throw new RatingException(e);
		}

	}

	private BigDecimal applyDeductiblePremium( ModelBean rateArea,  BigDecimal covAPremium, String category, String perilDed) throws Exception {
		RatingTable table = tables.get("AOPDeductible");
		if(!perilDed.equals("None")){
			BigDecimal perilDedAsBigDecimal = new BigDecimal(perilDed);
			String colVal = "Factor";
			Row[] rows = new Row[1];
			rows = table.lookup(new String[]{"Category", "Deductible"}, new String[]{category,perilDed});    	    	
			BigDecimal factor = rows[0].getCell(colVal).getBigDecimal().setScale(3, BigDecimal.ROUND_HALF_UP);
			covAPremium = covAPremium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
			addRatingStep(rateArea, "All Other Perils Deductible Factor", "All Other Perils Deductible: "+fmt.format(perilDedAsBigDecimal), "x",factor.setScale(2,BigDecimal.ROUND_HALF_UP) ,covAPremium.setScale(2, BigDecimal.ROUND_HALF_UP ));
		}else{
			covAPremium=zero;
			addRatingStep(rateArea, "All Other Perils Deductible Factor", "All Other Perils Deductible: None", "x",zero.setScale(2,BigDecimal.ROUND_HALF_UP) ,covAPremium.setScale(2, BigDecimal.ROUND_HALF_UP ));
		}
		return covAPremium;	
	}

	private BigDecimal applyCreditsPremium( ModelBean step ,  BigDecimal covAPremium, boolean protectiveDevice, ModelBean building   ) throws Exception {
		BigDecimal factor = new BigDecimal("0.00");

		if (protectiveDevice) {
			factor = applyProtectiveDeviceCredit(step, building);
			factor = factor.multiply(Minus_One);
		}
		String transactionCd = null;
		if(basicPolicy.gets("TransactionCd").equals("New Business") || basicPolicy.gets("TransactionCd").equals("Renewal") || basicPolicy.gets("TransactionCd").equals("Rewrite-New")){
			transactionCd = basicPolicy.gets("TransactionCd");
		} else {
			ModelBean transactionHistory = basicPolicy.findBeanByFieldValue("TransactionHistory", "TransactionNumber", "1");
			if(transactionHistory != null){
				transactionCd = transactionHistory.gets("TransactionCd");
			}
		}
		if (lossHistory != null  && lossHistory.length >0 && transactionCd != null && transactionCd.equals("Renewal")){ //sort when more than 1 element and go inside
			if(lossHistory.length >1 ){
				lossHistory = BeanTools.sortBeansBy(lossHistory,new String[]{"LastPaymentDt"},new String[]{"Descending"});
				// Sorted as per the last payment date on the claim
			}	
			String lastPaymentDt = lossHistory[0].gets("LastPaymentDt");
			if(!lastPaymentDt.equals("")){
				StringDate lastPaymentDtValue = new StringDate(lastPaymentDt);					
				//After sorting , need the date of the first loss
				int age = DateTools.getAge(lastPaymentDtValue,basicPolicy.getDate("EffectiveDt"));

				RatingTable table = tables.get("LossFreeCredit");
				if(table.lookupSingleRow("YearsLossFreeAfterInception", Integer.toString(age))!=null){
					BigDecimal lossFreePostFactor = table.lookupSingleRow("YearsLossFreeAfterInception", Integer.toString(age)).getCell("Credit").getBigDecimal().setScale(3, BigDecimal.ROUND_HALF_UP);
					factor = factor.add(lossFreePostFactor);
					addRatingStep(step, "Loss Free Credit Factor", "Years Loss Free: "+Integer.toString(age) , "+","-"+lossFreePostFactor.setScale(3, BigDecimal.ROUND_HALF_UP ) ,factor.multiply(Minus_One).setScale(2, BigDecimal.ROUND_HALF_UP ) );
				}
			}
		}
		if(lossHistory == null || lossHistory.length == 0){
			if(transactionCd != null && transactionCd.equals("Renewal")){	
				String policyVersion = basicPolicy.gets("PolicyVersion");
				policyVersion = StringTools.subtractInt(policyVersion, "1");
				RatingTable table = tables.get("LossFreeCredit");
				Row lossFreePostFactorRow = table.lookupSingleRow("YearsLossFreeAfterInception", policyVersion);
				if(lossFreePostFactorRow!=null){
					BigDecimal lossFreePostFactor = table.lookupSingleRow("YearsLossFreeAfterInception", policyVersion).getCell("Credit").getBigDecimal().setScale(3, BigDecimal.ROUND_HALF_UP);
					factor = factor.add(lossFreePostFactor);
					addRatingStep(step, "Loss Free Credit Factor", "Years Loss Free: "+policyVersion , "+","-"+lossFreePostFactor.setScale(3, BigDecimal.ROUND_HALF_UP ) ,factor.multiply(Minus_One).setScale(2, BigDecimal.ROUND_HALF_UP ) );
				}
			}
		}
		RatingTable table = tables.get("NewHomeCredit");
		String effYear = basicPolicy.gets("EffectiveDt").substring(6);
		String newHomeAge = StringTools.subtractInt(effYear, building.gets("YearBuilt"));		
		newHomeAge = StringTools.addInt(newHomeAge, "1");
		String homeAge =newHomeAge;
		if(new BigDecimal(newHomeAge).compareTo(new BigDecimal("0")) == -1){
			newHomeAge = "0";
		}
		if(new BigDecimal(newHomeAge).compareTo(new BigDecimal("15")) == 1){
			newHomeAge = "16AndMore";
		}
		if(table.lookupSingleRow("AgeOfDwelling", newHomeAge)!=null){
			BigDecimal newHomeCredit = table.lookupSingleRow("AgeOfDwelling", newHomeAge).getCell("Credit").getBigDecimal().setScale(3, BigDecimal.ROUND_HALF_UP);
			factor = factor.add(newHomeCredit);
			addRatingStep(step, "New Home Credit Factor", "Age of Dwelling: "+homeAge , "+","-"+newHomeCredit.setScale(3, BigDecimal.ROUND_HALF_UP ) ,factor.multiply(Minus_One).setScale(2, BigDecimal.ROUND_HALF_UP ) );
		}

		BigDecimal maxCredit = new BigDecimal(0.25);
		if (factor.doubleValue() > maxCredit.doubleValue() ) {
			factor = maxCredit;
		}		
		factor =factor.multiply(Minus_One);
		addRatingStep(step, "Maximum Credit Factor", "Maximum Total Credit: -.25", "","",factor.setScale(3, BigDecimal.ROUND_HALF_UP ) );
		BigDecimal totalCreditFactor = new BigDecimal("1.00").add(factor);
		covAPremium = covAPremium.multiply(totalCreditFactor).setScale(2, BigDecimal.ROUND_HALF_UP);
		step.setValue("Desc", "Total Credits Factor: 1 + "+factor.setScale(2, BigDecimal.ROUND_HALF_UP) );
		step.setValue("Value", covAPremium.setScale(2, BigDecimal.ROUND_HALF_UP) );
		step.setValue("Factor", totalCreditFactor.setScale(2, BigDecimal.ROUND_HALF_UP));

		return covAPremium;	
	}	

	private BigDecimal applyProtectiveDeviceCredit ( ModelBean step, ModelBean building ) throws Exception {
		BigDecimal factor = new BigDecimal("0.00");
		BigDecimal fireFactor = new BigDecimal("0.00");
		BigDecimal smokeDetectorFactor = new BigDecimal("0.00");
		BigDecimal sprinklerFactor = new BigDecimal("0.00");
		BigDecimal watertempFactor = new BigDecimal("0.00");

		RatingTable table =null;
		String fireAlarmCredit = building.gets("FireAlarmCd");
		String smokeDetectorCredit = building.gets("SmokeDetectorCd");
		String sprinklerSystemCredit = building.gets("SprinklerSystem");
		String waterTempAlarmCredit = building.gets("WaterTempAlarm");
		BigDecimal fireCredit = null;
		BigDecimal smokeCredit = null;
		BigDecimal sprinklerCredit = null;
		BigDecimal watertempCredit = null;

		if(!fireAlarmCredit.equalsIgnoreCase("")){
			table = tables.get("FireAlarmCredit");
			fireCredit = table.lookupSingleRow("ALARM", fireAlarmCredit , Table.MATCH_TYPE_STRING_CASE_INSENSITIVE).getCell("Factor").getBigDecimal().setScale(3, BigDecimal.ROUND_HALF_UP);
			if (fireCredit == null) {
				fireCredit = new BigDecimal(0.00);
			}
			fireFactor = factor.add(fireCredit);
			factor = factor.add(fireCredit);
		}
		if(!smokeDetectorCredit.equalsIgnoreCase("")){
			table = tables.get("SmokeDetectorCredit");
			smokeCredit = table.lookupSingleRow("SmokeDetector", smokeDetectorCredit , Table.MATCH_TYPE_STRING_CASE_INSENSITIVE).getCell("Factor").getBigDecimal().setScale(3, BigDecimal.ROUND_HALF_UP);
			if (smokeCredit == null) {
				smokeCredit = new BigDecimal(0.00);
			}
			smokeDetectorFactor = factor.add(smokeCredit);
			factor = factor.add(smokeCredit);
		}
		if(sprinklerSystemCredit != ""){
			table = tables.get("SprinklerSystemCredit");
			sprinklerCredit = table.lookupSingleRow("ALARM", sprinklerSystemCredit, Table.MATCH_TYPE_STRING_CASE_INSENSITIVE).getCell("Factor").getBigDecimal().setScale(3, BigDecimal.ROUND_HALF_UP);
			if (sprinklerCredit == null) {
				sprinklerCredit = new BigDecimal(0.00);
			}	
			sprinklerFactor = factor.add(sprinklerCredit);
			factor = factor.add(sprinklerCredit);
		}
		if(waterTempAlarmCredit != ""){
			table = tables.get("WaterTempAlarmCredit");
			watertempCredit = table.lookupSingleRow("ALARM", waterTempAlarmCredit , Table.MATCH_TYPE_STRING_CASE_INSENSITIVE).getCell("Factor").getBigDecimal().setScale(3, BigDecimal.ROUND_HALF_UP);
			if (watertempCredit == null) {
				watertempCredit = new BigDecimal(0.00);
			}	
			watertempFactor = factor.add(watertempCredit);
			factor = factor.add(watertempCredit);
		}
		BigDecimal maxCredit = new BigDecimal(0.10);
		if (factor.doubleValue() > maxCredit.doubleValue() ) {
			factor = maxCredit;
		}
		if (fireCredit != null) {
			addRatingStep(step, "Fire Alarm Credit", "Fire Alarm: "+fireAlarmCredit, "+","-"+fireCredit.setScale(2, BigDecimal.ROUND_HALF_UP ), fireFactor.multiply(Minus_One).setScale(2, BigDecimal.ROUND_HALF_UP ) );
		}
		if (smokeCredit != null) {
			addRatingStep(step, "Smoke Detector Credit", "Smoke Detector: "+smokeDetectorCredit, "+","-"+smokeCredit.setScale(2, BigDecimal.ROUND_HALF_UP ), smokeDetectorFactor.multiply(Minus_One).setScale(2, BigDecimal.ROUND_HALF_UP ) );
		}
		if (sprinklerCredit != null) {
			addRatingStep(step, "Sprinkler System Credit", "Sprinkler System: "+sprinklerSystemCredit, "+","-"+sprinklerCredit.setScale(2, BigDecimal.ROUND_HALF_UP ), sprinklerFactor.multiply(Minus_One).setScale(2, BigDecimal.ROUND_HALF_UP ) );
		}
		if (watertempCredit != null) {
			addRatingStep(step, "Water & Temperature Alarm Credit", "Water & Temperature Alarm: "+waterTempAlarmCredit, "+","-"+watertempCredit.setScale(2, BigDecimal.ROUND_HALF_UP ), watertempFactor.multiply(Minus_One).setScale(2, BigDecimal.ROUND_HALF_UP ) );
		}
		factor = factor.multiply(Minus_One);
		addRatingStep(step, "Maximum Protective Device Credit Factor", "Maximum Protective Device Credit: -.10", "","", factor.setScale(2, BigDecimal.ROUND_HALF_UP ) );

		return factor;		
	}

	private BigDecimal applyMaximiumFireAllOtherPerilsDeductibleCredit ( ModelBean rateArea,String rateAreaName, BigDecimal premiumSubtotal,String coverage,BigDecimal premiumCov) throws Exception {

		BigDecimal premium = new BigDecimal("0.00");
		BigDecimal covPremium = new BigDecimal("0.00");
		BigDecimal totalPremium = new BigDecimal("0.00");
		BigDecimal covApremium = new BigDecimal("0.00");
		BigDecimal covBpremium = new BigDecimal("0.00");
		BigDecimal covCpremium = new BigDecimal("0.00");
		BigDecimal covDpremium = new BigDecimal("0.00");
		BigDecimal covEpremium = new BigDecimal("0.00");
		BigDecimal covAddlpremium = new BigDecimal("0.00");
		BigDecimal covOrdpremium = new BigDecimal("0.00");
		if(rateAreaName.equals("FIRE")){
			covApremium=getCovAAOPPremFire();
			covBpremium=getCovBAOPPremFire();
			covCpremium=getCovCAOPPremFire();
			covDpremium=getCovDAOPPremFire();
			covEpremium=getCovEAOPPremFire();
			covAddlpremium=getCovAddlAOPPremFire();
			covOrdpremium=getCovOrdAOPPremFire();
		}else if(rateAreaName.equals("EC")){
			covApremium=getCovAAOPPremEc();
			covBpremium=getCovBAOPPremEc();
			covCpremium=getCovCAOPPremEc();
			covDpremium=getCovDAOPPremEc();
			covEpremium=getCovEAOPPremEc();
			covAddlpremium=getCovAddlAOPPremEc();
			covOrdpremium=getCovOrdAOPPremEc();
		}else if(rateAreaName.equals("VMM")){
			covApremium=getCovAAOPPremVmm();
			covBpremium=getCovBAOPPremVmm();
			covCpremium=getCovCAOPPremVmm();
			covDpremium=getCovDAOPPremVmm();
			covEpremium=getCovEAOPPremVmm();
			covAddlpremium=getCovAddlAOPPremVmm();
			covOrdpremium=getCovOrdAOPPremVmm();
		}

		totalPremium=covApremium.add(covBpremium.add(covCpremium).add(covDpremium).add(covEpremium).add(covAddlpremium).add(covOrdpremium));
		totalPremium=totalPremium.multiply(new BigDecimal("-1"));
		if(totalPremium.compareTo(new BigDecimal("1500"))>0){
			BigDecimal appliedMinPremium = new BigDecimal("1500");
			BigDecimal appliedMinPremiumFinal = new BigDecimal("1500");
			covApremium = covApremium.multiply(new BigDecimal("-1"));
			covBpremium = covBpremium.multiply(new BigDecimal("-1"));
			covCpremium = covCpremium.multiply(new BigDecimal("-1"));
			covDpremium = covDpremium.multiply(new BigDecimal("-1"));
			covEpremium = covEpremium.multiply(new BigDecimal("-1"));
			covAddlpremium = covAddlpremium.multiply(new BigDecimal("-1"));
			covOrdpremium = covOrdpremium.multiply(new BigDecimal("-1"));
			if(covOrdpremium.compareTo(zero)>0 && CoverageRenderer.isActive(application, "DP0471")){
				BigDecimal covOrdPercent = covOrdpremium.divide(totalPremium, 4, BigDecimal.ROUND_HALF_UP);
				BigDecimal covOrdMinPrem=appliedMinPremiumFinal.multiply(covOrdPercent).setScale(2, BigDecimal.ROUND_HALF_UP);
				appliedMinPremium=appliedMinPremium.subtract(covOrdMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
				covOrdpremium=covOrdMinPrem.setScale(2, BigDecimal.ROUND_HALF_UP);
			}
			if(covAddlpremium.compareTo(zero)>0 && CoverageRenderer.isActive(application, "DP0414")){
				BigDecimal covAddlPercent = covAddlpremium.divide(totalPremium, 4, BigDecimal.ROUND_HALF_UP);
				BigDecimal covAddlMinPrem=appliedMinPremiumFinal.multiply(covAddlPercent).setScale(2, BigDecimal.ROUND_HALF_UP);
				appliedMinPremium=appliedMinPremium.subtract(covAddlMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
				covAddlpremium=covAddlMinPrem.setScale(2, BigDecimal.ROUND_HALF_UP);
			}
			if(covEpremium.compareTo(zero)>0 && CoverageRenderer.isActive(application, "CovE")){
				BigDecimal covEPercent = covEpremium.divide(totalPremium, 4, BigDecimal.ROUND_HALF_UP);
				BigDecimal covEMinPrem=appliedMinPremiumFinal.multiply(covEPercent).setScale(2, BigDecimal.ROUND_HALF_UP);
				appliedMinPremium=appliedMinPremium.subtract(covEMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
				covEpremium=covEMinPrem.setScale(2, BigDecimal.ROUND_HALF_UP);
			}
			if(covDpremium.compareTo(zero)>0 && CoverageRenderer.isActive(application, "CovD")){
				BigDecimal covDPercent = covDpremium.divide(totalPremium, 4, BigDecimal.ROUND_HALF_UP);
				BigDecimal covDMinPrem=appliedMinPremiumFinal.multiply(covDPercent).setScale(2, BigDecimal.ROUND_HALF_UP);
				appliedMinPremium=appliedMinPremium.subtract(covDMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
				covDpremium=covDMinPrem.setScale(2, BigDecimal.ROUND_HALF_UP);
			}
			if(covCpremium.compareTo(zero)>0 && CoverageRenderer.isActive(application, "CovC")){
				BigDecimal covCPercent = covCpremium.divide(totalPremium, 4, BigDecimal.ROUND_HALF_UP);
				BigDecimal covCMinPrem=appliedMinPremiumFinal.multiply(covCPercent).setScale(2, BigDecimal.ROUND_HALF_UP);
				appliedMinPremium=appliedMinPremium.subtract(covCMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
				covCpremium=covCMinPrem.setScale(2, BigDecimal.ROUND_HALF_UP);
			}
			if(covBpremium.compareTo(zero)>0 && CoverageRenderer.isActive(application, "CovB")){
				BigDecimal covBPercent = covBpremium.divide(totalPremium, 4, BigDecimal.ROUND_HALF_UP);
				BigDecimal covBMinPrem=appliedMinPremiumFinal.multiply(covBPercent).setScale(2, BigDecimal.ROUND_HALF_UP);
				appliedMinPremium=appliedMinPremium.subtract(covBMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
				covBpremium=covBMinPrem.setScale(2, BigDecimal.ROUND_HALF_UP);
			}
			if(covApremium.compareTo(zero)>0 && CoverageRenderer.isActive(application, "CovA")){
				covApremium=appliedMinPremium.setScale(2, BigDecimal.ROUND_HALF_UP);			
			}
			covApremium = covApremium.multiply(new BigDecimal("-1"));
			covBpremium = covBpremium.multiply(new BigDecimal("-1"));
			covCpremium = covCpremium.multiply(new BigDecimal("-1"));
			covDpremium = covDpremium.multiply(new BigDecimal("-1"));
			covEpremium = covEpremium.multiply(new BigDecimal("-1"));
			covAddlpremium = covAddlpremium.multiply(new BigDecimal("-1"));
			covOrdpremium = covOrdpremium.multiply(new BigDecimal("-1"));
			if(coverage.equals("CovA")){
				covPremium=covApremium;
			}else if(coverage.equals("CovB")){
				covPremium=covBpremium;
			}else if(coverage.equals("CovC")){
				covPremium=covCpremium;
			}else if(coverage.equals("CovD")){
				covPremium=covDpremium;
			}else if(coverage.equals("CovE")){
				covPremium=covEpremium;
			}else if(coverage.equals("DP0414")){
				covPremium=covAddlpremium;
			}else if(coverage.equals("DP0471")){
				covPremium=covOrdpremium;
			}
			if(rateAreaName.equals("Fire")){
				setCovAAOPPremFire(covApremium);
				setCovBAOPPremFire(covBpremium);
				setCovCAOPPremFire(covCpremium);
				setCovDAOPPremFire(covDpremium);
				setCovEAOPPremFire(covEpremium);
				setCovAddlAOPPremFire(covAddlpremium);
				setCovOrdAOPPremFire(covOrdpremium);
			}else if(rateAreaName.equals("EC")){
				setCovAAOPPremEc(covApremium);
				setCovBAOPPremEc(covBpremium);
				setCovCAOPPremEc(covCpremium);
				setCovDAOPPremEc(covDpremium);
				setCovEAOPPremEc(covEpremium);
				setCovAddlAOPPremEc(covAddlpremium);
				setCovOrdAOPPremEc(covOrdpremium);
			}else if(rateAreaName.equals("VMM")){
				setCovAAOPPremVmm(covApremium);
				setCovBAOPPremVmm(covBpremium);
				setCovCAOPPremVmm(covCpremium);
				setCovDAOPPremVmm(covDpremium);
				setCovEAOPPremVmm(covEpremium);
				setCovAddlAOPPremVmm(covAddlpremium);
				setCovOrdAOPPremVmm(covOrdpremium);
			}

		}else{
			covPremium=premiumCov;
		}


		addRatingStep(rateArea, "Maximium Fire All Other Perils Deductible Credit" , "Maximum Fire All Other Perils Deductible Credit", "", "-$1500", covPremium);
		premium=covPremium.add(premiumSubtotal);
		addRatingStep(rateArea, "All Other Perils Deductible Premium" , "All Other Perils Deductible: ", "+", covPremium, premium);

		return premium;		
	}	

	private BigDecimal applyWindHailDeductiblePremium( ModelBean rateArea , BigDecimal covAPremium , String tableName , String windHailDed, String perilDed  ) throws Exception {
		RatingTable table = tables.get(tableName);
		BigDecimal perilDedAsBigDecimal = new BigDecimal(perilDed);
		String colVal = "Factor";
		Row[] rows = new Row[1];
		rows = table.lookup(new String[]{"Category", "Deductible"}, new String[]{"WIND_"+windHailDed,perilDed});    	    	
		BigDecimal factor = rows[0].getCell(colVal).getBigDecimal().setScale(3, BigDecimal.ROUND_HALF_UP);
		factor=new BigDecimal("1").subtract(factor);
		covAPremium = covAPremium.multiply(factor).setScale(2, BigDecimal.ROUND_HALF_UP);
		addRatingStep(rateArea, "Windstorm and Hail Deductible Factor", "Windstorm and Hail Deductible: " + fmt.format(perilDedAsBigDecimal), "x",factor.setScale(2,BigDecimal.ROUND_HALF_UP) ,covAPremium.setScale(2, BigDecimal.ROUND_HALF_UP ));		
		return covAPremium;	
	}	

	private BigDecimal applyLossSurchargePremium ( ModelBean rateArea ,  BigDecimal covAPremium   ) throws Exception {
		RatingTable table = tables.get("LossSurcharge");
		int numberofLosses = 0;
		if(preInceptionLossHistory!=null){
			numberofLosses = preInceptionLossHistory.length;
		}
		if (numberofLosses >5){
			numberofLosses = 5;
		}
		BigDecimal lossSurchargeFactor = table.lookupSingleRow("NumberOfLosses", Integer.toString(numberofLosses)).getCell("Factor").getBigDecimal().setScale(2, BigDecimal.ROUND_HALF_UP);
		covAPremium = covAPremium.multiply(lossSurchargeFactor).setScale(2, BigDecimal.ROUND_HALF_UP);
		addRatingStep(rateArea, "Loss Surcharge Factor", "Loss Surcharge Factor: "+lossSurchargeFactor, "x",lossSurchargeFactor,covAPremium);

		return covAPremium;	
	}

	private BigDecimal applyCoastalReinsuranceFactor ( ModelBean rateArea ,  BigDecimal covAPremium, String fullZipCode   ) throws Exception {
		String lookUpZipCode="";
		if(fullZipCode !=null && fullZipCode.length() > 5){
			lookUpZipCode = fullZipCode.substring(0,5);
		}else{
			lookUpZipCode = fullZipCode;
		}
		if(!lookUpZipCode.equals("")){
			RatingTable table = tables.get("CoastalReinsuranceFactor");
			BigDecimal coastalReinsuranceFactor = table.lookupSingleRow("ZipCode", lookUpZipCode).getCell("Factor").getBigDecimal().setScale(2, BigDecimal.ROUND_HALF_UP);
			covAPremium = covAPremium.multiply(coastalReinsuranceFactor).setScale(2, BigDecimal.ROUND_HALF_UP);			
			addRatingStep(rateArea, "Coastal Reinsurance Factor", "Coastal Reinsurance Premium:  "+coastalReinsuranceFactor , "x",coastalReinsuranceFactor.setScale(3, BigDecimal.ROUND_HALF_UP ) ,covAPremium.setScale(2, BigDecimal.ROUND_HALF_UP ) );
		}

		return covAPremium;	
	}

	private BigDecimal applyGroupDiscountPremium ( ModelBean rateArea ,  BigDecimal covAPremium   ) throws Exception {
		RatingTable table = tables.get("GroupDiscount");
		BigDecimal groupDiscountFactor = table.lookupSingleRow("Group", "No").getCell("Discount").getBigDecimal().setScale(2, BigDecimal.ROUND_HALF_UP);
		covAPremium = covAPremium.multiply(groupDiscountFactor).setScale(2, BigDecimal.ROUND_HALF_UP);			
		addRatingStep(rateArea, "Group Discount Factor", "Group Discount: "+groupDiscountFactor , "x",groupDiscountFactor.setScale(3, BigDecimal.ROUND_HALF_UP ) ,covAPremium.setScale(2, BigDecimal.ROUND_HALF_UP ) );

		return covAPremium;	
	}	

	public void rateEqBreakdownCoverage(ModelBean coverage) throws RatingException{
		try{
			Log.debug("Rating Coverage "+coverage.gets("CoverageCd"));
			String coverageDesc = coverage.gets("Description");
			RatingTable table = tables.get("EquipBreakdown");
			String covALimit = getCovALimit().toString();
			String colVal = "";
			if (StringTools.between(covALimit, "0-999999")){
				colVal = "LT1MILLION";
			}else {
				colVal = "GTEQ1MILLION";
			}
			BigDecimal premium = table.lookupSingleRow("CoverageALimit", colVal).getCell("Premium").getBigDecimal().setScale(2, BigDecimal.ROUND_HALF_UP);
			ModelBean step = addRatingStep(coverage, coverageDesc+" premium", "", "", "",premium.setScale(2, BigDecimal.ROUND_HALF_UP));
			step.setValue("Value", premium.setScale(2, BigDecimal.ROUND_HALF_UP) );
			step.setValue("Factor", premium.setScale(2, BigDecimal.ROUND_HALF_UP));
			coverage.setValue("FullTermAmt", premium.setScale(2,BigDecimal.ROUND_HALF_UP));
			totalPremium = totalPremium.add(new BigDecimal(coverage.gets("FullTermAmt")));

		}catch(Exception e){
			throw new RatingException(e);
		}
	}	

	/** 
	 * Rate Escaped Liquid Fuel Liability Coverage
	 * @param coverage
	 * @throws RatingException
	 */
	public void rateEscapedLiquidCoverage(ModelBean coverage) throws RatingException{
		try{
			Log.debug("Rating Coverage "+coverage.gets("CoverageCd"));
			ModelBean building = getBuildingFromCoverage(coverage);
			String fuelTankLocation = building.gets("FuelTankLocation");
			String classFactor = "";
			if (fuelTankLocation.contains("Under")){
				classFactor = "Under Ground";
			}else if (fuelTankLocation.contains("Above")){
				classFactor = "Above Ground";
			}
			BigDecimal premium = new BigDecimal("0.00");
			if( !fuelTankLocation.equalsIgnoreCase("") ) {
				RatingTable  escapedLiquidCoverage = tables.get("EscapedFuel");
				Row [] row = escapedLiquidCoverage.lookup(new String[] {"Location"} , new String[] {classFactor} , Table.MATCH_TYPE_STRING_CASE_INSENSITIVE);
				if (row.length > 0) {
					premium = row[0].getCell("Premium").getBigDecimal().setScale(2 , BigDecimal.ROUND_HALF_UP); 
				}
			}
			addRatingStep( coverage, "Escaped Liquid Fuel Liability Premium", "Fuel Tank Location: "+classFactor , "", premium ,premium);     		
			coverage.setValue("FullTermAmt", premium);
			totalPremium = totalPremium.add(new BigDecimal(coverage.gets("FullTermAmt")));

		}catch(Exception e){
			throw new RatingException(e);
		}
	} 

	public ModelBean getApplication() {
		return application;
	}


	public void setApplication(ModelBean application) {
		this.application = application;
	}


	public ModelBean getBasicPolicy() {
		return basicPolicy;
	}


	public void setBasicPolicy(ModelBean basicPolicy) {
		this.basicPolicy = basicPolicy;
	}


	public BigDecimal getBasicPolicyPremium() {
		return basicPolicyPremium;
	}


	public void setBasicPolicyPremium(BigDecimal basicPolicyPremium) {
		this.basicPolicyPremium = basicPolicyPremium;
	}

	/**
	 * Returns a list of loss history beans from application. Only active status and ingore indicator
	 * no.
	 * @return
	 */
	public ModelBean[] getAllActiveNonIgnoredLosses() {

		try {
			ArrayList<ModelBean> filteredLosses = new ArrayList<ModelBean>();
			ModelBean[] lossHistory = application.findBeansByFieldValue("LossHistory" , "StatusCd", "Active");
			StringDate lossCountableDt = DateRenderer.advanceMonth(application.getBean("BasicPolicy").getDate("EffectiveDt"), "-36");
			for (ModelBean loss: lossHistory) {
				if(!StringTools.isTrue( loss.gets("IgnoreInd")) && DateRenderer.compareTo(loss.getDate("LossDt"), lossCountableDt)>=0 && !loss.gets("LossAmt").equals("") && StringRenderer.greaterThan(loss.gets("LossAmt"), "0"))
					filteredLosses.add(loss);
			}

			return filteredLosses.toArray(new ModelBean[filteredLosses.size()]);    		
		} catch (Exception e) {
			return new ModelBean[0];    		
		}
	}

	/**
	 * Returns a list of loss history beans from application. Only active status and ingore indicator
	 * no.
	 * @return
	 */
	public ModelBean[] getAllActiveMigratedNonIgnoredLosses() {

		try {
			ArrayList<ModelBean> filteredLosses = new ArrayList<ModelBean>();
			ModelBean[] lossHistory = application.findBeansByFieldValue("LossHistory" , "StatusCd", "Active");
			StringDate lossCountableDt = DateRenderer.advanceMonth(application.getBean("BasicPolicy").getDate("EffectiveDt"), "-36");
			for (ModelBean loss: lossHistory) {
				if(!StringTools.isTrue( loss.gets("IgnoreInd")) && 
						DateRenderer.compareTo(loss.getDate("LossDt"), lossCountableDt)>=0 &&
						!loss.gets("LossAmt").equals("") &&
						StringRenderer.greaterThan(loss.gets("LossAmt"), "0")) 
				{
					filteredLosses.add(loss);
				}
			}

			return filteredLosses.toArray(new ModelBean[filteredLosses.size()]);    		
		} catch (Exception e) {
			return new ModelBean[0];    		
		}
	}


	public BigDecimal getCovAPremFireOrdinance() {
		return covAPremFireOrdinance;
	}


	public void setCovAPremFireOrdinance(BigDecimal covAPremFireOrdinance) {
		this.covAPremFireOrdinance = covAPremFireOrdinance;
	}


	public BigDecimal getCovBPremFireOrdinance() {
		return covBPremFireOrdinance;
	}


	public void setCovBPremFireOrdinance(BigDecimal covBPremFireOrdinance) {
		this.covBPremFireOrdinance = covBPremFireOrdinance;
	}


	public BigDecimal getCovDPremFireOrdinance() {
		return covDPremFireOrdinance;
	}


	public void setCovDPremFireOrdinance(BigDecimal covDPremFireOrdinance) {
		this.covDPremFireOrdinance = covDPremFireOrdinance;
	}


	public BigDecimal getCovEPremFireOrdinance() {
		return covEPremFireOrdinance;
	}


	public void setCovEPremFireOrdinance(BigDecimal covEPremFireOrdinance) {
		this.covEPremFireOrdinance = covEPremFireOrdinance;
	}


	public BigDecimal getCovAPremECOrdinance() {
		return covAPremECOrdinance;
	}


	public void setCovAPremECOrdinance(BigDecimal covAPremECOrdinance) {
		this.covAPremECOrdinance = covAPremECOrdinance;
	}


	public BigDecimal getCovBPremECOrdinance() {
		return covBPremECOrdinance;
	}


	public void setCovBPremECOrdinance(BigDecimal covBPremECOrdinance) {
		this.covBPremECOrdinance = covBPremECOrdinance;
	}


	public BigDecimal getCovDPremECOrdinance() {
		return covDPremECOrdinance;
	}


	public void setCovDPremECOrdinance(BigDecimal covDPremECOrdinance) {
		this.covDPremECOrdinance = covDPremECOrdinance;
	}


	public BigDecimal getCovEPremECOrdinance() {
		return covEPremECOrdinance;
	}


	public void setCovEPremECOrdinance(BigDecimal covEPremECOrdinance) {
		this.covEPremECOrdinance = covEPremECOrdinance;
	}


	public BigDecimal getCovCPremFireOrdinance() {
		return covCPremFireOrdinance;
	}


	public void setCovCPremFireOrdinance(BigDecimal covCPremFireOrdinance) {
		this.covCPremFireOrdinance = covCPremFireOrdinance;
	}


	public BigDecimal getCovPremFireOrdinance() {
		return covPremFireOrdinance;
	}


	public void setCovPremFireOrdinance(BigDecimal covPremFireOrdinance) {
		this.covPremFireOrdinance = covPremFireOrdinance;
	}


	public BigDecimal getCovCPremECOrdinance() {
		return covCPremECOrdinance;
	}


	public void setCovCPremECOrdinance(BigDecimal covCPremECOrdinance) {
		this.covCPremECOrdinance = covCPremECOrdinance;
	}


	public BigDecimal getCovPremECOrdinance() {
		return covPremECOrdinance;
	}


	public void setCovPremECOrdinance(BigDecimal covPremECOrdinance) {
		this.covPremECOrdinance = covPremECOrdinance;
	}

	public void applyMinimumPremium(ModelBean application)throws RatingException {
		try {
			ModelBean [] coverages  = application.findBeansByFieldValue("Coverage", "Status","Active");
			BigDecimal totalPremium = zero;
			for (ModelBean coverage : coverages) {
				if(!coverage.gets("FullTermAmt").equals(""))
					totalPremium =  totalPremium.add(new BigDecimal(coverage.gets("FullTermAmt")));
			}
			RatingTable table = tables.get("MinPremium");
			BigDecimal minPremium = table.getRow(0).getCell("MinimumPremium").getBigDecimal().setScale(2, BigDecimal.ROUND_HALF_UP);
			if(minPremium.compareTo(totalPremium)>0){
				BigDecimal appliedMinPremiumFinal = minPremium.subtract(totalPremium);
				BigDecimal appliedMinPremium = minPremium.subtract(totalPremium);
				BigDecimal covAPrem = zero;
				BigDecimal covBPrem = zero;
				BigDecimal covCPrem = zero;
				BigDecimal covDPrem = zero;
				BigDecimal covEPrem = zero;
				BigDecimal covLPrem = zero;
				BigDecimal covMPrem = zero;
				ModelBean covA = null;
				ModelBean covB = null;
				ModelBean covC = null;
				ModelBean covD = null;
				ModelBean covE = null;
				ModelBean covL = null;
				ModelBean covM = null;
				if(CoverageRenderer.isActive(application, "CovA")){
					covA = CoverageRenderer.getActiveCoverage(application, "CovA");
					if(!covA.gets("FullTermAmt").equals(""))
						covAPrem=  new BigDecimal(covA.gets("FullTermAmt"));
				}
				if(CoverageRenderer.isActive(application, "CovB")){
					covB = CoverageRenderer.getActiveCoverage(application, "CovB");
					if(!covB.gets("FullTermAmt").equals(""))
						covBPrem=  new BigDecimal(covB.gets("FullTermAmt"));
				}
				if(CoverageRenderer.isActive(application, "CovC")){
					covC = CoverageRenderer.getActiveCoverage(application, "CovC");
					if(!covC.gets("FullTermAmt").equals(""))
						covCPrem=  new BigDecimal(covC.gets("FullTermAmt"));
				}
				if(CoverageRenderer.isActive(application, "CovD")){
					covD = CoverageRenderer.getActiveCoverage(application, "CovD");
					if(!covD.gets("FullTermAmt").equals(""))
						covDPrem=  new BigDecimal(covD.gets("FullTermAmt"));
				}
				if(CoverageRenderer.isActive(application, "CovE")){
					covE = CoverageRenderer.getActiveCoverage(application, "CovE");
					if(!covE.gets("FullTermAmt").equals(""))
						covEPrem=  new BigDecimal(covE.gets("FullTermAmt"));
				}
				if(CoverageRenderer.isActive(application, "CovL")){
					covL = CoverageRenderer.getActiveCoverage(application, "CovL");
					if(!covL.gets("FullTermAmt").equals(""))
						covLPrem=  new BigDecimal(covL.gets("FullTermAmt"));
				}
				if(CoverageRenderer.isActive(application, "CovM")){
					covM = CoverageRenderer.getActiveCoverage(application, "CovM");
					if(!covM.gets("FullTermAmt").equals(""))
						covMPrem=  new BigDecimal(covM.gets("FullTermAmt"));
				}
				BigDecimal totalManCovPrem = covAPrem.add(covBPrem).add(covCPrem).add(covDPrem).add(covEPrem).add(covLPrem).add(covMPrem);
				if(covMPrem.compareTo(zero)>0 && CoverageRenderer.isActive(application, "CovM") && covM!=null){
					BigDecimal covMPercent = covMPrem.divide(totalManCovPrem, 4, BigDecimal.ROUND_HALF_UP);
					BigDecimal covMMinPrem=appliedMinPremiumFinal.multiply(covMPercent).setScale(2, BigDecimal.ROUND_HALF_UP);
					appliedMinPremium=appliedMinPremium.subtract(covMMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
					covMPrem=covMPrem.add(covMMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
					addRatingStep(covM, "Minimum Premium" , "Minimum Premium", "+", covMMinPrem, covMPrem);
					covM.setValue("FullTermAmt", covMPrem);
				}
				if(covLPrem.compareTo(zero)>0 && CoverageRenderer.isActive(application, "CovL") && covL!=null){
					BigDecimal covLPercent = covLPrem.divide(totalManCovPrem, 4, BigDecimal.ROUND_HALF_UP);
					BigDecimal covLMinPrem=appliedMinPremiumFinal.multiply(covLPercent).setScale(2, BigDecimal.ROUND_HALF_UP);
					appliedMinPremium=appliedMinPremium.subtract(covLMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
					covLPrem=covLPrem.add(covLMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
					addRatingStep(covL, "Minimum Premium" , "Minimum Premium", "+", covLMinPrem, covLPrem);
					covL.setValue("FullTermAmt", covLPrem);
				}
				if(covEPrem.compareTo(zero)>0 && CoverageRenderer.isActive(application, "CovE") && covE!=null){
					BigDecimal covEPercent = covEPrem.divide(totalManCovPrem, 4, BigDecimal.ROUND_HALF_UP);
					BigDecimal covEMinPrem=appliedMinPremiumFinal.multiply(covEPercent).setScale(2, BigDecimal.ROUND_HALF_UP);
					appliedMinPremium=appliedMinPremium.subtract(covEMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
					covEPrem=covEPrem.add(covEMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
					applyMinimumPremium(covE, covEMinPrem);
					covE.setValue("FullTermAmt", covEPrem);
				}
				if(covDPrem.compareTo(zero)>0 && CoverageRenderer.isActive(application, "CovD") && covD!=null){
					BigDecimal covDPercent = covDPrem.divide(totalManCovPrem, 4, BigDecimal.ROUND_HALF_UP);
					BigDecimal covDMinPrem=appliedMinPremiumFinal.multiply(covDPercent).setScale(2, BigDecimal.ROUND_HALF_UP);
					appliedMinPremium=appliedMinPremium.subtract(covDMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
					covDPrem=covDPrem.add(covDMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
					applyMinimumPremium(covD, covDMinPrem);
					covD.setValue("FullTermAmt", covDPrem);
				}
				if(covBPrem.compareTo(zero)>0 && CoverageRenderer.isActive(application, "CovB") && covB!=null){
					BigDecimal covBPercent = covBPrem.divide(totalManCovPrem, 4, BigDecimal.ROUND_HALF_UP);
					BigDecimal covBMinPrem=appliedMinPremiumFinal.multiply(covBPercent).setScale(2, BigDecimal.ROUND_HALF_UP);
					appliedMinPremium=appliedMinPremium.subtract(covBMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
					covBPrem=covBPrem.add(covBMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
					applyMinimumPremium(covB, covBMinPrem);
					covB.setValue("FullTermAmt", covBPrem);
				}
				if(covCPrem.compareTo(zero)>0 && CoverageRenderer.isActive(application, "CovC") && covC!=null){
					BigDecimal covCPercent = covCPrem.divide(totalManCovPrem, 4, BigDecimal.ROUND_HALF_UP);
					BigDecimal covCMinPrem=appliedMinPremiumFinal.multiply(covCPercent).setScale(2, BigDecimal.ROUND_HALF_UP);
					appliedMinPremium=appliedMinPremium.subtract(covCMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
					covCPrem=covCPrem.add(covCMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
					applyMinimumPremium(covC, covCMinPrem);
					covC.setValue("FullTermAmt", covCPrem);
				}
				if(covAPrem.compareTo(zero)>0 && CoverageRenderer.isActive(application, "CovA") && covA!=null){
					covAPrem=covAPrem.add(appliedMinPremium).setScale(2, BigDecimal.ROUND_HALF_UP);
					applyMinimumPremium(covA, appliedMinPremium);
					covA.setValue("FullTermAmt", covAPrem);
				}
				totalPremium=minPremium;
			}
			BigDecimal roundedTotalPremium =NumberTools.roundToDollar(totalPremium);
			BigDecimal adjustedPremium = roundedTotalPremium.subtract(totalPremium).setScale(2, BigDecimal.ROUND_HALF_UP);
			if(adjustedPremium.compareTo(zero)!=0){
				ModelBean covA = CoverageRenderer.getActiveCoverage(application, "CovA");
				ModelBean rateAreaFire = covA.findBeanByFieldValue("RateArea", "AreaName", "Fire");
				if(rateAreaFire!=null){
					if(!rateAreaFire.gets("FullTermAmt").equals("")){
						BigDecimal rateAreaFirePrem= new BigDecimal(rateAreaFire.gets("FullTermAmt")).add(adjustedPremium).setScale(2, BigDecimal.ROUND_HALF_UP);
						addRatingStep(rateAreaFire, "Round Difference" , "Round Difference", "+", adjustedPremium, rateAreaFirePrem);
						rateAreaFire.setValue("FullTermAmt", rateAreaFirePrem);
					}
				}
				BigDecimal covAPrem= new BigDecimal(covA.gets("FullTermAmt")).add(adjustedPremium).setScale(2, BigDecimal.ROUND_HALF_UP);
				covA.setValue("FullTermAmt", covAPrem);
			}

		} catch (Exception e) {
			throw new RatingException(e);
		}
	}
	public void applyMinimumPremium(ModelBean coverage, BigDecimal appliedMinPremium)throws RatingException {
		try {
			BigDecimal rateAreaFirePrem = zero;
			BigDecimal rateAreaECPrem = zero;
			BigDecimal rateAreaVMMPrem = zero;
			ModelBean rateAreaFire = null;
			ModelBean rateAreaEC = null;
			ModelBean rateAreaVMM = null;
			BigDecimal appliedMinPremiumFinal = appliedMinPremium;
			rateAreaFire = coverage.findBeanByFieldValue("RateArea", "AreaName", "Fire");
			if(rateAreaFire!=null){
				if(!rateAreaFire.gets("FullTermAmt").equals(""))
					rateAreaFirePrem=  new BigDecimal(rateAreaFire.gets("FullTermAmt"));
			}
			rateAreaEC = coverage.findBeanByFieldValue("RateArea", "AreaName", "Extended Coverage");
			if(rateAreaEC!=null){
				if(!rateAreaEC.gets("FullTermAmt").equals(""))
					rateAreaECPrem=  new BigDecimal(rateAreaEC.gets("FullTermAmt"));
			}

			rateAreaVMM = coverage.findBeanByFieldValue("RateArea", "AreaName", "VMM");
			if(rateAreaVMM!=null){
				if(!rateAreaVMM.gets("FullTermAmt").equals(""))
					rateAreaVMMPrem=  new BigDecimal(rateAreaVMM.gets("FullTermAmt"));
			}
			BigDecimal totalManCovPrem = rateAreaFirePrem.add(rateAreaECPrem).add(rateAreaVMMPrem);
			if(rateAreaVMMPrem.compareTo(zero)>0 && rateAreaVMM!=null){
				BigDecimal rateAreaVMMPercent = rateAreaVMMPrem.divide(totalManCovPrem, 4, BigDecimal.ROUND_HALF_UP);
				BigDecimal rateAreaVMMMinPrem=appliedMinPremiumFinal.multiply(rateAreaVMMPercent).setScale(2, BigDecimal.ROUND_HALF_UP);
				appliedMinPremium=appliedMinPremium.subtract(rateAreaVMMMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
				rateAreaVMMPrem=rateAreaVMMPrem.add(rateAreaVMMMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
				addRatingStep(rateAreaVMM, "Minimum Premium" , "Minimum Premium", "+", rateAreaVMMMinPrem, rateAreaVMMPrem);
				rateAreaVMM.setValue("FullTermAmt", rateAreaVMMPrem);
			}
			if(rateAreaECPrem.compareTo(zero)>0 && rateAreaEC!=null){
				BigDecimal rateAreaECPercent = rateAreaECPrem.divide(totalManCovPrem, 4, BigDecimal.ROUND_HALF_UP);
				BigDecimal rateAreaECMinPrem=appliedMinPremiumFinal.multiply(rateAreaECPercent).setScale(2, BigDecimal.ROUND_HALF_UP);
				appliedMinPremium=appliedMinPremium.subtract(rateAreaECMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
				rateAreaECPrem=rateAreaECPrem.add(rateAreaECMinPrem).setScale(2, BigDecimal.ROUND_HALF_UP);
				addRatingStep(rateAreaEC, "Minimum Premium" , "Minimum Premium", "+", rateAreaECMinPrem, rateAreaECPrem);
				rateAreaEC.setValue("FullTermAmt", rateAreaECPrem);
			}

			if(rateAreaFirePrem.compareTo(zero)>0 && CoverageRenderer.isActive(application, "CovA") && rateAreaFire!=null){
				rateAreaFirePrem=rateAreaFirePrem.add(appliedMinPremium).setScale(2, BigDecimal.ROUND_HALF_UP);
				addRatingStep(rateAreaFire, "Minimum Premium" , "Minimum Premium", "+", appliedMinPremium, rateAreaFirePrem);
				rateAreaFire.setValue("FullTermAmt", rateAreaFirePrem);
			}

		} catch (Exception e) {
			throw new RatingException(e);
		}
	}
	/**
	 * Rate WaterBackup Coverage
	 * @param coverage
	 * @throws RatingException
	 */
	public void rateWaterBackupCoverage(ModelBean coverage) throws RatingException{
		try{
			Log.debug("Rating Coverage "+coverage.gets("CoverageCd"));
			// Initialize the premium to 0
			BigDecimal premium = new BigDecimal("0.00");
			BigDecimal limit = new BigDecimal("0");         	
			ModelBean building = getBuildingFromCoverage(coverage);
			if( building.gets("WaterBackup").equalsIgnoreCase("Yes") ) {
				String waterBackupLimit = building.gets("WaterBackupLimit");
				RatingTable  waterBackupTable = tables.get("WaterBackup");

				limit = new BigDecimal(waterBackupLimit);
				Row [] row = waterBackupTable.lookup(new String[] {"Limit"} , new String[] {limit.toString()} , Table.MATCH_TYPE_STRING_CASE_INSENSITIVE);
				if (row.length > 0) {
					premium = row[0].getCell("Premium").getBigDecimal().setScale(2 , BigDecimal.ROUND_HALF_UP); 
				}
			}
			addRatingStep( coverage, "Water Back-up and Sump Discharge or Overflow Premium", "Limit: "+fmt.format(limit) , "", premium ,premium);     		
			coverage.setValue("FullTermAmt", premium);
			totalPremium = totalPremium.add(new BigDecimal(coverage.gets("FullTermAmt")));

		}catch(Exception e){
			throw new RatingException(e);
		}
	}

	/**
	 * Returns a list of loss history beans from application. Only active status and ingore indicator
	 * no.
	 * @return
	 */
	public ModelBean[] getAllActiveNonIgnoredLossesPostInception() {
		try {
			ArrayList<ModelBean> filteredLosses = new ArrayList<ModelBean>();
			ModelBean[] lossHistory = application.findBeansByFieldValue("LossHistory" , "StatusCd", "Active");
			StringDate inceptionDt = application.getBean("BasicPolicy").getDate("InceptionDt");
			for (ModelBean loss: lossHistory) {
				if(!StringTools.isTrue( loss.gets("IgnoreInd")) && DateRenderer.compareTo(loss.getDate("LossDt"), inceptionDt)>=0 && !loss.gets("PaidAmt").equals("") && StringRenderer.greaterThan(loss.gets("PaidAmt"), "0"))
					filteredLosses.add(loss);
			}			
			return filteredLosses.toArray(new ModelBean[filteredLosses.size()]);    		
		} catch (Exception e) {
			return new ModelBean[0];    		
		}
	}

	/**
	 * Returns a list of loss history beans from application. Only active status and ingore indicator
	 * no.
	 * @return
	 */
	public ModelBean[] getAllActiveMigratedNonIgnoredLossesPostInception() {
		try {
			ArrayList<ModelBean> filteredLosses = new ArrayList<ModelBean>();
			ModelBean[] lossHistory = application.findBeansByFieldValue("LossHistory" , "StatusCd", "Active");
			StringDate inceptionDt = application.getBean("BasicPolicy").getDate("InceptionDt");
			for (ModelBean loss: lossHistory) {
				if(!StringTools.isTrue( loss.gets("IgnoreInd")) &&
						DateRenderer.compareTo(loss.getDate("LossDt"), inceptionDt)>=0 &&
						!loss.gets("PaidAmt").equals("") &&
						StringRenderer.greaterThan(loss.gets("PaidAmt"), "0"))
				{
					filteredLosses.add(loss);
				}
			}			
			return filteredLosses.toArray(new ModelBean[filteredLosses.size()]);    		
		} catch (Exception e) {
			return new ModelBean[0];    		
		}
	}

	/**
	 * Goes through the lossHistory and if there are two losses on the same day, returns one with highest amount. 
	 * @param Losses 
	 * return loss with highest amount if there are two losses on the same day.
	 */
	public static ModelBean[] getLossHistory(ModelBean[] losses)throws  Exception{
		ModelBean[] lossHistory=null;
		if(losses!=null && losses.length>0){
			Map<String, ModelBean> lossMap = new HashMap<String, ModelBean>();
			for (ModelBean loss : losses) {
				String lossDt=loss.gets("LossDt");
				if(lossMap.get(lossDt)!=null){
					ModelBean mapLoss=lossMap.get(lossDt);
					if(StringRenderer.greaterThan(loss.gets("PaidAmt"), mapLoss.gets("PaidAmt"))){
						lossMap.put(lossDt, loss);
					}
				}else{
					lossMap.put(lossDt, loss);
				}
			}
			List<ModelBean> lossList= new ArrayList<ModelBean>();
			for (String lossDt : lossMap.keySet()) {		
				lossList.add(lossMap.get(lossDt));
			}
			if(lossList!=null && lossList.size()>0){
				lossHistory=(ModelBean[])lossList.toArray(new ModelBean[lossList.size()]);
				return lossHistory;
			}
		}  
		return new ModelBean[0];  
	}


	private boolean isApplicationReadyForRating(ModelBean application) {
		try {
			ModelBean[] buildings = application.findBeansByFieldValue("Building", "Status", "Active");

			for (ModelBean building : buildings ) {
				String sqFt = building.gets("SqFt");
				String allPerilDed = building.gets("AllPerilDed");
				String constructionCd = building.gets("ConstructionCd");
				if (sqFt.equals("") || !StringTools.isNumeric(sqFt) || allPerilDed.equals("") || constructionCd.equals("") ) {
					return false;
				}
			}

			return true;
		}
		catch (Exception e) {
			Log.error("General Error: " + e.getMessage());			
		}

		return true;
	}

}


//