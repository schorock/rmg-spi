/*
 * HOConversionToDW.java
 * 
 */
package com.ric.uw.common.product.model.template.ric.homeowners.cw.v01_00_00.rule;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.insurance.product.ProductMaster;
import com.iscs.insurance.product.ProductVersion;
import com.iscs.uw.common.shared.Form;
import com.iscs.uw.policy.PM;

/**
 * Product rule/handler to convert a Homeowners
 * application to a Dwelling application
 * 
 * @author julies
 * 
 */
public class HOConversionToDW implements RuleProcessor {

	/**
	 * Rule processor interface
	 * 
	 * @param ruleTemplate
	 *            Rule template
	 * @param bean
	 *            ModelBean to process
	 * @param user
	 *            User bean
	 * @param data
	 *            Database connection
	 * @return Rule Errors bean
	 * @throws RuleException
	 *             for rule errors
	 */
	@Override
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user, JDBCData data)
			throws RuleException {
		ModelBean errors = null;
		try {
			errors = new ModelBean("Errors");
			convertHomeownersToDwelling(ruleTemplate, bean, errors, data, additionalBeans[0]);

		} catch (Exception e) {
			throw new RuleException(e);
		}

		return errors;
	}

	private void convertHomeownersToDwelling(ModelBean ruleTemplate, ModelBean container, ModelBean errors, JDBCData data,
			ModelBean sourceContainer) throws ModelBeanException {
		try {
			// Determine if Conversion is Being Processed or Will Be Processed - Affects Wording in the Error Message
			boolean preprocessInd = false;
			ModelBean param = ruleTemplate.getBean("Param", "Name", "PreprocessInd");
			if (param != null) {
				if (StringTools.isTrue(param.gets("Value")))
					preprocessInd = true;
			}

			// already updated to the new product version, so don't need to set it
			String productVersionIdRef = container.getBean("BasicPolicy").gets("ProductVersionIdRef");

			// Convert BasicPolicy 
			ModelBean basicPolicy = container.getBean("BasicPolicy");

			// set default values for sub type-- this will be changed if the policy form
			//	is changed by the user
			//  must also set the default Policy form (to prevent problems in sub-type.vtl)
			ModelBean replies = container.getBean("QuestionReplies");
			if (replies == null) {
				replies = new ModelBean("QuestionReplies");
				container.addValue(replies);
			}
			ModelBean reply = new ModelBean("QuestionReply");
			reply.setValue("Name", "PolicyForm");
			if (basicPolicy.gets("SubTypeCd").equals("Non-Standard")) {
				basicPolicy.setValue("SubTypeCd", "Special");
				reply.setValue("Value", "Special");
			} else {
				basicPolicy.setValue("SubTypeCd", "Broad");
				reply.setValue("Value", "Broad");
			}
			replies.addValue(reply);

			// the QuestionSourceMDA string on all questions must be updated 
			//	with the new productVersionIdRef
			ModelBean[] questionReplies = container.getAllBeans("QuestionReplies");
			for (int i = 0; i < questionReplies.length; i++) {
				String questionSourceMDA = questionReplies[i].gets("QuestionSourceMDA");
				String array[] = questionSourceMDA.split("::");
				if (array.length >= 4) {
					String[] array2 = array[3].split("-Homeowners-");
					if (array2.length == 2) {
						array[3] = array2[0] + "-Dwelling-" + array2[1];
					}
					questionSourceMDA = array[0] + "::" + array[1] + "::" + productVersionIdRef + "::" + array[3];
					questionReplies[i].setValue("QuestionSourceMDA", questionSourceMDA);
				}
			}

			ProductVersion productVersion = ProductMaster.getProductMaster("UW").getProductVersion(productVersionIdRef);
			basicPolicy.setValue("CarrierGroupCd",
					productVersion.getBean().getParentBean().getParentBean().getParentBean().gets("CarrierGroupIdRef"));
			// this is usually set in sub-type.vtl
			basicPolicy.setValue("CarrierCd", "");

			// Change the description unless this is a linked quote
			if (container.getBeanName().equals("Policy") || container.getBean("ApplicationInfo").gets("MasterQuoteRef").equals(""))
				basicPolicy.setValue("Description", productVersion.getBean().getParentBean().gets("Name"));

			// Convert CommissionArea-- no need for this sample product

			// Convert the underwriting Questions; for this product, questions overlap, so 
			//	there is nothing to convert

			// Convert the lines; in this sample product, only one line per container
			ModelBean line = container.getBean("Line");
			line.setValue("LineCd", "Dwelling");

			// Convert the risks
			ModelBean[] risks = line.getAllBeans("Risk");
			for (int i = 0; i < risks.length; i++) {
				risks[i].setValue("TypeCd", "Dwelling");
				risks[i].setValue("Description", "Dwelling");

				ModelBean[] cvgs = risks[i].getAllBeans("Coverage");
				for (int j = 0; j < cvgs.length; j++) {
					// convert the coverages
					String cvgCd = cvgs[j].gets("CoverageCd");
					if (cvgCd.equals("ARR")) {
						risks[i].deleteBeanById(cvgs[j].getId());
						if (preprocessInd)
							errors.addValue(ErrorTools.createError(
									"General",
									"Additional Residence Rented to Others coverage will be removed from risk '"
											+ risks[i].gets("Description") + "'; coverage should be re-added.",
									ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_WARN));
						else
							errors.addValue(ErrorTools.createError(
									"General",
									"Additional Residence Rented to Others coverage was removed from risk '"
											+ risks[i].gets("Description") + "'; coverage should be re-added.",
									ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_WARN));
					} else if (cvgCd.equals("ROOF")) {
						risks[i].deleteBeanById(cvgs[j].getId());
						if (preprocessInd)
							errors.addValue(ErrorTools.createError("General", "Roof Exclusion coverage will be removed from risk '"
									+ risks[i].gets("Description") + "'.", ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_WARN));
						else
							errors.addValue(ErrorTools.createError("General", "Roof Exclusion coverage was removed from risk '"
									+ risks[i].gets("Description") + "'.", ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_WARN));
					} else if (cvgCd.equals("CovL")) {
						line.setValue("CovLLimit", risks[i].getBean("Building").gets("CovLLimit"));
					} else if (cvgCd.equals("CovM")) {
						line.setValue("CovMLimit", risks[i].getBean("Building").gets("CovMLimit"));
					}
				}
			}

			// Convert PolicyNumber
			//	a new policy number will be generated at close-out for 
			//	transactions Renewal and Rewrite-Renewal if the PolicyNumber field
			//	is blank; if you wish to use the same policy number, skip this step
			String transactionCd = container.getBean("TransactionInfo").gets("TransactionCd");
			String validTransactions = PM.TX_RENEWAL + "," + PM.TX_RENEWAL_START + "," + PM.TX_REWRITE_RENEWAL;
			if (StringTools.in(transactionCd, validTransactions)) {
				basicPolicy.setValue("PolicyNumber", "");
				basicPolicy.setValue("PolicyDisplayNumber", "");
			}

			// Convert the fees
			// 	in this sample, just delete any fees as they will be rebuilt on the new product.
			//  If possible, it would be better to try to map fees in the source product to matching
			//  fees in the destination product
			ModelBean[] fees = container.getAllBeans("Fee");
			for (int i = 0; i < fees.length; i++) {
				container.deleteBeanById(fees[i].getId());
			}

			// Convert the forms
			// 	in this sample, just delete any attached forms (warning on deletion of optional ones).
			//  If possible, it would be better to try to map optional forms in the source product to matching
			//  optional forms in the destination product
			StringDate effectiveDt = basicPolicy.getDate("EffectiveDt");
			ModelBean[] formArray = container.getAllBeans("Form");
			for (ModelBean form : formArray) {
				// Get the ProductForm(s) Whose Name Matches the Form Name Parameter     
				ModelBean productForm = Form.getProductForm(sourceContainer, form.gets("Name"), effectiveDt);
				if (productForm != null && productForm.gets("AttachmentMethodCd").equals("Optional")) {
					if (preprocessInd)
						errors.addValue(ErrorTools.createError("General",
								"Optional form '" + form.gets("Name") + "' will be removed.", ErrorTools.GENERIC_BUSINESS_ERROR,
								ErrorTools.SEVERITY_WARN));
					else
						errors.addValue(ErrorTools.createError("General", "Optional form '" + form.gets("Name") + "' was removed.",
								ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_WARN));
				}
				container.deleteBeanById(form.getId());
			}

		} catch (Exception e) {
			throw new ModelBeanException(e);
		}

	}

}
