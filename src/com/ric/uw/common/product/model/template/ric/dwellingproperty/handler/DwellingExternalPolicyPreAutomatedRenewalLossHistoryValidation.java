/*
 * ExternalPolicyPreAutomatedRenewalValidation.java
 *
 */

package com.ric.uw.common.product.model.template.ric.dwellingproperty.handler;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;

import com.iscs.common.tech.service.ExternalServiceBase;
import com.iscs.common.tech.service.ExternalServiceHandler;

/** Set up and call the process to pre-validate the automated renewal
 *
 * @author moniquef
 */
public class DwellingExternalPolicyPreAutomatedRenewalLossHistoryValidation extends ExternalServiceBase implements ExternalServiceHandler {
    
    /** Creates a new instance of ExternalPolicyPreAutomatedRenewalValidation */
    public DwellingExternalPolicyPreAutomatedRenewalLossHistoryValidation() throws Exception {
        super();
    }
    
    /** Obtain the Webpath service name that should be called associated with this external service call
     */
    public String getServiceName() {
        return "UWExternalPolicyPreAutomatedRenewalLossHistoryValidation";
    }
    
    /** Process the results from the external webpath service call
     * @param response The response ModelBean
     * @param bean The container bean associated with this service call
     * @param user The user bean
     * @param data The JDBC data repository connection
     * @return ModelBean response ModelBean
     * @throws Exception when an unexpected error occurs
     * @throws IBIZException when a business error occurs
     * @throws ServiceHandlerException when a service handler error occurs
     */ 
    public ModelBean processEnd(ModelBean response, ModelBean bean, ModelBean user, JDBCData data)
    throws Exception, IBIZException, ServiceHandlerException {
        return response;
    }
    
    /** Process the setup information required to call the external webpath service.
     * @param request The request ModelBean from the current external service call
     * @param bean The container bean associated with this service call
     * @param user The user bean
     * @param data The JDBC data repository connection
     * @return ModelBean response ModelBean
     * @throws Exception when an unexpected error occurs
     * @throws IBIZException when a business error occurs
     * @throws ServiceHandlerException when a service handler error occurs
     */
    public ModelBean processStart(ModelBean request, ModelBean bean, ModelBean user, JDBCData data) 
    throws Exception, IBIZException, ServiceHandlerException {
        ModelBean rq = createRequest(request, getServiceName() + "Rq");
        
        setCMM(rq, null, "Policy", "Policy", bean.getSystemId(), null, null);
        
        addAdditionalParam(rq, "TransactionCd", "Renewal");
        
        // Obtain the application from the previous response
        rq.addValue(bean);
        
        return rq;
    }
    
}
