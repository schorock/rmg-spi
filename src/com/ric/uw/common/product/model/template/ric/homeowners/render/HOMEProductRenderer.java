package com.ric.uw.common.product.model.template.ric.homeowners.render;


import java.math.BigDecimal;
import java.util.HashMap;

import com.iscs.common.admin.user.render.UserRenderer;
import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDAUrl;
import com.iscs.common.render.Renderer;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.shared.address.AddressRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.table.Row;
import com.iscs.common.utility.table.Table;
import com.iscs.common.utility.table.Tables;
import com.iscs.common.utility.table.excel.ExcelReader;
import com.iscs.insurance.product.ProductSetup;
import com.iscs.insurance.product.render.ProductRenderer;
import com.iscs.uw.common.rating.RatingTable;
import com.iscs.uw.common.rating.RatingTables;
import com.iscs.uw.interfaces.datareport.render.UWDataReportRenderer;

import net.inov.biz.server.ServiceContext;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

/**
 * HOME Specific Product Rendering Tools
 * 
 * @author sreerajja
 */
public class HOMEProductRenderer implements Renderer {
	
	private static HashMap<String, Tables> postalTablesCache = new HashMap<String, Tables>();

    /** Creates a New Instance of HOMEProductRenderer */
    public HOMEProductRenderer() {
    }
   
	public String getContextVariableName() {
		return "HOMEProductRenderer";
	}
	
	public String getDynamicSelectListOptions(String productVersionIdRef, String umol) {
    	try {
    		ModelBean[] list = getProductCoderefList(productVersionIdRef, umol);
    		
            if (list == null || list.length < 1) 
            	return "";
            else {
            	String options = "";
            	for (int i = 0; i < list.length; i++) {
            		ModelBean option = list[i];
            		options += ";" + option.gets("value") + ":" + option.gets("name");
            	}
            	if (options.startsWith(";")) 
            		return options.substring(1);
            	else 
            		return options;
            }    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		return "";
    	}

    }
	
	public static ModelBean[] getProductCoderefList(String productVersionIdRef, String umol) {
    	try {
            ModelBean coderef = ProductSetup.getProductCoderef(productVersionIdRef, umol);
            return coderef.getBean("Options").getBeans("Option");    		
    	} catch (Exception e) {
    		Log.debug("No coderef was found - " + umol + " and product version id ref = " + productVersionIdRef );
    		return new ModelBean[0];
    	}
    }
		
	public static Integer getYearDiff(String prevYr, StringDate effectiveDt) {
		try {
			String currDt = effectiveDt.toString();
			String currYr = currDt.substring(0, 4);
			Integer result = Integer.parseInt(StringRenderer.subtractInt(currYr, prevYr) );
			return result;
		} catch (Exception e) {
			Log.debug("No year was found");
			return 0;
		}
	}
	
	public static Integer getYearDiff(StringDate prevDt, StringDate effectiveDt) {
		try {
			String currDt = effectiveDt.toString();
			String currYr = currDt.substring(0, 4);
			String prevYr = prevDt.toString().substring(0, 4);
			Integer result = Integer.parseInt(StringRenderer.subtractInt(currYr, prevYr) );
			return result;
		} catch (Exception e) {
			Log.debug("No year was found");
			return 0;
		}
	}
	
	public Integer getTerritory(ModelBean application, String subType) {
		try {
			String productVersionIdRef = application.getBean("BasicPolicy").gets("ProductVersionIdRef");
			ModelBean productSetup = ProductSetup.getProductSetup(productVersionIdRef);
			MDAUrl mdaUrl = (MDAUrl) Store.getModelObject(productSetup.gets("TerritoryCd"));
			
			ExcelReader excelReader = new ExcelReader();
			RatingTables tables = new RatingTables(excelReader.read(mdaUrl.getURL()));
			RatingTable table = tables.get(subType);
			
			ModelBean riskAddr = application.getBean("Addr", "AddrTypeCd", "InsuredRiskAddr");
			String postalCd = riskAddr.gets("PostalCode").length() > 5 ? riskAddr.gets("PostalCode").substring(0, 5) : riskAddr.gets("PostalCode"); 
			
			Row[] rows = table.lookup("ZipCode", postalCd);
			if ( rows.length != 0) {
				Row row = rows[0];
				return row.getCell("Territory").getInt();
			}
			
			return 0;
		} catch (Exception e) {
			Log.debug("Zip code could not be matched to territory");
			return 0;
		}
	}
	
	//Check authority to view certain question for UW and Admin
	public boolean viewQuestions( ModelBean app, ModelBean userInfo) {
		try {
			if( UserRenderer.getAuthorityAttributeValue(userInfo, "AllowViewQuestions", app.getBean("TransactionInfo").getDate("TransactionEffectiveDt") ).equals("Yes") ) {
				return true;
			}
			return false;
		}
		catch (Exception e) {
			return false;
		}
	}
	
	//Check authority to view certain question for Agent 
	public boolean viewQuestionsAgent( ModelBean app, ModelBean userInfo) {
		try {
			if( UserRenderer.getAuthorityAttributeValue(userInfo, "AllowViewQuestionsAgent", app.getBean("TransactionInfo").getDate("TransactionEffectiveDt") ).equals("Yes") ) {
				return true;
			}
			return false;
		}
		catch (Exception e) {
			return false;
		}
	}

    /** Return the Name in the coderef list
     * @param productVersionIdRef String product version ID
     * @param umol String umol
     * @param name String value
     * @return String Value in the coderef associated with the name
     */
    public static String getCoderefName(String productVersionIdRef,
    		String umol, String value) throws Exception {
    	try {
    		// Get the coderef from the Product Setup
    		ModelBean[] list = getProductCoderefList(productVersionIdRef, umol);

    		for (int i = 0; i < list.length; i++) {
    			if (list[i].gets("value").equalsIgnoreCase(value)) {
    				return list[i].gets("name");
    			}
    		}

    		// should not get here
    		return "";
    	} catch (Exception e) {
    		e.printStackTrace();
    		Log.error(e);
    		return "";
    	}
    }
    
    public static String hasRiskAddrChanged(ModelBean riskAddr,ModelBean basicPolicy,String sourceId) {
		
		String riskAddrChanged="No";
		String verificationHash = "";
		String convVerificationHash = "";
		AddressRenderer  addr = new AddressRenderer();	
		
		try {
			ModelBean dataReportRequest = UWDataReportRenderer.getLatestDataReportRequest(basicPolicy.getParentBean(),sourceId,"PApplicationISOLocation", true);					
			if(dataReportRequest != null)
			 	verificationHash = addr.getAddressHash(dataReportRequest.getBean("ISOLocationSearchCriteria").getBean("Addr"));
			else{
				convVerificationHash = riskAddr.gets("VerificationHash");
				String county = riskAddr.gets("County");
				if(!basicPolicy.gets("ConversionSourceCd").equals("") && convVerificationHash.equals("") && !county.equals("")){
					verificationHash = addr.getAddressHash(riskAddr);
					riskAddr.setValue("ConvVerificationHash",verificationHash);	
				}
				else{
					verificationHash = convVerificationHash;
				}
			}		

			String riskAddrVerificationHash = addr.getAddressHash(riskAddr);
			if( !verificationHash.equals(riskAddrVerificationHash) ) {
				riskAddrChanged = "Yes";
			}
			
			return riskAddrChanged;
		}
		catch (Exception e) {
			Log.debug("Problem with address change verification");
			e.printStackTrace();
			return "No";				
		}		
	}
    
    public static void setDefaultValues(ModelBean application, String subtype) {
    	try {
	    	ModelBean building = application.findBeanByFieldValue("Line", "LineCd", "Homeowners").getBean("Risk").getBean("Building");
			building.setValue("CovALimitIncluded", "");
			building.setValue("CovALimitIncrease", "");
			building.setValue("CovALimit", "");
			building.setValue("CovBLimitIncluded", "");
			building.setValue("CovBLimitIncrease", "");
			building.setValue("CovBLimit", "");
			building.setValue("CovCLimitIncluded", "");
			building.setValue("CovCLimitIncrease", "");
			building.setValue("CovCLimit", "");
			building.setValue("CovDLimitIncluded", "");
			building.setValue("CovDLimitIncrease", "");
			building.setValue("CovDLimit", "");
			building.setValue("AdditionsAlterationsIncluded", "");
			building.setValue("AdditionsAlterationsIncrease", "");
			building.setValue("AdditionsAlterations", "");
			building.setValue("CovLLimit","300000");
			building.setValue("CovMLimit","1000");
			
			if(subtype.equals("HO-0002") || subtype.equals("HO-0003") || subtype.equals("HO-0004") || subtype.equals("HO-0005") || subtype.equals("HO-0006")){
				building.setValue("AllPerilDed", "500");
			}
			if( subtype.equals("HO") ) {	
				building.setValue("AllPerilDed", "500");
				building.setValue("UsageType","");
			}
			else if ( subtype.equals("CO") ) {
				building.setValue("CovCLimitIncluded", "25000");
				building.setValue("CovCLimit", "25000");
				building.setValue("AllPerilDed", "500");
				building.setValue("UsageType","");
			}
			else if ( subtype.equals("RT") ) {
				building.setValue("CovCLimitIncluded", "25000");
				building.setValue("CovCLimit", "25000");
				building.setValue("CovDLimitIncluded", "7500");
				building.setValue("CovDLimit", "7500");
				building.setValue("AllPerilDed", "500");
				building.setValue("UsageType","Tenant");
			}
	    }
	    catch (Exception e) {
	    	e.printStackTrace();
			Log.error(e);
	    }
    }
    
    /** Determine if an 'optional' coverage can be deleted
	 * 
	 *  This handles required coverages that are defined as 'optional' so they can be changed.
	 *  However, since they are required we need to prevent delete.
	 * 
	 * @param displayItem DisplayItem ModelBean
	 * @return True if the coverage can be deleted
	 * @throws Exception when an error occurs
	 */
	public static Boolean allowDelete(ModelBean displayItem) throws Exception {
		
		String coverageCd = displayItem.gets("CoverageCd");
		
		// Mandatory coverages that can be modified but not deleted 
		if( coverageCd.equals("MOLD") ) {
			return false;
		}		
						
		return true;
	}
    
    public ModelBean getApplication(String appRef) throws Exception {
    	
    	JDBCData data = ServiceContext.getServiceContext().getData();
		
		ModelBean application = new ModelBean("Application");
    	data.selectModelBean(application, appRef);
    	
    	return application;
    }      
    
    /**
	 * Returns a factor for increase of either the coverage A or C limit
	 * 
	 * @param productVersionIdRef Product version id reference number to determine
	 *                            the product
	 * @param postalCode          Postal code used to determine which factor to
	 *                            select
	 * @return Returns a factor for increasing the coverage A or C limit or returns
	 *         an empty string if no factor is present
	 * @throws Exception
	 */
	public static String getRenewalIncreaseFactor(String productVersionIdRef, String postalCode) throws Exception {
		// Initialize factor variable
		String factor = new String();

		// Attempt to Get Cached Postal Code Tables
		Tables postalTables = null;
		if (postalTablesCache.containsKey(productVersionIdRef)) {
			postalTables = (Tables) postalTablesCache.get(productVersionIdRef);
		} else {
			String mdaStr = ProductRenderer.getCoderefValue(productVersionIdRef, "UW::underwriting::inflation-factor::all", "inflationFactorPath");
			MDAUrl mdaUrl = (MDAUrl) Store.getModelObject(mdaStr);
			ExcelReader excelReader = new ExcelReader();
			postalTables = excelReader.read(mdaUrl.getURL());

			postalTablesCache.put(productVersionIdRef, postalTables);
		}

		// Get row(s) containing renewal increase factors
		Table table = postalTables.get("Territory");
		postalCode = StringTools.trim(postalCode.replaceAll("-", ""), 3);
		Row[] rows = table.lookup("ZipCode", postalCode);

		// Loop through rows to get correct renewal factor based on effective date
		for (int i = 0; i < rows.length; i++) {
			if (rows[i].getCell("InflationaryFactor").getValue() != null) {
				factor = rows[i].getCell("InflationaryFactor").getBigDecimal().setScale(3, BigDecimal.ROUND_HALF_UP).toString();
				break;
			}
		}

		return factor;
	}
}