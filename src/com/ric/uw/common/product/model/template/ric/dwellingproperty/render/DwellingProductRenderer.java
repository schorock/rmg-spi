package com.ric.uw.common.product.model.template.ric.dwellingproperty.render;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.jdom.Attribute;
import org.jdom.Content;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.xpath.XPath;

import com.iscs.ar.render.AccountRenderer;
import com.iscs.claims.claim.Claim;
import com.iscs.claims.claim.render.ClaimRenderer;
import com.iscs.claims.common.Feature;
import com.iscs.claims.common.Reserve;
import com.iscs.common.business.note.Note;
import com.iscs.common.business.provider.render.ProviderRenderer;
import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDAUrl;
import com.iscs.common.render.DateRenderer;
import com.iscs.common.render.DynamicString;
import com.iscs.common.render.Renderer;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.render.VelocityTools;
import com.iscs.common.shared.address.AddressTools;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.InnovationUtils;
import com.iscs.common.utility.SessionInfo;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.bean.render.BeanRenderer;
import com.iscs.common.utility.io.FileTools;
import com.iscs.common.utility.table.Row;
import com.iscs.common.utility.table.Table;
import com.iscs.common.utility.table.Tables;
import com.iscs.common.utility.table.excel.ExcelReader;
import com.iscs.common.utility.validation.render.ValidationRenderer;
import com.iscs.conversion.DataConversionHelper;
import com.iscs.document.render.DCRenderer;
import com.iscs.insurance.product.ProductMaster;
import com.iscs.insurance.product.ProductVersion;
import com.iscs.insurance.product.render.ProductRenderer;
import com.iscs.uw.common.coverage.render.CoverageRenderer;
import com.iscs.uw.common.product.UWProductSetup;
import com.iscs.uw.policy.PM;
import com.iscs.workflow.Task;
import com.iscs.workflow.TaskException;
import com.iscs.workflow.render.WorkflowRenderer;

import net.inov.biz.server.ServiceContext;
import net.inov.mda.MDAException;
import net.inov.tec.beans.Helper_Beans;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.math.Money;
import net.inov.tec.prefs.PrefsDirectory;
import net.inov.tec.security.SecurityManager;
import net.inov.tec.xml.XmlDoc;
import net.inov.tec.xml.XmlDocException;

/**
 * Dwelling Rendering Tools
 * 
 * @author
 */

public class DwellingProductRenderer implements Renderer {

	private static HashMap<String, Tables> postalTablesCache = new HashMap<String, Tables>();
	public static final String COMPANY_HOLIDAY = "Company Holiday";

	// public static final String BANK_HOLIDAY = "Bank Holiday";
	public static final int FACTOR_VALUE_3 = 100;
	public static final int FACTOR_VALUE_4 = 1000;

	/** Creates a new instance of ValidationRenderer */
	public DwellingProductRenderer() {
	}

	public String getContextVariableName() {
		return "DwellingProductRenderer";
	}

	public static void debug(Object o) {
		System.out.println(o);
	}

	public static void debug(String s) {
		System.out.println(s);
	}

	/**
	 * Returns a factor for increase of either the coverage A or C limit
	 * 
	 * @param productVersionIdRef Product version id reference number to determine
	 *                            the product
	 * @param postalCode          Postal code used to determine which factor to
	 *                            select
	 * @return Returns a factor for increasing the coverage A or C limit or returns
	 *         an empty string if no factor is present
	 * @throws Exception
	 */
	private static String getRenewalIncreaseFactor(String productVersionIdRef, String postalCode) throws Exception {
		// Initialize factor variable
		String factor = new String();

		// Attempt to Get Cached Postal Code Tables
		Tables postalTables = null;
		if (postalTablesCache.containsKey(productVersionIdRef)) {
			postalTables = (Tables) postalTablesCache.get(productVersionIdRef);
		} else {
			String mdaStr = ProductRenderer.getCoderefValue(productVersionIdRef, "UW::underwriting::inflation-factor::all", "inflationFactorPath");
			MDAUrl mdaUrl = (MDAUrl) Store.getModelObject(mdaStr);
			ExcelReader excelReader = new ExcelReader();
			postalTables = excelReader.read(mdaUrl.getURL());

			postalTablesCache.put(productVersionIdRef, postalTables);
		}

		// Get row(s) containing renewal increase factors
		Table table = postalTables.get("Territory");
		postalCode = StringTools.trim(postalCode.replaceAll("-", ""), 3);
		Row[] rows = table.lookup("ZipCode", postalCode);

		// Loop through rows to get correct renewal factor based on effective date
		for (int i = 0; i < rows.length; i++) {
			if (rows[i].getCell("InflationaryFactor").getValue() != null) {
				factor = rows[i].getCell("InflationaryFactor").getBigDecimal().setScale(3, BigDecimal.ROUND_HALF_UP).toString();
				break;
			}
		}

		return factor;
	}

	/***
	 * Gets the transaction change for different lines
	 * 
	 * @param container
	 * @return
	 * @throws ModelBeanException
	 */
	public static ModelBean[] getChangeText(ModelBean container) throws ModelBeanException {

		ModelBean transaction = null;
		ArrayList<ModelBean> changeTextList = new ArrayList<ModelBean>();

		if (container != null) {
			if (container.getBeanName() == "Application") {
				transaction = container.getBean("TransactionInfo");
			} else if (container.getBeanName() == "Policy") {
				ModelBean basicPolicy = container.getBean("BasicPolicy");
				ModelBean[] transactionHistories = basicPolicy.getBeansSorted("TransactionHistory", "TransactionNumber", "Descending");
				if (transactionHistories != null) {
					transaction = transactionHistories[0];
				}
			}

			if (transaction != null) {
				if (transaction.getBeans("TransactionText").length > 0) {
					for (ModelBean transactionText : transaction.getBeans("TransactionText")) {

						if (transactionText.gets("ChangeCd") != null)
							changeTextList.add(transactionText);
					}
				}
			}
		}
		return (ModelBean[]) changeTextList.toArray(new ModelBean[changeTextList.size()]);
	}

	/**
	 * Formats the insured display name
	 * 
	 * @param ModelBean insured
	 * @return String
	 * @throws Exception
	 */
	public static String buildInsuredDisplayName(ModelBean insured) {
		try {
			Log.debug("In buildInsuredDisplayName");
			String insuredDisplayName = "";
			if (insured != null) {
				if (insured.gets("EntityTypeCd").equalsIgnoreCase("Individual")) {
					ModelBean insuredName = insured.findBeanByFieldValue("NameInfo", "NameTypeCd", "InsuredName");
					insuredDisplayName = insuredName.gets("GivenName");
					if (!insuredName.gets("OtherGivenName").equals("")) {
						insuredDisplayName = insuredDisplayName + " " + insuredName.gets("OtherGivenName");
					}
					insuredDisplayName = insuredDisplayName + " " + insuredName.gets("Surname");
					if (!insuredName.gets("SuffixCd").equals("")) {
						insuredDisplayName = insuredDisplayName + " " + insuredName.gets("SuffixCd");
					}
				} else if (insured.gets("EntityTypeCd").equalsIgnoreCase("Joint")) {
					ModelBean insuredName1 = insured.findBeanByFieldValue("NameInfo", "NameTypeCd", "InsuredName");
					ModelBean insuredName2 = insured.findBeanByFieldValue("NameInfo", "NameTypeCd", "InsuredNameJoint");

					if (insuredName1.gets("Surname").equals(insuredName2.gets("Surname"))) {
						insuredDisplayName = insuredName1.gets("GivenName") + " and " + insuredName2.gets("GivenName") + " " + insuredName1.gets("Surname");
					} else {
						insuredDisplayName = insuredName1.gets("GivenName") + " " + insuredName1.gets("Surname") + " and " + insuredName2.gets("GivenName")
								+ " " + insuredName2.gets("Surname");
					}
				} else {
					ModelBean insuredName = insured.findBeanByFieldValue("NameInfo", "NameTypeCd", "InsuredName");
					insuredDisplayName = insuredName.gets("CommercialName");
				}
			}
			return insuredDisplayName;

		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error", e);
			return "";
		}
	}

	public static ModelBean getPolicy(String systemId) throws Exception {

		JDBCData data = ServiceContext.getServiceContext().getData();
		ModelBean policy = new ModelBean("Policy");
		policy = data.selectMiniBean("Policy", systemId);

		return policy;
	}

	public static ModelBean getFullPolicy(String systemId) throws Exception {

		JDBCData data = ServiceContext.getServiceContext().getData();
		ModelBean policy = new ModelBean("Policy");
		policy = data.selectModelBean(policy, systemId);

		return policy;
	}

	public static String exceedTotalLimit(JDBCData data, ModelBean claim) throws Exception {
		try {
			ModelBean[] transactions = claim.findBeansByFieldValue("ClaimantTransaction", "ClaimTransactionHistoryRef", "");
			String error = "";
			for (int cnt = 0; cnt < transactions.length; cnt++) {
				exceededCoverageLimits(data, claim, transactions[cnt]);
				if (!error.isEmpty()) {
					error = "true";
				}
			}
			return error;

		} catch (Exception e) {
			e.printStackTrace();
			Log.error(e);
			return null;
		}
	}

	private static String exceededCoverageLimits(JDBCData data, ModelBean claim, ModelBean claimantTransaction) throws Exception {
		String error = "";
		// Go through the transaction and validate all features to see if the reserves
		// are exceeding policy limits
		ModelBean[] features = claimantTransaction.getBeans("FeatureAllocation");
		Money cumulativeLimitAmt = new Money("0.00");
		ModelBean claimant = claimantTransaction.getParentBean();
		String featureCd = claimant.getBean("Feature").gets("FeatureCd");
		ModelBean limits = getCoverageLimit(claim, featureCd);
		String limit = limits.gets("Limit");
		for (int cnt = 0; cnt < features.length; cnt++) {
			ModelBean curFeature = Feature.find(claimant, features[cnt]);
			// Find the coverage definition

			if (StringTools.isNumeric(limit)) {
				// Determine limits per occurrence has been exceeded
				Money incurredTotal = Feature.calculateIncurredType(claim, curFeature, "Indemnity");
				Money recoveries = new Money(curFeature.gets("RecoveryAmt"));
				Money exceedAmt = incurredTotal.subtract(recoveries);
				cumulativeLimitAmt = cumulativeLimitAmt.add(exceedAmt);
			}
		}
		if (cumulativeLimitAmt.compareTo(limit) > 0) {
			error = "Limit Exceeded";
		}
		return error;
	}

	private static ModelBean getCoverageLimit(ModelBean claim, String featureCd) throws Exception {
		ModelBean coverage = claim.getBean("ClaimPolicyInfo").getBean("ClaimPolicyLimit", "CoverageCd", featureCd);
		ModelBean limits = new ModelBean("ClaimPolicyLimit");

		// Now determine if details need to be drilled down
		String limit = coverage.gets("LimitOverridden");
		if (limit.equals(""))
			limit = coverage.gets("LimitAdjusted");
		if (limit.equals(""))
			limit = coverage.gets("Limit");
		limits.setValue("Limit", limit);

		return limits;
	}

	/**
	 * This method is used to add Limit bean to Coverages which does not have one,
	 * to be compatible with the HPExtreme requirements
	 * 
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	public static void checkAndAddLimitBean(ModelBean coverage) throws Exception {
		try {
			ModelBean limit = coverage.findBeanByFieldValue("Limit", "LimitCd", "Limit1");
			if (limit == null) {
				limit = new ModelBean("Limit");
				limit.setValue("LimitCd", "Limit1");
				coverage.addValue(limit);
			}
		} catch (Exception e) {
			Log.error(e);
			throw e;
		}
	}

	/**
	 * Check PaymentDay is Valid
	 * 
	 * @param application Application ModelBean
	 * @return true/false
	 * @throws Exception if an Unexpected Error Occurs
	 */
	public static boolean isValidPaymentDay(ModelBean application) throws Exception {
		try {
			boolean validPaymentDay = false;
			ModelBean basicPolicy = application.getBean("BasicPolicy");
			String payPlanCd = basicPolicy.gets("PayPlanCd");
			StringDate effectiveDate = basicPolicy.getValueDate("EffectiveDt");
			int paymentDay = 0;
			boolean isAutomatedOrStatement = false;
			if (payPlanCd.contains("Automated") || payPlanCd.contains("Statement")) {
				isAutomatedOrStatement = true;
			}
			if (basicPolicy.gets("PaymentDay").equals("") || !isAutomatedOrStatement
					|| (StringTools.in(basicPolicy.gets("TransactionCd"), "New Business,Rewrite-New", true)
							&& !application.gets("StatementAccountRef").equals(""))) {
				return true;
			}
			paymentDay = Integer.parseInt(basicPolicy.gets("PaymentDay"));
			if (paymentDay != 0) {
				StringDate paymentDate = DateRenderer.advanceDate(effectiveDate, "10");
				int effectiveDay = Integer.parseInt(effectiveDate.toString().substring(6, 8));
				int effectiveMonth = Integer.parseInt(effectiveDate.toString().substring(4, 6));

				int paymentEndDay = Integer.parseInt(paymentDate.toString().substring(6, 8));
				int paymentMonth = Integer.parseInt(paymentDate.toString().substring(4, 6));

				if (paymentMonth != effectiveMonth) {
					if ((paymentDay <= paymentEndDay && paymentDay <= 31) || paymentDay >= effectiveDay) {
						validPaymentDay = true;
					}
				} else {
					if (paymentDay >= effectiveDay && paymentDay <= paymentEndDay) {
						validPaymentDay = true;
					}
				}
			} else {
				validPaymentDay = true;
			}
			return validPaymentDay;
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * This method is used to determine whether a specific loss is considered for
	 * eligibility rules
	 * 
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	public static void updateClaimCount(ModelBean application) throws Exception {
		if (application != null && !application.equals("")) {
			if (application.getBeanName().equals("Policy")
					|| (StringRenderer.in(application.getBean("TransactionInfo").gets("TransactionCd"), "New Business,Rewrite-New,Renewal"))) {
				initializeLossClaimCountInd(application);

				ModelBean[] countClaims = BeanRenderer.findBeansByFieldValueAndStatus(application, "LossHistory", "ClaimCountInd", "Yes", "Active");
				if (countClaims != null && countClaims.length > 0) {
					Map<String, ArrayList<ModelBean>> lossClaimsByDt = groupClaimsByLossDate(countClaims);
					for (ArrayList<ModelBean> lossDtClaims : lossClaimsByDt.values()) {
						if (lossDtClaims != null && lossDtClaims.size() > 0) {
							if (lossDtClaims != null && lossDtClaims.size() > 0) {
								boolean hasLiabilityClaims = false;
								boolean hasPropertyClaims = false;
								for (ModelBean lossDtClaim : lossDtClaims) {
									if (lossDtClaim.gets("LossType").equalsIgnoreCase("Property")) {
										hasPropertyClaims = true;
									}
									if (lossDtClaim.gets("LossType").equalsIgnoreCase("Liability")) {
										hasLiabilityClaims = true;
									}
								}
								if (hasLiabilityClaims && hasPropertyClaims) {
									ArrayList<ModelBean> lossDtPropertyClaims = new ArrayList<ModelBean>();
									for (ModelBean lossDtClaim : lossDtClaims) {
										if (lossDtClaim.gets("LossType").equalsIgnoreCase("Liability")) {
											lossDtClaim.setValue("ClaimCountInd", "No");
										} else {
											lossDtPropertyClaims.add(lossDtClaim);
										}
									}
									lossDtClaims = lossDtPropertyClaims;
								}
								if (lossDtClaims != null && lossDtClaims.size() > 0) {
									String lossAmt = "0.00";
									for (ModelBean lossDtClaim : lossDtClaims) {
										if (!lossDtClaim.gets("PaidAmt").equals("") && StringRenderer.greaterThan(lossDtClaim.gets("PaidAmt"), lossAmt)) {
											lossAmt = lossDtClaim.gets("PaidAmt");
										}
									}

									ArrayList<ModelBean> lossDtPropertyClaims = new ArrayList<ModelBean>();
									boolean firstClaim = false;
									for (ModelBean lossDtClaim : lossDtClaims) {
										if (!lossDtClaim.gets("PaidAmt").equals("") && StringRenderer.equal(lossDtClaim.gets("PaidAmt"), lossAmt)) {
											if (!firstClaim) {
												lossDtClaim.setValue("ClaimCountInd", "Yes");
												firstClaim = true;
											} else {
												lossDtClaim.setValue("ClaimCountInd", "No");
											}
										} else {
											lossDtClaim.setValue("ClaimCountInd", "No");
										}
									}
									lossDtClaims = lossDtPropertyClaims;
								}
							}
						}

					}
				}
			}
		}
	}

	private static Map<String, ArrayList<ModelBean>> groupClaimsByLossDate(ModelBean[] countClaims) throws ModelBeanException {
		Map<String, ArrayList<ModelBean>> lossDateClaims = new HashMap<String, ArrayList<ModelBean>>();
		for (ModelBean claim : countClaims) {
			if (claim != null && !claim.equals("")) {
				if (lossDateClaims.get(claim.gets("LossDt")) != null) {
					ArrayList<ModelBean> LossesByDates = lossDateClaims.get(claim.gets("LossDt"));
					LossesByDates.add(claim);
					lossDateClaims.put(claim.gets("LossDt"), LossesByDates);

				} else {
					ArrayList<ModelBean> LossesByDates = new ArrayList<ModelBean>();
					LossesByDates.add(claim);
					lossDateClaims.put(claim.gets("LossDt"), LossesByDates);
				}

			}
		}
		return lossDateClaims;
	}

	private static void initializeLossClaimCountInd(ModelBean application) throws ModelBeanException {
		StringDate lossDisplayDt = DateRenderer.advanceMonth(application.getBean("BasicPolicy").getDate("EffectiveDt"), "-48");
		StringDate lossCountableDt = DateRenderer.advanceMonth(application.getBean("BasicPolicy").getDate("EffectiveDt"), "-36");
		if (application.getBean("Insured").gets("EntityTypeCd").equals("Limited Liability Company")
				&& !application.getBean("BasicPolicy").gets("LLCOwnedDt").equals("")) {
			lossCountableDt = application.getBean("BasicPolicy").getDate("LLCOwnedDt");
		}
		ModelBean[] losses = application.findBeansByFieldValue("LossHistory", "StatusCd", "Active");
		for (ModelBean loss : losses) {
			if (loss!=null && !loss.equals("")) {
				if (loss.getDate("LossDt")!=null && DateRenderer.compareTo(loss.getDate("LossDt"), lossDisplayDt) < 0) {
					if (!loss.gets("SourceCd").equalsIgnoreCase("PICK") && !loss.gets("SourceCd").equalsIgnoreCase("Innovation")) {
						loss.setValue("StatusCd", "Deleted");
					}
				} 
				else if (loss.gets("IgnoreInd").equals("Yes") || (loss.getDate("LossDt")!=null &&  DateRenderer.compareTo(loss.getDate("LossDt"), lossCountableDt) < 0)) {
					loss.setValue("ClaimCountInd", "No");
				} else {
					boolean claimCount = false;
					if (application.getBean("Insured").gets("EntityTypeCd").equals("Limited Liability Company")) {
						if (StringRenderer.in(loss.gets("PolicyType"), "F,FIRE,EC,GL,UMPK")) {
							claimCount = true;
						} else {
							claimCount = false;
						}
					} else {
						claimCount = true;
					}
					if (claimCount) {
						if (StringRenderer.in(loss.gets("ClaimStatusCd"), "Subrogation,Closed")) {
							if (!loss.gets("PaidAmt").equals("") && StringRenderer.greaterThan(loss.gets("PaidAmt"), "0")) {
								loss.setValue("ClaimCountInd", "Yes");
							} else {
								loss.setValue("ClaimCountInd", "No");
							}
						} else {
							loss.setValue("ClaimCountInd", "No");
						}
					} else {
						loss.setValue("ClaimCountInd", "No");
					}
				}
			}
		}
	}

	public static ModelBean getBeanByItemNumber(ModelBean mb, String featureCd, String itemNum) throws Exception {
		ModelBean[] features = mb.findBeansByFieldValue("Feature", "FeatureCd", featureCd);
		ModelBean feature = new ModelBean();
		for (int i = 0; i <= features.length; i++) {
			if (features[i].gets("ItemNumber").equalsIgnoreCase(itemNum)) {
				feature = features[i].getBean("Feature", "ItemNumber", itemNum);
				break;
			}
		}
		return feature;
	}

	/**
	 * Determine if a feature has exceeded policy limits. The check is only done on
	 * features/reserves that are part of the current outstanding claimant
	 * transaction - sub features not mapping to coverage items or rate areas
	 * 
	 * @param data                The data connection
	 * @param claim               The Claim ModelBean
	 * @param claimantTransaction The Claimant Transaction ModelBean
	 * @return Feature ModelBean containing details on limits that were exceeded
	 * @throws Exception when an error occurs
	 */
	public static ModelBean[] exceededSubFeatureLimits(JDBCData data, ModelBean claim, ModelBean claimantTransaction) throws Exception {
		ArrayList<ModelBean> exceededList = new ArrayList<ModelBean>();
		// Go through the transaction and validate all features to see if the reserves
		// are exceeding policy limits
		ModelBean[] features = claimantTransaction.getBeans("FeatureAllocation");
		ModelBean claimant = claimantTransaction.getParentBean();
		ArrayList<String> featureCds = new ArrayList<String>();
		for (int cnt = 0; cnt < features.length; cnt++) {
			featureCds.add(features[cnt].gets("FeatureCd"));
		}
		featureCds = removeDuplicates(featureCds);
		for (Object featureCd : featureCds) {
			ModelBean[] featuresList = claimant.findBeansByFieldValue("Feature", "FeatureCd", featureCd.toString());
			Money exceedAmt = new Money("0.00");
			// Find the coverage definition
			ModelBean coverage = claim.getBean("ClaimPolicyInfo").getBean("ClaimPolicyLimit", "CoverageCd", featureCd.toString());
			String limit = coverage.gets("Limit");
			Money limitAmt = new Money(limit);
			if (StringTools.isNumeric(limit)) {
				for (int cnt = 0; cnt < featuresList.length; cnt++) {
					ModelBean curFeature = Feature.find(claimant, featuresList[cnt]);
					Money incurredTotal = Feature.calculateIncurredType(claim, curFeature, "Indemnity");
					Money recoveries = new Money(curFeature.gets("RecoveryAmt"));
					exceedAmt = exceedAmt.add(incurredTotal.subtract(recoveries));
				}
				if (exceedAmt.compareTo(limitAmt) > 0) {
					ModelBean feature = new ModelBean("Feature");
					feature.setValue("FeatureCd", featureCd.toString());
					feature.setValue("ReserveAmt", limitAmt);
					exceededList.add(feature);
				}
			}
		}
		return (ModelBean[]) exceededList.toArray(new ModelBean[exceededList.size()]);
	}

	private static ArrayList<String> removeDuplicates(ArrayList<String> a) {
		return new ArrayList<String>(new HashSet<String>(a));
	}

	public static ModelBean getBuildingLimitsDFDE(ModelBean application, String isRenewal) throws Exception {
		ModelBean basicPolicy = application.getBean("BasicPolicy");
		String subTypeCd = basicPolicy.gets("SubTypeCd");
		String productVersionIdRef = basicPolicy.gets("ProductVersionIdRef");

		ModelBean[] lines = application.getBeans("Line");
		for (ModelBean line : lines) {
			if (!line.gets("StatusCd").equalsIgnoreCase("Deleted")) {
				ModelBean[] risks = line.getBeans("Risk");
				for (ModelBean risk : risks) {
					if (!risk.gets("Status").equalsIgnoreCase("Deleted")) {

						ModelBean building = risk.getBean("Building");
						// Coverage A
						ModelBean covA = risk.getBean("Coverage", "CoverageCd", "CovA");
						String covALimit = building.gets("CovALimit");
						String coveageFactor = "UW::underwriting::coverageLimitPercent::coverAgeLimit";
						String covBLimitFactor = ProductRenderer.getCoderefValue(productVersionIdRef, coveageFactor, "covBLimit");
						String covCLimitFactor = ProductRenderer.getCoderefValue(productVersionIdRef, coveageFactor, "covCLimit");
						String covDLimitFactor = ProductRenderer.getCoderefValue(productVersionIdRef, coveageFactor, "covDLimit");

						String postalCode = building.getBean("Addr").gets("PostalCode");
						String factor = DwellingProductRenderer.getRenewalIncreaseFactor(productVersionIdRef, postalCode);
						if (StringRenderer.isTrue(isRenewal)) {
							// Increase Coverage A Building Limit
							if (!covA.gets("Status").equalsIgnoreCase("Deleted")) {
								covALimit = StringRenderer.round( StringRenderer.multiply( covALimit, StringRenderer.addFloat(factor, "1.0"), 3), 1000);
								building.setValue("CovALimit", covALimit);
								building.setValue("CovAInflationFactor", factor);
								covA.getBean("Limit", "LimitCd", "Limit1").setValue("Value", covALimit);
							}
						}

						// Coverage B
						String covBLimitIncluded = StringRenderer.multiply(covALimit, covBLimitFactor, 0);
						String covBLimitIncrease = building.gets("CovBLimitIncrease");
						if (covBLimitIncrease.equals("")) {
							covBLimitIncrease.equalsIgnoreCase("0");
						}
						if (subTypeCd.equals("DP2") || subTypeCd.equals("DP3")) {
							covBLimitIncrease = StringRenderer
									.round(StringRenderer.addFloat(covBLimitIncrease, StringRenderer.multiply(covBLimitIncrease, factor, 0)), FACTOR_VALUE_3);
						}
						String covBTotal = StringRenderer.addInt(covBLimitIncluded, covBLimitIncrease);
						building.setValue("CovBLimitIncrease", covBLimitIncrease);
						building.setValue("CovBLimitIncluded", covBLimitIncluded);
						building.setValue("CovBLimit", covBTotal);

						// Coverage C
						if (!building.gets("CovCLimit").equals("")) {
							String covCLimitIncluded = StringRenderer.multiply(covALimit, covCLimitFactor, 0);
							String covCLimitIncrease = building.gets("CovCLimitIncrease");
							if (covCLimitIncrease.equals("")) {
								covCLimitIncrease = "0";
								building.setValue("CovCLimitIncrease", "0");
							}
							String covCTotal = StringRenderer.addInt(covCLimitIncluded, covCLimitIncrease);
							building.setValue("CovCLimitIncluded", covCLimitIncluded);
							building.setValue("CovCLimit", covCTotal);
						}

						// Coverage D
						String covDIncluded = StringRenderer.multiply(covALimit, covDLimitFactor, 0);
						String covDLimitIncrease = building.gets("CovDLimitIncrease");
						if (covDLimitIncrease.equals("")) {
							covDLimitIncrease = "0";
							building.setValue("CovDLimitIncrease", "0");
						}
						String covDTotal = StringRenderer.addInt(covDIncluded, covDLimitIncrease);
						building.setValue("CovDLimitIncluded", covDIncluded);
						building.setValue("CovDLimit", covDTotal);

						// Coverage E
						String covELimitFactor = ProductRenderer.getCoderefValue(productVersionIdRef, coveageFactor, "covELimit");
						if (subTypeCd.equals("DP1")) {
							covELimitFactor = ProductRenderer.getCoderefValue(productVersionIdRef, coveageFactor, "covELimitDP1");
						}
						String covEIncluded = StringRenderer.multiply(covALimit, covELimitFactor, 0);
						String covELimitIncrease = building.gets("CovELimitIncrease");
						if (covELimitIncrease.equals("")) {
							covELimitIncrease = "0";
							building.setValue("CovELimitIncrease", "0");
						}
						String covETotal = StringRenderer.addInt(covEIncluded, covELimitIncrease);
						building.setValue("CovELimitIncluded", covEIncluded);
						building.setValue("CovELimit", covETotal);

					}
				}
			}
		}
		return application;
	}

	public static ModelBean getBuildingLimitsDFMD(ModelBean application, String isRenewal) throws Exception {
		ModelBean basicPolicy = application.getBean("BasicPolicy");
		String subTypeCd = basicPolicy.gets("SubTypeCd");
		String productVersionIdRef = basicPolicy.gets("ProductVersionIdRef");

		ModelBean[] lines = application.getBeans("Line");
		for (ModelBean line : lines) {
			if (!line.gets("StatusCd").equalsIgnoreCase("Deleted")) {
				ModelBean[] risks = line.getBeans("Risk");
				for (ModelBean risk : risks) {
					if (!risk.gets("Status").equalsIgnoreCase("Deleted")) {

						ModelBean building = risk.getBean("Building");
						// Coverage A
						ModelBean covA = risk.getBean("Coverage", "CoverageCd", "CovA");
						String covALimit = building.gets("CovALimit");
						String coveageFactor = "UW::underwriting::coverageLimitPercent::coverAgeLimit";
						String covBLimitFactor = ProductRenderer.getCoderefValue(productVersionIdRef, coveageFactor, "covBLimit");
						String covCLimitFactor = ProductRenderer.getCoderefValue(productVersionIdRef, coveageFactor, "covCLimit");
						String covDLimitFactor = ProductRenderer.getCoderefValue(productVersionIdRef, coveageFactor, "covDLimit");

						String postalCode = building.getBean("Addr").gets("PostalCode");
						String factor = DwellingProductRenderer.getRenewalIncreaseFactor(productVersionIdRef, postalCode);
						if (StringRenderer.isTrue(isRenewal)) {
							// Increase Coverage A Building Limit
							if (!covA.gets("Status").equalsIgnoreCase("Deleted")) {
								covALimit = StringRenderer.round( StringRenderer.multiply( covALimit, StringRenderer.addFloat(factor, "1.0"), 3), 1000);
								building.setValue("CovALimit", covALimit);
								building.setValue("CovAInflationFactor", factor);
								covA.getBean("Limit", "LimitCd", "Limit1").setValue("Value", covALimit);
							}
						}

						// Coverage B
						String covBLimitIncluded = StringRenderer.multiply(covALimit, covBLimitFactor, 0);
						String covBLimitIncrease = building.gets("CovBLimitIncrease");
						if (covBLimitIncrease.equals("")) {
							covBLimitIncrease.equalsIgnoreCase("0");
						}
						if (subTypeCd.equals("DP2") || subTypeCd.equals("DP3")) {
							covBLimitIncrease = StringRenderer
									.round(StringRenderer.addFloat(covBLimitIncrease, StringRenderer.multiply(covBLimitIncrease, factor, 0)), FACTOR_VALUE_3);
						}
						String covBTotal = StringRenderer.addInt(covBLimitIncluded, covBLimitIncrease);
						building.setValue("CovBLimitIncrease", covBLimitIncrease);
						building.setValue("CovBLimitIncluded", covBLimitIncluded);
						building.setValue("CovBLimit", covBTotal);

						// Coverage C
						if (!building.gets("CovCLimit").equals("")) {
							String covCLimitIncluded = StringRenderer.multiply(covALimit, covCLimitFactor, 0);
							String covCLimitIncrease = building.gets("CovCLimitIncrease");
							if (covCLimitIncrease.equals("")) {
								covCLimitIncrease = "0";
								building.setValue("CovCLimitIncrease", "0");
							}
							String covCTotal = StringRenderer.addInt(covCLimitIncluded, covCLimitIncrease);
							building.setValue("CovCLimitIncluded", covCLimitIncluded);
							building.setValue("CovCLimit", covCTotal);
						}

						// Coverage D
						String covDIncluded = StringRenderer.multiply(covALimit, covDLimitFactor, 0);
						String covDLimitIncrease = building.gets("CovDLimitIncrease");
						if (covDLimitIncrease.equals("")) {
							covDLimitIncrease = "0";
							building.setValue("CovDLimitIncrease", "0");
						}
						String covDTotal = StringRenderer.addInt(covDIncluded, covDLimitIncrease);
						building.setValue("CovDLimitIncluded", covDIncluded);
						building.setValue("CovDLimit", covDTotal);

						// Coverage E
						String covELimitFactor = ProductRenderer.getCoderefValue(productVersionIdRef, coveageFactor, "covELimit");
						if (subTypeCd.equals("DP1")) {
							covELimitFactor = ProductRenderer.getCoderefValue(productVersionIdRef, coveageFactor, "covELimitDP1");
						}
						String covEIncluded = StringRenderer.multiply(covALimit, covELimitFactor, 0);
						String covELimitIncrease = building.gets("CovELimitIncrease");
						if (covELimitIncrease.equals("")) {
							covELimitIncrease = "0";
							building.setValue("CovELimitIncrease", "0");
						}
						String covETotal = StringRenderer.addInt(covEIncluded, covELimitIncrease);
						building.setValue("CovELimitIncluded", covEIncluded);
						building.setValue("CovELimit", covETotal);

					}
				}
			}
		}
		return application;
	}

	/**
	 * Determine if a total DP1 feature for all Claimants has exceeded policy
	 * limits. The check is only done on features/reserves that are part of the
	 * current outstanding claimant transaction - sub features not mapping to
	 * coverage items or rate areas
	 * 
	 * @param data                The data connection
	 * @param claim               The Claim ModelBean
	 * @param claimantTransaction The Claimant Transaction ModelBean
	 * @return Feature ModelBean containing details on limits that were exceeded
	 * @throws Exception when an error occurs
	 */
	public static ModelBean getDP1TotalIncurredFeatureLimitDF(JDBCData data, ModelBean claim) throws Exception {
		ModelBean[] claimants = ClaimRenderer.getOpenClaimants(claim);
		Money exceedAmt = new Money("0.00");
		Money incurredTotal = new Money("0.00");
		Money recoveries = new Money("0.00");

		String policyRef = claim.getBean("ClaimPolicyInfo").gets("PolicyRef");
		ModelBean policy = data.selectMiniBean("PolicyMini", policyRef);
		ModelBean basicPolicy = policy.getBean("BasicPolicy");
		String subTypeStr = basicPolicy.gets("SubTypeCd");
		ModelBean featureBean = new ModelBean("Feature");
		if (subTypeStr.equalsIgnoreCase("DP1")) {
			for (ModelBean claimant : claimants) {
				if (claimant.gets("ClaimantTypeCd").equalsIgnoreCase("First Party")) {
					ModelBean[] covAFeatures = claimant.findBeansByFieldValue("Feature", "FeatureCd", "CovA");
					for (ModelBean feature : covAFeatures) {
						incurredTotal = Feature.calculateIncurredType(claim, feature, "Indemnity");
						recoveries = new Money(feature.gets("RecoveryAmt"));
						exceedAmt = exceedAmt.add(incurredTotal.subtract(recoveries));
					}
					ModelBean[] covBFeatures = claimant.findBeansByFieldValue("Feature", "FeatureCd", "CovB");
					for (ModelBean feature : covBFeatures) {
						incurredTotal = Feature.calculateIncurredType(claim, feature, "Indemnity");
						recoveries = new Money(feature.gets("RecoveryAmt"));
						exceedAmt = exceedAmt.add(incurredTotal.subtract(recoveries));
					}
					ModelBean[] covCFeatures = claimant.findBeansByFieldValue("Feature", "FeatureCd", "CovC");
					for (ModelBean feature : covCFeatures) {
						incurredTotal = Feature.calculateIncurredType(claim, feature, "Indemnity");
						recoveries = new Money(feature.gets("RecoveryAmt"));
						exceedAmt = exceedAmt.add(incurredTotal.subtract(recoveries));
					}
					ModelBean[] covDFeatures = claimant.findBeansByFieldValue("Feature", "FeatureCd", "CovD");
					for (ModelBean feature : covDFeatures) {
						incurredTotal = Feature.calculateIncurredType(claim, feature, "Indemnity");
						recoveries = new Money(feature.gets("RecoveryAmt"));
						exceedAmt = exceedAmt.add(incurredTotal.subtract(recoveries));
					}
					ModelBean[] covEFeatures = claimant.findBeansByFieldValue("Feature", "FeatureCd", "CovE");
					for (ModelBean feature : covEFeatures) {
						incurredTotal = Feature.calculateIncurredType(claim, feature, "Indemnity");
						recoveries = new Money(feature.gets("RecoveryAmt"));
						exceedAmt = exceedAmt.add(incurredTotal.subtract(recoveries));
					}
					ModelBean[] covDP0414Features = claimant.findBeansByFieldValue("Feature", "FeatureCd", "DP0414");
					for (ModelBean feature : covDP0414Features) {
						incurredTotal = Feature.calculateIncurredType(claim, feature, "Indemnity");
						recoveries = new Money(feature.gets("RecoveryAmt"));
						exceedAmt = exceedAmt.add(incurredTotal.subtract(recoveries));
					}
				}
			}
			Money totalLimit = new Money("0.00");
			Money covA30Limit = new Money("0.00");
			Money covAPercent = new Money("0.3");
			ModelBean covLimit = claim.findBeanByFieldValue("ClaimPolicyLimit", "CoverageCd", "CovA");
			if (covLimit != null) {
				totalLimit = totalLimit.add(new Money(covLimit.gets("Limit")));
				covA30Limit = new Money(covLimit.gets("Limit"));
				covA30Limit = covA30Limit.multiply(covAPercent);
			}
			covLimit = claim.findBeanByFieldValue("ClaimPolicyLimit", "CoverageCd", "CovB");
			if (covLimit != null) {
				totalLimit = totalLimit.add(new Money(covLimit.gets("Limit")));
			}
			covLimit = claim.findBeanByFieldValue("ClaimPolicyLimit", "CoverageCd", "CovC");
			if (covLimit != null) {
				totalLimit = totalLimit.add(new Money(covLimit.gets("Limit")));
			}
			covLimit = claim.findBeanByFieldValue("ClaimPolicyLimit", "CoverageCd", "CovD");
			if (covLimit != null) {
				totalLimit = totalLimit.add(new Money(covLimit.gets("Limit")));
			}
			covLimit = claim.findBeanByFieldValue("ClaimPolicyLimit", "CoverageCd", "CovE");
			if (covLimit != null) {
				totalLimit = totalLimit.add(new Money(covLimit.gets("Limit")));
			}
			covLimit = claim.findBeanByFieldValue("ClaimPolicyLimit", "CoverageCd", "DP0414");
			if (covLimit != null) {
				totalLimit = totalLimit.add(new Money(covLimit.gets("Limit")));
			}
			totalLimit = totalLimit.subtract(covA30Limit);
			if (exceedAmt.compareTo(totalLimit) > 0) {
				featureBean.setValue("FeatureCd",
						"Total incurred for combination of  Dwelling, Other Structure ,Personal Property, Fair Rental Value, and Additional Living Expense exceeds maximum allowed");
				featureBean.setValue("ReserveAmt", totalLimit);
			} else {
				featureBean.setValue("FeatureCd",
						"Total incurred for combination of  Dwelling, Other Structure ,Personal Property, Fair Rental Value, and Additional Living Expense exceeds maximum allowed");
				featureBean.setValue("ReserveAmt", "0");
			}
		}
		return featureBean;
	}

	/**
	 * Determine if a total DP2 and DP3 feature for all Claimants has exceeded
	 * policy limits. The check is only done on features/reserves that are part of
	 * the current outstanding claimant transaction - sub features not mapping to
	 * coverage items or rate areas
	 * 
	 * @param data                The data connection
	 * @param claim               The Claim ModelBean
	 * @param claimantTransaction The Claimant Transaction ModelBean
	 * @return Feature ModelBean containing details on limits that were exceeded
	 * @throws Exception when an error occurs
	 */
	public static ModelBean getDP23TotalIncurredFeatureLimitDF(JDBCData data, ModelBean claim) throws Exception {
		ModelBean[] claimants = ClaimRenderer.getOpenClaimants(claim);
		Money exceedAmt = new Money("0.00");
		Money incurredTotal = new Money("0.00");
		Money recoveries = new Money("0.00");

		String policyRef = claim.getBean("ClaimPolicyInfo").gets("PolicyRef");
		ModelBean policyMini = data.selectMiniBean("PolicyMini", policyRef);
		ModelBean basicPolicyMini = policyMini.getBean("BasicPolicy");
		String subTypeStr = basicPolicyMini.gets("SubTypeCd");
		ModelBean featureBean = new ModelBean("Feature");
		if (subTypeStr.equalsIgnoreCase("DP2") || subTypeStr.equalsIgnoreCase("DP3")) {
			for (ModelBean claimant : claimants) {
				if (claimant.gets("ClaimantTypeCd").equalsIgnoreCase("First Party")) {
					ModelBean[] covDFeatures = claimant.findBeansByFieldValue("Feature", "FeatureCd", "CovD");
					for (ModelBean feature : covDFeatures) {
						incurredTotal = Feature.calculateIncurredType(claim, feature, "Indemnity");
						recoveries = new Money(feature.gets("RecoveryAmt"));
						exceedAmt = exceedAmt.add(incurredTotal.subtract(recoveries));
					}
					ModelBean[] covEFeatures = claimant.findBeansByFieldValue("Feature", "FeatureCd", "CovE");
					for (ModelBean feature : covEFeatures) {
						incurredTotal = Feature.calculateIncurredType(claim, feature, "Indemnity");
						recoveries = new Money(feature.gets("RecoveryAmt"));
						exceedAmt = exceedAmt.add(incurredTotal.subtract(recoveries));
					}
				}
			}
			// Get the Policy
			ModelBean bean = new ModelBean("Policy");
			ModelBean policy = data.selectModelBean(bean, policyMini.getSystemId());
			ModelBean line = policy.getBean("Line");
			ModelBean risk = line.getBean("Risk");
			ModelBean building = risk.getBean("Building");

			Money totalLimit = new Money("0.00");
			Money covA20Limit = new Money("0.00");
			Money covAPercent = new Money("0.2");
			ModelBean covLimit = claim.findBeanByFieldValue("ClaimPolicyLimit", "CoverageCd", "CovA");
			if (covLimit != null) {
				covA20Limit = new Money(covLimit.gets("Limit"));
				covA20Limit = covA20Limit.multiply(covAPercent);
				totalLimit = totalLimit.add(covA20Limit);
			}
			if (building.gets("CovDLimitIncrease") != null && !building.gets("CovDLimitIncrease").equals("")) {
				totalLimit = totalLimit.add(new Money(building.gets("CovDLimitIncrease")));
			}
			if (building.gets("CovELimitIncrease") != null && !building.gets("CovELimitIncrease").equals("")) {
				totalLimit = totalLimit.add(new Money(building.gets("CovELimitIncrease")));
			}
			if (exceedAmt.compareTo(totalLimit) > 0) {
				featureBean.setValue("FeatureCd", "Total incurred for combination of  Fair Rental Value and Additional Living Expense exceeds maximum allowed");
				featureBean.setValue("ReserveAmt", totalLimit);
			} else {
				featureBean.setValue("FeatureCd", "Total incurred for combination of  Fair Rental Value and Additional Living Expense exceeds maximum allowed");
				featureBean.setValue("ReserveAmt", "0");
			}
		}
		return featureBean;
	}

	/**
	 * Determine if a feature has exceeded policy limits. The check is only done on
	 * features/reserves that are part of the current outstanding claimant
	 * transaction - sub features not mapping to coverage items or rate areas
	 * 
	 * @param data                The data connection
	 * @param claim               The Claim ModelBean
	 * @param claimantTransaction The Claimant Transaction ModelBean
	 * @return Feature ModelBean containing details on limits that were exceeded
	 * @throws Exception when an error occurs
	 */
	public static ModelBean[] exceededSubFeatureLimitsThirdParty(JDBCData data, ModelBean claim, ModelBean claimantTransaction) throws Exception {
		ArrayList<ModelBean> exceededList = new ArrayList<ModelBean>();
		// Go through the transaction and validate all features to see if the reserves
		// are exceeding policy limits
		ModelBean[] features = claimantTransaction.getBeans("FeatureAllocation");
		ModelBean claimant = claimantTransaction.getParentBean();
		if (claimant.gets("ClaimantTypeCd").equalsIgnoreCase("Third Party")) {
			ArrayList<String> featureCds = new ArrayList<String>();
			for (int cnt = 0; cnt < features.length; cnt++) {
				featureCds.add(features[cnt].gets("FeatureCd"));
			}
			featureCds = removeDuplicates(featureCds);
			for (Object featureCd : featureCds) {
				ModelBean[] featuresList = claimant.findBeansByFieldValue("Feature", "FeatureCd", featureCd.toString());
				Money exceedAmt = new Money("0.00");
				// Find the coverage definition
				ModelBean coverage = claim.getBean("ClaimPolicyInfo").getBean("ClaimPolicyLimit", "CoverageCd", featureCd.toString());
				String limit = coverage.gets("Limit");
				Money limitAmt = new Money(limit);
				if (StringTools.isNumeric(limit)) {
					for (int cnt = 0; cnt < featuresList.length; cnt++) {
						ModelBean curFeature = Feature.find(claimant, featuresList[cnt]);
						Money incurredTotal = Feature.calculateIncurredType(claim, curFeature, "Indemnity");
						Money recoveries = new Money(curFeature.gets("RecoveryAmt"));
						exceedAmt = exceedAmt.add(incurredTotal.subtract(recoveries));
					}
					if (exceedAmt.compareTo(limitAmt) > 0) {
						ModelBean feature = new ModelBean("Feature");
						feature.setValue("FeatureCd", featureCd.toString());
						feature.setValue("ReserveAmt", limitAmt);
						exceededList.add(feature);
					}
				}
			}
		}
		return (ModelBean[]) exceededList.toArray(new ModelBean[exceededList.size()]);
	}

	public String getClaimASL(ModelBean statRecord) throws Exception {
		String asl = statRecord.gets("AnnualStatementLineCd");
		try {
			String lossCauseCd = statRecord.gets("LossCauseCd");
			String featureCd = statRecord.gets("FeatureCd");
			if (StringRenderer.in(featureCd, "CovA,CovB,CovC,CovD,CovE,DP1914,DP0471,WB")) {
				if (StringRenderer.in(lossCauseCd, "Fire")) {
					asl = "010";
				} else {
					asl = "021";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return asl;
	}

	public static void getPRClaimTask(ModelBean claim, String userId, String securityId, String sessionId, String conversationId, JDBCData data)
			throws Exception {
		boolean taskExists = false;
		ArrayList<String> taskList = new ArrayList<String>();
		ModelBean response = claim.getParentBean();
		ModelBean responseParams = response.getBean("ResponseParams");
		StringDate todayDt = DateRenderer.getStringDate();
		String todayTm = DateRenderer.getTime();
		String taskQuickKey = "Claim" + claim.getSystemId();
		String transNo = claim.gets("TransactionNumber");
		if (transNo.equals("")) {
			transNo = "1";
		}

		ModelBean transactionInfo = claim.findBeanByFieldValue("ClaimTransactionInfo", "TransactionNumber", transNo);
		String adjusterId = ProviderRenderer
				.getProviderNumber(claim.findBeanByFieldValue("AssignedProvider", "AssignedProviderTypeCd", "AssignedAdjuster").gets("ProviderRef"));
		if (!adjusterId.equals("")) {
			if (claim.gets("CaseNumber").equals("")) {
				if (StringRenderer.in(claim.gets("LossCauseCd"), "Fire,Theft,TheftOffPremise,Vandalism and Malicious Mischief")) {
					taskList.add(0, "ClaimTask2007");
					for (ModelBean task : WorkflowRenderer.fetchTasks(taskQuickKey, taskList, data)) {
						if (task.gets("TemplateId").equals("ClaimTask2007")) {
							taskExists = true;
						}
					}
					// Create
					if (!taskExists) {
						ModelBean task = WorkflowRenderer.createTask(data, "ClaimTask2007", claim, todayDt);
						task.setValue("CurrentOwner", adjusterId);
						task.setValue("OriginalOwner", adjusterId);
						task.setValue("OriginalOwnerCd", "User");
						Task.insertTask(data, task);
					}
				}
			} else {
				// Email
				ModelBean output = new ModelBean();
				String submissionNumber = "1";
				if (!transactionInfo.gets("SubmissionNumber").equals("")) {
					submissionNumber = transactionInfo.gets("SubmissionNumber");
				}

				// Create Output Bean
				if (!claim.gets("PREmailInd").equalsIgnoreCase("Yes")) {
					ModelBean logKeyParam = responseParams.findBeanByFieldValue("Param", "Name", "LogKey");
					String logKey = logKeyParam.gets("Value");
					StringDate todayStringDt = DateRenderer.getStringDate(responseParams);
					output = DCRenderer.addCorrespondence(data, claim, "FirstNoticeofLossPR", "CLTransaction", submissionNumber, userId, todayStringDt, logKey);

					output.setValue("AddUser", "System");
					output.setValue("AddDt", todayDt);
					output.setValue("AddTm", todayTm);
					conversationId = ServiceContext.getServiceContext().getConversationId();
					sessionId = ServiceContext.getServiceContext().getSessionId();
					ModelBean user = ServiceContext.getServiceContext().getUser();
					securityId = SecurityManager.getSecurityManager().generateSecurityId(user);
					DCRenderer.processOutput(data, userId, securityId, sessionId, conversationId, claim, output, todayDt, todayTm);
					claim.setValue("PREmailInd", "Yes");
					data.saveModelBean(claim);
				}
			}
		}
	}

	public static void getDOBSSNClaimTask(ModelBean claim, String claimantName, JDBCData data) throws Exception {
		String taskQuickKey = "Claim" + claim.getSystemId();
		ArrayList<String> taskList = new ArrayList<String>();
		taskList.add(0, "ClaimTask2006");
		for (ModelBean task : WorkflowRenderer.fetchTasks(taskQuickKey, taskList, data)) {
			if (task.gets("TemplateId").equals("ClaimTask2006")) {
				if (task.gets("Text").equals(claimantName)) {
					task.setValue("Status", "Completed");
					Helper_Beans.setIds(task, task);
					data.saveModelBean(task);
				}
			}
		}
	}

	/*
	 * public static void getUWNonRenewalExpirationTask(JDBCData data, ModelBean
	 * policy) throws Exception { ModelBean basicPolicy =
	 * policy.getBean("BasicPolicy"); StringDate expirationDate =
	 * basicPolicy.getDate("ExpirationDt"); ModelBean task = new ModelBean();
	 * 
	 * }
	 */
	private static ModelBean[] getClaimsForPolicy(JDBCData data, ModelBean policy) {
		try {
			String customerRef = policy.gets("CustomerRef");
			ModelBean basicPolicy = policy.getBean("BasicPolicy");
			ModelBean[] claimList = Claim.getList(data, customerRef, basicPolicy.gets("PolicyNumber"), false);

			return claimList;
		} catch (Exception e) {
			Log.error(e.getMessage(), e);
			e.printStackTrace();
			return null;
		}
	}

	public static void getClaimTaskFlatCancellation(JDBCData data, ModelBean policy) throws Exception {
		StringDate todayDt = DateRenderer.getStringDate();

		// Get Sorted Claim List
		ModelBean[] claimList = getClaimsForPolicy(data, policy);
		for (int i = 0; i < claimList.length; i++) {
			if (claimList[i].gets("StatusCd").equalsIgnoreCase("Open")) {
				ModelBean[] claimants = claimList[i].findBeansByFieldValue("Claimant", "StatusCd", "Open");

				for (int j = 0; j < claimants.length; j++) {
					String adjusterId = ProviderRenderer.getProviderNumber(
							claimants[j].findBeanByFieldValue("AssignedProvider", "AssignedProviderTypeCd", "AssignedAdjuster").gets("ProviderRef"));
					if (!adjusterId.equals("")) {
						ModelBean task = WorkflowRenderer.createTask(data, "ClaimTask3010", claimList[i], todayDt);
						task.setValue("CurrentOwner", adjusterId);
						task.setValue("OriginalOwner", adjusterId);
						task.setValue("OriginalOwnerCd", "User");
						Task.insertTask(data, task);
					}
				}
			}
		}

	}

	public static ModelBean getBuildingLimitsCalcDFMD(ModelBean application, String covALim) throws Exception {
		ModelBean basicPolicy = application.getBean("BasicPolicy");
		String subTypeCd = basicPolicy.gets("SubTypeCd");
		String productVersionIdRef = basicPolicy.gets("ProductVersionIdRef");

		ModelBean[] lines = application.getBeans("Line");
		for (ModelBean line : lines) {
			if (!line.gets("StatusCd").equalsIgnoreCase("Deleted")) {
				ModelBean[] risks = line.getBeans("Risk");
				for (ModelBean risk : risks) {
					if (!risk.gets("Status").equalsIgnoreCase("Deleted")) {

						ModelBean building = risk.getBean("Building");
						String coveageFactor = "UW::underwriting::coverageLimitPercent::coverAgeLimit";
						String covBLimitFactor = ProductRenderer.getCoderefValue(productVersionIdRef, coveageFactor, "covBLimit");
						String covCLimitFactor = ProductRenderer.getCoderefValue(productVersionIdRef, coveageFactor, "covCLimit");
						String covDLimitFactor = ProductRenderer.getCoderefValue(productVersionIdRef, coveageFactor, "covDLimit");

						// Coverage A
						String covALimit = building.gets("CovALimit");

						// Set Coverage A Building Limit = 100,000 Minimum Value
						if (StringRenderer.greaterThan(covALimit, "0")) {
							building.setValue("CovALimit", covALim);
							covALimit = building.gets("CovALimit");
						}

						// Coverage B
						String covBLimitIncluded = StringRenderer.multiply(covALimit, covBLimitFactor, 0);
						String covBLimitIncrease = building.gets("CovBLimitIncrease");
						if (covBLimitIncrease.equals("")) {
							covBLimitIncrease.equalsIgnoreCase("0");
						}
						/*
						 * if ( (subTypeCd.equals("DP2") || subTypeCd.equals("DP3")) ) {
						 * covBLimitIncrease =
						 * StringRenderer.round(StringRenderer.addFloat(covBLimitIncrease,
						 * StringRenderer.multiply(covBLimitIncrease, factor,0) ), FACTOR_VALUE_3); }
						 */
						String covBTotal = StringRenderer.addInt(covBLimitIncluded, covBLimitIncrease);
						building.setValue("CovBLimitIncrease", covBLimitIncrease);
						building.setValue("CovBLimitIncluded", covBLimitIncluded);
						building.setValue("CovBLimit", covBTotal);

						// Coverage C
						if (!building.gets("CovCLimit").equals("")) {
							String covCLimitIncluded = StringRenderer.multiply(covALimit, covCLimitFactor, 0);
							String covCLimitIncrease = building.gets("CovCLimitIncrease");
							if (covCLimitIncrease.equals("")) {
								covCLimitIncrease = "0";
								building.setValue("CovCLimitIncrease", "0");
							}
							String covCTotal = StringRenderer.addInt(covCLimitIncluded, covCLimitIncrease);
							building.setValue("CovCLimitIncluded", covCLimitIncluded);
							building.setValue("CovCLimit", covCTotal);
						}

						// Coverage D
						String covDIncluded = StringRenderer.multiply(covALimit, covDLimitFactor, 0);
						String covDLimitIncrease = building.gets("CovDLimitIncrease");
						if (covDLimitIncrease.equals("")) {
							covDLimitIncrease = "0";
							building.setValue("CovDLimitIncrease", "0");
						}
						String covDTotal = StringRenderer.addInt(covDIncluded, covDLimitIncrease);
						building.setValue("CovDLimitIncluded", covDIncluded);
						building.setValue("CovDLimit", covDTotal);

						// Coverage E
						String covELimitFactor = ProductRenderer.getCoderefValue(productVersionIdRef, coveageFactor, "covELimit");
						if (subTypeCd.equals("DP1")) {
							covELimitFactor = ProductRenderer.getCoderefValue(productVersionIdRef, coveageFactor, "covELimitDP1");
						}
						String covEIncluded = StringRenderer.multiply(covALimit, covELimitFactor, 0);
						String covELimitIncrease = building.gets("CovELimitIncrease");
						if (covELimitIncrease.equals("")) {
							covELimitIncrease = "0";
							building.setValue("CovELimitIncrease", "0");
						}
						String covETotal = StringRenderer.addInt(covEIncluded, covELimitIncrease);
						building.setValue("CovELimitIncluded", covEIncluded);
						building.setValue("CovELimit", covETotal);

					}
				}
			}
		}
		return application;
	}

	/**
	 * set a question reply value under a form; used to indicate when a coverage
	 * value is changed on endorsement, so the form will be attached
	 * 
	 * @param replies      ModelBean QuestionReplies
	 * @param questionName String
	 * @param value        String
	 */
	private static void setQuestionReply(ModelBean replies, String questionName, String value) {
		try {
			// Log.debug("In setQuestionReply");
			ModelBean reply = replies.findBeanByFieldValue("QuestionReply", "Name", questionName);

			// Create the Question Reply bean if it doesn't exist
			if (reply == null) {
				reply = new ModelBean("QuestionReply");
				reply.setValue("Name", questionName);
				replies.addValue(reply);
			}

			// Set the question reply value
			reply.setValue("Value", value);
			// reply.setValue("Status", "Active");
		} catch (Exception e) {
			Log.error("Error", e);
		}
	}

	/**
	 * add question reply value(s) under a form; used to indicate when a coverage
	 * value is changed on endorsement, so the form will be attached
	 * 
	 * @param replies ModelBean container
	 * @param replies ModelBean form
	 * @param replies ModelBean coverage
	 */

	public static void addFormQuestions(ModelBean container, ModelBean form) {
		try {
			Log.debug("In addFormQuestions");
			ModelBean replies = form.getBean("QuestionReplies");
			if (replies == null) {
				replies = new ModelBean("QuestionReplies");
				form.addValue(replies);
			}
			for (ModelBean reply : replies.getBeans()) {
				reply.setValue("Value", "");
			}
			if (StringRenderer.in(form.gets("Name"), "HO0416,DP0470")) {
				ModelBean building = container.getBean("Line").getBean("Risk").getBean("Building");
				if (form.gets("Name").equals("HO0416")) {
					setQuestionReply(replies, "BurglarAlarmCd", building.gets("BurglarAlarmCd"));
				}
				setQuestionReply(replies, "FireAlarmCd", building.gets("FireAlarmCd"));
				setQuestionReply(replies, "SprinklerSystem", building.gets("SprinklerSystem"));
				setQuestionReply(replies, "WaterTempAlarm", building.gets("WaterTempAlarm"));
			} else if (StringRenderer.in(form.gets("Name"), "HO0312,DP0312")) {
				ModelBean building = container.getBean("Line").getBean("Risk").getBean("Building");
				setQuestionReply(replies, "WindHailDed", building.gets("WindHailDed"));
			} else if (StringRenderer.in(form.gets("Name"), "DL2401,FEF,DLC104,DLC102,DL2519,DL2480,DL2471,DL2451,DL2410,DL2416,DL2507,DLC101,DL2537,")) {
				ModelBean building = container.getBean("Line").getBean("Risk").getBean("Building");
				setQuestionReply(replies, "CovLLimit", building.gets("CovLLimit"));
				if (StringRenderer.in(form.gets("Name"), "DL2480")) {
					ModelBean cov = CoverageRenderer.getActiveCoverage(container, "DL2480");
					setQuestionReply(replies, "Limit1", cov.findBeanByFieldValue("Limit", "LimitCd", "Limit1").gets("Value"));
					setQuestionReply(replies, "Limit2", cov.findBeanByFieldValue("Limit", "LimitCd", "Limit2").gets("Value"));
				}
			} else if (StringRenderer.in(form.gets("Name"), "DP0538,DL2489,DLC103,DL2411")) {
				ModelBean building = container.getBean("Line").getBean("Risk").getBean("Building");
				setQuestionReply(replies, "CovLLimit", building.gets("CovLLimit"));
				setQuestionReply(replies, "OccupancyCd", building.gets("OccupancyCd"));
				if (!form.gets("Name").equals("DLC103")) {
					setQuestionReply(replies, "NumUnits", building.gets("Units"));
				}
			} else if (StringRenderer.in(form.gets("Name"), "DP1914,DP0414")) {
				ModelBean building = container.getBean("Line").getBean("Risk").getBean("Building");
				setQuestionReply(replies, "CovELimit", building.gets("CovELimit"));
			} else if (StringRenderer.in(form.gets("Name"), "DP0471")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "DP0471");
				setQuestionReply(replies, "OrdOrLawPercentage", cov.gets("OrdOrLawPercentage"));
			} else if (StringRenderer.in(form.gets("Name"), "DL2419")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "DL2419");
				setQuestionReply(replies, "Location", cov.gets("Location"));
				setQuestionReply(replies, "StateRegistered", cov.gets("StateRegistered"));
				setQuestionReply(replies, "OtherLocationDesc", cov.gets("OtherLocationDesc"));
				setQuestionReply(replies, "NumberOfPersons", cov.gets("NumberOfPersons"));
			} else if (StringRenderer.in(form.gets("Name"), "HO0426,HO0427,PHN133,PHN134,DP1610")) {
				ModelBean basicPolicy = container.getBean("BasicPolicy");
				setQuestionReply(replies, "SubTypeCd", basicPolicy.gets("SubTypeCd"));
			} else if (form.gets("Name").equals("HO0441")) {
				ModelBean[] ais = BeanRenderer.findBeansByFieldValueAndStatus(container, "AI", "InterestTypeCd", "Additional Insured-Residence Premises",
						"Active");
				for (ModelBean aiInfo : ais) {
					String aiID = aiInfo.getId();
					setQuestionReply(replies, aiID + "AI_InterestName", aiInfo.gets("InterestName"));
					setQuestionReply(replies, aiID + "AI_Comments", aiInfo.gets("Comments"));
					ModelBean nameInfo = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "AIParty").getBean("NameInfo");
					if (nameInfo != null && !nameInfo.equals("")) {
						setQuestionReply(replies, aiID + "AI_IndexName", nameInfo.gets("IndexName"));
					}
					ModelBean emailInfo = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "AIParty").getBean("EmailInfo");
					if (emailInfo != null && !emailInfo.equals("")) {
						setQuestionReply(replies, aiID + "AI_EmailAddr", emailInfo.gets("EmailAddr"));
					}
					ModelBean phoneInfo = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "AIParty").getBean("PhoneInfo");
					if (phoneInfo != null && !phoneInfo.equals("")) {
						setQuestionReply(replies, aiID + "AI_PhoneNumber", phoneInfo.gets("PhoneNumber"));
					}
					ModelBean addr = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "AIParty").getBean("Addr");
					if (addr != null && !addr.equals("")) {
						setQuestionReply(replies, aiID + "AI_Addr1", addr.gets("Addr1"));
						setQuestionReply(replies, aiID + "AI_City", addr.gets("City"));
						setQuestionReply(replies, aiID + "AI_StateProvCd", addr.gets("StateProvCd"));
						setQuestionReply(replies, aiID + "AI_PostalCode", addr.gets("PostalCode"));
						setQuestionReply(replies, aiID + "AI_RegionCd", addr.gets("RegionCd"));

					}

				}
			} else if (form.gets("Name").equals("HO0410")) {
				ModelBean[] ais = BeanRenderer.findBeansByFieldValueAndStatus(container, "AI", "InterestTypeCd", "Additional Interests- Residence Premises",
						"Active");
				for (ModelBean aiInfo : ais) {
					String aiID = aiInfo.getId();
					setQuestionReply(replies, aiID + "AI_InterestName", aiInfo.gets("InterestName"));
					setQuestionReply(replies, aiID + "AI_Comments", aiInfo.gets("Comments"));
					ModelBean nameInfo = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "AIParty").getBean("NameInfo");
					if (nameInfo != null && !nameInfo.equals("")) {
						setQuestionReply(replies, aiID + "AI_IndexName", nameInfo.gets("IndexName"));
					}
					ModelBean emailInfo = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "AIParty").getBean("EmailInfo");
					if (emailInfo != null && !emailInfo.equals("")) {
						setQuestionReply(replies, aiID + "AI_EmailAddr", emailInfo.gets("EmailAddr"));
					}
					ModelBean phoneInfo = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "AIParty").getBean("PhoneInfo");
					if (phoneInfo != null && !phoneInfo.equals("")) {
						setQuestionReply(replies, aiID + "AI_PhoneNumber", phoneInfo.gets("PhoneNumber"));
					}
					ModelBean addr = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "AIParty").getBean("Addr");
					if (addr != null && !addr.equals("")) {
						setQuestionReply(replies, aiID + "AI_Addr1", addr.gets("Addr1"));
						setQuestionReply(replies, aiID + "AI_City", addr.gets("City"));
						setQuestionReply(replies, aiID + "AI_StateProvCd", addr.gets("StateProvCd"));
						setQuestionReply(replies, aiID + "AI_PostalCode", addr.gets("PostalCode"));
						setQuestionReply(replies, aiID + "AI_RegionCd", addr.gets("RegionCd"));

					}

				}
			} else if (form.gets("Name").equals("HO0615")) {
				ModelBean[] ais = BeanRenderer.findBeansByFieldValueAndStatus(container, "AI", "InterestTypeCd", "Trust", "Active");
				for (ModelBean aiInfo : ais) {
					String aiID = aiInfo.getId();
					setQuestionReply(replies, aiID + "AI_InterestName", aiInfo.gets("InterestName"));
					setQuestionReply(replies, aiID + "AI_Comments", aiInfo.gets("Comments"));
					ModelBean nameInfo = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "AIParty").getBean("NameInfo");
					if (nameInfo != null && !nameInfo.equals("")) {
						setQuestionReply(replies, aiID + "AI_IndexName", nameInfo.gets("IndexName"));
					}
					ModelBean emailInfo = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "AIParty").getBean("EmailInfo");
					if (emailInfo != null && !emailInfo.equals("")) {
						setQuestionReply(replies, aiID + "AI_EmailAddr", emailInfo.gets("EmailAddr"));
					}
					ModelBean phoneInfo = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "AIParty").getBean("PhoneInfo");
					if (phoneInfo != null && !phoneInfo.equals("")) {
						setQuestionReply(replies, aiID + "AI_PhoneNumber", phoneInfo.gets("PhoneNumber"));
					}
					ModelBean addr = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "AIParty").getBean("Addr");
					if (addr != null && !addr.equals("")) {
						setQuestionReply(replies, aiID + "AI_Addr1", addr.gets("Addr1"));
						setQuestionReply(replies, aiID + "AI_City", addr.gets("City"));
						setQuestionReply(replies, aiID + "AI_StateProvCd", addr.gets("StateProvCd"));
						setQuestionReply(replies, aiID + "AI_PostalCode", addr.gets("PostalCode"));
						setQuestionReply(replies, aiID + "AI_RegionCd", addr.gets("RegionCd"));

					}
					ModelBean nameInfoTrusteeParty = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "TrusteeParty").getBean("NameInfo");
					if (nameInfoTrusteeParty != null && !nameInfoTrusteeParty.equals("")) {
						setQuestionReply(replies, aiID + "AI_TrusteePartyIndexName", nameInfoTrusteeParty.gets("IndexName"));
					}
					ModelBean addrTrusteeParty = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "TrusteeParty").getBean("Addr");
					if (addrTrusteeParty != null && !addrTrusteeParty.equals("")) {
						setQuestionReply(replies, aiID + "AI_TrusteePartyAddr1", addrTrusteeParty.gets("Addr1"));
						setQuestionReply(replies, aiID + "AI_TrusteePartyCity", addrTrusteeParty.gets("City"));
						setQuestionReply(replies, aiID + "AI_TrusteePartyStateProvCd", addrTrusteeParty.gets("StateProvCd"));
						setQuestionReply(replies, aiID + "AI_TrusteePartyPostalCode", addrTrusteeParty.gets("PostalCode"));
						setQuestionReply(replies, aiID + "AI_TrusteePartyRegionCd", addrTrusteeParty.gets("RegionCd"));

					}

				}
			} else if (StringRenderer.in(form.gets("Name"), "DP0441,DPC106")) {
				ModelBean[] ais = null;
				if (form.gets("Name").equals("DP0441")) {
					ais = BeanRenderer.findBeansByFieldValueAndStatus(container, "AI", "InterestTypeCd", "Additional Insured -Described Location", "Active");
				} else if (form.gets("Name").equals("DPC106")) {
					ais = BeanRenderer.findBeansByFieldValueAndStatus(container, "AI", "InterestTypeCd", "Additional Interests - Described Location", "Active");
				}
				for (ModelBean aiInfo : ais) {
					String aiID = aiInfo.getId();
					setQuestionReply(replies, aiID + "AI_InterestName", aiInfo.gets("InterestName"));
					setQuestionReply(replies, aiID + "AI_Comments", aiInfo.gets("Comments"));
					ModelBean nameInfo = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "AIParty").getBean("NameInfo");
					if (nameInfo != null && !nameInfo.equals("")) {
						setQuestionReply(replies, aiID + "AI_IndexName", nameInfo.gets("IndexName"));
					}
					ModelBean emailInfo = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "AIParty").getBean("EmailInfo");
					if (emailInfo != null && !emailInfo.equals("")) {
						setQuestionReply(replies, aiID + "AI_EmailAddr", emailInfo.gets("EmailAddr"));
					}
					ModelBean phoneInfo = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "AIParty").getBean("PhoneInfo");
					if (phoneInfo != null && !phoneInfo.equals("")) {
						setQuestionReply(replies, aiID + "AI_PhoneNumber", phoneInfo.gets("PhoneNumber"));
					}
					ModelBean addr = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "AIParty").getBean("Addr");
					if (addr != null && !addr.equals("")) {
						setQuestionReply(replies, aiID + "AI_Addr1", addr.gets("Addr1"));
						setQuestionReply(replies, aiID + "AI_City", addr.gets("City"));
						setQuestionReply(replies, aiID + "AI_StateProvCd", addr.gets("StateProvCd"));
						setQuestionReply(replies, aiID + "AI_PostalCode", addr.gets("PostalCode"));
						setQuestionReply(replies, aiID + "AI_RegionCd", addr.gets("RegionCd"));

					}

				}
			} else if (StringRenderer.in(form.gets("Name"), "HO2475")) {
				ModelBean covHO2475 = CoverageRenderer.getActiveCoverage(container, "HO2475");
				ModelBean[] covItems = covHO2475.findBeansByFieldValue("CoverageItem", "Status", "Active");
				for (ModelBean covItem : covItems) {
					String covItemID = covItem.getId();
					setQuestionReply(replies, covItemID + "WaterCraftPowerTypeI",
							covItem.findBeanByFieldValue("CoverageData", "CoverageDataCd", "WaterCraftPowerTypeI").gets("Value"));
					setQuestionReply(replies, covItemID + "ItemDesc", covItem.gets("ItemDesc"));
					setQuestionReply(replies, covItemID + "WaterCraftDesc",
							covItem.findBeanByFieldValue("CoverageData", "CoverageDataCd", "WaterCraftDesc").gets("Value"));
				}

			} else if (StringRenderer.in(form.gets("Name"), "HO2472")) {
				ModelBean covHO2472 = CoverageRenderer.getActiveCoverage(container, "HO2472");
				ModelBean[] covItems = covHO2472.findBeansByFieldValue("CoverageItem", "Status", "Active");
				for (ModelBean covItem : covItems) {
					String covItemID = covItem.getId();
					setQuestionReply(replies, covItemID + "ItemDesc", covItem.gets("ItemDesc"));
				}

			} else if (StringRenderer.in(form.gets("Name"), "HO2471")) {
				ModelBean covHO2471 = CoverageRenderer.getActiveCoverage(container, "HO2471");
				ModelBean[] covItems = covHO2471.findBeansByFieldValue("CoverageItem", "Status", "Active");
				for (ModelBean covItem : covItems) {
					String covItemID = covItem.getId();
					setQuestionReply(replies, covItemID + "Profession",
							covItem.findBeanByFieldValue("CoverageData", "CoverageDataCd", "Profession").gets("Value"));
					setQuestionReply(replies, covItemID + "ItemDesc", covItem.gets("ItemDesc"));
					setQuestionReply(replies, covItemID + "ItemLimitIncluded",
							covItem.findBeanByFieldValue("Limit", "LimitCd", "ItemLimitIncluded").gets("Value"));
				}

			} else if (StringRenderer.in(form.gets("Name"), "HO2470")) {
				ModelBean covHO2470 = CoverageRenderer.getActiveCoverage(container, "HO2470");
				ModelBean[] covItems = covHO2470.findBeansByFieldValue("CoverageItem", "Status", "Active");
				for (ModelBean covItem : covItems) {
					String covItemID = covItem.getId();
					setQuestionReply(replies, covItemID + "ItemDesc", covItem.gets("ItemDesc"));
					setQuestionReply(replies, covItemID + "ItemLimit1", covItem.findBeanByFieldValue("Limit", "LimitCd", "ItemLimit1").gets("Value"));
					setQuestionReply(replies, covItemID + "State", covItem.findBeanByFieldValue("CoverageData", "CoverageDataCd", "State").gets("Value"));
					setQuestionReply(replies, covItemID + "City", covItem.findBeanByFieldValue("CoverageData", "CoverageDataCd", "City").gets("Value"));
					setQuestionReply(replies, covItemID + "Zip", covItem.findBeanByFieldValue("CoverageData", "CoverageDataCd", "Zip").gets("Value"));
				}

			} else if (StringRenderer.in(form.gets("Name"), "HO2464")) {
				ModelBean covHO2464 = CoverageRenderer.getActiveCoverage(container, "HO2464");
				ModelBean[] covItems = covHO2464.findBeansByFieldValue("CoverageItem", "Status", "Active");
				for (ModelBean covItem : covItems) {
					String covItemID = covItem.getId();
					setQuestionReply(replies, covItemID + "ItemDesc", covItem.gets("ItemDesc"));
					setQuestionReply(replies, covItemID + "SnowMobileSerialNumber",
							covItem.findBeanByFieldValue("CoverageData", "CoverageDataCd", "SnowMobileSerialNumber").gets("Value"));
				}

			} else if (StringRenderer.in(form.gets("Name"), "HO2413")) {
				ModelBean covHO2413 = CoverageRenderer.getActiveCoverage(container, "HO2413");
				setQuestionReply(replies, "NumberOfPersons", covHO2413.gets("NumberOfPersons"));
			} else if (StringRenderer.in(form.gets("Name"), "HO2338")) {
				ModelBean covHO2338 = CoverageRenderer.getActiveCoverage(container, "HO2338");
				setQuestionReply(replies, "Limit1", covHO2338.findBeanByFieldValue("Limit", "LimitCd", "Limit1").gets("Value"));
				setQuestionReply(replies, "Location", covHO2338.gets("Location"));
				setQuestionReply(replies, "StateRegistered", covHO2338.gets("StateRegistered"));
				setQuestionReply(replies, "OtherLocationDesc", covHO2338.gets("OtherLocationDesc"));
				setQuestionReply(replies, "NumberOfPersons", covHO2338.gets("NumberOfPersons"));

			} else if (StringRenderer.in(form.gets("Name"), "HO2320")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "WB");
				setQuestionReply(replies, "Limit1", cov.findBeanByFieldValue("Limit", "LimitCd", "Limit1").gets("Value"));
			} else if (StringRenderer.in(form.gets("Name"), "HO0442")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "HO0442");
				ModelBean[] covItems = cov.findBeansByFieldValue("CoverageItem", "Status", "Active");
				for (ModelBean covItem : covItems) {
					String covItemID = covItem.getId();
					setQuestionReply(replies, covItemID + "ItemDesc", covItem.gets("ItemDesc"));
					setQuestionReply(replies, covItemID + "ItemLimit1", covItem.findBeanByFieldValue("Limit", "LimitCd", "ItemLimit1").gets("Value"));
					setQuestionReply(replies, covItemID + "BusinessLoc",
							covItem.findBeanByFieldValue("CoverageData", "CoverageDataCd", "BusinessLoc").gets("Value"));
					setQuestionReply(replies, covItemID + "BusinessType",
							covItem.findBeanByFieldValue("CoverageData", "CoverageDataCd", "BusinessType").gets("Value"));
				}

			} else if (StringRenderer.in(form.gets("Name"), "HO0412")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "HO0412");
				setQuestionReply(replies, "Limit1", cov.findBeanByFieldValue("Limit", "LimitCd", "Limit1").gets("Value"));
			} else if (StringRenderer.in(form.gets("Name"), "HO0435")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "HO0435");
				setQuestionReply(replies, "Limit1", cov.findBeanByFieldValue("Limit", "LimitCd", "Limit1").gets("Value"));
			} else if (StringRenderer.in(form.gets("Name"), "HO0557")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "HO0557");
				setQuestionReply(replies, "Limit1", cov.findBeanByFieldValue("Limit", "LimitCd", "Limit1").gets("Value"));
				setQuestionReply(replies, "Limit2", cov.findBeanByFieldValue("Limit", "LimitCd", "Limit2").gets("Value"));
			} else if (StringRenderer.in(form.gets("Name"), "HO0450")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "HO0450");
				ModelBean[] covItems = cov.findBeansByFieldValue("CoverageItem", "Status", "Active");
				for (ModelBean covItem : covItems) {
					String covItemID = covItem.getId();
					setQuestionReply(replies, covItemID + "ItemDesc", covItem.gets("ItemDesc"));
					setQuestionReply(replies, covItemID + "ItemLimit1", covItem.findBeanByFieldValue("Limit", "LimitCd", "ItemLimit1").gets("Value"));
					setQuestionReply(replies, covItemID + "State", covItem.findBeanByFieldValue("CoverageData", "CoverageDataCd", "State").gets("Value"));
					setQuestionReply(replies, covItemID + "City", covItem.findBeanByFieldValue("CoverageData", "CoverageDataCd", "City").gets("Value"));
					setQuestionReply(replies, covItemID + "Zip", covItem.findBeanByFieldValue("CoverageData", "CoverageDataCd", "Zip").gets("Value"));
				}

			} else if (StringRenderer.in(form.gets("Name"), "HO0453")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "HO0453");
				setQuestionReply(replies, "Limit1", cov.findBeanByFieldValue("Limit", "LimitCd", "Limit1").gets("Value"));
			} else if (StringRenderer.in(form.gets("Name"), "HO0454")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "HO0454");
				setQuestionReply(replies, "Limit1", cov.gets("NumberOfPersons"));
			} else if (StringRenderer.in(form.gets("Name"), "HO0420")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "HO0420");
				setQuestionReply(replies, "Limit1", cov.gets("NumberOfPersons"));
			} else if (StringRenderer.in(form.gets("Name"), "HO0461")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "HO0461");
				ModelBean[] covItems = cov.findBeansByFieldValue("CoverageItem", "Status", "Active");
				for (ModelBean covItem : covItems) {
					String covItemID = covItem.getId();
					setQuestionReply(replies, covItemID + "ItemDesc", covItem.gets("ItemDesc"));
					setQuestionReply(replies, covItemID + "ItemLimit1", covItem.findBeanByFieldValue("Limit", "LimitCd", "ItemLimit1").gets("Value"));
					setQuestionReply(replies, covItemID + "ClassCd", covItem.findBeanByFieldValue("CoverageData", "CoverageDataCd", "ClassCd").gets("Value"));
					setQuestionReply(replies, covItemID + "AppraisalDt",
							covItem.findBeanByFieldValue("CoverageData", "CoverageDataCd", "AppraisalDt").gets("Value"));
				}

			} else if (StringRenderer.in(form.gets("Name"), "HO0465")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "HO0465");
				ModelBean[] covItems = cov.findBeansByFieldValue("CoverageItem", "Status", "Active");
				for (ModelBean covItem : covItems) {
					String covItemID = covItem.getId();
					setQuestionReply(replies, covItemID + "ItemDesc", covItem.gets("ItemDesc"));
					setQuestionReply(replies, covItemID + "ItemLimit1", covItem.findBeanByFieldValue("Limit", "LimitCd", "ItemLimit1").gets("Value"));
					setQuestionReply(replies, covItemID + "ItemLimitIncluded",
							covItem.findBeanByFieldValue("Limit", "LimitCd", "ItemLimitIncluded").gets("Value"));
					setQuestionReply(replies, covItemID + "ItemLimitIncr", covItem.findBeanByFieldValue("Limit", "LimitCd", "ItemLimitIncr").gets("Value"));
				}

			} else if (StringRenderer.in(form.gets("Name"), "HO0466")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "HO0466");
				ModelBean[] covItems = cov.findBeansByFieldValue("CoverageItem", "Status", "Active");
				for (ModelBean covItem : covItems) {
					String covItemID = covItem.getId();
					setQuestionReply(replies, covItemID + "ItemDesc", covItem.gets("ItemDesc"));
					setQuestionReply(replies, covItemID + "ItemLimit1", covItem.findBeanByFieldValue("Limit", "LimitCd", "ItemLimit1").gets("Value"));
					setQuestionReply(replies, covItemID + "ItemLimitIncluded",
							covItem.findBeanByFieldValue("Limit", "LimitCd", "ItemLimitIncluded").gets("Value"));
					setQuestionReply(replies, covItemID + "ItemLimitIncr", covItem.findBeanByFieldValue("Limit", "LimitCd", "ItemLimitIncr").gets("Value"));
				}

			} else if (StringRenderer.in(form.gets("Name"), "HO0440")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "HO0440");
				ModelBean[] covItems = cov.findBeansByFieldValue("CoverageItem", "Status", "Active");
				for (ModelBean covItem : covItems) {
					String covItemID = covItem.getId();
					setQuestionReply(replies, covItemID + "ItemDesc", covItem.gets("ItemDesc"));
					setQuestionReply(replies, covItemID + "ItemLimit1", covItem.findBeanByFieldValue("Limit", "LimitCd", "ItemLimit1").gets("Value"));
				}

			} else if (StringRenderer.in(form.gets("Name"), "HO0448")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "HO0448");
				ModelBean[] covItems = cov.findBeansByFieldValue("CoverageItem", "Status", "Active");
				for (ModelBean covItem : covItems) {
					String covItemID = covItem.getId();
					setQuestionReply(replies, covItemID + "ItemDesc", covItem.gets("ItemDesc"));
					setQuestionReply(replies, covItemID + "ItemLimit1", covItem.findBeanByFieldValue("Limit", "LimitCd", "ItemLimit1").gets("Value"));
				}

			} else if (StringRenderer.in(form.gets("Name"), "HO0477")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "HO0477");
				setQuestionReply(replies, "Limit1", cov.gets("NumberOfPersons"));
			} else if (StringRenderer.in(form.gets("Name"), "HO0556")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "HO0556");
				setQuestionReply(replies, "Limit1", cov.findBeanByFieldValue("Limit", "LimitCd", "Limit1").gets("Value"));
				setQuestionReply(replies, "Limit2", cov.findBeanByFieldValue("Limit", "LimitCd", "Limit2").gets("Value"));
			} else if (StringRenderer.in(form.gets("Name"), "HO0555")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "HO0555");
				setQuestionReply(replies, "Limit1", cov.findBeanByFieldValue("Limit", "LimitCd", "Limit1").gets("Value"));
				setQuestionReply(replies, "Limit2", cov.findBeanByFieldValue("Limit", "LimitCd", "Limit2").gets("Value"));
			} else if (StringRenderer.in(form.gets("Name"), "HO0430")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "HO0430");
				setQuestionReply(replies, "Limit1", cov.findBeanByFieldValue("Limit", "LimitCd", "Limit1").gets("Value"));
				setQuestionReply(replies, "Limit2", cov.findBeanByFieldValue("Limit", "LimitCd", "Limit2").gets("Value"));
			} else if (StringRenderer.in(form.gets("Name"), "ELFL")) { // Forms - DE
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "ELFL");
				setQuestionReply(replies, "Limit1", cov.findBeanByFieldValue("Limit", "LimitCd", "Limit1").gets("Value"));
				setQuestionReply(replies, "Limit2", cov.findBeanByFieldValue("Limit", "LimitCd", "Limit2").gets("Value"));
			} else if (form.gets("Name").equals("HO0543")) {
				// Add Insured and AI details
				if (container.getBean("Insured").gets("EntityTypeCd").equalsIgnoreCase("Trust")) {
					ModelBean insuredParty = container.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "InsuredParty");
					setQuestionReply(replies, "Insured_PreferredDeliveryMethod", container.getBean("Insured").gets("PreferredDeliveryMethod"));
					ModelBean nameInfo = insuredParty.getBean("NameInfo");
					if (nameInfo != null && !nameInfo.equals("")) {
						setQuestionReply(replies, "Insured_CommercialName", nameInfo.gets("CommercialName"));
					}
					ModelBean personInfo = insuredParty.getBean("PersonInfo");
					if (personInfo != null && !personInfo.equals("")) {
						setQuestionReply(replies, "Insured_BirthDt", personInfo.gets("BirthDt"));
						setQuestionReply(replies, "Insured_EducationCd", personInfo.gets("EducationCd"));
						setQuestionReply(replies, "Insured_OccupationCd", personInfo.gets("OccupationCd"));
						setQuestionReply(replies, "Insured_EducationCd", personInfo.gets("EducationCd"));
						setQuestionReply(replies, "Insured_OccupationCd", personInfo.gets("OccupationCd"));
					}

					ModelBean taxInfo = insuredParty.getBean("TaxInfo");
					if (taxInfo != null && !taxInfo.equals("")) {
						setQuestionReply(replies, "Insured_SSN", taxInfo.gets("SSN"));
					}
					ModelBean emailInfo = insuredParty.findBeanByFieldValue("EmailInfo", "EmailTypeCd", "InsuredEmail");
					if (emailInfo != null && !emailInfo.equals("")) {
						setQuestionReply(replies, "Insured_EmailAddr", emailInfo.gets("EmailAddr"));
					}
					ModelBean phoneInfo = insuredParty.findBeanByFieldValue("PhoneInfo", "PhoneTypeCd", "InsuredPhonePrimary");
					if (phoneInfo != null && !phoneInfo.equals("")) {
						setQuestionReply(replies, "Insured_PhoneNumber", phoneInfo.gets("PhoneNumber"));
					}
					ModelBean addr = insuredParty.findBeanByFieldValue("Addr", "AddrTypeCd", "InsuredMailingAddr");
					if (addr != null && !addr.equals("")) {
						setQuestionReply(replies, "Insured_Addr1", addr.gets("Addr1"));
						setQuestionReply(replies, "Insured_City", addr.gets("City"));
						setQuestionReply(replies, "Insured_StateProvCd", addr.gets("StateProvCd"));
						setQuestionReply(replies, "Insured_PostalCode", addr.gets("PostalCode"));
						setQuestionReply(replies, "Insured_RegionCd", addr.gets("RegionCd"));

					}
				}

				ModelBean[] ais = BeanRenderer.findBeansByFieldValueAndStatus(container, "AI", "InterestTypeCd", "Trust - Beneficiary or Grantor", "Active");
				for (ModelBean aiInfo : ais) {
					String aiID = aiInfo.getId();
					setQuestionReply(replies, aiID + "AI_InterestName", aiInfo.gets("InterestName"));
					setQuestionReply(replies, aiID + "AI_Comments", aiInfo.gets("Comments"));
					ModelBean nameInfo = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "AIParty").getBean("NameInfo");
					if (nameInfo != null && !nameInfo.equals("")) {
						setQuestionReply(replies, aiID + "AI_IndexName", nameInfo.gets("IndexName"));
					}
					ModelBean emailInfo = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "AIParty").getBean("EmailInfo");
					if (emailInfo != null && !emailInfo.equals("")) {
						setQuestionReply(replies, aiID + "AI_EmailAddr", emailInfo.gets("EmailAddr"));
					}
					ModelBean phoneInfo = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "AIParty").getBean("PhoneInfo");
					if (phoneInfo != null && !phoneInfo.equals("")) {
						setQuestionReply(replies, aiID + "AI_PhoneNumber", phoneInfo.gets("PhoneNumber"));
					}
					ModelBean addr = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "AIParty").getBean("Addr");
					if (addr != null && !addr.equals("")) {
						setQuestionReply(replies, aiID + "AI_Addr1", addr.gets("Addr1"));
						setQuestionReply(replies, aiID + "AI_City", addr.gets("City"));
						setQuestionReply(replies, aiID + "AI_StateProvCd", addr.gets("StateProvCd"));
						setQuestionReply(replies, aiID + "AI_PostalCode", addr.gets("PostalCode"));
						setQuestionReply(replies, aiID + "AI_RegionCd", addr.gets("RegionCd"));

					}
					ModelBean nameInfoTrusteeParty = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "TrusteeParty").getBean("NameInfo");
					if (nameInfoTrusteeParty != null && !nameInfoTrusteeParty.equals("")) {
						setQuestionReply(replies, aiID + "AI_TrusteePartyIndexName", nameInfoTrusteeParty.gets("IndexName"));
					}
					ModelBean addrTrusteeParty = aiInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "TrusteeParty").getBean("Addr");
					if (addrTrusteeParty != null && !addrTrusteeParty.equals("")) {
						setQuestionReply(replies, aiID + "AI_TrusteePartyAddr1", addrTrusteeParty.gets("Addr1"));
						setQuestionReply(replies, aiID + "AI_TrusteePartyCity", addrTrusteeParty.gets("City"));
						setQuestionReply(replies, aiID + "AI_TrusteePartyStateProvCd", addrTrusteeParty.gets("StateProvCd"));
						setQuestionReply(replies, aiID + "AI_TrusteePartyPostalCode", addrTrusteeParty.gets("PostalCode"));
						setQuestionReply(replies, aiID + "AI_TrusteePartyRegionCd", addrTrusteeParty.gets("RegionCd"));

					}
				}
			} else if (StringRenderer.in(form.gets("Name"), "HOC106,DPC105,DPC102,TP2381")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "WB");
				setQuestionReply(replies, "Limit1", cov.findBeanByFieldValue("Limit", "LimitCd", "Limit1").gets("Value"));

			} else if (StringRenderer.in(form.gets("Name"), "PHN127,PHNWHFLT")) {
				ModelBean building = container.getBean("Line").getBean("Risk").getBean("Building");
				setQuestionReply(replies, "WindHailDed", building.gets("WindHailDed"));
			} else if (StringRenderer.in(form.gets("Name"), "IM325")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "IM325");
				ModelBean[] covItems = cov.findBeansByFieldValue("CoverageItem", "Status", "Active");
				for (ModelBean covItem : covItems) {
					String covItemID = covItem.getId();
					setQuestionReply(replies, covItemID + "ItemDesc", covItem.gets("ItemDesc"));
					setQuestionReply(replies, covItemID + "ItemLimit1", covItem.findBeanByFieldValue("Limit", "LimitCd", "ItemLimit1").gets("Value"));
					setQuestionReply(replies, covItemID + "ClassCd", covItem.findBeanByFieldValue("CoverageData", "CoverageDataCd", "ClassCd").gets("Value"));
					setQuestionReply(replies, covItemID + "AppraisalDt",
							covItem.findBeanByFieldValue("CoverageData", "CoverageDataCd", "AppraisalDt").gets("Value"));
					setQuestionReply(replies, covItemID + "SelectInd",
							covItem.findBeanByFieldValue("CoverageData", "CoverageDataCd", "SelectInd").gets("Value"));
				}
			} else if (StringRenderer.in(form.gets("Name"), "HO0451")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "HO0451");
				setQuestionReply(replies, "Limit1", cov.findBeanByFieldValue("Limit", "LimitCd", "Limit1").gets("Value"));
			} else if (StringRenderer.in(form.gets("Name"), "DPC2320")) {
				ModelBean cov = CoverageRenderer.getActiveCoverage(container, "WB");
				setQuestionReply(replies, "Limit1", cov.findBeanByFieldValue("Limit", "LimitCd", "Limit1").gets("Value"));
			} else if (StringRenderer.in(form.gets("Name"), "PHN144")) {
				ModelBean building = container.getBean("Line").getBean("Risk").getBean("Building");
				setQuestionReply(replies, "PHN144PrintInd", building.gets("RecalculatePremium") + "_" + StringDate.currentTime());
			}

		} catch (Exception e) {
			Log.error("Error", e);
		}
	}

	public static int mod(String str1, String str2) throws Exception {
		try {
			return Integer.parseInt(str1) % Integer.parseInt(str2);
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * public static void getEBEmailClaim(ModelBean claim, String userId, String
	 * securityId, String sessionId, String conversationId, JDBCData data) throws
	 * Exception { ModelBean response = claim.getParentBean(); ModelBean
	 * responseParams = response.getBean("ResponseParams"); StringDate todayDt =
	 * DateRenderer.getStringDate(); String todayTm = DateRenderer.getTime(); String
	 * transNo = claim.gets("TransactionNumber"); if (transNo.equals("")) { transNo
	 * = "1"; } boolean isReserveSet = false; if
	 * (!claim.gets("EBEmailClaimInd").equals("Yes")) { ModelBean[] claimants =
	 * ClaimRenderer.getOpenClaimants(claim); for(ModelBean claimant : claimants) {
	 * ModelBean feature = claimant.findBeanByFieldValue("Feature", "FeatureCd",
	 * "EB"); if ( feature != null ) { String reserveAmt =
	 * feature.gets("ReserveAmt"); if ( !reserveAmt.equalsIgnoreCase("") &&
	 * StringRenderer.greaterThan(reserveAmt, "0") ) { isReserveSet = true; break; }
	 * } } } // If Equipment Breakdown Reserve is set if (isReserveSet) { ModelBean
	 * transactionInfo = claim.findBeanByFieldValue("ClaimTransactionInfo",
	 * "TransactionNumber", transNo); // Email ModelBean output = new ModelBean();
	 * String submissionNumber = "1"; if
	 * (!transactionInfo.gets("SubmissionNumber").equals("") ) { submissionNumber =
	 * transactionInfo.gets("SubmissionNumber"); }
	 * 
	 * // Create Output Bean ModelBean logKeyParam =
	 * responseParams.findBeanByFieldValue("Param", "Name", "LogKey"); String logKey
	 * = logKeyParam.gets("Value"); StringDate todayStringDt =
	 * DateRenderer.getStringDate(responseParams); output =
	 * DCRenderer.addCorrespondence(data, claim, "FirstNoticeofLossEB",
	 * "CLTransaction", submissionNumber, userId, todayStringDt, logKey);
	 * 
	 * output.setValue("AddUser","System"); output.setValue("AddDt",todayDt);
	 * output.setValue("AddTm",todayTm); DCRenderer.processOutput(data, userId,
	 * securityId, sessionId, conversationId, claim, output, todayDt, todayTm);
	 * claim.setValue("EBEmailClaimInd", "Yes"); } }
	 */

	public static Money getClaimantTotalIndemnity(JDBCData data, ModelBean claim, ModelBean claimant) throws Exception {
		// Money reserveIndemnityAmt = new Money("0.00");
		Money exceedAmt = new Money("0.00");
		Money incurredTotal = new Money("0.00");
		Money recoveries = new Money("0.00");
		ModelBean[] covFeatures = claimant.getBeans("Feature");
		for (ModelBean feature : covFeatures) {
			// reserveIndemnityAmt =
			// reserveIndemnityAmt.add(ClaimantTransactionRenderer.sumReserveIndemnityAmt(claim,
			// feature));
			incurredTotal = Feature.calculateIncurredType(claim, feature, "Indemnity");
			recoveries = new Money(feature.gets("RecoveryAmt"));
			exceedAmt = exceedAmt.add(incurredTotal.subtract(recoveries));
		}
		// return reserveIndemnityAmt;
		return exceedAmt;
	}

	private static Money getClaimTotalIndemnity(JDBCData data, ModelBean claim) throws Exception {
		// Money reserveIndemnityAmt = new Money("0.00");
		Money exceedAmt = new Money("0.00");
		Money incurredTotal = new Money("0.00");
		Money recoveries = new Money("0.00");
		ModelBean[] claimants = ClaimRenderer.getOpenClaimants(claim);
		for (ModelBean claimant : claimants) {
			ModelBean[] covFeatures = claimant.getBeans("Feature");
			for (ModelBean feature : covFeatures) {
				// reserveIndemnityAmt =
				// reserveIndemnityAmt.add(ClaimantTransactionRenderer.sumReserveIndemnityAmt(claim,
				// feature));
				incurredTotal = Feature.calculateIncurredType(claim, feature, "Indemnity");
				recoveries = new Money(feature.gets("RecoveryAmt"));
				exceedAmt = exceedAmt.add(incurredTotal.subtract(recoveries));
			}
		}

		// return reserveIndemnityAmt;
		return exceedAmt;
	}

	/**
	 * get the Product Coverage beans
	 * 
	 * @param container ModelBean
	 * @param line      ModelBean
	 * @return ModelBean[] productRiskCoverage
	 */

	public static ModelBean[] getProductCoverages(ModelBean container, ModelBean line) {
		ModelBean[] productRiskCoverage = null;
		try {
			ModelBean basicPolicy = container.getBean("BasicPolicy");
			// Get ProductSetup
			String productVersionIdRef = basicPolicy.gets("ProductVersionIdRef");
			ProductVersion productVersion = ProductMaster.getProductMaster("UW").getProductVersion(productVersionIdRef);
			UWProductSetup uwProductSetup = new UWProductSetup(productVersion.getProductSetup());
			// Get ProductLine
			String subTypeCd = basicPolicy.gets("SubTypeCd");
			ModelBean productLine = uwProductSetup.getProductLine(subTypeCd, line.gets("LineCd"));
			ModelBean[] risk = line.findBeansByFieldValue("Risk", "Status", "Active");
			for (int i = 0; i < risk.length; i++) {
				String typeCd = risk[i].gets("TypeCd");
				ModelBean productRiskType = productLine.getBean("ProductRiskTypes").getBean("ProductRiskType", "RiskTypeCd", typeCd);
				if (productRiskType != null) {
					productRiskCoverage = productRiskType.getBean("ProductCoverages").getBeans("ProductCoverage");
				}
			}
		} catch (Exception e) {
			Log.error("Error", e);
		}
		return productRiskCoverage;
	}

	public static Money getClaimantTotalExpense(JDBCData data, ModelBean claim, ModelBean claimant) throws Exception {
		Money exceedAmt = new Money("0.00");
		Money expenseTotal = new Money("0.00");
		ModelBean[] covFeatures = claimant.getBeans("Feature");
		for (ModelBean feature : covFeatures) {
			expenseTotal = sumPaidAmt(claim, feature, "Adjustment");
			exceedAmt = exceedAmt.add(expenseTotal);
		}
		return exceedAmt;
	}

	private static Money getClaimTotalExpense(JDBCData data, ModelBean claim) throws Exception {
		Money exceedAmt = new Money("0.00");
		Money expenseTotal = new Money("0.00");
		ModelBean[] claimants = ClaimRenderer.getOpenClaimants(claim);
		for (ModelBean claimant : claimants) {
			ModelBean[] covFeatures = claimant.getBeans("Feature");
			for (ModelBean feature : covFeatures) {
				expenseTotal = sumPaidAmt(claim, feature, "Adjustment");
				exceedAmt = exceedAmt.add(expenseTotal);
			}
		}
		return exceedAmt;
	}

	/**
	 * Utility routine to determine the name of the Bean depending on the type of
	 * Feature
	 * 
	 * @param feature The Feature/FeatureAllocation Bean
	 * @return The Reserve Bean name of the children
	 * @throws Exception when an error occurs
	 */
	private static String getReserveBeanName(ModelBean feature) throws Exception {
		if (feature.getBeanName().equals("Feature"))
			return "Reserve";
		else
			return "ReserveAllocation";
	}

	/**
	 * Obtain the total paid reserves for a specific reserve type
	 * 
	 * @param claim       The Claim ModelBean
	 * @param feature     The Feature ModelBean
	 * @param reserveType The reserve type to total up
	 * @return The total paid amount
	 * @throws Exception when an error occurs
	 */
	private static Money sumPaidAmt(ModelBean claim, ModelBean feature, String reserveType) throws Exception {
		ModelBean[] reserves = feature.getBeans(getReserveBeanName(feature));
		Money totalPaidAmt = new Money("0.00");

		for (int cnt = 0; cnt < reserves.length; cnt++) {
			String type = Reserve.getReserveType(claim, reserves[cnt].gets("ReserveCd"));
			if (reserveType.equals(type)) {
				if (reserveType.equals("Adjustment")) {
					totalPaidAmt = totalPaidAmt.add(new Money(reserves[cnt].gets("PaidAmt")));
				}
			}
		}
		return totalPaidAmt;
	}

	public static boolean changeQuestionValue(ModelBean application, ModelBean policy) throws Exception {
		boolean isChanged = false;
		if (application != null && policy != null) {
			ModelBean questionReplies = application.getBean("QuestionReplies");
			if (questionReplies != null) {
				ModelBean[] questionReplys = questionReplies.getBeans("QuestionReply");
				if (questionReplys != null && questionReplys.length > 0) {
					for (ModelBean questionReply : questionReplys) {
						ModelBean polQuestionReply = policy.getBeanById(questionReply.gets("id"));
						if (polQuestionReply != null) {
							if (!questionReply.gets("Value").equalsIgnoreCase(polQuestionReply.gets("Value"))) {
								isChanged = true;
							}
						}
					}
				}
			}

		}
		return isChanged;
	}

	/**
	 * Create the task
	 * 
	 * @param data           JDBC Data connection
	 * @param taskTemplateId Template ID
	 * @param linkToBean     Model Bean to attach to
	 * @param todayDt        Date representing the current date or session override
	 * @return task bean
	 */
	public static void createUnderLyingPolicyTask(JDBCData data, String taskTemplateId, ModelBean linkToBean, StringDate todayDt) {
		try {
			ModelBean policy = DwellingProductRenderer.getFullPolicy(linkToBean.gets("SystemId"));
			ModelBean task = Task.createTask(data, taskTemplateId, policy, null, todayDt, null);
			if (task != null) {
				Task.insertTask(data, task);
			}
		} catch (Exception e) {
			return;
		}
	}

	public static void addFormQuestions(ModelBean form) {
		try {
			Log.debug("In addFormQuestions");
			ModelBean replies = form.getBean("QuestionReplies");
			if (replies == null) {
				replies = new ModelBean("QuestionReplies");
				form.addValue(replies);
			}
			for (ModelBean reply : replies.getBeans()) {
				reply.setValue("Value", "");
			}
			setQuestionReply(replies, "Time", new StringDate().toStringDisplay() + StringDate.currentTime());

		} catch (Exception e) {
			return;
		}
	}

	public static void addNoteTemplate(JDBCData data, ModelBean policy, String userId) throws Exception {
		ModelBean basicPolicy = policy.getBean("BasicPolicy");
		String transactionCd = basicPolicy.gets("TransactionCd");

		String templateId = "";
		if (transactionCd.equals("Cancellation")) {
			templateId = "ClaimNote0013";
		} else if (transactionCd.equals("Cancellation Notice")) {
			templateId = "ClaimNote0014";
		}
		String transactionNo = basicPolicy.gets("TransactionNumber");
		boolean createNote = false;
		if (transactionCd.equals("Cancellation")) {
			createNote = true;
		} else if (transactionCd.equals("Cancellation Notice")) {
			ModelBean transactionHistory = basicPolicy.getBean("TransactionHistory", "TransactionNumber", transactionNo);
			if (transactionHistory.getBean("TransactionReason", "ReasonCd", "NonPay") != null
					|| transactionHistory.getBean("TransactionReason", "ReasonCd", "NonPayNSF") != null) {
				createNote = true;
			}
		}
		if (createNote) {
			// Get Sorted Claim List
			ModelBean[] claimList = getClaimsForPolicy(data, policy);
			for (int i = 0; i < claimList.length; i++) {
				if (!claimList[i].gets("StatusCd").equalsIgnoreCase("Deleted")) {
					ModelBean claim = claimList[i];
					if (claim != null && !claim.equals("")) {
						if (DateRenderer.compareTo(claim.getDate("LossDt"), basicPolicy.getDate("CancelDt")) >= 0) {
							ModelBean notes = Note.getNoteTemplate(templateId);
							if (notes != null && !notes.equals("")) {
								ModelBean note = new ModelBean("Note");
								Helper_Beans.resetIds(note, note);
								if (transactionCd.equals("Cancellation")) {
									note.setValue("Description", "There is no coverage in effect on the Date of Loss. " + basicPolicy.gets("PolicyNumber")
											+ " is cancelled effective " + basicPolicy.gets("CancelDt"));
								} else if (transactionCd.equals("Cancellation Notice")) {
									String cancelDt = basicPolicy.gets("CancelDt");
									if (!policy.gets("AccountRef").equals("") && policy.getValueInt("AccountRef") != 0) {
										ModelBean account = AccountRenderer.getAccount(policy.getValueInt("AccountRef"));
										if (account != null) {
											cancelDt = account.gets("CancelDt");
										}
									}
									note.setValue("Description",
											"There is no coverage in effect on the Date of Loss. " + basicPolicy.gets("PolicyNumber")
													+ " is scheduled to cancel effective " + basicPolicy.gets("CancelDt") + " unless payment is received by "
													+ cancelDt + ". Notify the insured of the Coverage issue");
								}
								note.setValue("PriorityCd", notes.gets("PriorityCd"));
								note.setValue("StickyInd", notes.gets("StickyInd"));
								// note.setValue("Name", notes.gets("Name"));
								note.setValue("TemplateId", templateId);
								note.setValue("AddDt", DateTools.getDate());
								note.setValue("AddTm", DateTools.getTime());
								note.setValue("AddUser", userId);
								// Set the Note ModelBean Status to Active
								note.setValue("Status", "Active");
								Note.linkNoteToParent(claim, note);
								claim.addValue(note);
								// Update the Container ModelBean
								data.saveModelBean(claim);
								// Log Message
							}
						}
					}
				}
			}
		}
	}

	public static void updateNoteTemplate(JDBCData data, ModelBean policy, String userId) throws Exception {
		// Get Sorted Claim List
		ModelBean[] claimList = getClaimsForPolicy(data, policy);
		for (int i = 0; i < claimList.length; i++) {
			if (!claimList[i].gets("StatusCd").equalsIgnoreCase("Deleted")) {
				ModelBean claim = claimList[i];
				if (claim != null && !claim.equals("")) {
					ModelBean[] notes = claim.findBeansByFieldValue("Note", "TemplateId", "ClaimNote0013");
					for (ModelBean note : notes) {
						if (note != null && !note.equals("") && note.gets("StickyInd").equals("Yes")) {
							note.setValue("UpdateDt", DateTools.getDate());
							note.setValue("UpdateTm", DateTools.getTime());
							note.setValue("UpdateUser", userId);
							// Set the Note ModelBean Status to Active
							note.setValue("StickyInd", "No");

							// Log Message
						}
					}
					notes = claim.findBeansByFieldValue("Note", "TemplateId", "ClaimNote0014");
					for (ModelBean note : notes) {
						if (note != null && !note.equals("") && note.gets("StickyInd").equals("Yes")) {
							note.setValue("UpdateDt", DateTools.getDate());
							note.setValue("UpdateTm", DateTools.getTime());
							note.setValue("UpdateUser", userId);
							// Set the Note ModelBean Status to Active
							note.setValue("StickyInd", "No");

							// Log Message
						}
					}
					// Update the Container ModelBean
					data.saveModelBean(claim);
				}
			}
		}
	}

	public static boolean isReinsuranceTaskTriggered(ModelBean claim) throws Exception {
		boolean isReinsuranceTaskTriggered = false;
		if (claim != null) {
			ModelBean[] claimReinsurancePlacements = claim.findBeansByFieldValue("ClaimReinsurancePlacement", "StatusCd", "Active");
			if (claimReinsurancePlacements != null && claimReinsurancePlacements.length > 0) {
				for (ModelBean claimReinsurancePlacement : claimReinsurancePlacements) {
					if (claimReinsurancePlacement != null && !claimReinsurancePlacement.gets("PaidAmt").equals("")
							&& StringRenderer.greaterThan(claimReinsurancePlacement.gets("PaidAmt"), "0")) {
						String taskQuickKey = "Claim" + claim.getSystemId();
						boolean taskExists = false;
						ArrayList<String> taskList = new ArrayList<String>();
						taskList.add(0, "ClaimTask3008");
						ModelBean[] tasks = WorkflowRenderer.fetchTasks(taskQuickKey, taskList, VelocityTools.getConnection());
						if (tasks != null && tasks.length > 0) {
							for (ModelBean task : tasks) {
								if (task.gets("TemplateId").equals("ClaimTask3008")) {
									taskExists = true;
								}
							}
						}
						if (!taskExists) {
							isReinsuranceTaskTriggered = true;
						}
					}
				}
			}
		}
		return isReinsuranceTaskTriggered;
	}

	public static ModelBean[] getClaims(ModelBean policy, JDBCData data) throws Exception {
		ModelBean[] filteredClaims = null;
		List<ModelBean> claimList = null;
		if (policy != null) {
			String customerSystemId = policy.gets("CustomerRef");
			String policyNumber = policy.getBean("BasicPolicy").gets("PolicyNumber");
			ModelBean[] claims = Claim.getList(data, customerSystemId, policyNumber, false);
			if (claims != null && claims.length > 0) {
				for (ModelBean claim : claims) {
					String totalImdemnityPaidAmt = "0.00";
					ModelBean[] dtoReserveAllocation = claim.getAllBeans("ReserveAllocation");
					for (int i = 0; i < dtoReserveAllocation.length; i++) {
						if (dtoReserveAllocation[i].gets("ReserveCd").equalsIgnoreCase("Indemnity")) {
							totalImdemnityPaidAmt = StringTools.addMoney(dtoReserveAllocation[i].gets("PaidAmt"), totalImdemnityPaidAmt);
						}
					}
					if (StringRenderer.greaterThan(totalImdemnityPaidAmt, "0")) {
						if (claimList == null) {
							claimList = new ArrayList<ModelBean>();
						}
						claimList.add(claim);
					}
				}
				if (claimList != null && claimList.size() > 0) {
					filteredClaims = (ModelBean[]) claimList.toArray(new ModelBean[claimList.size()]);
				}
			}

		}

		return filteredClaims;
	}

	public static boolean isNonRenewalAction(ModelBean policy, ModelBean[] claims, boolean weatherClaimInd, boolean nonWeatherClaimInd, boolean totalClaimInd)
			throws Exception {
		boolean isNonRenewalAction = false;
		if (claims != null && claims.length > 0) {
			int weatherClaims = 0;
			int nonWeatherClaims = 0;
			int totalClaims = 0;
			StringDate losssDisplayDt = DateRenderer.advanceMonth(policy.getBean("BasicPolicy").getDate("EffectiveDt"), "-48");
			StringDate lossCountableDt = DateRenderer.advanceMonth(policy.getBean("BasicPolicy").getDate("EffectiveDt"), "-36");
			for (ModelBean claim : claims) {
				if (DateRenderer.compareTo(claim.getDate("LossDt"), lossCountableDt) > 0) {
					if (claim.gets("ClaimLossTemplateIdRef").equalsIgnoreCase("Weather")) {
						weatherClaims++;
					} else {
						nonWeatherClaims++;
					}
					totalClaims++;
				} else if (DateRenderer.compareTo(claim.getDate("LossDt"), losssDisplayDt) > 0) {
					totalClaims++;
				}
			}
			if (weatherClaims >= 3 && weatherClaimInd) {
				isNonRenewalAction = true;
			}
			if (nonWeatherClaims >= 2 && nonWeatherClaimInd) {
				isNonRenewalAction = true;
			}
			if (totalClaims >= 3 && totalClaimInd) {
				isNonRenewalAction = true;
			}
		}
		return isNonRenewalAction;
	}

	/**
	 * Get the FileName from the conversion job
	 * 
	 * @param conversionJobRef the id of the conversion job
	 * @return String
	 */
	public static String getConversionFileName(String conversionJobRef) {
		try {
			if (conversionJobRef.equals(""))
				return "";

			JDBCData data = ServiceContext.getServiceContext().getData();

			ModelBean job = new ModelBean("Job");

			data.selectModelBean(job, conversionJobRef);

			ModelBean fileNameQuestion = job.findBeanByFieldValue("QuestionReply", "Name", "FileNameOrDirectory");

			if (fileNameQuestion != null)
				return fileNameQuestion.gets("Value");
			else
				return "";
		} catch (Exception e) {
			e.printStackTrace();
			Log.error(e);
			return "";
		}
	}

	private static void generateRenewalCappingReport(ModelBean application) {
		try {
			String logDir = PrefsDirectory.getLocation() + "/log/";
			String header = "Policy Number,State,Line of Business(Homeowners or Dwelling),Uncapped Expiring Premium,Capped Expiring Premium,Expiring cap,Capper premium,Cap Amount,Uncapped renewal premium,Capped renewal premium,Expiring claim count,Renewal claim count,Renewal effective date\n";
			StringBuffer finalOutput = new StringBuffer(header);
			if (application != null) {
				GregorianCalendar gc = new GregorianCalendar();
				String fileName = "Renewal_Capping_Report_Export_" + gc.get(Calendar.YEAR) + "_" + gc.get(Calendar.MONTH) + "_" + gc.get(Calendar.DATE)
						+ ".csv";
				File csvFile = new File(logDir + fileName);
				if (!csvFile.exists()) {
					csvFile.createNewFile();
					FileTools.appendText(csvFile, finalOutput.toString());
				}

				ModelBean basicPolicy = application.getBean("BasicPolicy");
				FileTools.appendText(csvFile, basicPolicy.gets("PolicyNumber") + "," + basicPolicy.gets("ControllingStateCd") + ","
						+ basicPolicy.gets("Description") + "," + basicPolicy.gets("UncappedExpiringPremium") + "," + basicPolicy.gets("ExpiringPolicyPremium")
						+ "," + basicPolicy.gets("ExpiringPolicyCapAmount") + "," + basicPolicy.gets("CapperPremium") + ","
						+ basicPolicy.gets("RenewalCapAmount") + "," + basicPolicy.gets("UncappedRenewalPremium") + "," + basicPolicy.gets("FinalPremiumAmt")
						+ "," + basicPolicy.gets("ExpiringClaimCount") + "," + getClaimCount(application) + "," + basicPolicy.gets("EffectiveDt") + "\n");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void publishCappingInformation(ModelBean application, String text) {
		try {
			String logDir = PrefsDirectory.getLocation() + "/log/";
			StringBuffer finalOutput = new StringBuffer(text + "\n");
			if (application != null) {
				GregorianCalendar gc = new GregorianCalendar();
				String fileName = "Renewal_Capping_Report_Export_Log_" + gc.get(Calendar.YEAR) + "_" + gc.get(Calendar.MONTH) + "_" + gc.get(Calendar.DATE)
						+ ".log";
				File csvFile = new File(logDir + fileName);
				if (!csvFile.exists()) {
					csvFile.createNewFile();
				}
				FileTools.appendText(csvFile, finalOutput.toString());
				FileTools.appendText(csvFile, application.toPrettyString() + "\n");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static int getClaimCount(ModelBean application) throws Exception {
		int claimCount = 0;
		if (application != null) {
			ModelBean[] countableClaims = application.findBeansByFieldValue("LossHistory", "StatusCd", "Active");
			if (countableClaims != null && countableClaims.length > 0) {
				claimCount = countableClaims.length;
			}
		}
		return claimCount;
	}

	public static boolean hasOpenReserves(ModelBean claimant) throws ModelBeanException {
		boolean hasOpenReserves = false;
		if (claimant != null) {
			ModelBean[] features = claimant.getBeans("Feature");
			if (features != null && features.length > 0) {
				for (ModelBean feature : features) {
					if (!feature.gets("StatusCd").equals("Closed")) {
						hasOpenReserves = true;
					}
				}
			}
		}
		return hasOpenReserves;
	}

	public static String getNewClaimAssignmentTask(JDBCData data, String taskTemplateId, ModelBean linkToBean, String userId, StringDate todayDt) {
		try {
			ModelBean task = Task.createTask(data, taskTemplateId, linkToBean, null, todayDt, null);

			// Override current owner
			task.setValue("CurrentOwnerCd", "Group");
			task.setValue("CurrentOwner", userId);

			// Original Owner should always match current owner on a new task
			task.setValue("OriginalOwnerCd", "Group");
			task.setValue("OriginalOwner", userId);
			Task.insertTask(data, task);
			return BeanTools.getXmlString(task);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getSubrogationAdjusterTask(JDBCData data, ModelBean claim, StringDate todayDt) {

		String subrogationAdjusterId = "";
		ModelBean subroAdjusterTask = null;
		ArrayList<String> taskList = new ArrayList<String>();
		boolean taskExists = false;
		try {
			String taskQuickKey = "Claim" + claim.getSystemId();
			ModelBean provider = null;
			String idRef = getSubrogationAdjuster(data);

			provider = new ModelBean("Provider");
			data.selectModelBean(provider, idRef);

			subrogationAdjusterId = provider.gets("ProviderNumber");
			taskList.add(0, "ClaimTask3018");
			for (ModelBean task : WorkflowRenderer.fetchTasks(taskQuickKey, taskList, data)) {
				if (task.gets("TemplateId").equals("ClaimTask3018")) {
					taskExists = true;
				}
			}
			// Create
			if (!taskExists) {
				subroAdjusterTask = WorkflowRenderer.createTask(data, "ClaimTask3018", claim, todayDt);
				subroAdjusterTask.setValue("CurrentOwner", subrogationAdjusterId);
				subroAdjusterTask.setValue("CurrentOwnerCd", "User");
				subroAdjusterTask.setValue("OriginalOwner", subrogationAdjusterId);
				subroAdjusterTask.setValue("OriginalOwnerCd", "User");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return BeanTools.getXmlString(subroAdjusterTask);

	}

	public static String getSubrogationAdjusterSupervisorTask(JDBCData data, ModelBean claim, StringDate todayDt) {

		ModelBean subroAdjSupervisorTask = null;
		ArrayList<String> taskList = new ArrayList<String>();
		boolean taskExists = false;

		try {
			String taskQuickKey = "Claim" + claim.getSystemId();
			ModelBean provider = null;
			String idRef = getSubrogationAdjuster(data);

			provider = new ModelBean("Provider");
			data.selectModelBean(provider, idRef);
			String subrogationAdjusterId = provider.gets("ProviderNumber");
			ModelBean userInfo = BeanTools.selectModelBeanFromLookup(data, "UserInfo", "UserTaskGroupCd", subrogationAdjusterId);
			String supervisorId = userInfo.gets("Supervisor");
			taskList.add(0, "ClaimTask3019");
			for (ModelBean task : WorkflowRenderer.fetchTasks(taskQuickKey, taskList, data)) {
				if (task.gets("TemplateId").equals("ClaimTask3019")) {
					taskExists = true;
				}
			}
			// Create
			if (!taskExists) {
				subroAdjSupervisorTask = WorkflowRenderer.createTask(data, "ClaimTask3019", claim, todayDt);
				subroAdjSupervisorTask.setValue("CurrentOwner", supervisorId);
				subroAdjSupervisorTask.setValue("CurrentOwnerCd", "User");
				subroAdjSupervisorTask.setValue("OriginalOwner", supervisorId);
				subroAdjSupervisorTask.setValue("OriginalOwnerCd", "User");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return BeanTools.getXmlString(subroAdjSupervisorTask);
	}

	private static String getSubrogationAdjuster(JDBCData data) {

		StringBuilder sql = new StringBuilder();

		sql = new StringBuilder();

		sql.append("select distinct pl1.systemid from providerlookup pl1, providerlookup pl2 "
				+ "where pl1.lookupkey='ProviderType' and pl1.lookupvalue in ('SUBROGATION ADJUSTER') "
				+ "and pl2.lookupkey='StatusCd' and pl2.lookupvalue='ACTIVE' and pl1.systemid=pl2.systemid");

		String idRef = "";

		try {
			ResultSet rSet = data.doSQL(sql.toString());
			while (rSet.next()) {
				idRef = rSet.getString("SystemId");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return idRef;
	}

	/**
	 * This method is used to update the task status based on the claim status
	 * taskQuickKey is nothing but "Claim + claim systemID"
	 * 
	 * @param data
	 * @param taskQuickKey
	 * @param claim
	 * @param todayDt
	 * @throws Exception
	 */
	public static void updateClaimTaskStatus(JDBCData data, String taskQuickKey, ModelBean claim, StringDate todayDt) throws Exception {
		ArrayList<String> taskList;
		boolean claimTask2010Flag = false;
		try {
			Log.debug("In updateClaimTaskStatus...");
			if (Claim.isOpen(claim.gets("StatusCd"))) {
				taskList = new ArrayList<String>();
				taskList.add(0, "ClaimTask2010");
				for (ModelBean task : WorkflowRenderer.fetchTasks(taskQuickKey, taskList, data)) {
					String templateId = task.gets("TemplateId");
					if (!task.gets("Status").equals("Open")) {
						if (!claimTask2010Flag && templateId.equals("ClaimTask2010")) {
							ModelBean taskVar = WorkflowRenderer.createTask(data, templateId, claim, todayDt);
							Task.insertTask(data, taskVar);
							claimTask2010Flag = true;
						}
					}
				}
			} else if (Claim.isClosed(claim.gets("StatusCd"))) {
				taskList = new ArrayList<String>();
				taskList.add(0, "ClaimTask2009");
				taskList.add(1, "ClaimTask2010");
				taskList.add(2, "ClaimTask2006");
				taskList.add(3, "ClaimTask2007");
				taskList.add(4, "ClaimTask2015");
				taskList.add(5, "ClaimTask3003");
				taskList.add(6, "ClaimTask3004");
				taskList.add(7, "ClaimTask3012");
				taskList.add(8, "ClaimTask3015");
				for (ModelBean task : WorkflowRenderer.fetchTasks(taskQuickKey, taskList, data)) {
					String templateId = task.gets("TemplateId");
					if (templateId.equals("ClaimTask2009") || templateId.equals("ClaimTask2010") || templateId.equals("ClaimTask2006")
							|| templateId.equals("ClaimTask2007") || templateId.equals("ClaimTask2015") || templateId.equals("ClaimTask3003")
							|| templateId.equals("ClaimTask3004") || templateId.equals("ClaimTask3012") || templateId.equals("ClaimTask3015")) {

						WorkflowRenderer.closeTask(data, task);
					}
				}
			}
		} catch (Exception e) {
			Log.error(e);
			throw e;
		}
	}

	/**
	 * This method is used to calculate the total paid amount and Excludes Coverage
	 * C
	 * 
	 * @param data
	 * @param claim
	 * @return
	 * @throws Exception
	 */
	public static Money getTotalPaidAmtExclCovC(JDBCData data, ModelBean claimantTransaction) throws Exception {
		Money totalPaidAmt = new Money("0.00");
		ModelBean[] features = claimantTransaction.getBeans("FeatureAllocation");

		for (ModelBean curFeature : features) {
			if (!curFeature.gets("FeatureCd").equals("CovC")) {
				totalPaidAmt = totalPaidAmt.add(new Money(curFeature.gets("PaidAmt")));
			}

		}

		return totalPaidAmt;
	}

	public static boolean getThirdPartyClaimantDetails(JDBCData data, ModelBean claim, String maximumAdjustmentReserveAmt, boolean newClaim, ModelBean oldClaim,
			String maximumIndemnityReserveAmt) {
		boolean isThirdPartyClaim = false;

		try {
			ModelBean[] claimantsThird = claim.findBeansByFieldValue("Claimant", "ClaimantTypeCd", "Third Party");
			if (null != claimantsThird) {
				isThirdPartyClaim = true;
				Money reserveAdjustmentAmt = new Money();
				Money oldReserveAdjustmentAmt = new Money();
				Money reserveIndemnityAmt = new Money();
				Money oldReserveIndemnityAmt = new Money();
				reserveIndemnityAmt = getClaimTotalIndemnity(data, claim);
				reserveAdjustmentAmt = getClaimTotalExpense(data, claim);
				// Reserve Adjustment
				if (StringRenderer.greaterThan(reserveAdjustmentAmt.toString(), maximumAdjustmentReserveAmt)) {
					String showAmt = StringRenderer.formatValue(maximumAdjustmentReserveAmt, 2, "$0.00", true, true);
					String showMsg = "Maximum Reserve Adjustment Amount For the claim exceeded limit of " + showAmt;
					if (newClaim) {
						ValidationRenderer.addValidationError(claim, "Approval", "APPROVAL", showMsg);
					} else {
						oldReserveAdjustmentAmt = getClaimTotalExpense(data, oldClaim);
						if (StringRenderer.greaterThan(reserveAdjustmentAmt.toString(), oldReserveAdjustmentAmt.toString())) {
							ValidationRenderer.addValidationError(claim, "Approval", "APPROVAL", showMsg);
						}
					}
				}
				// Reserve Indemnity
				if (StringRenderer.greaterThan(reserveIndemnityAmt.toString(), maximumIndemnityReserveAmt)) {
					String showAmt = StringRenderer.formatValue(maximumIndemnityReserveAmt, 2, "$0.00", true, true);
					String showMsg = "Maximum Reserve Indemnity Amount For the claim exceeded limit of " + showAmt;
					if (newClaim) {
						ValidationRenderer.addValidationError(claim, "Approval", "APPROVAL", showMsg);
					} else {
						oldReserveIndemnityAmt = getClaimTotalIndemnity(data, oldClaim);
						if (StringRenderer.greaterThan(reserveIndemnityAmt.toString(), oldReserveIndemnityAmt.toString())) {
							ValidationRenderer.addValidationError(claim, "Approval", "APPROVAL", showMsg);
						}
					}
				}
			}
		} catch (ModelBeanException e) {
			e.printStackTrace();
			Log.error(e);
		} catch (Exception e) {
			e.printStackTrace();
			Log.error(e);
		}

		return isThirdPartyClaim;
	}

	/**
	 * This creates a temporary application and applys the cse rules to see if there
	 * are any issues on renewal
	 * 
	 * @param policy
	 * @param data
	 * @throws Exception
	 */
	public void createTempApp(ModelBean policy, JDBCData data) throws Exception {

		ModelBean result = createApplication(policy, data);
		if (result.getBean("application") != null) {
			ModelBean application = result.getBean("application");
			application.getBean("BasicPolicy").setValue("RenewalClaimCount", getClaimCount(application));
			application.getBean("BasicPolicy").setValue("CappedRenewalPremium", application.getBean("BasicPolicy").gets("FinalPremiumAmt"));
			String uncappedRenewalPremium = application.getBean("BasicPolicy").gets("RenewalCapAmount");
			if (!application.getBean("BasicPolicy").gets("RenewalCapAmount").equals("")
					&& !application.getBean("BasicPolicy").gets("FinalPremiumAmt").equals("")) {
				uncappedRenewalPremium = new BigDecimal(application.getBean("BasicPolicy").gets("FinalPremiumAmt"))
						.subtract(new BigDecimal(application.getBean("BasicPolicy").gets("RenewalCapAmount"))).toString();
			}
			application.getBean("BasicPolicy").setValue("UncappedRenewalPremium", uncappedRenewalPremium);
			generateRenewalCappingReport(application);
			String displayInd = InnovationUtils.getEnvironmentVariable("CAPReportEmailAddress", "CAPCalculationDisplay", "");
			if (displayInd.equals("Yes")) {
				publishCappingInformation(application,
						"Renewal Policy::::::::::::::::::::::::::::::::" + application.getBean("BasicPolicy").gets("PolicyNumber"));
			}
			try {
				PM.deleteApplication(data, application, "temp", DateTools.getStringDate());
				/** Remove the application from the database */
				data.deleteModelBean(application);
				result.deleteBeanById(application.getId());
			} catch (Exception e) {
				throw e;
			} finally {

			}

		}
	}

	/**
	 * This method creates a temporary application
	 * 
	 * @param policy
	 * @param data
	 * @return
	 * @throws Exception
	 */
	private ModelBean createApplication(ModelBean policy, JDBCData data) throws Exception {

		Log.debug("Processing createApplication...");
		SessionInfo session = new SessionInfo();
		/* The userId needs to be set as admin in order to create an Application */
		session.setUserID("admin");
		ModelBean service = null;
		service = DataConversionHelper.buildServiceRequest("UWPolicyTransactionAddRq", session);

		ModelBean transactionHistory = new ModelBean("TransactionHistory");
		transactionHistory.setValue("TransactionCd", "Renewal");
		transactionHistory.setValue("TransactionShortDescription", "Temporary Renewal Application");
		service.addValue(transactionHistory);

		ModelBean cmm = service.getBean("RequestParams").getBean("CMMParams");
		cmm.setValue("Container", "Policy");
		cmm.setValue("SystemId", policy.gets("SystemId"));
		ModelBean xfdfBean = service.getBean("RequestParams").getBean("XFDF");
		ModelBean responseParam = new ModelBean("Param");
		responseParam.setValue("Name", "PublishCapDetailsInd");
		responseParam.setValue("Value", "Yes");
		xfdfBean.addValue(responseParam);
		ModelBean result = null;
		try {
			result = DataConversionHelper.callService(service);

		} catch (Exception e) {
			throw e;
		}
		return result;

	}

//	the below methods needs to be revisited and agreed upon to be in a common renderer for BOP and dwelling to check single and multi risk policies.

	/**
	 * Determines if a tile should show
	 * 
	 * @return Yes/No
	 * @throws Exception
	 */
	public static String showSingleRiskTab(ModelBean container, String lineCd) throws Exception {
		return isSingleRisk(container, lineCd) ? "Yes" : "No";
	}

	public static String showMultiRiskTab(ModelBean container, String lineCd) throws Exception {
		return isSingleRisk(container, lineCd) ? "No" : "Yes";
	}

	/**
	 * Determines if a single line contains more than one risk or location
	 * 
	 * @param container The application ModelBean
	 * @param lineCd    The line code for the current product line
	 * @return true if line contains only one risk and location
	 * @throws Exception
	 */
	public static boolean isSingleRisk(ModelBean container, String lineCd) throws Exception {

		ModelBean[] locations = container.findBeansByFieldValue("Location", "Status", "Active");
		if (locations.length > 1)
			return false;

		ModelBean line = container.findBeanByFieldValue("Line", "LineCd", lineCd);
		if (line != null) {
			// find all the active risks. if there is more than one then this is
			// not single risk
			ModelBean[] risks = line.findBeansByFieldValue("Risk", "Status", "Active");
			if (risks.length > 1)
				return false;
		}

		return true;
	}

	public static boolean firstLocationFilledInd(ModelBean container) throws Exception {

		ModelBean location = container.findBeanByFieldValue("Location", "Status", "Active");
		if (location != null) {
			ModelBean addr = location.findBeanByFieldValue("Addr", "AddrTypeCd", "LocationLookupAddr");
			if (addr != null) {
				return !AddressTools.getAddressHash(addr).equals("");
			}
		}

		return false;
	}

	/**
	 * This Method Returns the Path of the LOGO
	 * 
	 * @param imageName
	 * @return
	 * @throws MDAException
	 */
	public String getAbsolutePathOfBmp(String imageName) throws MDAException {
		try {
			String repoPath = PrefsDirectory.getLocation();
			repoPath += "image/" + imageName;

			return repoPath;
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error Retrieving LOGO Path", e);
			return "";
		}
	}

	private static void addTaskLink(ModelBean task, String idRef, String modelName) throws ModelBeanException, Exception {
		ModelBean riskLink = new ModelBean("TaskLink");
		riskLink.setValue("IdRef", idRef);
		riskLink.setValue("ModelName", modelName);
		task.addValue(riskLink);
	}

	public static String RenderDynamicString(ModelBean[] beans, String message) throws Exception {
		return DynamicString.render(beans, message);
	}

	static class ReviewTask {
		public String addItem;
		public String[] itemsNotAdded;
		public String path;

		public ReviewTask(String path, String addItem, String[] itemsNotAdded) {
			this.addItem = addItem;
			this.itemsNotAdded = itemsNotAdded;
			this.path = path;
		}
		
		public String[] getItems() {
			return ArrayUtils.add(itemsNotAdded, addItem);
		}
	}

	static class TaskRetriever {
		ModelBean policy;
		JDBCData connection;
		XmlDoc tasksXml;
		private Element root;

		public TaskRetriever(ModelBean policy, JDBCData connection) throws TaskException, ModelBeanException, XmlDocException {
			this.policy = policy;
			this.connection = connection;
			ModelBean[] allTasks = Task.fetchTasks(policy.getBeanName(), policy.getSystemId(), connection);
			tasksXml = new XmlDoc("<Tasks />");
			this.root = tasksXml.getRootElement();

			for (ModelBean task : allTasks) {
				this.addTask(task);
			}
		}
		
		public void addTask(ModelBean task) throws ModelBeanException {
			this.root.addContent((Content) (task.marshal(true).getRootElement().clone()));
		}

		public String getExistingOpenTaskId(String riskId, String templateId) throws JDOMException {
			String path = String.format("//Task[@Status='Open' and @TemplateId='%s']/TaskLink[@IdRef='%s']/../@id", templateId, riskId);
			Attribute riskIdAttr = (Attribute) XPath.selectSingleNode(this.tasksXml, path);
			return (riskIdAttr == null) ? "" : riskIdAttr.getValue();
		}
	}

	public static ReviewTaskCreator getReviewTaskCreator(JDBCData data, ModelBean policy, ModelBean prevPolicy, StringDate todayDt, String userId)
			throws Exception {
		return new ReviewTaskCreator(data, policy, prevPolicy, todayDt, userId);
	}

	public static ReviewTaskCreator getReviewTaskCreator(JDBCData data, ModelBean policy, StringDate todayDt, String userId)
			throws Exception {
		return new ReviewTaskCreator(data, policy, null, todayDt, userId);
	}

	public static class ReviewTaskCreator {
		private TaskRetriever taskGetter;
		private ModelBean policy;
		private JDBCData data;
		private ModelBean prevPolicy;
		private StringDate todayDt;
		private String userId;

		public ReviewTaskCreator(JDBCData data, ModelBean policy, ModelBean prevPolicy, StringDate todayDt, String userId)
				throws Exception {
			this.data = data;
			this.policy = policy;
			this.prevPolicy = prevPolicy;
			this.todayDt = todayDt;
			this.userId = userId;
			this.taskGetter = new TaskRetriever(policy, data);
		}

		private static final String[] FIELDS_TO_UPDATE = { "Name", "Description", "CurrentOwner", "OriginalOwner" };

		public void createReviewTasks(String xpath, String attachmentName, String notReceivedTasks ) throws Exception {
			try {
				Log.debug("Creating review workflow tasks for attachment " + attachmentName);

				ReviewTask task = new ReviewTask(xpath, attachmentName, notReceivedTasks.split(",") );

				ModelBean[] risks = policy.findBeansByFieldValue("Risk", "Status", "Active");
				XPath valueFinder = XPath.newInstance(task.path);
				boolean isMultiRisk = risks.length > 1;

				for (ModelBean risk : risks) {
					Attribute valueAttribute = (Attribute) valueFinder.selectSingleNode(risk.marshal(true));
					String value = (valueAttribute == null) ? "" : valueAttribute.getValue();

					boolean hasCurrentValue = !value.isEmpty();
					boolean hasPreviousValue = false;
					boolean hasDifferingValues = false;
					String prevValue = "";

					if (prevPolicy != null) {
						ModelBean prevRisk = prevPolicy.getBeanById(risk.getId());
						
						if (prevRisk != null && prevRisk.gets("Status").equals("Active")) {
							Attribute prevValueAttribute = (Attribute) valueFinder.selectSingleNode(prevRisk.marshal(true));
							prevValue = (prevValueAttribute == null) ? "" : prevValueAttribute.getValue();
							hasPreviousValue = !prevValue.isEmpty();
						}
						
						hasDifferingValues = (hasCurrentValue && hasPreviousValue && !value.equals(prevValue));
					}

					// no current value, no previous value, nothing to do
					if (!hasCurrentValue && !hasPreviousValue) {
						Log.debug("No current or previous value.");
						continue;
					} else if (hasCurrentValue && !hasPreviousValue) {
						Log.debug("Current value but no previous value.");
						// no previous value, new value, create task
						createWorkflowReviewTasks(risk, task, isMultiRisk);
					} else if (!hasCurrentValue && hasPreviousValue) {
						Log.debug("Had previous value, now does not have a value.");
						closeWorkflowReviewTasks(data, risk.getId(), task);
						// had value, now does not, close this task from risk
					} else if (hasCurrentValue && hasPreviousValue && hasDifferingValues) {
						Log.debug("Has value and previous value and they are different.");
						// current and previous values are not equal
						createWorkflowReviewTasks(risk, task, isMultiRisk);
					}
				}
			} catch (Exception e) {
				Log.debug("Exception in CreateReviewTasks");
				Log.debug("attachmentName = " + attachmentName);
				Log.debug("xpath = " + xpath);
				Log.error(e);
				throw e;
			}
		}

		public void closeWorkflowReviewTasks(JDBCData data, String riskId, ReviewTask task) throws Exception {
			try {
				Log.debug("closeWorkflowReviewTasks for riskId = " + riskId);

				String[] taskTemplates = task.getItems();

				for (String taskTemplate : taskTemplates) {
					Log.debug("closeWorkflowReviewTasks: for templateId = " + taskTemplate);
					String existingTaskId = taskGetter.getExistingOpenTaskId(riskId, taskTemplate);
					if (!existingTaskId.isEmpty()) {
						Log.debug("closeWorkflowReviewTasks: found open templateId = " + taskTemplate);
						Task.updateTaskStatus(data, existingTaskId, "Completed");
					} else {
						Log.debug("closeWorkflowReviewTasks: no open templateId = " + taskTemplate);
					}
				}
			} catch (Exception e) {
				Log.error(e);
				throw e;
			}
		}

		public void createWorkflowReviewTasks(ModelBean risk, ReviewTask task, boolean isMultiRisk ) throws Exception {
			try {
				Log.debug("In createWorkflowReviewTasks...");

				String bldgNumber = risk.getBean("Building").gets("BldgNumber");
				String[] taskTemplates = task.getItems();

				Map<String, Object> extra = new HashMap<String, Object>();
				extra.put("userId", userId);
				extra.put("bldgNumber", bldgNumber );
				extra.put("forBldgNumber", isMultiRisk ? "for Building " + bldgNumber : "");

				String riskId = risk.gets("id");

				for (String taskTemplate : taskTemplates) {
					String existingTaskId = taskGetter.getExistingOpenTaskId(riskId, taskTemplate);
					if (existingTaskId.isEmpty()) {
						createTask(taskTemplate, risk, extra);
					}
				}
			} catch (Exception e) {
				Log.error(e);
				throw e;
			}
		}

		public void createTask(String taskTemplateId, ModelBean risk, Map<String, Object> extra) throws Exception {
			try {
				String riskId = risk.gets("id");
				Log.debug("Creating task " + taskTemplateId + " for risk " + riskId);
				ModelBean task = Task.createTask(this.data, taskTemplateId, this.policy, null, this.todayDt, null);

				for (String fieldName : ReviewTaskCreator.FIELDS_TO_UPDATE) {
					String textValue = task.gets(fieldName);

					for( String key : extra.keySet() ) {
						textValue = textValue.replace("$" + key, (String) extra.get(key) );
					}
					
					task.setValue(fieldName, textValue);
				}

				addTaskLink(task, riskId, "Risk");

				Helper_Beans.setIds(task, task);
				taskGetter.addTask(task);
				data.saveModelBean(task);
			} catch (Exception e) {
				Log.error(e);
				throw e;
			}
		}
	}

	/**
	 * Method to validate the change in address.
	 * 
	 * @param addr
	 * @return isAddressChanged
	 */
	public String validateChangeinAddress(ModelBean building) {
		String isAddressChanged = "No";
		try {
			ModelBean riskLookupAddr = building.findBeanByFieldValue("Addr", "AddrTypeCd", "RiskLookupAddr");
			String cluePropertyAddr = building.gets("CLUEPropertyPrevAddr");
			if (cluePropertyAddr.equals("")) {
				building.setValue("CLUEPropertyPrevAddr", riskLookupAddr.gets("VerificationHash"));
				isAddressChanged = "Yes";
			} else {
				if (building.gets("CLUEPropertyPrevAddr").equalsIgnoreCase(riskLookupAddr.gets("VerificationHash"))) {
					isAddressChanged = "No";
				} else {
					building.setValue("CLUEPropertyPrevAddr", riskLookupAddr.gets("VerificationHash"));
					isAddressChanged = "Yes";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isAddressChanged;
	}

	public void updateLosshistoryDetails(ModelBean application) {
		try {
			ModelBean basicPolicy = application.getBean("BasicPolicy");
			String applicationNumber = basicPolicy.gets("ApplicationNumber");
			if (applicationNumber.startsWith("Q")) {
				ModelBean[] losslistoryBeans = application.getAllBeans("LossHistory");
				for (ModelBean losshistory : losslistoryBeans) {
					losshistory.setValue("StatusCd", "Deleted");
				}
			}
		} catch (ModelBeanException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
