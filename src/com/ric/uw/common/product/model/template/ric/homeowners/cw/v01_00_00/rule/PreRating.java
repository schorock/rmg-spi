/*
 * 
 * PreRating.java
 */
package com.ric.uw.common.product.model.template.ric.homeowners.cw.v01_00_00.rule;

import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.data.JDBCData;

/**
 * Businessowners Policy PreRating example
 * 
 * Calculates number of risks that apply to certain coverages depending on rules specific to the coverage
 * 
 * @author Scott Ferguson
 */

public class PreRating implements RuleProcessor {


	@Override
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user, JDBCData data) throws RuleException {
		ModelBean errors = null;
		try{
			Log.debug("Processing PreRating...");

			// Build the Errors ModelBean
			errors = new ModelBean("Errors");	

			setRatingIndicator(bean, additionalBeans);
		}
		catch(Exception e){	
			throw new RuleException(e);
		}
		return errors;		
	}

	private void setRatingIndicator(ModelBean application, ModelBean[] additionalBeans) throws Exception {
		if (isRatingRequired(additionalBeans)) {
			application.setValue("ReadyToRateInd", "Yes");
		} else {
			application.setValue("ReadyToRateInd", "No");
		}	
	}

	/**Decides whether the Rating to the called
	 * Check for Netrate Param in Additional Param
	 * @return boolean
	 */
	private boolean isRatingRequired(ModelBean[] additionalBeans) throws Exception {
		try{
			ModelBean responseParams = additionalBeans[0].getBean("ResponseParams");
			ModelBean additionalParams = responseParams.getBean("AdditionalParams");
			ModelBean xfdf = responseParams.getBean("XFDF");

			boolean useNetRate = getUseNetRate(additionalParams, xfdf);
			String requestName = getRequestName(xfdf);
			
			// Do not rate certain transactions
			if (useNetRate || StringRenderer.in(requestName, "UWApplicationUpdateCloseout") || 
					StringRenderer.in(requestName, "UWRiskUpdateCloseout")) {
				return true;
			} else {
				return false;
			}
		}
		catch( Exception e ) {
			Log.error(e.getMessage(), e);
			e.printStackTrace();
			return false;
		}
	}

	private String getRequestName(ModelBean xfdf) throws ModelBeanException {
		String closeOutScreen = "";
		ModelBean rq = xfdf.findBeanByFieldValue("Param", "Name", "rq");
		if (rq != null) {
			closeOutScreen = rq.getValue("Value").toString();
		}
		return closeOutScreen;
	}

	private boolean getUseNetRate(ModelBean additionalParams, ModelBean xfdf) throws ModelBeanException {
		boolean useNetRate = false;
		ModelBean netrateParam = additionalParams.getBean("Param", "Name", "UseNetRate");
		if(netrateParam == null || netrateParam.equals("")){
			netrateParam = xfdf.getBean("Param", "Name", "UseNetRate");
		}

		// Check Netrate flag in Additional Param
		if(netrateParam != null) {
			String netrateParamVal = netrateParam.getValue("Value").toString();
			if(netrateParamVal!= "") {
				useNetRate = StringTools.isTrue(netrateParam.getValue("Value").toString());        	   
			}
		} 

		return useNetRate;
	}
}