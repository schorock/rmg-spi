package com.ric.uw.common.product.model.template.ric.dwellingproperty.handler;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;

import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.uw.common.risk.Location;

/**
 * Location need to exist but since Dryden doesn't want to see them (at least
 * for LPP) then we'll create them in the background when creating risks. This
 * process creates the Location and passes on its id so the RiskNew process
 * doesn't break.
 * 
 * @author justin.rumpf
 * 
 */
public class DwellingLocationNew extends InnovationIBIZHandler {

	public DwellingLocationNew() throws Exception {
		super();
	}

	/**
	 * Processes a response creating a new location passing on its id as a risk location id reference id.
	 */
	public ModelBean process() throws IBIZException, ServiceHandlerException {
		try {
			// Log a Greeting
			
			Log.debug("Processing " + this.getClass().getSimpleName());
			
			ModelBean rs = this.getHandlerData().getResponse();
			ModelBean cmm = rs.getBean("ResponseParams").getBean("CMMParams");
			ModelBean container = rs.getBean(cmm.gets("Container"));
			ModelBean params = rs.getBean("ResponseParams").getBean("AdditionalParams");
			// new location should not be created for the exposure risk type for RFP, as the exposure should be linked to a specific risk
			ModelBean riskType= params.findBeanByFieldValue("Param", "Name", "RiskTypeCd");
			if ( riskType != null && riskType.gets("Value").equals("Exposure")) {
				return rs;
			}
			// Create a new location and add it to the quote/application
			ModelBean location = new Location().create();
			location.setValue("Status", "New");
			container.addValue(location);

			/*
			 * The risk creation process expects a location reference to match
			 * up to an existing location. After creating a new location we need
			 * to set the its id as a the risk location reference id so that the
			 * risk new process doesn't bomb.
			 */			
			ModelBean rq = this.getHandlerData().getRequest();
			ModelBean additionalParams = rq.getBean("RequestParams").getBean("AdditionalParams");

			ModelBean riskLocationRefParam = additionalParams.findBeanByFieldValue("Param", "Name", "RiskLocationRef");
			if (riskLocationRefParam == null) {
				Log.debug("Adding new RiskLocationRef with " + location.getId()  );
				ModelBean param = new ModelBean("Param");
				param.setValue("Name", "RiskLocationRef");
				param.setValue("Value", location.getId());
				
				additionalParams.addValue(param);				
			}
			else {
				Log.debug("Replacing RiskLocationRef with " + location.getId() );
				riskLocationRefParam.setValue("Value", location.getId());
			}

			// Return the Response ModelBean
			return rs;
		} catch (Exception e) {
			throw new ServiceHandlerException(e);
		}
	}

}