/*
 * 
 * PreRating.java
 */
package com.ric.uw.common.product.model.template.ric.dwellingproperty.cw.v01_00_00.rule;

import com.iscs.common.business.rule.RuleException;
import com.iscs.common.tech.log.Log;
import com.ric.uw.common.shared.AbstractPreRating;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;

/**
 * Businessowners Policy PreRating example
 * 
 * Calculates number of risks that apply to certain coverages depending on rules specific to the coverage
 * 
 * @author Scott Ferguson
 */

public class PreRating extends AbstractPreRating {


	@Override
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user, JDBCData data) throws RuleException {
		ModelBean errors = null;
		try{
			Log.debug("Processing PreRating...");

			// Build the Errors ModelBean
			errors = new ModelBean("Errors");	

			setRatingIndicator(bean, additionalBeans);
		}
		catch(Exception e){	
			throw new RuleException(e);
		}
		return errors;		
	}

}