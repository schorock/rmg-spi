package com.ric.uw.common.product.model.template.ric.dwellingproperty.va.v01_00_00.rule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.iscs.common.admin.user.render.UserRenderer;
import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.render.DateRenderer;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.render.VelocityTools;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.bean.BeanDiff;
import com.iscs.common.utility.bean.render.BeanRenderer;
import com.iscs.common.utility.validation.render.ValidationRenderer;
import com.iscs.insurance.product.render.ProductRenderer;
import com.iscs.uw.common.transaction.render.TransactionRenderer;
import com.ric.insurance.product.render.RICProductRenderer;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;


public class ApprovalRules implements RuleProcessor{

	private String policyTabParam = "CodeRefOptionsKey=application-product&TabOption=Policy&ShowRequiredFieldsInd=Yes";
	private String dwellingTabParam = "CodeRefOptionsKey=application-product&TabOption=Risks&ShowRequiredFieldsInd=Yes";
	private String genericTabLink = "javascript:cmmXFormSafe('UWApplicationSync','%s','%s','%s','%s')";
	private ModelBean application;
	private ModelBean dwellingQuestionReplies;
	private ModelBean basicPolicy;
	private ModelBean insured;
	private ModelBean line;
	private ModelBean[] risks;
	private ModelBean[] losses;
	private ModelBean oldPolicy;
	private BeanDiff applicationDiff;
	private StringDate todayDt;
	private ModelBean transactionInfo;
	private ModelBean response;
	private ModelBean errors;
	private ModelBean user;
	private String transactionCd;
	private String todayYear;
	private String businessSourceCd;
	private Map<String, List<NonApprovedRisk>> riskApprovals;

	@Override
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user,
			JDBCData data) throws RuleException {
		try {
			this.user = user;
			response = additionalBeans[0];
			errors = response.getBean("ResponseParams").getBean("Errors");
			application = bean;
			basicPolicy = application.getBean("BasicPolicy");
			businessSourceCd = basicPolicy.gets("BusinessSourceCd");
			insured = application.getBean("Insured");
			line = application.getBean("Line");
			risks = line.findBeansByFieldValue("Risk", "Status", "Active");
			losses = application.findBeansByFieldValue("LossHistory", "StatusCd", "Active");
			transactionInfo = application.getBean("TransactionInfo");
			dwellingQuestionReplies = application.getBean("QuestionReplies");
			transactionCd = transactionInfo.gets("TransactionCd");
			todayDt = new StringDate(DateTools.getDate());
			todayYear = todayDt.toString().substring(0,4);
			riskApprovals = new LinkedHashMap<>();
			
			if( !checkLiabilityCoverageTypeForFirstRisk() )
				return errors;

			if( !businessSourceCd.equals("Conversion") ) { 
				if( !transactionCd.equals("New Business") && !transactionCd.equals("Rewrite-New") ) {
					oldPolicy = TransactionRenderer.getApplicationStart(data, application);
					applicationDiff = BeanRenderer.getBeanDiff(oldPolicy, application, true, "ChangeInfo,Output,AuditData");
				}
	
				if( transactionCd.equals("New Business") || transactionCd.equals("Rewrite-New") ){
					//RG-539
					triggerNewBusinessBackdatedApproval();
					triggerInsuredEntityTypeEstateApproval();
					triggerCLUELossesWithinPast3YrsApproval();
					triggerMoreThan1PaidLossApproval();
					triggerPriorPaidLossGreaterThan10kApproval();
					triggerNoHitNoInsuranceScoreApproval();
					triggerLowInsuranceScoreApproval();
					trigger6MonthTermApproval();
					triggerNewBusinessRiskApprovals();
					triggerUWApproval();
				}
	
				//RGUAT-582
				if( transactionCd.equals("Endorsement") ){
					triggerEndorseGreaterThan30DaysFromTodayApproval();
					triggerPolicyFormChangeApproval();
					triggerEntityTypeCdApproval();
					triggerDwProgramTypeCdChangeApproval();
					triggerEndorsementRiskApprovals();
					triggerUWApproval();
				}
	
				//RMGUAT-476
				if( transactionCd.equals("Renewal") || transactionCd.equals("Rewrite-Renewal") ){
					triggerInsuredEntityTypeEstateApproval();
					triggerCLUELossesWithinPast3YrsApproval();
					trigger6MonthTermApproval();
					triggerForceManualRenewalApproval();
					triggerNewWaterLossApproval();
					triggerLossesWithinPast3Yrs();
					triggerNewLiabilityLossApproval();
					triggerRenewalRiskApprovals();
				}
	
				if( transactionCd.equals("Cancellation") ){
					triggerCancelGreaterThan30DaysFromTodayApproval();
				}
			}
			return errors;
		} catch ( Exception e ) {
			throw new RuleException( e );
		}
	}

	private void triggerNewBusinessRiskApprovals() throws Exception {
		for( ModelBean risk : risks ) {
			triggerPrimaryHeatingRule(risk);
			triggerSecondaryHeatingRule(risk);
			triggerSupplyPipesRule(risk);
			triggerWiringSheathRule(risk);
			triggerRoofReplacedRule(risk);
		}

		if( ValidationRenderer.countValidationErrors(application, "Validation", "ERROR") == 0 ) {
			for (ModelBean risk : risks) {
				triggerNoOlderHomeUpdatesApproval(risk);
				triggerFL255DueToAgeApproval(risk);
				triggerStudentHousingApproval(risk);
				triggerFlatRoofApproval(risk);
				triggerSTRWithSwimmingPoolApproval(risk);
				triggerSTRSolidFuelHeatingApproval(risk);
				triggerSTRWithNoSmokeDetectorApproval(risk);
				triggerTenantSwimmingPoolApproval(risk);
				triggerFuelTankLocationBelowGroundApproval(risk);
				triggerNoPrimaryHeatTypeApproval(risk);
				triggerBiteyDogApproval(risk);
				triggerHorseExposureOnTenantOccupiedDwellingApproval(risk);
				triggerUseOfHorsesForNonPersonalUseApproval(risk);
				triggerNumberOfHorsesApproval(risk);
				triggerRiskWithRecVehicleApproval(risk);
				triggerRiskWithResidentEmployeeApproval(risk);
				triggerDoesntMeetFortifiedMapReqApproval(risk);
				triggerFarmingOrBusinessApproval(risk);
				triggerLocationsNotSameStateApproval(risk);
				triggerNumberOfChildrenAtDayCareApproval(risk);
				triggerTrampolineOnPremiseApproval(risk);
				triggerElectricalLessThan100AmpsApproval(risk);
				triggerElectricalTypeApproval(risk);
				triggerRoofAgeGreaterThan20Approval(risk);
				triggerPlumbingSystemApproval(risk);
				triggerDayCareLiabilityLimitApproval(risk);
				triggerNaturalGasWellApproval(risk);
				triggerDwellingStyleCovALimitGreaterThan150k(risk);
				triggerDwellingStyleCovALimitGreaterThan500k(risk);
				triggerVacantDwellingApproval(risk);
				triggerAdditionalHazardsApproval(risk);
				triggerLLPSolidFuelHeatingApproval(risk);
				triggerLLPDwellingOlderThan35Approval(risk);
				triggerLLPFireUnitsOrDivisionApproval(risk);
				triggerMobileHomeSolidFuelHeatingApproval(risk);
				triggerFL2AndFL3MobileHomesOlderThan15Approval(risk);
				triggerFL1MobileHomesOlderThan35Approval(risk);
				triggerGL1UnderConstructionApproval(risk);
				triggerLowInsuranceScore1MCovLLimitApproval(risk);
				triggerLiabilityLimitAtRICApproval(risk);
				triggerUnderConstructionApproval(risk);
				triggerDistanceOutdoorWoodFurnaceApproval(risk);
				trigger1MCovLLimitApproval(risk);
				triggerFailedRiskAddressVerificationApproval(risk);
				triggerLowCovALimitDiffersMarketValueApproval(risk);
				triggerHighCovALimitDiffersMarketValueApproval(risk);
				triggerCovALimitLessThanMarketValueApproval(risk);
				triggerPropertyMoreThan100MilesApproval(risk);
				triggerFL2OrFl3HeatingNotUpdatedApproval(risk);
				triggerFL1HeatingNotUpdatedApproval(risk);
				triggerPropertyNotWinterizedApproval(risk);
				triggerFL200NotOnPermanentFoundationApproval(risk);
				triggerUnoccupiedBuildingApproval(risk);
				triggerNoFenceSwimmingPoolApproval(risk);
				triggerDivingOrSlideApproval(risk);
				triggerCondoCovCLimitOver150KApproval(risk);
				triggerCovCIncreaseApproval(risk);
				triggerFL200AddedApproval(risk);
				triggerFL1PermanentHomesCovAApproval(risk);
				triggerFL1MobileHomeCovAApproval(risk);
				triggerFL2PermanentHomesCovAApproval(risk);
				triggerCovALimitLt100kApproval(risk);
				triggerManualExperienceGroupLt50kApproval(risk);
				triggerManualExperienceGroupMobileHomeApproval(risk);
			}

			generateApprovalMsgs();
		}
	}

	private void triggerEndorsementRiskApprovals() throws Exception {
		for( ModelBean risk : risks ){
			triggerDwellingStyleChangeApproval(risk);
			triggerECOrVMMChangesApproval(risk);
			triggerAddExperienceGroupApproval(risk);
			triggerUnderConstructionApproval(risk);
			triggerGL1UnderConstructionApproval(risk);
			triggerStudentHousingApproval(risk);
			triggerBiteyDogApproval(risk);
			triggerHorseExposureOnTenantOccupiedDwellingApproval(risk);
			triggerUseOfHorsesForNonPersonalUseApproval(risk);
			triggerNumberOfHorsesApproval(risk);
			triggerVacantDwellingApproval(risk);
			trigger1MCovLLimitApproval(risk);
			triggerLowInsuranceScore1MCovLLimitApproval(risk);
			triggerCondoCovCLimitOver150KApproval(risk);
			triggerNoneLiabilityTypeChangeApproval(risk);
			triggerGLLiabilityTypeChangeApproval(risk);
			triggerRiskAddedApproval(risk);
			triggerOccupancyTypeChangeApproval(risk);
			triggerResidenceChangeApproval(risk);
			triggerProtectionClassChangeApproval(risk);
			triggerYearBuiltChangeApproval(risk);
			triggerConstructionCdChangeApproval(risk);
			triggerNumberOfUnitsChangeApproval(risk);
			triggerNumFamiliesChangeApproval(risk);
			triggerCovAIncreaseApproval(risk);
			triggerCovADecreaseApproval(risk);
			triggerCovCIncreaseApproval(risk);
			triggerPolicyDeductibleDecreaseApproval(risk);
			triggerSolidFuelHeatChangeApproval(risk);
			triggerLastProfInspectionDtChangeApproval(risk);
			triggerFuelStorageTankChangeApproval(risk);
			triggerNewDogApproval(risk);
			triggerGL72AddedApproval(risk);
			triggerGL73AddedApproval(risk);
			triggerGL80AddedApproval(risk);
			triggerNewDomesticEmployeesApproval(risk);
			triggerNaturalGasWellChangeApproval(risk);
			triggerPropertyManagerChangeApproval(risk);
			triggerFL15AddedApproval(risk);
			triggerFL16AddedApproval(risk);
			triggerSecuredPartyChangeApproval(risk);
			triggerCondoCovChangeApproval(risk);
			triggerFL34AddedApproval(risk);
			triggerFL35AddedApproval(risk);
			triggerFL36AddedApproval(risk);
			triggerFL0208RMAddedApproval(risk);
			triggerRepairReplaceChangeApproval(risk);
			triggerRM422AddedApproval(risk);
			triggerELEDeletedApproval(risk);
			triggerRMDP74DeletedApproval(risk);
			triggerRMDP75DeletedApproval(risk);
			triggerSwimPoolChangeApproval(risk);
			triggerFL200AddedApproval(risk);
			triggerGL74AddedApproval(risk);
			triggerGL82AddedApproval(risk);
			triggerFL200NotOnPermanentFoundationApproval(risk);
			triggerCovALimitLt100kApproval(risk);
			triggerManualExperienceGroupLt50kApproval(risk);
			triggerManualExperienceGroupMobileHomeApproval(risk);
			triggerAdditionalHazardsApproval(risk);
		}

		generateApprovalMsgs();
	}

	private void triggerRenewalRiskApprovals() throws Exception {
		for( ModelBean risk : risks ){
			triggerUnderConstructionApproval(risk);
			triggerVacantDwellingApproval(risk);
			triggerUnoccupiedBuildingApproval(risk);
			triggerCovALimitLt100kApproval(risk);
		}

		generateApprovalMsgs();
	}

	private void triggerInsuredEntityTypeEstateApproval() throws Exception {
		boolean canApproveInsuredEntityTypeEstate = UserRenderer.hasAuthority(user,"DWApproveInsuredEntityTypeEstate", todayDt);
		String entityTypeCd = insured.gets("EntityTypeCd");

		if( applicationDiff != null && !applicationDiff.hasFieldModifiedByXPath("//Insured/@EntityTypeCd") )
			return;

		if( !canApproveInsuredEntityTypeEstate && entityTypeCd.equals("Estate") ){
			String msg = String.format("Entity Type selection of %s requires approval", entityTypeCd);
			ValidationRenderer.addValidationError( application,"Approval","APPROVAL", msg );
		}
	}

	private void triggerCLUELossesWithinPast3YrsApproval() throws Exception {
		boolean canApproveCLUELossesWithinPast3Yrs = UserRenderer.hasAuthority(user, "DWApproveCLUELossesWithin3Yrs", todayDt);
		String[] lossTypes = {
				"Accidental Discharge Or Overflow Of Water Or Steam", "Advertising injury", "All other liability", 
				"Bodily Injury", "Fire", "Fire legal", "Flood", "Imprisonment", "Intentional Acts", "Invasion of Privacy", 
				"Liability (BI, PD, Med-Pay)", "Personal injury", "Seepage Or Leakage", "Sewer Backup", "Theft", 
				"Vandalism", "Water Damage Backup", "Water, Liquids, Powder, Molten Material", "Water", "Theft/Burglary"
		};
		ArrayList<String> lossTypesList = new ArrayList<String>(Arrays.asList(lossTypes));
		StringDate threeYrsAgoToday = StringDate.advanceYear( new StringDate(), -3 );

		if( !canApproveCLUELossesWithinPast3Yrs ){
			for( ModelBean loss : losses ){
				StringDate lossDt = loss.getDate("LossDt");

				if( loss.gets("SourceCd").equals("CLUE") && lossTypesList.contains(loss.gets("LossCauseCd")) && 
						DateRenderer.greaterThanEqual(lossDt, threeYrsAgoToday)){
					String msg = "LexisNexis CLUE report returned with loss(es) within the past 3 years. This requires approval";
					ValidationRenderer.addValidationError( application,"Approval","APPROVAL", msg );
					return;
				}
			}
		}
	}

	private void triggerMoreThan1PaidLossApproval() throws Exception {
		boolean canApproveMoreThan1PaidLoss = UserRenderer.hasAuthority(user, "DWApproveMoreThan1PaidLoss", todayDt);
		int numberOfPriorPaidLoss = 0;

		if( !canApproveMoreThan1PaidLoss ){
			for( ModelBean loss : losses ) {
				if (StringRenderer.greaterThan(loss.gets("PaidAmt"), "0") && !StringRenderer.isTrue(loss.gets("IgnoreInd"))  )
					numberOfPriorPaidLoss++;
			}

			if( numberOfPriorPaidLoss > 1 ){
				String msg = "More than 1 prior paid loss requires approval";
				ValidationRenderer.addValidationError( application,"Approval","APPROVAL", msg );
			}
		}
	}

	private void triggerPriorPaidLossGreaterThan10kApproval() throws Exception {
		boolean canApprovePriorPaidLossGreaterThan10k = UserRenderer.hasAuthority(user, "DWApprovePriorPaidLossGreaterThan10k", todayDt);
		StringDate threeYrsAgoToday = StringDate.advanceYear( new StringDate(), -3 );
		int numberOfGreaterThan10kPaidLoss = 0;

		if( !canApprovePriorPaidLossGreaterThan10k ){
			for( ModelBean loss : losses ) {
				StringDate lossDt = loss.getDate("LossDt");

				if (StringRenderer.greaterThanEqual(loss.gets("PaidAmt"), "10000") && DateRenderer.lessThan(threeYrsAgoToday, lossDt) ) {
					numberOfGreaterThan10kPaidLoss++;
				}
			}

			if( numberOfGreaterThan10kPaidLoss >= 1 ){
				String msg = "Paid loss of $10,000 or more requires approval";
				ValidationRenderer.addValidationError( application,"Approval","APPROVAL", msg );
			}
		}
	}

	private void triggerNoHitNoInsuranceScoreApproval() throws Exception {
		boolean canApproveNoHitNoInsuranceScore = UserRenderer.hasAuthority(user, "DWApproveNoHitNoInsuranceScore", todayDt);
		ModelBean insuredInsuranceScoreModelBean = insured.getBean("InsuranceScore", "InsuranceScoreTypeCd", "InsuredInsuranceScore");
		ModelBean insuredJointInsuranceScoreModelBean = insured.getBean("InsuranceScore", "InsuranceScoreTypeCd", "InsuredJointInsuranceScore");
		String insuredInsuranceScore = insuredInsuranceScoreModelBean.gets("InsuranceScore");
		String insuredJointInsuranceScore = insuredJointInsuranceScoreModelBean.gets("InsuranceScore");

		if( !canApproveNoHitNoInsuranceScore ){
			String msg = "A %s was returned for %s requires approval";

			if( StringRenderer.equal(insuredInsuranceScore, "998") || StringRenderer.equal(insuredInsuranceScore, "999") ){
				String insuredCommercialName = insured.getBean("PartyInfo","PartyTypeCd","InsuredParty").getBean("NameInfo", "NameTypeCd", "InsuredName").gets("CommercialName");
				ValidationRenderer.addValidationError( application,"Approval","APPROVAL",  String.format( msg, insuredInsuranceScore, insuredCommercialName ) );
			} else if( StringRenderer.equal(insuredJointInsuranceScore, "998") || StringRenderer.equal(insuredJointInsuranceScore, "999") ){
				String insuredJointCommercialName = insured.getBean("PartyInfo","PartyTypeCd","InsuredPartyJoint").getBean("NameInfo", "NameTypeCd", "InsuredNameJoint").gets("CommercialName");
				ValidationRenderer.addValidationError( application,"Approval","APPROVAL", String.format( msg, insuredJointInsuranceScore, insuredJointCommercialName ) );
			}
		}
	}

	private void triggerLowInsuranceScoreApproval() throws Exception {
		boolean canApproveLowInsuranceScore = UserRenderer.hasAuthority(user,"DWApproveLowInsuranceScore", todayDt);
		ModelBean insuredInsuranceScoreModelBean = insured.getBean("InsuranceScore", "InsuranceScoreTypeCd", "InsuredInsuranceScore");
		ModelBean insuredJointInsuranceScoreModelBean = insured.getBean("InsuranceScore", "InsuranceScoreTypeCd", "InsuredJointInsuranceScore");
		String overriddenInsuredInsuranceScore = insuredInsuranceScoreModelBean.gets("OverriddenInsuranceScore");
		String overriddenInsuredJointInsuranceScore = insuredJointInsuranceScoreModelBean.gets("OverriddenInsuranceScore");
		String insuredInsuranceScore = !overriddenInsuredInsuranceScore.isEmpty() ?
				overriddenInsuredInsuranceScore : insuredInsuranceScoreModelBean.gets("InsuranceScore");
		String insuredJointInsuranceScore = !overriddenInsuredJointInsuranceScore.isEmpty() ?
				overriddenInsuredJointInsuranceScore : insuredJointInsuranceScoreModelBean.gets("InsuranceScore");
		boolean isIndividual = insured.gets("EntityTypeCd").equals("Individual");
		boolean isJoint = insured.gets("EntityTypeCd").equals("Joint");

		if( !canApproveLowInsuranceScore &&
				( ( isIndividual && StringRenderer.lessThan(insuredInsuranceScore, "600") ) ||
						( isJoint && StringRenderer.lessThan(insuredInsuranceScore, "600") && StringRenderer.lessThan(insuredJointInsuranceScore, "600") ) ) ){
			ValidationRenderer.addValidationError( application,"Approval","APPROVAL", "Insurance Score requires approval" );
		}
	}

	private void triggerPrimaryHeatingRule(ModelBean risk) throws Exception {
		if(risk.getBean("Building").gets("Heating").equals("") ) {
			String action = "javascript:cmmXFormSafe('UWApplicationSync','"+application.getSystemId()+"','"+line.getId()+"','"+risk.getId()+"','"+dwellingTabParam+"&HighlightFields=Building.Heating')";
			String dwellingTabLink = ValidationRenderer.createActionLink(action);
			ValidationRenderer.addValidationErrorWithLink(application, "Validation", "ERROR", "Field 'Primary Heating' is required.", dwellingTabLink);
		}
	}

	private void triggerSecondaryHeatingRule(ModelBean risk) throws Exception {
		if(risk.getBean("Building").gets("SecondaryHeat").equals("") ) {
			String action = "javascript:cmmXFormSafe('UWApplicationSync','"+application.getSystemId()+"','"+line.getId()+"','"+risk.getId()+"','"+dwellingTabParam+"&HighlightFields=Building.SecondaryHeat')";
			String dwellingTabLink = ValidationRenderer.createActionLink(action);	
			ValidationRenderer.addValidationErrorWithLink(application, "Validation", "ERROR", "Field 'Secondary Heating' is required.", dwellingTabLink);
		}
	}

	private void triggerSupplyPipesRule(ModelBean risk) throws Exception {
		ModelBean building = risk.getBean("Building");
		String yearBuilt = building.gets("YearBuilt");

		if(!yearBuilt.equals("") && StringRenderer.isNumeric(yearBuilt) && building.gets("PlumbingReplaced").equals("") && StringRenderer.greaterThan( StringRenderer.subtractInt(todayYear, yearBuilt ), "35") ) {
			String action = "javascript:cmmXFormSafe('UWApplicationSync','"+application.getSystemId()+"','"+line.getId()+"','"+risk.getId()+"','"+dwellingTabParam+"&HighlightFields=Building.Heating')";
			String dwellingTabLink = ValidationRenderer.createActionLink(action);
			ValidationRenderer.addValidationErrorWithLink(application, "Validation", "ERROR", "Question 'Have supply pipes been completely replaced...' is required.", dwellingTabLink);
		}
	}

	private void triggerWiringSheathRule(ModelBean risk) throws Exception {
		ModelBean building = risk.getBean("Building");
		String yearBuilt = building.gets("YearBuilt");

		if(!yearBuilt.equals("") && StringRenderer.isNumeric(yearBuilt) && building.gets("WiringSheathed").equals("") && StringRenderer.greaterThan( StringRenderer.subtractInt(todayYear, yearBuilt ), "35") ) {
			String action = "javascript:cmmXFormSafe('UWApplicationSync','"+application.getSystemId()+"','"+line.getId()+"','"+risk.getId()+"','"+dwellingTabParam+"&HighlightFields=Building.Heating')";
			String dwellingTabLink = ValidationRenderer.createActionLink(action);
			ValidationRenderer.addValidationErrorWithLink(application, "Validation", "ERROR", "'Is dwelling completely wired with NM (Romex) or BX (Armored Cable)?' Question is required.", dwellingTabLink);
		}
	}

	private void triggerRoofReplacedRule(ModelBean risk) throws Exception {
		ModelBean building = risk.getBean("Building");
		String yearBuilt = building.gets("YearBuilt");

		if(!yearBuilt.equals("") && StringRenderer.isNumeric(yearBuilt) && building.gets("RoofReplaced").equals("") && !building.gets("DwellingStyle").equals("Condominium/Co-operative") && StringRenderer.greaterThan( StringRenderer.subtractInt(todayYear, yearBuilt ), "20") ) {
			String action = "javascript:cmmXFormSafe('UWApplicationSync','"+application.getSystemId()+"','"+line.getId()+"','"+risk.getId()+"','"+dwellingTabParam+"&HighlightFields=Building.Heating')";
			String dwellingTabLink = ValidationRenderer.createActionLink(action);
			ValidationRenderer.addValidationErrorWithLink(application, "Validation", "ERROR", "Has The Roof Been Replaced?' Question is required.", dwellingTabLink);
		}
	}
	
	private boolean checkLiabilityCoverageTypeForFirstRisk() throws Exception {
		ModelBean[] activeBuildings = line.findBeansByFieldValue("Building", "Status", "Active");
		ModelBean[] sortedActiveBuildings = BeanRenderer.sortBeansBy(activeBuildings, "BldgNumber", "Ascending");
		
		ModelBean building = sortedActiveBuildings[0];
		
		if( building.gets("LiabilityCoverageType").isEmpty() ) {
			String action = "javascript:cmmXFormSafe('UWApplicationSync','"+application.getSystemId()+"','"+line.getId()+"','"+ sortedActiveBuildings[0].getParentBean().getId()+"','"+dwellingTabParam+"&HighlightFields=Building.LiabilityCoverageType')";
			String dwellingTabLink = ValidationRenderer.createActionLink(action);
			ValidationRenderer.addValidationErrorWithLink(application, "Validation", "ERROR", "A Liability Coverage Type must be selected.", dwellingTabLink);
			return false;
		}
		
		return true;
	}

	private void triggerNoOlderHomeUpdatesApproval( ModelBean risk ) throws Exception {
		boolean canApproveNoOlderHomeUpdates = UserRenderer.hasAuthority(user, "DWApproveNoOlderHomeUpdates", todayDt);
		ModelBean building = risk.getBean("Building");
		String subTypeCd = basicPolicy.gets("SubTypeCd");
		Integer buildingAge = todayDt.getCalendar().get(Calendar.YEAR) - Integer.parseInt(building.gets("YearBuilt"));
		String hasHeatingOrPlumbingUpdated = building.gets("ExperienceGroup");

		if( !canApproveNoOlderHomeUpdates &&
				(subTypeCd.equals("FL2") || subTypeCd.equals("FL3")) && buildingAge > 35 && StringRenderer.isFalse(hasHeatingOrPlumbingUpdated) ){
			addRiskApproval("Older home without completed updates requires approval", risk);
		}
	}

	private void triggerFL255DueToAgeApproval( ModelBean risk ) throws Exception {
		boolean canApproveFL255DueToAge = UserRenderer.hasAuthority(user,"DWApproveFL255DueToAge", todayDt);
		ModelBean building = risk.getBean("Building");

		if( !canApproveFL255DueToAge && !basicPolicy.gets("SubTypeCd").equals("FL1") &&
				StringRenderer.lessThanEqual(building.gets("YearBuilt"), "1940") &&
				!building.gets("RepairReplacementEndorsements").equals("FL-255") ){
			addRiskApproval("The Repair Cost FL 255 endorsement is required due to the age of the dwelling. To issue without FL 255 requires approval", risk);
		}
	}

	private void triggerStudentHousingApproval( ModelBean risk ) throws Exception {
		boolean canApproveStudentHousingRental = UserRenderer.hasAuthority(user,"DWApproveStudentHousingRental", todayDt);
		ModelBean building = risk.getBean("Building");

		if( !isNewRisk(risk) && applicationDiff != null &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@StudentHousing") )
			return;

		if( !canApproveStudentHousingRental && StringRenderer.isTrue( building.gets("StudentHousing") ) ){
			ValidationRenderer.addValidationError( application, "Approval", "APPROVAL", "Student Housing requires approval" );
		}
	}

	private void triggerGL1UnderConstructionApproval( ModelBean risk ) throws Exception {
		boolean canApproveGL1UnderConstruction = UserRenderer.hasAuthority(user, "DWApproveGL1UnderConstruction", todayDt);
		ModelBean building = risk.getBean("Building");

		if( !isNewRisk(risk) && applicationDiff != null &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@LiabilityCoverageType") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@Residence") )
			return;

		if( !canApproveGL1UnderConstruction &&
				building.gets("LiabilityCoverageType").equals("GL-1") && building.gets("Residence").equals("Under Construction (New)") ){
			addRiskApproval(
					"A dwelling under construction with the GL-1 Personal &amp; Premises liability requires approval.",
					risk, dwellingTabParam, "Building.LiabilityCoverageType");
		}
	}

	private void triggerFlatRoofApproval( ModelBean risk ) throws Exception {
		boolean canApproveFlatRoofType = UserRenderer.hasAuthority(user, "DWApproveFlatRoofType", todayDt);
		ModelBean building = risk.getBean("Building");
		String roofCd = building.gets("RoofCd");
		String roofCds = "Flat,Composition Roll,Composition Built Up,EPDM Membrane,PVC Membrane,Tar and Gravel,TPO Membrane";
		
		if( !canApproveFlatRoofType && StringRenderer.in(roofCd, roofCds) ){
			addRiskApproval("Flat roof type requires approval", risk );
		}
	}

	private void triggerLowInsuranceScore1MCovLLimitApproval( ModelBean risk ) throws Exception {
		if( dwellingQuestionReplies == null )
			return;

		boolean canApproveSTRLowInsurance1MCovLLimitApproval = UserRenderer.hasAuthority(user, "DWApproveWithLowInsuranceScore1MCovLLimit", todayDt);
		String dwProgramTypeCd = basicPolicy.gets("DwProgramTypeCd");
		String hasLiabilityLimitAtRIC = dwellingQuestionReplies.getBean("QuestionReply", "Name", "OtherLimitWithRIC") != null ?  
				dwellingQuestionReplies.getBean("QuestionReply", "Name", "OtherLimitWithRIC").gets("Value") : "";
		String covLLimit = risk.getBean("Building").gets("CovLLimit");
		ModelBean insuredInsuranceScoreModelBean = insured.getBean("InsuranceScore", "InsuranceScoreTypeCd", "InsuredInsuranceScore");
		ModelBean insuredJointInsuranceScoreModelBean = insured.getBean("InsuranceScore", "InsuranceScoreTypeCd", "InsuredJointInsuranceScore");
		String overriddenInsuredInsuranceScore = insuredInsuranceScoreModelBean.gets("OverriddenInsuranceScore");
		String overriddenInsuredJointInsuranceScore = insuredJointInsuranceScoreModelBean.gets("OverriddenInsuranceScore");
		String insuredInsuranceScore = !overriddenInsuredInsuranceScore.isEmpty() ?
				overriddenInsuredInsuranceScore : insuredInsuranceScoreModelBean.gets("InsuranceScore");
		String insuredJointInsuranceScore = !overriddenInsuredJointInsuranceScore.isEmpty() ?
				overriddenInsuredJointInsuranceScore : insuredJointInsuranceScoreModelBean.gets("InsuranceScore");

		if( !isNewRisk(risk) && applicationDiff != null &&
				!applicationDiff.hasFieldModifiedByXPath("//BasicPolicy/@DwProgramTypeCd") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@CovLLimit") &&
				!applicationDiff.hasFieldModifiedByXPath("//QuestionReplies/QuestionReply[@Name='OtherLimitWithRIC']/@Value") &&
				!applicationDiff.hasFieldModifiedByXPath("//Insured/InsuranceScore[@InsuranceScoreTypeCd='InsuredInsuranceScore']/@InsuranceScore") &&
				!applicationDiff.hasFieldModifiedByXPath("//Insured/InsuranceScore[@InsuranceScoreTypeCd='InsuredInsuranceScore']/@OverriddenInsuranceScore") &&
				!applicationDiff.hasFieldModifiedByXPath("//Insured/InsuranceScore[@InsuranceScoreTypeCd='InsuredJointInsuranceScore']/@InsuranceScore") &&
				!applicationDiff.hasFieldModifiedByXPath("//Insured/InsuranceScore[@InsuranceScoreTypeCd='InsuredJointInsuranceScore']/@OverriddenInsuranceScore"))
			return;

		if( !canApproveSTRLowInsurance1MCovLLimitApproval && !dwProgramTypeCd.equals("STR") && covLLimit.equals("1000000") && StringRenderer.isFalse(hasLiabilityLimitAtRIC) && 
				( StringRenderer.lessThan(insuredInsuranceScore, "700") || StringRenderer.lessThan(insuredJointInsuranceScore, "700") ) ){
			addRiskApproval("Application requires approval", risk );
		}
	}

	private void triggerCovALimitLessThanMarketValueApproval( ModelBean risk ) throws Exception {
		boolean canApproveCovALimitLessThanMarketValue = UserRenderer.hasAuthority(user, "DWApproveCovALimitLessThanMarketValue", todayDt);
		ModelBean building = risk.getBean("Building");
		String covALimit = building.gets("CovALimit");
		String dwellingStyle = building.gets("DwellingStyle");
		String replacementCost = VelocityTools.replaceAll(building.gets("ReplacementCost"), "[^0-9.]+", "");

		if( !canApproveCovALimitLessThanMarketValue && !replacementCost.isEmpty() && StringRenderer.lessThan(covALimit, replacementCost) && !dwellingStyle.equals("Condominium/Co-operative") ){
			String formattedReplacementCost = StringRenderer.formatMoney(replacementCost, false);
			String msg = String.format( "The Coverage - A Dwelling limit entered is lower than %s, "
					+ "which is the amount calculated from the e2value estimator. This requires approval", formattedReplacementCost);
			addRiskApproval(msg, risk);
		}
	}

	private void triggerLowCovALimitDiffersMarketValueApproval( ModelBean risk ) throws Exception {
		boolean canApproveLowCovALimitDiffersMarketValue = UserRenderer.hasAuthority(user, "DWApproveLowCovALimitDiffersMarketValue", todayDt);
		ModelBean insuredInsuranceScoreModelBean = insured.getBean("InsuranceScore", "InsuranceScoreTypeCd", "InsuredInsuranceScore");
		ModelBean insuredJointInsuranceScoreModelBean = insured.getBean("InsuranceScore", "InsuranceScoreTypeCd", "InsuredJointInsuranceScore");
		String insuredInsuranceScore = insuredInsuranceScoreModelBean.gets("InsuranceScore");
		String insuredJointInsuranceScore = insuredJointInsuranceScoreModelBean.gets("InsuranceScore");
		String overriddenInsuredInsuranceScore = insuredInsuranceScoreModelBean.gets("OverriddenInsuranceScore");
		String overriddenInsuredJointInsuranceScore = insuredJointInsuranceScoreModelBean.gets("OverriddenInsuranceScore");
		boolean insuranceScoreLessThan700 = 
				(!overriddenInsuredInsuranceScore.isEmpty() && StringRenderer.lessThan(overriddenInsuredInsuranceScore, "700")) || 
				(!overriddenInsuredJointInsuranceScore.isEmpty() && StringRenderer.lessThan(overriddenInsuredJointInsuranceScore, "700")) || 
				( !insuredInsuranceScore.isEmpty() && StringRenderer.lessThan(insuredInsuranceScore, "700") ) || 
				( !insuredJointInsuranceScore.isEmpty() && StringRenderer.lessThan(insuredJointInsuranceScore, "700") );
		ModelBean building = risk.getBean("Building");
		String dwellingStyle = building.gets("DwellingStyle");
		String covALimit = building.gets("CovALimit");
		String replacementCost = VelocityTools.replaceAll(building.gets("ReplacementCost"), "[^0-9.]+", "");
		String onePointFiveReplacementCost = StringRenderer.multiply(replacementCost, "1.5", 1);
		String replacementCostPlus100000 = StringRenderer.addInt(replacementCost, "100000");

		if( !canApproveLowCovALimitDiffersMarketValue && !replacementCost.isEmpty() && !dwellingStyle.equals("Condominium/Co-operative") &&
				( ( insuranceScoreLessThan700 && StringRenderer.greaterThan(covALimit, onePointFiveReplacementCost) ) || 
						StringRenderer.greaterThan(covALimit, replacementCostPlus100000) ) ){
			addRiskApproval( "Coverage A amount compared to Replacement Cost requires approval", risk );
		}
	}

	private void triggerHighCovALimitDiffersMarketValueApproval( ModelBean risk ) throws Exception {
		boolean canApproveHighCovALimitDiffersMarketValue = UserRenderer.hasAuthority(user, "DWApproveHighCovALimitDiffersMarketValue", todayDt);
		ModelBean insuredInsuranceScoreModelBean = insured.getBean("InsuranceScore", "InsuranceScoreTypeCd", "InsuredInsuranceScore");
		ModelBean insuredJointInsuranceScoreModelBean = insured.getBean("InsuranceScore", "InsuranceScoreTypeCd", "InsuredJointInsuranceScore");
		String insuredInsuranceScore = insuredInsuranceScoreModelBean.gets("InsuranceScore");
		String insuredJointInsuranceScore = insuredJointInsuranceScoreModelBean.gets("InsuranceScore");
		String overriddenInsuredInsuranceScore = insuredInsuranceScoreModelBean.gets("OverriddenInsuranceScore");
		String overriddenInsuredJointInsuranceScore = insuredJointInsuranceScoreModelBean.gets("OverriddenInsuranceScore");
		boolean insuranceScoreGreaterThanEqual700 = 
				(!overriddenInsuredInsuranceScore.isEmpty() && StringRenderer.greaterThanEqual(overriddenInsuredInsuranceScore, "700")) || 
				(!overriddenInsuredJointInsuranceScore.isEmpty() && StringRenderer.greaterThanEqual(overriddenInsuredJointInsuranceScore, "700")) || 
				(!insuredInsuranceScore.isEmpty() &&  StringRenderer.greaterThanEqual(insuredInsuranceScore, "700")) || 
				(!insuredJointInsuranceScore.isEmpty() && StringRenderer.greaterThanEqual(insuredJointInsuranceScore, "700"));
		ModelBean building = risk.getBean("Building");
		String dwellingStyle = building.gets("DwellingStyle");
		String covALimit = building.gets("CovALimit");
		String replacementCost = VelocityTools.replaceAll(building.gets("ReplacementCost"), "[^0-9.]+", "");
		String doubledReplacementCost = StringRenderer.multiply(replacementCost, "2", 0);
		String replacementCostPlus250000 = StringRenderer.addInt(replacementCost, "250000");

		if( !canApproveHighCovALimitDiffersMarketValue && !replacementCost.isEmpty() && !dwellingStyle.equals("Condominium/Co-operative") &&
				( ( insuranceScoreGreaterThanEqual700 && StringRenderer.greaterThan(covALimit, doubledReplacementCost) ) || 
						StringRenderer.greaterThan(covALimit, replacementCostPlus250000) ) ){
			addRiskApproval("Coverage A amount compared to Replacement Cost requires approval", risk);
		}
	}

	private void triggerSTRWithSwimmingPoolApproval( ModelBean risk ) throws Exception {
		boolean canApproveSTRWithSwimmingPool = UserRenderer.hasAuthority(user, "DWApproveSTRWithSwimmingPool", todayDt);
		ModelBean building = risk.getBean("Building");
		String dwProgramTypeCd = basicPolicy.gets("DwProgramTypeCd");

		if (!canApproveSTRWithSwimmingPool &&
				dwProgramTypeCd.equals("STR") && !building.gets("SwimPoolSurcharge").equals("No")) {
			addRiskApproval("Short Term Vacation Rental with a swimming pool requires approval", risk);
		}
	}

	private void triggerSTRSolidFuelHeatingApproval( ModelBean risk ) throws Exception {
		boolean canApproveSTRSolidFuelHeating = UserRenderer.hasAuthority(user, "DWApproveSTRSolidFuelHeating", todayDt);
		ModelBean building = risk.getBean("Building");
		String primaryHeat = building.gets("Heating");
		String secondaryHeat = building.gets("SecondaryHeat");
		String heatTypes = "Wood Forced Air,Wood Hot Water / Steam,Wood Radiant (Wood Stove),Pellet Stove,Outdoor Wood Furnace";

		if( !canApproveSTRSolidFuelHeating &&
				basicPolicy.gets("DwProgramTypeCd").equals("STR") && 
				( !building.gets("SolidFuelHeatType").equals("None") || 
						StringRenderer.in(primaryHeat, heatTypes) || StringRenderer.in(secondaryHeat, heatTypes) )){
			addRiskApproval("Short Term Vacation Rental with a solid fuel heating device requires approval", risk);
		}
	}

	private void triggerSTRWithNoSmokeDetectorApproval( ModelBean risk ) throws Exception {
		boolean canApproveSTRWithNoSmokeDetector = UserRenderer.hasAuthority(user, "DWApproveSTRWithNoSmokeDetector", todayDt);
		ModelBean building = risk.getBean("Building");
		String dwProgramTypeCd = basicPolicy.gets("DwProgramTypeCd");

		if( !canApproveSTRWithNoSmokeDetector &&
				dwProgramTypeCd.equals("STR") && building.gets("SmokeDetectorCd").equals("No") ){
			addRiskApproval("Short Term Vacation Rental without working local smoke detectors requires approval", risk );
		}
	}

	private void triggerTenantSwimmingPoolApproval( ModelBean risk ) throws Exception {
		boolean canApproveTenantSwimmingPool = UserRenderer.hasAuthority(user,"DWApproveTenantSwimmingPool", todayDt);
		ModelBean building = risk.getBean("Building");

		if( !canApproveTenantSwimmingPool 
				&& building.gets("OccupancyCd").equals("Tenant") && !building.gets("SwimPoolSurcharge").equals("No") ){
			addRiskApproval("Swimming pool with a tenant occupied dwelling requires approval", risk);
		}
	}

	private void triggerFuelTankLocationBelowGroundApproval( ModelBean risk ) throws Exception {
		boolean canApproveFuelTankLocationBelowGround = UserRenderer.hasAuthority(user, "DWApproveFuelTankLocationBelowGround", todayDt);
		ModelBean building = risk.getBean("Building");

		if( !canApproveFuelTankLocationBelowGround && building.gets("FuelTankLocation").equals("Outdoors - Below ground") ){
			addRiskApproval("Underground fuel storage tank requires approval", risk );
		}
	}

	private void triggerNoPrimaryHeatTypeApproval( ModelBean risk ) throws Exception {
		boolean canApproveNoPrimaryHeatType = UserRenderer.hasAuthority(user, "DWApproveNoPrimaryHeatType", todayDt);
		ModelBean building = risk.getBean("Building");

		if( !canApproveNoPrimaryHeatType && building.gets("Heating").equals("None") ){
			addRiskApproval("No primary heat type requires approval", risk );
		}
	}

	private void triggerLLPSolidFuelHeatingApproval( ModelBean risk ) throws Exception {
		boolean canApproveLLPSolidFuelHeating = UserRenderer.hasAuthority(user, "DWApproveLLPSolidFuelHeating", todayDt);
		ModelBean building = risk.getBean("Building");
		String primaryHeat = building.gets("Heating");
		String secondaryHeat = building.gets("SecondaryHeat");
		String[] heatTypes = {"Wood Forced Air", "Wood Hot Water / Steam", "Wood Radiant (Wood Stove)", "Pellet Stove", "Outdoor Wood Furnace"};
		ArrayList<String> heatTypesList = new ArrayList<String>(Arrays.asList(heatTypes));

		if( !canApproveLLPSolidFuelHeating &&
				basicPolicy.gets("DwProgramTypeCd").equals("LLP") && 
				( !building.gets("SolidFuelHeatType").equals("None") || 
						StringRenderer.contains(primaryHeat, heatTypesList) || StringRenderer.contains(secondaryHeat, heatTypesList) )){
			addRiskApproval(
					"A dwelling with a solid fuel heating device in the Landlords Premises program requires approval.",
					risk, policyTabParam, "BasicPolicy.DwProgramTypeCd");
		}
	}

	private void triggerLLPDwellingOlderThan35Approval( ModelBean risk ) throws Exception {
		boolean canApproveLLPSolidFuelHeating = UserRenderer.hasAuthority(user, "DWApproveLLPDwellingOlderThan35", todayDt);
		String dwProgramTypeCd = basicPolicy.gets("DwProgramTypeCd");
		ModelBean building = risk.getBean("Building");
		Integer buildingAge = todayDt.getCalendar().get(Calendar.YEAR) - Integer.parseInt(building.gets("YearBuilt"));

		if( !canApproveLLPSolidFuelHeating && dwProgramTypeCd.equals("LLP") && buildingAge > 35 ){
			addRiskApproval("Dwellings older than 35 years in the Landlords Premises program requires approval.",
					risk, policyTabParam, "BasicPolicy.DwProgramTypeCd");
		}
	}

	private void triggerLLPFireUnitsOrDivisionApproval( ModelBean risk ) throws Exception {
		boolean canApproveLLPFireUnitsOrDivision = UserRenderer.hasAuthority(user, "DWApproveLLPFireUnitsOrDivision", todayDt);
		String dwProgramTypeCd = basicPolicy.gets("DwProgramTypeCd");
		ModelBean building = risk.getBean("Building");

		if( !canApproveLLPFireUnitsOrDivision && dwProgramTypeCd.equals("LLP") && 
				( StringRenderer.greaterThanEqual( building.gets("NumFamilies"), "3") || StringRenderer.greaterThan(building.gets("Units"), "2")) ){
			addRiskApproval("A 3 or 4 family dwelling or a dwelling with more than 2 units in the fire division in the Landlords Premises program requires approval.",
					risk, policyTabParam, "BasicPolicy.DwProgramTypeCd");
		}
	}

	private void triggerMobileHomeSolidFuelHeatingApproval( ModelBean risk ) throws Exception {
		boolean canApproveMobileHomeSolidFuelHeating = UserRenderer.hasAuthority(user, "DWApproveMobileHomeSolidFuelHeating", todayDt);
		ModelBean building = risk.getBean("Building");
		String primaryHeat = building.gets("Heating");
		String secondaryHeat = building.gets("SecondaryHeat");
		String[] heatTypes = {"Wood Forced Air", "Wood Hot Water / Steam", "Wood Radiant (Wood Stove)", "Pellet Stove", "Outdoor Wood Furnace"};
		ArrayList<String> heatTypesList = new ArrayList<String>(Arrays.asList(heatTypes));

		if( !canApproveMobileHomeSolidFuelHeating &&
				building.gets("DwellingStyle").equals("Mobile home") && 
				( !building.gets("SolidFuelHeatType").equals("None") || 
						StringRenderer.contains(primaryHeat, heatTypesList) || StringRenderer.contains(secondaryHeat, heatTypesList) ) ){
			addRiskApproval("Solid Fuel Heating device in a Mobile Home requires approval", risk);
		}
	}

	private void triggerFL2AndFL3MobileHomesOlderThan15Approval( ModelBean risk ) throws Exception {
		boolean canApproveMobileHomeOlderThan15 = UserRenderer.hasAuthority(user, "DWApproveFL2AndFL3MobileHomeOlderThan15", todayDt);
		ModelBean building = risk.getBean("Building");
		String subTypeCd = basicPolicy.gets("SubTypeCd");
		Integer buildingAge = todayDt.getCalendar().get(Calendar.YEAR) - Integer.parseInt(building.gets("YearBuilt"));

		if( !canApproveMobileHomeOlderThan15 && 
				( subTypeCd.equals("FL2") || subTypeCd.equals("FL3") ) && buildingAge > 15 && building.gets("DwellingStyle").equals("Mobile home") ){
			addRiskApproval("A mobile home over 15 years old with a policy form of FL-2 or FL-3 requires approval",
					risk, policyTabParam, "BasicPolicy.SubTypeCd");
		}
	}

	private void triggerFL1MobileHomesOlderThan35Approval( ModelBean risk ) throws Exception {
		boolean canApproveMobileHomeOlderThan35 = UserRenderer.hasAuthority(user, "DWApproveFL1MobileHomeOlderThan35", todayDt);
		ModelBean building = risk.getBean("Building");
		Integer buildingAge = todayDt.getCalendar().get(Calendar.YEAR) - Integer.parseInt(building.gets("YearBuilt"));

		if( !canApproveMobileHomeOlderThan35 &&
				basicPolicy.gets("SubTypeCd").equals("FL1") && buildingAge > 35 && building.gets("DwellingStyle").equals("Mobile home") ){
			addRiskApproval("A mobile home over 35 years old requires approval", risk);
		}
	}

	private void triggerBiteyDogApproval( ModelBean risk ) throws Exception {
		boolean canApproveDogWithBiteHistory = UserRenderer.hasAuthority(user, "DWApproveDogWithBiteHistory", todayDt); 
		String anyBiteHistories = RICProductRenderer.findBeanByFieldValue(risk.getBean("QuestionReplies"), "QuestionReply", "Name", "QAnyBiteHistoryDogs").gets("Value");

		if( !isNewRisk(risk) && applicationDiff != null &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/QuestionReplies/QuestionReply[@Name='QAnyBiteHistoryDogs']/@Value") )
			return;

		if( !canApproveDogWithBiteHistory && StringRenderer.isTrue( anyBiteHistories ) ){
			addRiskApproval("Dog with a bite history requires approval", risk);
		}
	}

	private void triggerHorseExposureOnTenantOccupiedDwellingApproval( ModelBean risk ) throws Exception {
		boolean canApproveHorseOnTenantOccupiedDwelling = UserRenderer.hasAuthority(user, "DWApproveHorseOnTenantOccupiedDwelling", todayDt); 
		String hasHorses = RICProductRenderer.findBeanByFieldValue(risk.getBean("QuestionReplies"), "QuestionReply", "Name", "QAnyHorses").gets("Value");
		ModelBean building = risk.getBean("Building");
		String occupancyCd = building.gets("OccupancyCd");
		String liabilityType = building.gets("LiabAppLocInd").equals("No") ? "None" : building.gets("LiabilityCoverageType");

		if( !isNewRisk(risk) && applicationDiff != null &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/QuestionReplies/QuestionReply[@Name='QAnyHorses']/@Value") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@OccupancyCd") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@LiabAppLocInd") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@LiabilityCoverageType") )
			return;

		if( !canApproveHorseOnTenantOccupiedDwelling && 
				occupancyCd.equals("Tenant") &&	StringRenderer.isTrue( hasHorses ) && !liabilityType.equals("None") ){
			addRiskApproval("Equine exposure with a tenant occupied dwelling requires approval", risk);
		}
	}

	private void triggerUseOfHorsesForNonPersonalUseApproval( ModelBean risk ) throws Exception {
		boolean canApproveUseOfHorsesForNonPersonalUse = UserRenderer.hasAuthority(user, "DWApproveUseOfHorsesForNonPersonalUse", todayDt);
		String horsesForPersonalUse = RICProductRenderer.findBeanByFieldValue(risk.getBean("QuestionReplies"), "QuestionReply", "Name", "QHorsesPersonalUseOnly").gets("Value");
		ModelBean building = risk.getBean("Building");
		String occupancyCd = building.gets("OccupancyCd");
		String liabilityType = building.gets("LiabAppLocInd").equals("No") ? "None" : building.gets("LiabilityCoverageType");

		if( !isNewRisk(risk) && applicationDiff != null &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/QuestionReplies/QuestionReply[@Name='QHorsesPersonalUseOnly']/@Value") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@OccupancyCd") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@LiabAppLocInd") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@LiabilityCoverageType") )
			return;

		if( !canApproveUseOfHorsesForNonPersonalUse && 
				occupancyCd.equals("Owner") && StringRenderer.isFalse( horsesForPersonalUse ) && !liabilityType.equals("None") ){
			addRiskApproval("Non-personal use of a horse on the premises requires approval", risk);
		}
	}

	private void triggerNumberOfHorsesApproval( ModelBean risk ) throws Exception {
		boolean canApproveNumberOfHorses = UserRenderer.hasAuthority(user, "DWApproveNumberOfHorses", todayDt);
		ModelBean questionReplies = risk.getBean("QuestionReplies");
		String horsesForPersonalUse = RICProductRenderer.findBeanByFieldValue(questionReplies, "QuestionReply", "Name", "QHorsesPersonalUseOnly").gets("Value");
		String numberOfHorses = RICProductRenderer.findBeanByFieldValue(questionReplies, "QuestionReply", "Name", "QNumberOfHorses").gets("Value");
		String occupancyCd = risk.getBean("Building").gets("OccupancyCd");

		if( !isNewRisk(risk) && applicationDiff != null &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/QuestionReplies/QuestionReply[@Name='QHorsesPersonalUseOnly']/@Value") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/QuestionReplies/QuestionReply[@Name='QNumberOfHorses']/@Value") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@OccupancyCd") )
			return;

		if( !canApproveNumberOfHorses && 
				occupancyCd.equals("Owner") && StringRenderer.isTrue( horsesForPersonalUse ) && StringRenderer.greaterThan(numberOfHorses, "4") ){
			addRiskApproval("Number of horses requires approval", risk);
		}
	}

	private void triggerRiskWithRecVehicleApproval( ModelBean risk ) throws Exception {
		boolean canApproveRiskWithRecVehicle = UserRenderer.hasAuthority(user, "DWApproveRiskWithRecVehicle", todayDt);
		String recVehicleOnLocation = RICProductRenderer.findBeanByFieldValue(risk.getBean("QuestionReplies"), "QuestionReply", "Name", "QRecVehOnLocation").gets("Value");

		if( !canApproveRiskWithRecVehicle && StringRenderer.isTrue( recVehicleOnLocation ) ){
			addRiskApproval("Risk with recreational vehicles requires approval", risk);
		}
	}

	private void triggerRiskWithResidentEmployeeApproval( ModelBean risk ) throws Exception {
		boolean canApproveRiskWithResidentEmployee = UserRenderer.hasAuthority(user, "DWApproveRiskWithResidentEmployee", todayDt);
		String riskWithResidentEmployee = RICProductRenderer.findBeanByFieldValue(risk.getBean("QuestionReplies"), "QuestionReply", "Name", "QAnyResidenceEmployees").gets("Value");

		if( !canApproveRiskWithResidentEmployee && StringRenderer.isTrue( riskWithResidentEmployee ) ){
			addRiskApproval("Risk with a resident employee requires approval", risk);
		}
	}

	//RGUAT-79
	private void triggerDoesntMeetFortifiedMapReqApproval( ModelBean risk ) throws Exception {
		boolean canApproveDoesntMeetFortifiedMapReq = UserRenderer.hasAuthority(user, "DWApproveDoesntMeetFortifiedMapReq", todayDt);
		String doesMeetFortifiedMapReq = risk.getBean("Building").gets("MeetFortifiedMapReq");
		String productVersionIdRef = basicPolicy.gets("ProductVersionIdRef");
		String zip = risk.getBean("Building").getBean("Addr", "AddrTypeCd", "RiskLookupAddr").gets("PostalCode");
		if(zip.length() > 5) { zip = zip.substring(0, 5); }
		String zipLookup = ProductRenderer.getCoderefValue(productVersionIdRef, "UW::underwriting::risk-address-zip::all", zip);

		if(!canApproveDoesntMeetFortifiedMapReq && zipLookup.length() !=0 && StringRenderer.isFalse( doesMeetFortifiedMapReq ) ){
			addRiskApproval("The risk does not meet the fortified map requirements for distance to the coast", risk);
		}
	}

	//RGUAT-266
	private void triggerPropertyMoreThan100MilesApproval( ModelBean risk ) throws Exception {
		boolean canApprovePropertyMoreThan100Miles = UserRenderer.hasAuthority(user, "DWApprovePropertyMoreThan100Miles", todayDt);
		String distanceOfPropertyFromInsured = RICProductRenderer.findBeanByFieldValue(risk.getBean("QuestionReplies"), "QuestionReply", "Name", "MilesToInsuredResidence").gets("Value");

		if( !canApprovePropertyMoreThan100Miles && StringRenderer.greaterThan(distanceOfPropertyFromInsured, "100") ){
			addRiskApproval("The risk is greater than 100 miles from where the insured resides. Requires approval", risk);
		}
	}

	private void triggerFarmingOrBusinessApproval( ModelBean risk ) throws Exception {
		boolean canApproveFarmingOrBusiness = UserRenderer.hasAuthority(user, "DWApproveFarmingOrBusiness", todayDt);
		String isFarmingOrBusiness = RICProductRenderer.findBeanByFieldValue(risk.getBean("QuestionReplies"), "QuestionReply", "Name", "QFarmingorBusiness").gets("Value");

		if( !canApproveFarmingOrBusiness && StringRenderer.isTrue(isFarmingOrBusiness) ){
			addRiskApproval("Business exposure on the premises requires approval", risk);
		}
	}

	private void triggerLocationsNotSameStateApproval( ModelBean risk ) throws Exception {
		boolean canApproveLocationsNotSameState = UserRenderer.hasAuthority(user, "DWApproveLocationsNotSameState", todayDt);
		ModelBean insuredParty = insured.getBean("PartyInfo", "PartyTypeCd", "InsuredParty");
		String insuredMailingAddrStateProvCd = insuredParty.getBean("Addr", "AddrTypeCd", "InsuredMailingAddr").gets("StateProvCd");
		String insuredBillingAddrStateProvCd = insuredParty.getBean("Addr", "AddrTypeCd", "InsuredBillingAddr").gets("StateProvCd");
		String riskLookupAddrStateProvCd = risk.getBean("Building").getBean("Addr", "AddrTypeCd", "RiskLookupAddr").gets("StateProvCd");

		if( !canApproveLocationsNotSameState && 
				( !riskLookupAddrStateProvCd.equals(insuredMailingAddrStateProvCd) || !riskLookupAddrStateProvCd.equals(insuredBillingAddrStateProvCd) )){
			addRiskApproval("The dwelling is not located in the same state as the applicant. Requires approval", risk);
		}
	}

	private void triggerNumberOfChildrenAtDayCareApproval( ModelBean risk ) throws Exception {
		boolean canApproveNumberOfChildrenAtDayCare = UserRenderer.hasAuthority(user, "DWApproveNumberOfChildrenAtDayCare", todayDt);
		String occupancyCd = risk.getBean("Building").gets("OccupancyCd");
		ModelBean questionReplies = risk.getBean("QuestionReplies");
		String isFarmingOrBusiness = RICProductRenderer.findBeanByFieldValue(questionReplies, "QuestionReply", "Name", "QFarmingorBusiness").gets("Value");
		String businessType = RICProductRenderer.findBeanByFieldValue(questionReplies, "QuestionReply", "Name", "QBusinessType").gets("Value");
		String numberOfChildren = RICProductRenderer.findBeanByFieldValue(questionReplies, "QuestionReply", "Name", "QNbrOfChildren").gets("Value");

		if( !canApproveNumberOfChildrenAtDayCare && occupancyCd.equals("Owner") && StringRenderer.isTrue(isFarmingOrBusiness) && 
				businessType.equals("Day Care") && StringRenderer.greaterThan(numberOfChildren, "3") ){
			addRiskApproval("Number of children for the Day Care exposure requires approval", risk);
		}
	}

	private void triggerTrampolineOnPremiseApproval( ModelBean risk ) throws Exception {
		boolean canApproveTrampolineOnPremise = UserRenderer.hasAuthority(user, "DWApproveTrampolineOnPremise", todayDt);
		String isTrampolineOnPremise = RICProductRenderer.findBeanByFieldValue(risk.getBean("QuestionReplies"), "QuestionReply", "Name", "TrampolineonPremises").gets("Value");

		if( !canApproveTrampolineOnPremise && isTrampolineOnPremise.contains("Yes") ){
			addRiskApproval("Trampoline on premises requires approval", risk);
		}
	}

	private void triggerElectricalLessThan100AmpsApproval( ModelBean risk ) throws Exception {
		boolean canApproveElectricalLessThan100Amps = UserRenderer.hasAuthority(user, "DWApproveElectricalLessThan100Amps", todayDt);
		String electricalService = risk.getBean("Building").gets("ElectricalService");

		if( !canApproveElectricalLessThan100Amps && electricalService.equals("Less than 100 Amp Service") ){
			addRiskApproval("Electrical service with less than 100 amps requires approval", risk);
		}
	}

	private void triggerElectricalTypeApproval( ModelBean risk ) throws Exception {
		boolean canApproveElectricalType = UserRenderer.hasAuthority(user, "DWApproveElectricalType", todayDt);
		String subTypeCd = basicPolicy.gets("SubTypeCd");
		String electricalType = risk.getBean("Building").gets("ElectricalType");

		if( !canApproveElectricalType && (subTypeCd.equals("FL2") || subTypeCd.equals("FL3")) && 
				(electricalType.equals("Fuses") || electricalType.equals("Circuit Breaker/Fuses")) ){
			addRiskApproval("Form FL2 or FL3 coverage requested with electrical type selection of \"fuses\" requires approval", risk);
		}
	}

	private void triggerRoofAgeGreaterThan20Approval( ModelBean risk ) throws Exception {
		boolean canApproveRoofAgeGreaterThan20 = UserRenderer.hasAuthority(user, "DWApproveRoofAgeGreaterThan20", todayDt);
		ModelBean building = risk.getBean("Building");
		Integer buildingAge = todayDt.getCalendar().get(Calendar.YEAR) - Integer.parseInt(building.gets("YearBuilt"));
		String roofCd = building.gets("RoofCd");
		String roofReplaced = building.gets("RoofReplaced");
		String roofCds = "Composition Shingle,Wood Shingle,Wood Shake,Composition Roll,Composition Shingle,Composition Built Up,EPDM Membrane,PVC Membrane,Tar and Grave,TPO Membrane,Other";
		Integer roofAge = building.gets("RoofYear").isEmpty() ? null : todayDt.getCalendar().get(Calendar.YEAR) - Integer.parseInt(building.gets("RoofYear"));
		
		if( !canApproveRoofAgeGreaterThan20 && buildingAge != null && buildingAge > 20  && 
				(roofReplaced.equals("No") || (roofAge != null && roofAge > 20 && StringRenderer.in(roofCd, roofCds)) ) ){
			addRiskApproval("Roof age is greater than 20 years old and requires approval", risk);
		}
	}

	private void triggerPlumbingSystemApproval( ModelBean risk ) throws Exception {
		boolean canApprovePlumbingSystem = UserRenderer.hasAuthority(user, "DWApprovePlumbingSystem", todayDt);
		ModelBean building = risk.getBean("Building");
		Integer buildingAge = todayDt.getCalendar().get(Calendar.YEAR) - Integer.parseInt(building.gets("YearBuilt"));
		String plumbingReplaced = building.gets("PlumbingReplaced");
		String plumbingType = building.gets("PlumbingType");
		String plumbingTypes = "Mix of Copper & PVC,Copper,PVC,Polyethylene";

		if( !canApprovePlumbingSystem && buildingAge > 35 && 
				( plumbingReplaced.equals("No") || plumbingReplaced.equals("Partial") ) && StringRenderer.in(plumbingType, plumbingTypes) ){
			addRiskApproval("Plumbing system requires approval", risk);
		}
	}

	private void triggerDayCareLiabilityLimitApproval( ModelBean risk ) throws Exception {
		boolean canApproveDayCareLiabilityLimit = UserRenderer.hasAuthority(user, "DWApproveDayCareLiabilityLimit", todayDt);
		ModelBean questionReplies = risk.getBean("QuestionReplies");
		String isFarmingOrBusiness = RICProductRenderer.findBeanByFieldValue(questionReplies, "QuestionReply", "Name", "QFarmingorBusiness").gets("Value");
		String businessType = RICProductRenderer.findBeanByFieldValue(questionReplies, "QuestionReply", "Name", "QBusinessType").gets("Value");
		String covLLimit = risk.getBean("Building").gets("CovLLimit");

		if( !canApproveDayCareLiabilityLimit && 
				StringRenderer.isTrue(isFarmingOrBusiness) && businessType.equals("Day Care") && StringRenderer.greaterThanEqual(covLLimit, "500000")){
			addRiskApproval("Liability limit with a day care exposure requires approval", risk);
		}
	}

	private void triggerNaturalGasWellApproval( ModelBean risk ) throws Exception {
		boolean canApproveNaturalGasWell = UserRenderer.hasAuthority(user, "DWApproveNaturalGasWell", todayDt);
		ModelBean questionReplies = risk.getBean("QuestionReplies");
		String naturalGasWell = RICProductRenderer.findBeanByFieldValue(questionReplies, "QuestionReply", "Name", "QNaturalGasWell").gets("Value");
		String isMaintainedByEnergyCompany = RICProductRenderer.findBeanByFieldValue(questionReplies, "QuestionReply", "Name", "QNaturalGasMaintenance").gets("Value");

		if( !canApproveNaturalGasWell && 
				( naturalGasWell.equals("Hydraulic Fracturing") || naturalGasWell.equals("Hydraulic Fracturing and Low Pressure") || 
						StringRenderer.isFalse(isMaintainedByEnergyCompany) ) ){
			addRiskApproval("Natural gas well requires approval", risk);
		}
	}

	private void triggerDwellingStyleCovALimitGreaterThan150k( ModelBean risk ) throws Exception {
		boolean canApproveDwellingStyleCovALimitGreaterThan150k = UserRenderer.hasAuthority(user, "DWApproveDwellingStyleCovALimitGreaterThan150k", todayDt);
		ModelBean building = risk.getBean("Building");
		String dwellingStyle = building.gets("DwellingStyle");
		String covALimit = building.gets("CovALimit");

		if( !canApproveDwellingStyleCovALimitGreaterThan150k && dwellingStyle.equals("Mobile home") &&
				StringRenderer.greaterThan(covALimit, "150000") ){
			addRiskApproval("Coverage A amount requires approval", risk);
		}
	}

	private void triggerDwellingStyleCovALimitGreaterThan500k( ModelBean risk ) throws Exception {
		boolean canApproveDwellingStyleCovALimitGreaterThan500k = UserRenderer.hasAuthority(user, "DWApproveDwellingStyleCovALimitGreaterThan500k", todayDt);
		ModelBean building = risk.getBean("Building");
		String dwellingStyle = building.gets("DwellingStyle");
		String covALimit = building.gets("CovALimit");

		if( !canApproveDwellingStyleCovALimitGreaterThan500k && 
				( dwellingStyle.equals("Dwelling") || dwellingStyle.equals("Rowhouse") || dwellingStyle.equals("Townhouse") ) &&
				StringRenderer.greaterThan(covALimit, "500000") ){
			addRiskApproval("Coverage A amount requires approval", risk);
		}
	}

	private void triggerVacantDwellingApproval( ModelBean risk ) throws Exception {
		boolean canApproveVacantDwelling = UserRenderer.hasAuthority(user, "DWApproveVacantDwelling", todayDt);
		ModelBean building = risk.getBean("Building");

		if( !isNewRisk(risk) && applicationDiff != null &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@Vacancy") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@Residence") )
			return;

		if( !canApproveVacantDwelling && 
				(building.gets("Vacancy").equals("Yes") || building.gets("Residence").equals("Vacant Under Renovation") || building.gets("Residence").equals("Vacant Ready for Occupancy")) ){
			addRiskApproval("Vacant dwelling requires approval", risk);
		}
	}

	private void triggerAdditionalHazardsApproval( ModelBean risk ) throws Exception {
		boolean canApproveAdditionalHazards = UserRenderer.hasAuthority(user, "DWApproveAdditionalHazards", todayDt);
		ModelBean building = risk.getBean("Building");
		String isHeatProducingEquip = building.gets("HeatProducingEquip");
		String isElectricalEquip = building.gets("ElectricalEquip");
		String isMaintAndHousekeeping = building.gets("MaintAndHousekeeping");
		String isInferiorConstruction = building.gets("Construction");
		String hasExposure = building.gets("Exposure");

		if( !isNewRisk(risk) && applicationDiff != null &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@HeatProducingEquip") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@ElectricalEquip") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@MaintAndHousekeeping") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@Construction") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@Exposure") )
			return;

		if( !canApproveAdditionalHazards &&
				(StringRenderer.isTrue(isHeatProducingEquip) || StringRenderer.isTrue(isElectricalEquip) || 
						StringRenderer.isTrue(isMaintAndHousekeeping) || StringRenderer.isTrue(isInferiorConstruction) || StringRenderer.isTrue(hasExposure))){
			addRiskApproval("Additional hazards present require approval", risk);
		}
	}

	private void trigger1MCovLLimitApproval( ModelBean risk ) throws Exception {
		if( dwellingQuestionReplies == null )
			return;

		boolean canApprove1MCovLLimit = UserRenderer.hasAuthority(user, "DWApprove1MCovLLimit", todayDt);
		String dwProgramTypeCd = basicPolicy.gets("DwProgramTypeCd");
		String hasLiabilityLimitAtRIC = dwellingQuestionReplies.getBean("QuestionReply", "Name", "OtherLimitWithRIC") != null ?  
				dwellingQuestionReplies.getBean("QuestionReply", "Name", "OtherLimitWithRIC").gets("Value") : "";
				String covLLimit = risk.getBean("Building").gets("CovLLimit");

				if( !isNewRisk(risk) && applicationDiff != null &&
						!applicationDiff.hasFieldModifiedByXPath("//BasicPolicy/@DwProgramTypeCd") &&
						!applicationDiff.hasFieldModifiedByXPath("//QuestionReplies/QuestionReply[@Name='OtherLimitWithRIC']/@Value") )
					return;

				if( !canApprove1MCovLLimit && 
						!dwProgramTypeCd.equals("STR") && StringRenderer.equal(covLLimit, "1000000") && 
						losses.length > 0 && StringRenderer.isTrue(hasLiabilityLimitAtRIC) ){
					addRiskApproval("Liability limit requires approval", risk);
				}
	}

	private void triggerLiabilityLimitAtRICApproval( ModelBean risk ) throws Exception {
		if( dwellingQuestionReplies == null )
			return;

		boolean canApproveLiabilityLimitAtRIC = UserRenderer.hasAuthority(user, "DWApproveLiabilityLimitAtRIC", todayDt);
		String hasLiabilityLimitAtRIC = dwellingQuestionReplies.getBean("QuestionReply", "Name", "OtherLimitWithRIC") != null ?  
				dwellingQuestionReplies.getBean("QuestionReply", "Name", "OtherLimitWithRIC").gets("Value") : "";

				if( !canApproveLiabilityLimitAtRIC && StringRenderer.isFalse(hasLiabilityLimitAtRIC) ){
					addRiskApproval("Liability limit requires underwriting approval", risk);
				}
	}

	private void triggerUnderConstructionApproval( ModelBean risk ) throws Exception {
		boolean canApproveUnderConstruction = UserRenderer.hasAuthority(user, "DWApproveUnderConstruction", todayDt);

		if( !isNewRisk(risk) && applicationDiff != null && !applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@Residence"))
			return;

		if( !canApproveUnderConstruction && risk.getBean("Building").gets("Residence").equals("Under Construction (New)") ){
			addRiskApproval("Approval required for Residence type selection of Under Construction", risk);
		}
	}

	private void triggerDistanceOutdoorWoodFurnaceApproval( ModelBean risk ) throws Exception {
		boolean canApproveDistanceOutdoorWoodFurnace = UserRenderer.hasAuthority(user, "DWApproveDistanceOutdoorWoodFurnace", todayDt);
		ModelBean building = risk.getBean("Building");
		String primaryHeating = building.gets("Heating");
		String secondaryHeating = building.gets("SecondaryHeat");
		String distanceOutdoorWoodFurnace = building.gets("DistanceOutdoorWoodFurnace");

		if( !canApproveDistanceOutdoorWoodFurnace && 
				( primaryHeating.equals("Outdoor Wood Furnace") || secondaryHeating.equals("Outdoor Wood Furnace") ) && StringRenderer.lessThanEqual(distanceOutdoorWoodFurnace, "50") ){
			addRiskApproval("The outdoor wood furnace is within 50 feet of the dwelling, and requires approval", risk);
		}
	}

	private void triggerFL2OrFl3HeatingNotUpdatedApproval( ModelBean risk ) throws Exception {
		boolean canApproveFL2OrFl3HeatingNotUpdated = UserRenderer.hasAuthority(user, "DWApproveFL2OrFl3HeatingNotUpdated", todayDt);
		String subTypeCd = basicPolicy.gets("SubTypeCd");
		ModelBean building = risk.getBean("Building");
		Integer buildingAge = todayDt.getCalendar().get(Calendar.YEAR) - Integer.parseInt(building.gets("YearBuilt"));
		String primaryHeating = building.gets("Heating");
		String heatingReplaced = building.gets("HeatingReplaced");
		Integer renovationAge = building.gets("HeatingYear").isEmpty() ? null : todayDt.getCalendar().get(Calendar.YEAR) - Integer.parseInt(building.gets("HeatingYear"));

		if( !canApproveFL2OrFl3HeatingNotUpdated && (subTypeCd.equals("FL2") || subTypeCd.equals("FL3")) ) {

			if( !primaryHeating.equals("Electric Forced Air (Includes Heat Pump)") && !primaryHeating.equals("Electric Radiant") && 
					buildingAge > 35 && heatingReplaced.equals("No")){
				addRiskApproval("Heating system has not been sufficiently replaced or updated in the last 35 years and requires approval", risk);
				return;
			}

			if( (heatingReplaced.equals("Complete") || heatingReplaced.equals("Burner Unit Only") ) && renovationAge != null && renovationAge > 35 ){
				addRiskApproval("Heating system has not been sufficiently replaced or updated in the last 35 years and requires approval", risk);
				return;
			}
		}
	}

	private void triggerFL1HeatingNotUpdatedApproval( ModelBean risk ) throws Exception {
		boolean canApproveFL1HeatingNotUpdated = UserRenderer.hasAuthority(user, "DWApproveFL1HeatingNotUpdated", todayDt);
		String subTypeCd = basicPolicy.gets("SubTypeCd");
		ModelBean building = risk.getBean("Building");
		Integer buildingAge = todayDt.getCalendar().get(Calendar.YEAR) - Integer.parseInt(building.gets("YearBuilt"));
		String heatingReplaced = building.gets("HeatingReplaced");
		Integer renovationAge = building.gets("HeatingYear").isEmpty() ? null : todayDt.getCalendar().get(Calendar.YEAR) - Integer.parseInt(building.gets("HeatingYear"));

		if( !canApproveFL1HeatingNotUpdated && subTypeCd.equals("FL1") ) {

			if( buildingAge > 35 && heatingReplaced.equals("No")){
				addRiskApproval( "Furnace is older than 35 years old or the burner unit has not been replaced within the last 20 years and requires approval", risk);
				return;
			}

			if( renovationAge != null && 
					((heatingReplaced.equals("Complete") && renovationAge > 35) || (heatingReplaced.equals("Burner Unit Only")  && renovationAge > 20 ) )){
				addRiskApproval( "Furnace is older than 35 years old or the burner unit has not been replaced within the last 20 years and requires approval", risk);
			}
		}
	}

	private void triggerFailedRiskAddressVerificationApproval( ModelBean risk ) throws Exception {
		boolean canApproveFailedRiskAddressVerification = UserRenderer.hasAuthority(user, "DWApproveFailedRiskAddressVerification", todayDt);
		ModelBean riskLookupAddr = risk.getBean("Building").getBean("Addr", "AddrTypeCd", "RiskLookupAddr");

		if( !canApproveFailedRiskAddressVerification && !riskLookupAddr.gets("VerificationMsg").isEmpty() ){
			addRiskApproval("Address verification service has failed.", risk);
		}
	}

	private void triggerPropertyNotWinterizedApproval( ModelBean risk ) throws Exception {
		boolean canApprovePropertyNotWinterized = UserRenderer.hasAuthority(user, "DWApprovePropertyNotWinterized", todayDt);
		String isPropertyWinterized = RICProductRenderer.findBeanByFieldValue(risk.getBean("QuestionReplies"), "QuestionReply", "Name", "QSTRWinterized").gets("Value");
		
		if( !canApprovePropertyNotWinterized && isPropertyWinterized.equals("None") ){
			addRiskApproval("Property that is not winterized requires approval", risk);
		}
	}

	private void triggerFL200NotOnPermanentFoundationApproval( ModelBean risk ) throws Exception {
		boolean canApproveFL200NotOnPermanentFoundation = UserRenderer.hasAuthority(user, "DWApproveFL200NotOnPermanentFoundation", todayDt);
		String subTypeCd = basicPolicy.gets("SubTypeCd");
		ModelBean building = risk.getBean("Building");
		String dwellingStyle = building.gets("DwellingStyle");
		String architecturalStyle = building.gets("ArchitecturalStyle");
		String foundationType = building.gets("MobileHomeFoundationType");
		String rctMobileHomeCov = building.gets("RCTMobileHomeCov");

		if( !isNewRisk(risk) && applicationDiff != null &&
				!applicationDiff.hasFieldModifiedByXPath("//BasicPolicy/@SubTypeCd") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@YearBuilt") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@DwellingStyle") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@ArchitecturalStyle") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@MobileHomeFoundationType") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@RCTMobileHomeCov") )
			return;

		if( !canApproveFL200NotOnPermanentFoundation ) {
			if(dwellingStyle.equals("Mobile home") &&
					(architecturalStyle.equals("Doublewide") || architecturalStyle.equals("Triplewide")) &&
					(subTypeCd.equals("FL2") || subTypeCd.equals("FL3")) &&
					!foundationType.equals("Continuous Permanent Masonry") &&
					StringRenderer.lessThan(building.gets("YearBuilt"), "1994") &&
					rctMobileHomeCov.equals("Yes")) {
				addRiskApproval("Mobile home built prior to 1994 not on a permanent foundation with the FL200 requires approval", risk);
			}
		}
	}

	private void triggerECOrVMMChangesApproval( ModelBean risk ) throws Exception {
		boolean canApproveECOrVMMChanges = UserRenderer.hasAuthority(user, "DWApproveECOrVMMChanges", todayDt);
		BeanDiff buildingDiff = applicationDiff.getBean("Line").findBeanByFieldValue("Risk", "Id", risk.getId(), true, true).getBean("Building");
		ModelBean newBuilding = buildingDiff.getNewBean();

		if( !canApproveECOrVMMChanges && 
				basicPolicy.gets("SubTypeCd").equals("FL1") &&
				( ( buildingDiff.hasFieldModified("ExtendedCoverageInd") && newBuilding.gets("ExtendedCoverageInd").equals("Yes") ) ||
						( buildingDiff.hasFieldModified("VandalismMaliciousMischiefInd") && newBuilding.gets("VandalismMaliciousMischiefInd").equals("Yes") ) ) ){
			addRiskApproval("Adding Extended Coverage or Vandalism & Malicious Mischief coverage requires approval", risk);
		}
	}

	private void triggerAddExperienceGroupApproval(ModelBean risk ) throws Exception {
		boolean canApproveAddExperienceGroup = UserRenderer.hasAuthority(user, "DWApproveAddExperienceGroup", todayDt);
		BeanDiff buildingDiff = applicationDiff.getBean("Line").findBeanByFieldValue("Risk", "Id", risk.getId(), true, true).getBean("Building");
		ModelBean newBuilding = buildingDiff.getNewBean();

		if( !canApproveAddExperienceGroup &&
				buildingDiff.hasFieldModified("ExperienceGroup") && newBuilding.gets("ExperienceGroup").equals("Yes") ){
			addRiskApproval("Addition of the Experience Group discount requires approval", risk);
		}
	}

	private void triggerDwellingStyleChangeApproval( ModelBean risk ) throws Exception {
		boolean canApproveDwellingStyleChange = UserRenderer.hasAuthority(user, "DWApproveDwellingStyleChange", todayDt);

		if( !canApproveDwellingStyleChange && !isNewRisk(risk) &&
				applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@DwellingStyle")){
			addRiskApproval("A change to the Dwelling Style requires approval", risk);
		}
	}

	private void triggerPolicyFormChangeApproval() throws Exception {
		boolean canApprovePolicyFormChange = UserRenderer.hasAuthority(user, "DWApprovePolicyFormChange", todayDt);

		if( !canApprovePolicyFormChange && applicationDiff != null && applicationDiff.hasFieldModifiedByXPath("//Line/BasicPolicy/@SubTypeCd") ) {
			String msg = "A change to the Policy Form requires approval";
			ValidationRenderer.addValidationError(application, "Approval", "APPROVAL", msg);
		}
	}

	private void triggerUnoccupiedBuildingApproval( ModelBean risk ) throws Exception {
		boolean canApproveUnoccupiedBuilding = UserRenderer.hasAuthority(user, "DWApproveUnoccupiedBuilding", todayDt);

		if( !isNewRisk(risk) && applicationDiff != null && !applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@Residence"))
			return;

		if( !canApproveUnoccupiedBuilding && risk.getBean("Building").gets("Residence").equals("Unoccupied") ){
			addRiskApproval("Approval required for Residence type selection of Unoccupied", risk);
		}
	}

	private void trigger6MonthTermApproval() throws Exception {
		boolean canApprove6MonthTerm = UserRenderer.hasAuthority(user, "DWApprove6MonthTerm", todayDt);

		if( applicationDiff != null && !applicationDiff.hasFieldModifiedByXPath("//BasicPolicy/@RenewalTermCd") )
			return;

		if( !canApprove6MonthTerm && basicPolicy.gets("RenewalTermCd").equals("6 months") ){
			String msg = "Approval required for 6 month policy";
			ValidationRenderer.addValidationError(application, "Approval", "APPROVAL", msg);
		}
	}

	private void triggerForceManualRenewalApproval() throws Exception {
		boolean canApproveForceManualRenewal = UserRenderer.hasAuthority(user, "DWApproveForceManualRenewal", todayDt);

		if( !canApproveForceManualRenewal && StringRenderer.isTrue(basicPolicy.gets("ManualRenewalInd")) ){
			String msg = "Approval required for renewal - policy is set to manually renew";
			ValidationRenderer.addValidationError(application, "Approval", "APPROVAL", msg);
		}
	}

	private void triggerNewWaterLossApproval() throws Exception {
		boolean canApproveNewWaterLoss = UserRenderer.hasAuthority(user, "DWApproveNewWaterLoss", todayDt);
		String[] lossTypes = {"Accidental Discharge Or Overflow Of Water Or Steam", "Flood",
				"Fungus, Wet Rot, Dry Rot and Bacteria Falling Objects", "Seepage Or Leakage",
				"Sewer Backup", "Water Damage Backup", "Water, Liquids, Powder, Molten Material"};
		ArrayList<String> lossTypesList = new ArrayList<String>(Arrays.asList(lossTypes));

		if( !canApproveNewWaterLoss ){
			for( ModelBean loss : losses ) {
				if (isNewLoss(loss) && StringRenderer.contains(loss.gets("LossCauseCd"), lossTypesList) && StringRenderer.greaterThan(loss.gets("PaidAmt"), "0")) {
					String msg = "Approval required for water loss";
					ValidationRenderer.addValidationError(application, "Approval", "APPROVAL", msg);
					return;
				}
			}
		}
	}

	private void triggerLossesWithinPast3Yrs() throws Exception {
		boolean canApproveLossesWithinPast3Yrs = UserRenderer.hasAuthority(user, "DWApproveLossesWithinPast3Yrs", todayDt);
		int numberOfPriorPaidLoss = 0;

		if( !canApproveLossesWithinPast3Yrs ){
			for( ModelBean loss : losses ) {
				if (StringRenderer.greaterThan(loss.gets("PaidAmt"), "0") && !StringRenderer.isTrue(loss.gets("IgnoreInd"))  )
					numberOfPriorPaidLoss++;
			}

			if( numberOfPriorPaidLoss >= 2 ){
				String msg = "Approval required for number of losses";
				ValidationRenderer.addValidationError( application,"Approval","APPROVAL", msg );
			}
		}
	}

	private void triggerNewLiabilityLossApproval() throws Exception {
		boolean canApproveNewLiabilityLoss = UserRenderer.hasAuthority(user, "DWApproveNewLiabilityLoss", todayDt);

		if( !canApproveNewLiabilityLoss ){
			for( ModelBean loss : losses ) {
				if (isNewLoss(loss) && loss.gets("LossCauseCd").equals("Liability (BI, PD, Med-Pay)") && StringRenderer.greaterThan(loss.gets("PaidAmt"), "0")) {
					String msg = "Approval required for liability loss";
					ValidationRenderer.addValidationError(application, "Approval", "APPROVAL", msg);
					return;
				}
			}
		}
	}

	private void triggerNoFenceSwimmingPoolApproval( ModelBean risk ) throws Exception {
		boolean canApproveNoFenceSwimmingPool = UserRenderer.hasAuthority(user, "DWApproveNoFenceSwimmingPool", todayDt);
		ModelBean building = risk.getBean("Building");

		if( !isNewRisk(risk) && applicationDiff != null && !applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@SwimPoolSurcharge") )
			return;

		if( !canApproveNoFenceSwimmingPool &&
				( building.gets("SwimPoolSurcharge").equals("In Ground, not fully fenced with self locking gate") ||
						building.gets("SwimPoolSurcharge").equals("Above Ground, without removable ladder or deck w/ self-locking gate") ) ){
			addRiskApproval("Swimming pool without removable ladder or self locking gate", risk);
		}
	}

	private void triggerDivingOrSlideApproval( ModelBean risk ) throws Exception {
		boolean canApproveDivingOrSlide = UserRenderer.hasAuthority(user, "DWApproveDivingOrSlide", todayDt);
		String isDivingBoardOrSlidePresent = risk.getBean("Building").gets("DivingBoardOrSlidePresent");

		if( !isNewRisk(risk) && applicationDiff != null &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@DivingBoardOrSlidePresent") )
			return;

		if( !canApproveDivingOrSlide && !isDivingBoardOrSlidePresent.isEmpty() && !isDivingBoardOrSlidePresent.equals("No") ){
			addRiskApproval("Swimming pool with diving board or slide", risk);
		}
	}

	private void triggerCondoCovCLimitOver150KApproval( ModelBean risk ) throws Exception {
		boolean canApproveCondoCovCLimitOver150K = UserRenderer.hasAuthority(user, "DWCondoCovCLimitOver150k", todayDt);
		ModelBean building = risk.getBean("Building");
		String dwellingStyle = building.gets("DwellingStyle");
		String covCLimit = building.gets("CovCLimit");

		if( !isNewRisk(risk) && applicationDiff != null &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@DwellingStyle") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@CovCLimit") )
			return;

		if( !canApproveCondoCovCLimitOver150K &&
				dwellingStyle.equals("Condominium/Co-operative") && StringRenderer.greaterThan(covCLimit, "150000") ){
			addRiskApproval("Coverage C amount requires approval", risk);
		}
	}

	private void triggerEntityTypeCdApproval() throws Exception {
		boolean canApproveEntityTypeCd = UserRenderer.hasAuthority(user,"DWApproveEntityTypeCdChange", todayDt);

		if( !canApproveEntityTypeCd && applicationDiff != null && applicationDiff.hasFieldModifiedByXPath("//Insured/@EntityTypeCd") ){
			ValidationRenderer.addValidationError( application,"Approval","APPROVAL", "Entity Type selection has changed and requires approval" );
		}
	}

	private void triggerDwProgramTypeCdChangeApproval() throws Exception {
		boolean canApproveDwProgramTypeCdChange = UserRenderer.hasAuthority(user, "DWApproveDwProgTypeCdChange", todayDt);

		if( !canApproveDwProgramTypeCdChange && applicationDiff != null && applicationDiff.hasFieldModifiedByXPath("//BasicPolicy/@DwProgramTypeCd") ){
			ValidationRenderer.addValidationError( application,"Approval","APPROVAL", "Dwelling properties program change requires approval" );
		}
	}

	private void triggerNoneLiabilityTypeChangeApproval( ModelBean risk ) throws Exception {
		boolean canApproveNoneLiabilityTypeChange = UserRenderer.hasAuthority(user, "DWApproveNoneLiabTypeChange", todayDt);

		if( isNewRisk(risk) || (applicationDiff != null && !applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@LiabilityCoverageType")))
			return;

		BeanDiff buildingDiff = applicationDiff.getBean("Line").findBeanByFieldValue("Risk", "Id", risk.getId(), true, true).getBean("Building");
		String oldLiabType = buildingDiff.getOldValue("LiabilityCoverageType");
		String newLiabType = buildingDiff.getNewValue("LiabilityCoverageType");

		if (!canApproveNoneLiabilityTypeChange && oldLiabType.equals("None") && !newLiabType.equals("None")) {
			addRiskApproval("Liability coverage is being added and requires approval", risk);
		}
	}

	private void triggerGLLiabilityTypeChangeApproval( ModelBean risk ) throws Exception {
		boolean canApproveGLLiabilityTypeChange = UserRenderer.hasAuthority(user, "DWApproveGLLiabTypeChange", todayDt);

		if( isNewRisk(risk) || (applicationDiff != null && !applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@LiabilityCoverageType")))
			return;

		BeanDiff buildingDiff = applicationDiff.getBean("Line").findBeanByFieldValue("Risk", "Id", risk.getId(), true, true).getBean("Building");
		String oldLiabType = buildingDiff.getOldValue("LiabilityCoverageType");
		String newLiabType = buildingDiff.getNewValue("LiabilityCoverageType");

		if (!canApproveGLLiabilityTypeChange && oldLiabType.equals("GL-600") && newLiabType.equals("GL-1")) {
			addRiskApproval("Liability coverage change from GL 600 to GL 1 requires approval", risk);
		}
	}

	private void triggerRiskAddedApproval( ModelBean risk ) throws Exception {
		boolean canApproveRiskAdded = UserRenderer.hasAuthority(user, "DWApproveRiskAdded", todayDt);

		if( !canApproveRiskAdded && isNewRisk(risk) ){
			addRiskApproval("Additional risk added requires approval", risk);
		}
	}

	private void triggerOccupancyTypeChangeApproval( ModelBean risk ) throws Exception {
		boolean canApproveOccupancyTypeChange = UserRenderer.hasAuthority(user, "DWApproveOccupancyTypeChange", todayDt);

		if( !canApproveOccupancyTypeChange &&
				applicationDiff != null && applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@OccupancyCd") ){
			addRiskApproval("Occupancy type change requires approval", risk);
		}
	}

	private void triggerResidenceChangeApproval( ModelBean risk ) throws Exception {
		boolean canApproveResidenceChange = UserRenderer.hasAuthority(user, "DWApproveResidenceChange", todayDt);

		if( !canApproveResidenceChange &&
				applicationDiff != null && applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@Residence") ){
			addRiskApproval("Residence change requires approval", risk);
		}
	}

	private void triggerProtectionClassChangeApproval( ModelBean risk ) throws Exception {
		if( isNewRisk(risk) || (applicationDiff != null && !applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@ProtectionClass")))
			return;

		boolean canApproveProtectionClassChange = UserRenderer.hasAuthority(user, "DWApproveProtectClassChange", todayDt);
		BeanDiff buildingDiff = applicationDiff.getBean("Line").findBeanByFieldValue("Risk", "Id", risk.getId(), true, true).getBean("Building");
		String oldProtectionClass = buildingDiff.getOldValue("ProtectionClass");
		String newProtectionClass = buildingDiff.getNewValue("ProtectionClass");

		if( !canApproveProtectionClassChange &&
				(newProtectionClass.equals("Protected") || ( newProtectionClass.equals("Partially Protected") && oldProtectionClass.equals("Unprotected")) ) ){
			addRiskApproval("Protection class change requires approval",risk);
		}
	}

	private void triggerYearBuiltChangeApproval( ModelBean risk ) throws Exception {
		boolean canApproveYearBuiltChange = UserRenderer.hasAuthority(user, "DWApproveYearBuiltChange", todayDt);

		if( !canApproveYearBuiltChange &&
				applicationDiff != null && applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@YearBuilt") ){
			addRiskApproval("Year built change requires approval",risk);
		}
	}

	private void triggerConstructionCdChangeApproval( ModelBean risk ) throws Exception {
		boolean canApproveConstructionCdChange = UserRenderer.hasAuthority(user, "DWApproveConstructionCdChange", todayDt);

		if( !canApproveConstructionCdChange &&
				applicationDiff != null && applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@ConstructionCd") ){
			addRiskApproval("Construction type change requires approval",risk);
		}
	}

	private void triggerNumberOfUnitsChangeApproval( ModelBean risk ) throws Exception {
		boolean canApproveNumberOfUnitsChange = UserRenderer.hasAuthority(user, "DWApproveNumberOfUnitsChange", todayDt);

		if( !canApproveNumberOfUnitsChange &&
				applicationDiff != null && applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@Units") ) {
			addRiskApproval("Number of units in fire division change requires approval",risk);
		}
	}

	private void triggerNumFamiliesChangeApproval( ModelBean risk ) throws Exception {
		boolean canApproveNumFamiliesChange = UserRenderer.hasAuthority(user, "DWApproveNumFamiliesChange", todayDt);

		if( !canApproveNumFamiliesChange &&
				applicationDiff != null && applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@NumFamilies") ) {
			addRiskApproval("Number of families change requires approval",risk);
		}
	}

	private void triggerCovAIncreaseApproval( ModelBean risk ) throws Exception {
		if( isNewRisk(risk) || (applicationDiff != null && !applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@CovALimit")))
			return;

		boolean canApproveCovAIncrease = UserRenderer.hasAuthority(user, "DWApproveCovAIncrease", todayDt);
		BeanDiff buildingDiff = applicationDiff.getBean("Line").findBeanByFieldValue("Risk", "Id", risk.getId(), true, true).getBean("Building");
		String oldCovALimit = buildingDiff.getOldValue("CovALimit");
		String newCovALimit = buildingDiff.getNewValue("CovALimit");
		String covALimit = StringRenderer.multiply(oldCovALimit, "1.20", 2);

		if (!canApproveCovAIncrease && StringRenderer.greaterThan(newCovALimit, covALimit)) {
			addRiskApproval("Coverage A increase requires approval", risk);
		}
	}

	private void triggerCovADecreaseApproval( ModelBean risk ) throws Exception {
		if( isNewRisk(risk) || (applicationDiff != null && !applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@CovALimit")))
			return;

		boolean canApproveCovADecrease = UserRenderer.hasAuthority(user, "DWApproveCovADecrease", todayDt);
		BeanDiff buildingDiff = applicationDiff.getBean("Line").findBeanByFieldValue("Risk", "Id", risk.getId(), true, true).getBean("Building");
		String oldCovALimit = buildingDiff.getOldValue("CovALimit");
		String newCovALimit = buildingDiff.getNewValue("CovALimit");

		if (!canApproveCovADecrease && StringRenderer.lessThan(newCovALimit, oldCovALimit)){
			addRiskApproval("Coverage A decrease requires approval", risk);
		}
	}

	private void triggerCovCIncreaseApproval( ModelBean risk ) throws Exception {
		boolean canApproveCovCIncrease = UserRenderer.hasAuthority(user, "DWApproveCovCIncrease", todayDt);

		if( isNewRisk(risk) || ( applicationDiff != null &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@DwellingStyle") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@CovALimit") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@CovCLimit") ) )
			return;

		ModelBean building = risk.getBean("Building");
		String dwellingStyle = building.gets("DwellingStyle");
		String covCLimit = building.gets("CovCLimit");
		String covALimit50Percent = StringRenderer.multiply(building.gets("CovALimit"), "0.50", 2);

		if (!canApproveCovCIncrease && !dwellingStyle.equals("Condominium/Co-operative") && StringRenderer.greaterThan(covCLimit, covALimit50Percent)) {
			addRiskApproval("Coverage C increase requires approval", risk);
		}
	}

	private void triggerPolicyDeductibleDecreaseApproval( ModelBean risk ) throws Exception {
		if( !isNewRisk(risk) && applicationDiff != null &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@AllPerilDed") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@WindHailDed") )
			return;

		boolean canApprovePolicyDeductibleDecrease = UserRenderer.hasAuthority(user, "DWApprovePolicyDedDecrease", todayDt);
		BeanDiff buildingDiff = applicationDiff.getBean("Line").findBeanByFieldValue("Risk", "Id", risk.getId(), true, true).getBean("Building");
		String oldAllPerilDed = buildingDiff.getOldValue("AllPerilDed");
		String newAllPerilDed = buildingDiff.getNewValue("AllPerilDed");
		String oldWindHailDed = buildingDiff.getOldValue("WindHailDed");
		String newWindHailDed = buildingDiff.getNewValue("WindHailDed");

		if( !canApprovePolicyDeductibleDecrease && (
				newWindHailDed.equals("None") ||
				StringRenderer.lessThan(newAllPerilDed, oldAllPerilDed) ||
				( StringRenderer.lessThan(newWindHailDed, "10") && !oldWindHailDed.equals("None") && StringRenderer.greaterThan(oldWindHailDed, "10") ) ||
				( StringRenderer.greaterThan(newWindHailDed, "10") && !oldWindHailDed.equals("None") && StringRenderer.lessThan(oldWindHailDed, "10") ) ||
				( !oldWindHailDed.equals("None") && StringRenderer.lessThan(newWindHailDed, oldWindHailDed) ) ) ){
			addRiskApproval("Policy deductible decrease requires approval", risk);
		}
	}

	private void triggerSolidFuelHeatChangeApproval( ModelBean risk ) throws Exception {
		if( isNewRisk(risk) || (applicationDiff != null &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@SolidFuelHeatType") ) )
			return;

		boolean canApproveSolidFuelHeatChange = UserRenderer.hasAuthority(user, "DWApproveSolidFuelHeatChange", todayDt);
		BeanDiff buildingDiff = applicationDiff.getBean("Line").findBeanByFieldValue("Risk", "Id", risk.getId(), true, true).getBean("Building");
		String oldSolidFuelHeatType = buildingDiff.getOldValue("SolidFuelHeatType");

		if( !canApproveSolidFuelHeatChange && oldSolidFuelHeatType.equals("None") ){
			addRiskApproval("Solid fuel heat change requires approval", risk);
		}
	}

	private void triggerLastProfInspectionDtChangeApproval( ModelBean risk ) throws Exception {
		boolean canApproveLastProfInspectDtChange = UserRenderer.hasAuthority(user, "DWApproveLastInspectDtChange", todayDt);

		if( !canApproveLastProfInspectDtChange && applicationDiff != null &&
				applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@LastProfInspectionDt") ){
			addRiskApproval("Date of last professional inspection change requires approval", risk);
		}
	}

	private void triggerFuelStorageTankChangeApproval( ModelBean risk ) throws Exception {
		if( isNewRisk(risk) || (applicationDiff != null &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@FuelTankLocation")))
			return;

		boolean canApproveFuelStorageTankChange = UserRenderer.hasAuthority(user, "DWApproveFuelTankChange", todayDt);

		if( !canApproveFuelStorageTankChange && risk.getBean("Building").gets("FuelTankLocation").equals("Outdoors - Below ground") ){
			addRiskApproval("Fuel storage tank change requires approval", risk);
		}
	}

	private void triggerNewDogApproval( ModelBean risk ) throws Exception {
		if( isNewRisk(risk) || (applicationDiff != null &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/QuestionReplies/QuestionReply[@Name='QAnyDogs']/@Value")) )
			return;

		boolean canApproveNewDog = UserRenderer.hasAuthority(user, "DWApproveNewDog", todayDt);
		BeanDiff questionReplyDiff = applicationDiff.getBean("Line").findBeanByFieldValue("Risk", "Id", risk.getId(), true, true).getBean("QuestionReplies").findBeanByFieldValue("QuestionReply", "Name", "QAnyDogs");

		if( !canApproveNewDog && questionReplyDiff != null && StringRenderer.isTrue(questionReplyDiff.getNewValue("Value")) ){
			addRiskApproval("Review dog breed", risk);
		}
	}

	private void triggerGL72AddedApproval( ModelBean risk ) throws Exception {
		boolean canApproveGL72Added = UserRenderer.hasAuthority(user, "DWApproveGL72Added", todayDt);
		ModelBean coverage = risk.getBean("Coverage", "CoverageCd", "AIORH" );

		if( !canApproveGL72Added && isNewCoverage(coverage) ){
			addRiskApproval("GL 72 requires approval", risk);
		}
	}

	private void triggerGL73AddedApproval( ModelBean risk ) throws Exception {
		boolean canApproveGL73Added = UserRenderer.hasAuthority(user, "DWApproveGL73Added", todayDt);
		ModelBean coverage = risk.getBean("Coverage", "CoverageCd", "ARRO" );

		if( !canApproveGL73Added && isNewCoverage(coverage) ){
			addRiskApproval("GL 73 requires approval", risk);
		}
	}

	private void triggerGL80AddedApproval( ModelBean risk ) throws Exception {
		boolean canApproveGL80Added = UserRenderer.hasAuthority(user, "DWApproveGL80Added", todayDt);
		ModelBean coverage = risk.getBean("Coverage", "CoverageCd", "OPPL" );

		if( !canApproveGL80Added && isNewCoverage(coverage) ){
			addRiskApproval("GL 80 requires approval", risk);
		}
	}

	private void triggerNewDomesticEmployeesApproval( ModelBean risk ) throws Exception {
		if( !isNewRisk(risk) && applicationDiff != null &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@NumDomesticEmployees") )
			return;

		boolean canApproveNewDomesticEmployees = UserRenderer.hasAuthority(user, "DWApproveNewDomesticEmployees", todayDt);
		BeanDiff buildingDiff = applicationDiff.getBean("Line").findBeanByFieldValue("Risk", "Id", risk.getId(), true, true).getBean("Building");
		String oldNumDomesticEmployees = buildingDiff.getOldValue("NumDomesticEmployees");
		String newNumDomesticEmployees = buildingDiff.getNewValue("NumDomesticEmployees");

		if( !canApproveNewDomesticEmployees && oldNumDomesticEmployees.equals("0") && StringRenderer.greaterThan(newNumDomesticEmployees, "0") ){
			addRiskApproval("Domestic employees requires approval", risk);
		}
	}

	private void triggerNaturalGasWellChangeApproval( ModelBean risk ) throws Exception {
		if( isNewRisk(risk) || (applicationDiff != null && 
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/QuestionReplies/QuestionReply[@Name='QNaturalGasWellOnPremises']/@Value") ))
			return;

		boolean canApproveNaturalGasWellChange = UserRenderer.hasAuthority(user, "DWApproveGasWellChange", todayDt);
		BeanDiff questionReplyDiff = applicationDiff.getBean("Line").findBeanByFieldValue("Risk", "Id", risk.getId(), true, true).getBean("QuestionReplies").findBeanByFieldValue("QuestionReply", "Name", "QNaturalGasWellOnPremises");

		if( !canApproveNaturalGasWellChange && questionReplyDiff != null && StringRenderer.isTrue(questionReplyDiff.getNewValue("Value")) ){
			addRiskApproval("Natural gas well requires approval", risk);
		}
	}

	private void triggerPropertyManagerChangeApproval( ModelBean risk ) throws Exception {
		boolean canApprovePropertyManagerChange = UserRenderer.hasAuthority(user, "DWApprovePMChange", todayDt);

		if( !canApprovePropertyManagerChange && applicationDiff != null &&
				applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/QuestionReplies/QuestionReply[@Name='QPropMgmtCompanyName']/@Value") ){
			addRiskApproval("Property manager requires approval", risk);
		}
	}

	private void triggerFL15AddedApproval( ModelBean risk ) throws Exception {
		boolean canApproveFL15Added = UserRenderer.hasAuthority(user, "DWApproveFL15Added", todayDt);
		ModelBean coverage = risk.getBean("Coverage", "CoverageCd", "CCHICP" );

		if( !canApproveFL15Added && isNewCoverage(coverage) ){
			addRiskApproval("Coverage C - Higher limits on certain property - FL 15 requires approval", risk);
		}
	}

	private void triggerFL16AddedApproval( ModelBean risk ) throws Exception {
		boolean canApproveFL16Added = UserRenderer.hasAuthority(user, "DWApproveFL16Added", todayDt);
		ModelBean coverage = risk.getBean("Coverage", "CoverageCd", "PSIO" );

		if( !canApproveFL16Added && isNewCoverage(coverage) ){
			addRiskApproval("Private Structures with Incidental Occupancies -  FL 16 requires approval", risk);
		}
	}

	private void triggerSecuredPartyChangeApproval( ModelBean risk ) throws Exception {
		boolean canApproveSecuredPartyChange = UserRenderer.hasAuthority(user, "DWApproveSecuredPartyChange", todayDt);

		if( !canApproveSecuredPartyChange && applicationDiff != null &&
				applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@aspiFL") ){
			addRiskApproval("Additional Secured Party's Interest Coverage requires approval", risk);
		}
	}

	private void triggerCondoCovChangeApproval( ModelBean risk ) throws Exception {
		if( isNewRisk(risk) || (applicationDiff != null &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@CondOwnerAddSpcCov")))
			return;

		boolean canApproveCondoCovChange = UserRenderer.hasAuthority(user, "DWApproveCondoCovChange", todayDt);
		BeanDiff buildingDiff = applicationDiff.getBean("Line").findBeanByFieldValue("Risk", "Id", risk.getId(), true, true).getBean("Building");
		String newCondOwnerAddSpcCov = buildingDiff.getNewValue("CondOwnerAddSpcCov");

		if( !canApproveCondoCovChange && StringRenderer.isTrue(newCondOwnerAddSpcCov) ){
			addRiskApproval("Special coverage for Condo requires approval", risk);
		}
	}

	private void triggerFL34AddedApproval( ModelBean risk ) throws Exception {
		boolean canApproveFL34Added = UserRenderer.hasAuthority(user, "DWApproveFL34Added", todayDt);
		ModelBean coverage = risk.getBean("Coverage", "CoverageCd", "BTC" );

		if( !canApproveFL34Added && isNewCoverage(coverage) ){
			for( ModelBean loss : losses ){
				if( loss.gets("LossCauseCd").equals("Theft") && !StringRenderer.isTrue(loss.gets("IgnoreInd"))){
					addRiskApproval("Broad theft coverage - FL 34 with prior theft loss requires approval", risk);
					return;
				}
			}
		}
	}

	private void triggerFL35AddedApproval( ModelBean risk ) throws Exception {
		boolean canApproveFL35Added = UserRenderer.hasAuthority(user, "DWApproveFL35Added", todayDt);
		ModelBean coverage = risk.getBean("Coverage", "CoverageCd", "LTC" );

		if( !canApproveFL35Added && isNewCoverage(coverage) ){
			for( ModelBean loss : losses ){
				if( loss.gets("LossCauseCd").equals("Theft") && !StringRenderer.isTrue(loss.gets("IgnoreInd"))){
					addRiskApproval("Limited theft coverage - FL 35 with prior theft loss requires approval", risk);
					return;
				}
			}
		}
	}

	private void triggerFL36AddedApproval( ModelBean risk ) throws Exception {
		boolean canApproveFL36Added = UserRenderer.hasAuthority(user, "DWApproveFL36Added", todayDt);
		ModelBean coverage = risk.getBean("Coverage", "CoverageCd", "CCF" );

		if( !canApproveFL36Added && isNewCoverage(coverage) ){
			addRiskApproval("Credit Cards and Depositors Forgery - FL 36 requires approval", risk);
		}
	}

	private void triggerFL0208RMAddedApproval( ModelBean risk ) throws Exception {
		boolean canApproveFL0208RMAdded = UserRenderer.hasAuthority(user, "DWApproveFL0208RMAdded", todayDt);
		ModelBean coverage = risk.getBean("Coverage", "CoverageCd", "WD" );

		if( !canApproveFL0208RMAdded && isNewCoverage(coverage) ){
			addRiskApproval("Water Damage FL 0208RM requires approval", risk);
		}
	}

	private void triggerRepairReplaceChangeApproval( ModelBean risk ) throws Exception {
		boolean canApproveRepairReplaceChange = UserRenderer.hasAuthority(user, "DWApproveRepairRepChange", todayDt);

		if( !canApproveRepairReplaceChange && applicationDiff != null &&
				(applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@RepairReplacementEndorsements") ||
						applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@RepairReplacementLimit")) ){
			addRiskApproval("Repair/Replacement requires approval", risk);
		}
	}

	private void triggerRM422AddedApproval( ModelBean risk ) throws Exception {
		boolean canApproveRM422Added = UserRenderer.hasAuthority(user, "DWApproveRM422Added", todayDt);
		ModelBean coverage = risk.getBean("Coverage", "CoverageCd", "DUCT" );

		if( !canApproveRM422Added && isNewCoverage(coverage) ){
			addRiskApproval("Dwelling under construction theft - RM 422 requires approval", risk);
		}
	}

	private void triggerELEDeletedApproval( ModelBean risk ) throws Exception {
		boolean canApproveELEDeleted = UserRenderer.hasAuthority(user, "DWApproveELEDeleted", todayDt);
		ModelBean coverage = risk.getBean("Coverage", "CoverageCd", "ELE" );

		if( !canApproveELEDeleted && isDeletedCoverage(coverage) ){
			addRiskApproval("Equine liabiltiy exclusion requires approval", risk);
		}
	}

	private void triggerRMDP74DeletedApproval( ModelBean risk ) throws Exception {
		boolean canApproveRMDP74Deleted = UserRenderer.hasAuthority(user, "DWApproveRMDP74Deleted", todayDt);
		ModelBean coverage = risk.getBean("Coverage", "CoverageCd", "RPSENS" );

		if( !canApproveRMDP74Deleted && isDeletedCoverage(coverage) ){
			addRiskApproval("Related private structures exclusion - named structures - RM DP 74 requires approval", risk);
		}
	}

	private void triggerRMDP75DeletedApproval( ModelBean risk ) throws Exception {
		boolean canApproveRMDP75Deleted = UserRenderer.hasAuthority(user, "DWApproveRMDP75Deleted", todayDt);
		ModelBean coverage = risk.getBean("Coverage", "CoverageCd", "RPSESL" );

		if( !canApproveRMDP75Deleted && isDeletedCoverage(coverage) ){
			addRiskApproval("Related private structures exclusion - specified location - RM DP 75 requires approval", risk);
		}
	}

	private void triggerSwimPoolChangeApproval( ModelBean risk ) throws Exception {
		boolean canApproveSwimPoolChange = UserRenderer.hasAuthority(user, "DWApproveSwimPoolChange", todayDt);

		if( !canApproveSwimPoolChange && applicationDiff !=  null &&
				applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@SwimPoolSurcharge") ){
			addRiskApproval("Swimming pool requires approval", risk);
		}
	}

	private void triggerFL200AddedApproval( ModelBean risk ) throws Exception {
		boolean canApproveFL200Added = UserRenderer.hasAuthority(user, "DWApproveFL200Added", todayDt);
		ModelBean building = risk.getBean("Building");
		String dwellingStyle = building.gets("DwellingStyle");
		String architecturalStyle = building.gets("ArchitecturalStyle");
		String yearBuilt = building.gets("YearBuilt");
		String rctMobileHomeCov = building.gets("RCTMobileHomeCov");

		if( !isNewRisk(risk) && applicationDiff != null &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@DwellingStyle") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@ArchitecturalStyle") &&
				!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@RCTMobileHomeCov") )
			return;

		if( !canApproveFL200Added &&
				dwellingStyle.equals("Mobile home") && !architecturalStyle.equals("Doublewide") && !architecturalStyle.equals("Triplewide") &&
				StringRenderer.isTrue(rctMobileHomeCov) && StringRenderer.lessThan(yearBuilt, "1994") ){
			addRiskApproval("Mobile home with FL 200 requires approval", risk);
		}
	}
	
	private void triggerGL74AddedApproval( ModelBean risk ) throws Exception {
		boolean canApproveGL74Added = UserRenderer.hasAuthority(user, "DWApproveGL74Added", todayDt);
		ModelBean coverage = risk.getBean("Coverage", "CoverageCd", "BAL" );

		if( !canApproveGL74Added && isNewCoverage(coverage) ){
			addRiskApproval("GL 74 requires approval", risk);
		}
	}

	private void triggerGL82AddedApproval( ModelBean risk ) throws Exception {
		boolean canApproveGL82Added = UserRenderer.hasAuthority(user, "DWApproveGL82Added", todayDt);
		ModelBean coverage = risk.getBean("Coverage", "CoverageCd", "WL" );

		if( !canApproveGL82Added && isNewCoverage(coverage) ){
			addRiskApproval("GL 82 requires approval", risk);
		}
	}

	private void triggerEndorseGreaterThan30DaysFromTodayApproval() throws Exception {
		boolean canApproveEndorseGreaterThan30DaysFromToday = UserRenderer.hasAuthority(user, "DWEndorseGt30DaysFromToday", todayDt);
		StringDate effectiveDt = transactionInfo.getDate("TransactionEffectiveDt");
		StringDate after30Days = DateRenderer.advanceDate(todayDt, "30");

		if( !canApproveEndorseGreaterThan30DaysFromToday && DateRenderer.greaterThan(effectiveDt, after30Days) ){
			String msg = "Change effective date is greater than 30 days from today";
			ValidationRenderer.addValidationError( application,"Approval","APPROVAL", msg );
		}
	}

	private void triggerCancelGreaterThan30DaysFromTodayApproval() throws Exception {
		boolean canApproveCancelGreaterThan30DaysFromToday = UserRenderer.hasAuthority(user, "DWCancelGt30DaysFromToday", todayDt);
		StringDate effectiveDt = transactionInfo.getDate("TransactionEffectiveDt");
		StringDate after30Days = DateRenderer.advanceDate(todayDt, "30");

		if( !canApproveCancelGreaterThan30DaysFromToday && DateRenderer.greaterThan(effectiveDt, after30Days) ){
			String msg = "Cancellation date is greater than 30 days from today";
			ValidationRenderer.addValidationError( application,"Approval","APPROVAL", msg );
		}
	}

	private void triggerFL1PermanentHomesCovAApproval( ModelBean risk ) throws Exception {
		boolean canApproveFL1PermanentCovA = UserRenderer.hasAuthority(user, "DWApproveFL1PermanentCovA", todayDt);
		ModelBean building = risk.getBean("Building");
		String covALimit = building.gets("CovALimit");
		String dwellingStyle = building.gets("DwellingStyle");

		if( !canApproveFL1PermanentCovA && basicPolicy.gets("SubTypeCd").equals("FL1") &&
				!dwellingStyle.equals("Mobile home") && !dwellingStyle.equals("Condominium/Co-operative") &&
				StringRenderer.greaterThanEqual(covALimit, "5000") && StringRenderer.lessThan(covALimit, "25000") ) {
			addRiskApproval("An FL1 policy with Coverage A less than $25,000 requires approval", risk);
		}
	}

	private void triggerFL1MobileHomeCovAApproval( ModelBean risk ) throws Exception {
		boolean canApproveFL1MobileHomeCovA = UserRenderer.hasAuthority(user, "DWApproveFL1MobileHomeCovA", todayDt);
		ModelBean building = risk.getBean("Building");
		String covALimit = building.gets("CovALimit");

		if( !canApproveFL1MobileHomeCovA && basicPolicy.gets("SubTypeCd").equals("FL1") &&
				building.gets("DwellingStyle").equals("Mobile home") &&
				StringRenderer.greaterThanEqual(covALimit, "3000") && StringRenderer.lessThan(covALimit, "5000") ) {
			addRiskApproval("An FL1 policy covering a mobile home with Coverage A less than $5,000 requires approval", risk);
		}
	}

	private void triggerFL2PermanentHomesCovAApproval( ModelBean risk ) throws Exception {
		boolean canApproveFL2PermanentCovA = UserRenderer.hasAuthority(user, "DWApproveFL2PermanentCovA", todayDt);
		ModelBean building = risk.getBean("Building");
		String covALimit = building.gets("CovALimit");
		String dwellingStyle = building.gets("DwellingStyle");

		if( !canApproveFL2PermanentCovA && basicPolicy.gets("SubTypeCd").equals("FL2") &&
				!dwellingStyle.equals("Mobile home") && !dwellingStyle.equals("Condominium/Co-operative") &&
				StringRenderer.greaterThanEqual(covALimit, "50000") && StringRenderer.lessThan(covALimit, "75000") ) {
			addRiskApproval("An FL2 policy with Coverage A less than $75,000 requires approval", risk);
		}
	}

	private void triggerNewBusinessBackdatedApproval() throws Exception {
		boolean canApproveNewBusinessBackdated = UserRenderer.hasAuthority(user, "DWApproveNewBusinessBackdated", todayDt);
		StringDate effectiveDt = basicPolicy.getDate("EffectiveDt");
		StringDate threeDaysAgo = DateRenderer.advanceDate(todayDt, "-3" );

		if( !canApproveNewBusinessBackdated && DateRenderer.lessThan(effectiveDt, threeDaysAgo) ){
			ValidationRenderer.addValidationError(application, "Approval", "APPROVAL", "An effective date is more than three days in the past requires approval");
		}
	}
	
	private void triggerUWApproval() throws Exception {
		boolean canApproveUWorAbove = UserRenderer.hasAuthority(user, "ApprovalCatchAllApprovalRule", todayDt);
		
		if( !canApproveUWorAbove ){
			ValidationRenderer.addValidationError(application, "Approval", "APPROVAL", "The application needs to be reviewed by Underwriting prior to issuance");
		}
	}	
	
	private void triggerCovALimitLt100kApproval( ModelBean risk ) throws Exception {
		boolean canApproveCovALimitLt100k = UserRenderer.hasAuthority(user, "DWApproveCovALimitLt100k", todayDt);
		String dwProgramTypeCd = basicPolicy.gets("DwProgramTypeCd");
		String dwellingStyle = risk.getBean("Building").gets("DwellingStyle");
		String covALimit = risk.getBean("Building").gets("CovALimit");
		
		if( !isNewRisk(risk) && applicationDiff != null &&
			!applicationDiff.hasFieldModifiedByXPath("//BasicPolicy/@DwProgramTypeCd") &&
			!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@DwellingStyle") &&
			!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@CovALimit") )
			return;
		
		if( !canApproveCovALimitLt100k && dwProgramTypeCd.equals("LLP") && !dwellingStyle.equals("Condominium/Co-operative") && StringRenderer.lessThan(covALimit, "100000") ) {
			addRiskApproval("A - 'Dwelling Limit' less than $100,000 requires approval", risk);
		}
	}
	
	private void triggerManualExperienceGroupLt50kApproval( ModelBean risk ) throws Exception {
		boolean canApproveExpGroupCovALimitLt50k = UserRenderer.hasAuthority(user, "DWApproveExpGroupCovALimitLt50k", todayDt);
		String manualExperienceGroup  = risk.getBean("Building").gets("ManualExperienceGroup");
		String covALimit = risk.getBean("Building").gets("CovALimit");
		
		if( !isNewRisk(risk) && applicationDiff != null &&
			!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@ManualExperienceGroup") &&
			!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@CovALimit") )
			return;
		
		if( !canApproveExpGroupCovALimitLt50k && manualExperienceGroup.equals("Yes") && StringRenderer.lessThan(covALimit, "50000") ) {
			addRiskApproval("The experience group discount is applied when the Coverage A amount is less than $50,000", risk);
		}
	}
	
	private void triggerManualExperienceGroupMobileHomeApproval( ModelBean risk ) throws Exception {
		boolean canApproveMobileHomeExpGroup = UserRenderer.hasAuthority(user, "DWApproveMobileHomeExpGroup", todayDt);
		String manualExperienceGroup  = risk.getBean("Building").gets("ManualExperienceGroup");
		String dwellingStyle = risk.getBean("Building").gets("DwellingStyle");
		
		if( !isNewRisk(risk) && applicationDiff != null &&
			!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@DwellingStyle") &&
			!applicationDiff.hasFieldModifiedByXPath("//Line/Risk[@id='"+ risk.getId() + "']/Building/@ManualExperienceGroup") )
			return;
		
		if( !canApproveMobileHomeExpGroup && manualExperienceGroup.equals("Yes") && dwellingStyle.equals("Mobile Home") ) {
			addRiskApproval("The experience group discount is applied when the Dwelling Style = Mobile Home", risk);
		}
	}	
	

	/**
	 * Helper function to determine if a loss has been added.
	 * @param loss
	 * @return True - Only during New Business and Rewrite-New, True - If added after original policy, False - Not added after policy.
	 * @throws Exception
	 */
	private boolean isNewLoss( ModelBean loss ) throws Exception {
		if( loss == null || applicationDiff == null )
			return false;

		BeanDiff lossDiff = applicationDiff.getBeanById(loss.getId());
		return lossDiff != null && lossDiff.getOperation() == BeanDiff.OPERATION_ADD;
	}

	/**
	 * Helper function to determine if coverage has been added.
	 * @param coverage
	 * @return True - Only during New Business and Rewrite-New, True - If added after original policy, False - Not added after policy.
	 * @throws Exception
	 */
	private boolean isNewCoverage( ModelBean coverage ) throws Exception {
		if( coverage == null || applicationDiff == null )
			return false;

		BeanDiff coverageDiff = applicationDiff.getBean("Line").getBeanById(coverage.getParentBean().getId()).getBeanById(coverage.getId());
		return coverageDiff != null && coverageDiff.getOperation() == BeanDiff.OPERATION_ADD;
	}

	/**
	 * Helper function to determine if coverage has been deleted
	 * @param coverage
	 * @return True - If deleted after original policy,
	 *         False - Not deleted after policy.
	 * @throws Exception
	 */
	private boolean isDeletedCoverage( ModelBean coverage ) throws Exception {
		if( coverage == null || applicationDiff == null )
			return false;

		BeanDiff coverageDiff = applicationDiff.getBean("Line").getBeanById(coverage.getParentBean().getId()).getBeanById(coverage.getId());
		return coverageDiff != null && coverageDiff.getOperation() == BeanDiff.OPERATION_DELETE;
	}

	/**
	 * Helper function to determine if a new risk has been added.
	 * @param risk
	 * @return True - Only during New Business and Rewrite-New, True - If added after original policy, False - Not added after policy.
	 * @throws Exception
	 */
	private boolean isNewRisk( ModelBean risk ) throws Exception {
		if( risk == null || applicationDiff == null )
			return false;

		BeanDiff riskDiff = applicationDiff.getBean("Line").getBeanById(risk.getId());
		return riskDiff != null && riskDiff.getOperation() == BeanDiff.OPERATION_ADD;
	}

	/**
	 * Generate approval messages.
	 * NOTE: Needs to be ran after all risk approvals have been looped through.
	 * @throws Exception
	 */
	private void generateApprovalMsgs() throws Exception {
		boolean isSingleRisk = risks.length == 1;

		for( String msg : riskApprovals.keySet() ){
			//Single Risk
			if( isSingleRisk ) {
				NonApprovedRisk nonApprovedRisk = riskApprovals.get(msg).get(0);

				if( nonApprovedRisk.getTabLink() != null ){
					String action = String.format( genericTabLink, application.getSystemId(),
							nonApprovedRisk.getRisk().getId(), nonApprovedRisk.getRisk().getId(),
							nonApprovedRisk.getTabLink() + (nonApprovedRisk.getHighlightFields() == null ? "" : "&HighlightFields=" + nonApprovedRisk.getHighlightFields()));
					String tabLink = ValidationRenderer.createActionLink(action);
					ValidationRenderer.addValidationErrorWithLink( application,"Approval","APPROVAL", msg, tabLink );
				} else {
					ValidationRenderer.addValidationError(application, "Approval", "APPROVAL", msg);
				}
			} else { //Multiple Risks
				List<NonApprovedRisk> nonApprovedRisks = riskApprovals.get(msg);
				String modifiedMsg = msg + ": Risk #";

				for(int i = 0; i < nonApprovedRisks.size(); i++ ){
					NonApprovedRisk nonApprovedRisk = nonApprovedRisks.get(i);
					String buildingNum = nonApprovedRisk.getRisk().getBean("Building").gets("BldgNumber");

					if( i != 0 && i + 1 == nonApprovedRisks.size() )
						modifiedMsg += "and ";

					if( nonApprovedRisk.getTabLink() != null ){
						String action = String.format( genericTabLink, application.getSystemId(),
								nonApprovedRisk.getRisk().getId(), nonApprovedRisk.getRisk().getId(),
								(nonApprovedRisk.getTabLink() == null ? "" : nonApprovedRisk.getTabLink()) +
								(nonApprovedRisk.getHighlightFields() == null ? "" : "&HighlightFields=" + nonApprovedRisk.getHighlightFields()));
						String tabLink = ValidationRenderer.createActionLink(action, buildingNum);
						modifiedMsg += tabLink;
					} else {
						modifiedMsg += buildingNum;
					}

					if( nonApprovedRisks.size() > 2 )
						modifiedMsg += ", ";
					else
						modifiedMsg += " ";
				}

				ValidationRenderer.addValidationError(application, "Approval", "APPROVAL", modifiedMsg);
			}
		}
	}

	/**
	 * Add Approval message with risk to map.
	 * @param msg
	 * @param risk
	 */
	private void addRiskApproval( String msg, ModelBean risk ){
		addRiskApproval(msg, risk, null, null);
	}

	/**
	 * Add Approval message with risk, tabParams, and highlight fields to map.
	 * @param msg
	 * @param risk
	 * @param tabLink
	 * @param highlightFields
	 */
	private void addRiskApproval( String msg, ModelBean risk, String tabLink, String highlightFields ) {
		if( !riskApprovals.containsKey(msg) ){
			riskApprovals.put(msg, new ArrayList<NonApprovedRisk>());
		}
		riskApprovals.get(msg).add(new NonApprovedRisk(risk, tabLink, highlightFields));
	}

	/**
	 * NonApprovedRisk class to hold risk modelbean and link information.
	 */
	private class NonApprovedRisk {
		private ModelBean risk;
		private String tabLink;
		private String highlightFields; // Comma-separated values

		NonApprovedRisk(ModelBean risk, String tabLink, String highlightFields ){
			this.risk = risk;
			this.tabLink = tabLink;
			this.highlightFields = highlightFields;
		}

		public ModelBean getRisk(){
			return risk;
		}

		public String getTabLink(){
			return tabLink;
		}

		public String getHighlightFields() {
			return highlightFields;
		}
	}
}
