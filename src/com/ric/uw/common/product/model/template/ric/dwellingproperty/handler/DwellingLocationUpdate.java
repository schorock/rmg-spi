package com.ric.uw.common.product.model.template.ric.dwellingproperty.handler;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;

import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.bean.render.BeanRenderer;
import com.iscs.uw.common.risk.Location;

/**
 * Handler that keeps the location data, which isn't visible to the user,
 * matched up with the corresponding risk of the location. It is assumed that
 * each risk has only one location.
 * 
 * @author justin.rumpf
 * 
 */
public class DwellingLocationUpdate extends InnovationIBIZHandler {

	public DwellingLocationUpdate() throws Exception {
		super();
	}

	/**
	 * Process a response and ensures location data matches their corresponding
	 * risk data
	 */
	public ModelBean process() throws IBIZException, ServiceHandlerException {
		try {
			// Log a Greeting
			Log.debug("Processing " + this.getClass().getSimpleName());

			ModelBean rs = this.getHandlerData().getResponse();
			ModelBean cmm = rs.getBean("ResponseParams").getBean("CMMParams");
			ModelBean container = rs.getBean(cmm.gets("Container"));

			ModelBean[] risks = container.getAllBeans("Risk");
			for (ModelBean risk : risks) {
				if ( risk.gets("TypeCd").equals("Exposure") ) {
					continue;
				}
				ModelBean location = getOrCreateLocation(container, risk);

				Log.debug("Updating " + risk.getId()+" "+location.getId());

				/*
				 * Collecting address information and copying the risk values to
				 * the location to ensure at least these values match.
				 */
				ModelBean building = risk.getBean("Building");
				ModelBean riskAddr = building.getBean("Addr", "AddrTypeCd", "RiskAddr");
				ModelBean riskLookupAddr = building.getBean("Addr", "AddrTypeCd", "RiskLookupAddr");

				ModelBean locationAddr = location.getBean("Addr", "AddrTypeCd", "LocationAddr");
				ModelBean locationLookupAddr = location.getBean("Addr", "AddrTypeCd", "LocationLookupAddr");

				BeanTools.copyNonAliasValues(riskAddr, locationAddr);
				BeanTools.copyNonAliasValues(riskLookupAddr, locationLookupAddr);

				/*
				 * Match up additional location fields (i.e. keeping the
				 * 'numbers' the same as a risks) and making sure the status
				 * matches
				 */
				String bldgNumber = building.gets("BldgNumber");
				if ( bldgNumber != null && !bldgNumber.equals("")) {
					location.setValue("LocationNumber", bldgNumber);
				
				}
				if ( !risk.gets("Status").equals("Deleted")) {  //delete status can be handled at another method
					location.setValue("Status", risk.gets("Status"));
				}
				else {
					// as the location can be shared by other risks ( risk copied, exposure risk, etc...), only mark the risk deleted if there is no reference to it
					ModelBean[] activeRisks = BeanRenderer.findBeansByFieldValueAndStatus(container, "Risk", "LocationRef", location.getId(), "Active");
					if ( activeRisks.length == 0) {
						location.setValue("Status", "Deleted");
					}
						
				}
			}

			deleteExtraLocations(container);

			// Return the Response ModelBean
			return rs;
		} catch (Exception e) {
			throw new ServiceHandlerException(e);
		}
	}

	/**
	 * Helper method that will either get a Location ModelBean based on the
	 * LocationRef on the Risk or create a new one if one can't be found. This
	 * method was added because the RiskCopy functionality won't create a
	 * Location and we need some way of ensuring there's a one to one
	 * relationship between Risks and Locations.
	 * 
	 * @param container
	 * @param risk
	 * @return
	 * @throws ModelBeanException
	 * @throws Exception
	 */
	private ModelBean getOrCreateLocation(ModelBean container, ModelBean risk)
			throws ModelBeanException, Exception {
		ModelBean location = container.getBeanById("Location", (risk.gets("LocationRef")));
		if (location == null && !risk.gets("TypeCd").equals("Exposure")) {
			location = new Location().create();
			risk.setValue("LocationRef", location.getId());
			container.addValue(location);
		}
		return location;
	}

	/**
	 * Helper method that deletes all locations that don't have a matching risk.
	 * This exists because it's possible for a location to be created when you
	 * first create a risk. If that risk isn't saved it will be discarded but
	 * the location will remain and will need to be cleaned up.
	 * 
	 * @param rs
	 * @throws ModelBeanException
	 */
	private void deleteExtraLocations(final ModelBean container)
			throws ModelBeanException {
		ModelBean[] locations = container.getAllBeans("Location");
		for (ModelBean location : locations) {
			String id = location.getId();
			ModelBean risk = container.findBeanByFieldValue("Risk", "LocationRef", id);
			if (risk == null ) {
				container.deleteBeanById(id);
			}
		}
	}
	
	
}