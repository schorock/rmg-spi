package com.ric.uw.common.product.model.template.ric.dwellingproperty.handler;

import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.bean.render.BeanRenderer;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;

public class DwellingRiskPreset extends InnovationIBIZHandler {

	public DwellingRiskPreset() throws Exception {
		super();
	}

	/**
	 * Process multi-risks and preset them based on first risk.
	 * 
	 * @author rhoush
	 */
	public ModelBean process() throws IBIZException, ServiceHandlerException {
		try {
			// Log a Greeting
			Log.debug("Processing " + this.getClass().getSimpleName());

			ModelBean rs = this.getHandlerData().getResponse();
			ModelBean cmm = rs.getBean("ResponseParams").getBean("CMMParams");
			ModelBean application = rs.getBean(cmm.gets("Container"));
			ModelBean line = application.getBean("Line");
			ModelBean[] risks = line.findBeansByFieldValue("Risk", "Status", "Active");
			
			if( risks.length > 1 ) {
				ModelBean[] activeBuildings = line.findBeansByFieldValue("Building", "Status", "Active"); 
				ModelBean[] sortedActiveBuildings = BeanRenderer.sortBeansBy(activeBuildings, "BldgNumber", "Ascending");
			    ModelBean firstBuilding = sortedActiveBuildings[0];
			    ModelBean lastBuilding = sortedActiveBuildings[sortedActiveBuildings.length - 1];
			    
			    lastBuilding.setValue("LiabilityCoverageType", firstBuilding.gets("LiabilityCoverageType"));
			    lastBuilding.setValue("CovLLimit", firstBuilding.gets("CovLLimit"));
			    lastBuilding.setValue("CovMLimit", firstBuilding.gets("CovMLimit"));
			    lastBuilding.setValue("CovOLimit", firstBuilding.gets("CovOLimit"));
			    lastBuilding.setValue("PersonalInjLiabilityInd", firstBuilding.gets("PersonalInjLiabilityInd"));
			    lastBuilding.setValue("AnnualAggrMultiple", firstBuilding.gets("AnnualAggrMultiple"));
			    lastBuilding.setValue("NumOffice", firstBuilding.gets("NumOffice"));
			}
			
			// Return the Response ModelBean
			return rs;
		} catch (Exception e) {
			throw new ServiceHandlerException(e);
		}
	}
}
