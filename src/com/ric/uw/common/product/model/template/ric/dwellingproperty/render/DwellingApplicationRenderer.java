/**
 * 
 */
package com.ric.uw.common.product.model.template.ric.dwellingproperty.render;

import java.math.BigDecimal;
import java.util.ArrayList;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.date.StringDate;

import com.iscs.common.business.provider.render.ProviderRenderer;
import com.iscs.common.question.Question;
import com.iscs.common.question.render.QuestionRenderer;
import com.iscs.common.render.DynamicString;
import com.iscs.common.render.Renderer;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.render.BeanRenderer;

/**
 * @author karthick.gopalakrish
 *
 */
public class DwellingApplicationRenderer implements Renderer {

	/**
	 * 
	 */
	public DwellingApplicationRenderer() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * For close out validation rule, checks to see if all visible questions have been answered
	 * If no value defined for a visible question, then return false. If no question replies bean
	 * has been created, then return false. Otherwise if all visible questions are answered, return
	 * true
	 * @param application
	 * @return
	 */
	public static boolean areAllQuestionsAnswered(ModelBean container) {
		try {
			ModelBean questionReplies = container.getBean("QuestionReplies");
			if (questionReplies == null)
				return false;	

			DynamicString.render(container, "Set up the Rendering String Once");
			ModelBean questions = QuestionRenderer.getQuestions(questionReplies.gets("QuestionSourceMDA"), "Application");
			for (ModelBean question: questions.getAllBeans("Question")) {
				if (Question.isVisible(question, questionReplies)) {
					ModelBean reply = questionReplies.getBean("QuestionReply","Name",question.gets("Name"));
					if ((reply == null || reply.gets("Value").equals("")) && StringTools.isTrue(question.gets("RequiredInd")))
						return false;
				}
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static String getProducerSubmitTo(ModelBean basicPolicy) throws Exception{
		String submitTo = "UW1"; //default to UW1 team if submit to info is not available
		if(basicPolicy != null){    		
			try {		    	
				String providerRef = basicPolicy.gets("ProviderRef");
				ModelBean provider = ProviderRenderer.getProviderBySystemId(providerRef);
				submitTo = provider.getBean("ProducerInfo").gets("SubmitTo");
				return submitTo;
			} catch(Exception ex){
				Log.error(ex);

			}
		}
		return submitTo;
	}


	/**
	 * Returns true if "Maximum Protective Device Credit Factor" is greater than 0.04
	 * @param risk Risk ModelBean
	 * @return "true" if Protective Discount Value is greater than 0.04, "false" otherwise
	 */
	public static boolean exceedsMaxProtectiveDeviceCreditFactor(ModelBean risk) {
		BigDecimal protectiveDiscountValue = new BigDecimal(0.0);
		boolean isAbove = false;
		try {
			ModelBean coverageA = BeanRenderer.findBeanByFieldValueAndStatus(risk, "Coverage","CoverageCd","CovA","Active");
			if( coverageA != null ) {
				ModelBean protectiveStepBean = BeanRenderer.findBeanByFieldValueAndStatus(coverageA, "Step","Name","Total Credits","Active");
				if( protectiveStepBean != null ) {
					ModelBean protectiveStepMaxFactorBean = BeanRenderer.findBeanByFieldValueAndStatus( protectiveStepBean.getBean("Steps"), "Step","Name","Maximum Protective Device Credit Factor","Active");
					if( protectiveStepMaxFactorBean != null ) {
						protectiveDiscountValue = new BigDecimal(protectiveStepMaxFactorBean.gets("Value")).abs();
					}
				}
			}
			if( (protectiveDiscountValue!= null) && StringRenderer.greaterThan(protectiveDiscountValue+"","0.04")) {
				isAbove = true;
			}          
		} catch (Exception e) {
			return false;
		}
		return isAbove;
	}

	@Override
	public String getContextVariableName() {
		// TODO Auto-generated method stub
		return "DwellingApplicationRenderer";
	}


	/**
	 * Returns if loss history less than 3 years is valid for LossType = Liability
	 * no.
	 * @return
	 */    
	public static boolean validateLossHistory(ModelBean application) throws Exception {
		ModelBean[] lossHistory = getAllActiveNonIgnoredLosses(application);
		boolean isValidLossHistory = false;
		
		if (lossHistory != null && lossHistory.length > 2) {
			isValidLossHistory = true;
		} else if (lossHistory != null && lossHistory.length > 1) {
			boolean isWeatherFlag = false; 
			for (ModelBean loss : lossHistory) {
				if(!StringTools.isTrue(loss.gets("WeatherInd"))) {
					if(isWeatherFlag){
						isValidLossHistory = true;
						break;
					}
					isWeatherFlag = true;
				}
			}   
		} 
		return isValidLossHistory;		
	}    

	/**
	 * Returns if loss history less than 3 years is valid
	 * no.
	 * @return
	 */    
	public static boolean getLossHistoryForTask(ModelBean application) throws Exception {
		//ModelBean[] lossHistory = getAllActiveNonIgnoredLosses(application);
		ModelBean[] lossHistory = application.findBeansByFieldValue("LossHistory" , "StatusCd", "Active");
		boolean isValid = false;
		try {
			for (ModelBean loss : lossHistory) {
				if(!loss.gets("IgnoreInd").equalsIgnoreCase("Yes") && loss.gets("SourceCd").equalsIgnoreCase("Application") && !loss.gets("LossCauseCd").equalsIgnoreCase("Wind") && loss.gets("WeatherInd").equalsIgnoreCase("Yes")) {
					isValid = true;
				}
			}    
		} catch (Exception e) {
			return false;    		
		}
		return isValid;		
	}    

	/**
	 * Returns a list of last 3 years of loss history beans from application. Only active status and ignore claoim count indicator "No".
	 * @return
	 */
	private static ModelBean[] getAllActiveNonIgnoredLosses(ModelBean application) {

		try {
			ArrayList<ModelBean> filteredLosses = new ArrayList<ModelBean>();
			ModelBean[] lossHistory = application.findBeansByFieldValue("LossHistory" , "StatusCd", "Active");
			for (ModelBean loss: lossHistory) {
				StringDate effectiveDt = application.getBean("BasicPolicy").getDate("EffectiveDt");		
				if ((DateTools.getAge(loss.getDate("LossDt"), effectiveDt) < 3) && loss.gets("ClaimCountInd").equalsIgnoreCase("Yes")) {
					filteredLosses.add(loss);
				}
			}

			return filteredLosses.toArray(new ModelBean[filteredLosses.size()]);    		
		} catch (Exception e) {
			return new ModelBean[0];
		}
	}	

}
