/*
 * RuleProcessorReview.java
 *
 */

package com.ric.uw.common.rule.handler;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;

import com.iscs.common.tech.log.Log;
import com.iscs.uw.policy.PM;

/** Processes Review rules *
 * @author  Ken Brower
 */
public class RuleProcessorRating extends com.iscs.uw.common.rule.handler.RuleHandler {
    
    // Create an object for logging messages
    
    
    /** Creates a new instance of RuleProcessorRating 
     * @throws Exception never
     */
    public RuleProcessorRating() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {            
            Log.debug("Processing...");
            
            ModelBean application = getResponse().getBean("Application");
            String transCd = application.getBean("BasicPolicy").gets("TransactionCd");
            
            // Do not rate Cancellation type transactions
            if( application.getBean("TransactionInfo").gets("TransactionCd").equals(PM.TX_CANCELLATION) || 
            	application.getBean("TransactionInfo").gets("TransactionCd").equals(PM.TX_FUTURE_EXPIRE) ){
            	return application;
            }

            // Reinstatement should be the reversal of the cancellation w/out rating
            if( application.getBean("TransactionInfo").gets("TransactionCd").equals(PM.TX_REINSTATEMENT) ) {            	
            	return application;            	
            }
            
            // Do not rate Commission Reversal transactions
            if( application.getBean("TransactionInfo").gets("TransactionCd").equals(PM.TX_COMMISSION_REVERSAL)){
            	return application;
            }
            
            // Do not rate Billing Entity Change transactions
            if( application.getBean("TransactionInfo").gets("TransactionCd").equals(PM.TX_BILLING_ENTITY_CHANGE)){
            	return application;
            }
            
            /// Process the review rules                     
            ModelBean ruleRs = null;
            String[] ruleTypes = new String[] {"Rating"};
            if( !transCd.equals("Cancellation Notice")) {
            	// Return the response
                ruleRs = process(ruleTypes, application);
            }
            return ruleRs;
        }
        catch( IBIZException e){
            throw e;
        }
        catch( ServiceHandlerException e){
            throw e;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
    
}

