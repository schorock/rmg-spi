package com.ric.uw.common.rule.handler;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jdom.Attribute;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.xpath.XPath;

import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.render.DateRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.bean.render.BeanRenderer;
import com.iscs.workflow.Task;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
/**Task level rule used to close a task when one or more attachments are added
 * Attachment(s) and task must be attached (TaskLink) to the same risk
 * Attachments are specified by a required TemplateId and an optional TagId
 * Only attachments added after the addTm/dateTm of the task are considered
 * An example (explained below):
 * 
 * <TaskRule id="RuleRiskTaskAddOnMultiAttachment" RuleTypeCd="Service" Processor="java" RuleSource="UWRule::RuleRiskTaskCloseOnMultiAttachment::none::none">
 * 	<Param Name="TemplateId,1" Value="PPolicyAttachment3004" />
 * 	<Param Name="TagId,1" Value="SolidFuelHeatingDevices" />
 * 	<Param Name="TemplateId,2" Value="PPolicyAttachment3006" />
 * </TaskRule>
 * 
 * The Name of the Param is broken into two parts by the ',' (comma):
 * 		1:  either TemplateId (required) or TagId (optional)
 * 		2:  The 'key' for this particular value, used to associate together the TemplateId and TagId
 * 
 * Parameters beginning with TagId and TemplateId are paired up by the value after the comma
 * 
 * Thus above there are two attachments specified:
 * With a 'key' of 1:  PPolicyAttachment3004, which also requires a TagId of SolidFuelHeatingDevices
 * With a 'key' of 2:  PPolicyAttachment3006 (which has no associated TagId)
 * 
 * One other Param may be specified, which must match this exact Name and Value:
 *   <Param Name="Debug" Value="true" />
 * 
 * This turns on extensive logging to aid in troubleshooting of this somewhat complex risk-based task rule.
 */

public class RuleRiskTaskCloseOnMultiAttachment implements RuleProcessor {
	/**
	 * 
	 */
	private boolean debug = false;
	private XMLOutputter outr = new XMLOutputter(Format.getPrettyFormat());
	
	/**
	 * @param e
	 * @return
	 * @throws IOException
	 */
	private String prettyXml(Element e) throws IOException {
		try( StringWriter sw = new StringWriter() ) {
			outr.output(e, sw);
			String tmp = sw.getBuffer().toString();
			return tmp;
		}
	}
	
	/**
	 * @param message
	 * @param params
	 */
	private void log(String message, Object... params) {    
		if(debug) Log.debug(String.format(message, params));
	}
	
    /**
     * 
     */
    public RuleRiskTaskCloseOnMultiAttachment() { }

	/**
	 * @return
	 */
	public String getContextVariableName() {
		return "RuleRiskTaskCloseOnMultiAttachment";
	}
	
	/**
	 * @param name
	 * @return
	 */
	private String afterComma(String name) {
		String[] splits = name.split(",");
		if(splits.length >= 2)  {
			log("Found split on , = %s", name);
			return splits[1];
		} else {
			log("No split on , = %s", name);
			return null;
		}
	}
	
	/**
	 * @param templateXml
	 * @param xpath
	 * @return
	 * @throws JDOMException
	 */
	private Map<String,String> paramMap(Element templateXml, String xpath) throws JDOMException {
		Map<String,String> map = new HashMap<String,String>();
		List<?> nodeList = XPath.selectNodes(templateXml, xpath);
		log("xpath = %s", xpath);
		log("nodeList.size = %d", nodeList.size());

        Iterator<?> iter = nodeList.iterator();
        while (iter.hasNext()) {
            Element e = (org.jdom.Element) iter.next();
            String name = e.getAttributeValue("Name");
			log("paramMap name = %s", name);
            String key = afterComma(name);
            if(key!=null) {
				String value = e.getAttributeValue("Value");
				log("paramMap key = %s, value = %s", key, value);
				map.put(key,value);
            }
        }
		log("map.size = %d", map.size());
		return map;
	}
	
	/**
	 * @param xml
	 * @param xpath
	 * @return
	 * @throws JDOMException
	 * @throws IOException
	 */
	private String getAttrByXpath(Element xml, String xpath) throws JDOMException, IOException {
		String pretty = prettyXml(xml);
		log("getAttrByXpath xml:", pretty);
		log("getAttrByXpath xpath:", xpath);
		Attribute attr = (Attribute) XPath.selectSingleNode(xml, xpath);
		if(attr==null) {
			log("getAttrByXpath returning null");
			return null;
		}
		String value = attr.getValue();
		log("getAttrByXpath value:", value);
		return value;
	}
	
	/**
	 * @param ruleTemplate
	 * @param task
	 * @param policy
	 * @param user
	 * @param data
	 * @return
	 * @throws Exception
	 */
	private Boolean completeTask(ModelBean ruleTemplate, ModelBean task, ModelBean policy, ModelBean user, JDBCData data) throws Exception {
		if(!task.gets("Status").equals("Open")) return false;

		Element templateXml = ((ModelBean) ruleTemplate).marshal(true).getRootElement();

		Element taskXml = ((ModelBean) task).marshal(true).getRootElement();
		
		log("templateXml:", prettyXml(templateXml));
		log("taskXml:", prettyXml(taskXml));

		String riskId = getAttrByXpath(taskXml, "//TaskLink[@ModelName='Risk']/@IdRef");
		if(riskId==null) {
			log("riskId is null, exiting completeTask");
			return false;
		}
		log("riskId = %s", riskId);

		Map<String,String> templates = paramMap(templateXml, "//Param[starts-with(@Name,'TemplateId,')]");
		if(templates.size()==0) return false;
		log("templates.size = %d", templates.size());

		Map<String,String> tags = paramMap(templateXml, "//Param[starts-with(@Name,'TagId,')]");
		log("tags.size = %d", templates.size());

		Element policyXml = ((ModelBean) policy).marshal(true).getRootElement();
		String pol = prettyXml(policyXml);
		log("policyXml:", pol);
		
		StringDate taskDtTm = this.getStringDate(taskXml);
		log("taskDtTm = %s", taskDtTm);
		
		for (Map.Entry<String, String> entry : templates.entrySet()) {
			String templateKey = entry.getKey();
			String templateId = entry.getValue();
			String tagId = tags.get(templateKey);
			log("checking for attachment key=%s, template=%s, tag=%s.",templateKey,templateId,tagId);
			
			if( !this.findOneAttachment(templateId, riskId, tagId, policyXml, taskDtTm) ) return false;
		}
		return true;
	}

	/**
	 * @param e
	 * @return
	 * @throws Exception
	 */
	private StringDate getStringDate(Element e) throws Exception { 
		String addDt = getAttrByXpath(e, "//@AddDt");
		String addTm = getAttrByXpath(e, "//@AddTm");
		if(addDt==null || addTm==null)  {
			log("getStringDate return null;  addDt=%s, addTm=%s",addDt, addTm);
			return null;
		}
		
		return DateRenderer.addTimeToDate(new StringDate(addDt), addTm);
	}

	/**
	 * @param templateId
	 * @param riskId
	 * @param tagId
	 * @param policyXml
	 * @param taskDtTm
	 * @return
	 * @throws Exception
	 */
	private Boolean findOneAttachment(String templateId, String riskId, String tagId, Element policyXml, StringDate taskDtTm) throws Exception {
		List<?> result = null;
		if(tagId!=null) {
			String xp = String.format("//Attachment[@TemplateId='%s']/LinkReference[@IdRef='%s']/../Tag[@TagTemplateIdRef='%s']/..",templateId,riskId,tagId);
			log("xpath = %s", xp);
			result = XPath.selectNodes(policyXml, xp);
			log("templateId = %s, riskId=%s, tagId= %s, size = %d",templateId, riskId, tagId, result.size());
		} else {
			String xp = String.format("//Attachment[@TemplateId='%s']/LinkReference[@IdRef='%s']/..",templateId,riskId);
			log("xpath = %s", xp);
			result = XPath.selectNodes(policyXml, xp);
			log("templateId = %s, riskId=%s, size = %d",templateId, riskId, result.size());
		}

		if( result==null || result.size()==0 ) return false;

		Iterator<?> iter = result.iterator();
		
		while (iter.hasNext()) {
			Element e = (Element) iter.next();
			StringDate attachmentDtTm = this.getStringDate(e);
			log("attachmentDtTm = %s, e = %s",attachmentDtTm, prettyXml(e));
			if( DateRenderer.calendarCompareTo(taskDtTm, attachmentDtTm) < 0 ) {
				log("found attachment of correct type created after task creation.");
				return true;
			} else {
				log("attachment created before task created.");
			}
					
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.iscs.common.business.rule.RuleProcessor#process(net.inov.tec.beans.ModelBean, net.inov.tec.beans.ModelBean, net.inov.tec.beans.ModelBean[], net.inov.tec.beans.ModelBean, net.inov.tec.data.JDBCData)
	 */
	@Override
	public ModelBean process(ModelBean ruleTemplate, ModelBean task, ModelBean[] additionalBeans, ModelBean user, JDBCData data) throws RuleException {
		try {
			setDebugState(task);
			log("Start");
			
			ModelBean taskLink = task.getBean("TaskLink","ModelName","Policy");
			if(taskLink==null) {
				log("taskLink null, exiting");
			}
			log("taskLink = %s", taskLink);
	        ModelBean policy = BeanRenderer.selectModelBean(data,"Policy", taskLink.gets("IdRef"));

			if(policy==null) {
				log("policy null, exiting");
			}
	        
	        if(this.completeTask(ruleTemplate, task, policy, user, data)) {
				log("completeTask returned true");
				Task.updateTaskStatus(task, "Completed");
	        }
	        
			log("completeTask returned false");
			log("End");
	        return new ModelBean("UWRuleRs");
		}
		catch (Exception e) {
			Log.error(e.getMessage(), e);
            throw new RuleException(e);
		}
	}

	/**
	 * @param task
	 * @throws ModelBeanException
	 */
	private void setDebugState(ModelBean task) throws ModelBeanException {
		ModelBean debugOn = task.getBean("Param", "Name", "Debug");
		if(debugOn!=null) {
			String value = (String) debugOn.getValue("Value");
			if(value!=null && value.equals("true")) {
				this.debug=true;
			}
		}
	}
}

