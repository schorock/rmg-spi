package com.ric.uw.common.reinsurance;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import net.inov.mda.MDAObject;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.math.Money;

import com.ibm.math.BigDecimal;
import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDATemplate;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.uw.common.reinsurance.PolicyReinsurance;
import com.iscs.uw.common.reinsurance.PolicyReinsuranceException;
import com.iscs.uw.common.reinsurance.ReinsurancePlacementBase;
import com.iscs.uw.common.reinsurance.ReinsurancePremiumBase;
import com.iscs.uw.common.reinsurance.handler.ReinsuranceProcessHandler;
import com.iscs.uw.policy.PM;

public class ERPPolicyReinsurance implements ReinsuranceProcessHandler {

    /** Calculate the written premium for the Reinsurance placements.
     * Calculate the transaction commission at the same time.
     * @param container The policy container
     * @throws Exception when an error occurs
     */
    public static void calculationWrittenPremium(ModelBean container) 
    throws Exception {
    	ModelBean[] placements = container.getAllBeans("PolicyReinsurancePlacement");
    	for(ModelBean placement : placements) {
    		// See if the inforce changed, if so then need to calculate the written premium amount
    		Money prevInforce = new Money(placement.gets("PrevFinalAmt", "0.00"));
    		Money curInforce = new Money(placement.gets("PremiumAmt"));
    		Money zeroAmt = new Money("0.00");
    		
            // Get the factor to use for the current changes
            ModelBean basicPolicy = container.getBean("BasicPolicy");
            ModelBean transaction = container.getBean("TransactionInfo");
            StringDate changeDt = null;
            if (transaction == null) {
            	// This is a new application
            	changeDt = basicPolicy.getDate("EffectiveDt");
            } else {
            	changeDt = transaction.getDate("TransactionEffectiveDt");
            }
            
            // Get the pro rata factor if applicable
            double factor = 1.00;
            ModelBean detail = placement.getParentBean();
            if (!detail.gets("ReinsuranceTemplateIdRef").equals("ERP-Liability")) {
                factor = PM.calculateTermFactor(basicPolicy.getDate("EffectiveDt") , basicPolicy.getDate("ExpirationDt"), changeDt);            	
            }
            
			// Always recalculat written premium.
            // Don't just do it if the inforce has changed, because "changed" is relative to the prior
            // policy transaction, no the prior application save. Since nowhere else is this process
            // zeroing placement written premium (like PM is for coverages, etc.) just recalculating it
            // every time has the same effect.
            BigDecimal written = null;
            if (PolicyReinsurance.hasOldPlacedReinsurance(container.getBean("PolicyReinsurance")) || PolicyReinsurance.isCalculateWritten(placement)) {
                written = new BigDecimal(curInforce.subtract(prevInforce).multiply(BigDecimal.valueOf(factor)).toString()).setScale(2, BigDecimal.ROUND_HALF_UP);
                placement.setValue("WrittenPremiumAmt", new Money(written));   
            } else {
        		Money prevWritten= new Money(placement.gets("PrevPremiumWrittenAmt", "0.00"));
        		Money curWritten = new Money(placement.gets("PremiumWrittenAmt"));
        		if (curInforce.subtract(prevInforce).equals(zeroAmt)) {
        			// Change in Inforce is zero, written premium has to be zero
        			written = new BigDecimal(zeroAmt.toString());
        		} else if (basicPolicy.gets("TransactionCd").equals((PM.TX_UNAPPLY))) {
        			// The written just needs to be negated on unapplies
        			if (prevInforce.equals(zeroAmt)) {
                    	written = new BigDecimal(curWritten.subtract(prevWritten).toString()).setScale(2, BigDecimal.ROUND_HALF_UP);
        			} else {
            			written = new BigDecimal(placement.getValueDecimal("PrevWrittenAmt").multiply(new BigDecimal("-1.00")).toString());        			
        			}
        		} else if (!transaction.gets("ReplacementOfTransactionNumber").isEmpty()) {
        			// Reapplies unfortunately just merge prior changes with current, and therefore prior written cannot be considered
        			written = new BigDecimal(curWritten.toString());
        		} else {
                	written = new BigDecimal(curWritten.subtract(prevWritten).toString()).setScale(2, BigDecimal.ROUND_HALF_UP);
        		}
                placement.setValue("WrittenPremiumAmt", new Money(written));   

                // Now update the placement subjects with the written premium breakdown
    	    	PolicyReinsurance.updatePlacementSubjectWrittePremium(container, placement, factor, changeDt);                
            }
            
			// Calculate the transaction commission amount
            BigDecimal commAmt = new BigDecimal("0.00");
			String comm = placement.gets("CommissionPct");
			if (comm.length() > 0) {
				BigDecimal commPct = new BigDecimal(comm);
				commAmt = written.multiply(commPct).divide(new BigDecimal("100.00"), 2, BigDecimal.ROUND_HALF_UP);
			}
			placement.setValue("TransactionCommissionAmt", commAmt);
    	}    	
    }
    
    /** Process reinsurance on the Application.  Set up and place the reinsurance information on the 
     * Application first.  Then calculate the reinsurance premiums and commissions.
     * @param data The data connection
     * @param application The Application ModelBean
     * @param errors The list of errors from processing the reinsurance
     * @throws Exception when an error occurs
     */
    public void processReinsurance(JDBCData data, ModelBean application, ArrayList<ModelBean> errors) 
    throws Exception {
        ModelBean reinsurance = application.getBean("PolicyReinsurance");
        if (reinsurance == null) {
        	reinsurance = new ModelBean("PolicyReinsurance");
        	reinsurance.setValue("CoveredElsewhereInd", "No");
        	application.setValue(reinsurance);
        }
        
		if (!ReinsurancePremiumBase.validate(data, application, errors)) {
			if (errors.size() > 0) {
				return;
			}
		}

        ReinsurancePremiumBase reinBase = new ReinsurancePremiumBase();
        if (reinsurance.gets("CoveredElsewhereInd").isEmpty() || reinsurance.gets("CoveredElsewhereInd").equals("No")) {
            // Call the Reinsurance Premium Detail process to create the Auto Fac layers
            try {
            	reinBase.placePremium(data, application, reinsurance, errors);
            	
            	if (errors.size() > 0) {
            		return; 
            	}
            	
            } catch (PolicyReinsuranceException pre) {
            	ErrorTools.addError("General", pre.getMessage(), ErrorTools.GENERIC_BUSINESS_ERROR, errors);
                return;
            }
                        
            // Call the Reinsurance Placement process to create the Auto placement
            ModelBean[] templates = getTemplates();
            ModelBean[] details = reinsurance.findBeansByFieldValue("PolicyReinsuranceDetail", "StatusCd", "Active");
            details = orderReinsuranceDetails(templates, details);

            for (int i=0; i<details.length; i++) {
                ReinsurancePlacementBase reinPlace = new ReinsurancePlacementBase();
                reinPlace.place(data, application, details[i]);            	
            }
        } else {
            ModelBean[] details = reinsurance.findBeansByFieldValue("PolicyReinsuranceDetail", "StatusCd", "Active");
            for (ModelBean detail : details) {
        		reinBase.deleteDetail(detail);
            }
        }                    
        // Process the written calculations
        calculationWrittenPremium(application);
    }

	/** Obtain all of the Reinsurance templates from the setup
	 * @param templateId The template Id
	 * @return The ModelBean containing the ReinsuranceTemplate
	 * @throws Exception when an error occurs
	 */
	public static ModelBean[] getTemplates()
	throws Exception {
		try {
	        String umol = "::uw-reinsurance-template::ReinsuranceTemplate::*";
	        MDAObject[] objects = Store.getModelObjects(umol,Store.SEARCH_LAST_PACKAGE);
	        
	        ArrayList<ModelBean> templateList = new ArrayList<ModelBean>();
	        for (MDAObject object : objects) {
	        	ModelBean template = ((MDATemplate) object).getBean();
	        	templateList.add(template);
	        }
			return templateList.toArray(new ModelBean[templateList.size()]);
		} catch (Exception e) {
            e.printStackTrace();
            Log.error(e.getMessage(), e);
            throw e;			
		}
	}
	
	/** Order the reinsurance details based on when they should be placed first. This is to insure that if a reinsurance template item inures to another reinsurance 
	 * template item in a different template, that the current reinsurance template or detail is processed first.
	 * @param templates The list of templates to validate ordering on
	 * @param details The list of reinsurance details on the current policy
	 * @return The ordered list of when the reinsurance templates should be processed for inuring purposes
	 * @throws Exception when an error occurs
	 */
	public ModelBean[] orderReinsuranceDetails(final ModelBean[] templates, ModelBean[] details) throws Exception {
		List<ModelBean> sorted = new ArrayList<ModelBean>();
		HashSet<ModelBean> visited = new HashSet<ModelBean>();
		
		for (ModelBean detailItem : details) {
			orderVisit(templates, details, detailItem, visited, sorted);
		}
		
		return sorted.toArray(new ModelBean[sorted.size()]);
	}
	
	/** Determine for the current item, any dependencies it needs, and recurse to find out any dependencies for each of its dependencies.
	 * 
	 * @param templates The entire list of items to verify for dependencies
	 * @param details The list of reinsurance details on the current policy
	 * @param detailItem The current item to obtain dependencies for
	 * @param visited The current list of items already visited
	 * @param sorted The final sorted list from visiting all of the items
	 * @throws Exception when an error occurs
	 */
	protected void orderVisit(ModelBean[] templates, ModelBean[] details, ModelBean detailItem, HashSet<ModelBean> visited, List<ModelBean> sorted) throws Exception {
		if (!visited.contains(detailItem)) {
			visited.add(detailItem);
			
			ModelBean[] dependencies = getDetailDependencies(templates, details, detailItem);
			for (ModelBean dependency : dependencies) {
				orderVisit(templates, details, dependency, visited, sorted);
			}
			
			sorted.add(detailItem);
		} else {
			if (!sorted.contains(detailItem)) {
				throw new Exception("Your Contract Reinsurance Templates have Contracts that depends on each other using the ContractReinsuranceSubject definitions.");
			}
		}
	}
	
	/** Determine the list of dependencies for a specific item
	 * 
	 * @param templates The list of items to look through for dependencies
	 * @param details The list of reinsurance details on the current policy
	 * @param detailItem The current item to look for dependencies
	 * @return The list of dependent items
	 * @throws Exception when an error occurs
	 */
	protected ModelBean[] getDetailDependencies(ModelBean[] templates, ModelBean[] details, ModelBean detailItem) throws Exception {
		HashSet<ModelBean> dependencies = new HashSet<ModelBean>();

		ModelBean template = null;
		for (ModelBean templateItem : templates) {
			if (detailItem.gets("ReinsuranceTemplateIdRef").equals(templateItem.getId())) {
				template = templateItem;
				break;
			}
		}
		
		// Check first for dependencies for reinsurance placements
		ModelBean[] includeItems = template.getAllBeans("ReinsuranceTemplateItemIncludeItem");
		for (ModelBean includeItem : includeItems) {
			for (ModelBean detail : details) {
				if (detail.findBeanByFieldValue("PolicyReinsurancePlacement", "PlacementId", includeItem.gets("TemplateItemId")) != null) {
					dependencies.add(detail);
					break;
				}
			}
		}
		
		includeItems = template.getAllBeans("ReinsuranceTemplateIncludeItem");
		for (ModelBean includeItem : includeItems) {
			for (ModelBean detail : details) {
				if (detail.gets("ReinsuranceTemplateIdRef").equals(includeItem.gets("TemplateId"))) {
					dependencies.add(detail);
					break;
				}
			}
		}

		return dependencies.toArray(new ModelBean[dependencies.size()]);
	}
}
