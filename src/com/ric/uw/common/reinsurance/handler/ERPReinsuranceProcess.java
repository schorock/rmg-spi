/*
 * ReinsuranceProcess.java
 *
 */

package com.ric.uw.common.reinsurance.handler;

import java.util.ArrayList;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;

import com.ric.uw.common.reinsurance.ERPPolicyReinsurance;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;

/** Process the reinsurance calculations in the current Application.
 * This is an ERP buildout that will handle any reinsurance contracts 
 * with ERP.  The premium should not be pro-rated.
 * 
 * This service handler is deprecated because its declaration is no longer service specific.
 * The common service handler ReinsuranceProcess now uses product setup to determine what
 * processor to call for reinsurance processing. This was done because this service handler is 
 * no longer product service specific.
 * @author  allend
 */
@Deprecated
public class ERPReinsuranceProcess extends InnovationIBIZHandler {
    // Create an object for logging messages
    
    
    /** Creates a new instance of ReinsuranceProcess
     * @throws Exception never
     */
    public ERPReinsuranceProcess() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a greeting
            Log.debug("Processing ERPReinsuranceProcess...");
            ModelBean rs = this.getHandlerData().getResponse();
            JDBCData data = this.getHandlerData().getConnection();
            
            ArrayList<ModelBean> errors = new ArrayList<ModelBean>();
            ModelBean application = rs.getBean("Application");
            ERPPolicyReinsurance reinsuranceProcessor = new ERPPolicyReinsurance();
            reinsuranceProcessor.processReinsurance(data, application, errors);

            // Loop Through Validation Errors and Add Them to the Response
            for( int i = 0; i < errors.size(); i++ ) {
                ModelBean errorBean = (ModelBean) errors.get(i);
                addErrorMsg(errorBean.gets("Name"), errorBean.gets("Msg"), errorBean.gets("Type"), errorBean.gets("Severity"));
            }
            
            // If Response has Errors, Throw IBIZException
            if( hasErrors() )
                throw new IBIZException();
                                    
            // Return the response bean
            return null;
        }
        catch( IBIZException e ) {
            throw new IBIZException();
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
    
}

