package com.ric.uw.common.reinsurance.handler;


import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;

import com.ibm.math.BigDecimal;
import com.iscs.uw.common.reinsurance.ReinsurancePlacementPreHandler;

public class ReinsurancePlacementPreSimple implements ReinsurancePlacementPreHandler {

	/** This is a sample rule to demonstrate one of the possible ways to use this rule, such as increasing the PML based on some external factors that are 
	 * not easily defined by the product setup dealing with reinsurance. Shortcuts were taken to accomplish this, and in no way is a representation of what 
	 * a real business rule would do.
	 *  
	 * Rule to allow manipulation of Policy Attaching Reinsurance before Innovation is going to place ceded premium and calculating ceded written premiums.
	 * This rule should be very simple. It allows you to manipulate details such as the total premium gathered during the direct premium phase. So you could 
	 * potentially change the PML by a percentage, or recalculate it completely if needed.
	 * 
	 *  It also allows you to make any placement changes such as adding/deleting reinsurance contracts. If you will also be using the Post rule, remember to not
	 *  duplicate what is it going to do. If you are making changes to the PML or the contracts but want the system to calculate the ceded premiums for you then 
	 *  you want to use this rule. 
	 *  
	 *  If you need to make changes to the ceded premiums based on the resulting reinsurance calculations then you are better off changing it in the post rule.
	 *  
	 * No beans data structure passed to you can be deleted. They must either be marked with a status of Deleted/Cleared, or the amounts appropriately set to
	 * zero or the negation of depending on the transaction. 
	 * @param data The data connection
	 * @param container The container ModelBean holding the Application
	 * @param detail The policy reinsurance detail model. This contains all of the current reinsurance placements that will be processed by the system. You
	 * can manipulate this structure to provide the enhancements needed by your specific reinsurance situation.
	 * @throws Exception when an error occurs
	 */
	public void process(JDBCData data, ModelBean container, ModelBean detail) throws Exception {
		ModelBean basicPolicy = container.getBean("BasicPolicy");
		String productVersionId = basicPolicy.gets("ProductVersionIdRef");

		// NOTE: This rule by default runs for all products. The Product Version test below is for base testing only 
		// and does not represent how a customer build-out should implement this rule.
		if (productVersionId.equals("Commercial-Package-2.00.00")) {
			BigDecimal targetPmlAmt = new BigDecimal("120000");
			BigDecimal pml = detail.getDecimal("ProbableMaxLoss");			
			if (pml.compareTo(targetPmlAmt) == 0) {
				BigDecimal modifiedPmlAmt = new BigDecimal("1100000");
				detail.setValue("ProbableMaxLoss", modifiedPmlAmt);
			}
		}
	}
	
}
