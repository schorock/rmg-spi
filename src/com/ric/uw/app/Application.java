package com.ric.uw.app;

import com.iscs.common.tech.log.Log;
import com.iscs.insurance.product.ProductSetup;
import com.iscs.uw.common.product.UWProductSetup;
import com.iscs.uw.common.risk.Location;
import com.ric.uw.common.risk.Risk;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
public class Application extends com.iscs.uw.app.Application {
	
    /** Get Application ModelBean using a SystemId
     * @param data JDBCData
     * @param systemId Application System Id
     * @return Application ModelBean. Null if not found or exception.
     */    
    public static ModelBean getApplicationBySystemId(JDBCData data, int systemId) {
        try {
        	if( systemId == 0 )
        		return null;
        	
            ModelBean application = new ModelBean("Application");
            
            data.selectModelBean(application, systemId);
            
            return application;
            
        }
        catch( Exception e ) {
            Log.error(e.getMessage(), e);
            e.printStackTrace();
            return null;
        }
    }
    
    /** Add Default Location to the Application
     * @param application ModelBean
     * @throws Exception if an Error Occurs
     */
    public static void addDefaultLocation(ModelBean application)
    throws Exception {
    	addDefaultLocation(application, "");
    }
    
    /** Add Default Location to the Application
     * @param application ModelBean
     * @param stateIdRef String
     * @throws Exception if an Error Occurs
     */
    public static void addDefaultLocation(ModelBean application, String StateIdRef)
    throws Exception {
        ModelBean[] risks = application.getAllBeans("Risk");
        ModelBean location = null;
        // obtain the product setup for validations
        ModelBean productSetup = getProductSetup(application);
        
        
        // If the insured address and controlling state do not match, do not create a default location
        String productVersionIdRef = application.getBean("BasicPolicy").gets("ProductVersionIdRef");  
        if(	UWProductSetup.isMultiStatePolicy(productVersionIdRef) ) {
        	String controllingState = application.getBean("BasicPolicy").gets("ControllingStateCd");
        	ModelBean insuredLookupAddr = application.getBeanByAlias("InsuredLookupAddr");
        	if( insuredLookupAddr != null && !insuredLookupAddr.gets("StateProvCd").equals(controllingState) ) { 
        		return;
        	}
        }        
        
        if (risks.length == 0) {
        	// There are no risks, so add the default location without tieing risk data
   	       	location = addLocation(application,StateIdRef);
        } else {
        	// There are risks, so add the default location and link if any of their respective lines are property (RiskBeanName=Building)
	        for (int cnt=0; cnt<risks.length; cnt++ ) {
	        	ModelBean line = risks[cnt].getParentBean();
	        	if(line != null) {
		        	ModelBean productLine = productSetup.findBeanByFieldValue("ProductLine", "LineCd", line.gets("LineCd") );
		        	if(productLine != null) {
			        	if(productLine.valueEquals("RiskBeanName", "Building")) {
			                if (location == null) {
			                    // location is null, create a new one
			                	location = addLocation(application,StateIdRef);
			                }
			                risks[cnt].setValue("LocationRef", location.getId() );
			                // Default the risk address with the location address
			                Risk.setDefaultAddress(application, risks[cnt]);			            	
			            }
		        	}
	        	}
	        }
        }
    }
    
    /** Add a new location to the current application
     * @param application ModelBean holding the current application
     * @return The Location ModelBean
     * @throws Exception when an error occurs
     */
    private static ModelBean addLocation(ModelBean application,String stateIdRef)
    throws Exception {
        Location locSetup = new Location();
        ModelBean location = locSetup.create();
        application.addValue("Location", location);
        Location.setDefaultAddress(application, location );
        // Increment the location number
        int nextNum = Location.nextNumber(application, location);
        location.setValue("LocationNumber", Integer.toString(nextNum) );
        // Set the status to Active
        location.setValue("Status", "Active");
        //get the default location in an application or quote from product list.xml file if not setup then use default location as Property Location
        ModelBean basicPolicy = application.getBean("BasicPolicy");
    	ModelBean coderef = ProductSetup.getProductCoderef(basicPolicy.gets("productVersionIdRef"), "UW::underwriting::default-location-description::all");    	           
    	    if(coderef!=null){
    	    	ModelBean[] options = coderef.getBean("Options").getBeans("Option");
    	        for( ModelBean option : options ) {    	        	
    	        	location.setValue("LocationDesc", option.gets("Value"));
    				break;
    	         }
    	    } else{
    	    	location.setValue("LocationDesc", "Property Location");
    	    }

        location.setValue("CoveredStateIdRef",stateIdRef);
        
        // set the default location product version if multi-state product
        String productVersionIdRef = application.getBean("BasicPolicy").gets("ProductVersionIdRef");
        if( UWProductSetup.isMultiStatePolicy(productVersionIdRef) ){
	        location.setValue("ProductVersionIdRef", productVersionIdRef);
	        ModelBean addr = location.getBeanByAlias("LocationAddr");
	        if( addr!=null){
	        	if( addr.gets("StateProvCd").equals("")){
	        		//ModelBean basicPolicy = application.getBean("BasicPolicy");
	        		addr.setValue("StateProvCd", basicPolicy.gets("ControllingStateCd"));
	        	}
	        }
        }
        return location;
    }
}
