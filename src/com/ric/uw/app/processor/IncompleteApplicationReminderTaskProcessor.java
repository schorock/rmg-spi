package com.ric.uw.app.processor;

import java.sql.SQLException;
import java.util.List;

import com.iscs.common.tech.log.Log;
import com.iscs.common.tech.service.ModelBeanProcessorBaseSPI;
import com.iscs.common.utility.DateTools;
import com.iscs.workflow.Task;
import com.ric.insurance.product.render.RICProductRenderer;

import net.inov.biz.server.IBIZXmlController;
import net.inov.tec.batch.MBPException;
import net.inov.tec.batch.MBPLookupSpec;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

/**
 * @author rhoush
 */
public class IncompleteApplicationReminderTaskProcessor extends ModelBeanProcessorBaseSPI {

  public IncompleteApplicationReminderTaskProcessor() throws Exception {
    super();
    this.spec = ModelSpecification.getSharedModel();
  }

  /** Bean processor main routine.  Each bean in the data repository table will be presented to this processor.  The criteria
   * for task escalation will be handled and then a resultant sucess or fail indicator will be passed back.
   * @param application Each application bean in the data repository that is passed for evaluation
   * @param data JDBC data repository connection
   * @return boolean of whether this bean has been processed or not
   * @throws MBPException when an error has occurred.
   */
  public boolean processBean(ModelBean application, JDBCData data) throws MBPException {
    try {
      // Log a Greeting
      Log.debug("Processing IncompleteApplicationReminderTaskProcessor...");
      List<String> user = RICProductRenderer.getUserFromProviderRef( application.getBean("BasicPolicy").gets("ProviderRef") );
      ModelBean task = Task.getTask(data, "IncompleteApplicationReminderTask", application, "Open");
      
      if( application.getBean("BasicPolicy").gets("ProviderRef").isEmpty() || task != null )
        return true;

      task = Task.createTask(data, "IncompleteApplicationReminderTask", new ModelBean[] {application}, null, null,  getRunDate(), null, "");
      task.setValue("CurrentOwner", user.get(0));
      task.setValue("CurrentOwnerCd", user.get(1));
      task.setValue("OriginalOwner", user.get(0));
      task.setValue("OriginalOwnerCd", user.get(1));
		
      // Save Task
      Task.insertTask(data, task);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  /** Data repository selection criteria.
   *  To look up any InProcess, NewBusiness Quote having EffectiveDt older than the runDt - ExpireDays (which is configured in the job.xml)
   * @return MBPLookupSpec The lookup specification object containing the fields to be used for data selection
   * @throws MBPException when an error occurs
   */
  public MBPLookupSpec getLookupSpec() throws MBPException {
    try {
      StringDate runDt = getRunDate();
      return new MBPLookupSpec(
          new String[] { "TypeCd", "Status", "EffectiveDt" },
          new String[] { "!=", "=", "=" },
          new String[] {"QuickQuote", "InProcess", runDt.toString() });
    } catch (Exception e) {
      throw new MBPException(e);
    }
  }

  /** Find out the run date of the current batch process
   * @return StringDate The date of the current batch run is set to.
   * @throws Exception when an error occurs
   */
  public StringDate getRunDate() throws Exception {
    // Find if the run date has been passed in
    if (this.getParam("RunDt", "").equals("")) {
      return DateTools.getStringDate();
    } else {
      return new StringDate(this.getParam("RunDt"));
    }
  }

  protected ModelBean callService(ModelBean rq)
      throws Exception {

    // Call the service
    return IBIZXmlController.processRequest(rq, true);
  }

  /** Set up the processor before the batch job is executed.
   * <BR><BR>
   * Instance variables runDt, myName, and templateId in base TaskReminderProcess are private, so must get my own.
   *
   * @throws Exception when an error occurs
   */
  public void initialCall() throws MBPException {
    try {
      // Set the error reporting to use the new JobProgress class instead of immediately
      // reporting to the Processor table.
      this.setReportErrorsImmediately(false);
    } catch (Exception e) {
      // If the job has been entered into the process table, go ahead and mark it as an error
      try {
        jobActionProgress.addExceptionError(e.getMessage());
        jobActionProgress.updateJobActionStatus("Error");
      } catch (Exception ec) {
        throw new MBPException(ec);
      }

      throw new MBPException(e);
    }
  }

  /** Update the process control manager: this process is now complete.
   * @param beanData The data repository connection
   * @throws MBPException when an error occurs
   */
  public void finalCall (JDBCData beanData) throws MBPException {
    try {
      //  Make an entry in the processor table to show that we have finished.
      //  This is for information purposes only.
      jobActionProgress.updateJobActionStatus("Complete");
    } catch (SQLException sqle) {
      // Do not throw an exception if its because the result of the search is empty
      if (sqle.getErrorCode() != 0)
        throw new MBPException(sqle);
    } catch (Exception e) {
      throw new MBPException(e);
    }
  }
}
