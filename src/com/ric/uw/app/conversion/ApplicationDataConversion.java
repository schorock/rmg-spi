package com.ric.uw.app.conversion;

import net.inov.biz.server.ServiceContext;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.math.Money;
import net.inov.tec.xml.XmlDoc;
import org.jdom.Element;

import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.conversion.DataConversionHelper;
import com.iscs.uw.policy.PM;

public class ApplicationDataConversion extends com.iscs.uw.app.conversion.ApplicationDataConversion {

	// Removed "RateArea" from premiumBeans because RateAreas are generated on runtime
	protected String[] premiumBeans = new String[] {"Line", "Risk", "Coverage", "CoverageItem", "Fee", "BasicPolicy", "TransactionHistory", "CommissionArea"};
	
	public ApplicationDataConversion()
	throws Exception {
		super();
	}
	
	public boolean validatePremium(ModelBean dtoBean, ModelBean originalBean, ModelBean response) 
		    throws Exception {
		    	boolean isValid = true;
		    	for (int i=0; i<premiumBeans.length; i++) {
		    		ModelBean[] beans = originalBean.getAllBeans(premiumBeans[i]);
		    		for (int k=0; k<beans.length; k++) {
		    			// For this type of Bean, find the matching Bean in the processed bean and validate its value
		    			// if present in the original Bean
		    			for (int x=0; x<fixedAmts.length; x++) {
		        			if (!beans[k].hasBeanField(fixedAmts[x]))
		        				continue;
		        			String value = beans[k].gets(fixedAmts[x], null);
		        			if (value != null) {
		            			Money oldAmt = new Money(value);
		            			ModelBean foundBean = findMatchingBean(beans[k], response);
		            			if (foundBean != null) {
		                			Money newAmt = new Money(foundBean.gets(fixedAmts[x]));
		                			if (oldAmt.compareTo(newAmt) != 0) {
		                    			addErrorMsg("ValidationError", "There is a difference in the amounts for node DTO" + beans[k].getBeanName() + " and the attribute " + fixedAmts[x] + ".  Old value is " + oldAmt + " and new value is " + newAmt, ErrorTools.FIELD_CONSTRAINT_ERROR);
		                    			isValid = false;
		                			}
		            			}
		        			}    				
		    			}
		    		}
		    	}
		    	return isValid;
		    }

	/** Compare all of the existence ModelBeans to make sure no new Beans are created, or possibly were not 
     * reported on a future transaction.  (i.e. The continuing transactions did not report a ModelBean that was
     * deleted, deleted in the old system but not reported as deleted in this conversion) 
     * @param dtoBean The DTO ModelBean
     * @param originalBean The original converted ModelBean
     * @param response The response from the service chain call
     * @return true/false if the existence check is valid
     * @throws Exception when an error occurs
     */
    public boolean validateBeanExist(ModelBean dtoBean, ModelBean originalBean, ModelBean response) 
    throws Exception {
    	boolean isValid = true;
    	for (int i=0; i<existBeans.length; i++) {
    		ModelBean[] beans = response.getAllBeans(premiumBeans[i]);
    		for (int k=0; k<beans.length; k++) {
    			// For this type of Bean, find the matching Bean in the processed bean and validate its status
    			// if present in the original Bean
    			String statusField = null;
    			if (!beans[k].hasBeanField("Status") && !beans[k].hasBeanField("StatusCd")) {
    				continue;    				
    			} else {
    				if (beans[k].hasBeanField("Status"))
    					statusField = "Status";
    				else 
    					statusField = "StatusCd";    						
    			}
    			String statusCd = beans[k].gets(statusField);
    			if (statusCd.equals("Active")) {
        			ModelBean foundBean = findMatchingBean(beans[k], originalBean);
        			if (foundBean != null) {
        				if (!statusCd.equals(foundBean.gets(statusField))) {
                			addErrorMsg("ValidationError", "The bean " + foundBean.getBeanName() + " with id of " + foundBean.getId() + " has a status of " + foundBean.gets(statusField) + " which does not match the current processed status of " + statusCd, ErrorTools.FIELD_CONSTRAINT_ERROR);
                			isValid = false;
        				}
        			} else {
            			addErrorMsg("ValidationError", "The bean " + beans[k].getBeanName() + " with id of " + beans[k].getId() + " should exist in the incoming DTO model, but does not at all. ", ErrorTools.FIELD_CONSTRAINT_ERROR);
            			isValid = false;
        			}    				
    			} else {
    				ModelBean foundBean = originalBean.getBeanById(beans[k].getId());
    				if (foundBean != null && foundBean.gets(statusField).equals("Active")) {
            			addErrorMsg("ValidationError", "The bean " + foundBean.getBeanName() + " with id of " + foundBean.getId() + " has a status of " + foundBean.gets(statusField) + " which does not match the current processed status of " + statusCd, ErrorTools.FIELD_CONSTRAINT_ERROR);    					
            			isValid = false;
    				}
    			}
    		}    		
    	}
    	return isValid;
    	
    }
    
    protected String findMatchingBeanCode(String beanName) {
    	if (beanName.equals("Line")) {
    		return "LineCd";
    	} else if (beanName.equals("Risk")) {
    		return "TypeCd";
    	} else if (beanName.equals("Coverage")) {
    		return "CoverageCd";
    	} else if (beanName.equals("CoverageItem")) {
    		return "CoverageItemCd";
    	} else if (beanName.equals("RateArea")) {
    		return "AreaName";
    	} else if (beanName.equals("Fee")) {
    		return "CategoryCd";
    	}
    	return null;
    }
    
    /** Determine if the current transaction passed in is allowable to the current system.  The following rules
     * determine if a transaction is allowed:
     * 1) The policy number passed in from BasicPolicy.PolicyNumber and BasicPolicy.PolicyVersion does not 
     * exist in the system current.
     * 2) The policy number exists in the system, and the current transaction number is greater than what is 
     * in the system
     * @param dtoBean The DTOApplication ModelBean
     * @return true/false if the Application should continue to process
     * @throws Exception when an error occurs
     */
    protected boolean validateTransaction(ModelBean dtoBean)
    throws Exception {
    	ModelBean basicPolicy = dtoBean.getBean("DTOBasicPolicy");
    	boolean isValid = true;
    	// Validate the current Transaction
    	ModelBean transactionInfo = dtoBean.getBean("DTOTransactionInfo");
    	if (!DataConversionHelper.validateField(transactionInfo, "SourceCd", null, true, getErrors())) {
    		isValid = false;
    	}
    	    	
    	// Validate the basic policy information if Source is different than Innovation
    	if (!transactionInfo.gets("SourceCd").equals(PM.SOURCE_CODE)) {
//        	if (!DataConversionHelper.validateField(basicPolicy, "PolicyNumber", null, true, getErrors()))
//        		isValid = false;
//        	if (!DataConversionHelper.validateField(basicPolicy, "PolicyDisplayNumber", null, true, getErrors()))
//        		isValid = false;
        	if (!DataConversionHelper.validateField(basicPolicy, "PolicyVersion", null, true, getErrors()))
        		isValid = false;
        	if (isValid) {
        		JDBCData data = ServiceContext.getServiceContext().getData();
        		ModelBean policy = findBean(data, basicPolicy.gets("PolicyDisplayNumber"));
        		if (policy != null) {
        			// Determine if the current Transaction to convert is the next valid transaction in Innovation
        			ModelBean basicP = policy.getBean("BasicPolicy");
        			int sysTransNo = Integer.parseInt(basicP.gets("TransactionNumber"));
        			int convTransNo = Integer.parseInt(basicPolicy.gets("TransactionNumber"));
        			if (convTransNo != sysTransNo + 1) { 
        				isValid = false;
        				String msg = "The current transaction to convert, " + convTransNo + " is not the next transaction in Innovation, which should be " + (sysTransNo+1);
        				addErrorMsg("ValidationError", msg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
        			}
        			// Make sure the policy has not been touched/updated in Innovation
        			ModelBean[] transactions = basicP.findBeansByFieldValue("TransactionHistory", "SourceCd", PM.SOURCE_CODE);
        			if (transactions.length > 0) {
        				isValid = false;
        				String msg = "The current transaction to convert, " + convTransNo + " for policy " + basicPolicy.gets("PolicyDisplayNumber") + " is not valid because the policy has been updated within Innovation";
        				addErrorMsg("ValidationError", msg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);        				
        			}        			
        			
        			// If this is a Reapply transaction, only reapply in order
        	    	String replaceTransNo = transactionInfo.gets("ReplacementOfTransactionNumber");
        	    	if (!replaceTransNo.equals("")) {
        	    		ModelBean[] changes = BeanTools.sortBeansBy(policy.getBeans("ChangeInfo"), new String[] {"TransactionNumber"}, new String[] {"Ascending"});
        	    		for (int i=0; i<changes.length; i++) {
        	    			if (changes[i].gets("Status").equals("Unapplied")) {
        	    				if (!changes[i].gets("TransactionNumber").equals(replaceTransNo)) {
        	    					isValid = false;
        	    					String msg = "The current transaction " + replaceTransNo + " to Reapply, must be done in order off all reapplies required.  The first reapply transaction is transaction number " + changes[i].gets("TransactionNumber");
        	        				addErrorMsg("ValidationError", msg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);        	    					
        	    				}
        	    				break;
        	    			}
        	    		}
        	    	}        	    	
        		} else {
        			// If the policy cannot be found, make sure its the first Transaction.
        			int convTransNo = Integer.parseInt(basicPolicy.gets("TransactionNumber"));
        			if (convTransNo > 1) {
        				isValid = false;
        				String msg = "The current transaction number of " + convTransNo + " indicates it is not the first transaction.  However the policy cannot be located in the system.";
        				addErrorMsg("ValidationError", msg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
        			}        				
        		}
        	}   

        	// Validate if the Transaction is a renewal or rewrite it must have a policy reference number
        	if ((transactionInfo.gets("TransactionCd").equals(PM.TX_RENEWAL) || transactionInfo.gets("TransactionCd").equals(PM.TX_RENEWAL_START)) 
        			&& basicPolicy.gets("RenewedFromPolicyNumber").equals("")) {
				isValid = false;
				String msg = "The current renewal transaction must reference a previous Policy Number in the DTOBasicPolicy.RenewedFromPolicyNumber node/attribute";
				addErrorMsg("ValidationError", msg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
        	} else if ((!transactionInfo.gets("TransactionCd").equals(PM.TX_RENEWAL) && !transactionInfo.gets("TransactionCd").equals(PM.TX_RENEWAL_START)) 
        			&& !basicPolicy.gets("RenewedFromPolicyNumber").equals("")) {
				isValid = false;
				String msg = "The DTOBasicPolicy.RenewedFromPolicyNumber node/attribute can only be specified on Renewal and Renewal Start transactions";
				addErrorMsg("ValidationError", msg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);        		
        	} else if ((transactionInfo.gets("TransactionCd").equals(PM.TX_REWRITE_NEW) || transactionInfo.gets("TransactionCd").equals(PM.TX_REWRITE_RENEWAL)) 
        			    && basicPolicy.gets("RewriteFromPolicyNumber").equals("")) {
				isValid = false;
				String msg = "The current rewrite transaction must reference a previous Policy Number in the DTOBasicPolicy.RewriteFromPolicyNumber node/attribute";
				addErrorMsg("ValidationError", msg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
        	} else if ((!transactionInfo.gets("TransactionCd").equals(PM.TX_REWRITE_NEW) && !transactionInfo.gets("TransactionCd").equals(PM.TX_REWRITE_RENEWAL)) 
        			    && !basicPolicy.gets("RewriteFromPolicyNumber").equals("")) {
				isValid = false;
				String msg = "The DTOBasicPolicy.RewriteFromPolicyNumber node/attribute can only be specified on Rewrite New and Rewrite Renewal transactions";
				addErrorMsg("ValidationError", msg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
        	}
        	
        	// Validate that prior term pending changes fields cannot be set
        	if (!basicPolicy.gets("PriorTermPendingChangeTransactionNumber").equals("") || !basicPolicy.gets("PriorTermPendingChangePolicyRef").equals("")) {
				isValid = false;
				String msg = "The DTOBasicPolicy.PriorTermPendingChangeTransactionNumber and/or DTOBasicPolicy.PriorTermPendingChangePolicyRef node/attribute can not be set directly.";
				addErrorMsg("ValidationError", msg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);        		
        	}
    	}
    	return isValid;
    }

    /** Add one row of the 'Application' specific data to the audit report
	 * @param xmlDoc Original XML conversion document
	 * @param resultBean Result of the conversion process
	 * @return String array of Container specific audit data
	 */
	public String[] getAuditData(XmlDoc xmlDoc, ModelBean resultBean) throws Exception {
		
		// Initialize return values
		String typeCd = "";
		String number = "";
		String conversionContext="";
		
		// Populate return values
		Element application = xmlDoc.getRootElement();		
		typeCd = application.getAttributeValue("TypeCd");	
		
		if( resultBean != null ) {
			number = resultBean.gets("ApplicationNumber");
		}
		if(resultBean != null && resultBean.getBean("BasicPolicy") != null){
		conversionContext =  resultBean.getBean("BasicPolicy").gets("ConversionContext");
		}else{
			if(xmlDoc != null){
        		if(xmlDoc.selectSingleElement("//DTOBasicPolicy") != null)
            		conversionContext = xmlDoc.selectSingleElement("//DTOBasicPolicy").getAttribute("ConversionContext").getValue();
			}
		}
		if(conversionContext == null)
		conversionContext = "";
		
		// Return data
		return new String[] {typeCd,number,conversionContext};
	}

	/** Set up the audit report with the 'Application' specific column headers
	 * @return String array of Container specific audit headers
	 */
	public String[] getAuditHeader() throws Exception {
		
		return new String[] {"TypeCd","Number","ConversionContext"};
	}

}
