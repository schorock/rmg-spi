package com.ric.uw.app.handler;

/*
 * ApplicationCPCLUEPropertyDataReportApply.java
 *
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Hashtable;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

import com.ibm.math.BigDecimal;
import com.iscs.common.business.datareport.DataReport;
import com.iscs.common.business.logging.Logger;
import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.uw.common.shared.LossHistory;

/** Applies the CP CLUE Personal Property Data Report Response Data to the Application
 *
 * @author moniquef
 */
public class ApplicationCPCLUEPropertyDataReportApply implements RuleProcessor  {

	/** Creates a new instance of ApplicationCPCLUEPropertyDataReportApply
	 * @throws Exception never
	 */
	public ApplicationCPCLUEPropertyDataReportApply() throws Exception {
	}

	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user, JDBCData data) throws RuleException {
		try {

			// Initialize ModelBean Variables
			ModelBean application = bean;
			ModelBean dataReport = BeanTools.searchArray("DataReport", additionalBeans);
			ModelBean dataReportRequest = BeanTools.searchArray("DataReportRequest", additionalBeans); 

			// Skip Processing if Application is Deleted or DataReportRequest is Cancelled
			if( application.valueEquals("Status", "Deleted") || dataReportRequest.valueEquals("StatusCd", "Cancelled") )
				return application;

			// Loop All Through CP Claims
			ModelBean[] cpClaimArray = sortByClaimDt(dataReport.getAllBeans("CPClaim"), "Ascending");
			for( ModelBean cpClaim : cpClaimArray ) {
				boolean clueMatch = false;
				if(cpClaim==null){
						continue;
				}
				ModelBean cpLoss = cpClaim.getBean("CPLoss");
				ModelBean cpPolicy = cpClaim.getBean("CPPolicy");
				StringDate cpLossDt = cpLoss.getDate("ClaimDt");
				String cpClaimNumber = cpLoss.gets("ContributorClaimNumber");

				// For Each CP Claim, See if There is a Matching Loss History
				ModelBean[] lossHistoryArray = application.findBeansByFieldValue("LossHistory", "StatusCd", "Active");
				for( ModelBean lossHistory : lossHistoryArray ) {

					StringDate lossDt = lossHistory.getDate("LossDt");
					String claimNumber = lossHistory.gets("ClaimNumber");

					if( lossHistory.valueEquals("SourceCd", "Application") ) {
						// Match Date Range
						StringDate startDt = StringDate.advanceDate(cpLossDt, -30);
						StringDate endDt = StringDate.advanceDate(cpLossDt, 30);
						if( (lossDt.compareTo(startDt) >= 0) && (lossDt.compareTo(endDt) <= 0) ) {

							// Match Claim Number
							if( claimNumber.equals(cpClaimNumber) ) {
								lossHistory.setValue("IgnoreInd", "Yes");
							}
						}	
					} else if( lossHistory.valueEquals("SourceCd", "CLUE") ) {
						// Match Exact Date
						if( lossDt.compareTo(cpLossDt) == 0 ) { 

							// Match Claim Number
							if( claimNumber.equals(cpClaimNumber) ) {
								clueMatch = true;
								continue;
							}
						}
					}
				}
				// If an CLUE Match was not Found
				if( !clueMatch ) { 
					// Process Claim Payment ModelBeans
					String paidAmt = "0";
					String maxPaymentAmt = "0";
					String lossCauseCd = "";
					String lossDesc = "";
					Hashtable<String, BigDecimal> claimTypeCdHash = new Hashtable<String, BigDecimal>();
					ModelBean[] cpClaimPaymentArray = cpClaim.getBeansSorted("CPClaimPayment", "ClaimTypeCd", "Ascending");
					String claimType = cpClaim.gets("ClaimTypeCd");
					for( ModelBean cpClaimPayment : cpClaimPaymentArray ) {

						// Calculate Total Paid Amount
						String cpPaymentAmt = cpClaimPayment.gets("PaymentAmt");
						paidAmt = StringTools.addMoney(paidAmt, cpPaymentAmt);

						// Use the Claim Type with the Largest Payment as the Loss Cause
						if( StringTools.greaterThanEqual(cpPaymentAmt, maxPaymentAmt) ) {
							lossCauseCd = cpClaimPayment.gets("ClaimTypeDisplay");
							maxPaymentAmt = cpPaymentAmt;
						}	

						// Track Payments in Hashtable for the Loss Description
						String claimTypeCd = cpClaimPayment.gets("ClaimTypeCd");
						if( !claimTypeCdHash.containsKey(claimTypeCd) ) {
							// Put Claim Type in the Hashtable
							claimTypeCdHash.put(claimTypeCd, cpClaimPayment.getDecimal("PaymentAmt"));
						} else {
							// Add Payment Amount to Existing Amount in the Hashtable
							BigDecimal paymentAmtTotal = new BigDecimal(claimTypeCdHash.get(claimTypeCd).toString());
							paymentAmtTotal = paymentAmtTotal.add(cpClaimPayment.getDecimal("PaymentAmt"));
							claimTypeCdHash.put(claimTypeCd, paymentAmtTotal);
						}	
					}

					// Build Loss Description
					Enumeration<String> list = claimTypeCdHash.keys();
					while( list.hasMoreElements() ) {
						String claimTypeCd = (String) list.nextElement();
						String paymentAmt = claimTypeCdHash.get(claimTypeCd).toString();
						String claimTypeDesc = claimTypeCd + "(" + StringTools.formatMoney(paymentAmt, true, true, true)+ ")";
						lossDesc = StringTools.concat(lossDesc, claimTypeDesc, ", "); 
					}
					boolean subInd = false;
					boolean claimTypeInd = false;
					// Add Loss History
					ModelBean lossHistory = new ModelBean("LossHistory");
					lossHistory.setValue("StatusCd", "Active");
					lossHistory.setValue("LossHistoryNumber", Integer.toString(LossHistory.nextNumber(application)));
					lossHistory.setValue("SourceCd", "CLUE");
					lossHistory.setValue("LossDt", cpLossDt);
					lossHistory.setValue("LossCauseCd", lossCauseCd);
					lossHistory.setValue("LossAmt", "");
					lossHistory.setValue("LossDesc", lossDesc);
					lossHistory.setValue("AtFaultCd", cpLoss.gets("AtFaultDisplay"));
					lossHistory.setValue("ClaimNumber", cpClaimNumber);
					if( cpLoss.valueEquals("DispositionDisplay", "Open/Active") )
						lossHistory.setValue("ClaimStatusCd", "Open");
					else	
						lossHistory.setValue("ClaimStatusCd", cpLoss.gets("DispositionDisplay"));
					if(cpLoss.valueEquals("DispositionDisplay","Subrogation")){
						subInd = true;
						lossHistory.setValue("IgnoreInd", "Yes");
						lossHistory.setValue("Comment", "Loss was subrogated");
					}
					if(claimType.equalsIgnoreCase("Insured Section Claim")||claimType.equals("")){
						claimTypeInd = true;
						lossHistory.setValue("IgnoreInd", "Yes");
						lossHistory.setValue("Comment", "Loss associated with the insured");
					}
					if(subInd && claimTypeInd){
						lossHistory.setValue("Comment", "Loss associated with the insured and loss was subrogated");
					}
					lossHistory.setValue("PolicyNumber", cpPolicy.gets("PolicyNumber"));
					lossHistory.setValue("PolicyTypeCd", cpPolicy.gets("TypeDisplay"));
					lossHistory.setValue("CarrierName", cpPolicy.gets("CompanyName"));
					lossHistory.setValue("PaidAmt", paidAmt);
					lossHistory.setValue("TypeCd", "Property");
					if( cpClaim.valueEquals("ClaimTypeCd", "Risk Section Claim") && !StringTools.isTrue(cpLoss.gets("PossibleRelatedClaimInd")) ) {
						lossHistory.setValue("RiskIdRef", dataReportRequest.gets("SourceIdRef"));
					} 
					ModelBean cpAddress = cpClaim.getBean("CPAddress", "ClassDisplay", "Risk Address");
					if( cpAddress != null ) {
						String addressLine = cpAddress.gets("PrimaryNumber");
						addressLine = StringTools.concat(addressLine.trim(), cpAddress.gets("StreetName"), " ");
						addressLine = StringTools.concat(addressLine.trim(), cpAddress.gets("SecondaryNumber"), " ");
						addressLine = StringTools.concat(addressLine.trim(), cpAddress.gets("City"), ", ");
						addressLine = StringTools.concat(addressLine.trim(), cpAddress.gets("StateProvCd"), ", ");
						addressLine = StringTools.concat(addressLine.trim(), cpAddress.gets("PostalCode"), " ");
						if(!subInd&&!claimTypeInd)
							lossHistory.setValue("Comment", addressLine);

					}

					application.addValue(lossHistory);
				}
			}

			// Process Current Carrier Information  
			ModelBean cpCurrentCarrier = dataReport.getBean("CPCLUEPropertyReport").getBean("CPCurrentCarrier");
			if( cpCurrentCarrier != null ) {
				ModelBean basicPolicy = application.getBean("BasicPolicy");
				ModelBean cpPriorPolicy = cpCurrentCarrier.getBean("CPPriorPolicy");
				if( cpPriorPolicy != null ) {
					basicPolicy.setValue("PreviousCarrierCd", cpPriorPolicy.gets("CarrierName"));
					basicPolicy.setValue("PreviousPolicyNumber", cpPriorPolicy.gets("PolicyNumber"));
				}
				ModelBean cpPriorPolicyInsured = cpPriorPolicy.getBean("CPPriorPolicyInsured");
				if( cpPriorPolicyInsured != null ) {
					basicPolicy.setValue("PreviousExpirationDt", cpPriorPolicyInsured.getValue("EndDt"));
				}
			}

			// Log Message 
			StringDate todayDt = DateTools.getStringDate();
			String logKey = "Application::" + application.getSystemId();
			ModelBean[] beans = DataReport.getAssociatedBeans(data, dataReportRequest, logKey);
			if( application.valueEquals("TypeCd", "Quote") )
				Logger.message(data, "UWQuoteAppliedDataReport", "System", todayDt, beans);
			else if( application.valueEquals("TypeCd", "Application") )
				Logger.message(data, "UWApplicationAppliedDataReport", "System", todayDt, beans);

			// Set the DataReportRequest ModelBean Date, and Time Applied
			dataReportRequest.setValue("AppliedDt", DateTools.getStringDate());
			dataReportRequest.setValue("AppliedTm", DateTools.getTime("HH:mm:ss z"));           

			// Set the DataReportRequest ModelBean Status to Applied
			dataReportRequest.setValue("StatusCd", "Applied");

			// Update Application ModelBean 
			BeanTools.saveBean(application, data);

			return application;
		}
		catch (Exception e) {
			Log.error(e.getMessage(), e);
			throw new RuleException(e);
		}
	}

	/** Sort an array of ModelBeans
	 * @param beans Model bean array to sort
	 * @param sortOrder Ascending/Descending order
	 * @return sorted ModelBean array
	 */
	public static ModelBean[] sortByClaimDt(ModelBean[] beans, String sortOrder) {

		final String sort = sortOrder;

		ArrayList<ModelBean> list = new ArrayList<ModelBean>();

		for(int i = 0;i < beans.length; i++) {
			list.add(beans[i]);
		}

		Collections.sort(list, new Comparator<Object>() {

			public int compare(Object obj1, Object obj2) throws ClassCastException {
				try {
					ModelBean bean1  = (ModelBean) obj1;
					ModelBean bean2  = (ModelBean) obj2;

					StringDate date1 = bean1.getBean("CPLoss").getDate("ClaimDt");
					StringDate date2 = bean2.getBean("CPLoss").getDate("ClaimDt");

					if( sort.equals("Ascending") )
						return date1.compareTo(date2);
					else
						return date2.compareTo(date1);

				}
				catch (ModelBeanException mbe) {
					throw new ClassCastException("Error occurred trying to compare fields for sorting." + mbe.toString() );
				}
			}
		});

		ModelBean[] sortedList = new ModelBean[list.size()];
		list.toArray(sortedList);

		return sortedList;
	}
}