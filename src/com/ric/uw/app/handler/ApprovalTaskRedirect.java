package com.ric.uw.app.handler;

import com.iscs.common.render.DateRenderer;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.workflow.Task;
import com.iscs.workflow.render.WorkflowRenderer;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;

public class ApprovalTaskRedirect extends InnovationIBIZHandler {

	public ApprovalTaskRedirect() throws Exception {
		super();
	}

	/**
	 * Process approvals to check to see if redirect to Policy Services task group is needed.
	 * 
	 * @author rhoush
	 */
	public ModelBean process() throws IBIZException, ServiceHandlerException {
		try {
			// Log a Greeting
			Log.debug("Processing " + this.getClass().getSimpleName());

			JDBCData data = this.getConnection();
			ModelBean rs = this.getHandlerData().getResponse();
			ModelBean cmm = rs.getBean("ResponseParams").getBean("CMMParams");
			ModelBean application = rs.getBean(cmm.gets("Container"));
			ModelBean[] approvals = application.findBeansByFieldValue("ValidationError", "TypeCd", "Approval");
			ModelBean task = Task.getTask(data, "ApplicationTask2002", application, "Open");
			Boolean found = false;			
			
			for( ModelBean approval : approvals )
				found = found || approval.gets("Msg").contains("Review dog breed") || approval.gets("Msg").contains("Protection class change requires approval");
			
			if( found && task != null )
				WorkflowRenderer.changeOwnership(task, "System", "PolicyServices", "Group", "Transfer", "System Comment - Task owner changed by approvals redirect", DateRenderer.getStringDate(rs), DateRenderer.getTime(rs));
			
			// Return the Response ModelBean
			return rs;
		} catch (Exception e) {
			throw new ServiceHandlerException(e);
		}
	}
}