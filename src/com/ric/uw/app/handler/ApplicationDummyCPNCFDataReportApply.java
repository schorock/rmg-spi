package com.ric.uw.app.handler;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.tech.log.Log;

public class ApplicationDummyCPNCFDataReportApply implements RuleProcessor {

	/**
	 * Creates a new instance of ApplicationDummyCPNCFDataReportApply
	 * 
	 * @throws Exception
	 */
	public ApplicationDummyCPNCFDataReportApply() throws Exception {
	}

	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user,
			JDBCData data) throws RuleException {
		ModelBean errors = null;
		try {
			errors = new ModelBean("Errors");
			return errors;
		} catch (Exception e) {
			Log.error(e.getMessage(), e);
			throw new RuleException(e);
		}
	}

}