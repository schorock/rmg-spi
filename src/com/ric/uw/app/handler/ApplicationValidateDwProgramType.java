package com.ric.uw.app.handler;

import com.iscs.common.render.StringRenderer;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.validation.render.ValidationRenderer;
import com.iscs.uw.common.rating.RatingBase;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;

public class ApplicationValidateDwProgramType extends InnovationIBIZHandler {

	private static final String ERR_MESSAGE = "Please revisit the Dwelling chevron and complete information related to the program change to receive an accurate premium.";
	
	public ApplicationValidateDwProgramType() throws Exception {
		super();
	}

	/**
	 * Process Dwelling Program Type Change Validation.
	 * 
	 */
	public ModelBean process() throws IBIZException, ServiceHandlerException {

		try {
			// Log a Greeting
			Log.debug("Processing " + this.getClass().getSimpleName());

			JDBCData data = this.getHandlerData().getConnection();
			ModelBean rs = this.getHandlerData().getResponse();
			
			ModelBean xfdf = rs.getBean("ResponseParams").getBean("XFDF");
			ModelBean currentFieldId = xfdf.findBeanByFieldValue("Param", "Name", "CurrentFieldId");
			
			if (currentFieldId == null || !currentFieldId.gets("Value").equals("BasicPolicy.DwProgramTypeCd")) {
				return rs;
			}
			
			ModelBean application = rs.getBean("Application");
        	ModelBean basicPolicy = application.getBean("BasicPolicy");
			ModelBean risk = application.getBean("Line").getBean("Risk");
			
			// Error is not generated if the application has no rate.
			String finalPremiumAmt = basicPolicy.gets("FinalPremiumAmt");
			if ("".equals(finalPremiumAmt) || StringRenderer.equal(finalPremiumAmt, "0")) {
				data.saveModelBean(application);
				return rs;
			}

			// Generate error only if it does not exist in the application.
			ModelBean error = application.findBeanByFieldValue("ValidationError", "Msg", ERR_MESSAGE);
			if (error == null) {
				// Create Error and Dwelling Tab Link
				String dwellingTabParam = "CodeRefOptionsKey=application-product&TabOption=Risks&ShowRequiredFieldsInd=Yes";
				String action = String.format("javascript:cmmXFormSafe('UWApplicationSync','%s','%s','%s','%s')", application.getSystemId(), risk.getId(), risk.getId(), dwellingTabParam);
				String dwellingLink = ValidationRenderer.createActionLink(action);
				ValidationRenderer.addValidationErrorWithLink(application, "Validation", "ERROR", ERR_MESSAGE, dwellingLink);
			}

            basicPolicy.setValue("FullTermAmt", "0.00");
            basicPolicy.setValue("WrittenPremiumAmt", "0.00");
            basicPolicy.setValue("FinalPremiumAmt", "0.00");
            
        	RatingBase.clearRating(application);
        	data.saveModelBean(application);
			
			// Return the Response ModelBean
			return rs;

		} catch (Exception e) {
			throw new ServiceHandlerException(e);
		}
	}
}