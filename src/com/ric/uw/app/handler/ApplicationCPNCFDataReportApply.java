/*
 * ApplicationCPNCFDataReportApply.java
 *
 */

package com.ric.uw.app.handler;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

import com.iscs.common.business.datareport.DataReport;
import com.iscs.common.business.logging.Logger;
import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;

/** Applies the CP NCF Data Report Response Data to the Application
 *
 * @author rajur
 */
public class ApplicationCPNCFDataReportApply implements RuleProcessor  {
	
    /** Creates a new instance of ApplicationCPNCFDataReportApply
     * @throws Exception never
     */
    public ApplicationCPNCFDataReportApply() throws Exception {
    }
    
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user, JDBCData data) throws RuleException {
		try {
			
            // Initialize ModelBean Variables
			ModelBean application = bean;
            ModelBean dataReport = BeanTools.searchArray("DataReport", additionalBeans);
            ModelBean dataReportRequest = BeanTools.searchArray("DataReportRequest", additionalBeans); 
            ModelBean cpNCFReport = dataReport.getBean("CPNCFReport");
            ModelBean spouseCPNCFReport = cpNCFReport.getBean("CPNCFReport");
            boolean spouseScore = false;
            
            if( spouseCPNCFReport != null ) {
            	spouseScore = true;
            }	
            
            ModelBean cPInquiryInfo=cpNCFReport.getBean("CPInquiryInfo");
            String referenceNumber="";
            if(cPInquiryInfo!=null && cPInquiryInfo.gets("ChoicePointReferenceNumber")!=null){
            	referenceNumber= cPInquiryInfo.gets("ChoicePointReferenceNumber");
            }
            
            // Skip Processing if Application is Deleted or DataReportRequest is Cancelled
            if( application.valueEquals("Status", "Deleted") || dataReportRequest.valueEquals("StatusCd", "Cancelled") )
            	return application;
            
            // Save Source
            ModelBean insuranceScore = application.getBeanByAlias("InsuredInsuranceScore");
            insuranceScore.setValue("SourceCd", "NCF");
            insuranceScore.setValue("SourceIdRef", dataReportRequest.getId());
            
            
            ModelBean insuranceJointScore = application.getBeanByAlias("InsuredJointInsuranceScore");

            
            // Save Status
            String statusCd = "";
            String processingStatusCd = cpNCFReport.gets("ProcessingStatusCd");
            if( processingStatusCd.equals("M") ) 
            	statusCd = "No Hit";
            else if( StringTools.in(processingStatusCd, "C,H,P") ) 
            	statusCd = "Hit";
            else if( StringTools.in(processingStatusCd, "N,U") ) 
            	statusCd = "No Score";
            else
            	statusCd = "Error";
           	
            if( spouseCPNCFReport != null ) {
            	if( statusCd.equals("No Hit") || statusCd.equals("No Score") ) {
            		if( StringTools.in(spouseCPNCFReport.gets("ProcessingStatusCd"), "C,H,P") ) {
            			statusCd = "Hit";
            		}	
                }
            }
            insuranceScore.setValue("StatusCd", statusCd);
            if(spouseScore){
	            insuranceJointScore.setValue("SourceCd", "NCF");
	            insuranceJointScore.setValue("SourceIdRef", dataReportRequest.getId());
	            insuranceJointScore.setValue("StatusCd", statusCd);
            }
            
            // If Hit or No Score - Clear Insurance Score Information 
            if( statusCd.equals("Hit") || statusCd.equals("No Score") ) {
            	insuranceScore.setValue("InsuranceScore", "");
            	insuranceScore.setValue("OverriddenInsuranceScore", "");
            	insuranceScore.deleteBeans("InsuranceScoreReason");
            }
            
            if( (statusCd.equals("Hit") || statusCd.equals("No Score")) && spouseScore) {
            	insuranceJointScore.setValue("InsuranceScore", "");
            	insuranceJointScore.setValue("OverriddenInsuranceScore", "");
            	insuranceJointScore.deleteBeans("InsuranceScoreReason");
            }
            
            // Save Insurance Score 
            saveInsuranceScore(insuranceScore, cpNCFReport);
            if( spouseCPNCFReport != null ) { 
            	saveInsuranceScore(insuranceJointScore, spouseCPNCFReport);
            }
           
            // Log Message 
            StringDate todayDt = DateTools.getStringDate();
            String logKey = "Application::" + application.getSystemId();
            ModelBean[] beans = DataReport.getAssociatedBeans(data, dataReportRequest, logKey);
            if( application.valueEquals("TypeCd", "Quote") )
            	Logger.message(data, "UWQuoteAppliedDataReport", "System", todayDt, beans);
            else if( application.valueEquals("TypeCd", "Application") )
            	Logger.message(data, "UWApplicationAppliedDataReport", "System", todayDt, beans);
            
            // Set the DataReportRequest ModelBean Date, and Time Applied
            dataReportRequest.setValue("AppliedDt", DateTools.getStringDate());
            dataReportRequest.setValue("AppliedTm", DateTools.getTime("HH:mm:ss z"));           
            
            // Set the DataReportRequest ModelBean Status to Applied
            dataReportRequest.setValue("StatusCd", "Applied");
            
            dataReportRequest.setValue("ReferenceNumber", referenceNumber);
            
            // Update Application ModelBean 
            BeanTools.saveBean(application, data);

            return application;
		}
		catch (Exception e) {
			Log.error(e.getMessage(), e);
            throw new RuleException(e);
		}
	}
        
	/** Save Insurance Score 
	 * @param insuranceScore InsuranceScore ModelBean
	 * @param cpNCFReport CPNCFReport ModelBean 
	 * @throws Exception if an Unexpected Error Occurs
	 */
	private void saveInsuranceScore(ModelBean insuranceScore, ModelBean cpNCFReport)
    throws Exception {
		
		// Save Insurance Score
        ModelBean cpNCFScore = cpNCFReport.getBean("CPNCFScore");
        ModelBean cPInquiryInfo=cpNCFReport.getBean("CPInquiryInfo");
        String referenceNumber="";
        if(cPInquiryInfo!=null && cPInquiryInfo.gets("ChoicePointReferenceNumber")!=null){
        	referenceNumber= cPInquiryInfo.gets("ChoicePointReferenceNumber");
        	insuranceScore.setValue("ReferenceNumber", referenceNumber);
        }
        if( cpNCFScore != null ) {
        	String score = cpNCFScore.gets("Value");
        	if( StringTools.greaterThan(score, insuranceScore.gets("InsuranceScore"))) {
	        	insuranceScore.setValue("InsuranceScore", score);
	        	insuranceScore.setValue("OverriddenInsuranceScore", "");
	        	
	        	// Delete Existing Insurance Score Reasons 
	        	insuranceScore.deleteBeans("InsuranceScoreReason");
	        	
	        	// Save Insurance Score Reasons
	        	ModelBean[] cpNCFScoreReasons = cpNCFScore.getBeans("CPNCFScoreReason");
	        	for( ModelBean cpNCFScoreReason : cpNCFScoreReasons ) {
	        		ModelBean insuranceScoreReason = new ModelBean("InsuranceScoreReason");
	        		insuranceScoreReason.setValue("ReasonCd", cpNCFScoreReason.gets("ReasonCd"));
	        		insuranceScoreReason.setValue("Description", cpNCFScoreReason.gets("Description"));
	        		insuranceScore.addValue(insuranceScoreReason);
	        	}
        	}
        }
        
	}
	
	
}