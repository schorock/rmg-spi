/*
 * ApplicationAddBaseline.java
 *
 */

package com.ric.uw.app.handler;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.data.JDBCData.QueryResult;
import net.inov.tec.thread.ManagedTask;
import net.inov.tec.thread.MasterThreadManager;
import net.inov.tec.web.cmm.AdditionalParams;

import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.uw.common.shared.Insured;

/** Save a copy of the submitted application in ApplicationBaseline. This will later be compared to the modified application to generate a list of changes.
 *
 * @author  Benjamin Olsen
 */
public class ApplicationAddBaseline extends InnovationIBIZHandler {

    /** Creates a new instance of ApplicationAddBaseline
     * @throws Exception never
     */
    public ApplicationAddBaseline() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws ServiceHandlerException {
        try {
            // Log a Greeting
            Log.debug("Processing ApplicationAddBaseline...");
            ModelBean rs = this.getHandlerData().getResponse();       
            JDBCData data = this.getHandlerData().getConnection();        
            AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest() );
                          
            String managedTaskId = ap.gets("ManagedTaskId","");
            if( !managedTaskId.equals("")){
            	ManagedTask managedTask = MasterThreadManager.getInstance().getTask(managedTaskId);
            	managedTask.setProgress(ap.gets("ManagedTaskPrefix") + " " + "Saving a copy of the application");
            }

            ModelBean application = rs.getBean("Application"); 
            
            
            						
            QueryResult[] result = data.doQueryFromLookup("ApplicationBaseline", new String[] {"SubmissionApplicationRef"}, new String[] {"="}, new String[] {application.getSystemId()}, 1);
            if( result.length == 0 ) {
	            // Save a copy of the application
	            ModelBean applicationClone = application.cloneBean();
	            // Remove dashes from baseline because is causes invalid approver changes  RMGPROD-373
	            // Get Insured ModelBean
	            ModelBean insured = applicationClone.getBean("Insured");
	            // Remove Dashes from Tax ID fields
	            Insured.indexTaxIdFieldValues(insured);
	            ModelBean applicationBaseline = new ModelBean("ApplicationBaseline");
	            applicationBaseline.setValue("SubmissionApplicationRef",application.getSystemId());
	            applicationBaseline.addValue(applicationClone);
	            BeanTools.saveBean(applicationBaseline, data);
            }
            // Return the Response ModelBean
            return rs;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }   
}