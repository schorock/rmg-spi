package com.ric.uw.app;

import com.iscs.common.erasure.ContainerErasure;
import com.iscs.common.shared.LookupAddressKeys;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.uw.app.Application;
import com.iscs.uw.quickquote.QuickQuote;
import com.iscs.uw.quote.Quote;
import net.inov.mda.MDATable;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.LookupKey;
import net.inov.tec.date.StringDate;

import java.util.ArrayList;

/**
 * @author rhoush
 */
public class GKApplication extends com.iscs.uw.app.GKApplication {
  public LookupKey[] generateKeys(ModelBean bean) throws Exception {
    Log.debug("Generate Keys...");

    // Initialize Variables
    ModelBean insured = bean.getBean("Insured");
    ModelBean insuredName = BeanTools.getBeanWithAlias(insured,"InsuredName");

    // Create an array of lookup keys to return
    ArrayList<LookupKey> keyArray = new ArrayList<LookupKey>();

    // Generate 'ApplicationNumberLookup' key
    String typeCd = bean.gets("TypeCd");
    if( typeCd.equals("QuickQuote") ) {
      String lookup = QuickQuote.generateQuickQuoteLookup(null, bean);
      LookupKey kv = new LookupKey("ApplicationNumberLookup", ModelSpecification.indexString(lookup));
      keyArray.add(kv);
    } else if( typeCd.equals("Quote") || typeCd.equalsIgnoreCase("ExternalQuote") ) {
      String lookup = Quote.generateQuoteLookup(null, bean);
      LookupKey kv = new LookupKey("ApplicationNumberLookup", ModelSpecification.indexString(lookup));
      keyArray.add(kv);

      // EffectiveDt key to be used by Quote Expiration job and to be used by reminder tasks for user (RGUAT-517)
      if (typeCd.equals("Quote")) {
        ModelBean basicPolicy = bean.getBean("BasicPolicy");
        StringDate effectiveDt = basicPolicy.getDate("EffectiveDt");
        if (effectiveDt != null) {
          LookupKey kv2 = new LookupKey("EffectiveDt", effectiveDt.toString());
          keyArray.add(kv2);
        }
      }
    } else if( typeCd.equals("Application") ) {
      String lookup = Application.generateApplicationLookup(null, bean);
      LookupKey kv = new LookupKey("ApplicationNumberLookup", ModelSpecification.indexString(lookup));
      keyArray.add(kv);

      // EffectiveDt key to be used by reminder tasks for user (RGUAT-517)
      ModelBean basicPolicy = bean.getBean("BasicPolicy");
      StringDate effectiveDt = basicPolicy.getDate("EffectiveDt");
      if (effectiveDt != null) {
        LookupKey kv2 = new LookupKey("EffectiveDt", effectiveDt.toString());
        keyArray.add(kv2);
      }
    }
    else{
      throw new Exception("Application type code not set");
    }

    MDATable table = ContainerErasure.getPartyInfoErasureTable();

    if( insured.valueEquals("EntityTypeCd","Individual") ) {

      // Generate the 'TaxId' key from Insured SSN
      ModelBean insuredTaxInfo = BeanTools.getBeanWithAlias(insured,"InsuredTaxInfo");
      if( insuredTaxInfo != null ) {
        String ssn = insuredTaxInfo.gets("SSN");
        if( !ssn.equals("") && !ContainerErasure.isErased(table, "TaxInfo.SSN", ssn)) {
          LookupKey kv = new LookupKey("TaxId", ModelSpecification.indexString(ssn));
          keyArray.add(kv);
        }
      }
    } else if( insured.valueEquals("EntityTypeCd","Joint") ) {

      // Generate the 'TaxId' key from Insured SSN
      ModelBean insuredTaxInfo = BeanTools.getBeanWithAlias(insured,"InsuredTaxInfo");
      if( insuredTaxInfo != null ) {
        String ssn = insuredTaxInfo.gets("SSN");
        if( !ssn.equals("") && !ContainerErasure.isErased(table, "TaxInfo.SSN", ssn) ) {
          LookupKey kv = new LookupKey("TaxId", ModelSpecification.indexString(ssn));
          keyArray.add(kv);
        }
      }

      // Generate the 'TaxId' key from Joint SSN
      ModelBean insuredTaxInfoJoint = BeanTools.getBeanWithAlias(insured,"InsuredTaxInfoJoint");
      if( insuredTaxInfoJoint != null ) {
        String ssnJoint = insuredTaxInfoJoint.gets("SSN");
        if( !ssnJoint.equals("") && !ContainerErasure.isErased(table, "TaxInfo.SSN", ssnJoint)) {
          LookupKey kv = new LookupKey("TaxId", ModelSpecification.indexString(ssnJoint));
          keyArray.add(kv);
        }
      }

      // Generate the 'SpouseIndexName' key
      ModelBean insuredNameJoint = BeanTools.getBeanWithAlias(insured,"InsuredNameJoint");
      if( insuredNameJoint != null ) {
        String spouseIndexName = StringTools.buildName(insuredNameJoint.gets("Surname"), insuredNameJoint.gets("GivenName") + " " + insuredNameJoint.gets("OtherGivenName"), 1, 100);
        if( !spouseIndexName.equals("") && !ContainerErasure.isErased(table, "NameInfo.DBAIndexName", spouseIndexName) ) {
          LookupKey kv = new LookupKey("SpouseIndexName", ModelSpecification.indexString(spouseIndexName));
          keyArray.add(kv);
        }
      }
    } else if( insured.valueEquals("EntityTypeCd","Trust") ) {

      // Generate the 'TaxId' key from Insured SSN
      ModelBean insuredTaxInfo = BeanTools.getBeanWithAlias(insured,"InsuredTaxInfo");
      if( insuredTaxInfo != null ) {
        String ssn = insuredTaxInfo.gets("SSN");
        if( !ssn.equals("") && !ContainerErasure.isErased(table, "TaxInfo.SSN", ssn) ) {
          LookupKey kv = new LookupKey("TaxId", ModelSpecification.indexString(ssn));
          keyArray.add(kv);
        }
      }

      // Generate the 'TaxId' key from Trust FEIN
      ModelBean insuredTaxInfoTrust = BeanTools.getBeanWithAlias(insured,"InsuredTaxInfoTrust");
      if( insuredTaxInfoTrust != null ) {
        String taxIdTrust = insuredTaxInfoTrust.gets("FEIN");
        if( !taxIdTrust.equals("") && !ContainerErasure.isErased(table, "TaxInfo.FEIN", taxIdTrust) ){
          LookupKey kv = new LookupKey("TaxId", ModelSpecification.indexString(taxIdTrust));
          keyArray.add(kv);
        }
      }
    } else if( insured.valueEquals("EntityTypeCd","Estate") ) {

      // Generate the 'TaxId' key from Insured SSN
      ModelBean insuredTaxInfo = BeanTools.getBeanWithAlias(insured,"InsuredTaxInfo");
      if( insuredTaxInfo != null ) {
        String ssn = insuredTaxInfo.gets("SSN");
        if( !ssn.equals("") && !ContainerErasure.isErased(table, "TaxInfo.SSN", ssn) ){
          LookupKey kv = new LookupKey("TaxId", ModelSpecification.indexString(ssn));
          keyArray.add(kv);
        }
      }

      // Generate the 'TaxId' key from Estate FEIN
      ModelBean insuredTaxInfoEstate = BeanTools.getBeanWithAlias(insured,"InsuredTaxInfoEstate");
      if( insuredTaxInfoEstate != null ) {
        String taxIdEstate = insuredTaxInfoEstate.gets("FEIN");
        if( !taxIdEstate.equals("") && !ContainerErasure.isErased(table, "TaxInfo.FEIN", taxIdEstate) ) {
          LookupKey kv = new LookupKey("TaxId", ModelSpecification.indexString(taxIdEstate));
          keyArray.add(kv);
        }
      }
    } else if( insured.valueEquals("EntityTypeCd","Business") ) {

      // Generate the 'TaxId' key from Insured FEIN
      ModelBean insuredTaxInfo = BeanTools.getBeanWithAlias(insured,"InsuredTaxInfo");
      if( insuredTaxInfo != null ) {
        String taxId = insuredTaxInfo.gets("FEIN");
        if( !taxId.equals("") && !ContainerErasure.isErased(table, "TaxInfo.FEIN", taxId) ){
          LookupKey kv = new LookupKey("TaxId", ModelSpecification.indexString(taxId));
          keyArray.add(kv);
        }
      }

      // Generate the 'DBAIndexName' key
      if( insuredName != null ) {
        String dbaIndexName = insuredName.gets("DBAIndexName");
        if( !dbaIndexName.equals("") && !ContainerErasure.isErased(table, "NameInfo.DBAIndexName", dbaIndexName) ) {
          LookupKey kv = new LookupKey("DBAIndexName", ModelSpecification.indexString(dbaIndexName));
          keyArray.add(kv);
        }
      }
    }

    // Generate the 'ExtendedName' key
    if( insuredName != null ) {
      if( BeanTools.fieldExists(insuredName, "ExtendedName") ) {
        String extendedName = insuredName.gets("ExtendedName");
        if( !extendedName.equals("") && !ContainerErasure.isErased(table, "NameInfo.ExtendedName", extendedName) ) {
          LookupKey kv = new LookupKey("ExtendedName", ModelSpecification.indexString(extendedName));
          keyArray.add(kv);
        }
      }
    }

    // Generate 'ContactNumber' key from Insured Primary Phone
    ModelBean insuredPhonePrimary = BeanTools.getBeanWithAlias(insured,"InsuredPhonePrimary");
    if( insuredPhonePrimary != null ) {
      String primaryNumber = insuredPhonePrimary.gets("PhoneNumber");
      if( !primaryNumber.equals("") && !ContainerErasure.isErased(table, "PhoneInfo.PhoneNumber", primaryNumber) ) {
        LookupKey kv = new LookupKey("ContactNumber", ModelSpecification.indexString(primaryNumber));
        keyArray.add(kv);
      }
    }

    // Generate 'ContactNumber' key from Insured Secondary Phone
    ModelBean insuredPhoneSecondary = BeanTools.getBeanWithAlias(insured,"InsuredPhoneSecondary");
    if( insuredPhoneSecondary != null ) {
      String secondaryNumber = insuredPhoneSecondary.gets("PhoneNumber");
      if( !secondaryNumber.equals("") && !ContainerErasure.isErased(table, "PhoneInfo.PhoneNumber", secondaryNumber) ) {
        LookupKey kv = new LookupKey("ContactNumber", ModelSpecification.indexString(secondaryNumber));
        keyArray.add(kv);
      }
    }

    // Generate 'ContactNumber' key from  Insured Fax
    ModelBean insuredFax = BeanTools.getBeanWithAlias(insured,"InsuredFax");
    if( insuredFax != null ) {
      String faxNumber = insuredFax.gets("PhoneNumber");
      if( !faxNumber.equals("") && !ContainerErasure.isErased(table, "PhoneInfo.PhoneNumber", faxNumber) ) {
        LookupKey kv = new LookupKey("ContactNumber", ModelSpecification.indexString(faxNumber));
        keyArray.add(kv);
      }
    }

    // Generate 'EmailAddr' key from Insured Email
    ModelBean insuredEmail = BeanTools.getBeanWithAlias(insured,"InsuredEmail");
    if( insuredEmail != null ) {
      String emailAddr = insuredEmail.gets("EmailAddr");
      if( !emailAddr.equals("") && !ContainerErasure.isErased(table, "EmailInfo.EmailAddr", emailAddr) ) {
        LookupKey kv = new LookupKey("EmailAddr", emailAddr);
        keyArray.add(kv);
      }
    }

    // Generate Components of Insured Mailing Address as keys
    ModelBean insuredMailingAddr = BeanTools.getBeanWithAlias(insured,"InsuredMailingAddr");
    if( insuredMailingAddr != null ) {
      String mailCity = insuredMailingAddr.gets("City");
      if( !ContainerErasure.isErased(table, "Addr.City", mailCity) ){
        LookupKey kv = new LookupKey("City", ModelSpecification.indexString(mailCity));
        keyArray.add(kv);
      }
      String mailState = insuredMailingAddr.gets("StateProvCd");
      if( !ContainerErasure.isErased(table, "Addr.StateProvCd", mailState) ){
        LookupKey kv = new LookupKey("StateProvCd", ModelSpecification.indexString(mailState));
        keyArray.add(kv);
      }
      String mailZip = insuredMailingAddr.gets("PostalCode");
      if( !ContainerErasure.isErased(table, "Addr.PostalCode", mailZip) ){
        LookupKey kv = new LookupKey("PostalCode", ModelSpecification.indexString(mailZip));
        keyArray.add(kv);
      }
    }

    // Generate Components of Insured Billing Address as keys
    ModelBean insuredBillingAddr = BeanTools.getBeanWithAlias(insured,"InsuredBillingAddr");
    if( insuredBillingAddr != null ) {
      String billCity = insuredBillingAddr.gets("City");
      if( !ContainerErasure.isErased(table, "Addr.City", billCity) ){
        LookupKey kv = new LookupKey("City", ModelSpecification.indexString(billCity));
        keyArray.add(kv);
      }
      String billState = insuredBillingAddr.gets("StateProvCd");
      if( !ContainerErasure.isErased(table, "Addr.StateProvCd", billState) ){
        LookupKey kv = new LookupKey("StateProvCd", ModelSpecification.indexString(billState));
        keyArray.add(kv);
      }
      String billZip = insuredBillingAddr.gets("PostalCode");
      if( !ContainerErasure.isErased(table, "Addr.PostalCode", billZip) ){
        LookupKey kv = new LookupKey("PostalCode", ModelSpecification.indexString(billZip));
        keyArray.add(kv);
      }
    }

    // Generate Components of Insured Lookup Address as keys
    ModelBean insuredLookupAddr = BeanTools.getBeanWithAlias(insured,"InsuredLookupAddr");
    if( insuredLookupAddr != null ) {
      String lookupCity = insuredLookupAddr.gets("City");
      if( !ContainerErasure.isErased(table, "Addr.City", lookupCity) ){
        LookupKey kv = new LookupKey("City", ModelSpecification.indexString(lookupCity));
        keyArray.add(kv);
      }
      String lookupState = insuredLookupAddr.gets("StateProvCd");
      if( !ContainerErasure.isErased(table, "Addr.StateProvCd", lookupState) ){
        LookupKey kv = new LookupKey("StateProvCd", ModelSpecification.indexString(lookupState));
        keyArray.add(kv);
      }
      String lookupZip = insuredLookupAddr.gets("PostalCode");
      if( !ContainerErasure.isErased(table, "Addr.PostalCode", lookupZip) ){
        LookupKey kv = new LookupKey("PostalCode", ModelSpecification.indexString(lookupZip));
        keyArray.add(kv);
      }
      if( !ContainerErasure.isBeanErased(insuredLookupAddr.getParentBean()) ){
        LookupAddressKeys.generateLookupAddressKeys(insuredLookupAddr, "LookupAddress", "lookupaddress-lookup-key-components", keyArray);
      }

    }

    // Generate 'RewriteFromPolicyRef' key
    String rewriteFromPolicyRef = bean.getBean("BasicPolicy").gets("RewriteFromPolicyRef");
    if( !rewriteFromPolicyRef.equals("") ) {
      LookupKey kv = new LookupKey("RewriteFromPolicyRef", rewriteFromPolicyRef);
      keyArray.add(kv);
    }

    // Generate 'AIName' keys
    ModelBean[] ais = bean.findBeansByFieldValue("AI","Status","Active");
    for( ModelBean ai : ais ) {
      if( !ai.gets("InterestName").equals("") ) {
        LookupKey kv = new LookupKey("AIName", ModelSpecification.indexString(ai.gets("InterestName")));
        keyArray.add(kv);
      }
      ModelBean nameInfo = BeanTools.getBeanWithAlias(ai, "AIName");
      if( nameInfo != null && !nameInfo.gets("IndexName").equals("") ) {
        LookupKey kv = new LookupKey("AIName", ModelSpecification.indexString(nameInfo.gets("IndexName")));
        keyArray.add(kv);
      }
    }

    // Generate 'PropertyAddr' keys from Risk Addresses
    ModelBean[] risks = bean.getAllBeans("Risk");
    for( ModelBean risk : risks ) {
      ModelBean riskLookupAddr = risk.getBeanByAlias("RiskLookupAddr");
      if( riskLookupAddr != null ) {

        LookupAddressKeys.generateLookupAddressKeys(riskLookupAddr, "PropertyAddr", "propertyaddress-lookup-key-components", keyArray);

        // Generate 'LookupAddress' keys from Risk Address
        if( risk.gets("Status").equals("Active") ) {
          LookupAddressKeys.generateLookupAddressKeys(riskLookupAddr, "LookupAddress", "lookupaddress-lookup-key-components", keyArray);
        }
      }
    }

    // Generate 'PropertyAddr keys from Location Addresses
    ModelBean[] locations = bean.getAllBeans("Location");
    for( ModelBean location : locations ) {
      ModelBean locationLookupAddr = location.getBeanByAlias("LocationLookupAddr");
      if( locationLookupAddr != null ) {

        LookupAddressKeys.generateLookupAddressKeys(locationLookupAddr, "PropertyAddr", "propertyaddress-lookup-key-components", keyArray);

        // Generate 'LookupAddress' keys from Location Address
        if( location.gets("Status").equals("Active") ) {
          LookupAddressKeys.generateLookupAddressKeys(locationLookupAddr, "LookupAddress", "lookupaddress-lookup-key-components", keyArray);
        }
      }
    }

    // Generate 'StickyInd' keys from Notes
    if( BeanTools.findBeansByFieldValueAndStatus(bean, "Note", "StickyInd", "Yes", "Active").length > 0 ){
      LookupKey kv = new LookupKey("StickyInd", ModelSpecification.indexString("Yes"));
      keyArray.add(kv);
    }

    // Return key/value pairs to the called
    LookupKey[] keys = keyArray.toArray( new LookupKey[keyArray.size()] );
    return keys;
  }

  /** Gets the Application Model Name
   * @return Application String
   */
  public String getModelName() {
    return "Application";
  }

}
