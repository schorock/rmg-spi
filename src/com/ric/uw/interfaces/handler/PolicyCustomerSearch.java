/*
 * PolicyCustomerSearch.java
 */
package com.ric.uw.interfaces.handler;

import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;
import net.inov.tec.data.JDBCLookup;
import net.inov.tec.web.cmm.AdditionalParams;

/**
 * Search for policy based on the search criteria. Returns DTOPolicy beans
 * 
 * @author 292642
 *
 */
public class PolicyCustomerSearch extends com.iscs.uw.interfaces.handler.PolicyCustomerSearch {

	public PolicyCustomerSearch() throws Exception {
		super();
	}

	/**
	 * Processes a generic service request for policy search. Filters the
	 * results based on the producer's agency.
	 * 
	 * @return the current response bean
	 * @throws IBIZException
	 *             when an error requiring user attention occurs
	 * @throws ServiceHandlerException
	 *             when a critical error occurs
	 */
	public ModelBean process() throws IBIZException, ServiceHandlerException {
		
		try {

			ModelBean rs = super.process();

			AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest());
			String agencyId = ap.gets("agencyID");
			ModelBean[] dtoPolicies = rs.getBeans("DTOPolicy");

			ModelBean[] dtoCustomers = rs.getBeans("DTOCustomer");

			// Apply filtering based on the agency ID
			if (dtoPolicies != null && dtoPolicies.length > 0) {
				for (ModelBean dtoPolicy : dtoPolicies) {
					ModelBean dtoBasicPolicy = dtoPolicy.getBean("DTOBasicPolicy");
					if (dtoBasicPolicy != null) {
						String providerNumber = dtoBasicPolicy.gets("ProviderNumber");
						ModelBean producer = getProducer(providerNumber);
						if(producer != null&& agencyId != null){
							ModelBean producerInfo=producer.getBean("ProducerInfo");
							if(!(agencyId.trim().equalsIgnoreCase(producerInfo.gets("ProducerAgency"))||StringTools.in(agencyId.trim(),"2VA000,2PA021"))){
								rs.deleteBeanById("DTOPolicy", dtoPolicy.getId());
							}
						}
					}
				}
			}
			
			// Remove all customer DTO

			if (dtoCustomers != null && dtoCustomers.length > 0) {
				for (ModelBean dtoCustomer : dtoCustomers) {
					rs.deleteBeanById("DTOCustomer", dtoCustomer.getId());
				}

			}

			return rs;
		} catch (IBIZException ibe) {
			throw ibe;
		} catch (Exception e) {
			throw new ServiceHandlerException(e);
		}
	}

	/**
	 * Search provider using provider ID/Number
	 * 
	 * @param producerNumber
	 * @return
	 */
	private ModelBean getProducer(String producerNumber) {

		ModelBean resultBean = null;

		try {
			producerNumber = ModelSpecification.indexString(producerNumber);

			JDBCData data = this.getHandlerData().getConnection();

			JDBCLookup lookup = new JDBCLookup("ProviderLookup");
			lookup.addLookupKey("ProviderType", "PRODUCER", JDBCLookup.LOOKUP_EQUALS);
			lookup.addLookupKey("ProviderNumber", producerNumber, JDBCLookup.LOOKUP_EQUALS);
			JDBCData.QueryResult[] result = lookup.doLookup(data, 0);

			if (result != null && result.length > 0) {
				resultBean = new ModelBean("Provider");
				String systemId = result[0].getSystemId();
				data.selectModelBean(resultBean, Integer.parseInt(systemId));
			}

		} catch (Exception e) {
			Log.error("Error in fetching producer using producer ID : " + producerNumber);

		}

		return resultBean;

	}

}
