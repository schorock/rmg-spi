package com.ric.uw.interfaces.choicepoint.contribution.currentcarrier.handler;

import java.util.HashMap;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;
import net.inov.tec.data.JDBCLookup;
import net.inov.tec.data.JDBCLookup.LookupKey;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

import com.iscs.common.tech.bean.pojomapper.BeanPojoMapper;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.interfaces.choicepoint.contribution.ContributionFileGenerator;
import com.iscs.interfaces.utility.record.Record;
import com.ric.interfaces.choicepoint.contribution.currentcarrier.CurrentCarrierBuilder;
import com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.CurrentCarrierGenerator;
import com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.entity.CurrentCarrierInfo;

/** Create the initial ChoicePoint Current Carrier Contribution.  The initial contribution
 * consists of all Active and in-force policies. 
 * The contribution consists of the current state or transaction the policy is at.  There 
 * is no need to regenerate the policy as of its inception date.
 * 
 * @author allend
 *
 */
public class CurrentCarrierInitialContribution extends InnovationIBIZHandler {
	
	public CurrentCarrierInitialContribution() throws Exception {
	}
	
    /** Process the Initial Contribution
     * 1) Obtain the job parameters, and determine if regen-ability is needed.  
     * 2) If so, delete the previous initial contribution records
     * 3) Process all Active and Inforce policies
     * 4) Convert these into the File format required
     * 5) If electronic transmission available, call the transmission
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process()
    throws ServiceHandlerException {
        JDBCData data = this.getConnection();
        try {            
            Log.debug("Processing CurrentCarrierInitialContribution...");
            
            ModelBean rs = this.getHandlerData().getResponse();
            ModelBean rq = this.getHandlerData().getRequest();
            AdditionalParams ap = new AdditionalParams(rq);
            StringDate todayDt = DateTools.getStringDate(rs.getBean("ResponseParams"));
            String todayTm = DateTools.getTime(rs.getBean("ResponseParams"));
            
        	// Ensure the Errors bean exists in the response
        	ModelBean responseParams = getResponse().getBean("ResponseParams");
        	ModelBean errorsBean = responseParams.getBean("Errors");
        	if(errorsBean == null){
        		errorsBean = new ModelBean("Errors");
        		responseParams.addValue(errorsBean);
        	}
    		            
            // Obtain the parameters needed for the batch job
            boolean regenInd = StringTools.isTrue(ap.gets("RegenerateInd"));
            boolean transmitInd = StringTools.isTrue(ap.gets("TransmitElectronicallyInd"));
            String productId = ap.gets("UWProductId");
            
            if (regenInd) {
            	deletePrevious(data, productId);
            } else {
            	// Validate that there are no other initial or periodic contribution data 
        		if (productId.equals("All")) {
                	JDBCData.QueryResult[] results = data.doQueryFromLookup("CPCarrierContrib", new String[] {"TypeCd"}, new String[] {"IN"}, new String[] {"INITIAL,PERIOD"}, 1);
                	if (results.length > 0) {
                		throw new IBIZException("The Initial Contribution has been processed already");
                	}            		        			
        		} else {
                	CurrentCarrierBuilder builder = new CurrentCarrierBuilder();        	
            		String[] ids = builder.contributionProductVersionsEnabled(productId);
            		StringBuilder list = new StringBuilder();
            		for (String id : ids) {
            			if (list.length() > 0)
            				list.append(",");
            			list.append(ModelSpecification.indexString(id));			
            		}

                	JDBCData.QueryResult[] results = data.doQueryFromLookup("CPCarrierContrib", new String[] {"TypeCd", "ProductVersionIdRef"}, new String[] {"IN", "IN"}, new String[] {"INITIAL,PERIOD", list.toString()}, 1);
                	if (results.length > 0) {
                		throw new IBIZException("The Initial Contribution has been processed already for this product " + productId);
                	}        			
        		}
            }
            
            processPolicy(data, todayDt, todayTm, productId, transmitInd, regenInd);
                     
            data.commit();
            
            return rs;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        } 
        finally {
        	try { data.rollback(); } catch(Exception e) { Log.error(e); }
        }
    }
    
    /** Delete the previous initial contribution for regeneration
     * @param data The data connection
     * @param productId The product id to delete, All will remove all Initial contribution records
     * @throws Exception when an error occurs
     */
    protected void deletePrevious(JDBCData data, String productId) 
    throws Exception {
    	JDBCData.QueryResult[] results = null;    	
    	if (productId.equals("All")) {
        	results = data.doQueryFromLookup("CPCarrierContrib", new String[] {"TypeCd"}, new String[] {"="}, new String[] {"INITIAL"}, 0);    		
    	} else {
        	CurrentCarrierBuilder builder = new CurrentCarrierBuilder();        	
    		String[] ids = builder.contributionProductVersionsEnabled(productId);
    		StringBuilder list = new StringBuilder();
    		for (String id : ids) {
    			if (list.length() > 0)
    				list.append(",");
    			list.append(ModelSpecification.indexString(id));			
    		}
    		JDBCLookup lookup = new JDBCLookup("CPCarrierContribLookup");
    		LookupKey lookupKey = lookup.addLookupKey("TypeCd", "INITIAL", JDBCLookup.LOOKUP_EQUALS);
    		lookupKey = lookup.addDelimitedListLookupKey("ProductVersionIdRef", list.toString(), ",");
    		lookupKey.setLookupType(JDBCLookup.LOOKUP_IN_LIST);
    		results = lookup.doLookup(data, 0);    		
    	}
    	
    	for (JDBCData.QueryResult result : results) {
    		ModelBean currentCarrier = new ModelBean("CPCarrierContrib");
    		data.selectModelBean(currentCarrier, result.getSystemId());
    		data.deleteModelBean(currentCarrier);
    	}
    }
    
    /** Go through the system and find all Active and Inforce policies for
     * initial contribution
     * @param data The data connection
     * @param reportDt The report date
     * @param reportTm The report time
     * @param productId Product ID
     * @param transmitElectronicallyInd Indicator if this is to be transmitted electronically
     * @param regeneratedInd Indicator specifying if this is a regeneration of data
     * @throws Exception when an error occurs
     */
    private void processPolicy(JDBCData data, StringDate reportDt, String reportTm, String productId, boolean transmitElectronicallyInd, boolean regeneratedInd)
    throws Exception {
		ContributionFileGenerator fileGen = new ContributionFileGenerator();
    	try {
    		java.util.HashMap<String, Object> contextObjs = new HashMap();
    		contextObjs.put("ReportPeriodBeginDt", reportDt);
    		contextObjs.put("ReportPeriodEndDt", reportDt);
    		
    		if (!fileGen.setFile("CPContribution", "CurrentCarrierInitial", contextObjs)) {
    			String msg = fileGen.getErrorMsg();
    			fileGen = null;
				throw new Exception(msg);
			}
        	CurrentCarrierBuilder builder = new CurrentCarrierBuilder();        	
    		CurrentCarrierGenerator generator = new CurrentCarrierGenerator();
    		
    		Record sacRec = generator.createSACHeader(reportDt, reportDt, builder.getContentCode());
    		fileGen.writeRecord(sacRec);
    		
    		String[] ids = builder.contributionProductVersionsEnabled(productId);
    		StringBuilder list = new StringBuilder();
    		for (String id : ids) {
    			if (list.length() > 0)
    				list.append(",");
    			list.append(ModelSpecification.indexString(id));			
    		}
    		
    		JDBCLookup lookup = new JDBCLookup("PolicyLookup");
    		LookupKey lookupKey = lookup.addDelimitedListLookupKey("StatusCd", "Expired,Future,Renewed,Cancelled", ",");
    		lookupKey.setLookupType(JDBCLookup.LOOKUP_NOT_IN_LIST);
    		lookupKey = lookup.addDelimitedListLookupKey("ProductVersionIdRef", list.toString(), ",");
    		lookupKey.setLookupType(JDBCLookup.LOOKUP_IN_LIST);
    		lookup.setSortType(JDBCLookup.SORT_ASCENDING);
    		JDBCData.QueryResult[] results = lookup.doLookup(data, 0);
    		
        	int totalRecords = 0;
        	for (JDBCData.QueryResult result : results) {
        		ModelBean policy = new ModelBean("Policy");
        		data.selectModelBean(policy, result.getSystemId());
        		// Since this will be reported right away, validate the report dates
        		if (builder.isValidContribution(policy, reportDt)) {
            		CurrentCarrierInfo[] ccInfos = builder.processInitial(policy, reportDt);        		
            		
                    // Save the contribution record to the database for future reporting
            		for (CurrentCarrierInfo ccInfo : ccInfos) {
                		if (ccInfo != null) {
                			ccInfo.setTypeCd("Initial");
                            ModelBean ccInfoBean = BeanPojoMapper.toModelBean(ccInfo);
                            ccInfoBean.setValue("ReportPeriodBeginDt", reportDt);
                            ccInfoBean.setValue("ReportPeriodEndDt", reportDt);
                            ccInfoBean.setValue("AddDt", reportDt);
                            ccInfoBean.setValue("AddTm", reportTm);
                            ccInfoBean.setValue("ProcessDt", reportDt);
                            ccInfoBean.setValue("ProcessTm", reportTm);                        
                            ccInfoBean.setValue("StatusCd", builder.STATUS_PROCESSED);
                            if (regeneratedInd)
                            	ccInfoBean.setValue("RegeneratedInd", "Yes");
                            
                            BeanTools.saveBean(ccInfoBean, data);
                                   
                    		// Log the Contribution data
                            Log.debug("Contribution Record : " + ccInfoBean.readableDoc());
                            
                            Record[] records = generator.createPolicySet(ccInfo);
                        	for (Record rec : records) {
                                fileGen.writeRecord(rec);
                                totalRecords++;
                        	}
                		}    			            			
            		}
        		}
        	}
        	
        	if (totalRecords > 0) {
            	Record satRec = generator.createSATTrailer(totalRecords);
            	fileGen.writeRecord(satRec);
            	
            	// Process the file electronically if needed
            	if (transmitElectronicallyInd) {
            		
            	} else {
                    // Return info messages for batch reporting
                	super.addErrorMsg("ProcessingInfo", "The initial contribution file with a Report Date of " + reportDt.toString() + " was saved in the file " + fileGen.getFilename() , ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_INFO);
                	super.addErrorMsg("ProcessingInfo", "There were a total of " + totalRecords + " processed for this period." , ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_INFO);
            	}
        	} else {
        		fileGen.cleanFile();
            	super.addErrorMsg("ProcessingInfo", "The initial contribution file with a Report Date of " + reportDt.toString() + " was not created because there were no records to process.", ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);        		
        	}
        	
    	} catch (Exception e) {
    		if (fileGen != null)
    			fileGen.cleanFile();
    		throw e;
    	}
    }
    
}
