package com.ric.uw.interfaces.choicepoint.contribution.currentcarrier.handler;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceContext;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;
import net.inov.tec.data.JDBCLookup;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

import com.iscs.common.tech.bean.pojomapper.BeanPojoMapper;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.interfaces.choicepoint.contribution.ContributionFileGenerator;
import com.iscs.interfaces.utility.record.Record;
import com.ric.interfaces.choicepoint.contribution.currentcarrier.CurrentCarrierBuilder;
import com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.CurrentCarrierGenerator;
import com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.entity.CurrentCarrierInfo;

/** Create the periodic ChoicePoint Current Carrier Contribution.  The periodic contribution
 * consists of all Active and in-force policies that have had changes since the last 
 * periodic report date. 
 * 
 * @author allend
 *
 */
public class CurrentCarrierPeriodicContribution extends InnovationIBIZHandler {
	
	public CurrentCarrierPeriodicContribution() throws Exception {
	}
	
    /** Process the Periodic Contribution.  There are many permutations available here.
     * There are 6 parameters that will allow normal periodic contributions, regeneration of
     * previous data, and finally a re-run of a previous contribution file without altering
     * any data.
     * 
     * 1) The simplest option is to run without any parameters.  This will look to see if there
     * are any outstanding Contributions to be reported.  If so, those records will be retrieved
     * and processed into a contribution file for ChoicePoint processing.  The records are updated
     * to show which report period it belongs to. The Begin and End Dates are updated to show this.  
     * The Begin Date is the previous Report Period End Date + 1, and the End Date is the current
     * date.  This should be the default option most of the time, and will be option for an automatic
     * cron job or other OS scheduled job run for contributions.
     * 
     * 2) The second option is to re-run a previous contribution file.  If the Begin Date and End 
     * Date are filled out, then all Contributions that were processed for that period will be
     * picked up and re-processed into a Contribution file.  This option will be needed if the 
     * contribution file needs to be re-transmitted to ChoicePoint again because of 
     * communication problems.
     * 
     * 3) The third option is a regeneration of the Contribution data from current Policy data.
     * Use this option if one or more Policies were rejected, or if there were data fixes required.
     * To initiate this option, select the indicator to Regenerate Contributions, and possibly the
     * where clause options of Keys, Operators and Values.  If the where clause options are left blank,
     * then the previous Report Period is selected and all of the Policies within that period are 
     * regenerated.  Please note that the regenerated contributions are not put into the same contribution
     * file as before.  A new contribution file is created with the new Report Period.  The where clause
     * options should only be used if someone is familiar with SQL.  It allows searching of previous
     * contribution data to reprocess by selecting on the following key fields, PolicyNumber, AddDt, 
     * ReportPeriodBeginDt, and ReportPeriodEndDt.  
     * It is a breakdown of the where clause into the respective components, the field to search on, the 
     * operator to use for the field, and the value(s) of that field.  
     * 
     * Therefore a simple case would be
     * WHERE PolicyNumber = "ABC00100" would be translated into the 3 fields as:
     * KEYS : PolicyNumber
     * OPERATORS : =
     * VALUES : ABC00100
     * 
     * or if multiple fields are needed such as 
     * WHERE ReportPeriodBeginDt >= "20090101" and ReportPeriodEndDt <= "20090201" would be:
     * KEYS : ReportPeriodBeginDt,ReportPeriodEndDt
     * OPERATORS : >=,<=
     * VALUES : 20090101,20090201
     * 
     * With multiple values, you can escape with the double quote " to create a list for that field
     * Most SQL operators are supported, such as IN, NOT IN, BETWEEN, etc as with the case of:
     * WHERE PolicyNumber IN ("ABC00100", "XYZ00200") AND ReportPeriodEndDt = "20090201" would be:
     * KEYS : PolicyNumber,ReportPeriodEndDt
     * OPERATORS: IN,=
     * VALUES : \"ABC00100,XYZ00200\",20090201
     * 
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process()
    throws ServiceHandlerException {
        JDBCData data = this.getConnection();
        try {            
            Log.debug("Processing CurrentCarrierPeriodicContribution...");
            
            ModelBean rs = this.getHandlerData().getResponse();
            ModelBean rq = this.getHandlerData().getRequest();
            AdditionalParams ap = new AdditionalParams(rq);
            
        	// Ensure the Errors bean exists in the response
            rs.getBean("ResponseParams").getBean("Errors");
            
            boolean transmitInd = StringTools.isTrue(ap.gets("TransmitElectronicallyInd"));        	

            ContributionJobHelper contribHelper = new ContributionJobHelper(rq, rs);
        	
        	if (contribHelper.isRegenInd()) {
        		processPolicies(data, contribHelper);
            }            

            if (!contribHelper.isRegenInd() && contribHelper.getBeginDt() != null && contribHelper.getEndDt() != null) {
            	boolean completed = reprocessFile(data, contribHelper.getBeginDt(), contribHelper.getEndDt(), transmitInd, contribHelper.getFileGen());
                if (!completed) {
                	this.addErrorMsg("General", "There were no matching Contributions for this reporting period beginning " + contribHelper.getReportBeginDt().toStringDisplay() + " and ending " + contribHelper.getReportEndDt().toStringDisplay() + ". The file was not created.", ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
                }
            } else {            	
                boolean completed = processContribution(data, contribHelper, transmitInd);            	
                if (!completed) {
                	this.addErrorMsg("General", "There were no matching Contributions for this reporting period beginning " + contribHelper.getReportBeginDt().toStringDisplay() + " and ending " + contribHelper.getReportEndDt().toStringDisplay() + ". The file was not created.", ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
                }
            }
            
            if (contribHelper.isTestInd()) {
            	data.rollback();
            } else {
            	data.commit();
            }            
            
            return rs;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
        finally {
        	try { data.rollback(); } catch(Exception e) { Log.error(e); }
        }
    }
    
    /** Go through the system and find all matching Contributions
     * to create the Report file
     * @param data The data connection
     * @param beginDt The report period begin date
     * @param endDt The report period end date
     * @param transmitElectronicallyInd Indicator if this is to be transmitted electronically
     * @return true/false if periodic processing succeeded
     * @throws Exception when an error occurs
     */
    private boolean processContribution(JDBCData data, ContributionJobHelper contribHelper, boolean transmitElectronicallyInd)
    throws Exception {
    	// Do not process the file unless it is a full file regen or a testfile is not indicated
    	if ((contribHelper.isRegenInd() && (contribHelper.getBeginDt() == null || contribHelper.getEndDt() == null)) || contribHelper.isTestInd())
    		return true;
    	
    	ContributionFileGenerator fileGen = contribHelper.getFileGen();
    	try {
        	CurrentCarrierBuilder builder = new CurrentCarrierBuilder();
    		CurrentCarrierGenerator generator = new CurrentCarrierGenerator();
    		StringDate beginDt = contribHelper.getReportBeginDt();
    		StringDate endDt = contribHelper.getReportEndDt();
    		Record sacRec = generator.createSACHeader(beginDt, endDt, builder.getContentCode());
        	Log.debug("Sac Rec = " + sacRec.toString());
    		fileGen.writeRecord(sacRec);
    		
            String statusCd = ModelSpecification.indexString(builder.STATUS_NEW);
        	JDBCData.QueryResult[] results = null;
        	if (contribHelper.isRebuildFile()) {
        		JDBCLookup lookup = new JDBCLookup("CPCarrierContribLookup");
    			lookup.addLookupKey("TypeCd", "PERIOD", JDBCLookup.LOOKUP_EQUALS);    			
    			lookup.addLookupKey("AddDt", contribHelper.getTodayDt().toString(), JDBCLookup.LOOKUP_EQUALS);
    			lookup.addLookupKey("AddTm", contribHelper.getTodayTm(), JDBCLookup.LOOKUP_EQUALS);
    			lookup.addLookupKey("StatusCd", statusCd, JDBCLookup.LOOKUP_EQUALS);
    			lookup.setSortType(JDBCLookup.SORT_ASCENDING);
        		results = lookup.doLookup(data, 0);
        	} else {
        		JDBCLookup lookup = new JDBCLookup("CPCarrierContribLookup");
    			lookup.addLookupKey("TypeCd", "PERIOD", JDBCLookup.LOOKUP_EQUALS);
    			/** Changed this to bookDt */
    			lookup.addLookupKey("BookDt", beginDt.toString(), JDBCLookup.LOOKUP_GREATER_THAN_OR_EQUALS);
    			lookup.addLookupKey("BookDt", endDt.toString(), JDBCLookup.LOOKUP_LESS_THAN_OR_EQUALS);
    			lookup.addLookupKey("StatusCd", statusCd, JDBCLookup.LOOKUP_EQUALS);
    			lookup.setSortType(JDBCLookup.SORT_ASCENDING);
        		results = lookup.doLookup(data, 0);
        	}
    		
        	int totalRecords = 0;
        	for (JDBCData.QueryResult result : results) {        		
        		ModelBean contribution = new ModelBean("CPCarrierContrib");
        		data.selectModelBean(contribution, result.getSystemId());
        		// Since this will be reported right away, validate the report dates
        		if (builder.isValidContribution(contribution, endDt)) {
        			contribution.setValue("TypeCd", "Period");
            		// Update the period dates
            		contribution.setValue("ReportPeriodBeginDt", beginDt);
            		contribution.setValue("ReportPeriodEndDt", endDt);
            		contribution.setValue("ProcessDt", contribHelper.getTodayDt());
            		contribution.setValue("ProcessTm", contribHelper.getTodayTm());
            		contribution.setValue("StatusCd", builder.STATUS_PROCESSED);
            		// Dont save if in testfile mode, save some processing cycles.
            		if (!contribHelper.testInd)
            			data.saveModelBean(contribution);

        			CurrentCarrierInfo ccInfo = new CurrentCarrierInfo();
        			BeanPojoMapper.populatePojo(ccInfo, contribution);
            		
                    Record[] records = generator.createPolicySet(ccInfo);
                	for (Record rec : records) {
                        fileGen.writeRecord(rec);
                        totalRecords++;
                	}
        		}
        	}
        	
        	if (totalRecords > 0) {
            	Record satRec = generator.createSATTrailer(totalRecords);
            	fileGen.writeRecord(satRec);
            	// Process the file electronically if needed
            	if (transmitElectronicallyInd) {
            	
            	} else {
                    // Return info messages for batch reporting
                	super.addErrorMsg("ProcessingInfo", "The periodic contribution file for the Report Period between " + beginDt.toString() + " to " + endDt.toString() + " was saved in the file " + fileGen.getFilename() , ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_INFO);
                	super.addErrorMsg("ProcessingInfo", "There were a total of " + totalRecords + " processed for this period." , ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_INFO);
            	}
            	return true;
        	} else {
        		fileGen.cleanFile();
            	super.addErrorMsg("ProcessingInfo", "The periodic contribution file for the Report Period between " + beginDt.toString() + " to " + endDt.toString() + " was not created because there were no records to process.", ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
        		return false;
        	}
        	
    	} catch (Exception e) {
    		fileGen.cleanFile();
    		throw e;
    	}
    }

    /** Go through the system and find all matching Contributions
     * to re-create the Report file
     * @param data The data connection
     * @param beginDt The report period begin date
     * @param endDt The report period end date
     * @param transmitElectronicallyInd Indicator if this is to be transmitted electronically
     * @param testFilename The test filename 
     * @return true/false if periodic processing succeeded
     * @throws Exception when an error occurs
     */
    private boolean reprocessFile(JDBCData data, StringDate beginDt, StringDate endDt, boolean transmitElectronicallyInd, ContributionFileGenerator fileGen)
    throws Exception {
    	try {
        	CurrentCarrierBuilder builder = new CurrentCarrierBuilder();
    		CurrentCarrierGenerator generator = new CurrentCarrierGenerator();
    		Record sacRec = generator.createSACHeader(beginDt, endDt, builder.getContentCode());
        	Log.debug("Sac Rec = " + sacRec.toString());
    		fileGen.writeRecord(sacRec);
    		
        	JDBCLookup lookup = new JDBCLookup("CPCarrierContribLookup");
			lookup.addLookupKey("TypeCd", "PERIOD", JDBCLookup.LOOKUP_EQUALS);
			lookup.addLookupKey("ReportPeriodBeginDt", beginDt.toString(), JDBCLookup.LOOKUP_EQUALS);
			lookup.addLookupKey("ReportPeriodEndDt", endDt.toString(), JDBCLookup.LOOKUP_EQUALS);
			lookup.setSortType(JDBCLookup.SORT_ASCENDING);
			JDBCData.QueryResult[] results = lookup.doLookup(data, 0);
    		
        	int totalRecords = 0;
        	for (JDBCData.QueryResult result : results) {        		
        		ModelBean contribution = new ModelBean("CPCarrierContrib");
        		data.selectModelBean(contribution, result.getSystemId());

    			CurrentCarrierInfo ccInfo = new CurrentCarrierInfo();
    			BeanPojoMapper.populatePojo(ccInfo, contribution);
        		
                Record[] records = generator.createPolicySet(ccInfo);
            	for (Record rec : records) {
                    fileGen.writeRecord(rec);
                    totalRecords++;
            	}            	        		
        	}
        	
        	if (totalRecords > 0) {
            	Record satRec = generator.createSATTrailer(totalRecords);
            	fileGen.writeRecord(satRec);
            	// Process the file electronically if needed
            	if (transmitElectronicallyInd) {
            	
            	} else {
                    // Return info messages for batch reporting
                	super.addErrorMsg("ProcessingInfo", "The periodic contribution file for the Report Period between " + beginDt.toString() + " to " + endDt.toString() + " was saved in the file " + fileGen.getFilename() , ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_INFO);
            	}
            	return true;
        	} else {
        		fileGen.cleanFile();
        		return false;
        	}
        	
    	} catch (Exception e) {
    		fileGen.cleanFile();
    		throw e;
    	}
    }
    
    /** Obtain the previous Report Period End Date.  This is used to set the current Reporting Period Begin Date
     * @param data The data connection
     * @return The previous report end date
     * @throws Exception when an error occurs
     */
    private StringDate getPreviousEndDt(JDBCData data) throws Exception {
    	String lookupTableName = data.getLookupTableName(new ModelBean("CPCarrierContrib"));
    	String sql = "SELECT MAX(LookupValue) ReportPeriodEndDt FROM " + lookupTableName + " WHERE LookupKey = 'ReportPeriodEndDt'";
    	ResultSet rSet = data.doSQL(sql);
    	String endDate = null;
    	if (rSet.next()) {
    		endDate = rSet.getString("ReportPeriodEndDt");
    		if (endDate == null || endDate.length() == 0)
    			return null;
    	}
    	return new StringDate(endDate);
    }
    

    /** Process the list of policies to regenerate contribution data from
     * @param data The data connection
     * @param policyList The list of Policy numbers to process
     * @param addDt The date the contribution data was added
     * @throws Exception when an error occurs
     */
    private void processPolicies(JDBCData data, ContributionJobHelper contribHelper) throws Exception {
		JDBCData.QueryResult[] results = data.doQueryFromLookup("CPCarrierContrib", contribHelper.getKeyList(), contribHelper.getOpList(), contribHelper.getValueList(), 0);            	
    	
    	for (JDBCData.QueryResult contribution : results) {
        	ModelBean contrib = new ModelBean("CPCarrierContrib");
    		data.selectModelBean(contrib, contribution.getSystemId());
    		ModelBean policy = new ModelBean("Policy");
    		data.selectModelBean(policy, contrib.gets("PolicyRef"));
    		com.iscs.uw.policy.PM.getPolicyAsOfTransaction(policy, contrib.getValueInt("TransactionNumber"));
    		
            CurrentCarrierBuilder builder = new CurrentCarrierBuilder();
            if (builder.contributionRequired(policy)) {
                CurrentCarrierInfo[] ccInfos = builder.processPeriod(policy, null, contrib.gets("TransactionNumber"), contribHelper.getTodayDt());
                
                // Save the contribution record to the database for future reporting
                for (CurrentCarrierInfo ccInfo : ccInfos) {
                    ModelBean ccInfoBean = BeanPojoMapper.toModelBean(ccInfo);
                	ccInfoBean.setValue("StatusCd", builder.STATUS_NEW);
                	ccInfoBean.setValue("RegeneratedInd", "Yes");
                    ccInfoBean.setValue("AddDt", contribHelper.getTodayDt().toString());
                    ccInfoBean.setValue("AddTm", contribHelper.getTodayTm());
                    data.saveModelBean(ccInfoBean);            	                	
                }
            }
    	}    	
    }

    public class ContributionJobHelper {
    	
    	private boolean regenInd = false;
    	private String[] keyList = null;
    	private String[] opList = null;
    	private String[] valueList = null;
    	private StringDate beginDt = null;
    	private StringDate endDt = null;
    	private StringDate reportBeginDt = null;
    	private StringDate reportEndDt = null;
    	private boolean testInd = false;
    	private StringDate todayDt = null;
    	private String todayTm = null;
    	private ContributionFileGenerator fileGen = null;

    	ContributionJobHelper (ModelBean request, ModelBean response) throws Exception {
            AdditionalParams ap = new AdditionalParams(request);
            todayDt = DateTools.getStringDate(response.getBean("ResponseParams"));
            todayTm = DateTools.getTime(response.getBean("ResponseParams"));
            
            regenInd = StringTools.isTrue(ap.gets("RegenerateInd"));
            String keyStr = ap.gets("KeyList");
            String opStr = ap.gets("OperatorList");
            String valueStr = ap.gets("ValueList");
            String beginDate = ap.gets("BeginDt");
            String endDate = ap.gets("EndDt");
            testInd = StringTools.isTrue(ap.gets("TestInd"));
            String testFilename = ap.gets("TestFilename");

            // If test file is to be created, validate the filename/path and make sure it does not exist 
            if (testInd) {
            	if (testFilename.equals("")) 
        			throw new IBIZException("The Test Filename must be filled out if the Test Indicator is set to Yes.");
            }
            
            if (beginDate.length() > 0) {
            	beginDt = new StringDate(beginDate);
            }
            if (endDate.length() > 0) {
            	endDt = new StringDate(endDate);
            }

    		StringDate previousEndDt = getPreviousEndDt(ServiceContext.getServiceContext().getData());
            
            if (regenInd) {
            	// If the question fields are left blank, except the regen indicator, then the previous periodic contribution is regenerated
            	//     The Policies from the this contribution are used to regenerate the next periodic contribution 
            	// If the where fields are filled out, then parse them and obtain the list of Policies to regenerate from
            	
            	if (keyStr.equals("") && opStr.equals("") && valueStr.equals("")) {
            		if (previousEndDt == null) 
            			throw new IBIZException("There are no previous Contributions to regenerate from.");
            		reportBeginDt = StringDate.advanceDate(previousEndDt, 1);
            		
            		if (beginDt != null || endDt != null) {
            			if (beginDt == null || endDt == null) {
                			throw new IBIZException("If Begin Date or End Date is filled out with the Regenerate Indicator, both date fields must be filled out");            				
            			}
            			keyList = new String[] {"TypeCd", "ReportPeriodBeginDt", "ReportPeriodEndDt", "RegeneratedInd"};
            			opList = new String[] {"=", "=", "=", "<>"};
            			valueList = new String[] {"PERIOD", beginDt.toString(), endDt.toString(), "YES"};
            		}
            		
            		keyList = new String[] {"TypeCd", "ReportPeriodEndDt", "RegeneratedInd"};
            		opList = new String[] {"=", "=", "<>"};
            		valueList = new String[] {"PERIOD", StringDate.advanceDate(previousEndDt, 1).toString(), "YES"};
                	
            	} else {
            		List keySet = StringTools.split(keyStr, ",", "\"");
            		List opSet = StringTools.split(opStr, ",", "\"");
            		List valueSet = StringTools.split(valueStr, ",", "\"");
            		
            		if (keySet.size() != opSet.size() || keySet.size() != valueSet.size()) {
            			throw new IBIZException("The Key List, Operator List, and Values List must have the same number of parameters delimited by the character |");
            		}
            		
            		if (beginDt != null || endDt != null) {
            			throw new IBIZException("If the Key List, Operator List, and Value List are filled out, then the Begin Date and End Date cannot be filled out.");
            		}
            		
            		keyList = (String[]) keySet.toArray(new String[keySet.size()]);
            		opList = (String[]) opSet.toArray(new String[opSet.size()]);
            		valueList = (String[]) valueSet.toArray(new String[valueSet.size()]);            		
            	}
            }

            // Determine the Report Period Begin and End Dates
            if (endDt == null) {
            	String runDate = ap.gets("RunDt");
            	 if (runDate.equals("")) {
            		 reportEndDt = todayDt;           	
                 } else {
                	 reportEndDt =  new StringDate(runDate);
                 }            	
            }            	
            else {
            	reportEndDt = endDt;
            }
            
            if (beginDt == null) {            	
        		if (previousEndDt == null) 
        			throw new IBIZException("There are no previous Contributions to base the Begin Date from.  Please enter a Begin Date for the beginning of the first reported contribution.");
        		reportBeginDt = StringDate.advanceDate(previousEndDt, 1);
            } else {
            	reportBeginDt = beginDt;
            }
            
            if (reportBeginDt.compareTo(todayDt) > 0) {
            	throw new IBIZException("The Contribution for the current period has been processed already.");
            }
            
            if (reportBeginDt.compareTo(reportEndDt) > 0) {
            	throw new IBIZException("The Report Period Begin Date of " + reportBeginDt.toStringDisplay() + " is greater than the Report Period End Date of " + reportEndDt.toStringDisplay());
            }
         
            // Validate the file and make sure it does not exist
        	fileGen = new ContributionFileGenerator();
    		java.util.HashMap<String, Object> contextObjs = new HashMap();
    		contextObjs.put("ReportPeriodBeginDt", reportBeginDt);
    		contextObjs.put("ReportPeriodEndDt", reportEndDt);
        	
        	boolean validFile = false;
        	if (testFilename != null && testFilename.length() > 0) {
    			validFile = fileGen.setFile(testFilename, contextObjs);
    		} else {
        		validFile = fileGen.setFile("CPContribution", "CurrentCarrier", contextObjs);
        	}
    		if (!validFile) {
    			throw new IBIZException("The periodic contribution file " + fileGen.getFilename() + " currently exists or is not a valid filename.  There may have been an error processing that file to ChoicePoint.");
    		}
    	}

		public boolean isRegenInd() {
			return regenInd;
		}

		public String[] getKeyList() {
			return keyList;
		}

		public String[] getOpList() {
			return opList;
		}

		public String[] getValueList() {
			return valueList;
		}

		public StringDate getBeginDt() {
			return beginDt;
		}

		public StringDate getEndDt() {
			return endDt;
		}

		public StringDate getReportBeginDt() {
			return reportBeginDt;
		}

		public StringDate getReportEndDt() {
			return reportEndDt;
		}

		public boolean isTestInd() {
			return testInd;
		}

		public StringDate getTodayDt() {
			return todayDt;
		}

		public String getTodayTm() {
			return todayTm;
		}

		public ContributionFileGenerator getFileGen() {
			return fileGen;
		}
    
		public boolean isRebuildFile() {
			if (beginDt != null & endDt != null)
				return true;
			else
				return false;
		}
    }
}