package com.ric.uw.quote;

import com.iscs.common.tech.log.Log;
import com.iscs.insurance.customer.Customer;
import com.ric.uw.app.Application;
import com.iscs.uw.common.risk.Risk;
import com.iscs.uw.policy.Policy;
import com.iscs.uw.quickquote.QuickQuote;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import com.iscs.uw.common.risk.Location;
import com.iscs.conversion.ModelBeanDefault;
import com.iscs.insurance.product.ProductSetup;
public class Quote extends com.iscs.uw.quote.Quote {
	
	
	/** Set Values Needed to Add a New Quote
	 * 
	 * @param data JDBCData connection to the database
	 * @param application ModelBean containing the application
	 * @param createDefaultLine whether or not to create the default line
	 * @param createDefaultLocation whether or not to create the default location
	 * @param mergeDefaultLines flag to indicate if we should merge with the default lines
	 * @throws Exception when an error occurs
	 */
	public static void addQuote(JDBCData data, ModelBean application, boolean createDefaultLine, boolean createDefaultLocation) throws Exception{
		try{
			// Set the Status and Type to Quote
			application.setValue("Status", "In Process");
			application.setValue("Description", "New Quote");
			
			ModelBean basicPolicy = application.getBean("BasicPolicy");
			String stateCd = basicPolicy.gets("ControllingStateCd");

			// Add the Lines Needed for this Product
			if (createDefaultLine){
				Application.setLine(application);	
			} 
			
			// <TODO>  For now create a location if a risk exists without a location reference
			if(createDefaultLocation){
				// Add Default state bean to the quote.
				if (Policy.isWorkersCompensation(application)) {
					ModelBean coveredState = Application.addCoveredState(application, stateCd, true);
					coveredState.setValue("ProductVersionIdRef", basicPolicy.gets("ProductVersionIdRef"));
					Application.addDefaultLocation(application,coveredState.gets("id"));	
				} else {
					Application.addDefaultLocation(application);	
				}
			}else if(application.valueEquals("Location", "")) {
				//add default locationnew.xml to Application so it's never null
				application.addValue(new Location().create());
			}
			//always merge default template to make sure the Location model is acceptable
			ModelBean[] locationBeans = application.getBeans("Location");
			for(int i = 0; i < locationBeans.length; i++) {
				ModelBeanDefault.mergeDefault(locationBeans[i], new Location().create());
			}
			populateRequiredLocationFields(application, locationBeans);
			// Set the Quote Number
			String quoteNumber = "";
			if( application.valueEquals("TypeCd", "QuickQuote") )
				quoteNumber = QuickQuote.generateQuickQuoteNumber(data, application);
			else	
				quoteNumber = Quote.generateQuoteNumber(data, application);
			application.setValue("ApplicationNumber", quoteNumber);
			basicPolicy.setValue("QuoteNumber", quoteNumber);
			basicPolicy.setValue("QuoteNumberLookup",Quote.generateQuoteLookup(data, application));
			
			// Set Term Expiration Date
			basicPolicy.setValue("ExpirationDt",Application.getExpirationDt(application,false));
						
			// Default All Other Product Setup Details
			productSetup(data, application);
			
			// Set the VIP Security Level
			if( !application.gets("CustomerRef").equals("") ) {
				ModelBean customer = Customer.getCustomerBySystemId(data, Integer.parseInt(application.gets("CustomerRef")));
				application.setValue("VIPLevel",customer.gets("VIPLevel"));
			}
			
			ModelBean[] risks = application.getAllBeans("Risk");
	        for(ModelBean risk: risks){
	          	Risk.setRiskAddTransactionFields(application,risk);
	        }
			
		}
		catch(Exception e){
			Log.error(e.getMessage(), e);
			e.printStackTrace();
			throw e;
		}
	}
	
	/** Set Values Needed to Add a New Quote
	 * 
	 * @param data JDBCData connection to the database
	 * @param application ModelBean containing the application
	 * @throws Exception when an error occurs
	 */
	public static void addQuote(JDBCData data, ModelBean application) throws Exception{
		
		addQuote(data, application, true, true);
		
	}
	/**
	 * Fills in LocationNumber, Status, and LocationDesc if it isn't in the Location Bean
	 * LocationNumber defaults to next available location number
	 * Status defaults to Active
	 * LocationDesc defaults to product specific default-location-description
	 * @param locationBeans
	 */
	private static void populateRequiredLocationFields(ModelBean application, ModelBean[] locationBeans) throws Exception {
		//LocationNumber
		for(int i = 0; i < locationBeans.length; i++) {
			ModelBean location = locationBeans[i];
			if(location.valueEquals("LocationNumber", "")) {
				// Increment the location number
				int nextNum = Location.nextNumber(application, location);
				location.setValue("LocationNumber", Integer.toString(nextNum));
			}
			if(location.valueEquals("Status", "")) {
				// Set the status to Active
				location.setValue("Status", "Active");
			}
			if(location.valueEquals("LocationDesc", "")) {
				//get the default location in an application or quote from product list.xml file if not setup then use default location as Property Location
				ModelBean basicPolicy = application.getBean("BasicPolicy");
				ModelBean coderef = ProductSetup.getProductCoderef(basicPolicy.gets("productVersionIdRef"), "UW::underwriting::default-location-description::all");
				if (coderef != null) {
					ModelBean[] options = coderef.getBean("Options").getBeans("Option");
					for (ModelBean option : options) {
						location.setValue("LocationDesc", option.gets("Value"));
						break;
					}
				} else {
					location.setValue("LocationDesc", "Property Location");
				}
			}
		}
	}
}
