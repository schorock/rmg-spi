/*
 * QuoteAdd.java
 *
 */

package com.ric.uw.quote.handler;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

import com.iscs.common.tech.log.Log;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.uw.app.Application;
import com.iscs.uw.common.coverage.Coverage;
import com.iscs.uw.common.risk.Risk;
import com.ric.uw.quote.Quote;
import com.iscs.workflow.TaskException;
/** Add A Quote
 *
 * @author  moniquef
 */
public class QuoteAdd extends InnovationIBIZHandler {

    /** Creates a new instance of QuoteAdd
     * @throws Exception never
     */
    public QuoteAdd() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a Greeting
            Log.debug("Processing QuoteAdd...");
            ModelBean rs = this.getHandlerData().getResponse();
            ModelBean rq = this.getHandlerData().getRequest();
            JDBCData data = this.getHandlerData().getConnection();
            AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest() );
            ModelBean responseParams = rs.getBean("ResponseParams");
            ModelBean xfdf = responseParams.getBean("XFDF");
            
            // Get the task system id
            ModelBean taskSystemIdBean = xfdf.findBeanByFieldValue("Param", "Name", "TaskSystemId");
            String taskId = "";
            if(taskSystemIdBean != null){
            	taskId = taskSystemIdBean.gets("Value");   
            }
            String logKey = ap.gets("LogKey");
            StringDate todayDt = DateTools.getStringDate(responseParams);
            String userId = rq.getBean("RequestParams").gets("UserId");
            
            // Get the Application ModelBean
            ModelBean application = rs.getBean("Application");           
            
            // Set the Customer Reference Field
            if(ap.gets("CustomerSystemId").length() > 0){
            	application.setValue("CustomerRef", ap.gets("CustomerSystemId"));	
            }
            
            try {
            	// Set Values Needed to Add a New Quote
            	String typeCd = application.gets("TypeCd");
            	if(typeCd.equalsIgnoreCase("ExternalQuote")){
            		// If the quote is an external quote passed into the system, do not attempt to initialize the quote with
            		// a default line and risk
            		application.setValue("TypeCd", "Quote");
            		Quote.addQuote(data, application, false, false);          
            	}
            	else{
            		// This quote was created internally, so create the default line and risk
            		application.setValue("TypeCd", "Quote");
            		Quote.addQuote(data, application, true, true);    
            	}
            	
            	// Default Coverage Commission from Producer
    			try {
    				Coverage.setCommissionPercent(data, application);
    			} catch( Exception e ) {
    				addErrorMsg("General", e.getMessage(), ErrorTools.GENERIC_BUSINESS_ERROR,"Warn");
    			}
            	
                // Set the ApplicationInfo fields
                ModelBean applicationInfo = application.getBean("ApplicationInfo");
                applicationInfo.setValue("AddDt", todayDt);
                applicationInfo.setValue("AddTm", responseParams.gets("TransactionResponseTm"));
                applicationInfo.setValue("AddUser", userId);    
                
                // Create Provider Link References For All The Quotes To The Customer
                if (rs.hasBeanField("Customer") && rs.getBean("Customer") != null) {
                	// The response has a Customer bean so make sure we pass it along so it gets updated
                	// by Application.createCustomerProviderLink too
	                ModelBean customer = rs.getBean("Customer");
	            	Application.createCustomerProviderLink(data, application, customer);
                } else {
	            	Application.createCustomerProviderLink(data, application);
                }
            
                // Default the RiskBeanName field in Risk(s)
                ModelBean[] risks = application.getAllBeans("Risk");
                for (ModelBean risk : risks) {
                    Risk.setRiskBeanName(application, risk);
                }
            }
            catch( Exception e) {
                addErrorMsg("General", e.getMessage(), ErrorTools.GENERIC_BUSINESS_ERROR);
                throw new IBIZException();
            }
                        
            // Force database insertion so that tasks can obtain the system id
            BeanTools.saveBean(application, data);
                 
            try {
            	
            	// Default the new quote task template to "QuoteInProcess"
            	String newQuoteProductTaskAction = "QuoteInProcess";
            	
            	if(ap.gets("NewQuoteProductTaskAction").length() > 0){
            		newQuoteProductTaskAction = ap.gets("NewQuoteProductTaskAction");
            	}
            	
                Quote.addSystemTasks(data, application, rs.getBean("ResponseParams"), "UWQuote", logKey, newQuoteProductTaskAction);
                if (taskId.length() > 0){
            		Quote.completeNewBusinessTask(data,taskId, responseParams, application);
            	}
                
            } 
            catch( TaskException e ) {
                addErrorMsg("General", e.getMessage(), ErrorTools.GENERIC_BUSINESS_ERROR);
                application.setValue("SystemId","");
                throw new IBIZException();
            }
            
            BeanTools.saveBean(application, data);
            
            // Return the Response ModelBean
            return rs;
        }
        catch( IBIZException e ) {
            throw new IBIZException();
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
    
}

