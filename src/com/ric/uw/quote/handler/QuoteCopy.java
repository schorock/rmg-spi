/*
 * QuoteCopy.java
 *
 */

package com.ric.uw.quote.handler;

import java.util.ArrayList;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.Helper_Beans;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

import com.iscs.common.business.logging.Logger;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.NameValuePair;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.uw.policy.PM;
import com.iscs.uw.quote.Quote;

/** Copy A Quote 
 *
 * @author  Benjamin Olsen
 */
public class QuoteCopy extends InnovationIBIZHandler {
    
    /** Creates a new instance of QuoteCopy
     * @throws Exception never
     */
    public QuoteCopy() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a Greeting
            Log.debug("Processing QuoteCopy...");
            ModelBean rq = this.getHandlerData().getRequest();
            ModelBean rs = this.getHandlerData().getResponse();
            JDBCData data = this.getHandlerData().getConnection();
            ModelBean responseParams = rs.getBean("ResponseParams");
            AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest() );  
            String logKey = ap.gets("LogKey");
            String linkInd = ap.gets("LinkInd");
            String linkDescription = ap.gets("LinkDescription");
            String currentDescription = ap.gets("CurrentDescription");
            String userId = rq.getBean("RequestParams").gets("UserId");
            StringDate todayDt = DateTools.getStringDate(responseParams);
                        
            // Get Current Quote
            ModelBean application = rs.getBean("Application");
                                    
            // Create Copy of Current Quote
            ModelBean newApplication = Helper_Beans.makeNewBean("Application");
            Helper_Beans.copyValues(application,newApplication);
            newApplication.setValue("SystemId", "");
            newApplication.setValue("id", "");
            newApplication.nextUid();
            newApplication.setValue("TypeCd", "Quote");

            // Set the Status
            newApplication.setValue("Status", "In Process");

            // Set the Quote Number
			String quoteNumber = Quote.generateQuoteNumber(data, application);
			newApplication.setValue("ApplicationNumber", quoteNumber );
			
			ModelBean newBasicPolicy = application.getBean("BasicPolicy").cloneBean();			
			newBasicPolicy.setValue("QuoteNumber", quoteNumber);
			newApplication.deleteBeanById("BasicPolicy", newApplication.getBean("BasicPolicy").getId());
			newApplication.addValue(newBasicPolicy);
			
			// Set the Quote Number Lookup Format			
			String code = Quote.generateQuoteLookup(data, newApplication);
			newApplication.getBean("BasicPolicy").setValue("QuoteNumberLookup", code);			
            
			// Set Transaction Information
			ModelBean transactionInfo = newApplication.getBean("TransactionInfo");
			transactionInfo.setValue("TransactionCd", PM.TX_NEW_BUSINESS);
			transactionInfo.setValue("TransactionEffectiveDt", newBasicPolicy.getValue("EffectiveDt"));
			transactionInfo.setValue("TransactionShortDescription", "New Business");
			
            // Clear the Close Quote fields
            ModelBean newApplicationInfo = application.getBean("ApplicationInfo").cloneBean();
            newApplicationInfo.setValue("CloseReasonCd","");
            newApplicationInfo.setValue("CloseSubReasonCd","");
            newApplicationInfo.setValue("CloseSubReasonLabel","");
            newApplicationInfo.setValue("CloseComment","");
            newApplicationInfo.setValue("CloseDt","");
            newApplicationInfo.setValue("CloseTm","");
            newApplicationInfo.setValue("CloseUser",""); 
            newApplicationInfo.setValue("AddDt", todayDt);
            newApplicationInfo.setValue("AddTm", responseParams.gets("TransactionResponseTm"));
            newApplicationInfo.setValue("AddUser", userId);    
            newApplication.deleteBeanById("ApplicationInfo", newApplication.getBean("ApplicationInfo").getId());
            newApplication.addValue(newApplicationInfo);
            
            // Clear Submission Data
            newApplicationInfo.setValue("SubmittedDt","");
            newApplication.deleteBeans("SubmitterIssues");
            ModelBean newTransactionInfo = newApplication.getBean("TransactionInfo");
            newTransactionInfo.setValue("SubmissionNumber", "");
            
			// Copy sub beans
            newApplication.deleteBeanById("Insured", newApplication.getBean("Insured").getId());
            PM.addApplicationData(application, newApplication);            
            
            // Copy Tabs Visited
            ModelBean tabsVisited = application.getBean("TabsVisited");
            if( tabsVisited != null ) {
            	newApplication.addValue(tabsVisited);
            }
            
            // Copy Data reports
            ModelBean[] dataReportRequestArray = application.getAllBeans("DataReportRequest");
            for( ModelBean dataReport : dataReportRequestArray ) {
            	newApplication.addValue(dataReport);
            }
            
            // Add a new Task
            Quote.addSystemTasks(data, newApplication, rs.getBean("ResponseParams"), "UWQuote", logKey, "QuoteInProcess");
            
            // Save the new Quote
            BeanTools.saveBean(newApplication, data);

            // Clear pre-approval requests
            ModelBean[] preApprovalArray = newApplication.getAllBeans("PreApproval");
            for( ModelBean preApproval : preApprovalArray ) {
            	if( !StringTools.isTrue(preApproval.gets("ApprovedInd")) ) {
            		preApproval.setValue("RequestedInd","");
            	}
            }
            
            // Create new Application Baseline for pre-approval
            for( ModelBean preApproval : preApprovalArray ) {
            	if( StringTools.isTrue(preApproval.gets("ApprovedInd")) ) {
            		ModelBean applicationClone = newApplication.cloneBean();
    	            ModelBean applicationBaseline = new ModelBean("ApplicationBaseline");
    	            applicationBaseline.setValue("PreApprovalApplicationRef",newApplication.getSystemId());
    	            applicationBaseline.addValue(applicationClone);
    	            BeanTools.saveBean(applicationBaseline, data);
    	            
    	            break;
            	}
            }
            
            // Log Message 
            ArrayList<NameValuePair> a1 = new ArrayList<NameValuePair>();
            NameValuePair option = new NameValuePair("NewQuoteNumber",newApplication.gets("ApplicationNumber"));
        	a1.add(option);
        	NameValuePair[] pairs = (NameValuePair[]) a1.toArray(new NameValuePair[a1.size()]);
        	ModelBean[] beans;
            if( !application.gets("CustomerRef").equals("") ) {
            	ModelBean customer = new ModelBean("Customer");
            	data.selectModelBean(customer, Integer.parseInt(application.gets("CustomerRef")));
            	beans = new ModelBean[] {application, customer};   
            }
            else {
            	beans = new ModelBean[] {application};
            }            
            if( linkInd.equals("Yes") )
            	Logger.message(data, "CopyLinkedQuote", userId, todayDt, beans, pairs);
            else
            	Logger.message(data, "CopyQuote", userId, todayDt, beans, pairs);
            
            // Set or Clear the Master Quote Reference
            if( linkInd.equals("Yes") ) {
            	
            	boolean originalAppUpdated = false;
            	
            	// Get the Master Quote Reference
                if( application.getBean("ApplicationInfo").gets("MasterQuoteRef").equals("") ) {
                	application.getBean("ApplicationInfo").setValue("MasterQuoteRef",application.getSystemId());
                	originalAppUpdated = true;
                }  

                // Set the current quote description
                if( !currentDescription.equals("") ) {
                	application.getBean("ApplicationInfo").setValue("IterationDescription",currentDescription);
                	originalAppUpdated = true;
                }
                
                // Save the original Application if necessary
                if( originalAppUpdated ) {
                	BeanTools.saveBean(application, data);
                }
                
            	String masterQuoteRef = application.getBean("ApplicationInfo").gets("MasterQuoteRef");
                 
                // Set the Master Quote Reference
                newApplication.getBean("ApplicationInfo").setValue("MasterQuoteRef",masterQuoteRef);
                newApplication.getBean("ApplicationInfo").setValue("IterationDescription",linkDescription);
            }
            else {
            	newApplication.getBean("ApplicationInfo").setValue("MasterQuoteRef","");
            }
            
            rs.deleteBeanById(application.getId());
            rs.addValue(newApplication);
            
            // Add informational message to inform user of successfull copy
            addErrorMsg("General", "The current quote " + newApplication.gets("ApplicationNumber") + " was successfully copied from quote " + application.gets("ApplicationNumber"), ErrorTools.GENERIC_BUSINESS_ERROR, "Info");
            
            // Return the Response ModelBean
            return rs;
        }        
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
}