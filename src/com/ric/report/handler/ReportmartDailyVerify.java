/*
 * ReportmartVerify.java
 *
 */

package com.ric.report.handler;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

import com.iscs.common.render.DynamicString;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.report.ReportmartException;
import com.iscs.report.ReportmartUtils;

/**
 * ReportmartVerify - This service handler verifies the daily reportmart
 * completed successfully
 * 
 * @author patriciat
 */
public class ReportmartDailyVerify extends InnovationIBIZHandler {

	private static final String SQL_SCRIPTS =  "sqlscript-template::*::*";

	/**
	 * Creates a new instance
	 * 
	 * @throws Exception
	 *             never
	 */
	public ReportmartDailyVerify() throws Exception {
	}

	/**
	 * Processes a service request.
	 * 
	 * @return the current response bean
	 * @throws IBIZException
	 *             when an error requiring user attention occurs
	 * @throws ServiceHandlerException
	 *             when a critical error occurs
	 */
	public ModelBean process() throws IBIZException, ServiceHandlerException {
		JDBCData data = null;
		try {            

			Log.debug("Processing ReportmartDailyVerify...");		         

			ModelBean rq =  this.getHandlerData().getRequest();
			ModelBean rs = this.getHandlerData().getResponse();
			ModelBean ap = rq.getBean("RequestParams").getBean("AdditionalParams");
			ModelBean runParam = ap.findBeanByFieldValue("Param","Name", "RunDt");
			String runDt = "";

			if (runParam!=null) 
				runDt = runParam.gets("Value");			
			if (runDt.equals(""))				
				runDt = DateTools.getDate();	

			ModelBean type = ap.findBeanByFieldValue("Param","Name", "TypeCd");

			ModelBean errors = rs.getBean("ResponseParams").getBean("Errors");
			if( errors == null){
				errors = new ModelBean("Errors");
				rs.getBean("ResponseParams").addValue(errors);					
			}
			
			String reportmartTypeCd = "";
			if( type != null)
				reportmartTypeCd = DynamicString.render(rs,type.gets("Value"));
			

			if( reportmartTypeCd.equals(""))
				throw new ReportmartException("Reportmart Type Code is missing");	


			ReportmartUtils rm = new ReportmartUtils();
			StringDate reportDt = new StringDate(runDt);
			data = rm.getDataConnection(SQL_SCRIPTS, reportmartTypeCd);	
			String balanceType = "";			
			String source = "";
			if( reportmartTypeCd.equalsIgnoreCase("dailybalance")) {
				source = "Daily Balance";
				balanceType = "'Earned', 'ClaimsIncurred'";
			} else if( reportmartTypeCd.equalsIgnoreCase("dailystatstosummarystats")){
				source = "Daily Balancing Of Stats To Summary Stats" ;
				balanceType = "'AccountStatsToAccountSummaryStats','AccountingStatsToAccountingSummaryStats','ClaimStatsToClaimSummaryStats'";
			} else if( reportmartTypeCd.equalsIgnoreCase("dailywrittentoreceivables")) {
				source = "Written To Receivables";
				balanceType = "'WrittenToBilled'";
			} else if( reportmartTypeCd.equalsIgnoreCase("dailypolicysummarystatstoaccountingsummarystats")) {
				source = "Policy Summary Stats vs Accounting Summary Stats";
				balanceType = "'PolicySummaryStatsToAccountingSummaryStats'";
			} else if( reportmartTypeCd.equalsIgnoreCase("dailypolicysummarystatsearnedandunearnedcheck")) {
				source = "Policy Summary Stats Earned and Unearned Check";
				balanceType = "'CheckUnearnedAndEarneds'";
			}  
			
			
			String sql = "Select count(*) from daybalance where reportdate =" + data.getDelegate().formatForSQL(reportDt) + " and BalanceType in (" + balanceType + ")";
			Log.debug("Executing query..." + sql);
			ResultSet result = data.doSQL(sql);
			if( result.next()){
				if( result.getString(1).equals("0")) {
					ModelBean error = ErrorTools.createError("ProcessingError", source + " Script might not have completed successfully..." + DateTools.getDate(reportDt, "MM/dd/yyyy") + " to identify issues.", ErrorTools.GENERIC_BUSINESS_ERROR,ErrorTools.SEVERITY_ERROR);
					errors.addValue(error);	
				}
			} else {
				throw new ReportmartException("Query returns null ResultSet");	
			}
			
			sql = "Select count(*) from daybalance where reportdate = " + data.getDelegate().formatForSQL(reportDt) + " and BalanceType in (" + balanceType + ") and BalanceStatusCd = 'ERROR'";
			Log.debug("Executing query..." + sql);
			result = data.doSQL(sql);
			if( result.next()){
				if( result.getInt(1) > 0) {
					ModelBean error = ErrorTools.createError("ProcessingError", source + " contains errors! Run the Day Balance and Day Balance Detail Reports under Operations menu for " + DateTools.getDate(reportDt, "MM/dd/yyyy") + " to identify issues.", ErrorTools.GENERIC_BUSINESS_ERROR,ErrorTools.SEVERITY_ERROR);
					errors.addValue(error);	
				}
			} else {
				throw new ReportmartException("Query returns null ResultSet");	
			}
			
			
			if( reportmartTypeCd.equalsIgnoreCase("dailybalance")) {

				sql = "Select count(*) from accountbalance where AsOfDt = " + data.getDelegate().formatForSQL(reportDt);
				Log.debug("Executing query..." + sql);
				result = data.doSQL(sql);
				if( result.next()){
					if( result.getString(1).equals("0")) {
						ModelBean error = ErrorTools.createError("ProcessingError", source + " Script might not have completed successfully..."   + DateTools.getDate(reportDt, "MM/dd/yyyy") + " to identify issues.", ErrorTools.GENERIC_BUSINESS_ERROR,ErrorTools.SEVERITY_ERROR);
						errors.addValue(error);		
					}
				} else {
					throw new ReportmartException("Query returns null ResultSet");	
				}
				
				sql = "Select TotalAmt  from accountbalance where AsOfDt = " + data.getDelegate().formatForSQL(reportDt) + " and SequenceNum = '903'";
				Log.debug("Executing query..." + sql);
				result = data.doSQL(sql);
				if( result.next()){
					if( result.getBigDecimal(1).compareTo(new BigDecimal("0.00")) != 0) {
						ModelBean error = ErrorTools.createError("ProcessingError", "Receivables are not in balance! Run the Receivable Reconciliation Report under Accounting Billing Reports menu for "   + DateTools.getDate(reportDt, "MM/dd/yyyy") + " to identify issues.", ErrorTools.GENERIC_BUSINESS_ERROR,ErrorTools.SEVERITY_ERROR);
						errors.addValue(error);	
					}
				} else {
					throw new ReportmartException("Query returns null ResultSet");	
				}

				sql = "Select TotalLineAmt from accountbalance where AsOfDt = " + data.getDelegate().formatForSQL(reportDt) + " and SequenceNum = '903'";
				Log.debug("Executing query..." + sql);
				result = data.doSQL(sql);
				if( result.next()){
					if( result.getBigDecimal(1).compareTo(new BigDecimal("0.00")) != 0) {
						ModelBean error = ErrorTools.createError("ProcessingError", "Receivables are not in balance! Run the Receivable Reconciliation Report under Accounting Billing Reports menu for "   + DateTools.getDate(reportDt, "MM/dd/yyyy") + " to identify issues.", ErrorTools.GENERIC_BUSINESS_ERROR,ErrorTools.SEVERITY_ERROR);
						errors.addValue(error);		
					}
				} else {
					throw new ReportmartException("Query returns null ResultSet");	
				}
				
			}
			data.commit();

			// Return
			return null;

		} catch( Exception e ) {
			try {
				Log.error(e);
				if( data!=null) data.rollback();
			} catch( SQLException sqle){
				Log.error(sqle);
			}			
			throw new ServiceHandlerException(e);
			
		} finally {
			if (data != null) {
				data.closeConnection();
			}
		}
	}  
}