package com.ric.common.handler;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCConfig;
import net.inov.tec.data.JDBCData;

/*
 * This job is to be used post-production to add new columns to data mart tables when the
 * Data Model has been extended.
 *
 */
public class AlterDatamartTables extends InnovationIBIZHandler{


	/** Creates a new instance of AlterDatamartTables
	 * @throws Exception never
	 */
	public AlterDatamartTables() throws Exception {
	}

	/** Processes a service request.
	 * @return the current response bean
	 * @throws IBIZException when an error requiring user attention occurs
	 * @throws ServiceHandlerException when a critical error occurs
	 */
	public ModelBean process() throws IBIZException, ServiceHandlerException {
		try {

			Log.debug("Processing AlterDatamartTables...");

			//Add new columns
			addColumntoDataMartTables();

			return null;

		} catch (Exception e){
			throw new ServiceHandlerException(e);
		}
	}

	/** Add New Columns
	 * @return the current response bean
	 * @throws IBIZException when an error requiring user attention occurs
	 * @throws ServiceHandlerException when a critical error occurs
	 */
	protected ModelBean addColumntoDataMartTables() throws IBIZException, ServiceHandlerException {
		JDBCData dataMartConnection = null;
		try {

			Log.debug("Processing AlterDatamartTables...");

			dataMartConnection = new JDBCData(new JDBCConfig("jdbc/jdbc_datamart"));

			String tableName = "";
			String columnName = "";
			
			//Example: Add DwellingStyleCd to building table
			/* 
			tableName = "building";
			columnName = "DwellingStyleCd";

			if(!columnExists(dataMartConnection, tableName, columnName)){
				addColumn(dataMartConnection, tableName, columnName);
				dataMartConnection.commit();
			}
			*/ 
			

			return null;
		} catch( Exception e ) {
			try {
				Log.error(e);
				if( dataMartConnection!=null) dataMartConnection.rollback();
			} catch( SQLException sqle){
				Log.error(sqle);
			}
			throw new ServiceHandlerException(e);

		} finally {
			if (dataMartConnection != null) {
				dataMartConnection.closeConnection();
			}
		}
	}


	/**
	 * @param data JDBCData The data connection
	 * @param tableName String The table name
	 * @throws SQLException
	 */
	public void addColumn(JDBCData data, String tableName, String columnName) throws SQLException{

		String sql = null;
			sql =
				"ALTER TABLE " + data.getDelegate().tableName(data, tableName) + " " +
				"ADD (" + columnName + " VARCHAR(255) NULL) ";

		data.getDelegate().prepareAndExecStmt(data, sql);

	}
	
	public void addIntegerColumn(JDBCData data, String tableName, String columnName) throws SQLException{

		String sql = null;
			sql =
				"ALTER TABLE " + data.getDelegate().tableName(data, tableName) + " " +
				"ADD (" + columnName + " INT(11) NULL) ";

		data.getDelegate().prepareAndExecStmt(data, sql);

	}
	
	public void addDateColumn(JDBCData data, String tableName, String columnName) throws SQLException{

		String sql = null;
			sql =
				"ALTER TABLE " + data.getDelegate().tableName(data, tableName) + " " +
				"ADD (" + columnName + " DATE NULL) ";

		data.getDelegate().prepareAndExecStmt(data, sql);

	}
	
	public void addMultipleColumns(JDBCData data, String sql) throws SQLException{

		data.getDelegate().prepareAndExecStmt(data, sql);

	}


	public void addTimeStampColumn(JDBCData data, String tableName, String columnName) throws SQLException{

		String sql = null;
			sql =
				"ALTER TABLE " + data.getDelegate().tableName(data, tableName) + " " +
				"ADD (" + columnName + " timestamp NULL) ";

		data.getDelegate().prepareAndExecStmt(data, sql);

	}
	
	
	public boolean columnExists(JDBCData data, String tableName, String columnName) {
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT " + columnName+" FROM ")
		   .append(data.getDelegate().tableName(data, tableName))
		   .append(" where 0=1");

		try {
			//data.getDelegate().prepareAndExecStmt(data, sql.toString());
			rs = data.doSQL(sql.toString());
			return true;
		} catch (SQLException sqle) {
			return false;
		}
	}


}