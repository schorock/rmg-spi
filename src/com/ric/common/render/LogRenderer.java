package com.ric.common.render;


import net.inov.tec.beans.ModelBean;

import com.iscs.common.render.Renderer;
import com.iscs.common.tech.log.Log;

/** Specific Product Rendering Tools
*
* @author  luann
*/
public class LogRenderer implements Renderer {

	@Override
	public String getContextVariableName() {
		// TODO Auto-generated method stub
		return "LogRenderer";
	}
	
	public static void dumpString(Object bean1) {
		if (bean1 instanceof String) {
			String bean = (String) bean1;
			Log.debug("::Start of velocity string dump.::::");
			for (char c : bean.toCharArray()) {
				Log.debug(((int) c) +" "+ c);				
			}
			Log.debug("::End of velocity variable dump.::::");
		}
		else {
			Log.debug("::Not a String.::::");
		}
	}
	
/*  Velocity examples of using the Debug statement below. You can have three values to process or 
*		you can have just text value to see workflow. If you need more than three values just call 
*		the method again.
*	vtl file example
*	$LogRenderer.viewVelocityVariables("$waiverSubrExposureAmt","test","some other value")
*	html file example
*	<!--  $LogRenderer.viewVelocityVariables("$glClass","some other value","test") -->
*/
    public static void viewVelocityVariables(Object bean1, Object bean2, Object bean3){
		Log.debug("::::Start of velocity variable check.::");
    	if(bean1 instanceof ModelBean){
    		Log.debug(((ModelBean)bean1).getBeanName()+"("+bean1.getClass().getSimpleName()+")");
    	}else{
    		Log.debug(bean1!=null?bean1.toString()+"("+bean1.getClass().getSimpleName()+")":"");
    	}
    	if(bean2 instanceof ModelBean){
    		Log.debug(((ModelBean)bean2).getBeanName()+"("+bean2.getClass().getSimpleName()+")");
    	}else{
    		Log.debug(bean2!=null?bean2.toString()+"("+bean2.getClass().getSimpleName()+")":"");
    	}
    	if(bean3 instanceof ModelBean){
    		Log.debug(((ModelBean)bean3).getBeanName()+"("+bean3.getClass().getSimpleName()+")");
    	}else{
    		Log.debug(bean3!=null?bean3.toString()+"("+bean3.getClass().getSimpleName()+")":"");
    	}
		Log.debug("::End of velocity variable check.::::");
    	
	}
    
	
    public static void viewVelocityVariables(Object[] beans){
		Log.debug("::::Start of velocity variable check.::");
		for (Object bean : beans) {
	    	if (bean instanceof ModelBean){
	    		Log.debug(((ModelBean)bean).getBeanName());
	    	} else {
	    		Log.debug(bean!=null ? bean.toString() : "");
	    	}
	    }
 
		Log.debug("::End of velocity variable check.::::");
    	
	}
}
