<?xml version="1.0" encoding="ISO-8859-1"?>

<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">

  <html>

  <head>

    <style type="text/css">

body {
  font-family: verdana,sans-serif;
  font-size: 12px;
}

h5 {
  text-decoration: underline;
}

table {
  font-size: 11px;
}

.header {
  background-color: #9acd32;
}

.set-row {
  background-color: #BBBBBB;
  font-weight: bold;
}

.separator {
  background-color: #BBBBBB;
}

.duplicate {
  background-color: #FFBBBB;
}

.duplicateInSet {
  background-color: #CCFFCC;
}

.duplicateInOtherSet {
  background-color: #CCCCFF;
}

.undefined {
  background-color: #FFBBBB;
}

.oddRow {
  background-color: #EEEEEE;
}

.oddRow .duplicateInSet {
  background-color: #CCFFCC;
}

.oddRow .duplicateInOtherSet {
  background-color: #CCCCFF;
}

.oddRow .oddCol {
  background-color: #EEEEEE;
}

.oddRow .oddCol.duplicate {
  background-color: #FFBBBB;
}

.oddRow .evenCol {
  background-color: #DDDDDD;
}

.oddRow .evenCol.duplicate {
  background-color: #EEBBBB;
}

.evenRow .duplicateInSet {
  background-color: #BBEEBB;
}

.evenRow .duplicateInOtherSet {
  background-color: #BBBBEE;
}

.evenRow .oddCOl {
  background-color: #FFFFFF;
}

.evenRow .oddCol.duplicate {
  background-color: #EEBBBB;
}

.evenRow .evenCOl {
  background-color: #EEEEEE;
}

.evenRow .evenCol.duplicate {
  background-color: #FFBBBB;
}

.cell {
  text-align: center;
  word-break: break-all;
  word-wrap: break-word;
  -moz-hyphens: auto;
  hyphens: auto;
}

    </style>

  </head>

  <body>

    <h2>Authority Sets and Roles</h2>
    <table id="mainTable">
    <tr class="header">
      <!-- fixed columns -->
      <th align="left">Set</th>
      <th align="left">Attribute</th>
<!-- 
      <th align="left">Name</th>
-->
      <th align="left">Description</th>
      <th align="left">Default</th>
      <th></th>
      <!-- dynamic columns -->
      <xsl:for-each select="document('authority-role.xml')//AuthorityRole">
        <th aligh="center"><xsl:value-of select="@id"/></th>
      </xsl:for-each>
    </tr>

    <xsl:variable name="roleCount" select="count(document('authority-role.xml')//AuthorityRole)"/>

    <!-- Rows for normal attributes defined in sets ni authority-set.xml -->
    
    <xsl:for-each select="document('authority-set.xml')//AuthorityAttributeSet">
      <xsl:sort select="@id"/>
      <xsl:variable name="setId" select="@id"/>
      <!-- set header -->
      <tr class="set-row">
        <td>
          <xsl:attribute name="colspan">
            <xsl:value-of select="$roleCount + 4"/>
          </xsl:attribute>
          <xsl:value-of select="$setId"/>
        </td>
      </tr>

      <xsl:for-each select="AuthorityAttribute">
        <xsl:sort select="@id"/>
        <xsl:variable name="attributeId" select="@id"/>
        <xsl:variable name="attributeDefault" select="@DefaultValue"/>
        <xsl:variable name="attributeName" select="@Name"/>
        <xsl:variable name="attributeDescription" select="@Description"/>

        <xsl:if test="position() mod 2 = 1">
          <xsl:call-template name="row">
            <xsl:with-param name="setId" select="$setId"/>
            <xsl:with-param name="attributeId" select="$attributeId"/>
            <xsl:with-param name="attributeDefault" select="$attributeDefault"/>
            <xsl:with-param name="attributeName" select="$attributeName"/>
            <xsl:with-param name="attributeDescription" select="$attributeDescription"/>
            <xsl:with-param name="rowClass" select="'oddRow'"/>
          </xsl:call-template>
        </xsl:if>

        <xsl:if test="position() mod 2 = 0">
          <xsl:call-template name="row">
            <xsl:with-param name="setId" select="$setId"/>
            <xsl:with-param name="attributeId" select="$attributeId"/>
            <xsl:with-param name="attributeDefault" select="$attributeDefault"/>
            <xsl:with-param name="attributeName" select="$attributeName"/>
            <xsl:with-param name="attributeDescription" select="$attributeDescription"/>
            <xsl:with-param name="rowClass" select="'evenRow'"/>
          </xsl:call-template>
        </xsl:if>

      </xsl:for-each>
    </xsl:for-each>

    </table>

    <h5>Issue Legend:</h5>
    <table>
      <tr><td class="duplicate">Duplicate AuthorityRoleAttribute in authority-role.xml</td></tr>
      <tr><td class="duplicateInSet">Duplicate AuthorityAttribute in the same AuthorityAttributeSet in authority-set.xml</td></tr>
      <tr><td class="duplicateInOtherSet">Duplicate AuthorityAttribute in another AuthorityAttributeSet in authority-set.xml</td></tr>
    </table>

    <h5>Sets or Attributes referenced in authority-role.xml but missing in authority-set.xml:</h5>
    <table>
      <tr class="header">
        <!-- fixed columns -->
        <th align="left">Role</th>
        <th align="left">Set</th>
        <th align="left">Attribute</th>
        <th align="left">Value</th>
      </tr>
    
    <xsl:for-each select="document('authority-role.xml')//AuthorityRoleAttribute">
      <xsl:sort select="../@AuthorityAttributeSetIdRef"/>
      <xsl:sort select="@AuthorityAttributeIdRef"/>
      <xsl:variable name="roleId" select="../../@id"/>
      <xsl:variable name="setId" select="../@AuthorityAttributeSetIdRef"/>
      <xsl:variable name="attributeId" select="@AuthorityAttributeIdRef"/>
      <xsl:variable name="countDefinedSet" select="count(document('authority-set.xml')//AuthorityAttributeSet[@id=$setId])"/>
      <xsl:variable name="countDefinedAttribute" select="count(document('authority-set.xml')//AuthorityAttributeSet[@id=$setId]/AuthorityAttribute[@id=$attributeId])"/>

      <xsl:if test="$countDefinedSet = 0 or $countDefinedAttribute = 0">
        <tr class="oddRow">
          <td><xsl:value-of select="$roleId"/></td>
          <td>
            <xsl:attribute name="class">
              <xsl:choose>
                <xsl:when test="$countDefinedSet = 0">
                  <xsl:value-of select="'undefined'"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="''"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <xsl:value-of select="$setId"/>
          </td>
          <td>
            <xsl:attribute name="class">
              <xsl:choose>
                <xsl:when test="$countDefinedAttribute = 0">
                  <xsl:value-of select="'undefined'"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="''"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <xsl:value-of select="$attributeId"/>
          </td>
          <td><xsl:value-of select="@Value"/></td>
        </tr>
      </xsl:if>
    </xsl:for-each>
    
    </table>

  </body>
  </html>
</xsl:template>

<xsl:template name="row">
  <xsl:param name="setId"/>
  <xsl:param name="attributeId"/>
  <xsl:param name="attributeDefault"/>
  <xsl:param name="attributeName"/>
  <xsl:param name="attributeDescription"/>
  <xsl:param name="rowClass"/>

  <tr>
    <xsl:attribute name="class">
      <xsl:value-of select="$rowClass"/>
    </xsl:attribute>
    <!-- attribute definition columns -->
    <xsl:variable name="countSetAttribute" select="count(../AuthorityAttribute[@id=$attributeId])"/>
    <xsl:variable name="countOtherSetAttribute" select="count(../../AuthorityAttributeSet[@id != $setId]/AuthorityAttribute[@id=$attributeId])"/>
    <td width="10px">-</td>
    <td>
      <xsl:attribute name="class">
        <xsl:choose>
          <xsl:when test="$countSetAttribute > 1">
            <xsl:value-of select="'duplicateInSet'"/>
          </xsl:when>
          <xsl:when test="$countOtherSetAttribute > 0">
            <xsl:value-of select="'duplicateInOtherSet'"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="''"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:value-of select="$attributeId"/>
    </td>
<!-- 
    <td><xsl:value-of select="$attributeName"/></td>
-->
    <td><xsl:value-of select="$attributeDescription"/></td>
    <td><xsl:value-of select="$attributeDefault"/></td>
    <!-- role override (value) columns (dynamic) -->
    <td class="separator">&nbsp;&nbsp;</td>
    <xsl:for-each select="document('authority-role.xml')//AuthorityRole">
      <xsl:if test="position() mod 2 = 1">
        <xsl:call-template name="cell">
          <xsl:with-param name="role" select="@id"/>
          <xsl:with-param name="setId" select="$setId"/>
          <xsl:with-param name="attributeId" select="$attributeId"/>
          <xsl:with-param name="colClass" select="'oddCol'"/>
        </xsl:call-template>
      </xsl:if>
      <xsl:if test="position() mod 2 = 0">
        <xsl:call-template name="cell">
          <xsl:with-param name="role" select="@id"/>
          <xsl:with-param name="setId" select="$setId"/>
          <xsl:with-param name="attributeId" select="$attributeId"/>
          <xsl:with-param name="colClass" select="'evenCol'"/>
        </xsl:call-template>
      </xsl:if>
    </xsl:for-each>
  </tr>
</xsl:template>

<xsl:template name="cell">
  <xsl:param name="role"/>
  <xsl:param name="setId"/>
  <xsl:param name="attributeId"/>
  <xsl:param name="colClass"/>

  <xsl:variable name="countRoleSetAttribute" select="count(AuthorityRoleSet[@AuthorityAttributeSetIdRef=$setId]/AuthorityRoleAttribute[@AuthorityAttributeIdRef=$attributeId])"/>

  <td>
    <xsl:attribute name="class">
      <xsl:choose>
        <xsl:when test="$countRoleSetAttribute > 1">
          <xsl:value-of select="concat('cell',' ',$colClass,' ','duplicate')"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="concat('cell',' ',$colClass)"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    <xsl:value-of select="AuthorityRoleSet[@AuthorityAttributeSetIdRef=$setId]/AuthorityRoleAttribute[@AuthorityAttributeIdRef=$attributeId]/@Value"/>
  </td>
</xsl:template>

</xsl:stylesheet>