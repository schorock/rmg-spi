package com.ric.common.report.render;

import java.util.Calendar;

import com.iscs.common.render.Renderer;
import com.iscs.common.report.render.ReportRenderer;
import com.iscs.common.tech.log.Log;

import net.inov.tec.date.StringDate;



public class RICReportRenderer implements Renderer {

	/** Creates a new instance of ReportRenderer */
	public RICReportRenderer() {
		
	}

	/** Context Variable Getter
	 * @return DateRenderer
	 */     
	public String getContextVariableName() {
		return "RICReportRenderer";
	}
	
	public static void debug(String string){
		System.out.println(string);
	}
	
	/**get prior report period list
	 * @param date
	 * @param months
	 * @return list of the prior report period
	 * @throws Exception
	 */
	public static String getPriorReportPeriodList(StringDate date) throws Exception {
		Log.debug("pass in: " + date.toString());
		StringBuffer priorYearReportPeriodList = new StringBuffer();
		String reportPeriod = ReportRenderer.getReportPeriod(date);
		priorYearReportPeriodList.append(getPriorYearReportPeriodList(reportPeriod));
		priorYearReportPeriodList.append(getCurrentBOYReportPeriodList(date));
		
		return priorYearReportPeriodList.toString();
	}
	
	/**Get prior year report period list
	 * 
	 * @param reportPeriod
	 * @param months
	 * @return list of the prior year report period list
	 * @throws Exception
	 */
	public static String getPriorYearReportPeriodList(String reportPeriod) throws Exception {
		StringBuffer priorYearReportPeriodList = new StringBuffer();
		String priorYearReportPeriod = ReportRenderer.getPriorReportPeriod(reportPeriod,11);
		String priorYear = priorYearReportPeriod.substring(0,4);
		String priorYearMonth = priorYearReportPeriod.substring(4,6);
		if(priorYear.equals(reportPeriod.substring(0,4)))
			return "";
		for( int i = Integer.valueOf(priorYearMonth); i <=12 ; i++){
			priorYearReportPeriodList.append("'");
			if(i<10)
				priorYearReportPeriodList.append(priorYear + "0" + i);
			else
				priorYearReportPeriodList.append(priorYear + i);
			priorYearReportPeriodList.append("',");
		}
		
		return priorYearReportPeriodList.toString();
	}
	
	/**Get current BOY report period list
	 * @param date
	 * @return list of current BOY(Beginning of year) report period list
	 * @throws Exception
	 */
	public static String getCurrentBOYReportPeriodList(StringDate date) throws Exception {
		Calendar cal = date.getCalendar();    	
		int currentReportPeriodMonth = Integer.valueOf(ReportRenderer.getFormattedMonth(date));
		String currentReportPeriodYear = Integer.toString(cal.get(Calendar.YEAR));
		StringBuffer currentBOYReportPeriodList = new StringBuffer();
		
		for(int i = 1 ; i<=currentReportPeriodMonth; i ++){
			currentBOYReportPeriodList.append("'");
			if(i < 10)
				currentBOYReportPeriodList.append(currentReportPeriodYear + "0" + i);
			else
				currentBOYReportPeriodList.append(currentReportPeriodYear + i);
			currentBOYReportPeriodList.append("'");
			if(i!=currentReportPeriodMonth)
				currentBOYReportPeriodList.append(",");
		}
		
		return currentBOYReportPeriodList.toString();
	}

	/**Get year end report period list
	 * @param minReportPeriod
	 * @param date
	 * @return list of year end report period list
	 * @throws Exception
	 */
	public static String getYearEndReportPeriodList(String minReportPeriod, StringDate date) throws Exception {
		
		StringBuffer reportPeriodList = new StringBuffer();
		//if min report period is null, it means that we don't have any data (empty table - system recently cleared/initialized)
		if(minReportPeriod==null){
			reportPeriodList.append("''");
			return reportPeriodList.toString();
		}
		
		int minReportYear = Integer.valueOf(minReportPeriod.substring(0,4));
		String currentReportPeriod = ReportRenderer.getReportPeriod(date);
		Calendar cal = date.getCalendar();
		int currentYear = cal.get(Calendar.YEAR);

				
		for(int i = minReportYear; i <= currentYear; i++){
			if(!currentReportPeriod.substring(4, 6).equals("12") && i == currentYear){
				if(reportPeriodList != null && reportPeriodList.length() > 0 ){
					reportPeriodList.replace(reportPeriodList.lastIndexOf(","), reportPeriodList.lastIndexOf(",") + 1, "");
					continue;
				}else{
					continue;
				}
			}

			reportPeriodList.append("'");
			reportPeriodList.append(i + "12");
			reportPeriodList.append("'");
			if(i!=currentYear){
				reportPeriodList.append(",");
			}
		}
		
		//If there is only one month of data and reportPeriodList is empty then return empty list
        if(reportPeriodList.length() == 0 ){
               reportPeriodList.append("''");
        }

		
		return reportPeriodList.toString();
	}
	
	/**Get current BOY report period array
	 * @param date
	 * @return list of current BOY(Beginning of year) report period array
	 * @throws Exception
	 */
	public static String[] getCurrentBOYReportPeriodArray(StringDate date) throws Exception {
		Calendar cal = date.getCalendar();    	
		int currentReportPeriodMonth = Integer.valueOf(ReportRenderer.getFormattedMonth(date));
		String currentReportPeriodYear = Integer.toString(cal.get(Calendar.YEAR));		
		String[] array = new String[currentReportPeriodMonth];
		for(int i = 1 ; i<=currentReportPeriodMonth; i ++){			
			if(i < 10)
				array[i-1] = currentReportPeriodYear + "0" + i;
			else
				array[i-1] = currentReportPeriodYear + i;			
		}
		
		return array;
	}
}
