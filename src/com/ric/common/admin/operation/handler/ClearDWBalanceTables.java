/*
 * ClearDWBalanceTables.java
 *
 */

package com.ric.common.admin.operation.handler;

import java.sql.ResultSet;

import net.inov.biz.server.*;
import net.inov.tec.beans.*;
import net.inov.tec.data.JDBCData;
import net.inov.tec.web.cmm.AdditionalParams;

import com.iscs.common.tech.log.Log;
import com.iscs.common.shared.SystemData;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.utility.InnovationUtils;
import com.iscs.common.utility.error.ErrorTools;
import net.inov.tec.date.StringDate;

/** job to clear contents of data warehouse tables when resetting an environment
 *
 * @author  bobf
 */
public class ClearDWBalanceTables extends InnovationIBIZHandler {
    // Create an object for logging messages
    
	String[] tableNames = new String[]{"accountbalance","annualstatement","daybalance","daybalancedetail",
			"eombalance","eombalanceDetail","experiencechart","experienceranking","experiencereport","inforce",
			"premiumtransactionexperience","productionexperiencestats","reinsurancerecap","rolling12month",
			"schedulep","schedulepdriver","outofbalance"};
	
    
    /** Creates a new instance of BookDateSet
     * @throws Exception never
     */
    public ClearDWBalanceTables() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a Greeting
            Log.debug("Processing ClearDWBalanceTables...");
            ModelBean rs = this.getHandlerData().getResponse();
            JDBCData data = InnovationUtils.getDataMartConnection();

            StringBuilder sb = new StringBuilder();
            
            /*Loop through defined tables and delete records if they exist*/
            for(String table: tableNames){
            	String tableName = data.getDelegate().tableName(data,table);
            	int result = this.resultCount(data, tableName);
                
                if(result>0){
    	    		this.deleteRecords(data, tableName);
    	    		data.commit();
                }
            }
            
            data.commit();
            
            // Return the Response ModelBean
            return rs;
        }
        catch( IBIZException e ) {
            throw new IBIZException();
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
    
    private int resultCount(JDBCData data,String tableName) throws Exception {
    	ResultSet resultSet = null;
    	int returnCount = 0;
        try {
    		String sql = "SELECT COUNT(*) AS RECORD_COUNT FROM " + tableName;    		
    		resultSet = data.doSQL(sql);
    		resultSet.next();
    		
    		returnCount = resultSet.getInt("RECORD_COUNT");
    	} catch(Exception e){
            e.printStackTrace();
            returnCount = -1;
    	}finally {
    		// Clean up Statement and ResultSet. This is required of any caller to JDBCData.doSQL().
    		// We clean up the Statement because closing just the ResultSet does not close the Statement
    		// and that can cause open cursors to pile up on Oracle until the Statement objects are garbage
    		// collected. That often happens after the maximum open cursors has been exceeded.
    		if (resultSet != null) {
    			if (resultSet.getStatement() != null) {
    				try { resultSet.getStatement().close(); } catch( Exception e ) { Log.error(e); }
    			}
    			// technically the Statement close above should have closed the ResultSet too
    			// but will do it ourselves just to be sure.
    			try { resultSet.close(); } catch( Exception e ) { Log.error(e); }
    			resultSet = null;
    		}
    	}
    	
    	return returnCount;
    }
    
    private void deleteRecords(JDBCData data,String tableName) throws Exception {
    	StringBuffer sb = new StringBuffer();
    	sb.append("DELETE FROM ")
		.append(tableName);
		String sql = sb.toString();
		data.getDelegate().prepareAndExecStmt(data, sql);
		
    }
    
}

