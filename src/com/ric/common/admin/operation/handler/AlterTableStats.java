package com.ric.common.admin.operation.handler;

import java.sql.SQLException;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.data.MySQLDelegate;
import net.inov.tec.data.OracleDelegate;
import net.inov.tec.data.SQLServer2008Delegate;
import net.inov.tec.data.SQLServerDelegate;

import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;

/*  
 * Generic job to alter stats tables
*/

public class AlterTableStats extends InnovationIBIZHandler{
	/** Creates a new instance of AlterTableStats
	 * @throws Exception never
	 */
	public AlterTableStats() throws Exception {
	}
	
	/** Processes a service request.
	 * @return the current response bean
	 * @throws IBIZException when an error requiring user attention occurs
	 * @throws ServiceHandlerException when a critical error occurs
	 */
	public ModelBean process() throws IBIZException, ServiceHandlerException {
		try {            

			Log.debug("Processing AlterTableStats...");	
			
			JDBCData data = this.getHandlerData().getConnection();
			
			//--------------------------------------------------
			//ClaimStats - starts
			//--------------------------------------------------
			
			if( !columnsExist(data,"ClaimStats","Severity"))
				 addColumns(data, "ClaimStats", "Severity");
						
			if( !columnsExist(data,"ClaimStats","OverriddenSeverityLevel"))
				 addColumns(data, "ClaimStats", "OverriddenSeverityLevel");
			
			if( !columnsExist(data,"ClaimStats","CoverageItemCdDescription"))
				 addColumns(data, "ClaimStats", "CoverageItemCdDescription");
						
			if( !columnsExist(data,"ClaimStats","CoverageCdDescription"))
				 addColumns(data, "ClaimStats", "CoverageCdDescription");
			
			//RMG-903 changes
			if( !columnsExist(data,"ClaimStats","LossTm"))
				addColumns(data, "ClaimStats", "LossTm");
				
			if( !columnsExist(data,"ClaimStats","ReportedBy"))
				addColumns(data, "ClaimStats", "ReportedBy");
				
			if( !columnsExist(data,"ClaimStats","ReportedTm"))
				addColumns(data, "ClaimStats", "ReportedTm");
				
			if( !columnsExist(data,"ClaimStats","ReportedTo"))
				addColumns(data, "ClaimStats", "ReportedTo");
				
			if( !columnsExist(data,"ClaimStats","AssignmentOfBenefits"))
				addColumns(data, "ClaimStats", "AssignmentOfBenefits");
				
			if( !columnsExist(data,"ClaimStats","ForRecordOnlyInd"))
				addColumns(data, "ClaimStats", "ForRecordOnlyInd");
				
			if( !columnsExist(data,"ClaimStats","InSIUInd"))
				addColumns(data, "ClaimStats", "InSIUInd");
				
			if( !columnsExist(data,"ClaimStats","DOIComplaintInd"))
				addColumns(data, "ClaimStats", "DOIComplaintInd");
			////RMG-903 changes end
			
			//--------------------------------------------------
			//ClaimStats - Ends
			//--------------------------------------------------
			
			//--------------------------------------------------
			//ClaimSummaryStats - starts
			//--------------------------------------------------	
			
			if( !columnsExist(data,"ClaimSummaryStats","Severity"))
				 addColumns(data, "ClaimSummaryStats", "Severity");
						
			if( !columnsExist(data,"ClaimSummaryStats","OverriddenSeverityLevel"))
				 addColumns(data, "ClaimSummaryStats", "OverriddenSeverityLevel");
			
			if( !columnsExist(data,"ClaimSummaryStats","CoverageItemCdDescription"))
				 addColumns(data, "ClaimSummaryStats", "CoverageItemCdDescription");
						
			if( !columnsExist(data,"ClaimSummaryStats","CoverageCdDescription"))
				 addColumns(data, "ClaimSummaryStats", "CoverageCdDescription");
			
			//RMG-905 changes
			if( !columnsExist(data,"ClaimSummaryStats","LossTm"))
				addColumns(data, "ClaimSummaryStats", "LossTm");
				
			if( !columnsExist(data,"ClaimSummaryStats","ReportedBy"))
				addColumns(data, "ClaimSummaryStats", "ReportedBy");
				
			if( !columnsExist(data,"ClaimSummaryStats","ReportedTm"))
				addColumns(data, "ClaimSummaryStats", "ReportedTm");
				
			if( !columnsExist(data,"ClaimSummaryStats","ReportedTo"))
				addColumns(data, "ClaimSummaryStats", "ReportedTo");
				
			if( !columnsExist(data,"ClaimSummaryStats","AssignmentOfBenefits"))
				addColumns(data, "ClaimSummaryStats", "AssignmentOfBenefits");
				
			if( !columnsExist(data,"ClaimSummaryStats","ForRecordOnlyInd"))
				addColumns(data, "ClaimSummaryStats", "ForRecordOnlyInd");
				
			if( !columnsExist(data,"ClaimSummaryStats","InSIUInd"))
				addColumns(data, "ClaimSummaryStats", "InSIUInd");
				
			if( !columnsExist(data,"ClaimSummaryStats","DOIComplaintInd"))
				addColumns(data, "ClaimSummaryStats", "DOIComplaintInd");
			//RMG-905 changes end
			
			//--------------------------------------------------
			//ClaimSummaryStats - Ends
			//--------------------------------------------------
			
			//--------------------------------------------------
			//GLStats - starts
			//--------------------------------------------------	
			
			if( !columnsExist(data,"GLStats","ASLModified"))
				 addColumns(data, "GLStats", "ASLModified");
			
			//--------------------------------------------------
			//PolicyStats - starts
			//--------------------------------------------------
			
			if( !columnsExist(data,"PolicyStats","CoverageItemCdDescription"))
				 addColumns(data, "PolicyStats", "CoverageItemCdDescription");
						
			if( !columnsExist(data,"PolicyStats","CoverageCdDescription"))
				 addColumns(data, "PolicyStats", "CoverageCdDescription");
			
			if( !columnsExist(data,"PolicyStats","WindHailDedLimit"))
				 addColumns(data, "PolicyStats", "WindHailDedLimit");
			//RMG-895 changes DF PolicyStat fields
			if( !columnsExist(data,"PolicyStats","DwProgramTypeCd"))
				addColumns(data, "PolicyStats", "DwProgramTypeCd");
				
			if( !columnsExist(data,"PolicyStats","SurveyReceivedDt"))
				addColumns(data, "PolicyStats", "SurveyReceivedDt");
				
			if( !columnsExist(data,"PolicyStats","OriginalInceptionDt"))
				addColumns(data, "PolicyStats", "OriginalInceptionDt");
				
			if( !columnsExist(data,"PolicyStats","Movedin3years"))
				addColumns(data, "PolicyStats", "Movedin3years");
		
			if( !columnsExist(data,"PolicyStats","RiskDescription"))
				addColumns(data, "PolicyStats", "RiskDescription");
				
			if( !columnsExist(data,"PolicyStats","InsuranceScore"))
				addColumns(data, "PolicyStats", "InsuranceScore");
				
			if( !columnsExist(data,"PolicyStats","OverriddenInsuranceScore"))
				addColumns(data, "PolicyStats", "OverriddenInsuranceScore");
				
			if( !columnsExist(data,"PolicyStats","FACRiskNumber"))		
				addColumns(data, "PolicyStats", "FACRiskNumber");
				
			if( !columnsExist(data,"PolicyStats","LLPremiumGroup"))
				addColumns(data, "PolicyStats", "LLPremiumGroup");
				
			if( !columnsExist(data,"PolicyStats","PremiumGroup"))
				addColumns(data, "PolicyStats", "PremiumGroup");
				
			if( !columnsExist(data,"PolicyStats","MilesToFireDept"))
				addColumns(data, "PolicyStats", "MilesToFireDept");
				
			if( !columnsExist(data,"PolicyStats","FeetToFireHydrant"))
				addColumns(data, "PolicyStats", "FeetToFireHydrant");
				
			if( !columnsExist(data,"PolicyStats","NumFamilies"))
				addColumns(data, "PolicyStats", "NumFamilies");
			
			if( !columnsExist(data,"PolicyStats","NumStories"))
				addColumns(data, "PolicyStats", "NumStories");
				
			if( !columnsExist(data,"PolicyStats","DwellingStyle"))
				addColumns(data, "PolicyStats", "DwellingStyle");
				
			if( !columnsExist(data,"PolicyStats","ArchitecturalStyle"))
				addColumns(data, "PolicyStats", "ArchitecturalStyle");
				
			if( !columnsExist(data,"PolicyStats","PrimaryExterior"))
				addColumns(data, "PolicyStats", "PrimaryExterior");
				
			if( !columnsExist(data,"PolicyStats","ConstructionQuality"))
				addColumns(data, "PolicyStats", "ConstructionQuality");
				
			if( !columnsExist(data,"PolicyStats","ElectricalType"))
				addColumns(data, "PolicyStats", "ElectricalType");
				
			if( !columnsExist(data,"PolicyStats","ElectricalService"))
				addColumns(data, "PolicyStats", "ElectricalService");
				
			if( !columnsExist(data,"PolicyStats","RoofCd"))
				addColumns(data, "PolicyStats", "RoofCd");
				
			if( !columnsExist(data,"PolicyStats","Heating"))
				addColumns(data, "PolicyStats", "Heating");
				
			if( !columnsExist(data,"PolicyStats","HeatExplanation"))
				addColumns(data, "PolicyStats", "HeatExplanation");
				
			if( !columnsExist(data,"PolicyStats","SecondaryHeat"))
				addColumns(data, "PolicyStats", "SecondaryHeat");
				
			if( !columnsExist(data,"PolicyStats","SecondaryHeatExplanation"))
				addColumns(data, "PolicyStats", "SecondaryHeatExplanation");
				
			if( !columnsExist(data,"PolicyStats","ReplacementCost"))
				addColumns(data, "PolicyStats", "ReplacementCost");
				
			if( !columnsExist(data,"PolicyStats","AllPerilDed"))
				addColumns(data, "PolicyStats", "AllPerilDed");
				
			if( !columnsExist(data,"PolicyStats","OtherDeductibleType"))
				addColumns(data, "PolicyStats", "OtherDeductibleType");
				
			if( !columnsExist(data,"PolicyStats","WindHailDed"))
				addColumns(data, "PolicyStats", "WindHailDed");
				
			if( !columnsExist(data,"PolicyStats","NumDomesticEmployees"))
				addColumns(data, "PolicyStats", "NumDomesticEmployees");
				
			if( !columnsExist(data,"PolicyStats","ElectricalEquip"))
				addColumns(data, "PolicyStats", "ElectricalEquip");
				
			if( !columnsExist(data,"PolicyStats","HeatProducingEquip"))
				addColumns(data, "PolicyStats", "HeatProducingEquip");
				
			if( !columnsExist(data,"PolicyStats","Construction"))
				addColumns(data, "PolicyStats", "Construction");
				
			if( !columnsExist(data,"PolicyStats","Vacancy"))
				addColumns(data, "PolicyStats", "Vacancy");
				
			if( !columnsExist(data,"PolicyStats","MaintAndHousekeeping"))
				addColumns(data, "PolicyStats", "MaintAndHousekeeping");
				
			if( !columnsExist(data,"PolicyStats","StudentHousing"))
				addColumns(data, "PolicyStats", "StudentHousing");
				
			if( !columnsExist(data,"PolicyStats","UnderConstructionOrRenovation"))
				addColumns(data, "PolicyStats", "UnderConstructionOrRenovation");
				
			if( !columnsExist(data,"PolicyStats","ManualExperienceGroup"))
				addColumns(data, "PolicyStats", "ManualExperienceGroup");
				
			if( !columnsExist(data,"PolicyStats","SprinklerSystem"))
				addColumns(data, "PolicyStats", "SprinklerSystem");
				
			if( !columnsExist(data,"PolicyStats","SmokeDetectorCd"))
				addColumns(data, "PolicyStats", "SmokeDetectorCd");
				
			if( !columnsExist(data,"PolicyStats","CentralAlarms"))
				addColumns(data, "PolicyStats", "CentralAlarms");
				
			if( !columnsExist(data,"PolicyStats","SolidFuelHeatType"))
				addColumns(data, "PolicyStats", "SolidFuelHeatType");
			
			if( !columnsExist(data,"PolicyStats","SwimPoolSurcharge"))
				addColumns(data, "PolicyStats", "SwimPoolSurcharge");
				
			if( !columnsExist(data,"PolicyStats","WholeHouseGenDiscount"))
				addColumns(data, "PolicyStats", "WholeHouseGenDiscount");
	
			if( !columnsExist(data,"PolicyStats","SuperiorConditions"))
				addColumns(data, "PolicyStats", "SuperiorConditions");
				
			if( !columnsExist(data,"PolicyStats","Exposure"))
				addColumns(data, "PolicyStats", "Exposure");
			//RMG-895 end changes
			//--------------------------------------------------
			//PolicyStats - Ends
			//--------------------------------------------------
			
			//--------------------------------------------------
			//PolicySummaryStats - starts
			//--------------------------------------------------
			
			if( !columnsExist(data,"PolicySummaryStats","CoverageItemCdDescription"))
				 addColumns(data, "PolicySummaryStats", "CoverageItemCdDescription");
						
			if( !columnsExist(data,"PolicySummaryStats","CoverageCdDescription"))
				 addColumns(data, "PolicySummaryStats", "CoverageCdDescription");
			
			if( !columnsExist(data,"PolicySummaryStats","WindHailDedLimit"))
				 addColumns(data, "PolicySummaryStats", "WindHailDedLimit");
			
			//RMG-895 changes DF PolicyStat fields
			if( !columnsExist(data,"PolicySummaryStats","DwProgramTypeCd"))
				addColumns(data, "PolicySummaryStats", "DwProgramTypeCd");
				
			if( !columnsExist(data,"PolicySummaryStats","SurveyReceivedDt"))
				addColumns(data, "PolicySummaryStats", "SurveyReceivedDt");
				
			if( !columnsExist(data,"PolicySummaryStats","OriginalInceptionDt"))
				addColumns(data, "PolicySummaryStats", "OriginalInceptionDt");
				
			if( !columnsExist(data,"PolicySummaryStats","Movedin3years"))
				addColumns(data, "PolicySummaryStats", "Movedin3years");
			
			if( !columnsExist(data,"PolicySummaryStats","RiskDescription"))
				addColumns(data, "PolicySummaryStats", "RiskDescription");
				
			if( !columnsExist(data,"PolicySummaryStats","InsuranceScore"))
				addColumns(data, "PolicySummaryStats", "InsuranceScore");
				
			if( !columnsExist(data,"PolicySummaryStats","OverriddenInsuranceScore"))
				addColumns(data, "PolicySummaryStats", "OverriddenInsuranceScore");

			if( !columnsExist(data,"PolicySummaryStats","FACRiskNumber"))		
				addColumns(data, "PolicySummaryStats", "FACRiskNumber");
				
			if( !columnsExist(data,"PolicySummaryStats","LLPremiumGroup"))
				addColumns(data, "PolicySummaryStats", "LLPremiumGroup");
				
			if( !columnsExist(data,"PolicySummaryStats","PremiumGroup"))
				addColumns(data, "PolicySummaryStats", "PremiumGroup");
				
			if( !columnsExist(data,"PolicySummaryStats","MilesToFireDept"))
				addColumns(data, "PolicySummaryStats", "MilesToFireDept");
				
			if( !columnsExist(data,"PolicySummaryStats","FeetToFireHydrant"))
				addColumns(data, "PolicySummaryStats", "FeetToFireHydrant");
				
			if( !columnsExist(data,"PolicySummaryStats","NumFamilies"))
				addColumns(data, "PolicySummaryStats", "NumFamilies");
				
			if( !columnsExist(data,"PolicySummaryStats","NumStories"))
				addColumns(data, "PolicySummaryStats", "NumStories");
				
			if( !columnsExist(data,"PolicySummaryStats","DwellingStyle"))
				addColumns(data, "PolicySummaryStats", "DwellingStyle");
				
			if( !columnsExist(data,"PolicySummaryStats","ArchitecturalStyle"))
				addColumns(data, "PolicySummaryStats", "ArchitecturalStyle");
				
			if( !columnsExist(data,"PolicySummaryStats","PrimaryExterior"))
				addColumns(data, "PolicySummaryStats", "PrimaryExterior");
				
			if( !columnsExist(data,"PolicySummaryStats","ConstructionQuality"))
				addColumns(data, "PolicySummaryStats", "ConstructionQuality");
				
			if( !columnsExist(data,"PolicySummaryStats","ElectricalType"))
				addColumns(data, "PolicySummaryStats", "ElectricalType");
				
			if( !columnsExist(data,"PolicySummaryStats","ElectricalService"))
				addColumns(data, "PolicySummaryStats", "ElectricalService");
				
			if( !columnsExist(data,"PolicySummaryStats","RoofCd"))
				addColumns(data, "PolicySummaryStats", "RoofCd");
				
			if( !columnsExist(data,"PolicySummaryStats","Heating"))
				addColumns(data, "PolicySummaryStats", "Heating");
				
			if( !columnsExist(data,"PolicySummaryStats","HeatExplanation"))
				addColumns(data, "PolicySummaryStats", "HeatExplanation");
				
			if( !columnsExist(data,"PolicySummaryStats","SecondaryHeat"))
				addColumns(data, "PolicySummaryStats", "SecondaryHeat");
				
			if( !columnsExist(data,"PolicySummaryStats","SecondaryHeatExplanation"))
				addColumns(data, "PolicySummaryStats", "SecondaryHeatExplanation");
				
			if( !columnsExist(data,"PolicySummaryStats","ReplacementCost"))
				addColumns(data, "PolicySummaryStats", "ReplacementCost");
				
			if( !columnsExist(data,"PolicySummaryStats","AllPerilDed"))
				addColumns(data, "PolicySummaryStats", "AllPerilDed");
				
			if( !columnsExist(data,"PolicySummaryStats","OtherDeductibleType"))
				addColumns(data, "PolicySummaryStats", "OtherDeductibleType");
				
			if( !columnsExist(data,"PolicySummaryStats","WindHailDed"))
				addColumns(data, "PolicySummaryStats", "WindHailDed");
				
			if( !columnsExist(data,"PolicySummaryStats","NumDomesticEmployees"))
				addColumns(data, "PolicySummaryStats", "NumDomesticEmployees");
				
			if( !columnsExist(data,"PolicySummaryStats","ElectricalEquip"))
				addColumns(data, "PolicySummaryStats", "ElectricalEquip");
				
			if( !columnsExist(data,"PolicySummaryStats","HeatProducingEquip"))
				addColumns(data, "PolicySummaryStats", "HeatProducingEquip");
				
			if( !columnsExist(data,"PolicySummaryStats","Construction"))
				addColumns(data, "PolicySummaryStats", "Construction");
				
			if( !columnsExist(data,"PolicySummaryStats","Vacancy"))
				addColumns(data, "PolicySummaryStats", "Vacancy");
				
			if( !columnsExist(data,"PolicySummaryStats","MaintAndHousekeeping"))
				addColumns(data, "PolicySummaryStats", "MaintAndHousekeeping");
				
			if( !columnsExist(data,"PolicySummaryStats","StudentHousing"))
				addColumns(data, "PolicySummaryStats", "StudentHousing");
				
			if( !columnsExist(data,"PolicySummaryStats","UnderConstructionOrRenovation"))
				addColumns(data, "PolicySummaryStats", "UnderConstructionOrRenovation");
				
			if( !columnsExist(data,"PolicySummaryStats","ManualExperienceGroup"))
				addColumns(data, "PolicySummaryStats", "ManualExperienceGroup");
				
			if( !columnsExist(data,"PolicySummaryStats","SprinklerSystem"))
				addColumns(data, "PolicySummaryStats", "SprinklerSystem");
				
			if( !columnsExist(data,"PolicySummaryStats","SmokeDetectorCd"))
				addColumns(data, "PolicySummaryStats", "SmokeDetectorCd");
				
			if( !columnsExist(data,"PolicySummaryStats","CentralAlarms"))
				addColumns(data, "PolicySummaryStats", "CentralAlarms");
				
			if( !columnsExist(data,"PolicySummaryStats","SolidFuelHeatType"))
				addColumns(data, "PolicySummaryStats", "SolidFuelHeatType");
				
			if( !columnsExist(data,"PolicySummaryStats","SwimPoolSurcharge"))
				addColumns(data, "PolicySummaryStats", "SwimPoolSurcharge");
				
			if( !columnsExist(data,"PolicySummaryStats","WholeHouseGenDiscount"))
				addColumns(data, "PolicySummaryStats", "WholeHouseGenDiscount");
				
			if( !columnsExist(data,"PolicySummaryStats","SuperiorConditions"))
				addColumns(data, "PolicySummaryStats", "SuperiorConditions");
				
			if( !columnsExist(data,"PolicySummaryStats","Exposure"))
				addColumns(data, "PolicySummaryStats", "Exposure");
			//RMG-895 end changes
			
			//--------------------------------------------------
			//PolicySummaryStats - Ends
			//--------------------------------------------------
			
			//--------------------------------------------------
			//AccountStats - Starts
			//--------------------------------------------------
			
			if( !columnsExist(data,"AccountStats","ARReceiptTypeCd"))
				addColumns(data, "AccountStats", "ARReceiptTypeCd");
			
			//--------------------------------------------------
			//AccountStats - Ends
			//--------------------------------------------------
			
			//--------------------------------------------------
			//AccountSummaryStats - Starts
			//--------------------------------------------------
			
			if( !columnsExist(data,"AccountSummaryStats","ARReceiptTypeCd"))
				addColumns(data, "AccountSummaryStats", "ARReceiptTypeCd");
			
			//--------------------------------------------------
			//AccountSummaryStats - Ends
			//--------------------------------------------------
			
			
			data.commit();
			
			return null;
		} catch (Exception e){
			throw new ServiceHandlerException(e);			
		}
	}
	
	/**
	 * Add columns to table 
	 * @param data JDBCData The data connection
	 * @param tableName String The table name

	 * @throws SQLException
	 */
	private void addColumns(JDBCData data, String tableName, String columnName) throws SQLException{
		
		String sql = null;
		if(data.getDelegate() instanceof MySQLDelegate){
			sql = "ALTER TABLE " + data.getDelegate().tableName(data, tableName) + " " +
				"ADD COLUMN " + columnName + " VARCHAR(255) NULL";				
		}
		else if(data.getDelegate() instanceof SQLServerDelegate || data.getDelegate() instanceof SQLServer2008Delegate){
			sql = "ALTER TABLE " + data.getDelegate().tableName(data, tableName) + " " +
				"ADD " + columnName + " VARCHAR(255) NULL";						
		}
		else if(data.getDelegate() instanceof OracleDelegate) {  
			sql = "ALTER TABLE " + data.getDelegate().tableName(data, tableName) + " " +
				"ADD (" + columnName + " VARCHAR2(255) NULL)";				
		}
		else{
			throw new RuntimeException("Unknown database vendor");
		}		
	
		data.getDelegate().prepareAndExecStmt(data, sql);
		
	}
	
	/**
	 * Add date columns to the table 
	 * @param data JDBCData The data connection
	 * @param tableName String The table name
	 * @throws SQLException
	 */
	public void addDateColumns(JDBCData data, String tableName,String columnName) throws SQLException{
		
		String sql = null;
		if(data.getDelegate() instanceof MySQLDelegate){
			sql = "ALTER TABLE " + data.getDelegate().tableName(data, tableName) + " " +
			"ADD COLUMN " + columnName + " Date NULL";					
		}
		else if(data.getDelegate() instanceof SQLServerDelegate ){
			sql = "ALTER TABLE " + data.getDelegate().tableName(data, tableName) + " " +
				"ADD " + columnName + " DATETIME NULL";						
		}
		else if(data.getDelegate() instanceof SQLServer2008Delegate){
			sql = "ALTER TABLE " + data.getDelegate().tableName(data, tableName) + " " +
			"ADD " + columnName + " DATETIME2 NULL";	
		}
		else if(data.getDelegate() instanceof OracleDelegate) {  
			sql = "ALTER TABLE " + data.getDelegate().tableName(data, tableName) + " " +
				"ADD (" + columnName + " DATE NULL)";				
		}
		else{
			throw new RuntimeException("Unknown database vendor");
		}
	
		data.getDelegate().prepareAndExecStmt(data, sql);
	}
	
	/** Determine if the columns exist first
	 * @param data JDBCData The data connection
	 * @param tableName String The table to validate if the columns exist
	 * @return true/false boolean indicator of existence 
	 * @throws Exception when an error occurs
	 */
	public boolean columnsExist(JDBCData data, String tableName, String columnName) {
		
		String sql = null;
		sql = "SELECT " + columnName + " FROM " + data.getDelegate().tableName(data, tableName);
		try {
			data.getDelegate().prepareAndExecStmt(data, sql.toString());
			return true;
		} catch (SQLException sqle) {
			return false;
		}		
	}

}
