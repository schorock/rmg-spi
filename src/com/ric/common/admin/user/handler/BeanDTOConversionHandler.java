package com.ric.common.admin.user.handler;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.iscs.common.business.provider.Provider;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.insurance.product.ProductSetup;
import com.iscs.uw.app.Application;
import com.iscs.uw.policy.Policy;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;

public class BeanDTOConversionHandler extends InnovationIBIZHandler {

	ModelBean rq = null;
	ModelBean rs = null;
	JDBCData data = null;
	ModelBean ap = null;

	String[] FIELD_FILTER = new String[] { "CustomerRef", "LockTaskId", "SystemId", "Commission", "CommissionAmt",
			"CommissionPct", "ContributionPct", "CoinsurancePct", "TransactionCommissionAmt", "QuoteNumber",
			"QuoteNumberLookup", "FullTermAmt", "WrittenPremiumAmt", "FinalPremiumAmt", "WrittenFeeAmt",
			"WrittenCommissionFeeAmt", "EarnedAmt", "ProviderRef", "ApplicationNumber", "RatingValue",
			"VerificationHash", "Latitude", "Longitude", "DPV", "DPVDesc", "DPVNotes", "DPVNotesDesc",
			"GrossVehicleWeight", "EngineType", "EngineHorsePower", "EngineCylinders", "EngineSize", "PerformanceCd",
			"VehBodyTypeCd", "DaytimeRunningLights", "COMPRatingValue", "COLLRatingValue" };
	String[] FIELD_FILTER_WITH_VIN = new String[] { "Manufacturer", "Model", "ModelYr", "RestraintCd", "AntiBrakingSystemCd", "AntiTheftCd" };
	String[] BEAN_FILTER = new String[] { "DTOSteps", "DTOStep", "DTORiskGroup", "DTOCommissionArea",
			"ElectronicPaymentSource", "DTOForm", "DTOApplicationInfo" };

	/**
	 * 
	 */
	public BeanDTOConversionHandler() {
		// TODO Auto-generated constructor stub
	}

	public ModelBean process() throws IBIZException, ServiceHandlerException {
		Log.debug("Processing BeanDTOConversionHandler...");

		try {
			rq = this.getHandlerData().getRequest();
			rs = this.getHandlerData().getResponse();
			data = this.getConnection();
			ap = rq.getBean("RequestParams").getBean("AdditionalParams");

			ModelBean beanNameParam = ap.findBeanByFieldValue("Param", "Name", "BeanName");
			ModelBean beanIdentifierParam = ap.findBeanByFieldValue("Param", "Name", "BeanIdentifier");
			ModelBean beanNewIdentifierParam = ap.findBeanByFieldValue("Param", "Name", "BeanNewIdentifier");

			String beanName = beanNameParam.gets("Value");
			String beanIdentifier = beanIdentifierParam.gets("Value");
			String beanNewIdentifier = beanNewIdentifierParam.gets("Value");
			boolean removeUnwantedFields = false;
			boolean populateProviderNumber = false;
			ModelBean sourceBean = null;
			ModelBean targetBean = null;

			if (beanName.equalsIgnoreCase("Application")) {
				beanIdentifier = beanIdentifier.replaceAll("-", "");
				ModelBean[] appSearchArray = Application.searchByNumber(data, beanIdentifier, true, true, true, true,
						false, false);
				if (appSearchArray != null && appSearchArray.length > 0) {
					sourceBean = appSearchArray[0];
				}
				removeUnwantedFields = true;
				populateProviderNumber = true;
				targetBean = new ModelBean("DTO" + beanName);
			}

			ModelBean errors = rs.getBean("ResponseParams").getBean("Errors");
			if (errors == null) {
				errors = new ModelBean("Errors");
				rs.getBean("ResponseParams").addValue(errors);
			}

			if (sourceBean != null) {
				try {
					targetBean = convertBeanToDTO(sourceBean, targetBean, beanNewIdentifier, removeUnwantedFields,
							populateProviderNumber);
					String beanStr = targetBean.toPrettyString();
					// Remove ids that are being referenced (can't remove the
					// IDs using ModelBean object)
					List<String> allIds = new ArrayList<String>();
					Matcher m = Pattern.compile(" id=([\"'])(?:(?=(\\\\?))\\2.)*?\\1").matcher(beanStr);
					while (m.find()) {
						allIds.add(m.group());
					}

					for (String id : allIds) {
						String idValue = id.substring(" id=".length());
						if (StringUtils.countMatches(beanStr, idValue) < 2) {
							// Only 1 entry with that id value (no references)
							beanStr = beanStr.replaceFirst(id, "");
						}
					}

					rs.setValue("DTO", beanStr);

					ModelBean basicPolicy = targetBean.getBean("DTOBasicPolicy");
					if (basicPolicy != null) {
						String productVersionIdRef = basicPolicy.gets("ProductVersionIdRef");
						ModelBean productSetupBean = ProductSetup.getProductSetup(productVersionIdRef);
						ModelBean productList = productSetupBean.getBean("ProductList");
						ModelBean codeRefs = productList.getBean("coderefs");
						String codeRefsString = codeRefs.toPrettyString();
						// clear all ids for readability
						codeRefsString = codeRefsString.replaceAll(" id=([\"'])(?:(?=(\\\\?))\\2.)*?\\1", "");
						// replace all velocity inputs
						codeRefsString = codeRefsString.replaceAll("[$#]", "");
						rs.setValue("CodeRefs", codeRefsString);
					}
				} catch (Exception ex) {
					ModelBean error = new ModelBean(getHandlerData().getModel(), "Error");
					error.setValue("Msg", ex.getLocalizedMessage());
					error.setValue("Name", "Data Error");
					error.setValue("Type", ErrorTools.FIELD_CONSTRAINT_ERROR);
					error.setValue("Severity", ErrorTools.SEVERITY_INFO);
					errors.addValue(error);
				}
			} else {
				String errorMsg = null;
				if (sourceBean == null) {
					errorMsg = beanName + "# '" + beanIdentifier + "' does not exist in database.";
				}
				ModelBean error = new ModelBean(getHandlerData().getModel(), "Error");
				error.setValue("Msg", errorMsg);
				error.setValue("Name", "Data Error");
				error.setValue("Type", ErrorTools.FIELD_CONSTRAINT_ERROR);
				error.setValue("Severity", ErrorTools.SEVERITY_INFO);
				errors.addValue(error);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return rs;
	}

	/**
	 * Gets the latest policy based on the policy number
	 * 
	 * @param policyNumber
	 *            The policy number to retrieve
	 * @param data
	 *            JDBCData Connection
	 */
	public ModelBean getPolicyFromPolicyNumber(JDBCData data, String policyNumber) {
		try {
			ModelBean[] results = Policy.searchByQuoteOrApplicationNumber(data, policyNumber, true, false);

			if (results.length == 0)
				return null;
			else
				return results[0];
		} catch (Exception e) {
			e.printStackTrace();
			Log.error(e);
			return null;
		}
	}

	public ModelBean convertBeanToDTO(ModelBean sourceBean, ModelBean targetBean, String policyNumber,
			boolean removeUnwantedFields, boolean populateProviderNumber) throws Exception {
		BeanTools.copyAllValuesToDTO(targetBean.getModelSpec(), sourceBean, targetBean);
		if (populateProviderNumber) {
			int providerRef = targetBean.getBean("DTOBasicPolicy").getValueInt("ProviderRef");
			ModelBean provider = Provider.getProviderBySystemId(data, providerRef);
			targetBean.getBean("DTOBasicPolicy").setValue("ProviderNumber", provider.gets("ProviderNumber"));
		}

		if (removeUnwantedFields) {
			targetBean = removeUnwantedFields(targetBean.getModelSpec(), targetBean);
		}
		
		// Change Transaction SourceCd
		ModelBean transactionInfo = targetBean.getBean("DTOTransactionInfo");
		if (transactionInfo != null) {
			transactionInfo.setValue("SourceCd", "PLRater");
		}
		return targetBean;
	}

	public ModelBean removeUnwantedFields(ModelSpecification ms, ModelBean rawDTOBean) throws Exception {
		if (rawDTOBean.getBeans() != null && rawDTOBean.getBeans().length > 0) {
			for (ModelBean childBean : rawDTOBean.getBeans()) {
				boolean beanExists = true;
				for (String beanName : BEAN_FILTER) {
					if (childBean.getBeanName().equalsIgnoreCase(beanName)) {
						rawDTOBean.deleteBeanById(childBean.getBeanName(), childBean.getId());
						beanExists = false;
					}
				}
				if (beanExists) {
					rawDTOBean.deleteBeanById(childBean.getBeanName(), childBean.getId());
					ModelBean newchildBean = removeUnwantedFields(ms, childBean);
					rawDTOBean.addValue(newchildBean);
				}
			}
		}
		for (String fieldName : FIELD_FILTER) {
			if (rawDTOBean.hasBeanField(fieldName)) {
				rawDTOBean.setValue(fieldName, "");
			}
		}

		// filter vehicle bean fields
		if (rawDTOBean.getBeanName().equals("DTOVehicle")
				&& !StringTools.isBlank(rawDTOBean.getValueString("VehIdentificationNumber"))) {
			for (String fieldName : FIELD_FILTER_WITH_VIN) {
				if (rawDTOBean.hasBeanField(fieldName)) {
					rawDTOBean.setValue(fieldName, "");
				}
			}
		}
		
		return rawDTOBean;
	}
}
