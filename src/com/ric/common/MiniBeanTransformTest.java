package com.ric.common;

import static org.junit.Assert.*;

import java.util.ArrayList;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanField;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.beans.ModelSpecification.ModelClass;
import net.inov.tec.beans.mini.MiniBeanTransform;
import net.inov.tec.data.JDBCData;
import net.inov.tec.xml.XmlDoc;

import org.junit.BeforeClass;
import org.junit.Test;

import com.iscs.common.mda.Store;
import com.iscs.common.tech.log.Log;

/** Junit test for testing all registered minibean transforms to help ensure they don't do something that will cause errors at runtime
 * 
 * @author mmeier
 *
 */
public class MiniBeanTransformTest {

    /** Setup the test
     */ 
    @BeforeClass
    public static void preTestInit() throws Exception {
		Store.initialize(null, true);    	
    }
	
	/** Test the transformation and subsequent unmarshalling to a bean for every possible field in every minibean
	 * defined in the model spec. The purpose is to identify any fields that are getting in to the mini Xml but aren't
	 * actually in the bean model.
	 * @throws Exception
	 */
    @Test
    public void testEveryMiniBean() throws Exception {
    	Log.debug("Running testEveryMiniBean...");
    	
    	StringBuilder results = new StringBuilder("testEveryMiniBean results:");
    	StringBuilder exceptions = new StringBuilder();
    	
    	ModelClass[] containerClasses = ModelSpecification.getSharedModel().getContainers();
    	for (ModelClass containerClass : containerClasses) {
    		String miniBeanTransformClassName = containerClass.getInfo().getAttributeValue("minibean");
    		if (miniBeanTransformClassName != null && miniBeanTransformClassName.length() > 0 ) {
    			MiniBeanTransform xform = (MiniBeanTransform) Class.forName(miniBeanTransformClassName).newInstance();
    			try {
    				testEveryField(containerClass.getName(), xform);
    				results.append("\n")
    				       .append(containerClass.getName())
    				       .append(": OK");
    			} catch (Exception e) {
    				Log.error(e);
    				if (exceptions.length() > 0) {
        				exceptions.append("\n");
    				}
    				exceptions.append(containerClass.getName() + ": " + e.getMessage());
    				results.append("\n")
				       .append(containerClass.getName())
				       .append(": ")
				       .append(e.getMessage());
    			}
    		}
		}
    	
    	Log.debug(results.toString());
    	
    	if (exceptions.length() > 0) {
    		String failureMsg = "Minibean transformation problems found:\n\n" + exceptions.toString();
    		Log.error(failureMsg);
    		fail(failureMsg);
    	}
    }
    
	/** Test the transformation and subsequent unmarshalling to a bean for every possible field in the main bean.
	 * This will pick up any fields that are getting in to the mini Xml but aren't actually in the bean model.
	 * @throws Exception
	 */
	public void testEveryField(String beanName, MiniBeanTransform xform) throws Exception {
		
		// Build a bean with every possible field set to something
		ModelBean bean = generateEverythingBean(beanName, new ArrayList<String>());
		Log.debug(bean.readableDoc());
		
		// Convert it to a minibean XML
		XmlDoc beanXml = bean.marshal(true);
		XmlDoc miniXml = xform.process(new JDBCData(), bean, beanXml);
		Log.debug(miniXml.readableDoc());
		
		// Try to unmarshal it into a real bean (this is where an error will occur if there is one)
		new ModelBean(miniXml.getRootElement().getName(), miniXml);
		
	}

	/** Generates an "everything" bean that has every possible field set to some value. It is recursive.
	 * @param beanName
	 * @param beansEncountered ArrayList of bean names already encountered. Used to prevent recursive loops
	 * @return ModelBean
	 * @throws Exception
	 */
	private ModelBean generateEverythingBean(String beanName, ArrayList<String> beansEncountered) throws Exception {
		
		// Build a bean with every possible field set to something
		
		ModelBean bean = new ModelBean(beanName);
		beansEncountered.add(beanName);
		
		for (int i = 0; i < bean.countFields(); i++) {
			ModelBeanField field = bean.getBeanField(i);
			
			// for simple fields, set the value
			if (field.isDateType()) {
				field.setValue("20100101");
			} else if (field.isNumberType()) {
				field.setValue("1");
			} else if (field.isStringType()) {
				field.setValue("s");
			}
			// for complex fields, build 1 recursively
			else if (field.isComplexType()) {
				if (beansEncountered.contains(field.getName())) {
					// check for a recursive loop and don't go any deeper;
					continue;
				}
				ModelBean subbean = generateEverythingBean(field.getName(), beansEncountered);
				bean.addValue(subbean);
			} else {
				throw new Exception("Unexpected field type for field " + field.getName() + " in " + beanName);
			}
		}
		
		return bean;
	}
}
