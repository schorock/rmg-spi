package com.ric.common;

import java.util.Iterator;
import java.util.List;

import junit.framework.Test;
import net.inov.biz.server.IBIZServer;

import org.jdom.Element;
import org.jdom.xpath.XPath;

import com.iscs.common.tech.biz.IBIZ;
import com.iscs.common.tech.junit.InnovationTestBase;
import com.iscs.common.tech.log.Log;

public class IBIZValidationTest extends InnovationTestBase {

    /** Creates a new instance of IBIZValidationTest
     */
	public IBIZValidationTest() {		
	}
	
    /** Test Suite
     * @throws Exception Thrown When an Error Occurs Testing
     * @return TestSuite
     */    
    public static Test suite() throws Exception {      	
    	return suite(IBIZValidationTest.class);   
    }
    
    /** Main Calling Method
     * @param args String[]
     */ 
    public static void main(String args[]) {
        try {
            junit.textui.TestRunner.run(suite());
        } catch(Exception e){
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    /** Test to see if all service sequences contain the common service call */
    public void testCOServiceSequence() 
    throws Exception {
    	IBIZServer biz = IBIZ.getIBIZServer();
    	
    	@SuppressWarnings("unchecked")
		List<Element> list = XPath.selectNodes(biz.getServices(), "//sequence");
    	Iterator<Element> iter = list.iterator();
    	int cnt = 0;
    	
    	while (iter.hasNext()) {
    		Element elem = (Element) iter.next();
    		Element child = elem.getChild("action");
    		if (child != null) {
    			String serviceName = child.getAttributeValue("name");
    			if (!serviceName.equals("COPreprocess")) {
    				Log.debug("---> Sequence " + elem.getAttributeValue("name") + " does not have COPreprocess");
    				cnt++;
    			}
    		}
    	}    	
    	Log.debug("---> There are " + cnt + " sequences that do not have COPreprocess");
    }
}

