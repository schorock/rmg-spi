/*
 * RICDataReportRenderer.java
 *
 */

package com.ric.common.business.datareport.render;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

import com.iscs.claims.claim.render.ClaimRenderer;
import com.iscs.claims.common.shared.render.ClaimAIRenderer;
import com.iscs.common.business.datareport.render.DataReportRenderer;
import com.iscs.common.business.provider.render.ProviderRenderer;
import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDAUrl;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.tech.form.field.FormFieldRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.table.Row;
import com.iscs.common.utility.table.Table;
import com.iscs.common.utility.table.Tables;
import com.iscs.common.utility.table.excel.ExcelReader;

import net.inov.biz.server.ServiceContext;
import net.inov.mda.MDAOption;
import net.inov.mda.MDATable;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;
/** Rendering class called during the Velocity Transformation stage to help process tilesets with DataReport references.
 *
 * @author  Anusha.N
 */
public class RICDataReportRenderer extends DataReportRenderer {
    
	protected static final String XACT_DATE_FORMAT = "yyyy-MM-dd";

	private static final String TEMPLATE_NAME = "CLClaim::xact-url::template::xact-analysis-mapping.xml";
	private static Hashtable xactAnalysisMappingTablesCache = new Hashtable();
	private Set<String> xactRequestIdentifierSet = new HashSet<String>();

    /** Creates a new instance of DataReportRenderer */
    public RICDataReportRenderer() {
    }
    

	
    /** Get Property Select List
     * @param fieldId Field Identifier
     * @param defaultValue Select List Default Value
     * @param container ModelBean Containing the Risks
     * @param includeDeletedRisks Include Deleted Risks in Select List 
     * @param meta Parameters
     * @return String HTML Select Tag
     * @throws Exception if an Unexpected Error Occurs
     */
    public static String clamiantSelectField(String fieldId, String defaultValue, ModelBean container,String meta)
    throws Exception {
        try {
        	ArrayList<MDAOption> array = new ArrayList<MDAOption>();
        	ModelBean [] involvedParties = ClaimRenderer.getOpenClaimants(container);
        	

            for( ModelBean involvedParty : involvedParties ) {
	        		MDAOption option = new MDAOption();
	        		option.setValue(involvedParty.getId());
	        		option.setLabel(involvedParty.gets("IndexName"));
	        		array.add(option);
            	}
            //Always add select option
        	MDAOption selectOption = new MDAOption();
        	selectOption.setValue("Select...");
        	selectOption.setLabel("Select...");
    		array.add(selectOption);
            MDAOption[] options = array.toArray(new MDAOption[array.size()]);
            return FormFieldRenderer.renderSelect(fieldId, defaultValue, options, "SelectList", meta);
        }
        catch( Exception e ) {
            e.printStackTrace();
            Log.error(e);
            return "";
        }
    }
    
    
	/**
     * Get the list of all the locations details in the application in coderef options format
	 * @param container
	 * @param claimantId
	 * @return String
	 */
	public static String getLocationListOption(ModelBean container,String claimantId) throws Exception {
		ModelBean claimant = container.findBeanById("Claimant", claimantId);
		ModelBean[] location = claimant.findBeansByFieldValue("PropertyDamaged", "StatusCd", "Active");
		StringBuilder locationList = new StringBuilder("<options>");
		locationList.append("<option value=\"\">Select...</option>\n");
		for (int i = 0; i < location.length; i++) {
			ModelBean addr = location[i].findBeanByFieldValue("Addr", "AddrTypeCd", "PropertyDamagedLocation");
			String addrString = null;
			if (addr.gets("Addr2").equals("")) {
				addrString = location[i].gets("PropertyDamagedNumber") + ": " + addr.gets("Addr1") + " " + addr.gets("City") + " " + addr.gets("StateProvCd") + " " + addr.gets("PostalCode");
			} else {
				addrString = location[i].gets("PropertyDamagedNumber") + ": " + addr.gets("Addr1") + " " + addr.gets("Addr2") + " " + addr.gets("City") + " " + addr.gets("StateProvCd") + " " + addr.gets("PostalCode");
			}
			locationList.append("<option value=\"" + addr.gets("id") + "\">" + addrString + "</option>\n");
			
		}
		locationList.append("</options>");
		String str = locationList.toString();
		return str;
	}
	
	
	
	

	
	public ModelBean retrievePropertyDmgAddress(ModelBean claim, String involvedParty, String locationId) throws ModelBeanException{
		ModelBean claimant = claim.getBeanById(involvedParty);
		ModelBean[] location = claimant.findBeansByFieldValue("PropertyDamaged", "StatusCd", "Active");
		ModelBean addr = null;
		for (int i = 0; i < location.length; i++) {
			if(addr == null){
			addr = location[i].findBeanById("Addr", locationId);
			}
		}
		
	    return addr;
	}
	
	public String retrievePropertyDmgLocDesc(ModelBean claim, String involvedParty, String locationId) throws ModelBeanException{
		ModelBean claimant = claim.getBeanById(involvedParty);
		ModelBean[] location = claimant.findBeansByFieldValue("PropertyDamaged", "StatusCd", "Active");
		ModelBean addr = null;
		String vehLocDesc = "";
		for (int i = 0; i < location.length; i++) {
			if(addr == null){
			addr = location[i].findBeanById("Addr", locationId);
			if(addr != null){
				ModelBean propDmgVeh = addr.getParentBean();
				if(propDmgVeh != null){
					if(propDmgVeh.gets("PropertyTypeCd").equals("Vehicle")){
						vehLocDesc = propDmgVeh.gets("VehicleLocation");
					}else if(propDmgVeh.gets("PropertyTypeCd").equals("Property")){
						vehLocDesc = propDmgVeh.gets("LocationDesc");	
					}
					}
				}
			}
		}
	    return vehLocDesc;
	}
	

	
	
	public String replaceSplCharacter(String value){
		//Define all the special characters and replace them in name and stepId.
		String specialChars = "&\"'<>";
		value = StringTools.replaceSpecialCharacters(value, specialChars, "");
		return value;
	}
	
    /* Returns the velocity variable context name
     * @return "DataReportRenderer"
     */
    public String getContextVariableName() {
        return "RICDataReportRenderer";
    }

	
	public String retrieveJobSize(String jobSize) throws Exception{
		
			String repositoryName =  "CLInterfaceXactAnalysisImport::xactlist::jobsize::all";
			MDATable table = (MDATable) Store.getModelObject(repositoryName);
			MDAOption option = table.getOption(jobSize);
			String jobSizeLabel = option.getLabel();
			return jobSizeLabel;
	}
	
	public String truncateLossDesc(String lossDesc) throws Exception{
		if(lossDesc.length() > 35 ) {
			// Truncate to 35 digits because xact accepts 35 character length
			lossDesc = lossDesc.substring(0, 35);
		}
		return lossDesc;
	}
	/**
     * 
     * @param xact Transaction ID
     */
    public void clearXactDataSet() {
    	xactRequestIdentifierSet.clear();
    }
    
    public boolean setXactRequestIdentifierSet(String xactRequestIdentifier) {
    	return xactRequestIdentifierSet.add(xactRequestIdentifier);
	}
    
    public static ModelBean getClaimInsuredLookUpAddr(ModelBean claim)throws Exception {			
		try {
			ModelBean claimPolicyInfo = claim.getBean("ClaimPolicyInfo");
			ModelBean claimPartyInfo = claimPolicyInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "InsuredParty");
			ModelBean insuredLookUpAddr = claimPartyInfo.getBeanByAlias("InsuredLookupAddr");
			return insuredLookUpAddr;
		} catch( Exception e ) {
			e.printStackTrace();
			Log.error(e);
			return null;	
		}
	}
    
    public static ModelBean getClaimInsuredMailingAddr(ModelBean claim)throws Exception {			
		try {
			ModelBean claimPolicyInfo = claim.getBean("ClaimPolicyInfo");
			ModelBean claimPartyInfo = claimPolicyInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "InsuredParty");
			ModelBean insuredLookUpAddr = claimPartyInfo.getBeanByAlias("InsuredMailingAddr");
			return insuredLookUpAddr;
		} catch( Exception e ) {
			e.printStackTrace();
			Log.error(e);
			return null;	
		}
	}
    
    /** Get XACT Claimant Type
	 * @param claim Claim ModelBean
	 * @param claimantIdRef Claimant ID Reference
	 * @return xact ClaimantType
	 * @throws Exception when an unexpected error occurs
	 */
	public static String getXACTClaimantType(ModelBean claim, String claimantIdRef)
	throws Exception {			
		try {
			if( claimantIdRef.equals("") )
				return "";
			
			String xactClaimantType = "";
			ModelBean claimant = claim.getBeanById(claimantIdRef);
			if(claimant.gets("ClaimantTypeCd").equalsIgnoreCase("First Party")){
				xactClaimantType = "Client";
			}
			else{
				xactClaimantType = "Claimant";
			}
			return xactClaimantType;
			
		} catch( Exception e ) {
			e.printStackTrace();
			Log.error(e);
			return "";	
		}
	}
	
	
	public static String getClaimantIndexName(ModelBean claim, String claimantIdRef)
	throws Exception {			
		try {
			if( claimantIdRef.equals("") )
				return "";
			
			ModelBean claimant = claim.getBeanById(claimantIdRef);
			return claimant.gets("IndexName");
			
		} catch( Exception e ) {
			e.printStackTrace();
			Log.error(e);
			return "";	
		}
	}
	
	
	/** Get Claimant Name 
	 * @param claim Claim ModelBean
	 * @param claimantIdRef Claimant ID Reference
	 * @return Claimant Name
	 * @throws Exception when an unexpected error occurs
	 */
	public static ModelBean getClaimantPhone(ModelBean claim, String claimantIdRef, String phoneType)
	throws Exception {			
		try {
			if( claimantIdRef.equals("") )
				return null;
			
			ModelBean claimant = claim.getBeanById(claimantIdRef);
			ModelBean partyInfo = claimant.getBean("PartyInfo");
			ModelBean[] phones = partyInfo.getBeans("PhoneInfo");
			for (ModelBean phone : phones) {
				if (phone.gets("PhoneTypeCd").equalsIgnoreCase(phoneType)){
					return phone;
					 
				}
				else if(phone.gets("PhoneTypeCd").equalsIgnoreCase(phoneType)){
					return phone;
				}
			}
			return null;
			
		} catch( Exception e ) {
			e.printStackTrace();
			Log.error(e);
			return null;	
		}
	}
	
	
	 /** Format phone number
     * @param address String phone number
     * @return String Formatted phone number (nnn) nnn-nnnnxnnn
     */
    public static String formatPhone(String phone)
    throws Exception {
		try { 

			phone = phone.replaceAll("\\(", "");
			phone = phone.replaceAll("\\)", "-");
			phone = phone.replaceAll("-", "");
			phone = ModelSpecification.indexString(phone);
			String value =""; 
			if (phone.length() == 10){
				value = phone.substring(0, 3) + "-" + phone.substring(3, 6)
						+ "-" + phone.substring(6, 10);
			}
			String formattedPhone = value;
			return formattedPhone;
		} catch( Exception e ) {
			e.printStackTrace();
            Log.error(e);
            return "";
        }
  	 }
    
    /** Get Claimant Name 
	 * @param claim Claim ModelBean
	 * @param claimantIdRef Claimant ID Reference
	 * @return Claimant Name
	 * @throws Exception when an unexpected error occurs
	 */
	public static ModelBean getClaimantEmailInfo(ModelBean claim, String claimantIdRef)
	throws Exception {			
		try {
			if( claimantIdRef.equals("") )
				return null;
			
			ModelBean claimant = claim.getBeanById(claimantIdRef);
			ModelBean partyInfo = claimant.getBean("PartyInfo");
			ModelBean email = partyInfo.getBean("EmailInfo");
			return email;
			
		} catch( Exception e ) {
			e.printStackTrace();
			Log.error(e);
			return null;	
		}
	}
    
	
	/**
	 * 
	 * Convert date(strDate) coming in different format(extFormat) to SPI
	 * format.
	 * 
	 * @param strDate
	 * @param extFormat
	 * @return
	 * @throws Exception
	 */
	public static String getXactDateFormat(String strDate) throws Exception {
		 SimpleDateFormat sdfSource = new SimpleDateFormat("MM/dd/yyyy");
         Date date = sdfSource.parse(strDate);
         
       //Parse the string into Date object
         SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd");
         
         String checkDate = sdfDestination.format(date);
       
         return checkDate;
	
	}
	
	/**
	 * Map loss cause to Xact losscause and Xact sublosscause code based on the innovation losscd and sublosscd
	 */	 
	public  MDAOption[] getXactLossType(String productVersionId, String lossCauseCd, String subLossCauseCd) throws ModelBeanException, Exception 
	{
		if(lossCauseCd == null) //don't report
			return null;
		//lossCauseCd = "Accidental Death";
		Row[] rows = null;
		Table table = getXactAnalysisMappingTables(productVersionId).get("loss-cause");
		if(!subLossCauseCd.equals("")){
			rows = table.lookup(new String[]{"LossCauseCd","SubLossCauseCd"}, new String[]{lossCauseCd, subLossCauseCd}, Table.MATCH_TYPE_STRING_CASE_INSENSITIVE);
			if(rows.length == 0)
			{
				rows = table.lookup(new String[]{"LossCauseCd"}, new String[]{lossCauseCd}, Table.MATCH_TYPE_STRING_CASE_INSENSITIVE);
			}
		}else{
			rows = table.lookup(new String[]{"LossCauseCd"}, new String[]{lossCauseCd}, Table.MATCH_TYPE_STRING_CASE_INSENSITIVE);
		}
		
		if(rows.length == 0)
		return null; //mapping not found = don't report
			ArrayList array = new ArrayList();
			MDAOption option = new MDAOption();
	        option.setValue( rows[0].getCell("XactLossCauseCd").getString());
	        option.setLabel("xactLossCause");
	        array.add(option);
	        if(!subLossCauseCd.equals("")){
	        	if(rows[0].getCell("XactSubLossCauseCd").getValue() != null){
		        MDAOption option2 = new MDAOption();
		        option2.setValue( rows[0].getCell("XactSubLossCauseCd").getString());
		        option2.setLabel("xactSubLossCause");
		        array.add(option2);
		        }
	        }
        MDAOption[] options = (MDAOption[]) array.toArray(new MDAOption[array.size()]);
		return options;
	}
	
	
	public String getXactCauseDetails(MDAOption[] options,String causeCd){
	String xactCode ="";
    if (options.length > 0){
    	if(!causeCd.equals("")){
	    	if(causeCd.equalsIgnoreCase("LossCauseCd")){
	    		xactCode = options[0].getValue();
	    	}
	    	else if(causeCd.equalsIgnoreCase("SubLossCauseCd")){
	    		if (options.length >= 1){
	    			xactCode = options[1].getValue();
	    		}
	    		}
    	}
	}
    return xactCode;
}
	
	/**
	 * Returns xact analysis tables from Cache if already present. Otherwise, loads Cache and returns table.
	 * @param productVersionId
	 * @return
	 * @throws Exception
	 */	
	private Tables getXactAnalysisMappingTables(String productVersionId) throws Exception{
		
		Tables xactAnalysisMappingTables = null;
		
		//lookup product version id iso cache
		if( xactAnalysisMappingTablesCache.containsKey("XACT") ) {
			xactAnalysisMappingTables = (Tables) xactAnalysisMappingTablesCache.get("XACT");
		} 
		//if not in cache, load cache
		else {
			//ModelBean productSetup = ProductSetup.getProductSetup(productVersionId);
            
            MDAUrl mdaUrl = (MDAUrl) Store.getModelObject(TEMPLATE_NAME);
    	    ExcelReader excelReader = new ExcelReader();
    	    xactAnalysisMappingTables = excelReader.read(mdaUrl.getURL());
			
    	    xactAnalysisMappingTablesCache.put("XACT", xactAnalysisMappingTables);
    	        	    
		}
		
		return xactAnalysisMappingTables;
	}	
	
	/**
	 * 
	 * Gets current date and time in xact format
	 * format.
	 * @return String
	 * @throws Exception
	 */
	public static String getXactDateTimeFormat()throws Exception {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		//get current date time with Date()
		Date date = new Date();
		String checkDate = dateFormat.format(date);
		return checkDate;
	}
    
	
	/** Get Claimant Name 
	 * @param claim Claim ModelBean
	 * @param claimantIdRef Claimant ID Reference
	 * @return Claimant Name
	 * @throws Exception when an unexpected error occurs
	 */
	public static String getClaimantType(ModelBean claim, String claimantIdRef)
	throws Exception {			
		try {
			if( claimantIdRef.equals("") )
				return "";
			
			ModelBean claimant = claim.getBeanById(claimantIdRef);
			
			return claimant.gets("ClaimantTypeCd");
			
		} catch( Exception e ) {
			e.printStackTrace();
			Log.error(e);
			return "";	
		}
	}
	
	
	public static ModelBean[] retrieveMortgageeInfo(ModelBean claim) throws Exception{ 
		
		ClaimAIRenderer claimAI = new ClaimAIRenderer();
		// Get Active Additional Interests
		ModelBean[] ais = claimAI.getActiveAdditionalInterests(claim);

		// Create an Array of Additional Interest types that exist in the Product
		ArrayList aiArray = new ArrayList();
		for( ModelBean ai : ais ) {
			String interestTypeCd = ai.gets("TypeCd");
				// Check if Type Codes Match
				if( interestTypeCd.equals("First Mortgagee") || interestTypeCd.equals("Second Mortgagee") || interestTypeCd.equals("Third Mortgagee") )
				aiArray.add(ai);
				}						
		return (ModelBean[]) aiArray.toArray(new ModelBean[aiArray.size()]);

		}
	
	public static ModelBean getClaimantInfo(ModelBean claim, String claimantIdRef)throws Exception {			
		try {
			if( claimantIdRef.equals("") )
				return null;
			
			ModelBean claimant = claim.getBeanById(claimantIdRef);
			return claimant;
			
		} catch( Exception e ) {
			e.printStackTrace();
			Log.error(e);
			return null;	
		}
	}
	
	/**
	 * For form attachment rules, determines if the 
	 * last data report was ordered and NCF was used for the rating
	 * If so, then return the level determined through rating, otherwise
	 * return blank
	 * @param container
	 * @return
	 */
	public static String getNCFLevel(ModelBean container) {
		try {
			ModelBean insured = container.getBean("Insured");
			ModelBean insuranceScore = insured.getBean("InsuranceScore");
			String dataReportRequestRef = insuranceScore.gets("SourceIdRef");
			
			if (!dataReportRequestRef.equals("") && container.getBeanById(dataReportRequestRef) != null) {
				return insuranceScore.gets("InsuranceScoreDisplay");
			}	
			
			return "";
		} catch (Exception e) {
			e.printStackTrace();
			Log.debug("Trouble determining if NCF was ordered for this transaction");
			return "";
		}
	}
	
	
	public static ModelBean retrieveExaminerInfo(ModelBean claim)throws Exception {			
		try {
			ModelBean provider = null;
			String examinerRef = claim.gets("ExaminerProviderRef");
			
			if(!examinerRef.equals("")){
				provider = ProviderRenderer.getProviderBySystemId(examinerRef);
				provider = provider.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "ProviderParty");
			}
			return provider;
			
		} catch( Exception e ) {
			e.printStackTrace();
			Log.error(e);
			return null;	
		}
	}
	
	public static ModelBean retrieveInsuredInfo(ModelBean claim)throws Exception {			
		try {
			ModelBean partyinfo = null;
			partyinfo = claim.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "InsuredParty");
			return partyinfo;
			
		} catch( Exception e ) {
			e.printStackTrace();
			Log.error(e);
			return null;	
		}
	}
	
	public static ModelBean retrieveThirdPartyClaimantInfo(ModelBean claim)throws Exception {			
		try {
			ModelBean partyinfo = null;
			ModelBean claimant = claim.findBeanByFieldValue("Claimant", "ClaimantTypeCd", "Third Party");
			partyinfo = claimant.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "ClaimantParty");
			return partyinfo;
			
		} catch( Exception e ) {
			e.printStackTrace();
			Log.error(e);
			return null;	
		}
	}
	
	 
	public ModelBean getDataReportBySystemId( String systemId ) {
        try {
        	JDBCData data = ServiceContext.getServiceContext().getData();
        	
        	if( systemId.isEmpty() || StringRenderer.equal(systemId, "0") )
        		return null;
        	
            ModelBean dataReport = new ModelBean("DataReport");
            
            data.selectModelBean(dataReport, Integer.parseInt(systemId));
            
            return dataReport;
            
        }
        catch( Exception e ) {
            Log.error(e.getMessage(), e);
            e.printStackTrace();
            return null;
        }
    }
}