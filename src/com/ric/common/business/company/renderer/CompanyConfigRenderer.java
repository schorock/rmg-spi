/*
 * CompanyRenderer.java
 *
 */

package com.ric.common.business.company.renderer;

import com.iscs.common.render.DynamicString;
import com.iscs.common.render.Renderer;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.InnovationUtils;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;

/** Company Rendering Tools
 *
 * @author  sangeetham
 */
public class CompanyConfigRenderer implements Renderer {
    
    /** Creates a New Instance of CompanyRenderer */
    public CompanyConfigRenderer() {
    }
      
    /** Context Variable Getter
     * @return ProductRenderer
     */     
    public String getContextVariableName() {
        return "CompanyConfigRenderer";
    }    
    
	public static String getACHProcessURL() {
		String directoryPath = "";

		try {
			directoryPath = getEnvironmentVariable("CompanyACH", "processURL", "");
		}
		catch (Exception e) {
			Log.error(e);
		}

		return directoryPath;
	}

	public static String getACHExceptionURL() {
		String directoryPath = "";

		try {
			directoryPath = getEnvironmentVariable("CompanyACH", "exceptionURL", "");
		}
		catch (Exception e) {
			Log.error(e);
		}

		return directoryPath;
	}

	public static String getACHExceptionBackupFilename() {
		String directoryPath = "";
		
		try {

			directoryPath = getEnvironmentVariable("CompanyACH", "exceptionBackupFilename", "");
		}
		catch (Exception e) {
			Log.error(e);
		}
		return directoryPath;
	}

	public static String getEnvironmentVariable(String section, String name, String defaultValue) throws ModelBeanException, Exception {
		return DynamicString.render(new ModelBean("Company"), InnovationUtils.getEnvironmentVariable(section, name, defaultValue));
	}
}
