package com.ric.common.business.attachment.handler;

import com.innovextechnology.service.Log;
import com.iscs.claims.claim.render.ClaimRenderer;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.workflow.Task;
import com.iscs.workflow.TaskException;
import com.iscs.workflow.render.WorkflowRenderer;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

public class AttachmentClaimsTaskNoAdjuster extends InnovationIBIZHandler {
  //Default Constructor
  public AttachmentClaimsTaskNoAdjuster() {
    super();
  }

  public ModelBean process() {
    try {
      ModelBean rs = this.getHandlerData().getResponse();
      ModelBean rq = this.getHandlerData().getRequest();
      JDBCData data = this.getHandlerData().getConnection();
      ModelBean claim = rs.getBean("Claim");
      StringDate todayDt = new StringDate();
      if (claim != null) {
        boolean hasAdjuster = false;
				for( ModelBean claimant : claim.getBeans("Claimant") ) {
					for( ModelBean assignedProvider : claimant.getBeans("AssignedProvider") ) {
						if( assignedProvider.gets("AssignedProviderTypeCd").contains("Other") ) {
							if( assignedProvider.gets("ProviderTypeCd").equalsIgnoreCase("Adjuster") ) {
							  hasAdjuster = true;
								//ModelBean task = WorkflowRenderer.createTaskOverrideCurrentOwner(data, "ClaimsTask03028", claim, assignedProvider.gets("ProviderRef"), todayDt);
								//Task.insertTask(data, task);
							}
						}
					}
				}
				if( !hasAdjuster ) {
          ModelBean attachmentTemplateId = rq.findBeanByFieldValue("Param", "Name", "Attachment.TemplateId");
          if (attachmentTemplateId != null) {
            ModelBean requestParams = rq.getBean("RequestParams");
            ModelBean userInfo = requestParams.getBean("UserInfo");
            String loginId = userInfo.gets("LoginId");
            String templateName = attachmentTemplateId.gets("Value");
            String attachmentId = requestParams.getBean("CMMParams").gets("IdRef");
            ModelBean attachment = rq.getBeanById("Attachment", attachmentId);
            ModelBean task;
            Log.debug("templateName = " + templateName);
            switch (templateName) {
              case "XTransactionAttachment3017":  //Police Report
                break;
              case "XTransactionAttachment3003":  //Claim Document
                Log.debug("RGUAT-521:  No Adjuster, Claim Document attachment added, adding task ClaimReportReceived");
                task = Task.createTask(data, "ClaimReportReceived", claim, loginId, todayDt, attachment);
                Task.insertTask(data, task);
                break;
              case "XTransactionAttachment3000":  // Adjuster Report
                Log.debug("RGUAT-521:  No Adjuster, Adjuster Report attachment added, adding task Adjuster Report Received");
                task = Task.createTask(data, "AdjusterReportReceived", claim, loginId, todayDt, attachment);
                Task.insertTask(data, task);
                break;
              case "XTransactionAttachment3015":  //Notice of Loss
                Log.debug("RGUAT-521:  No Adjuster, Notice of Loss attachment added, adding task Notice of Loss Received");
                task = Task.createTask(data, "NoticeofLossReceived", claim, loginId, todayDt, attachment);
                Task.insertTask(data, task);
                break;
              case "XTransactionAttachment3010":  //Investigative Report
                Log.debug("RGUAT-521:  No Adjuster, Investigative Report attachment added, adding task Investigative Report Received");
                task = Task.createTask(data, "InvestigativeReportReceived", claim, loginId, todayDt, attachment);
                Task.insertTask(data, task);
                break;
            }
          }
        }
        return rs;
      } else {
        return null;
      }
    } catch (ModelBeanException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } /*catch (TaskException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} */ catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return null;
  }

}
