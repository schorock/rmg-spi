package com.ric.common.business.rule;

import com.iscs.claims.claim.render.ClaimRenderer;
import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.render.DateRenderer;
import com.iscs.common.render.StringRenderer;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

abstract public class RICClaimsRuleProcessor implements RuleProcessor {

	public ModelBean claim;
	public ModelBean oldClaim;
	public ModelBean response;
	public ModelBean request;
	public StringDate todayDt;
	public ModelBean errors;
	public ModelBean user;
	public JDBCData data;
	public Boolean isNewClaim;
	
	@Override
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user,
			JDBCData data) throws RuleException {
		
		try {
			claim = bean;
			todayDt = new StringDate(DateRenderer.getDate());
			request = additionalBeans[1];
			response = additionalBeans[2];
			errors = response.getBean("ResponseParams").getBean("Errors");
			this.user = user;
			this.data = data;
			isNewClaim = claim.gets("ClaimRef").equals("");
			
			if( StringRenderer.greaterThan(claim.gets("TransactionNumber"), "1") ){
				oldClaim = ClaimRenderer.getClaim(data, claim.gets("ClaimRef"));
			}
			
			processRules();
			
		} catch(Exception e) {
			throw new RuleException(e);
		}
		
		return errors;
	}
	
	abstract public void processRules() throws Exception;
}
