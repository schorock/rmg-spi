/*
 * StatRenderer.java
 *
 */


package com.ric.common.business.stat.render;


import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.data.JDBCData;
import com.iscs.common.tech.log.Log;

import com.iscs.common.render.Renderer;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.utility.StringTools;
import net.inov.biz.server.ServiceContext;
import net.inov.tec.date.StringDate;
import com.iscs.common.business.company.Company;



public class RICStatRenderer implements Renderer {
    
    /** Creates a new instance of StatRenderer */
    public RICStatRenderer() {
    }

    /** Context Variable Getter
     * @return StatRenderer
     */     
    public String getContextVariableName() {
        return "RICStatRenderer";
    }
    
    /** Get CarrierCd
     * 
     * @param suspense
     * @return
     * @throws Exception
     */
    public static String getCarrierCd(ModelBean suspense)throws Exception{
    	String carrierCd = null;
    	if(suspense!=null && !suspense.equals("") ){
    		carrierCd = "RIC";
    	}
    	return carrierCd;
    }
  
    
    /**
	 * @param policyRef
	 * @return
	 * @throws Exception
	 * Need to put Subtype information in to the AccountStats table ,pulling the specific Poliy Data to get this info, 
	 * as it's not available in the Account information
	 */
	public static String  getSubTypeForAccountStats(String policyRef) throws Exception{
		
		String subTypeCd = "";
		ModelBean policyMini = null;
		
		if(null != policyRef){
			
			JDBCData data = ServiceContext.getServiceContext().getData();
			policyMini = data.selectMiniBean("PolicyMini", policyRef);
			
			ModelBean basicPolicy =  policyMini.getBeanByAlias("BasicPolicy");
			subTypeCd =  basicPolicy.gets("SubTypeCd");
		}
		return subTypeCd;
	}
	
	 /** getCoverageDescription
     * @param statCov ModelBean
     * @return coverageDescription String
     * @throws Exception
     */
    public static String getCoverageDescription(ModelBean statCov) throws Exception {
		String coverageDescription = null;
		try {
			if ( statCov != null ) {
				coverageDescription = statCov.gets("Description");
			}
		} catch (Exception e) {
			Log.error("Exception occurred when try to get Coverage Description", e);
		}
		return coverageDescription;
	}
    
    /** getCoverageDescription
     * @param statCov ModelBean
     * @return coverageDescription String
     * @throws Exception
     */
    public static String getCoverageItemDescription(ModelBean statCov) throws Exception {
		String coverageDescription = null;
		try {
			//If CoverageItem status is not active use ItemName else use CoverageData Value
			if ( statCov != null && ("active").equalsIgnoreCase(statCov.gets("Status"))) {			
				ModelBean covData = statCov.getBean("CoverageData");
				if ( covData != null ){
					coverageDescription = covData.gets("Value");
				}
				//Check if coverageCd is WL then use WaterCreaftType else use ItemName field		
				if(("").equals(coverageDescription)|| coverageDescription == null){	
					ModelBean coverage = statCov.getParentBean();
					if ( ("WL").equalsIgnoreCase(coverage.gets("CoverageCd")) ){
						coverageDescription = statCov.gets("WatercraftType");
					}else{
						coverageDescription = statCov.gets("ItemName");
					}
				}
			}else if(statCov != null && ("deleted").equalsIgnoreCase(statCov.gets("Status"))){
				coverageDescription = statCov.gets("ItemName");
			}
		} catch (Exception e) {
			Log.error("Exception occurred when try to get Coverage Description", e);
		}
		return coverageDescription;
	}
    
    /** getCoverageDescription
     * @param ClaimPolicyInfo ModelBean\
      * @param FeatureAllocation ModelBean
     * @return claimCoverageDesc String
     * @throws Exception
     */
    public static String getClaimCoverageDescription(ModelBean ClaimPolicyInfo, ModelBean FeatureAllocation) throws Exception {
    	String coverageCd = FeatureAllocation.gets("FeatureCd");
		String claimCoverageDesc = null;
		try {
			if ( coverageCd != null ) {
				ModelBean ClaimPolicyLimit = ClaimPolicyInfo.findBeanByFieldValue("ClaimPolicyLimit", "CoverageCd", coverageCd);
				if(ClaimPolicyLimit != null){
					claimCoverageDesc = ClaimPolicyLimit.gets("Description");
				}
			}
		} catch (Exception e) {
			Log.error("Exception occurred when try to get Coverage Description", e);
		}
		return claimCoverageDesc;
	}
    
    /** getCoverageDescription
     * @param ClaimPolicyInfo ModelBean\
      * @param FeatureAllocation ModelBean
     * @return claimCoverageDesc String
     * @throws Exception
     */
    public static String getClaimCoverageItemDescription(ModelBean ClaimPolicyInfo, ModelBean FeatureAllocation) throws Exception {
    	String coverageCd = FeatureAllocation.gets("FeatureCd");
    	String coverageItem = FeatureAllocation.gets("FeatureSubCd");
    	
		String claimCoverageDesc = null;
		try {
			if ( coverageCd != null ) {
				ModelBean ClaimPolicyLimit = ClaimPolicyInfo.findBeanByFieldValue("ClaimPolicyLimit", "CoverageCd", coverageCd);
				if(ClaimPolicyLimit != null && !"".equals(coverageItem)){
					ModelBean ClaimPolicySubLimit = ClaimPolicyLimit.findBeanByFieldValue("ClaimPolicySubLimit", "CoverageSubCd", coverageItem);
					claimCoverageDesc = ClaimPolicySubLimit.gets("Description");
				}
			}
		} catch (Exception e) {
			Log.error("Exception occurred when try to get Coverage Description", e);
		}
		return claimCoverageDesc;
	}
    
    //RMG-895 Changes for fields added PolicyStats for DF
    /** getStatInformation
     * @param bean ModelBean
     * @return field String
     * @throws Exception
     */
    public static String getStatInformation(ModelBean bean, String field) throws Exception {
		String statField = null;
		
		try {
			if ( bean != null ) {
				statField = bean.getValueString(field);
			}else{
				//leave blank for other stats table logic
			}
		} catch (Exception e) {
			Log.error("Exception occurred when try to get Building field: "+ field, e);
		}
		return statField;
	}
    
    /** getInsuredInformation
     * @param insured ModelBean
     * @return field String
     * @throws Exception
     */
    public static String getInsuredInformation(ModelBean insured, String field) throws Exception {
		String statField = null;
		
		try {
			if ( insured != null ) {
				statField = insured.getValueString(field);
			}else{
				//leave blank for other stats table logic
			}
		} catch (Exception e) {
			Log.error("Exception occurred when try to get Insured field: "+ field, e);
		}
		return statField;
	}

    /** getProgramType
     * @param bean ModelBean
     * @return field String
     * @throws Exception
     */
    public static String getProgramType(ModelBean bean, String field) throws Exception {
		String statField = null;
		
		try {
			if ( bean != null ) {
				statField = bean.getValueString(field);
				if(statField != null){
					if("DP".equalsIgnoreCase(statField))
						statField = "Dwelling Properties";
					else if("LLP".equalsIgnoreCase(statField))
						statField = "Landlord Premises";
					else if("STR".equalsIgnoreCase(statField))
						statField = "Short Term Vacation Rental";
				}
			}else{
				//leave blank for other stats table logic
			}
		} catch (Exception e) {
			Log.error("Exception occurred when try to get Building field: "+ field, e);
		}
		return statField;
	}
    //End Changes for RMG-895
     
    /** setReceiptTypeCd
     * @param account ModelBean
     * @return receiptCd String
     * @throws ModelBeanException, Exception
     */
    //RMGUAT-1045 changes for ARReceiptTypeCd for AccountStats/Birt reports
    public static String setReceiptTypeCd(ModelBean account)throws ModelBeanException,Exception{
      	String receiptCd = "";
    	    if(null != account){
    	    		ModelBean arReceipt = account.getParentBean().getBeanById(account.gets("ARTransIdRef"));
    	    		if( arReceipt.gets("TypeCd").equalsIgnoreCase("Receipt")){
    	    			receiptCd = (!arReceipt.gets("ARReceiptTypeCd").equalsIgnoreCase("ACH")) ? arReceipt.gets("ARReceiptTypeCd") : (arReceipt.gets("ACHAgent").equals("") || arReceipt.gets("ACHAgent") == null) ? "ACH Customer" : "ACH Agency";	    				
    	    		}
    	    }
    	    return receiptCd;
    	    
      }
    // End changes for RMGUAT-1045
    
}
