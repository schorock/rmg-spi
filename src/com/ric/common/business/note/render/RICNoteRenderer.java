/*
 * RICNoteRenderer.java
 *
 */

package com.ric.common.business.note.render;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.iscs.common.business.note.Note;
import com.iscs.common.business.note.render.NoteRenderer;
import com.iscs.common.tech.form.field.FormFieldRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;
import net.inov.mda.MDAOption;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.date.StringDate;

/** Rendering class called during the Velocity Transformation stage to help process tilesets with Note references.
 *
 * @author  Viplava Kallam
 */
public class RICNoteRenderer extends NoteRenderer{
    
    /** Creates a new instance of NoteRenderer */
    public RICNoteRenderer() {
    }
    
    /** Generates an HTML select form field of templates this user is allowed to add
     * @param fieldId Html field name and id
     * @param defaultValue Default Selected value
     * @param packageName MDA Package Name
     * @param container container Container
     * @param repositoryName MDA Repository
     * @param onChangeAction Action HTML (can be null or empty)
     * @param extra Extra metadata for select tag (can be null or empty)
     * @param user User bean
     * @param todayDt Date representing the current date or session override
     * @return String HTML select tag
     */
    public static String templateSelectField( String fieldId, String defaultValue, String packageName, ModelBean container, String repositoryName, String onChangeAction, String extra, ModelBean user, StringDate todayDt) {
        try {  
            // Default repository to note-template if not supplied
            if(StringTools.isBlank(repositoryName))
                repositoryName = "note-template";            
            
            // Load the MDA bean template for attachments     
            
            ModelBean templates[] = Note.getNoteTemplates(packageName, repositoryName, todayDt);
                        
            // Filter templates
            templates = Note.filterNoteTemplates(templates, packageName, container);
  
            String templateId = defaultValue; //The template Id to be selected
            
            ModelBean template = getTemplate(templates, templateId);
            String templateName = null;
        	
            // Add the ModelBean of the templateId to the templates array if it doesn't already exist in the array
            if (!templateId.isEmpty() && template == null) {
            	ModelBean theTemplate = Note.getNodeTemplate(templateId, container, todayDt);
            	if (theTemplate != null) {
            		templateName = theTemplate.gets("Name");
            		
            		// Add the template to the templates array if there's no template in the array matches the template's name
            		boolean added = true;
            		for (ModelBean t : templates) {
            			if (t.gets("Name").equalsIgnoreCase(templateName)) {
            				added = false;
            				break;
            			}
            		}
            		
            		if (added) {
		            	List<ModelBean> newTemplates = new ArrayList<ModelBean>();
		    			newTemplates.add(theTemplate);
		    			newTemplates.addAll(Arrays.asList(templates));
		    			templates = newTemplates.toArray(new ModelBean[newTemplates.size()]);
            		}
	    		}
            	else {
            		Log.error("Note template not found by Id: " + templateId);
            		MDAOption[] options = new MDAOption[0];
            		return FormFieldRenderer.renderSelect(fieldId, defaultValue, options, "SelectList", "Prompt=Select...|OnChange=" + onChangeAction + "|" + extra);
            	}
            }
            else if (template != null) {
            	templateName = template.gets("Name");
            }
               
            List<MDAOption> array = new ArrayList<MDAOption>();
            
            // Loop through the templates and add them as list items            
            for (int i=0;i<templates.length;i++) {
                if (canDoOperation(templates[i], "Add", true, user, todayDt)){
                    MDAOption option = new MDAOption();
                    
                    if (!templateId.isEmpty() && templates[i].gets("Name").equalsIgnoreCase(templateName)) {
                    	option.setValue(templateId);
                	}
                	else {
                		option.setValue(templates[i].gets("Id"));
                	}
                    
                    option.setLabel(templates[i].gets("Name"));
                    if(checkConversion(container, templates[i].gets("Name"),true)){
                    	array.add(option);
                    }
                }                    
            }
            MDAOption[] options = (MDAOption[]) array.toArray(new MDAOption[array.size()]);
            
            return FormFieldRenderer.renderSelect(fieldId, defaultValue, options, "SelectList", "Prompt=Select...|OnChange=" + onChangeAction + "|" + extra);
        }
        catch( Exception e ) {
            e.printStackTrace();
            Log.error(e);
            return "";
        }
    }
     
    /** Generates an HTML select form field of templates this user is allowed to add
     * @param fieldId Html field name and id
     * @param selected Selected value
     * @param packageName MDA Package Name
     * @param container container Container
     * @param action Action HTML (can be null or empty)
     * @param extra Extra metadata for select tag (can be null or empty)
     * @param user User bean
     * @param todayDt Date representing the current date or session override
     * @return String HTML select tag
     */
    public static String templateSelectField( String fieldId, String selected, String packageName, ModelBean container, String action, String extra, ModelBean user,  StringDate todayDt) {
        try {  
            
            return templateSelectField( fieldId, selected, packageName, container, null, action, extra, user, todayDt);
        }
        catch( Exception e ) {
            e.printStackTrace();
            Log.error(e);
            return "";
        }
    }
    
    /* Returns the velocity variable context name
     * @return "NoteRenderer"
     */
    public String getContextVariableName() {
        return "RICNoteRenderer";
    }    

    
    /** Get the template from the templates array that matches the templateId
	 * @param templates The array of templates
	 * @param templateId The template Id to match
	 * @return The found template or null
	 * @throws Exception When an error occurs
	 */
	private static ModelBean getTemplate(ModelBean[] templates, String templateId) throws Exception {
		if (templateId.isEmpty()) {
			return null;
		}
		
		for (int i = 0; i < templates.length; i++) {
			ModelBean template = templates[i];
			if (template.getId().equalsIgnoreCase(templateId)) {
				return template;
			}
		}
		return null;
	}
	
	/**
	 * 
	 * @param container
	 * @param noteType
	 * @param isConversion
	 * @return
	 * @throws ModelBeanException
	 */
	
	public static boolean checkConversion(ModelBean container,String noteType,boolean isConversion) throws ModelBeanException{
		String noteTypes = "Attorney,Conversion Note,Police Detail,Reassigned,Surveillance,Severity Level";
		if( StringTools.in(noteType, noteTypes) ) {
			if(isConversion){
				return false;
			}
		}
		return true;
	}
}
