package com.ric.common.business.provider.conversion;

import com.iscs.common.utility.error.ErrorTools;
import com.iscs.conversion.DataConversionHandler;

import net.inov.biz.server.ServiceContext;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;


/** Convert the Provider ModelBean from the original system into Innovation.  
 * 
 * @author pavithrak
 *
 */
public class ProviderDataConversion extends com.iscs.common.business.provider.conversion.ProviderDataConversion implements DataConversionHandler {

	public ProviderDataConversion() throws Exception {
		super();
	}
			
	/** Process the ModelBean by invoking it through a Service Chain or other methods to process the data before
	 * it is saved to Innovation
     * @param dtoBean The original DTO ModelBean
	 * @param bean The ModelBean in question
	 * @param serviceName The Webpath service name to call
	 * @return ModelBean result of the Bean Processing
	 * @throws Exception when an error occurs
	 */
	protected ModelBean processModelBean(ModelBean dtoBean, ModelBean bean, String serviceName)
	throws Exception {
		// Determine if the bean exists first.  If so, then update it
		// else insert it
		JDBCData data = ServiceContext.getServiceContext().getData();
		ModelBean foundBean = findBean(data, bean);
		if (foundBean != null) {
			bean.setValue("SystemId", foundBean.getSystemId());
			bean.setValue("UpdateCount", foundBean.getValueInt("UpdateCount"));		// These 3 lines ensure
			bean.setValue("UpdateUser", foundBean.gets("UpdateUser"));				// we don't get a false
			bean.setValue("UpdateTimestamp", foundBean.gets("UpdateTimestamp"));	// concurrency exception
			
			
			ModelBean producerInfoBean = foundBean.getBean("ProducerInfo");		
			if(producerInfoBean != null) {				
				//CommissionPayThroughDt should not be updated if it exists in system
				String commissionPayThruDt = producerInfoBean.gets("CommissionPayThroughDt");				
				bean.getBean("ProducerInfo").setValue("CommissionPayThroughDt", commissionPayThruDt);				
			}
			
    		ModelBean[] attachmentBeans = foundBean.getBeans("Attachment");	
    		for(ModelBean attachment : attachmentBeans) {
				//Attachment beans should not be updated if it exists in system			
				bean.addValue(attachment.cloneBean());
    		}
    		
    		ModelBean[] noteBeans = foundBean.getBeans("Note");	
    		for(ModelBean note : noteBeans) {
				//Note beans should not be updated if it exists in system			
				bean.addValue(note.cloneBean());
    		}
    		
    		
		}
		
		// Add the additional params needed
		ModelBean rq = buildServiceRequest(serviceName, bean);
		ModelBean taxInfo = bean.getBean("PartyInfo").getBean("TaxInfo");
		if (taxInfo.gets("TaxIdTypeCd").equals("SSN"))
			setAdditionalParam(rq, "TaxId", taxInfo.gets("SSN"));
		else
			setAdditionalParam(rq, "TaxId", taxInfo.gets("FEIN"));
		
		rq.addValue(bean);  // Add the bean
		
		// Call the service chain
		ModelBean result = callService(rq);
		
		// Add all of the errors in the service chain to here
		ModelBean[] errors = result.getBean("ResponseParams").getBean("Errors").getBeans("Error");
		boolean hasErrors = false;
		for (int i=0; i<errors.length; i++) {
    		if (errors[i].gets("Severity").equals(ErrorTools.SEVERITY_ERROR.toString())) {
    			addErrorMsg(errors[i]);
    			hasErrors = true;
    		}
		}		
		if (hasErrors) {
			return null;  // Set to null if there are errors.  This is for the return value.
		} else {
			// Update/Create the original System ID
			ModelBean resultBean = result.getBean(getBeanName());
			return resultBean;		
		}
	}

	

}