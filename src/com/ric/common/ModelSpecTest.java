package com.ric.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import junit.framework.Test;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.export.TableMapper;

import com.iscs.common.business.stat.StatProcessing;
import com.iscs.common.tech.bean.data.SQLRelationDelegateBase;
import com.iscs.common.tech.junit.InnovationTestBase;
import com.iscs.common.tech.log.Log;

/**
 * @author kenb
 *
 */
public class ModelSpecTest extends InnovationTestBase {

	private static final int MAX_BEAN_TABLENAME_ORACLE = 27;
	private static final int MAX_STAT_BEAN_TABLENAME_ORACLE = 29;
	private static final int MAX_DATAMART_TABLENAME_ORACLE = 24;
	private static final int MAX_COLUMNNAME_ORACLE = 30;
	private static final String[] NON_STAT_RELATIONAL_MODELS = new String[] {"CommissionDetail"};
	
	static {
		// Make sure the NON_STAT_RELATIONAL_MODELS array is sorted so Arrays.binarySearch works
		// Tha Javadoc for Arrays.sort seems to imply the following won't work...but it does.
		Arrays.sort(NON_STAT_RELATIONAL_MODELS);
	}
    
    /** Test Suite
     * @throws Exception Thrown When an Error Occurs Testing
     * @return TestSuite
     */    
    public static Test suite() throws Exception {      	
    	return suite(ModelSpecTest.class);   
    }
    
    /** Test getting a running sequence
     * 
     */
    public void testModelSpec(){
    	try{

    		// Check for model errors related to Oracle
    		String[] errors = modelErrorsOracle(ModelSpecification.getSharedModel());
    		for(int i=0; i<errors.length; i++){
    			Log.error(errors[i]);
    		}
    		assertEquals("Possible problems with Model Spec on Oracle", 0, errors.length);
        	
    	}
    	catch(Exception e){
    		e.printStackTrace();
    		fail(e.getMessage());
    	}

    }
    
	/** Check for model errors that would occur in Oracle
	 * 
	 * @param spec Model Specification
	 * @return Errors array
	 * @throws Exception for unexpected errors
	 */
	public String[] modelErrorsOracle(ModelSpecification spec) throws Exception{
		
		TableMapper map = new TableMapper();
		
		List<String> errors = new ArrayList<String>();
		
		ModelSpecification.ModelClass[] classes = spec.getModels();
		for(int i=0; i<classes.length; i++){
			
			String beanName = classes[i].getName();
			
			// Attempt to create the bean
			
			ModelBean bean = null;
			try {
				bean = new ModelBean(beanName);				
			} catch(Exception e){
				errors.add("Cannot create bean: " + beanName);
			}
			
			// Check the table name of stand-alone beans for problems
			
			if (classes[i].isTop() && bean != null) {
				if (bean.getTableName().length() > MAX_BEAN_TABLENAME_ORACLE) {
					errors.add("Container bean '" + beanName + "' table name too long for Oracle: '" + bean.getTableName() + "' (" + bean.getTableName().length() + " characters). Change table attribute in bean model to be <= " + MAX_BEAN_TABLENAME_ORACLE + " characters.");
				}
			}
			
			//
			// Check non-stat relational beans for problems
			
			if (Arrays.binarySearch(NON_STAT_RELATIONAL_MODELS, beanName) >= 0) {
				// check for potential table name problems
				if (bean.getTableName().length() > MAX_BEAN_TABLENAME_ORACLE) {
					errors.add("Relational model bean '" + beanName + "' table name too long for Oracle: '" + bean.getTableName() + "' (" + bean.getTableName().length() + " characters). ModelBean name must be <= " + MAX_BEAN_TABLENAME_ORACLE + " characters to be stored as a relational table.");
				}
				// check for potential column problems
				String[] fieldNames = SQLRelationDelegateBase.getBeanFields(bean);
				for (int fieldIndex=0; fieldIndex<fieldNames.length; fieldIndex++) {
					String fieldName = fieldNames[fieldIndex];
					if(fieldName.length() > MAX_COLUMNNAME_ORACLE){
						errors.add("Relational model Field name too long for Oracle: '" + fieldName + "' in '" + beanName + "' (" + fieldName.length() + " characters). Change field name to be <= " + MAX_COLUMNNAME_ORACLE + " characters.");
					}				
				}
			}
			
			// Check the bean for possible datamart export problems
			
			String mappedTableName = map.mapTableName(beanName);
			
			if(mappedTableName.length() > MAX_DATAMART_TABLENAME_ORACLE && !beanName.endsWith("Rq") && !beanName.endsWith("Rs")){
				errors.add("Table name too long for Oracle datamart: '" + mappedTableName + "' (" + mappedTableName.length() + " characters). Use datamart-mapping.xml to map to <= " + MAX_DATAMART_TABLENAME_ORACLE + " characters.");
			}
			
			ModelSpecification.ModelField[] fields = classes[i].getSimpleFields();
			for(int fieldIndex=0; fieldIndex<fields.length; fieldIndex++){
				String fieldName = fields[fieldIndex].getName();
				String mappedFieldName = map.mapColumnName(beanName, fieldName);
				if(mappedFieldName.length() > MAX_COLUMNNAME_ORACLE){
					errors.add("Field name too long for Oracle datamart: '" + mappedFieldName + "' in '" + beanName + "' (" + mappedFieldName.length() + " characters). Use datamart-mapping.xml to map to <= " + MAX_COLUMNNAME_ORACLE + " characters.");
				}				
			}
			
		}
		
		//
		// Check any stat tables (relational tables) for possible problems
		
		String[] statTables = null;
		try {
	        Log.debug("Setting logging level to error to silently initialize StatProcessing");
	        Log.setLogLevel(Log.ERROR);
			statTables = StatProcessing.getStatTables();
		} finally {
	        Log.setLogLevel(Log.DEBUG);
	        Log.debug("Resetting logging level to debug");
		}
		Log.debug("Stats tables detected: " + Arrays.asList(statTables).toString());
		for (int statIndex=0; statIndex<statTables.length; statIndex++) {
			String beanName = statTables[statIndex];
			
			// check the table name
			if (beanName.length() > MAX_STAT_BEAN_TABLENAME_ORACLE) {
				errors.add("Stat bean '" + beanName + "' table name too long for Oracle: '" + beanName + "' (" + beanName.length() + " characters). Change bean name to be <= " + MAX_BEAN_TABLENAME_ORACLE + " characters.");
			}
			
			// check the field names
			String[] fieldNames = SQLRelationDelegateBase.getBeanFields(new ModelBean(beanName));
			for (int fieldIndex=0; fieldIndex<fieldNames.length; fieldIndex++) {
				String fieldName = fieldNames[fieldIndex];
				if(fieldName.length() > MAX_COLUMNNAME_ORACLE){
					errors.add("Stat Field name too long for Oracle: '" + fieldName + "' in '" + beanName + "' (" + fieldName.length() + " characters). Change field name to be <= " + MAX_COLUMNNAME_ORACLE + " characters.");
				}				
			}
		}
		
		// Prepare the errors
		Collections.sort(errors);
		
		return (String[]) errors.toArray(new String[errors.size()]);
		
	}
}
