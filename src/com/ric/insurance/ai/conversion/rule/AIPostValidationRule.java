package com.ric.insurance.ai.conversion.rule;

import java.util.ArrayList;

import net.inov.mda.MDATable;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;

import com.iscs.common.business.rule.RuleException;
import com.iscs.common.mda.Store;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.NameValuePair;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.common.utility.error.ErrorType;
import com.iscs.conversion.DataConversionHelper;

/** This java rule handles the post DTO validation for Additional Interests.  
 *	It extends the base validation of the Provider Converter.
 * Therefore any changes to the ProviderDataConversion class should take care in that it may affect how the AI
 * validation is done.
 * 
 * @author satishb
 *
 */
public class AIPostValidationRule implements com.iscs.common.business.rule.RuleProcessor 
{
	ArrayList errorList = null;
	
    /** Extend the Provider validation and process the Post DTO validation
     * @param ruleTemplate Rule template bean
     * @param bean Primary bean to operate on
     * @param additionalBeans Additional beans to add to the rule's context.  Pass null for no additional beans 
     * @param user User bean processing the rule
     * @param data JDBCData if applicable
     * @return Rule result bean
     * @throws RuleException for errors
     */
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user, JDBCData data) 
	throws RuleException {
		errorList = new ArrayList();
		ModelBean errors;
		try {
			errors = new ModelBean("Errors");
			// Create the return ModelBean process
			DataConversionHelper.validateField(bean, "ProviderTypeCd", "", new NameValuePair[] {new NameValuePair("Additional Interest", "Additional Interest")}, true, errorList);
			
			//Check Structure
			ModelBean[] partyInfos; 
			
			partyInfos = bean.getBeans("PartyInfo");
			if (partyInfos.length != 1) 
				addErrorMsg("ValidationError", "There must be exactly one PartyInfo node below the DTOProvider node", ErrorTools.FIELD_CONSTRAINT_ERROR);			
			else 
			{
				validateAddressInfo(partyInfos[0], new String[]{"ProviderBillingAddr","ProviderStreetAddr"}, new boolean[]{true,true}, new boolean[]{true,true});
				validateTaxInfo(partyInfos[0], "ProviderTaxInfo", true, true);
				validateEmailInfo(partyInfos[0], "ProviderEmail", true, true);
				validatePhoneInfo(partyInfos[0], "ProviderPrimaryPhone", "ProviderSecondaryPhone", "ProviderFax");
			    validateNameInfo(partyInfos[0], "ProviderName", true, true);
			}
			
			//Validate field codes
			String legalEntityCd = partyInfos[0].getBean("TaxInfo").gets("LegalEntityCd");
			String validLegalEntityCd = getValidCodes("INAI::ai::businesstype::all");
			if(!isValidCode(validLegalEntityCd, legalEntityCd) || legalEntityCd.trim().equals(""))
				addErrorMsg("ValidationError", "The LegalEntityCd \"" + legalEntityCd + "\" isn't one of " + validLegalEntityCd, ErrorTools.FIELD_CONSTRAINT_ERROR);
			
			
			// Add of the current errors into the ModelBean response
			for (int i=0; i<errorList.size(); i++) {
				errors.addValue((ModelBean) errorList.get(i));
			}			
		} 
		catch(Exception e)
		{	
			throw new RuleException(e);
		}
		return errors;
	}
	
    /** Adds a single error message to the Error List
     * @param name Error name
     * @param message Error message
     * @param type Error type
     * @param severity Error severity (Info/Warn/Error)
     */
    public void addErrorMsg(String name, String message, ErrorType type) {
        try {
            ModelBean error = new ModelBean("Error");
            error.setValue("Msg", message);
            error.setValue("Name", name);
            error.setValue("Type", type);
            error.setValue("Severity",ErrorTools.SEVERITY_ERROR.getSeverity());
            errorList.add(error);
        }
        catch( Exception e ) {
            Log.error(e.getMessage(),e);
        }
    }
	
    /** Add an error in the form of a ModelBean Error into the List
     * @param error The Error ModelBean
     */
    public void addErrorMsg(ModelBean error) {
    	errorList.add(error);
    }
    
    public void validatePhoneInfo(ModelBean partyInfo, String primaryType, String secondaryType, String faxType)
    throws Exception {
    	String parent = partyInfo.getParentBean().getBeanName();
		// Validate the Phone types
		ModelBean[] phones = partyInfo.getBeans("PhoneInfo");
		int phoneOneCnt = 0;
		int phoneTwoCnt = 0;
		int faxCnt = 0;
		for (int i=0; i<phones.length; i++) {
			String phoneTypeCd = phones[i].gets("PhoneTypeCd");
			if (!phoneTypeCd.equals(primaryType) && !phoneTypeCd.equals(secondaryType) && !phoneTypeCd.equals(faxType)) {
    			String msg = "Field PartyInfo.PhoneInfo.PhoneTypeCd for parent " + parent + " with the value of " + phoneTypeCd + " has an incorrect phone type. It should be either (" + primaryType + ", " + secondaryType + ", " + faxType + ").";
    			addErrorMsg("ValidationError", msg, ErrorTools.FIELD_CONSTRAINT_ERROR);
			} else {
				if (phoneTypeCd.equals(primaryType)) phoneOneCnt++;
				if (phoneTypeCd.equals(secondaryType)) phoneTwoCnt++;
				if (phoneTypeCd.equals(faxType)) faxCnt++;				
			}
		}
		if (phoneOneCnt > 1 || phoneTwoCnt > 1 || faxCnt > 1) {
			String msg = "There can only be one PhoneTypeCd of each of the following (" + primaryType + ", " + secondaryType + ", " + faxType + ").";
			addErrorMsg("ValidationError", msg, ErrorTools.FIELD_CONSTRAINT_ERROR);
		}
    }
    
    /** Convenience method to validate a EmailInfo ModelBean underneath a PartyInfo ModelBean
     * @param partyInfo The PartyInfo ModelBean
     * @param emailType The Alias name of the EmailInfo
     * @param isRequired true/false indicator if it is required
     * @param addIfMissing true/false indicator if the ModelBean should be added if it does not exist
     * @throws Exception when an error occurs
     */
    public void validateEmailInfo(ModelBean partyInfo, String emailType, boolean isRequired, boolean addIfMissing)
    throws Exception {
		ArrayList errors = new ArrayList();
		
    	DataConversionHelper.validateSubBeanType(partyInfo, "EmailInfo", "EmailTypeCd", emailType, isRequired, addIfMissing, errors);
    	ModelBean[] errorBeans = (ModelBean[]) errors.toArray(new ModelBean[errors.size()]);
    	for (int i=0; i<errorBeans.length; i++) {
    		addErrorMsg(errorBeans[i]);
    	}		
    }
    
    /** Convenience method to validate an AddressInfo ModelBean underneath a PartyInfo ModelBean
     * @param partyInfo The PartyInfo ModelBean
     * @param personType The Alias name of the PersonInfo
     * @param isRequired true/false indicator if it is required
     * @param addIfMissing true/false indicator if the ModelBean should be added if it does not exist
     * @throws Exception when an error occurs
     */
    public void validateAddressInfo(ModelBean partyInfo, String[] addressTypes, boolean[] isRequired, boolean[] addIfMissing)
    throws Exception {
		ArrayList errors = new ArrayList();
		
    	DataConversionHelper.validateSubBeanType(partyInfo, "Addr", "AddrTypeCd", addressTypes, isRequired, addIfMissing, errors);
    	ModelBean[] errorBeans = (ModelBean[]) errors.toArray(new ModelBean[errors.size()]);
    	for (int i=0; i<errorBeans.length; i++) {
    		addErrorMsg(errorBeans[i]);
    	}		
    }
    
    
    /** Convenience method to validate a TaxInfo ModelBean underneath a PartyInfo ModelBean
     * @param partyInfo The PartyInfo ModelBean
     * @param taxType The Alias name of the TaxInfo
     * @param isRequired true/false indicator if it is required
     * @param addIfMissing true/false indicator if the ModelBean should be added if it does not exist
     * @throws Exception when an error occurs
     */
    public void validateTaxInfo(ModelBean partyInfo, String taxType, boolean isRequired, boolean addIfMissing)
    throws Exception {
		ArrayList errors = new ArrayList();
		
    	DataConversionHelper.validateSubBeanType(partyInfo, "TaxInfo", "TaxTypeCd", taxType, isRequired, addIfMissing, errors);
    	ModelBean[] errorBeans = (ModelBean[]) errors.toArray(new ModelBean[errors.size()]);
    	for (int i=0; i<errorBeans.length; i++) {
    		addErrorMsg(errorBeans[i]);
    	}		
    }

    /** Convenience method to validate a NameInfo ModelBean underneath a PartyInfo ModelBean
     * @param partyInfo The PartyInfo ModelBean
     * @param nameType The Alias name of the NameInfo
     * @param isRequired true/false indicator if it is required
     * @param addIfMissing true/false indicator if the ModelBean should be added if it does not exist
     * @throws Exception when an error occurs
     */
    public void validateNameInfo(ModelBean partyInfo, String nameType, boolean isRequired, boolean addIfMissing)
    throws Exception {
		ArrayList errors = new ArrayList();
		
    	DataConversionHelper.validateSubBeanType(partyInfo, "NameInfo", "NameTypeCd", nameType, isRequired, addIfMissing, errors);
    	ModelBean[] errorBeans = (ModelBean[]) errors.toArray(new ModelBean[errors.size()]);
    	for (int i=0; i<errorBeans.length; i++) {
    		addErrorMsg(errorBeans[i]);
    	}
    }
    
    protected boolean isValidCode(String itemList, String code) 
    throws Exception {
    	String[] list = itemList.split(",");
    	for (int i=0; i<list.length; i++) {
    		if (code.equalsIgnoreCase(list[i]))
    			return true;
    	}
    	return false;
    }
    
    protected String getValidCodes(String umol)
    throws Exception {
    	MDATable table = (MDATable) Store.getModelObject(umol);
    	NameValuePair[] list = NameValuePair.fromTable(table);
    	StringBuffer itemList = new StringBuffer();
    	for (int i=0; i<list.length; i++) {
    		if (i > 0)
    			itemList.append(",");
    		itemList.append(list[i].getValue());
    	}    	
    	return itemList.toString();
    }
 }
