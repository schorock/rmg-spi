/*
 * AIValidate.java
 *
 */

package com.ric.insurance.ai.handler;

import java.util.ArrayList;

import com.ric.insurance.product.render.RICProductRenderer;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.insurance.ai.AI;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;

/** Validates an Additional Interest
 *
 * @author  moniquef
 */
public class RICAIValidate extends InnovationIBIZHandler {
    // Create an object for logging messages
    
    
    /** Creates a new instance of AIValidate
     * @throws Exception never
     */
    public RICAIValidate() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a Greeting
            Log.debug("Processing AIValidate...");
            ModelBean rs = this.getHandlerData().getResponse();
            
            // Get Provider ModelBean
            ModelBean provider = rs.getBean("Provider");
            
         
            // Validate Field Values
            ArrayList<ModelBean> errors = new ArrayList<ModelBean>();
            AI.validate(provider, errors);
            
            // Loop Through Validation Errors and Add Them to the Response
            for( int i = 0; i < errors.size(); i++ ) {
                ModelBean errorBean = (ModelBean) errors.get(i);
                addErrorMsg(errorBean.gets("Name"), errorBean.gets("Msg"), errorBean.gets("Type") );
            }
            
            // If Response has Errors, Throw IBIZException
            if( hasErrors() )
                throw new IBIZException();
            else {
            	if ( provider.gets("ProviderNumber").equals("") ) {
            	  String providerNumber = RICProductRenderer.nextAIProviderNumber();
                  provider.setValue("ProviderNumber", providerNumber);
            	}
            }
            // Cleanup Unused ModelBeans in the Provider Model
            AI.cleanupModel(provider);
            
            // Return the Response ModelBean
            return rs;
        }
        catch( IBIZException e ) {
            throw new IBIZException();
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
    
}

