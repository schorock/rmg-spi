/*
 * ProductRenderer.java
 *
 */

package com.ric.insurance.product.render;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.axis.utils.StringUtils;
import org.jdom.Element;

import com.google.gson.Gson;
import com.iscs.common.business.datareport.DataReport;
import com.iscs.common.business.provider.render.ProviderRenderer;
import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDAXmlDoc;
import com.iscs.common.question.Question;
import com.iscs.common.question.render.QuestionRenderer;
import com.iscs.common.render.DateRenderer;
import com.iscs.common.render.DynamicString;
import com.iscs.common.render.Renderer;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.render.VelocityTools;
import com.iscs.common.shared.PartyInfo;
import com.iscs.common.shared.address.AddressRenderer;
import com.iscs.common.shared.address.AddressTools;
import com.iscs.common.statementaccount.render.HPStatementAccountRenderer;
import com.iscs.common.tech.form.field.FormFieldRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.bean.render.BeanRenderer;
import com.iscs.common.utility.counter.Counter;
import com.iscs.document.render.DCRenderer;
import com.iscs.insurance.product.ProductSetup;
import com.iscs.insurance.product.render.ProductRenderer;
import com.iscs.uw.common.product.UWProductSetup;
import com.iscs.uw.interfaces.datareport.render.UWDataReportRenderer;
import com.iscs.uw.policy.PM;
import com.iscs.uw.policy.render.PolicyRenderer;
import com.ric.uw.app.Application;

import net.inov.biz.server.ServiceContext;
import net.inov.mda.MDAOption;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.beans.ModelBeanField;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.prefs.PrefsDirectory;
import net.inov.tec.security.SecurityManager;
import net.inov.tec.xml.XmlDoc;


public class RICProductRenderer implements Renderer {

    protected ModelBean rs; // Response
    private static final long AIPROVIDERNUMBER_START = 3800;
	private static final String AIPROVIDERNUMBER_PREFIX = "AI-";
	
	
    /** Creates a New Instance of ProductRenderer */
    public RICProductRenderer() {
    }

	public String getContextVariableName() {
		return "RICProductRenderer";
	}

	public static void debug(Object o){
		System.out.println(o);
	}

	public static void debug(String s){
		System.out.println(s);
	}

	public String getSPIVersion() {
		String version = null;
		try {
			version = this.getClass().getPackage().getImplementationVersion();
		} catch (Exception e) {
			Log.error(e);
			// Swallow the exception. We don't want a failure to get the version to cripple the app.
		}
		if (version == null) {
			version = "";
		}
		return version;
	}	

	public static ModelBean createCopyOfBean(ModelBean source, String targetBeanName) throws Exception{
		ModelBean target = new ModelBean(targetBeanName);
		BeanTools.copyBeanById(source, target, true);
		return target;
	}

	/**
	 * Find bean by field value. Returns bean if found, else returns new bean for access and manipulation. 
	 * @param container
	 * @param beansToFind
	 * @param fieldname
	 * @param value
	 * @return If found, return ModelBean. If not found, return new ModelBean.
	 * @throws Exception
	 */
	public static ModelBean findBeanByFieldValue( ModelBean container, String beansToFind, String fieldname, String value ) throws Exception {
		ModelBean foundBean = container.findBeanByFieldValue(beansToFind, fieldname, value);

		if( foundBean != null )
			return foundBean;

		return new ModelBean(beansToFind);
	}	

	/**
	 * For ISO location use only, creates a risk address bean from parsed to unparsed.
	 * @param source
	 * @return
	 * @throws Exception
	 */
	public static ModelBean createUnparsedCopyOfBean(ModelBean source) throws Exception {
		ModelBean copyTo = new ModelBean("Addr");
		copyTo.setValue("AddrTypeCd","RiskAddr");
		return copyAddressValues(source, copyTo);
	} 

	/**
	 * For address beans, copy non alias values even if the copyFrom is blank
	 * Wipe all fields first, and then set the copyTo bean
	 * @param copyFrom
	 * @param copyTo
	 * @return
	 * @throws Exception
	 */
	public static ModelBean copyAddressValues(ModelBean copyFrom, ModelBean copyTo) throws Exception {

		Log.debug("Old Address: " + copyTo.toPrettyString());

		for (ModelBeanField field: copyFrom.getAllFields()) {
			if (!field.isComplexType()) {
				String name = field.getName();
				if (!StringTools.in(name,"id,SystemId,AddrTypeCd")) {
					copyTo.setValue(name,"");
				}
			}
		}
		BeanTools.copyNonAliasValues(copyFrom, copyTo);

		if (copyFrom.gets("AddrTypeCd").equals("InsuredResidentAddr") && !copyTo.gets("AddrTypeCd").contains("Lookup")) {
			AddressTools.clearParsedFields(copyTo);
			copyTo.setValue("Addr1",AddressTools.getAddressLine1(copyFrom));
			copyTo.setValue("Addr2",AddressTools.getAddressLine2(copyFrom));
		}

		Log.debug("New Address: " + copyTo.toPrettyString());
		return copyTo;
	}

	/**
	 * Get list.xml options which can be used to populate
	 * a select list in JS
	 * Collects the name and value
	 */
	public String getDynamicSelectListOptions(String productVersionIdRef, String umol) {
		try {
			ModelBean[] list = getProductCoderefList(productVersionIdRef, umol);
			if (list == null || list.length < 1) 
				return "";
			else {
				String options = "";
				for (int i = 0; i < list.length; i++) {
					ModelBean option = list[i];
					options += ";" + option.gets("value") + ":" + option.gets("name");
				}
				if (options.startsWith(";")) 
					return options.substring(1);
				else 
					return options;
			}    		
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

	}

	public ModelBean[] getProductCoderefList(String productVersionIdRef, String umol) {
		try {
			ModelBean coderef = ProductSetup.getProductCoderef(productVersionIdRef, umol);
			return coderef.getBean("Options").getBeans("Option");    		
		} catch (Exception e) {
			e.printStackTrace();
			Log.debug("No coderef was found - " + umol );
			return new ModelBean[0];
		}
	}

	public ModelBean getOptionByNameFromProductCoderefList( String productVersionIdRef, String umol, String name ){
		try {
			ModelBean coderef = ProductSetup.getProductCoderef(productVersionIdRef, umol);
			return coderef.getBean("options").getBean("option", "name", name );
		} catch (Exception e) {
			e.printStackTrace();
			Log.debug("No coderef was found - " + umol );
			return null;
		}
	}
	
	/**
	 * Get JSON Solid Fuel correlating heating type map.
	 * @param productVersionIdRef
	 * @return
	 * @throws ModelBeanException
	 */
	public String getJSONSolidFuelCorrelatingHeatingTypeMap( String productVersionIdRef ) throws ModelBeanException {
		Map<String, String> json = new HashMap<>();
		
		try{ 
			ModelBean heatingTypes = ProductSetup.getProductCoderef( productVersionIdRef, "UW::underwriting::solid-fuel-heat::correlating-heating-type");
			for( ModelBean option : heatingTypes.getBean("Options").getBeans("Option") ){
					json.put(option.gets("name"), option.gets("value"));
			}
			
			return new Gson().toJson(json).toString();
		} catch( NullPointerException nullPointerException ) {
			Log.debug("UMOL does not exist.");
			return "{}";
		}
	}

	/**
	 * Get all peril options.
	 * @param productVersionIdRef
	 * @param covALimit
	 * @param county
	 * @param city
	 * @return
	 * 
	 * Returns an all peril options key based on location (county or city) and Coverage A limit. 
	 * The key will be returned as location-group-<location group letter>-<Cov A range>. If there are no key returned based on location,
	 * there is a "catch all" default, location-group-default-<Cov A range>.
	 * Please see RIC DW-Std-DP-UW workbook under the windhailhurricaneded tab for more information.
	 * 
	 * NOTE: Underscore is used as a delimiter.
	 */
	public String getAllPerilDedOptionsKey( String productVersionIdRef, String covALimit, String county, String city){
		try {
			String umol = "UW::underwriting::location-groups::",
					countiesUmol = null, citiesUmol = null, foundCorrectKey = null;

			if( !covALimit.isEmpty() ){
				if( StringRenderer.greaterThanEqual(covALimit, "500000") ){

					countiesUmol = umol + "location-group-gte-500000-counties";
					citiesUmol = umol + "location-group-gte-500000-cities";

					foundCorrectKey = getLinkedCodeRef(productVersionIdRef, county, city, countiesUmol, citiesUmol);
				} 

				if( foundCorrectKey == null && StringRenderer.greaterThanEqual(covALimit, "250000") ){
					countiesUmol = umol + "location-group-gte-250000-lt-500000-counties";
					citiesUmol = umol + "location-group-gte-250000-lt-500000-cities";

					foundCorrectKey = getLinkedCodeRef(productVersionIdRef, county, city, countiesUmol, citiesUmol);

					if( foundCorrectKey == null ){
						countiesUmol = umol + "location-group-gte-250000-counties";
						citiesUmol = umol + "location-group-gte-250000-cities";

						foundCorrectKey = getLinkedCodeRef(productVersionIdRef, county, city, countiesUmol, citiesUmol);
					}
				} 

				if( foundCorrectKey == null && StringRenderer.greaterThanEqual(covALimit, "200000") ){
					countiesUmol = umol + "location-group-gte-200000-lt-250000-counties";
					citiesUmol = umol + "location-group-gte-200000-lt-250000-cities";

					foundCorrectKey = getLinkedCodeRef(productVersionIdRef, county, city, countiesUmol, citiesUmol);

					if( foundCorrectKey == null ){
						countiesUmol = umol + "location-group-gte-200000-counties";
						citiesUmol = umol + "location-group-gte-200000-cities";

						foundCorrectKey = getLinkedCodeRef(productVersionIdRef, county, city, countiesUmol, citiesUmol);
					}
				} 

				if( foundCorrectKey == null && StringRenderer.greaterThanEqual(covALimit, "100000") ){

					countiesUmol = umol + "location-group-gte-100000-lt-200000-counties";
					citiesUmol = umol + "location-group-gte-100000-lt-200000-cities";

					foundCorrectKey = getLinkedCodeRef(productVersionIdRef, county, city, countiesUmol, citiesUmol);

				} 

				if( foundCorrectKey == null ){

					countiesUmol = umol + "location-group-lt-100000-counties";
					citiesUmol = umol + "location-group-lt-100000-cities";

					foundCorrectKey = getLinkedCodeRef(productVersionIdRef, county, city, countiesUmol, citiesUmol);
				}

				//If there is no key found, go directly to the location-group-default options.
				if( foundCorrectKey != null )
					return foundCorrectKey;
				else {

					if( StringRenderer.greaterThanEqual(covALimit, "500000") )
						return "location-group-default_gte-500000";

					if( StringRenderer.greaterThanEqual(covALimit, "250000") )
						return "location-group-default_gte-250000-lt-500000";

					if( StringRenderer.greaterThanEqual(covALimit, "200000") )
						return "location-group-default_gte-200000-lt-250000";

					if( StringRenderer.greaterThanEqual(covALimit, "100000") )
						return "location-group-default_gte-100000-lt-200000";

					return "location-group-default_lt-100000";
				}
			}

			return null;
		} catch ( Exception e ) {
			e.printStackTrace();
			Log.debug("No coderef was found" );
			return null;
		}
	}
	
	/** Gets Ordinal Numbers for a Given date
	 * 
	 * @param date
	 * @return
	 */
	public String getOrdinalDate(String datestr) {
		try {
			int date = Integer.parseInt(datestr);
			switch (date) {
            case 1:
            case 21:
            case 31:
                return "" + date + "st";

            case 2:
            case 22:
                return "" + date + "nd";

            case 3:
            case 23:
                return "" + date + "rd";

            default:
                return "" + date + "th";
}    		
		} catch (Exception e) {
			e.printStackTrace();
			Log.debug("Not able to find Ordinal Number for a date - " + datestr );
			return null;
		}
	}


	public String getFirstOptionValue( String productVersionIdRef, String umol ) {
		try{ 
			ModelBean options = ProductSetup.getProductCoderef( productVersionIdRef, umol );

			if( options != null )
				return options.getBean("Options").getBeans("Option")[0].gets("value");

			return "";

		} catch( NullPointerException nullPointerException ) {
			Log.debug("UMOL does not exist.");
		} catch (Exception e) {
			Log.debug("Value does not exist.");
		}

		return "";
	}

	public boolean isAnOption( String productVersionIdRef, String umol, String option ) {
		try{ 
			ModelBean options = ProductSetup.getProductCoderef( productVersionIdRef, umol );

			if( options != null )
				return options.getBean("Options").findBeansByFieldValue("Option", "value", option ).length > 0; 

				return false;

		} catch( NullPointerException nullPointerException ) {
			Log.debug("UMOL does not exist.");
		} catch (Exception e) {
			Log.debug("Value does not exist.");
		}

		return false;
	}

	/**
	 * Get the corresponding AllPeril coderef for county or city. If it doesn't exist, return null. Else return the linked coderef.
	 * @param productVersionIdRef
	 * @param county
	 * @param city
	 * @param countiesUmol
	 * @param citiesUmol
	 * @return
	 */
	public String getLinkedCodeRef( String productVersionIdRef, String county, String city, String countiesUmol, String citiesUmol ){
		try{
			try{ 
				ModelBean counties = ProductSetup.getProductCoderef( productVersionIdRef, countiesUmol );

				if( counties != null ){
					for( ModelBean option : counties.getBean("Options").getBeans("Option") ){
						if( option.gets("name").equals( county ) )
							return option.gets("value");
					}
				}

			} catch( NullPointerException nullPointerException ) {
				Log.debug("Counties UMOL does not exist.");
			}

			try{ 
				ModelBean cities = ProductSetup.getProductCoderef( productVersionIdRef, citiesUmol );

				if( cities != null ){
					for( ModelBean option : cities.getBean("Options").getBeans("Option") ){
						if( option.gets("name").equals( city ) )
							return option.gets("value");
					}
				}

			} catch( NullPointerException nullPointerException ){
				Log.debug("Cities UMOL does not exist");
			}

			return null;

		} catch (Exception e) {
			Log.debug("No coderef was found" );
			return null;
		}
	}


	/**
	 * Set the previous carrier on an application.
	 * @param applicationId
	 * @param previousCarrierCd
	 * @return
	 * @throws Exception 
	 */
	public String updatePreviousCarrierCd( String applicationSystemId, String previousCarrierCd ) throws Exception{
		try {
			JDBCData data = ServiceContext.getServiceContext().getData();

			ModelBean application = Application.getApplicationBySystemId(data, Integer.parseInt(applicationSystemId));
			application.getBean("BasicPolicy").setValue("PreviousCarrierCd", previousCarrierCd);
			BeanTools.saveBean(application, data);

			return "ok";

		} catch(Exception e) {
			throw e;
		}
	}

	/**
	 * Divide and round two values Function origin: Velocity allows math
	 * calculations on integers only
	 * 
	 * @param value1
	 *            first value
	 * @param value2
	 *            second value
	 * @return rounded result of division
	 */
	public static String divide(String value1, String value2) {
		try {
			BigDecimal a1 = new BigDecimal(value1);
			BigDecimal a2 = new BigDecimal(value2);

			//scaling to two decimal places 
			BigDecimal result = a1.divide(a2, 2, BigDecimal.ROUND_HALF_UP);

			return result.toString();

		} catch (Exception e) {
			return value1;
		}
	}

	/***
	 * Gets the changed content in the forms during transaction change
	 * @param container
	 * @return
	 * @throws ModelBeanException 
	 */
	public static Object[] getChangeText(ModelBean container) throws ModelBeanException {

		ModelBean transaction = null;
		ArrayList<String> changeTextList = new ArrayList<String>();

		if( "Application".equalsIgnoreCase(container.getBeanName())  ) {
			transaction = container.getBean("TransactionInfo");
		} else if( "Policy".equalsIgnoreCase(container.getBeanName())) {
			ModelBean basicPolicy = container.getBean("BasicPolicy");
			ModelBean[] transactionHistories = basicPolicy.getBeansSorted("TransactionHistory","TransactionNumber","Descending");
			transaction = transactionHistories[0];
		}

		if( transaction!= null ) {
			if( transaction.getBeans("TransactionText").length > 0 ) {
				for ( ModelBean transactionText : transaction.getBeans("TransactionText") ) {
					changeTextList.add(transactionText.gets("Text"));
				}
			}
		}
		return (Object[])changeTextList.toArray();
	}

	/**
	 * Convert time string to HH:mm
	 * @param srcStr
	 * @return
	 * @throws ParseException 
	 */
	public static String convertTimeString(String srcStr) throws ParseException {
		String outputStr = "";
		if(srcStr != null && srcStr.length() > 0) {
			SimpleDateFormat sdfDest = new SimpleDateFormat("K:mm a");
			Date dt = formatTimeString(srcStr);
			if (dt != null) outputStr = sdfDest.format(dt);
		}
		return outputStr;
	}

	/**
	 * Convert time string into date allowing strings matching one of the formats specified
	 * @param timeStr
	 * @return Date object
	 */
	private static Date formatTimeString(String timeStr) {
		String[] formatStrings = {"h:mm a", "hh:mm a", "h:mma", "hh:mma", "H:mm", "HH:mm"};  
		for (String formatString : formatStrings) 
		{ 
			try 
			{ 
				return new SimpleDateFormat(formatString).parse(timeStr); 
			} 
			catch (ParseException e) {} 
		} 
		return null; 
	}
	
	
    /** Find the Earliest Payment Due current invoice.  For Statement Accounts, they will have been rendered already.  So
     * obtain the last renderered invoice.
     * @param account ModelBean The Account ModelBean
     * @return The last rendered invoice
     * @throws Exception when an error occurs
     */
    public static ModelBean getCurrentInvoiceEarliestPaymentDue(ModelBean[] accountList, String feeAccountInvoiceNumber) 
    throws Exception {
    	ArrayList<ModelBean> invoices = new ArrayList<ModelBean>();
    	for( ModelBean accountItem : accountList ) {
	    	//ModelBean accountItem;
			ModelBean currentARInvoice = HPStatementAccountRenderer.getCurrentInvoice(accountItem, feeAccountInvoiceNumber);
    		if( currentARInvoice != null ) {    	
	        	if( !HPStatementAccountRenderer.isFeeAccount(accountItem) ) {
	            	if( StringRenderer.greaterThan(currentARInvoice.gets("OpenAmt"), "0") ) {
	    				//arPayPlan = false;
	    				ModelBean arPayPlan = accountItem.getBean("ARPayPlan");
	                    //boolean arInvoice = false;
	                    ModelBean arInvoice = accountItem.findBeanByFieldValue("ARInvoice","StatusCd","Rendered");
	                    String pastDueDt = "";
	                    //pastDueAmt = "";
	                    if( arInvoice != null ) {
	                    	invoices.add(arInvoice);
	                    	//pastDueDt = arInvoice.gets("DueDt");
	            		} else {
	                    	if( arPayPlan != null ) {
	                    		boolean billedARScheduleInd = false;
	                    		ModelBean[] sortedARSchedule = arPayPlan.getBeansSorted("ARSchedule","Number","Descending");
	                    	   	for( ModelBean arSchedule : sortedARSchedule ) {            
	                    		    if( !billedARScheduleInd && arSchedule.gets("StatusCd").equals("Billed") && arInvoice != null) {  
	                    		    	invoices.add(arInvoice);
	    		                    	//pastDueDt = arSchedule.gets("DueDt");
	                    		        billedARScheduleInd = true;
	                    	   		}
	            				}
	            			}
	        			}
	    				//pastDueDt
	        		}
	    				//No Balance Due
	        	}
	        }
    	}
    	if( invoices.size() > 0 ) {
	    	ModelBean[] invoicesArray = new ModelBean[invoices.size()];              
			for(int j =0;j<invoices.size();j++){
				invoicesArray[j] = invoices.get(j);
			}	    	
	    	invoicesArray = BeanTools.sortBeansBy(invoicesArray, new String[] {"DueDt"}, new String[] {"Ascending"});
	    	if (invoicesArray.length >0) return invoicesArray[0];
	    	else return new ModelBean("ARInvoice");
    	} else return new ModelBean("ARInvoice");
    }		
    
    /** 
     * gets unbilled AR Scheduled bean and returns unbilled Bean of Multiple Account Inquires
     * @param accountInquiries 
     * @return
     */
    public static ModelBean[] getFuturePaymentSchedules(ModelBean[] accountInquiries){
    	ModelBean[] futurePaymentlists = null;
    	try {
    		if(accountInquiries!=null){
    			for (ModelBean accountInquiry : accountInquiries){
        			ModelBean[] arScheduleUnbilled  = accountInquiry.findBeansByFieldValue("ARSchedule", "StatusCd", "Unbilled");
        			if( arScheduleUnbilled != null ){
        				futurePaymentlists = BeanRenderer.addBeansToBeans(futurePaymentlists, arScheduleUnbilled);
        			}
        		}
        		Log.debug("FuturePaymentSchedules ModelBean Array: " + futurePaymentlists);
    		}
    	} catch (Exception ex) {
    		Log.error("Error when adding ModelBeans into Bean", ex);
    	}
    	return futurePaymentlists;
    }
    /** 
     * gets Payment History for Multiple Account Inquires of statement Account
     * @param accountInquiries 
     * @return Payment History beans
     */
    public static ModelBean[] getpaymentHistory(ModelBean[] accountInquiries){
    	ModelBean[] paymentHistorylists = null;
    	try {
    		if(accountInquiries!=null){
    			for (ModelBean accountInquiry : accountInquiries){
        			ModelBean[] paidReceipts  = accountInquiry.findBeansByFieldValue("ARTrans", "TypeCd", "Receipt");
        			if( paidReceipts != null ){
        				paymentHistorylists = BeanRenderer.addBeansToBeans(paymentHistorylists, paidReceipts);
        			}
        		}
        		Log.debug("PaymentHistory ModelBean Array: " + paymentHistorylists);
    		}
    	} catch (Exception ex) {
    		Log.error("Error when adding ModelBeans into Bean", ex);
    	}
    	return paymentHistorylists;
    }
    	

	/**
	 * Method to get Insured Name in the specified format
	 * @param insured
	 * @return
	 * @throws Exception
	 */
	public static String getInsuredName(ModelBean insured) throws Exception{
		String finalIndividualName = "";
		String finalJointName = "";
		String finalConcatString = "";
		ModelBean insuredPartyInfo = insured.getBean("PartyInfo","PartyTypeCd","InsuredParty");
		ModelBean insName = insuredPartyInfo.findBeanByFieldValue("NameInfo","NameTypeCd","InsuredName");
		if(!StringRenderer.equal(insName.gets("OtherGivenName"),"")){
			finalIndividualName = StringRenderer.concat(insName.gets("GivenName"),insName.gets("OtherGivenName"), " ");
			finalIndividualName =	StringRenderer.concat(finalIndividualName,insName.gets("Surname")," ");
		}else{
			finalIndividualName = StringRenderer.concat(insName.gets("GivenName"),insName.gets("Surname"), " ");
		}
		finalIndividualName = StringRenderer.concat(finalIndividualName,insName.gets("SuffixCd")," ");
		finalIndividualName = StringRenderer.trim(finalIndividualName,43);

		if(StringRenderer.equal(insured.gets("EntityTypeCd"), "Joint")){
			ModelBean insuredPartyInfoJoint = insured.findBeanByFieldValue("PartyInfo","PartyTypecd","InsuredPartyJoint");
			ModelBean insuredJointName = insuredPartyInfoJoint.findBeanByFieldValue("NameInfo","NameTypeCd","InsuredNameJoint");
			if(!StringRenderer.equal(insuredJointName.gets("OtherGivenName"),"")){
				finalJointName = StringRenderer.concat(insuredJointName.gets("GivenName"),insuredJointName.gets("OtherGivenName"), " ");
				finalJointName = StringRenderer.concat(finalJointName,insuredJointName.gets("Surname"), " ");
				finalJointName = StringRenderer.concat(finalJointName,insuredJointName.gets("SuffixCd"), " ");
				finalJointName = StringRenderer.trim(finalJointName,43);
			}else{
				finalJointName = StringRenderer.concat(insuredJointName.gets("GivenName"),insuredJointName.gets("Surname"), " ");
				finalJointName = StringRenderer.concat(finalJointName,insuredJointName.gets("SuffixCd"), " ");
			}
			finalConcatString = StringRenderer.concat(finalIndividualName,finalJointName, "\n");
		}else if( StringRenderer.in(insured.gets("EntityTypeCd"), "Trust,Estate")){
			finalConcatString = StringRenderer.concat(finalIndividualName,StringRenderer.trim(insName.gets("CommercialName"),43),"\n");
		}else if(StringRenderer.in(insured.gets("EntityTypeCd"), "Individual")){
			finalConcatString = finalIndividualName;
		}else{
			finalConcatString = StringRenderer.trim(insName.gets("CommercialName"),43);
		}

		return finalConcatString;
	}

	/**
	 * Method to get all applicable product pay plans
	 * @param application
	 * @return
	 */
	public static String PAYPLANCD_LIST = "Automated 12 Pay,Direct Bill Full Pay,Direct Bill 2 Pay,Direct Bill 4 Pay";

	public static ModelBean[] getProductPayPlans(ModelBean application) {
		try{
			boolean renewalInd = false;      
			ModelBean basicPolicy = application.getBean("BasicPolicy");      
			String termCd = basicPolicy.gets("RenewalTermCd");


			ModelBean productSetup = UWProductSetup.getProductSetup(application);
			ModelBean productTerms = productSetup.getBean("ProductTerms");
			ModelBean productTerm = productTerms.getBean("ProductTerm","ShortDesc",termCd);


			if( !basicPolicy.gets("RenewedFromPolicyRef").equals("") || basicPolicy.gets("TransactionCd").equals("Renewal")) {
				renewalInd = true;
			} 
			ArrayList<ModelBean> array = new ArrayList<ModelBean>();

			if( productTerm != null ) {
				ModelBean productPayPlans = productTerm.getBean("ProductPayPlans");
				for(String payplanCd : StringRenderer.split(PAYPLANCD_LIST, ",")){
					ModelBean productPayPlan = productPayPlans.findBeanByFieldValue("ProductPayPlan", "PayPlanCd", payplanCd);
					if(productPayPlan != null){
						String payPlanCd = productPayPlan.gets("PayPlanCd"); 
						if(renewalInd && payPlanCd.contains("Renewal")){
							array.add(productPayPlan);
						}else if(!renewalInd && !payPlanCd.contains("Renewal")){
							array.add(productPayPlan);
						}
					}
				}          
			}

			return (ModelBean[]) array.toArray( new ModelBean[array.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			Log.error("Error", e);
			return null;
		}
	}

	public static void test(Object o, String value){
		Log.debug(value);
	}


	/**
	 * Get the List of Action Names from the Single Policy cycle
	 * 
	 * @throws Exception
	 *             when an Error Occurs
	 * @return MDAOption Array
	 */
	public static MDAOption[] getConversionUtilitiesList(String source) throws Exception {

		ArrayList<MDAOption> list = new ArrayList<MDAOption>();
		MDAOption option1 = new MDAOption();
		option1.setLabel("Application");
		option1.setValue("Application");
		list.add(option1);
		MDAOption[] options = (MDAOption[]) list.toArray(new MDAOption[list.size()]);
		return options;

	}

	public String getDTOBeanFromFile(String filePath) throws Exception {
		File file = new File(PrefsDirectory.getLocation());
		FileInputStream fstream = new FileInputStream(file);
		int len = (int) file.length();
		byte[] buf = new byte[len];
		fstream.read(buf, 0, len);
		fstream.close();
		return new String(buf);
	}

	/**
	 * Helper method to determine if this transaction is a converted renewal transaction
	 * 
	 * @param basicPolicy
	 * @param transactionInfo
	 * @return
	 * @throws Exception
	 */
	public static boolean isConversionPolicyTransaction(ModelBean basicPolicy, ModelBean transactionInfo) throws Exception {
		if (basicPolicy.gets("BusinessSourceCd").equals("Conversion") && !transactionInfo.gets("SourceCd").equals(PM.SOURCE_CODE))
			return true;
		else
			return false;
	}

	/**
	 * Called in function scrubRiskAddress to set old address after updated by
	 * addressValidate
	 * 
	 * @param Address
	 * @return
	 */
	public static void setAddress(ModelBean address, String addr1, String addr2, String postalCode, String county, String city, String state) throws ModelBeanException {

		AddressRenderer addressRenderer = new AddressRenderer();
		address.setValue("Addr1", addr1);
		address.setValue("Addr2", addr2);
		address.setValue("PostalCode", postalCode);
		address.setValue("County", county);
		address.setValue("City", city);
		address.setValue("StateProvCd", state);
		String addrHash = addressRenderer.getAddressHash(address);
		address.setValue("VerificationHash", addrHash);
	}


	/**
	 * For data conversion, scrub all risk addresses, including insured resident
	 * address and location address. call to address scrubbing service only once
	 * and use that result to prefill all other addresses.
	 * 
	 * @param application
	 */
	public static String scrubRiskAddresses(ModelBean application) {

		Log.debug("Starting Address Validation: scrubRiskAddresses() ...");
		try {

			AddressRenderer addressRenderer = new AddressRenderer();

			ModelBean insuredMailingAddr = application.getBean("Insured").getBeanByAlias("InsuredMailingAddr");
			addressRenderer.verifyAddress(insuredMailingAddr);

			addressRenderer.parseAddress(application.getBean("Insured").getBeanByAlias("InsuredLookupAddr"));
			application.getBean("Insured").getBeanByAlias("InsuredLookupAddr").setValue("Addr1", "");
			application.getBean("Insured").getBeanByAlias("InsuredLookupAddr").setValue("Addr2", "");
			application.getBean("Insured").getBeanByAlias("InsuredLookupAddr").setValue("VerificationHash", addressRenderer.getAddressHash(application.getBean("Insured").getBeanByAlias("InsuredLookupAddr"))) ;

			ModelBean insuredBillingAddr = application.getBean("Insured").getBeanByAlias("InsuredBillingAddr");
			if(insuredBillingAddr!=null){
				addressRenderer.verifyAddress(insuredBillingAddr);
			}	
			// Verifying InsuredLookUp Resident Address 
			addressRenderer.parseAddress(application.getBean("Insured").getBeanByAlias("InsuredLookupAddr"), false);
			application.getBean("Insured").getBeanByAlias("InsuredLookupAddr").setValue("Addr1", "");
			application.getBean("Insured").getBeanByAlias("InsuredLookupAddr").setValue("Addr2", "");
			application.getBean("Insured").getBeanByAlias("InsuredLookupAddr").setValue("VerificationHash",
					addressRenderer.getAddressHash(application.getBean("Insured").getBeanByAlias("InsuredLookupAddr")));

			// Verifying Location Address and Risk Address 
			for (ModelBean risk : application.getBean("Line").getBeans("Risk")) {
				ModelBean location = application.findBeanById("Location", risk.gets("LocationRef"));
				Log.debug("location: " + location);
				ModelBean building = risk.getBean("Building");
				ModelBean riskAddr = building.getBeanByAlias("RiskAddr");
				Log.debug("riskAddr: " + riskAddr);
				ModelBean riskLookupAddr = building.getBeanByAlias("RiskLookupAddr");
				Log.debug("riskLookupAddr: " + riskLookupAddr);

				String riskAddrVerify = addressRenderer.verifyAddress(riskAddr);
				String riskAddrLookupParseAddr = addressRenderer.parseAddress(building.getBeanByAlias("RiskLookupAddr"));
				building.getBeanByAlias("RiskLookupAddr").setValue("Addr1", "");
				building.getBeanByAlias("RiskLookupAddr").setValue("Addr2", "");
				building.getBeanByAlias("RiskLookupAddr").setValue("VerificationHash", addressRenderer.getAddressHash(building.getBeanByAlias("RiskLookupAddr"))) ;

				Log.debug("condition 1: " + riskAddrVerify);
				Log.debug("condition 2: " + riskAddrLookupParseAddr);

				if (riskAddrVerify.equals("") && riskAddrLookupParseAddr.equals("")) {

					riskLookupAddr.setValue("Addr1", "");
					riskLookupAddr.setValue("Addr2", "");

					Log.debug("Setting verificationHash for riskAddr");

					riskLookupAddr.setValue("VerificationHash", addressRenderer.getAddressHash(riskLookupAddr));
					Log.debug("RiskLookupAddr Hash: " + riskLookupAddr.gets("VerificationHash"));
					riskAddr.setValue("VerificationHash", addressRenderer.getAddressHash(riskAddr));
					Log.debug("RiskAddr Hash: " + riskAddr.gets("VerificationHash"));

					ModelBean locationLookupAddr = location.getBeanByAlias("LocationLookupAddr");
					BeanTools.copyNonAliasValues(riskLookupAddr, locationLookupAddr);

					BeanTools.copyNonAliasValues(riskAddr, location.getBeanByAlias("LocationAddr"));

					Log.debug("Ending Address Validation");
				} else {
					Log.debug("Address verification error.");
					return "Error from interface: " + riskAddrVerify + " - " + riskAddrLookupParseAddr;
				}
			}
			return "";
		} catch (Exception e) {
			Log.debug("Problem with parsing address");
		}
		return "Error with address validation.";
	}
	public static String  mapRoofType(String roofType) throws Exception {
		String roofTypeCd = "";
		roofType = roofType.replace(",", "");
		if(StringRenderer.in(roofType, "aluminum,metal other than standing seam,metal corrugated,metal ribbed,standing seam copper,standing seam metal,steel,steel with solar panels"))
			roofTypeCd = "Tin (Includes Copper and Steel)";
		else if(StringRenderer.in(roofType, "asbestos shingle replacement"))
			roofTypeCd = "Asbestos";
		else if(StringRenderer.in(roofType, "architectural shingle,asbestos shingle replacement,asphalt shingle,asphalt shingle with solar panels,composition shingle,dimensional asphalt shingle,fiberglass shingle,hail proof shingle,Minera shingle"))
			roofTypeCd = "Asphalt (Composition)";
		else if(StringRenderer.in(roofType, "composition built-up,tar and gravel"))
			roofTypeCd = "Tar and Gravel (Includes built up or rubber membrane)";
		else if(StringRenderer.in(roofType, "Lamarite shingle,red slate,slate,slate graduated,slate patterned,slate synthetic"))
			roofTypeCd = "Slate";
		else if(StringRenderer.in(roofType, "barrel tile,cement fiber,clay tile,concrete tile,glazed tile,Ludowici tile,Spanish tile,tile with solar panels"))
			roofTypeCd = "Tile (Clay and Hendrix)";
		else if(StringRenderer.in(roofType, "cedar shingle/shake,TPO membrane,wood shake,wood shingle"))
			roofTypeCd = "Wood Shingle";
		else
			roofTypeCd = "Other";
		return roofTypeCd;
	}

	public static String mapConstructionType(String constructionType) throws Exception {
		constructionType = constructionType.replace(",", "");
		String consType = "";
		if(StringRenderer.in(constructionType, "A - frame,framing steel,framing wood,framing wood with elevated slab,log,modular wood frame,stick,structural insulated panel,stucco on frame,timber/post & beam"))
			consType = "Frame";
		else if(StringRenderer.in(constructionType, "autoclaved aerated concrete,foam form concrete,masonry block,masonry stone,poured concrete,rammed earth,stucco on masonry"))
			consType = "Masonry (Brick)";
		else
			consType = "Masonry Veneer";
		return consType;
	}

	public static String getAddress1(ModelBean addr) throws Exception {
		return AddressTools.getAddressLine1(addr);

	}
	public static String getAddress2(ModelBean addr) throws Exception {
		return AddressTools.getAddressLine2(addr);

	}

	/**
	 * Boolean check to determine if all the characters in the String are the same 
	 */
	public static boolean allCharactersSame(String s) {
		s = s.replaceAll("-", "");
		int n = s.length();
		for (int i = 1; i < n; i++) {
			if (s.charAt(i) != s.charAt(0))
				return false;
		}
		return true;
	}


	/** Copy lookup address to another address bean
	 * @param fromAddr Lookup Address to be copied from
	 * @param toAddr Adress to be copied to
	 * @throws Exception 
	 */
	public static void copyLookupAddr(ModelBean fromAddr, ModelBean toAddr) throws Exception {

		PartyInfo.convertLookupAddressToStandardAddress(fromAddr,toAddr);

	}

	/**
	 * Determine Loss HistoryFactor for each risk, which is needed for Rating
	 */
	public static void calcLossHistoryFactor(ModelBean risk, StringDate effectiveDt, ModelBean[] lossHistories)
    throws Exception {
    	int paidLossWithRICCnt = 0;
		int unpaidLossWithRICCnt = 0;
		int paidLossPriorCnt = 0;
		int unpaidLossPriorCnt = 0;
		int paidPriorInThreeYears = 0;
		int paidPriorOverThreeYears = 0;
		String lossHistoryFactor = "";
		BigDecimal zero = new BigDecimal("0");
		ModelBean building = risk.getBean("Building");
    	String riskId = risk.gets("id"); 
    	
    	//if there are no loss histories on the application, default the risk's LossHistoryFactor to 0.90
    	if( lossHistories.length == 0 ) {
    		building.setValue("LossHistoryFactor", "0.90");
    	} else {
	    	for( ModelBean lossHistory : lossHistories ) {
	    		String lossRiskId = lossHistory.gets("RiskIdRef");
	    		if(riskId.equals(lossRiskId) ){
	    			StringDate lossDt = lossHistory.getDate("LossDt");
	    			StringDate inceptionDt = null;
	    			int incidentSince = 0 ;
	    			if(lossDt!=null)
	    				inceptionDt = new StringDate(lossHistory.getDate("LossDt").toString());
	    			if(inceptionDt!=null)
	    				incidentSince = DateTools.getAge(inceptionDt, effectiveDt);
	
					if(incidentSince == 3) {
						Double incidentYearsSince = (double) DateRenderer.diffDaysCompare(effectiveDt, inceptionDt)/365;
						if(incidentYearsSince > 3 && incidentYearsSince < 4 ) {
							incidentSince = 4; 
						}
					}
					if( !lossHistory.gets("IgnoreInd").equals("Yes") && incidentSince <= 3) {
						String carrierName = lossHistory.gets("CarrierName"); 
						String paidAmtStr = lossHistory.gets("PaidAmt");
						BigDecimal paidAmt = null;
						if(paidAmtStr!=null && paidAmtStr!="")
							paidAmt = new BigDecimal(paidAmtStr); 
						if(carrierName!=null && carrierName!="" && carrierName.equals("RIC"))  {
							if(paidAmt!=null && paidAmt.compareTo(zero) > 0 )  {
								paidLossWithRICCnt++; 
							} else {
								unpaidLossWithRICCnt++; 
							}
						} else {
							if(paidAmt!=null && paidAmt.compareTo(zero) > 0) {
								paidLossPriorCnt++; 
								if(incidentSince <= 3) {
									paidPriorInThreeYears++;
								} else {
									paidPriorOverThreeYears++;
								}
							} else {
								unpaidLossPriorCnt++; 
							}
						}
					}
	    		}
	    	}
	    	
	    	//No paid losses
			if( (unpaidLossWithRICCnt + unpaidLossPriorCnt) >= 0 ) {
				lossHistoryFactor = "0.90";
			}
	
			//One paid with Other Than RIC within 3 Years
			if(paidLossPriorCnt == 1) {
				if(paidPriorInThreeYears == 1) {
					lossHistoryFactor = "1.10";
				} else if(paidPriorOverThreeYears == 1) {
					lossHistoryFactor = "1.00";
				}
			}
	
			if(paidLossWithRICCnt == 1 && paidLossPriorCnt == 0) {
				lossHistoryFactor = "1.00";
			} else if( (paidLossWithRICCnt + paidLossPriorCnt) == 2) {
				lossHistoryFactor = "1.25";
			} else if((paidLossWithRICCnt + paidLossPriorCnt) == 3) {
				lossHistoryFactor = "1.50";
			} else if((paidLossWithRICCnt + paidLossPriorCnt) >= 4) {
				lossHistoryFactor = "1.90";
			}
			
			//if risk has no loss histories associated to it, default to 0.90
			if(lossHistoryFactor.equals("") ) {
				building.setValue("LossHistoryFactor", "0.90");
			} else {
				building.setValue("LossHistoryFactor", lossHistoryFactor);
			}
    	}
	}	
	
	/**
	 * Determine Loss HistoryFactor for each risk, which is needed for Rating
	 */
	public static void calcWindHailDedLimit(ModelBean risk)
    throws Exception {
		ModelBean building = risk.getBean("Building");
    	String windhailDeductible = building.gets("WindHailDed"); 
		
    	if (!windhailDeductible.equalsIgnoreCase("None") && !windhailDeductible.equals("")) {
    		Double windHailDed = Double.parseDouble(windhailDeductible);
    		Double covALimit = Double.parseDouble(building.gets("CovALimit"));
    		
    		if( windHailDed < 1000 ) {
    			int windHailDedLimit = (int) (covALimit * windHailDed/100); 
    			building.setValue("WindHailDedLimit", windHailDedLimit);
    		} else {
    			building.setValue("WindHailDedLimit", building.gets("WindHailDed"));
    		}
    	} else {
			building.setValue("WindHailDedLimit", "0");
    	}
	}

    /** Returns the HTML for a question input field
     * @return HTML String
     * @param questions ModelBean
     * @param repliesParent ModelBean
     * @param container ModelBean which should form the context for any rendered dynamic content data
     */        
    public String render(ModelBean questions, ModelBean repliesParent, ModelBean container, Boolean disabledInd){
        return QuestionRenderer.render(questions, repliesParent, container, rs, disabledInd);
    }
    
    /**
	 * Helper method to read attribute from e2value-settings.xml to determine if we should attempt
	 * to order the e2value or not.  This is a convenience method to dynamically turn on/off ordering
	 * withouth a full deployment
	 * 
	 * @return
	 * @throws Exception
	 */
	public static boolean doOrderE2Value() throws Exception {
		MDAXmlDoc mdaDoc = (MDAXmlDoc)Store.getModelObject("e2value-settings::null::null");
     	XmlDoc doc = mdaDoc.getDocument();
     	Element root = doc.getRootElement();
     	String shouldOrder = root.getAttributeValue("OrderE2Value");
     	if(shouldOrder.equals("")){
     		return false;
     	} else {
     		boolean doOrder = StringRenderer.isTrue(shouldOrder);
     		return doOrder;
     	}
	}
	
	/**
	 * For close out validation rule, checks to see if all visible questions have been answered
	 * If no value defined for a visible question, then return false. If no question replies bean
	 * has been created, then return false. Otherwise if all visible questions are answered, return
	 * true
	 * @param application
	 * @return
	 */
	public static boolean areAllQuestionsAnswered(ModelBean container) {
		try {
			ModelBean questionReplies = container.getBean("QuestionReplies");
			if (questionReplies == null)
				return false;	

			DynamicString.render(container, "Set up the Rendering String Once");
			ModelBean questions = QuestionRenderer.getQuestions(questionReplies.gets("QuestionSourceMDA"), "Application");
			for (ModelBean question: questions.getAllBeans("Question")) {
				if (Question.isVisible(question, questionReplies)) {
					ModelBean reply = questionReplies.getBean("QuestionReply","Name",question.gets("Name"));
					if ((reply == null || reply.gets("Value").equals("")) && StringTools.isTrue(question.gets("RequiredInd")))
						return false;
				}
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static String getCcDetails(ModelBean Claim) throws Exception {
		
		ModelBean claimPolicyInfo = Claim.getBean("ClaimPolicyInfo") ;
		String policyRef = claimPolicyInfo.gets("policyRef") ;
		PolicyRenderer pr = new PolicyRenderer();
		ModelBean policyBean = pr.getPolicy(policyRef) ;
		ModelBean basicPolicy = policyBean.getBean("BasicPolicy");
		ModelBean[] ais = policyBean.findBeansByFieldValue("AI", "Status", "Active");
		ModelBean provider = ProviderRenderer.getProviderBySystemId(basicPolicy.gets("ProviderRef")) ;
		String ccDetail = ProviderRenderer.getProviderName(provider.gets("ProviderNumber"),provider.gets("ProviderTypeCd")) ;
		for (ModelBean ai : ais){
			String aiName = ai.gets("InterestName");
			ccDetail = StringRenderer.concat(ccDetail, aiName, ",");
		}
		return ccDetail;
	}
	
	public static String getIncidentType(ModelBean Claim) throws ModelBeanException {
		
		String incidentType = StringRenderer.concat(Claim.gets("LossCauseCd"), Claim.gets("SubLossCauseCd")," and ");
		return incidentType ;
		
	}
	public static String getSuspectList(ModelBean Claim) throws Exception{
		ModelBean claimPolicyInfo = Claim.getBean("ClaimPolicyInfo") ;
		String policyRef = claimPolicyInfo.gets("policyRef") ;
		PolicyRenderer pr = new PolicyRenderer();
		ModelBean policyBean = pr.getPolicy(policyRef) ;
		ModelBean insName = policyBean.findBeanByFieldValue("NameInfo","NameTypeCd","InsuredName") ;
		String insuredname = StringRenderer.buildName(insName.gets("CommercialName"), "", 1, 50) ;
		insuredname = StringRenderer.replaceAll(insuredname,"," , "" );
		String suspectDeatail = StringRenderer.concat(insuredname, "Suspect Name", ",");
		return suspectDeatail;
	}
	/**
	 * This method gets called when the entity is "Business" and checks for
	 * change in Insured Contact Name and Address
	 * @param sourceId
	 * @param insuredPartyinfo
	 * @param application
	 * @param dataReportRequestName
	 * @return boolean true if any change has occured,else false
	 * @throws Exception if an Unexpected Error Occurs
	 */
	public boolean validateChangeinBusinessNameAddress(String sourceId, ModelBean insuredPartyinfo,
			ModelBean application, String dataReportRequestName) {
		Boolean isChanged = Boolean.FALSE;
		ModelBean lastDataReportRequest;
		try {
			lastDataReportRequest = UWDataReportRenderer.getLatestDataReportRequest(application, sourceId,
					dataReportRequestName, true);
			if (lastDataReportRequest != null) {
				ModelBean cpNCFSearchCriteria = lastDataReportRequest.getBean("CPNCFSearchCriteria");
				/******** Last Ordered Details *********/
				ModelBean cpSearchSubject = cpNCFSearchCriteria.findBeanByFieldValue("CPSearchSubject",
						"SearchSubjectTypeCd", "Subject");
				ModelBean insured = application.getBean("Insured");
				ModelBean insuredInsuranceScore = insured.getBeanByAlias("InsuredInsuranceScore");
				String ncfStatusCd = insuredInsuranceScore.gets("StatusCd");
				String insuredNameLastOrderedCon = getConcatenatedinsuredName(cpSearchSubject);
				String insuredAddressLastOrdered = getlastOrderedAddress(cpNCFSearchCriteria,"No");
				//DOB
				StringDate insuredbirthDtLastOrderedTemp = cpSearchSubject.getDate("BirthDt");
				String insuredbirthDtLastOrdered = "" + insuredbirthDtLastOrderedTemp;
				String insuredSsnLastOrdered = getHypenRemovedString(cpSearchSubject.gets("SSN"));
				ModelBean insuredTaxInfo = insuredPartyinfo.getBean("TaxInfo", "TaxTypeCd", "InsuredContactTaxInfo");
				String insuredSsn = getHypenRemovedString(insuredTaxInfo.gets("SSN"));
				//DOB
				/******** Last Ordered Details *********/

				/******** Policy Details **************/
				ModelBean insuredContactNameInfo = insuredPartyinfo.getBean("NameInfo", "NameTypeCd",
						"InsuredContactName");
				String insuredNameCon = getConcatenatedinsuredName(insuredContactNameInfo);
				ModelBean insuredContactAddr = insuredPartyinfo.getBean("Addr", "AddrTypeCd", "InsuredContactAddr");
				String insuredContactAddrCon = getConcatenatedInsuredAddress(insuredContactAddr);
				//DOB
				ModelBean insuredPersonal = insuredPartyinfo.findBeanByFieldValue("PersonInfo", "PersonTypeCd",
						"InsuredContactPersonal");
				StringDate insuredbirthDtTemp = insuredPersonal.getDate("BirthDt");
				String insuredbirthDt = "" + insuredbirthDtTemp;
				//DOB
				/******** Policy Details *********/

				/******** Checking for change in Contact Name *********/
				if ((!StringUtils.isEmpty(insuredNameLastOrderedCon)
						&& !StringUtils.isEmpty(insuredNameCon))
						&& !insuredNameLastOrderedCon.equalsIgnoreCase(insuredNameCon)) {
					isChanged = Boolean.TRUE;
				}
				/******** Checking for change in Contact Address *********/
				if(StringTools.in(ncfStatusCd, "No Hit,No Score")){
					if ((!StringUtils.isEmpty(insuredAddressLastOrdered)
							&& !StringUtils.isEmpty(insuredContactAddrCon))
							&& !insuredAddressLastOrdered.equalsIgnoreCase(insuredContactAddrCon)) {
						isChanged = Boolean.TRUE;
					}
				}
				//check for DOB changed start
				if ((!StringUtils.isEmpty(insuredbirthDtLastOrdered)
						&& !StringUtils.isEmpty(insuredbirthDt))
						&& !insuredbirthDtLastOrdered.equalsIgnoreCase(insuredbirthDt)) {
					isChanged = Boolean.TRUE;
				}
				//check for DOB changed end
				//ssn change start
				if (!insuredSsnLastOrdered.equalsIgnoreCase(insuredSsn)) {
					isChanged = Boolean.TRUE;
				}
				
				//ssn change end
			}
		} catch (Exception e) {
			Log.error("Exception occurred in method validateChangeinBusinessNameAddress:" + e);
		}
		return isChanged;
	}

	/**
	 * This method gets called when the entity is not "Business" and when the
	 * Insurance Score is equal to Nohit or Noscore and checks for change in
	 * Insured Name and Address
	 * @param sourceId
	 * @param entityTypeCd
	 * @param insuredPartyinfo
	 * @param insuredPartyJointinfo
	 * @param application
	 * @param dataReportRequestName
	 * @return boolean true if any change has occured,else false
	 * @throws Exception if an Unexpected Error Occurs
	 */
	public Boolean validateChangeinOtherEntityNameAddress(String sourceId, String entityTypeCd,
			ModelBean insuredPartyinfo, ModelBean insuredPartyJointinfo, ModelBean application,
			String dataReportRequestName) throws Exception {
		Boolean isChanged = Boolean.FALSE;
		String insuredPriorAddrCon = "";
		String previousAddInd = application.getBean("Insured").gets("Movedin3years");
		/******** Last Ordered Details *********/
		ModelBean lastDataReportRequest = UWDataReportRenderer.getLatestDataReportRequest(application, sourceId,
				dataReportRequestName, true);
		if (lastDataReportRequest != null) {
			ModelBean cpNCFSearchCriteria = lastDataReportRequest.getBean("CPNCFSearchCriteria");
			ModelBean cpSearchSubject = cpNCFSearchCriteria.findBeanByFieldValue("CPSearchSubject",
					"SearchSubjectTypeCd", "Subject");
			String insuredNameLastOrderCon = getConcatenatedinsuredName(cpSearchSubject);
			String insuredLastOrderAddrCon = "";
			insuredLastOrderAddrCon = getlastOrderedAddress(cpNCFSearchCriteria,previousAddInd);
			
			//DOB ssn
			StringDate insuredbirthDtLastOrderedTemp = cpSearchSubject.getDate("BirthDt");
			String insuredbirthDtLastOrdered = "" + insuredbirthDtLastOrderedTemp;
			String insuredSsnLastOrdered = getHypenRemovedString(cpSearchSubject.gets("SSN"));
			//DOB ssn
			
			/******** Last Ordered Details *********/

			/******** Policy Details *********/
			ModelBean insuredNameInfo = insuredPartyinfo.getBean("NameInfo", "NameTypeCd", "InsuredName");
			String insuredNameCon = getConcatenatedinsuredName(insuredNameInfo);
			ModelBean insuredLookupAddr = insuredPartyinfo.getBean("Addr", "AddrTypeCd", "InsuredLookupAddr");
			String insuredLookupAddrCon = getConcatenatedInsuredAddress(insuredLookupAddr);
			
			//Prior Address
			if(previousAddInd.equals("Yes")){
				ModelBean insuredPriorLookupAddr = insuredPartyinfo.getBean("Addr", "AddrTypeCd", "InsuredPriorLookupAddr");
				insuredPriorAddrCon = getConcatenatedInsuredAddress(insuredPriorLookupAddr);
			}
			//Prior Address
			ModelBean insuredTaxInfo = insuredPartyinfo.getBean("TaxInfo", "TaxTypeCd", "InsuredTaxInfo");
			String insuredSsn = getHypenRemovedString(insuredTaxInfo.gets("SSN"));
			//DOB 
			ModelBean insuredPersonal = insuredPartyinfo.findBeanByFieldValue("PersonInfo", "PersonTypeCd",
					"InsuredPersonal");
			StringDate insuredbirthDtTemp = insuredPersonal.getDate("BirthDt");
			String insuredbirthDt = "" + insuredbirthDtTemp;
			//DOB 
			/******** Policy Details *********/

			/******** Joint Entity Name Change check *********/
			if (!StringUtils.isEmpty(entityTypeCd) && entityTypeCd.equalsIgnoreCase("Joint")) {
				/******** Last Ordered Details *********/
				String concatenatedInsuredSpouseNameLastOrder = "";
				String insuredSpouseNameLastOrderCon = "";
				ModelBean cpSearchSpouse = cpNCFSearchCriteria.findBeanByFieldValue("CPSearchSubject",
						"SearchSubjectTypeCd", "Spouse");
				if (cpSearchSpouse != null) {
					insuredSpouseNameLastOrderCon = getConcatenatedinsuredName(cpSearchSpouse);
				}
				concatenatedInsuredSpouseNameLastOrder = insuredNameLastOrderCon + insuredSpouseNameLastOrderCon;
				/******** Last Ordered Details *********/
				/******** Policy Details **************/
				ModelBean insuredNameJointInfo = insuredPartyJointinfo.getBean("NameInfo", "NameTypeCd",
						"InsuredNameJoint");
				String insuredSpouseNameCon = getConcatenatedinsuredName(insuredNameJointInfo);
				String concatenatedInsuredSpouseName = insuredNameCon + insuredSpouseNameCon;
				/******** Checking for change in Insured Joint Name *********/
				if ((!StringUtils.isEmpty(concatenatedInsuredSpouseNameLastOrder)
						&& !StringUtils.isEmpty(concatenatedInsuredSpouseName))
						&& !concatenatedInsuredSpouseNameLastOrder.equalsIgnoreCase(concatenatedInsuredSpouseName)) {
					isChanged = Boolean.TRUE;
				}
				
				String insuredSpouseBirthDtLastOrderedConcat = "";
				String insuredSpouseSsnLastOrdered = "";
				String insuredSpousebirthDtLastOrdered = "";
				if (cpSearchSpouse != null) {
					StringDate insuredSpousebirthDtLastOrderedTemp = cpSearchSpouse.getDate("BirthDt");
					insuredSpousebirthDtLastOrdered = "" + insuredSpousebirthDtLastOrderedTemp;
					insuredSpouseSsnLastOrdered = getHypenRemovedString(cpSearchSpouse.gets("SSN"));
				}
				insuredSpouseBirthDtLastOrderedConcat = insuredbirthDtLastOrdered + insuredSpousebirthDtLastOrdered;// 1950022219650923
				String insuredSpouseSsnLastOrderedConcat = insuredSsnLastOrdered + insuredSpouseSsnLastOrdered;
				/******** Last Ordered Details *********/
				/******** Policy Details *********/
				ModelBean insuredPersonalJoint = insuredPartyJointinfo.findBeanByFieldValue("PersonInfo",
						"PersonTypeCd", "InsuredPersonalJoint");
				StringDate insuredJointbirthDtTemp = insuredPersonalJoint.getDate("BirthDt");
				String insuredJointbirthDt = "" + insuredJointbirthDtTemp;
				String insuredBirthDtConcat = insuredbirthDt + insuredJointbirthDt;
				ModelBean insuredTaxInfoJoint = insuredPartyJointinfo.findBeanByFieldValue("TaxInfo", "TaxTypeCd",
						"InsuredTaxInfoJoint");
				String insuredJointSSN = getHypenRemovedString(insuredTaxInfoJoint.gets("SSN"));
				String insuredjointConcat = insuredSsn + insuredJointSSN;
				/******** Policy Details *********/
				if ((!StringUtils.isEmpty(insuredSpouseBirthDtLastOrderedConcat)
						&& !StringUtils.isEmpty(insuredBirthDtConcat))
						&& !insuredSpouseBirthDtLastOrderedConcat.equalsIgnoreCase(insuredBirthDtConcat)) {
					isChanged = Boolean.TRUE;
				}
				if (!insuredSpouseSsnLastOrderedConcat.equalsIgnoreCase(insuredjointConcat)) {
					isChanged = Boolean.TRUE;
				}
				
			}
			/******** Checking for change in Prior Address *********/
			
			if(application.getBean("Insured").gets("Movedin3years").equals("Yes")){
				String ncfAddrConcat = insuredLookupAddrCon+insuredPriorAddrCon;
				if (!StringUtils.isEmpty(insuredPriorAddrCon)
						&& !insuredLastOrderAddrCon.equalsIgnoreCase(ncfAddrConcat)) {
					isChanged = Boolean.TRUE;
				}
			}else{
				/******** Checking for change in Insured Address *********/
				if ((!StringUtils.isEmpty(insuredLastOrderAddrCon)
						&& !StringUtils.isEmpty(insuredLookupAddrCon))
						&& !insuredLastOrderAddrCon.equalsIgnoreCase(insuredLookupAddrCon)) {
					isChanged = Boolean.TRUE;
				}
			}
			/******* Checking for change in Prior Address *********/
			/******** Joint Entity Name Change check *********/

			
			/******** Checking for change in Insured Name *********/
			if (!StringUtils.isEmpty(entityTypeCd) && !entityTypeCd.equalsIgnoreCase("Joint")) {
				if ((!StringUtils.isEmpty(insuredNameLastOrderCon) && !StringUtils.isEmpty(insuredNameCon))
						&& !insuredNameLastOrderCon.equalsIgnoreCase(insuredNameCon)) {
					isChanged = Boolean.TRUE;
				}
				//check for DOB changed start
				if ((!StringUtils.isEmpty(insuredbirthDtLastOrdered)
						&& !StringUtils.isEmpty(insuredbirthDt))
						&& !insuredbirthDtLastOrdered.equalsIgnoreCase(insuredbirthDt)) {
					isChanged = Boolean.TRUE;
				}
				//check for DOB changed end
				//ssn change start
				if (!insuredSsnLastOrdered.equalsIgnoreCase(insuredSsn)) {
					isChanged = Boolean.TRUE;
				}
				//ssn end
			}
		}
		return isChanged;
	}

	/**
	 * This method gets called when the entity is not "Business" and when the
	 * Insurance Score is not equal to Nohit or Noscore and checks for change in
	 * Date of Birth or SSN fields
	 * @param sourceId
	 * @param entityTypeCd
	 * @param insuredPartyinfo
	 * @param insuredPartyJointinfo
	 * @param application
	 * @param dataReportRequestName
	 * @return boolean true if any change has occured,else false
	 * @throws Exception if an Unexpected Error Occurs
	 */
	public Boolean validateChangeinOtherEntityDOBSSN(String sourceId, String entityTypeCd, ModelBean insuredPartyinfo,
			ModelBean insuredPartyJointinfo, ModelBean application, String dataReportRequestName) throws Exception {
		Boolean isChanged = Boolean.FALSE;
		/******** Last Ordered Details *********/
		ModelBean lastDataReportRequest = UWDataReportRenderer.getLatestDataReportRequest(application, sourceId,
				dataReportRequestName, true);
		if (lastDataReportRequest != null) {
			ModelBean cpNCFSearchCriteria = lastDataReportRequest.getBean("CPNCFSearchCriteria");
			ModelBean cpSearchSubject = cpNCFSearchCriteria.findBeanByFieldValue("CPSearchSubject",
					"SearchSubjectTypeCd", "Subject");
			StringDate insuredbirthDtLastOrderedTemp = cpSearchSubject.getDate("BirthDt");
			String insuredbirthDtLastOrdered = "" + insuredbirthDtLastOrderedTemp;
			String insuredSsnLastOrdered = getHypenRemovedString(cpSearchSubject.gets("SSN"));
			/******** Last Ordered Details *********/
			/******** Policy Details ***************/
			ModelBean insuredPersonal = insuredPartyinfo.findBeanByFieldValue("PersonInfo", "PersonTypeCd",
					"InsuredPersonal");
			StringDate insuredbirthDtTemp = insuredPersonal.getDate("BirthDt");
			String insuredbirthDt = "" + insuredbirthDtTemp;
			ModelBean insuredTaxInfo = insuredPartyinfo.getBean("TaxInfo", "TaxTypeCd", "InsuredTaxInfo");
			String insuredSsn = getHypenRemovedString(insuredTaxInfo.gets("SSN"));// 666-45-0168
			/******** Policy Details ***************/
			/********
			 * Checking for change in Insured Birthdate,SSN For Joint Entity
			 *********/
			if (!StringUtils.isEmpty(entityTypeCd) && entityTypeCd.equalsIgnoreCase("Joint")) {
				/******** Last Ordered Details *********/
				String insuredSpouseBirthDtLastOrderedConcat = "";
				String insuredSpouseSsnLastOrdered = "";
				String insuredSpousebirthDtLastOrdered = "";
				ModelBean cpSearchSpouse = cpNCFSearchCriteria.findBeanByFieldValue("CPSearchSubject",
						"SearchSubjectTypeCd", "Spouse");
				if (cpSearchSpouse != null) {
					StringDate insuredSpousebirthDtLastOrderedTemp = cpSearchSpouse.getDate("BirthDt");
					insuredSpousebirthDtLastOrdered = "" + insuredSpousebirthDtLastOrderedTemp;
					insuredSpouseSsnLastOrdered = getHypenRemovedString(cpSearchSpouse.gets("SSN"));
				}
				insuredSpouseBirthDtLastOrderedConcat = insuredbirthDtLastOrdered + insuredSpousebirthDtLastOrdered;// 1950022219650923
				String insuredSpouseSsnLastOrderedConcat = insuredSsnLastOrdered + insuredSpouseSsnLastOrdered;
				/******** Last Ordered Details *********/
				/******** Policy Details *********/
				ModelBean insuredPersonalJoint = insuredPartyJointinfo.findBeanByFieldValue("PersonInfo",
						"PersonTypeCd", "InsuredPersonalJoint");
				StringDate insuredJointbirthDtTemp = insuredPersonalJoint.getDate("BirthDt");
				String insuredJointbirthDt = "" + insuredJointbirthDtTemp;
				String insuredBirthDtConcat = insuredbirthDt + insuredJointbirthDt;
				ModelBean insuredTaxInfoJoint = insuredPartyJointinfo.findBeanByFieldValue("TaxInfo", "TaxTypeCd",
						"InsuredTaxInfoJoint");
				String insuredJointSSN = getHypenRemovedString(insuredTaxInfoJoint.gets("SSN"));
				String insuredjointConcat = insuredSsn + insuredJointSSN;
				/******** Policy Details *********/
				if ((!StringUtils.isEmpty(insuredSpouseBirthDtLastOrderedConcat)
						&& !StringUtils.isEmpty(insuredBirthDtConcat))
						&& !insuredSpouseBirthDtLastOrderedConcat.equalsIgnoreCase(insuredBirthDtConcat)) {
					isChanged = Boolean.TRUE;
				}
				if (!insuredSpouseSsnLastOrderedConcat.equalsIgnoreCase(insuredjointConcat)) {
					isChanged = Boolean.TRUE;
				}
			}
			/******** Checking for change in Insured Birthdate,SSN *********/
			if (!StringUtils.isEmpty(entityTypeCd) && !entityTypeCd.equalsIgnoreCase("Joint")) {
				if ((!StringUtils.isEmpty(insuredbirthDtLastOrdered)
						&& !StringUtils.isEmpty(insuredbirthDt))
						&& !insuredbirthDtLastOrdered.equalsIgnoreCase(insuredbirthDt)) {
					isChanged = Boolean.TRUE;
				}
				if (!insuredSsnLastOrdered.equalsIgnoreCase(insuredSsn)) {
					isChanged = Boolean.TRUE;
				}
				
			}
			
		}
		return isChanged;
	}

	/**
	 * Get Concatenated Last Ordered Insured Address
	 * @param insuredOrderedAddr ModelBean
	 * @return finalInsuredOrderedAddrString Concatenated Last Ordered Insured Address String
	 * @throws ModelBeanException if an Unexpected Error Occurs
	 */
	private static String getlastOrderedAddress(ModelBean insuredOrderedAddr,String previousInd) throws ModelBeanException {
		String finalInsuredOrderedAddrString = "";
		String mailingAddrString = "";
		String previousAddrString = "";
		String primaryNumber="";
		String secondaryNumber = "";
		String streetName = "";
		String city = "";
		String stateProvCd ="";
		String postalCode1 = "";
		String postalCode = "";
		String prevPrimaryNumber="";
		String prevSecondaryNumber = "";
		String prevStreetName = "";
		String prevCity = "";
		String prevStateProvCd ="";
		String prevPostalCode1 = "";
		String prevPostalCode = "";
		if(previousInd.equalsIgnoreCase("Yes")){
			prevPrimaryNumber = insuredOrderedAddr.gets("FormerAddressPrimaryNumber");
			prevSecondaryNumber = insuredOrderedAddr.gets("FormerAddressSecondaryNumber");
			prevStreetName = insuredOrderedAddr.gets("FormerAddressStreetName");
			prevCity = insuredOrderedAddr.gets("FormerAddressCity");
			prevStateProvCd = insuredOrderedAddr.gets("FormerAddressStateProvCd");
			prevPostalCode1 = insuredOrderedAddr.gets("FormerAddressPostalCode");
			prevPostalCode = prevPostalCode1.replace("-", "");
		}//else{
			primaryNumber = insuredOrderedAddr.gets("AddressPrimaryNumber");
			secondaryNumber = insuredOrderedAddr.gets("AddressSecondaryNumber");
			streetName = insuredOrderedAddr.gets("AddressStreetName");
			city = insuredOrderedAddr.gets("AddressCity");
			stateProvCd = insuredOrderedAddr.gets("AddressStateProvCd");
			postalCode1 = insuredOrderedAddr.gets("AddressPostalCode");
			postalCode = postalCode1.replace("-", "");
			
		//}
		mailingAddrString = primaryNumber + secondaryNumber + streetName + city + stateProvCd + postalCode;	
		if(previousInd.equalsIgnoreCase("Yes")){
			previousAddrString = prevPrimaryNumber + prevSecondaryNumber + prevStreetName + prevCity + prevStateProvCd + prevPostalCode;
			finalInsuredOrderedAddrString = mailingAddrString+previousAddrString;
		}else{
			finalInsuredOrderedAddrString = mailingAddrString;
		}
		
		finalInsuredOrderedAddrString = getSpaceRemovedString(finalInsuredOrderedAddrString);
		
		return finalInsuredOrderedAddrString;
	}

	/**
	 * Get Concatenated Insured Name
	 * @param insuredNameInfo ModelBean
	 * @return finalInsuredNameString Concatenated Insured Name String
	 * @throws ModelBeanException if an Unexpected Error Occurs
	 */
	private static String getConcatenatedinsuredName(ModelBean insuredNameInfo) throws ModelBeanException {
		String finalInsuredNameString = "";
		String givenName = insuredNameInfo.gets("GivenName");
		String surname = insuredNameInfo.gets("Surname");
		String otherGivenName = insuredNameInfo.gets("OtherGivenName");
		String suffixCd = insuredNameInfo.gets("SuffixCd");
		finalInsuredNameString = givenName + surname + otherGivenName + suffixCd;
		finalInsuredNameString = getSpaceRemovedString(finalInsuredNameString);
		return finalInsuredNameString;
	}

	/**
	 * Get Concatenated Insured Address
	 * @param insuredAddr ModelBean
	 * @return finalInsuredAddrString Concatenated Insured Address String
	 * @throws ModelBeanException if an Unexpected Error Occurs
	 */
	private static String getConcatenatedInsuredAddress(ModelBean insuredAddr) throws ModelBeanException {
		String finalInsuredAddrString = "";
		String insuredPrimaryNumber = insuredAddr.gets("PrimaryNumber");
		String insuredSecondaryNumber = insuredAddr.gets("SecondaryNumber");
		String insuredStreetNameCon = StringRenderer.concat(insuredAddr.gets("PreDirectional"),
				insuredAddr.gets("StreetName"), " ");
		String suffix = insuredAddr.gets("Suffix");
		String insuredStreetName = StringRenderer.concat(insuredStreetNameCon, suffix, " ");
		String postDirectional = insuredAddr.gets("PostDirectional");
		String insuredCity = insuredAddr.gets("City");
		String insuredStateProvCd = insuredAddr.gets("StateProvCd");
		String insuredPostalCode1 = insuredAddr.gets("PostalCode");
		String insuredPostalCode = insuredPostalCode1.replace("-", "");
		finalInsuredAddrString = insuredPrimaryNumber + insuredSecondaryNumber + insuredStreetName + postDirectional
				+ insuredCity + insuredStateProvCd + insuredPostalCode;
		finalInsuredAddrString = getSpaceRemovedString(finalInsuredAddrString);
		return finalInsuredAddrString;
	}

	/**
	 * Get Single and Double Space Removed String
	 * @param inputString Concatenated String
	 * @return outputString Single and Double Space removed String
	 * @throws ModelBeanException if an Unexpected Error Occurs
	 */
	private static String getSpaceRemovedString(String inputString) throws ModelBeanException {
		String outputString = "";
		StringBuffer cleanString = new StringBuffer(1024);
		StringBuffer str = new StringBuffer(inputString);
		for (int i = 0; i < str.length(); i++) {
			if (Character.isLetterOrDigit(str.charAt(i)) || Character.isSpaceChar(str.charAt(i))) {
				cleanString.append(Character.toUpperCase(str.charAt(i)));
			}
		}
		inputString = cleanString.toString();
		// Replace all double spaces with a single space
		while (inputString.indexOf("  ") > -1) {
			inputString = inputString.replaceAll("  ", " ");
		}
		// replace all single spaces
		while (inputString.indexOf(" ") > -1) {
			inputString = inputString.replaceAll(" ", "");
		}
		// Trim
		outputString = inputString.trim();
		return outputString;

	}

	/**
	 * This method is used to replace the hypen with empty string
	 * @param inputString
	 * @return outputString 
	 * @throws ModelBeanException if an Unexpected Error Occurs
	 */
	private static String getHypenRemovedString(String inputString) throws ModelBeanException {
		String outputString = "";
		if (!StringUtils.isEmpty(inputString)) {
			outputString = inputString.replace("-", "");
		}
		return outputString;
	}

	/**
	 * This method gets called when the entity is not "Business" and when the
	 * Insurance Score is not equal to Nohit or Noscore and checks for newly
	 * added Insured Name via Joint Entity Type
	 * @param sourceId
	 * @param entityTypeCd
	 * @param insuredPartyinfo
	 * @param insuredPartyJointinfo
	 * @param application
	 * @param dataReportRequestName
	 * @return boolean true if any change has occured,else false
	 * @throws Exception if an Unexpected Error Occurs
	 */
	public Boolean validateAdditioninInsuredName(String sourceId, String entityTypeCd, ModelBean insuredPartyinfo,
			ModelBean insuredPartyJointinfo, ModelBean application, String dataReportRequestName) throws Exception {
		Boolean isChanged = Boolean.FALSE;
		/******** Joint Entity Name Change check *********/
		if (!StringUtils.isEmpty(entityTypeCd) && entityTypeCd.equalsIgnoreCase("Joint")) {
			/******** Last Ordered Details *********/
			ModelBean lastDataReportRequest = UWDataReportRenderer.getLatestDataReportRequest(application, sourceId,
					dataReportRequestName, true);
			if (lastDataReportRequest != null) {
				ModelBean cpNCFSearchCriteria = lastDataReportRequest.getBean("CPNCFSearchCriteria");
				ModelBean cpSearchSubject = cpNCFSearchCriteria.findBeanByFieldValue("CPSearchSubject",
						"SearchSubjectTypeCd", "Subject");
				String insuredNameLastOrderCon = getConcatenatedinsuredName(cpSearchSubject);
				/******** Last Ordered Details *********/

				/******** Policy Details *********/
				ModelBean insuredNameInfo = insuredPartyinfo.getBean("NameInfo", "NameTypeCd", "InsuredName");
				String insuredNameCon = getConcatenatedinsuredName(insuredNameInfo);
				/******** Policy Details *********/

				/******** Last Ordered Details *********/
				String concatenatedInsuredSpouseNameLastOrder = "";
				String insuredSpouseNameLastOrderCon = "";
				ModelBean cpSearchSpouse = cpNCFSearchCriteria.findBeanByFieldValue("CPSearchSubject",
						"SearchSubjectTypeCd", "Spouse");
				if (cpSearchSpouse != null) {
					insuredSpouseNameLastOrderCon = getConcatenatedinsuredName(cpSearchSpouse);
				}
				concatenatedInsuredSpouseNameLastOrder = insuredNameLastOrderCon + insuredSpouseNameLastOrderCon;
				/******** Last Ordered Details *********/
				/******** Policy Details **************/
				ModelBean insuredNameJointInfo = insuredPartyJointinfo.getBean("NameInfo", "NameTypeCd",
						"InsuredNameJoint");
				String insuredSpouseNameCon = getConcatenatedinsuredName(insuredNameJointInfo);
				String concatenatedInsuredSpouseName = insuredNameCon + insuredSpouseNameCon;
				/******** Checking for change in Insured Joint Name *********/
				if ((!StringUtils.isEmpty(concatenatedInsuredSpouseNameLastOrder)
						&& !StringUtils.isEmpty(concatenatedInsuredSpouseName))
						&& !concatenatedInsuredSpouseNameLastOrder.equalsIgnoreCase(concatenatedInsuredSpouseName)) {
					isChanged = Boolean.TRUE;
				}
			}
			/******** Joint Entity Name Change check *********/
		}
		return isChanged;
	}

	/**
	 * This method is used to check if the last insurance score data call was > 3 year from
	 * the renewal term effective date
	 * @param sourceId
	 * @param application
	 * @param dataReportRequestName
	 * @param effDt
	 * @return boolean true or false
	 */
	public boolean getInsuranceScoreAge(String sourceId, ModelBean application,String effDt) {
		boolean dateCompareCheckInd = false;
		try {
			ModelBean lastOrderedDataReportRequest = UWDataReportRenderer.getLatestDataReportRequest(application,
					sourceId, "XApplicationDataReport0001,XPolicyDataReport0001,XQuoteDataReport0001,XTaskDataReport0001", true);
			if (lastOrderedDataReportRequest != null) {
				String lastDtStr = lastOrderedDataReportRequest.gets("AppliedDt");
				StringDate lastDt = DateRenderer.getStringDate(lastDtStr);
				StringDate effectiveDate = DateRenderer.getStringDate(effDt);
					if (DateRenderer.getAge(lastDt,effectiveDate) > 3) {
						dateCompareCheckInd = true;
					}
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return dateCompareCheckInd;
	}

	
	public String getStructureDamage(ModelBean Claim) {
		String structuredamagetype = "";
		try {
			ArrayList<String> structuredamages = new ArrayList<String>();
			if(Claim.gets("StructDamageMainDwellingInd").equals("Yes")){
				structuredamages.add("Main Dwelling");
			}
			if(Claim.gets("StructDamageStructureInd").equals("Yes")){
				structuredamages.add("Structure (Garage/Farm)");
			}
			if(Claim.gets("StructDamagePersonalPropInd").equals("Yes")){
				structuredamages.add("Personal Property");
			}
			if(Claim.gets("StructDamageOtherInd").equals("Yes")){
				structuredamages.add("Other (Fence, Pool)");
			}
			for(String structuredamage : structuredamages)
				structuredamagetype = StringTools.concat(structuredamagetype, structuredamage, ", ");
		
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Loss Notice Damage type..........");
			return null;
		}
		return structuredamagetype;
	}
	
	public String getFireDamage(ModelBean Claim) {
		String Damagetype = "";
		try {
			ArrayList<String> damages = new ArrayList<String>();
			if(Claim.gets("FireDamageChimneyInd").equals("Yes")){
				damages.add("Chimney");
			}
			if(Claim.gets("FireDamageNonTotalLossInd").equals("Yes")){
				damages.add("Non-Total Loss Fire");
			}
			if(Claim.gets("FireDamageSmokeInd").equals("Yes")){
				damages.add("Smoke");
			}
			if(Claim.gets("FireDamageTotalLossInd").equals("Yes")){
				damages.add("Total Loss Fire");
			}
			for(String damage : damages)
				Damagetype = StringTools.concat(Damagetype, damage, ", ");
		
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Loss Notice Damage type..........");
			return null;

		}
		return Damagetype;
	}
	
	public String getWaterDamage(ModelBean Claim) {
		String Damagetype = "";
		try {
			ArrayList<String> damages = new ArrayList<String>();
			if(Claim.gets("WaterDamageFloodWaterInd").equals("Yes")){
				damages.add("Flood Water");
			}
			if(Claim.gets("WaterDamageInteriorInd").equals("Yes")){
				damages.add("Interior");
			}
			if(Claim.gets("WaterDamageMajorMoldInd").equals("Yes")){
				damages.add("Major/Mold");
			}
			for(String damage : damages)
				Damagetype = StringTools.concat(Damagetype, damage, ", ");
		
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Loss Notice Damage type..........");
			return null;

		}
		return Damagetype;
	}
	public String getTreeDamage(ModelBean Claim) {
		String Damagetype = "";
		try {
			ArrayList<String> damages = new ArrayList<String>();
			if(Claim.gets("TreeDamageGroundNoDamageInd").equals("Yes")){
				damages.add("Ground (No Damage)");
			}
			if(Claim.gets("TreeDamageGroundDrivewayInd").equals("Yes")){
				damages.add("Ground (Driveway Obstruction)");
			}
			if(Claim.gets("TreeDamageMainDwellingInd").equals("Yes")){
				damages.add("Main Dwelling");
			}
			if(Claim.gets("TreeDamageStructureInd").equals("Yes")){
				damages.add("Structure (Garage/Farm)");
			}
			if(Claim.gets("TreeDamageOtherInd").equals("Yes")){
				damages.add("Other (Fence, Personal Property)");
			}
			for(String damage : damages)
				Damagetype = StringTools.concat(Damagetype, damage, ", ");
		
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Loss Notice Damage type..........");
			return null;

		}
		return Damagetype;
	}

	public String getLightningDamage(ModelBean Claim) {
		String Damagetype = "";
		try {
			ArrayList<String> damages = new ArrayList<String>();
			if(Claim.gets("LightningDamageMainDwellingInd").equals("Yes")){
				damages.add("Main Dwelling");
			}
			if(Claim.gets("LightningDamageStructureInd").equals("Yes")){
				damages.add("Structure (Garage/Farm)");
			}
			if(Claim.gets("LightningDamageOtherInd").equals("Yes")){
				damages.add("Other (Fence, Personal Property)");
			}
			for(String damage : damages)
				Damagetype = StringTools.concat(Damagetype, damage, ", ");
		
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Loss Notice Damage type..........");
			return null;

		}
		return Damagetype;
	}
	public String getPowerSurgeDamage(ModelBean Claim) {
		String Damagetype = "";
		try {
			ArrayList<String> damages = new ArrayList<String>();
			if(Claim.gets("PwrSurgeDamageMainDwellingInd").equals("Yes")){
				damages.add("Main Dwelling");
			}
			if(Claim.gets("PwrSurgeDamageStructureInd").equals("Yes")){
				damages.add("Structure (Garage/Farm)");
			}
			if(Claim.gets("PwrSurgeDamageOtherInd").equals("Yes")){
				damages.add("Other (electronics, television, computer, etc.)");
			}
			for(String damage : damages)
				Damagetype = StringTools.concat(Damagetype, damage, ", ");
		
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Loss Notice Damage type..........");
			return null;

		}
		return Damagetype;
	}
	
	public String getWindDamage(ModelBean Claim) {
		String Damagetype = "";
		try {
			ArrayList<String> damages = new ArrayList<String>();
			if(Claim.gets("WindDamageMainDwellingInd").equals("Yes")){
				damages.add("Main Dwelling");
			}
			if(Claim.gets("WindDamageStructureInd").equals("Yes")){
				damages.add("Structure (Garage/Farm)");
			}
			if(Claim.gets("WindDamageOtherInd").equals("Yes")){
				damages.add("Other (Fence, Personal Property)");
			}
			for(String damage : damages)
				Damagetype = StringTools.concat(Damagetype, damage, ", ");
		
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Loss Notice Damage type..........");
			return null;

		}
		return Damagetype;
	}
	/**
	 * This method is used to apply the insurance score to the Renewal Policy,
	 * which got ordered through the "Pre-Automated Policy Insurance Score Data Report Ordering" Task 
	 * @param application
	 * @param priorPolicy
	 * @return void
	 * @throws Exception if an Unexpected Error Occurs
	 */
	public static void setCPNCFDataReportApply(ModelBean application, ModelBean priorPolicy) throws Exception {
		    ModelBean priorBasicPolicyBean = priorPolicy.getBean("BasicPolicy");
			ModelBean presentBasicPolicyBean = application.getBean("BasicPolicy");
			JDBCData data = ServiceContext.getServiceContext().getData();
			ModelBean insuredBean = priorPolicy.getBean("Insured");
			String sourceId = insuredBean.gets("id");
			ModelBean dataReportRequestTemp = application.findBeanByFieldValue("DataReportRequest","TemplateIdRef", "XTaskDataReport0001");			
			ModelBean dataReportRequest = UWDataReportRenderer.getLatestDataReportRequest(application, sourceId,
					"XTaskDataReport0001", true);
			if (dataReportRequest != null) {
				String latestStatusCd = dataReportRequest.gets("StatusCd");
				if (!StringUtils.isEmpty(latestStatusCd) && latestStatusCd.equalsIgnoreCase("Ordered")) {
				ModelBean dataReport = BeanRenderer.selectModelBean("DataReport",
						dataReportRequest.gets("DataReportRef"));
				ModelBean cpNCFReport = dataReport.getBean("CPNCFReport");
				ModelBean spouseCPNCFReport = cpNCFReport.getBean("CPNCFReport");
				boolean spouseScore = false;
	            
	            if( spouseCPNCFReport != null ) {
	                spouseScore = true;
	            }  
	            
				// Save Source
				ModelBean insuranceScore = application.getBeanByAlias("InsuredInsuranceScore");
				insuranceScore.setValue("SourceCd", "NCF");
				insuranceScore.setValue("SourceIdRef", dataReportRequest.getId());

				ModelBean insuranceJointScore = application.getBeanByAlias("InsuredJointInsuranceScore");
				
				// Save Status
				String statusCd = "";
				String processingStatusCd = cpNCFReport.gets("ProcessingStatusCd");
				if (processingStatusCd.equals("M"))
					statusCd = "No Hit";
				else if (StringTools.in(processingStatusCd, "C,H,P"))
					statusCd = "Hit";
				else if (StringTools.in(processingStatusCd, "N,U"))
					statusCd = "No Score";
				else
					statusCd = "Error";

				if (spouseCPNCFReport != null) {
					if (statusCd.equals("No Hit") || statusCd.equals("No Score")) {
						if (StringTools.in(spouseCPNCFReport.gets("ProcessingStatusCd"), "C,H,P")) {
							statusCd = "Hit";
						}
					}
				}
				insuranceScore.setValue("StatusCd", statusCd);
				
				if(spouseScore) {
		            insuranceJointScore.setValue("SourceCd", "NCF");
		            insuranceJointScore.setValue("SourceIdRef", dataReportRequest.getId());
		            insuranceJointScore.setValue("StatusCd", statusCd);
				}    
				
				// If Hit or No Score - Clear Insurance Score Information
				if (statusCd.equals("Hit") || statusCd.equals("No Score")) {
					insuranceScore.setValue("InsuranceScore", "");
					insuranceScore.setValue("OverriddenInsuranceScore", "");
					insuranceScore.deleteBeans("InsuranceScoreReason");
				}

				if( (statusCd.equals("Hit") || statusCd.equals("No Score")) && spouseScore) {
	            	insuranceJointScore.setValue("InsuranceScore", "");
	            	insuranceJointScore.setValue("OverriddenInsuranceScore", "");
	            	insuranceJointScore.deleteBeans("InsuranceScoreReason");
	            }
				
				// Save Insurance Score
				saveInsuranceScore(insuranceScore, cpNCFReport);
				if (spouseCPNCFReport != null) {
					saveInsuranceScore(insuranceJointScore, spouseCPNCFReport);
				}

				// Set the DataReportRequest ModelBean Date, and Time Applied
				dataReportRequest.setValue("AppliedDt", DateTools.getStringDate());
				dataReportRequest.setValue("AppliedTm", DateTools.getTime("HH:mm:ss z"));

				// Set the DataReportRequest ModelBean Status to Applied
				dataReportRequest.setValue("StatusCd", "Applied");
				
				// Update Application ModelBean
				priorBasicPolicyBean.setValue("InsuranceScoreRequestInd", "No");
				BeanTools.saveBean(priorPolicy, data);
				//for manual renewal,dataReportRequestTemp will be null
				if(dataReportRequestTemp==null){
					application.addValue(dataReportRequest);
				}
				DataReport.processDataReportRequests(data, application, priorPolicy);
				presentBasicPolicyBean.setValue("InsuranceScoreRequestInd", "No");
				BeanTools.saveBean(application, data);
			 }
			}
	}

	/**
	 * This method is used to save the Insurance Score
	 * @param insuranceScore InsuranceScore ModelBean
	 * @param cpNCFReport CPNCFReport ModelBean
	 * @throws Exception if an Unexpected Error Occurs
	 */
	private static void saveInsuranceScore(ModelBean insuranceScore, ModelBean cpNCFReport) throws Exception {

		// Save Insurance Score
		ModelBean cpNCFScore = cpNCFReport.getBean("CPNCFScore");
		if (cpNCFScore != null) {
			String score = cpNCFScore.gets("Value");
			if (StringTools.greaterThan(score, insuranceScore.gets("InsuranceScore"))) {
				insuranceScore.setValue("InsuranceScore", score);
				insuranceScore.setValue("OverriddenInsuranceScore", score);

				// Delete Existing Insurance Score Reasons
				insuranceScore.deleteBeans("InsuranceScoreReason");

				// Save Insurance Score Reasons
				ModelBean[] cpNCFScoreReasons = cpNCFScore.getBeans("CPNCFScoreReason");
				for (ModelBean cpNCFScoreReason : cpNCFScoreReasons) {
					ModelBean insuranceScoreReason = new ModelBean("InsuranceScoreReason");
					insuranceScoreReason.setValue("ReasonCd", cpNCFScoreReason.gets("ReasonCd"));
					insuranceScoreReason.setValue("Description", cpNCFScoreReason.gets("Description"));
					insuranceScore.addValue(insuranceScoreReason);
				}
			}
		}

	}
	
	/**
	 * IRNumber helped method - resets the IR number count
	 * 
	 * @throws Exception
	 */
	public static void resetAIProviderNumberCounter() throws Exception {
		JDBCData data = ServiceContext.getServiceContext().getData();
		ModelBean counters = data.selectModelBean(new ModelBean("Counters"), 1);
		ModelBean counter = counters.findBeanByFieldValue("Counter", "Type", "AIProvider");
		if (counter != null) {
			counter.setValue("Next", AIPROVIDERNUMBER_START);
			data.saveModelBean(counters);
			data.commit();
		}
	}
	
	/**
	 * Identifies the next number in the sequence for numbring of IR number
	 * 
	 * @param connection
	 * @param name
	 * @param application
	 * @return
	 * @throws Exception
	 */
	public static String nextAIProviderNumber() throws Exception {
		long nextNumber = Counter.nextNumber("AIProvider");

		if (nextNumber == 1) {
			resetAIProviderNumberCounter();
			nextNumber = Counter.nextNumber("AIProvider");
		}

		return AIPROVIDERNUMBER_PREFIX + nextNumber;
	}
	
	/**
	 * Get Risks Description Select List
	 * 
	 * @param container
	 *            ModelBean Containing the Policy/Application
	 * @return String of Risk with building description seperated by delimiter
	 *         comma
	 * @throws Exception
	 *             if an Unexpected Error Occurs
	 */
	public static String getRiskDescriptionList(ModelBean container) {
		try {

			String riskDescriptionList = "";
			ModelBean[] risks = container.getBean("Line").findBeansByFieldValue("Risk", "Status", "Active");
			String lineCd = container.getBean("Line").gets("LineCd");
			
			if (lineCd.equals("Dwelling")) {
				for (ModelBean risk : risks) {
					ModelBean building = risk.getBean("Building");
					ModelBean riskLookupAddr = building.getBeanByAlias("RiskLookupAddr");
					String riskDescription = riskLookupAddr.gets("VerificationHash");
					riskDescriptionList = StringTools.concat(riskDescriptionList, riskDescription, ",");
				}
			}
				
			return riskDescriptionList;

		} catch (Exception e) {
			Log.error(e);
			return "";
		}
	}
	
    /**
	 * Get the Option Label
	 * 
	 * @param o
	 * @param value
	 */
	public static String getOptionLabel(String umol, String value, String defaultValue) {
		String returnvalue = "";

		try {
			ModelBean coderef = BeanTools.convertMDACodeRef(umol);
			java.util.StringTokenizer st = new java.util.StringTokenizer(umol, "::", false);
			st.nextToken();
			st.nextToken();
			st.nextToken();
			
			String keyName = st.nextToken();
			// Find the option and return the name
			ModelBean options = coderef.getBean("options", "key", keyName);
			ModelBean[] optionList = options.getBeans("option");
			for( ModelBean option : optionList){
				if(option.gets("value").equalsIgnoreCase(value)){
					if (option == null) {
						returnvalue = "";
					} else {
						returnvalue = option.gets("name");
					}
				}
			}
		} catch (Exception mbe) {
		}
		return returnvalue;
	}
	public static String getOptionValue(String umol, String label, String defaultValue) {
		String returnvalue = "";

		try {
			ModelBean coderef = BeanTools.convertMDACodeRef(umol);
			java.util.StringTokenizer st = new java.util.StringTokenizer(umol, "::", false);
			st.nextToken();
			st.nextToken();
			st.nextToken();
			
			String keyName = st.nextToken();
			// Find the option and return the name
			ModelBean options = coderef.getBean("options", "key", keyName);
			ModelBean[] optionList = options.getBeans("option");
			for( ModelBean option : optionList){
				if(option.gets("name").equalsIgnoreCase(label)){
					if (option == null) {
						returnvalue = "";
					} else {
						returnvalue = option.gets("value");
					}
				}
			}
		} catch (Exception mbe) {

		}
		return returnvalue;
	}
	public static void setFRScore(ModelBean application) throws Exception {
		ArrayList<String> frScoresList = new ArrayList<String>();
		ArrayList<String> baseFRScoreList = new ArrayList<String>() {{
			add("R");
			add("P");
			add("N");
			add("L");
			add("J");
			add("H");
			add("F");
			add("D");		
		}};

		ModelBean basicPolicy = application.getBean("BasicPolicy"); 
		String entityTypeCd = application.getBean("Insured").gets("EntityTypeCd");
		String insuranceScore = "";
		String statusCd = "";
		String insuredFRScore = "";
		String jointFRScore = "";
	
		ModelBean [] insuranceScores = application.getBean("Insured").getBeans("InsuranceScore");
		for( ModelBean myInsuranceScore : insuranceScores) {
			String overriddenInsuranceScore = "";
			insuranceScore = myInsuranceScore.gets("InsuranceScore");
			overriddenInsuranceScore = myInsuranceScore.gets("OverriddenInsuranceScore");
			statusCd = myInsuranceScore.gets("StatusCd");
			
			if (!overriddenInsuranceScore.equals("") ) {
				insuranceScore = overriddenInsuranceScore; 
			}
			
			if(insuranceScore.equals("998") || insuranceScore.equals("") || ( statusCd.equals("No Hit")  && overriddenInsuranceScore.equals("") ) ) {
				frScoresList.add("P");
			} else if( insuranceScore.equals("999") || statusCd.equalsIgnoreCase("No Score") ) {
				frScoresList.add("R");
			} else {
				if( StringRenderer.lessThan( insuranceScore, "500")  ) 
					frScoresList.add("N");
				else if(StringRenderer.lessThanEqual(insuranceScore, "549")  && StringRenderer.greaterThanEqual(insuranceScore, "500") )
					frScoresList.add("L");
				else if(StringRenderer.lessThanEqual(insuranceScore, "600")  && StringRenderer.greaterThanEqual(insuranceScore, "550") )
					frScoresList.add("J");
				else if(StringRenderer.lessThanEqual(insuranceScore, "700")  && StringRenderer.greaterThanEqual(insuranceScore, "601") )
					frScoresList.add("H");
				else if(StringRenderer.lessThanEqual(insuranceScore, "800")  && StringRenderer.greaterThanEqual(insuranceScore, "701") )
					frScoresList.add("F");
				else if(StringRenderer.greaterThan(insuranceScore, "800") )
					frScoresList.add("D");
			}
		}
		
		insuredFRScore = frScoresList.get(0); 
		basicPolicy.setValue("FRScore", insuredFRScore);
		
		if( entityTypeCd.equals("Joint") ) {
			jointFRScore = frScoresList.get(1);
			int index1 = baseFRScoreList.indexOf(insuredFRScore);
			int index2 = baseFRScoreList.indexOf(jointFRScore);
			if (index1 > index2 ) {
				basicPolicy.setValue("FRScore", insuredFRScore);
			} else {
				basicPolicy.setValue("FRScore", jointFRScore);
			}
		}

	}
	
	 public static long findNumericPortion(String format) {
	    	try {
	    		StringBuilder intPortion = new StringBuilder();
	    		
	    		// Strip all of the alpha characters
	    		for( int cnt=0; cnt<format.length(); cnt++ ) {
	    			char ch = format.charAt(cnt);
	    			if( ch < '0' || ch > '9' ) continue;
	    			intPortion = intPortion.append(ch);
	    		}
	    		
	    		// Now return the numeric portion
	    		if (intPortion.length() > 0) {
	    			return Long.parseLong(intPortion.toString());    			
	    		} else {
	    			return 0;
	    		}
	    	} catch( Exception e ) {
	            e.printStackTrace();
	            Log.error(e);
	            return 0;
	    	}
	    }
	 
	 /** Returns UI label and SSN/FEIN masked/unmasked textbox based on logic defined in RMGUAT-1040
	 *
	 * @param ssnModified           string containing comma seperated list of 'modified' tax ids
     *                              if 'fieldName' is present in list, value is not masked
	 *
     * @param sourceBean            bean to pull 'fieldName' value from
	 *
     * @param sourceBeanName        name of above bean
	 *
     * @param fieldName             fieldName in bean that has tax id value
	 *
     * @param label                 label value to display in label td row
	 *
     * @param mask                  boolean indicator if value should be masked
	 *
     * @param disable               standard disable indicator
	 *
     * @param rs                    standard CMMParam value passed to all renderers
     *
	 * @return                     string containing td encapsolated tax id label and textbox
	 */
	 public static String outputTaxIdWithLabel(String ssnModified, ModelBean sourceBean, String sourceBeanName, String fieldName, String label, boolean mask, String disable, ModelBean rs) throws Exception {
		 ssnModified = (ssnModified == null ? "" : ssnModified);
		 String uiType = fieldName;
		String value = sourceBean.gets(fieldName);
		//check if value may have come from the 'other' tax id
		if(value.isEmpty()) {
			if(fieldName.equals("SSN")) {
				String FEIN = sourceBean.gets("FEIN");
				if(!FEIN.isEmpty()) {
					value = FEIN;
				}
			} else if(fieldName.equals("FEIN")) {
				String SSN = sourceBean.gets("SSN");
				if(!SSN.isEmpty()) {
					value = SSN;
				}
			}
		}
		String fullFieldName = sourceBeanName+"."+fieldName;
		if(ssnModified.contains(fullFieldName) || !mask) {
			uiType = "String";
		}
		String output = "<th class='label'>";
		output += FormFieldRenderer.renderLabel(fullFieldName, label, "Label", rs, "");
		output += "</th>";
		output += "<td id='Individual_SSN'>";
		output += FormFieldRenderer.render(fullFieldName, sourceBean.gets(fieldName), uiType, "maxlength=11|"+disable);
		output += "</td>";
		return output;
	 }
	 /** Returns UI SSN/FEIN masked/unmasked textbox based on logic defined in RMGUAT-1040
	 *
	 * @param ssnModified           string containing comma seperated list of 'modified' tax ids
     *                              if 'fieldName' is present in list, value is not masked
	 *
     * @param sourceBean            bean to pull 'fieldName' value from
	 *
     * @param sourceBeanName        name of above bean
	 *
     * @param fieldName             fieldName in bean that has tax id value
	 *
     * @param mask                  boolean indicator if value should be masked
	 *
     * @param disable               standard disable indicator
	 *
     * @param rs                    standard CMMParam value passed to all renderers
     *
	 * @return                     string containing td encapsolated tax id label and textbox
	 */
	 public static String outputTaxIdTextBox(String ssnModified, ModelBean sourceBean, String sourceBeanName, String fieldName,  boolean mask, String disable, ModelBean rs) throws Exception {
		 ssnModified = (ssnModified == null ? "" : ssnModified);
		 String uiType = fieldName;
		String value = sourceBean.gets(fieldName);
		if(value.isEmpty()) {
			if(fieldName.equals("SSN")) {
				String FEIN = sourceBean.gets("FEIN");
				if(!FEIN.isEmpty()) {
					value = FEIN;
				}
			} else if(fieldName.equals("FEIN")) {
				String SSN = sourceBean.gets("SSN");
				if(!SSN.isEmpty()) {
					value = SSN;
				}
			}
		}
		String fullFieldName = sourceBeanName+"."+fieldName;
		if(ssnModified.contains(fullFieldName) || !mask) {
			uiType = "String";
		}
		String output = "<td id='Individual_SSN'>";
		output += FormFieldRenderer.render(fullFieldName, sourceBean.gets(fieldName), uiType, "maxlength=11|"+disable);
		output += "</td>";
		return output;
	 }

	 /** Return SubmitTo and SubmitToCd for an agent for a particular product.
	 *
	 *  SubmitTo information is available on the ProducerInfo record, but may be overridden
	 *  at the LicenseClass level.  Used, in particular, for Safeguard Select (SEL) products.
	 *
	 * @param producerSystemId     the SystemId for the Provider bean of the agent code, e.g.
	 *                             BasicPolicy.ProviderRef
	 * @param productVersionIdRef  the VersionIdRef of the product (for looking up the
	 *                             LicenseClass), e.g. BasicPolicy.ProductVersionIdRef
	 * @return                     an ArrayList containing SutmitTo and SubmitToCd in
	 *                             elements 0 and 1
	 */
	public static ArrayList<String> getSubmitTo( String stateCd, String producerSystemId, String productVersionIdRef ){
		String[] submitTo = { "", "" };
		try {
			// Look for overriding SubmitTo for this LicensedProduct on this agent
			ModelBean producer = ProviderRenderer.getProviderBySystemId( producerSystemId );
			ModelBean licensedProductsBean = producer.getBean("LicensedProducts");
			String licenseClassCd = ProductRenderer.getLicenseClassCd(productVersionIdRef);
			ModelBean[] licensedProducts = BeanTools.findBeansByFieldValueAndStatus( licensedProductsBean, "LicensedProduct", "LicenseClassCd", licenseClassCd, "Active" );
			
			for ( ModelBean licensedProduct : licensedProducts ) {
				if ( licensedProduct.gets("LicenseClassCd").equalsIgnoreCase(licenseClassCd) && licensedProduct.gets("StateProvCd").equalsIgnoreCase(stateCd)) {
					submitTo[0] = licensedProduct.gets("SubmitTo");
					submitTo[1] = licensedProduct.gets("SubmitToCd");
					return new ArrayList<String>( Arrays.asList(submitTo) );
				}
			}
			
			
			// If not found, use default SubmitTo for this agent
			ModelBean producerInfo = producer.getBean("ProducerInfo");
			submitTo[0] = producerInfo.gets("SubmitTo");
			submitTo[1] = producerInfo.gets("SubmitToCd");
			
			// If not found, use default SubmitTo to UW group.
			if( submitTo[0].isEmpty() ) {
				submitTo[0] = "UW";
				submitTo[1] = "Group";
			}

		} catch(Exception ex){
			ex.printStackTrace();
		}
		
		return new ArrayList<String>( Arrays.asList(submitTo) );
	}
	
	/**
	 * Get user from providerRef
	 * @param data
	 * @param providerRef
	 * @return
	 * @throws Exception
	 */
	public static ArrayList<String> getUserFromProviderRef( String providerRef ) throws Exception {
		JDBCData data = ServiceContext.getServiceContext().getData();
		String[] user = { "UW", "Group" };
		
		if( providerRef.isEmpty() )
			return new ArrayList<String>( Arrays.asList(user) );
		
		ModelBean[] userInfos = BeanRenderer.selectModelBeansFromLookup(data, "UserInfo", "ProviderRef", providerRef );
		
		for( ModelBean userInfo : userInfos ) {
			if( userInfo.gets("StatusCd").equals("Active") ) {
				user[0] = userInfo.gets("LoginId");
				user[1] = "User";
				
				return new ArrayList<String>( Arrays.asList(user) );
			}
		}
		
		return new ArrayList<String>( Arrays.asList(user) );
	}
	
	public static String isAdditionalAreaPresent(ModelBean building) throws Exception {
		String additionalArea1 = RICProductRenderer.getOptionLabel("e2value::e2value::mapping::dw_additional_area_to_e2Value", building.gets("AdditionalArea1"),"");
		String additionalArea2 = RICProductRenderer.getOptionLabel("e2value::e2value::mapping::dw_additional_area_to_e2Value", building.gets("AdditionalArea2"),"");
		String additionalArea3 = RICProductRenderer.getOptionLabel("e2value::e2value::mapping::dw_additional_area_to_e2Value", building.gets("AdditionalArea3"),"");
		String additionalArea4 = RICProductRenderer.getOptionLabel("e2value::e2value::mapping::dw_additional_area_to_e2Value", building.gets("AdditionalArea4"),"");
		String additionalArea5 = RICProductRenderer.getOptionLabel("e2value::e2value::mapping::dw_additional_area_to_e2Value", building.gets("AdditionalArea5"),"");
		String additionalArea6 = RICProductRenderer.getOptionLabel("e2value::e2value::mapping::dw_additional_area_to_e2Value", building.gets("AdditionalArea6"),"");
		
		if (isValidAddlArea(additionalArea1) || isValidAddlArea(additionalArea2) || isValidAddlArea(additionalArea3)
				|| isValidAddlArea(additionalArea4) || isValidAddlArea(additionalArea5)
				|| isValidAddlArea(additionalArea6)) {
			return "yes";
		}

		return "no";
	}
	
	public static boolean isValidAddlArea(String addlArea) throws Exception {
		
		if(addlArea!=null && !addlArea.equalsIgnoreCase("") && !addlArea.equalsIgnoreCase("None") && !addlArea.equalsIgnoreCase("DO NOT SEND")){
			return true;
		}
		
		return false;
	}
	
	public static String extractNumbers(String sqft) throws Exception {
		String numberOnly="";
		try{
			numberOnly= sqft.replaceAll("[^0-9]", "");
		}catch(Exception e){
			e.printStackTrace();
			return "";
		}
		return numberOnly;
	}
	
	public static void suppressCancellationBill(ModelBean application) throws Exception{
		ModelBean output = application.findBeanByFieldValue("Output","OutputTemplateIdRef","Cancellation Advice");
		if( output != null ){
			ModelBean outputItems []= output.findBeansByFieldValue("OutputItem", "DocumentTypeCd", "Cancellation Advice") ;
				for(ModelBean outputItem : outputItems){
					output.deleteBeanById(outputItem.getId());
				}
				ModelBean[] outputItemsAfter = output.findBeansByFieldValue("OutputItem", "DocumentTypeCd", "Cancellation Advice");
				if(VelocityTools.beanArrayLength(outputItemsAfter) <= 0){
                    application.deleteBeanById( output.getId());
				}
			}
		}
	
	public static ModelBean  parseUnverifiedAddress(ModelBean address,String type) throws Exception{
		ModelBean parsedAddr = new ModelBean("Addr");
		if(type.equalsIgnoreCase("InsuredPriorAddr"))
			parsedAddr.setValue("AddrTypeCd","ParsedInsuredPriorAddr");
		else
			parsedAddr.setValue("AddrTypeCd","ParsedInsuredMailingAddr");
		String address1 = address.gets("Addr1");
		int firstSpacePos = address1.indexOf(' ');
		/*if (firstSpacePos <= 0) {
			return;
		}*/
		String firstWord = address1.substring(0, firstSpacePos);
		boolean numberParsed = false;
		try {
			Integer.parseInt(firstWord);
			numberParsed = true;
		} catch (NumberFormatException ne) {
			numberParsed = false;
		}
		if (numberParsed) {
			parsedAddr.setValue("PrimaryNumber", firstWord);
			parsedAddr.setValue("StreetName", address1.substring(firstSpacePos+1));
			
		} else {
			parsedAddr.setValue("StreetName", address1);
		}
		parsedAddr.setValue("SecondaryNumber","");
		parsedAddr.setValue("City",address.gets("City"));
		parsedAddr.setValue("StateProvCd",address.gets("StateProvCd"));
		parsedAddr.setValue("PostalCode",address.gets("PostalCode"));
		return parsedAddr;
	}
		
	public boolean isFinalPremiumDifference(String oldFinalPremiumAmt, String newFinalPremiumAmt) {
		try{ 
			double oldPremiumAmt = Double.parseDouble(oldFinalPremiumAmt);
			double newPremiumAmt = Double.parseDouble(newFinalPremiumAmt);
			
			double diff = oldPremiumAmt - newPremiumAmt; 

			if( diff > 0.0 && diff <= 3.0) {
				return true; 
			} else {
				return false;
			}

		} catch( NullPointerException nullPointerException ) {
			Log.debug("Error when calculating FinalPremiumAmt Difference");
		} catch (Exception e) {
			Log.debug("Error when calculating FinalPremiumAmt Difference");
		}

		return false;
	}
	
	
	public static void generateRefundLetter(JDBCData data, ModelBean policy, ModelBean responseParams) throws ModelBeanException {	
       	try {
       			String userId = "System";
       			String todayDt = DateRenderer.getDate(responseParams);
       			String todayTm = DateRenderer.getTime(responseParams);
		       	ModelBean user = ServiceContext.getServiceContext().getUser();
				String securityId = SecurityManager.getSecurityManager().generateSecurityId(user);
		        String sessionId = responseParams.gets("SessionId");
		        String conversationId = responseParams.gets("ConversationId");
				String logKey = "";
				StringDate todayStringDt = DateRenderer.getStringDate(responseParams);
				ModelBean output = DCRenderer.addCorrespondence(data, policy, "RefundLetter", "UWPolicy", "", userId, todayStringDt, logKey);
			    output.setValue("AddUser","System");
			    output.setValue("AddDt",todayDt);
			    output.setValue("AddTm",todayTm);
				DCRenderer.processOutput(data,userId,securityId, sessionId, conversationId,policy,output,todayStringDt,todayTm);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
