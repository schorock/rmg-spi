package com.ric.insurance.product;

import java.util.Hashtable;
import java.util.Map;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.xml.XmlDoc;

import com.iscs.common.business.rule.RuleManager;
import com.iscs.insurance.product.ProductRuleTransformer;

/**
 * Runs product rules to transform beans or xml docs 
 * 
 * @author Anusha.N
 *
 */
public class RICProductRuleTransformer extends ProductRuleTransformer {
	
	/** Transform an xml document in a product rule
	 * @param productSetup 
	 * @param ruleTypeCd 
	 * @param doc 
	 * @return
	 * @throws Exception
	 */
	/*public XmlDoc transform(ModelBean productSetup, String ruleTypeCd, XmlDoc doc, ModelBean bean) throws Exception{

		// Get the rule template
		ModelBean ruleTemplate = getRuleTemplate(productSetup, ruleTypeCd);      

		// Prepare the rule's context containing name value pairs
		Map<String, Object> context = new Hashtable<String, Object>();
		
		context.put(doc.getRootElement().getName(), doc.getRootElement());
		context.put("UWExternalQuoteRs", bean);
		// Process the rule
		String result = RuleManager.processRule(ruleTemplate, context);
		
		// Build a model bean from the result
		result = result.replace("&", "&amp;");
		XmlDoc resultDoc = new XmlDoc(result);
		return resultDoc;
	}*/
	
	
	/** Get a rule template from a product setup
	 * 
	 * @param productSetup
	 * @param ruleTypeCd
	 * @return
	 * @throws Exception
	 */
	/*private ModelBean getRuleTemplate(ModelBean productSetup, String ruleTypeCd) throws Exception {
		
		// Get the product rule
		ModelBean productRule = productSetup.findBeanByFieldValue("ProductRule", "RuleTypeCd", ruleTypeCd);
		if(productRule == null){
			throw new RuntimeException("Product rule '" + ruleTypeCd + "' not found in product setup");
		}
		
		// Convert the product rule to a RuleTemplate
		ModelBean ruleTemplate = new ModelBean("RuleTemplate");
		ruleTemplate.setValue("Processor", productRule.gets("Processor"));
		ruleTemplate.setValue("RuleSource", productRule.gets("RuleSource"));
		
		return ruleTemplate;
	}*/

	
//	public XmlDoc claimRuleTransform(ModelBean claimProductSetup, String ruleTypeCd,  ModelBean claim, ModelBean xactAnalysisReqCriteria,ModelBean provider) throws Exception{
	public XmlDoc claimRuleTransform(ModelBean claimProductSetup, String ruleTypeCd,  ModelBean claim,ModelBean claimant ,ModelBean provider) throws Exception{

		// Get the rule template
		ModelBean ruleTemplate = getClaimRuleTemplate(claimProductSetup, ruleTypeCd);      

		// Prepare the rule's context containing name value pairs
		Map<String, Object> context = new Hashtable<String, Object>();
		
		context.put(claim.getBeanName(), claim);
		context.put(claimant.getBeanName(), claimant);
//		context.put(xactAnalysisReqCriteria.getBeanName(), xactAnalysisReqCriteria);
		context.put(provider.getBeanName(), provider);
		
		// Process the rule
		String result = RuleManager.processRule(ruleTemplate, context);		
		// Build a model bean from the result
		result = result.replace("&", "&amp;");
		XmlDoc resultDoc = new XmlDoc(result);
		return resultDoc;
	}
	
	
	/** Get a rule template from a product setup
	 * 
	 * @param productSetup
	 * @param ruleTypeCd
	 * @return
	 * @throws Exception
	 */
	private ModelBean getClaimRuleTemplate(ModelBean productSetup, String ruleTypeCd) throws Exception {
		
		// Get the product rule
		ModelBean productRule = productSetup.findBeanByFieldValue("ClaimRule", "RuleTypeCd", ruleTypeCd);
		if(productRule == null){
			throw new RuntimeException("Product rule '" + ruleTypeCd + "' not found in product setup");
		}
		
		// Convert the product rule to a RuleTemplate
		ModelBean ruleTemplate = new ModelBean("RuleTemplate");
		ruleTemplate.setValue("Processor", productRule.gets("Processor"));
		ruleTemplate.setValue("RuleSource", productRule.gets("RuleSource"));
		
		return ruleTemplate;
	}

}
