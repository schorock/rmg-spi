package com.ric.insurance.interfaces.gl;

import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.jdom.Element;

import com.iscs.common.utility.StringTools;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.date.StringDate;

/**
* @author peter.hose
*
*/
public class CsvFileWriter extends FileWriter {

	/**
	 * @param fileName
	 * @throws IOException
	 */
	public CsvFileWriter(final String fileName) throws IOException {
		super(fileName);
	}

	/**
	 * @param file
	 * @param append
	 * @throws IOException
	 */
	public CsvFileWriter(final File file, final boolean append) throws IOException {
		super(file, append);
	}

	/**
	 * @param file
	 * @throws IOException
	 */
	public CsvFileWriter(final File file) throws IOException {
		super(file);
	}

	/**
	 * @param fd
	 */
	public CsvFileWriter(final FileDescriptor fd) {
		super(fd);
	}

	/**
	 * @param fileName
	 * @param append
	 * @throws IOException
	 */
	public CsvFileWriter(final String fileName, final boolean append) throws IOException {
		super(fileName, append);
	}

	


	protected void write(final StringBuffer sb, final ModelBean bean, final RecordDefinition record) throws Exception {
		final String _method = "void write(StringBuffer sb, ModelBean bean, RecordDefinition record)";
		
		_enter(_method);

		try {
			int written = 0;

			for (final FieldDefinition field : record.fields) {
				final CsvFieldDefinition cfd = (CsvFieldDefinition) field;

				Object value = null;

				if (log.isInfoEnabled())
					log.info(String.format("Processing field %1$s of type %2$s", cfd.id, cfd.type));

				if (records.containsKey(cfd.type)) {
					//
					// Process a sub record.
					//
					final ModelBean[] beans = bean.getBeans(cfd.id);

					for (int index = 0, count = beans.length; index < count; ++index) {
						if (index < cfd.max)
							write(sb, beans[index], getRecordDefinition(cfd.type));
					}
				}
				else {

					try {
						if (bean.hasBeanField(cfd.id))
							value = bean.getValue(cfd.id);

						// If the value is null and a default is specified, create a 
						// new instance of the value from the default
						if (value == null && cfd._default != null) {
							if (log.isInfoEnabled())
								log.info("Constructing value from default");

							final Class<?> cls = FileWriterFactory.getFieldType(cfd.type);
							final Class<?>[] argtypes = { String.class };
							final Object[] arglist = { cfd._default };
							final Constructor<?> ctor = cls.getConstructor(argtypes);

							value = ctor.newInstance(arglist);
						}

						// If the does not exist for any reason, and it is required,
						// blow up as there is no way to proceed
						if (value == null) {
							if (cfd.required)
								throw new Exception(String.format("Value for field %1$s is required and no default exists.", cfd.id));
						}

						// At this point a value exists. Now it must be formatted
						String formatted = null;

						if (!(value == null || cfd.format == null)) {
							if (log.isInfoEnabled())
								log.info(String.format("Formatting to field %1$s value %2$s using format %3$s", cfd.id, value.toString(), cfd.format));

							try {
								if (value instanceof StringDate)
									formatted = ((StringDate) value).toStringDisplay(cfd.format);
								else if (value instanceof com.ibm.math.BigDecimal)
									formatted = String.format(cfd.format, new java.math.BigDecimal(value.toString()));
								else
									formatted = String.format(cfd.format, value);
							}
							catch (final Exception e) {
								exception(String.format("Failed to field %1$s value %2$s using format %3$s", cfd.id, value.toString(), cfd.format), e);

								throw e;
							}
						}

						// Truncate to the correct length if specified
						if (formatted != null) {
							if (cfd.truncate > 0) {
								formatted = StringTools.trim(formatted, cfd.truncate);
							}
						}
						
						// Escape and quote value if necessary
						if (formatted != null) {
							if (record.force_quote || formatted.contains(record.quote) || formatted.contains(record.fs) || formatted.contains(record.rs)) {
								formatted = formatted.replace(record.quote, record.quote + record.quote);
								formatted = record.quote + formatted + record.quote;
							}
						}

						if (written > 0)
							sb.append(record.fs);

						if (formatted != null)
							sb.append(formatted);

					}
					catch (final Exception e) {
						exception(String.format("Failed to write field %1$s", cfd.id), e);

						throw e;
					}

					++written;
				}
			}
		}
		catch (final Exception e) {
			_exception(_method, e);

			throw e;
		}

		_leave(_method);
	}

	@Override
	public void write(final ModelBean bean, final RecordDefinition record) throws Exception {
		final String _method = "void write(ModelBean bean, RecordDefinition record)";

		_enter(_method);

		try {
			final StringBuffer sb = new StringBuffer();

			write(sb, bean, record);

			write(sb.toString());
			write(record.rs);
		}
		catch (final Exception e) {
			_exception(_method, e);

			throw e;
		}

		_leave(_method);
	}

	protected void write(final StringBuffer sb, final Map<String, Object> map, final RecordDefinition record) throws Exception {
		final String _method = "void write(final StringBuffer sb, final Map<String, Object> map, final RecordDefinition record)";
		
		_enter(_method);

		try {
			int written = 0;

			for (final FieldDefinition field : record.fields) {
				final CsvFieldDefinition cfd = (CsvFieldDefinition) field;

				Object value = null;

				if (log.isInfoEnabled())
					log.info(String.format("Processing field %1$s of type %2$s", cfd.id, cfd.type));

				if (records.containsKey(cfd.type)) {
					//
					// Process a sub record.
					//
					@SuppressWarnings("unchecked")
					final List<Map<String, Object>> maps = (List<Map<String, Object>>) map.get(cfd.id);

					for (int index = 0, count = maps.size(); index < count; ++index) {
						if (index < cfd.max)
							write(sb, maps.get(index), getRecordDefinition(cfd.type));
					}
				}
				else {

					try {
						if (map.containsKey(cfd.id))
							value = map.get(cfd.id);

						// If the value is null and a default is specified, create a 
						// new instance of the value from the default
						if (value == null && cfd._default != null) {
							if (log.isInfoEnabled())
								log.info("Constructing value from default");

							final Class<?> cls = FileWriterFactory.getFieldType(cfd.type);
							final Class<?>[] argtypes = { String.class };
							final Object[] arglist = { cfd._default };
							final Constructor<?> ctor = cls.getConstructor(argtypes);

							value = ctor.newInstance(arglist);
						}

						// If the does not exist for any reason, and it is required,
						// blow up as there is no way to proceed
						if (value == null) {
							if (cfd.required)
								throw new Exception(String.format("Value for field %1$s is required and no default exists.", cfd.id));
						}

						// At this point a value exists. Now it must be formatted
						String formatted = null;

						if (!(value == null || cfd.format == null)) {
							if (log.isInfoEnabled())
								log.info(String.format("Formatting to field %1$s value %2$s using format %3$s", cfd.id, value.toString(), cfd.format));

							try {
								if (value instanceof StringDate)
									formatted = ((StringDate) value).toStringDisplay(cfd.format);
								else if (value instanceof com.ibm.math.BigDecimal)
									formatted = String.format(cfd.format, new java.math.BigDecimal(value.toString()));
								else
									formatted = String.format(cfd.format, value);
							}
							catch (final Exception e) {
								exception(String.format("Failed to field %1$s value %2$s using format %3$s", cfd.id, value.toString(), cfd.format), e);

								throw e;
							}
						}

						// Truncate to the correct length if specified
						if (formatted != null) {
							if (cfd.truncate > 0) {
								formatted = StringTools.trim(formatted, cfd.truncate);
							}
						}
						
						// Escape and quote value if necessary
						if (formatted != null) {
							if (record.force_quote || formatted.contains(record.quote) || formatted.contains(record.fs) || formatted.contains(record.rs)) {
								formatted = formatted.replace(record.quote, record.quote + record.quote);
								formatted = record.quote + formatted + record.quote;
							}
						}

						if (written > 0)
							sb.append(record.fs);

						if (formatted != null)
							sb.append(formatted);

					}
					catch (final Exception e) {
						exception(String.format("Failed to write field %1$s", cfd.id), e);

						throw e;
					}

					++written;
				}
			}
		}
		catch (final Exception e) {
			_exception(_method, e);

			throw e;
		}

		_leave(_method);
	}
	
	
	public void write(Map<String, Object> map, RecordDefinition record) throws Exception {
		final String _method = "void write(Map<String, Object> map, RecordDefinition record)";

		_enter(_method);

		try {
			final StringBuffer sb = new StringBuffer();

			write(sb, map, record);

			write(sb.toString());
			write(record.rs);
		}
		catch (final Exception e) {
			_exception(_method, e);

			throw e;
		}

		_leave(_method);
	}
	
	protected Map<String, RecordDefinition> copy(final Map<String, RecordDefinition> records, final Element parent) throws Exception {

		for (final Iterator<?> children = parent.getChildren("RecordDefinition").iterator(); children.hasNext();) {
			final Element child = (Element) children.next();

			if (child.getAttributeValue("Format").equalsIgnoreCase("CSV")) {
				final RecordDefinition record = copy(new RecordDefinition(), child);

				records.put(record.getKey(), record);
			}
		}

		return records;
	}
	
	protected void _enter(final String method) {
		if (log.isTraceEnabled())
			log.trace(String.format("%s enter", method));
	}

	protected void _leave(final String method) {
		if (log.isTraceEnabled())
			log.trace(String.format("%s leave", method));
	}

	protected void _exception(final String method, final Throwable t) {
		if (log.isTraceEnabled())
			log.trace(String.format("%s exception", method), t);
	}

	protected void exception(final String message, final Throwable t) {
		if (log.isDebugEnabled())
			log.debug(message, t);
	}
	
	
	protected final Logger log = LogManager.getLogger(CsvFileWriter.class);
}
