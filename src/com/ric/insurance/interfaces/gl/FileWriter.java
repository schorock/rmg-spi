package com.ric.insurance.interfaces.gl;

import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.jdom.Element;

import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDAXmlDoc;
import com.iscs.insurance.interfaces.gl.ConfigurationException;
import com.iscs.insurance.interfaces.gl.Settings;

import net.inov.mda.MDAException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.xml.XmlDoc;

/**
 * @author peter.hose
 * 
 */
public class FileWriter extends java.io.FileWriter {

	protected Map<String, RecordDefinition> records = null;	
	protected HashMap<String, Class<?>> types = null;

	public FileWriter(String fileName) throws IOException {
		super(fileName);
	}

	public FileWriter(File file, boolean append) throws IOException {
		super(file, append);
	}

	public FileWriter(File file) throws IOException {
		super(file);
	}

	public FileWriter(FileDescriptor fd) {
		super(fd);
	}

	public FileWriter(String fileName, boolean append) throws IOException {
		super(fileName, append);
	}
	
	protected Map<String, RecordDefinition> getRecordDefinitions() throws Exception {
		final String _method = "Map<String, RecordDefinition> getRecordDefinitions()";
		
		_enter(_method);
		// Load on demand;
		if (records == null) {
			// Build the beans from the Record Definitions
			final XmlDoc document = Settings.getRecordDefinitions();
			if (document != null)
				records = copy(new HashMap<String, RecordDefinition>(), document.getRootElement());
		}

		_leave(_method);
		
		return records;
	}

	protected Map<String, RecordDefinition> copy(final Map<String, RecordDefinition> records, final Element parent) throws Exception {
		final String _method = "Map<String, RecordDefinition> copy(final Map<String, RecordDefinition> records, final Element parent)";
		_enter(_method);
		_leave(_method);
		return null;
	}

	protected RecordDefinition copy(final RecordDefinition record, final Element parent) throws Exception {
		final String _method = "RecordDefinition copy(final RecordDefinition recordDefinition, final Element next)";
		
		_enter(_method);
		
		if (isNullOrEmpty(record.id = parent.getAttributeValue("Id")))
			throw new ConfigurationException("Record definition is missing a identifier");
			
		record.variant = parent.getAttributeValue("Variant");
		
		if (isNullOrEmpty(record.format = parent.getAttributeValue("Format")))
			throw new ConfigurationException(String.format("Record definition %1$s is missing a format declaration", record.id));
		
		_leave(_method);

		return record;
	}

	protected FieldDefinition copy(final FieldDefinition field, final Element parent) throws Exception {
		final String _method = "FieldDefinition copy(final FieldDefinition field, final Element parent)";
		
		_enter(_method);
		
		if (isNullOrEmpty(field.id = parent.getAttributeValue("Id")))
			throw new ConfigurationException("Field definition is missing a identifier");

		if (isNullOrEmpty(field.type = parent.getAttributeValue("Type")))
			throw new ConfigurationException(String.format("Field definition %1$s is missing a type declaration", field.id));

		_leave(_method);

		return field;
	}

	protected RecordDefinition getRecordDefintion(String key) throws MDAException, Exception {
		RecordDefinition record = null;

		Map<String, RecordDefinition> records = getRecordDefinitions();
		if (records != null)
			record = records.get(key);

		return record;
	}

	protected HashMap<String, Class<?>> getFieldTypes() throws Exception {
		// Load on demand;
		if (types == null) {
			// Set up the field types file
			MDAXmlDoc mdaDoc = (MDAXmlDoc) Store.getModelObject("INGLStats::field-types::*::*");
			if (mdaDoc == null)
				throw new Exception("Field types are not configured");
			// Build the beans from the field types
			XmlDoc document = mdaDoc.getDocument();
			if (document != null) {
				types = new HashMap<String, Class<?>>();

				for (Iterator<?> children = document.getRootElement().getChildren("FieldType").iterator(); children.hasNext();) {
					Element child = (Element) children.next();

					types.put(child.getAttributeValue("Id"), Class.forName(child.getAttributeValue("Class")));
				}
			}
		}

		return types;
	}
	
	protected boolean isNullOrEmpty(final String text) {
		return (text == null || text.isEmpty());
	}
	
	protected String unescape(final String st) {
		final StringBuilder sb = new StringBuilder(st.length());
	
		for (int i = 0; i < st.length(); i++) {
			char ch = st.charAt(i);
			if (ch == '\\') {
				final char nextChar = (i == st.length() - 1) ? '\\' : st
						.charAt(i + 1);
				// Octal escape?
				if (nextChar >= '0' && nextChar <= '7') {
					String code = "" + nextChar;
					i++;
					if ((i < st.length() - 1) && st.charAt(i + 1) >= '0'
							&& st.charAt(i + 1) <= '7') {
						code += st.charAt(i + 1);
						i++;
						if ((i < st.length() - 1) && st.charAt(i + 1) >= '0'
								&& st.charAt(i + 1) <= '7') {
							code += st.charAt(i + 1);
							i++;
						}
					}
					sb.append((char) Integer.parseInt(code, 8));
					continue;
				}
				switch (nextChar) {
				case '\\':
					ch = '\\';
					break;
				case 'b':
					ch = '\b';
					break;
				case 'f':
					ch = '\f';
					break;
				case 'n':
					ch = '\n';
					break;
				case 'r':
					ch = '\r';
					break;
				case 't':
					ch = '\t';
					break;
				case '\"':
					ch = '\"';
					break;
				case '\'':
					ch = '\'';
					break;
					// Hex Unicode: u????
				case 'u':
					if (i >= st.length() - 5) {
						ch = 'u';
						break;
					}
					final int code = Integer.parseInt(
							"" + st.charAt(i + 2) + st.charAt(i + 3)
							+ st.charAt(i + 4) + st.charAt(i + 5), 16);
					sb.append(Character.toChars(code));
					i += 5;
					continue;
				}
				i++;
			}
			sb.append(ch);
		}
		return sb.toString();
	}

	protected Class<?> getFieldType(String id) throws Exception {
		return getFieldTypes().get(id);
	}
	

	public void write(ModelBean[] beans, String key) throws MDAException, Exception {
		for (ModelBean bean : beans) {
			write(bean, key);
		}
	}

	public void write(ModelBean bean, String key) throws MDAException, Exception {
		RecordDefinition record = getRecordDefintion(key);
		if (record != null)
			write(bean, record);
	}

	public void write(ModelBean bean, RecordDefinition record) throws Exception {

	}
	protected RecordDefinition getRecordDefinition(final String key) throws MDAException, Exception {
		final String _method = "RecordDefinition getRecordDefinition(final String key)";

		RecordDefinition record = null;
		
		_enter(_method);
		
		final Map<String, RecordDefinition> records = getRecordDefinitions();
		if (records != null)
			record = records.get(key);

		_leave(_method);
		
		return record;
	}
	protected void _enter(final String method) {
		if (log.isTraceEnabled())
			log.trace(String.format("%s enter", method));
	}

	protected void _leave(final String method) {
		if (log.isTraceEnabled())
			log.trace(String.format("%s leave", method));
	}

	protected final Logger log = LogManager.getLogger(FileWriter.class);
	
	public void write(final Map<String, Object>[] maps, final String key) throws MDAException, Exception {
		final String _method = "void write(final Map<String, Object>[] maps, final String key)";

		_enter(_method);

		for (final Map<String, Object> map : maps) {
			write(map, key);
		}
		
		_leave(_method);
	}
	
	public void write(final Map<String, Object> map, final String key) throws Exception {
		final String _method = "void write(final Map<String, Object> map, final String key)";

		_enter(_method);

		final RecordDefinition record = getRecordDefinition(key);
		if (record != null)
			write(map, record);

		_leave(_method);
	}

	public void write(final Map<String, Object> map, final RecordDefinition record) throws Exception {
		final String _method = "void write(final Map<String, Object> map, final RecordDefinition record)";
		
		_enter(_method);
		
		_leave(_method);
	}
}
