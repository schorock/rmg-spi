package com.ric.insurance.interfaces.gl.stat;

import java.util.ArrayList;
import java.util.List;

import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.insurance.interfaces.gl.Dates;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceContext;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

/**
 * Handler for starting the GL Stat jobs
 * 
 * @author matthew.mcclellan
 *
 */
public class GLStatsHandler extends InnovationIBIZHandler {
	@Override
	public ModelBean process() throws IBIZException, ServiceHandlerException {
		try {
			GLStatsDatabase glDatabase = new GLStatsDatabase(ServiceContext.getServiceContext().getData());
    		ModelBean rs = this.getHandlerData().getResponse();            
    		ModelBean responseParams = rs.getBean("ResponseParams");
    		responseParams.getBean("Errors");
            ModelBean rq = this.getHandlerData().getRequest();
            AdditionalParams ap = new AdditionalParams(rq);
            
            String period = ap.gets("Period");
			String runDt = ap.gets("RunDt");

			List<ModelBean> missedStats = new ArrayList<ModelBean>();
			ArrayList<ModelBean> errors = new ArrayList<ModelBean>();
			if("Daily".equals(period)) {
				ArrayList<String> accountingDts = new Dates().getRunDates(runDt, errors);
				if (accountingDts.size() > 0) {
					String startDt = accountingDts.get(0);
					String endDt = accountingDts.get(accountingDts.size()-1);

					glDatabase.clearDailyStats(startDt, endDt);
					missedStats = glDatabase.createDailyStats(startDt, endDt);
				}
			} else if("Monthly".equals(period)) {
				StringDate runStringDt = new StringDate(runDt); 
				String reportPeriod = runStringDt.toStringDisplay("yyyyMM");
				
				glDatabase.clearMonthlyStats(reportPeriod);
				missedStats = glDatabase.createMonthlyStats(reportPeriod, runDt);
			} else {
				throw new IllegalStateException(String.format("Unknown stat period of [%s], expected Daily or Monthly", period));
			}
			
			if (errors.size() > 0) {
				for (ModelBean error: errors) {
	                addErrorMsg(error.gets("Name"), error.gets("Msg"), error.gets("Type"), error.gets("Severity"));
				}
			}
			
			// Warn about missing stats
			for(ModelBean bean: missedStats) {
				String transactionTypeCd = "";
				String errorMsg = "";
				if (bean.hasBeanField("TransactionTypeCd")){
					transactionTypeCd = bean.gets("TransactionTypeCd");
					errorMsg = String.format("Entry in stat table [%s] with SystemId [%s] did not get assigned to a transaction (TypeCd = %s)", bean.getBeanName(), bean.gets("SystemId"), transactionTypeCd);

				} else {
					errorMsg = String.format("Entry in stat table [%s] with SystemId [%s] did not get assigned to a transaction", bean.getBeanName(), bean.gets("SystemId"));
				}
				addErrorMsg("Missed Stat Entry", errorMsg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
			}

			return rs;
		} catch (Exception e) {
			throw new IBIZException(e);
		}
	}
}
