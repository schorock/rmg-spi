package com.ric.insurance.interfaces.gl.stat;

import java.util.ArrayList;
import java.util.List;

import com.iscs.common.render.DateRenderer;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.utility.error.ErrorTools;


import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceContext;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

/**
 * Handler for regenerating a large amount of gl stats
 * 
 * @author matthew.mcclellan
 *
 */
public class GLStatsRegenerateHandler extends InnovationIBIZHandler {
	@Override
	public ModelBean process() throws IBIZException, ServiceHandlerException {
		try {
			GLStatsDatabase glDatabase = new GLStatsDatabase(ServiceContext.getServiceContext().getData());
    		ModelBean rs = this.getHandlerData().getResponse();            
    		ModelBean responseParams = rs.getBean("ResponseParams");
    		responseParams.getBean("Errors");
            ModelBean rq = this.getHandlerData().getRequest();
            AdditionalParams ap = new AdditionalParams(rq);
            
			String startDt = ap.gets("StartDt");
			String endDt = ap.gets("EndDt");
			
			StringDate startStringDt = new StringDate(startDt);
			StringDate endStringDt = new StringDate(endDt);
			
			if(DateRenderer.greaterThan(startStringDt, endStringDt)) {
				throw new IllegalStateException(String.format("Start Date must be less than the End Date, [%s] was greater than [%s]", startStringDt, endStringDt));
			}

			List<ModelBean> missedStats = null;
			glDatabase.clearDailyStats(startDt, endDt);
			missedStats = glDatabase.createDailyStats(startDt, endDt);

			List<String> reportPeriods = getMonthList(startStringDt, endStringDt);
			for(String reportPeriod : reportPeriods) {
				glDatabase.clearMonthlyStats(reportPeriod);	
				missedStats.addAll(glDatabase.createMonthlyStats(reportPeriod, endDt));
			}
			
			// Warn about missing stats
			for(ModelBean bean: missedStats) {
				String transactionTypeCd = "";
				String errorMsg = "";
				if (bean.hasBeanField("TransactionTypeCd")){
					transactionTypeCd = bean.gets("TransactionTypeCd");
					errorMsg = String.format("Entry in stat table [%s] with SystemId [%s] did not get assigned to a transaction (TypeCd = %s)", bean.getBeanName(), bean.gets("SystemId"), transactionTypeCd);

				} else {
					errorMsg = String.format("Entry in stat table [%s] with SystemId [%s] did not get assigned to a transaction", bean.getBeanName(), bean.gets("SystemId"));
				}	
				addErrorMsg("Missed Stat Entry", errorMsg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
			}
			
			if(!missedStats.isEmpty()) {
				throw new IBIZException("Stat rows were unexpectedly missed during processing, review job errors");
			}

			return rs;
		} catch (Exception e) {
			throw new IBIZException(e);
		}
	}
	
	public List<String> getMonthList(StringDate startDt, StringDate endDt) {
			StringDate currentDt = new StringDate(startDt.toString());

			List<String> periods = new ArrayList<String>();
			while(DateRenderer.lessThan(currentDt, endDt)) {
				String reportPeriod = currentDt.toStringDisplay("yyyyMM");
				periods.add(reportPeriod);
				
				currentDt = DateRenderer.advanceMonth(currentDt, "1");
			}
			
			return periods;
	}
}
