package com.ric.insurance.interfaces.gl.stat;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.innovextechnology.service.Log;
import com.iscs.ar.HPAccount;
import com.iscs.common.business.company.Company;
import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDAUrl;
import com.iscs.common.tech.bean.data.SQLRelationDelegate;
import com.iscs.common.tech.bean.data.SQLRelationHelper;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.table.Cell;
import com.iscs.common.utility.table.Column;
import com.iscs.common.utility.table.Table;
import com.iscs.common.utility.table.Tables;
import com.iscs.common.utility.table.excel.ExcelReader;
import com.iscs.insurance.interfaces.gl.Dates;
import com.iscs.insurance.interfaces.gl.GLCustomerTables;
import com.iscs.insurance.interfaces.gl.TableTools;
import com.iscs.payables.account.BankAccount;

import net.inov.mda.MDAOption;
import net.inov.mda.MDATable;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.data.JDBCData;
import net.inov.tec.data.SQLDelegate;
import net.inov.tec.date.StringDate;

/**
 * Extracts stats from base stat tables and converts them into gl stats that are loaded back into the database 
 * 
 * @author matthew.mcclellan
 *
 */
public class GLStatsDatabase {
	protected final JDBCData data; 
	protected final SQLRelationHelper relationHelper;
	protected final HashMap<String, ModelBean> bankAccounts;
	protected final Set<String> billingAccountCds;
	protected final Tables glStatsTables;
	protected final Tables glCustTables;
	protected static final String IGNORED_CLAIM_TRANSACTIONS = "Adjust Reserve,Adjust Recovery,Deny";
	protected static final String IGNORED_PAYABLE_TRANSACTIONS = "Clear Payment,Unclear Payment,Submitted,Cancel Failed";
	public static final String DAILY_MARKER_NAME = "Daily Run End";
	public static final String MONTHLY_MARKER_NAME = "Monthly Run End";
	protected MDAOption[] BILLING_FEE_CATEGORIES = ((MDATable) Store.getModelObject("INGLStats::glstats-list::billing-fees-categories::all")).getOptions();
	protected static int BATCH_SIZE = 1000;
	
	protected class GLStatsBuilder {
		protected final ArrayList<String> fields;
		protected final String dateColumn;
		protected final boolean isSummary; 
		protected final List<ModelBean> stats = new ArrayList<ModelBean>();
		protected final String statSource;
		
		public List<String> getCustomerFields(String tableName) throws Exception {
			Table selectColumns = glCustTables.get("select-columns");
			
			if(selectColumns != null && selectColumns.hasColumn(tableName)) {
				ArrayList<String> columns = new ArrayList<String>();
				Column tableColumn = selectColumns.getColumn(tableName);

				for(Cell cell: tableColumn.getCells()) {
					if(cell.getValue() != null) {
						columns.add(cell.getString());
					}
				}

				return columns;
			} else {
				return Collections.emptyList();
			}
		}
		
		public GLStatsBuilder(String tableName, boolean isSummary) throws Exception {
			this.fields = TableTools.getColumnValues(glStatsTables.get("select-columns").getColumn(tableName));
			this.fields.addAll(getCustomerFields(tableName));
			this.dateColumn = TableTools.querySingleRow(glStatsTables.get("stat-table-settings"), new String[] {"TableName"}, new String[] {tableName}).getCell("DateColumn").getString();
			this.isSummary = isSummary;
			this.statSource = tableName;
		}
		
		protected void setDateField(ModelBean sourceBean, ModelBean glStatsBean) throws ModelBeanException {
			String targetField = isSummary ? "TransactionPeriod" : "TransactionDate";
			glStatsBean.setValue(targetField, sourceBean.getValue(dateColumn));
			
			// Populate the TransactionPeriod even for daily beans so that they
			// can optionally be used in monthly reporting
			if(!isSummary) {
				String period = sourceBean.getDate(dateColumn).toStringDisplay("yyyyMM");
				glStatsBean.setValue("TransactionPeriod", period);
			}
		}
		
		protected void setASLModified(ModelBean glStatsBean) throws ModelBeanException {
	
			String AnnualStatementLineCd = glStatsBean.gets("AnnualStatementLineCd");
			if(null != AnnualStatementLineCd){
				if(StringTools.in(AnnualStatementLineCd, "BS1,EN1,MBD,SLE")){
					glStatsBean.setValue("ASLModified","L01.FIRE");	
					}else if(AnnualStatementLineCd.equals("030")){
						glStatsBean.setValue("ASLModified", "L03.FO");
					}else if(AnnualStatementLineCd.equals("040")){
						glStatsBean.setValue("ASLModified","L04.HO");
					}else if(AnnualStatementLineCd.equals("051")){
						glStatsBean.setValue("ASLModified","L05.1.CMP");
					}else if(AnnualStatementLineCd.equals("052")){
						glStatsBean.setValue("ASLModified","L05.2.CMP");
					}else if(AnnualStatementLineCd.equals("171")){
						glStatsBean.setValue("ASLModified", "L17.1.OL");
					}else if(AnnualStatementLineCd.equals("192")){
						glStatsBean.setValue("ASLModified","L19.2.PPA-LIAB");
					}else if(AnnualStatementLineCd.equals("211")){
						glStatsBean.setValue("ASLModified","L21.1.PPA-PD");
					}else if(AnnualStatementLineCd.equals("034")){
						glStatsBean.setValue("ASLModified","L34.IPP");
					}else if(AnnualStatementLineCd.equals("021")){
						glStatsBean.setValue("ASLModified","L02.1.ALLIED");
					}else if(AnnualStatementLineCd.equals("LLL") || AnnualStatementLineCd.equals("PPL")){
						glStatsBean.setValue("ASLModified","L17.1.OL");
					}
				}
		}

		public ModelBean createStatBean(ModelBean source, String amount, String transactionName) throws Exception {
			ModelBean glStatsBean = new ModelBean("GLStats");
			for(String field: fields) {
				setGLStatsField(source, glStatsBean, field);	
			}	
			glStatsBean.setValue("Amount", amount);
			glStatsBean.setValue("TransactionName", transactionName);
			glStatsBean.setValue("StatSource", statSource);
			glStatsBean.setValue("StatSourceId", source.gets("SystemId"));

			setDateField(source, glStatsBean);
			setASLModified(glStatsBean);
			return glStatsBean;
		}

		protected void setGLStatsField(ModelBean source, ModelBean glStatsBean, String field) throws ModelBeanException {
			if (field.equals("ClaimantTransactionCd") && source.getBeanName().equals("ClaimStats")){
				glStatsBean.setValue("TransactionTypeCd", source.getValue(field));
			} else if (field.equals("AccountingDt") && StringTools.in(source.getBeanName(), "ClaimStats,PayableStats,SuspenseStats") ){	
				glStatsBean.setValue("AccountingDt", source.getValue("BookDt"));	
			} else if (field.equals("AccountingDt") && StringTools.in(source.getBeanName(), "AccountStats") ){	
				glStatsBean.setValue("AccountingDt", source.getValue("AccountingDt"));		
			} else if (field.equals("ProviderCd") && StringTools.in(source.getBeanName(), "AccountStats,PolicyReinsuranceSummaryStats,CommissionDetail") ){
				glStatsBean.setValue("ProducerCd", source.getValue(field));
			} else if (field.equals("TransactionCd") && StringTools.in(source.getBeanName(), "AccountingStats,AccountingSummaryStats") ){	
				glStatsBean.setValue("TransactionTypeCd", source.getValue(field));			
			} else if (field.equals("ProducerProviderCd") && StringTools.in(source.getBeanName(), "ClaimStats,ClaimSummaryStats") ){	
				glStatsBean.setValue("ProducerCd", source.getValue(field));
			} else if (field.equals("CommissionArea") && source.getBeanName().equals("CommissionDetail") ){
				glStatsBean.setValue("CommissionAreaCd", source.getValue(field));	
			} else if (field.equals("SourceNumber") && source.getBeanName().equals("CommissionDetail") ){
				glStatsBean.setValue("PolicyNumber", source.getValue(field));					
			} else {
				glStatsBean.setValue(field, source.getValue(field));
			}
		}

		/** Ready the marker row to be written out to the database to indicate closure to a GLStats run. Pass in false to the isDaily parameter to have it 
		 * be a marker for monthly runs
		 * @param transactionName The name you want to signify for the marker row to detect later in reporting
		 * @param runDt The current run date
		 * @param isDaily indicator if this marker is for a daily run
		 * @return The marker row record to be written out
		 * @throws Exception when an error occurs
		 */
		public ModelBean createMarkerRow(String transactionName, String transactionDate, String accountingDate, boolean isDaily) throws Exception {
			ModelBean glStatsBean = new ModelBean("GLStats");
			glStatsBean.setValue("Amount", "0.00");
			glStatsBean.setValue("TransactionName", transactionName);
			StringDate accountingDt = Dates.getStringDate(accountingDate);
			glStatsBean.setValue("AccountingDt", accountingDt);
			glStatsBean.setValue("StatSource", "GLStats");
			if (isDaily) {
				StringDate statDt = Dates.getStringDate(transactionDate);
				String period = statDt.toStringDisplay("yyyyMM");
				glStatsBean.setValue("TransactionDate", statDt);
				glStatsBean.setValue("TransactionPeriod", period);
			} else {
				glStatsBean.setValue("TransactionPeriod", transactionDate);
			}
			
			return glStatsBean;
		}
		
		public void add(ModelBean source, String amount, String transactionName) throws Exception {
			stats.add(createStatBean(source, amount, transactionName));
		}
		
		public void add(ModelBean glStatsBean) {
			stats.add(glStatsBean);
		}
		
		public void add(ModelBean source, String amount, String transactionName, String billMethod) throws Exception, ModelBeanException {
			ModelBean glStatBean = createStatBean(source, amount, transactionName);
			glStatBean.setValue("BillMethod", billMethod);
			stats.add(glStatBean);
		}
		
		public List<ModelBean> getBeans() {
			return this.stats;
		}
		
		public int getSize() {
			return this.stats.size();
		}
	}
	
	protected static class GLStatsTableResult {
		protected final List<ModelBean> glStatsBeans;
		protected final List<ModelBean> missedSourceBeans;

		public GLStatsTableResult(List<ModelBean> glStatsBeans, List<ModelBean> missedSourceBeans) {
			super();
			this.glStatsBeans = glStatsBeans;
			this.missedSourceBeans = missedSourceBeans;
		}
	}
	
	protected HashMap<String, ModelBean> buildAccountsMap(JDBCData data) throws Exception {
		ModelBean[] accounts = new BankAccount().getAccounts(data, false);
		HashMap<String, ModelBean> accountMap = new HashMap<String, ModelBean>();
		
		for(ModelBean account: accounts) {
			accountMap.put(account.gets("AccountCd"), account);
		}
		
		return accountMap;
	}
	
	protected Set<String> buildBillingAccountCds() throws Exception {
		Company company = Company.getCompany();
		Set<String> billingAccounts = new HashSet<String>();

		for(ModelBean carrier : company.getBean().getBeans("Carrier")) {
			billingAccounts.add(carrier.gets("BillingBankAccount"));
		}
		
		return billingAccounts;
	}
	
	public GLStatsDatabase(JDBCData data) throws Exception {
		super();
		this.data = data;
		this.bankAccounts = buildAccountsMap(this.data);

		SQLRelationDelegate delegate = SQLRelationHelper.getDelegate(this.data);
		this.relationHelper = new SQLRelationHelper(this.data, delegate);
		this.billingAccountCds = buildBillingAccountCds();

		MDAUrl statTableMda = (MDAUrl) Store.getModelObject("INGLStats::model-url::materials::gl-stats-tables.xml");
		ExcelReader excelReader = new ExcelReader();
		this.glStatsTables = excelReader.read(statTableMda.getURL()); 
		
		this.glCustTables = GLCustomerTables.getTables();
	}
	
	public void clearDailyStats(String startDt, String endDt) throws Exception {
		this.data.deleteStatement(String.format("DELETE FROM GLStats WHERE TransactionDate >= '%s' AND TransactionDate <= '%s'", startDt, endDt));
	}
	
	public void clearMonthlyStats(String reportPeriod) throws Exception {
		this.data.deleteStatement(String.format("DELETE FROM GLStats WHERE TransactionPeriod = '%s' AND TransactionDate is NULL", reportPeriod));
	}
	
	/** Create the daily stats between the start and end dates given.  
	 * @param startDt The date to start picking up data to create GL stats
	 * @param endDt The end date
	 * @return The list of missed source beans
	 * @throws Exception when an error occurs
	 */
	public List<ModelBean> createDailyStats(String startDt, String endDt) throws Exception {
		
		List<ModelBean> missedSourceBeans = null;
		missedSourceBeans = insertGlStatsBatch(startDt, endDt, null, "AccountingStats", this::addAccountingGLStatBean);
		missedSourceBeans.addAll(insertGlStatsBatch(startDt, endDt, null, "ClaimStats", this::addClaimGLStatsBean));
		missedSourceBeans.addAll(insertGlStatsBatch(startDt, endDt, null, "ClaimReinsuranceStats", this::addClaimReinsuranceGLStatsBean));
		missedSourceBeans.addAll(insertGlStatsBatch(startDt, endDt, null, "AccountStats", this::addAccountGLStatBean));
		missedSourceBeans.addAll(insertGlStatsBatch(startDt, endDt, null, "CommissionDetail", this::addCommissionDetailGLStatBean));
		missedSourceBeans.addAll(insertGlStatsBatch(startDt, endDt, null, "SuspenseStats", this::addSuspenseGLStatBean));
		missedSourceBeans.addAll(insertGlStatsBatch(startDt, endDt, null, "PayableStats", this::addPayableGLStatBean));
		
		// Write out a marker row to allow detection of run end
		GLStatsBuilder statBeans = new GLStatsBuilder("AccountingStats", false);
		List<String> dateList = new Dates().getRunDateList(startDt, endDt);
		for (String date : dateList) {
			ModelBean statBean = statBeans.createMarkerRow(DAILY_MARKER_NAME, date, endDt, true);
			relationHelper.saveBean(statBean);
		}

		data.commit();
		return missedSourceBeans;
	}
	
	/** Create the monthly stats given the current month and run date to record when it was done
	 * 
	 * @param reportPeriod The month and year to create the monthly stats for
	 * @param runDate The run date of when the monnth stats were recorded
	 * @return The list of records created
	 * @throws Exception when an error occurs
	 */
	public List<ModelBean> createMonthlyStats(String reportPeriod, String runDate) throws Exception {
		List<ModelBean> missedSourceBeans = null;
		missedSourceBeans = insertGlStatsBatch(null, null, reportPeriod, "AccountingSummaryStats", this::addAccountingSummaryGLStatBean);
		missedSourceBeans.addAll(insertGlStatsBatch(null, null, reportPeriod, "ClaimSummaryStats", this::addClaimSummaryGLStatBean));
		missedSourceBeans.addAll(insertGlStatsBatch(null, null, reportPeriod, "ClaimReinsuranceSummaryStats", this::addClaimReinsuranceSummaryGLStatBean));
		missedSourceBeans.addAll(insertGlStatsBatch(null, null, reportPeriod, "PolicyReinsuranceSummaryStats", this::addPolicyReinsuranceSummaryGLStatBean));
		missedSourceBeans.addAll(insertGlStatsBatch(null, null, reportPeriod, "AccountSummaryStats", this::addAccountSummaryGLStatBean));

		// Write out a marker row to allow detection of run end
		GLStatsBuilder statBeans = new GLStatsBuilder("AccountingStats", false);	
		ModelBean statBean = statBeans.createMarkerRow(MONTHLY_MARKER_NAME, reportPeriod, runDate, false);
		relationHelper.saveBean(statBean);
		
		data.commit();
		return missedSourceBeans;
	}
	
	protected PreparedStatement fetchSourceBeanPs(String table, String startDt, String endDt, long minSystemId) throws Exception {
		String dateColumn = TableTools.querySingleRow(glStatsTables.get("stat-table-settings"), new String[] {"TableName"}, new String[] {table}).getCell("DateColumn").getString();

		return this.relationHelper.selectModelBeanPs(table, new String[] {dateColumn, dateColumn, "SystemId"}, new String[] {">=", "<=", ">="}, new String[] {formatDateforSQL(startDt), formatDateforSQL(endDt), String.valueOf(minSystemId)}, BATCH_SIZE);
	}
	
	protected PreparedStatement fetchSummarySourceBeanPs(String table, String reportPeriod, long minSystemId) throws Exception {
		String dateColumn = TableTools.querySingleRow(glStatsTables.get("stat-table-settings"), new String[] {"TableName"}, new String[] {table}).getCell("DateColumn").getString();

		return this.relationHelper.selectModelBeanPs(table, new String[] {dateColumn, "SystemId"}, new String[] {"=", ">="}, new String[] {reportPeriod, String.valueOf(minSystemId)}, BATCH_SIZE);
	}
	
	protected String getAccountType(String sourceCd, String dividendPaymentInd) throws Exception {
		if("Billing".equals(sourceCd)) {
			if( dividendPaymentInd.equalsIgnoreCase("Yes") ){
				return "Dividend";
			} else {				
				return "Refund";
			}
		} else if("Claims".equals(sourceCd)) {
			return "Claim";
		} else if("Commission".equals(sourceCd)) {
			return "Commission";
		} else {
			throw new IllegalStateException(String.format("Unknown sourceCd [%s]", sourceCd));
		}
	}
	
	/** Formats the date 
	 * @param dateObject The date 
	 * @throws SQLException 
	 */
	public String formatDateforSQL(Object dateObject) throws SQLException  {
		StringDate date = null;
		if (dateObject instanceof StringDate){
			date = (StringDate) dateObject;
		} else if (dateObject instanceof String){
			date = new StringDate((String)dateObject);
		} else {
			throw new SQLException("Class '" + dateObject.getClass().getName() + "' not supported in formatDateforSQL function");
		}
		SQLDelegate delegate = new JDBCData().getDelegate();
		return delegate.formatDate(date.getCalendar(), false, false );
	}
	
	protected boolean isCompanyBillingAccount(String accountNumber) {
		return this.billingAccountCds.contains(accountNumber);
	}
	
	protected boolean isExternalAccount(String accountCd) throws ModelBeanException {
		ModelBean bankAccount = this.bankAccounts.get(accountCd);
		
		if(bankAccount == null) {
			throw new IllegalStateException(String.format("Cannot find bank account with AccountCd [%s]", accountCd));
		}
		
		return StringTools.isTrue(bankAccount.gets("ExternalPayableInd"));
	}

	protected void addPayableGLStatBean(GLStatsBuilder statBeans, List<ModelBean> missedBeans, ModelBean sourceBean) throws ModelBeanException, Exception {
		int originalSize = statBeans.getSize();
		String accountCd = sourceBean.gets("PaymentAccountCd");
		String paymentMethodCd = sourceBean.gets("PaymentMethodCd");
		
		String transactionTypeCd = sourceBean.gets("TransactionTypeCd");
		boolean externalAccountInd = isExternalAccount(accountCd);
		boolean notExternallyPaid = (externalAccountInd && !StringTools.in(paymentMethodCd, "Check - Batch,ACH")) || !externalAccountInd;
		if(notExternallyPaid) {
			String accountType = getAccountType(sourceBean.gets("SourceCd"), sourceBean.gets("DividendPaymentInd"));

			boolean isIssuePayment = StringTools.in(transactionTypeCd, "Print,Reprint,Open");
			boolean isStopPayment = StringTools.in(transactionTypeCd, "Stop Payment,Void Payment");
			boolean isUnstopPayment = StringTools.in(transactionTypeCd, "Unstop Payment,Unvoid Payment");
			boolean isEscheatPayment = transactionTypeCd.equals("Escheat");
			boolean isUnescheatPayment = transactionTypeCd.equals("Unescheat");
			
			if(isIssuePayment) {
				statBeans.add(sourceBean, sourceBean.gets("AllocationAmt"), accountType + " Issued");
			} else if(isStopPayment) {
				statBeans.add(sourceBean, sourceBean.gets("AllocationAmt"), "Void Stop " + accountType + " Payments");
			} else if(isUnstopPayment) {
				statBeans.add(sourceBean, sourceBean.gets("AllocationAmt"), "Unvoid Unstop " + accountType + " Payments");
			} else if( isEscheatPayment) {
				statBeans.add(sourceBean, sourceBean.gets("AllocationAmt"), "Escheat " + accountType + " Payments");
			} else if( isUnescheatPayment) {
				statBeans.add(sourceBean, sourceBean.gets("AllocationAmt"), "Unescheat " + accountType + " Payments");
			}
		}

		// Only report missing if they are not an external account, the external accounts are handled by accounts payable interface
		if(notExternallyPaid && originalSize == statBeans.getSize() && !StringTools.in(transactionTypeCd, IGNORED_PAYABLE_TRANSACTIONS)) {
			missedBeans.add(sourceBean);
		}
	}

	protected void addSuspenseGLStatBean(GLStatsBuilder statBeans, List<ModelBean> missedBeans, ModelBean sourceBean) throws ModelBeanException, Exception {
		int originalSize = statBeans.getSize();
		String transactionTypeCd = sourceBean.gets("TransactionTypeCd");
		Boolean recreateSuspense = StringTools.isTrue(sourceBean.gets("RecreateSuspenseInd"));
		
		if("CreateSuspense".equals(transactionTypeCd) && !recreateSuspense) {
			statBeans.add(sourceBean, sourceBean.gets("PendingAmt"), "Suspense Receipts");
		}
		
		String activityTypeCd = sourceBean.gets("ActivityTypeCd", "");
		String accountNumber = sourceBean.gets("AccountNumber");
		boolean isCompanyBillingAccount = isCompanyBillingAccount(accountNumber);
		if(  ("SuspenseRefund".equals(transactionTypeCd) && !isCompanyBillingAccount) 
		  || ("CreateSuspense".equals(transactionTypeCd) && recreateSuspense && activityTypeCd.isEmpty())
		  || ("SuspenseDelete".equals(transactionTypeCd)) ) {
			statBeans.add(sourceBean, sourceBean.gets("PendingAmt"), "Suspense Receipts Reversal");
		}
		
		if("SuspenseRefund".equals(transactionTypeCd) && isCompanyBillingAccount) {
			statBeans.add(sourceBean, sourceBean.gets("PendingAmt"), "Suspense Refunds");
		}
		
		if("SuspenseRefundReversed".equals(transactionTypeCd)) {
			statBeans.add(sourceBean, sourceBean.gets("PendingAmt"), "Suspense Refunds Reversal");
		}
		
		if("SuspensePost".equals(transactionTypeCd) 
				|| ("CreateSuspense".equals(transactionTypeCd) && recreateSuspense && !activityTypeCd.isEmpty())) {
			ModelBean glStatsBean = statBeans.createStatBean(sourceBean, sourceBean.gets("PendingAmt"), "Susp Cash To AR-Remove From Suspense");
			glStatsBean.setValue("TransferCarrier", getSuspenseTransferTo(sourceBean));
			statBeans.add(glStatsBean);
		}
		
		if("TransferCash".equals(transactionTypeCd)) {
			ModelBean glStatsBean = statBeans.createStatBean(sourceBean, sourceBean.gets("PendingAmt"), "AR To Susp-Apply To Suspense");
			glStatsBean.setValue("TransferCarrier", getSuspenseTransferTo(sourceBean));
			statBeans.add(glStatsBean);
		}
		
		if(originalSize == statBeans.getSize()) {
			missedBeans.add(sourceBean);
		}
	}
	
	protected String getAccountTransferTo(ModelBean sourceBean) throws Exception {
		ModelBean[] suspenseStats = this.relationHelper.selectModelBean(
			"SuspenseStats", 
			new String[] {"SystemCheckNumber", "BookDt"}, 
			new String[] {"=", "="}, new String[] {sourceBean.gets("SystemCheckNumber"), formatDateforSQL(sourceBean.getDate("BookDt"))}, 
			0);
		
		if(suspenseStats.length > 0) {
			return findAccountCarrierCd(suspenseStats[0].gets("SourceCd"));
		} else {
			return null;
		}
	}

	protected String getSuspenseTransferTo(ModelBean sourceBean) throws Exception {
		ModelBean[] accountStats = this.relationHelper.selectModelBean(
			"AccountStats", 
			new String[] {"SystemCheckNumber", "BookDt"}, 
			new String[] {"=", "="}, new String[] {sourceBean.gets("SystemCheckNumber"), formatDateforSQL(sourceBean.getDate("BookDt"))}, 
			0);
		
		if(accountStats.length > 0) {
			return accountStats[0].gets("CarrierCd");
		} else {
			return null;
		}
	}

	protected void addCommissionDetailGLStatBean(GLStatsBuilder statBeans, List<ModelBean> missedBeans, ModelBean sourceBean) throws ModelBeanException, Exception {
		int originalSize = statBeans.getSize();
		boolean isCorrection = StringTools.isTrue(sourceBean.gets("CorrectionInd"));

		if(isCorrection) {
			if("Refund Received".equals(sourceBean.gets("CorrectionReasonCd"))) {
				statBeans.add(sourceBean, sourceBean.gets("CommissionAmt"), "Commission Refunds Received");
			} else {
				statBeans.add(sourceBean, sourceBean.gets("CommissionAmt"), "Commission Adjustment");
			}
		}
		
		if(originalSize == statBeans.getSize() && isCorrection) {
			missedBeans.add(sourceBean);
		}
	}

	protected void addClaimReinsuranceGLStatsBean(GLStatsBuilder statBeans, List<ModelBean> missedBeans, ModelBean sourceBean) throws ModelBeanException, Exception {
		int originalSize = statBeans.getSize();

		// TODO: When InsuranceTypeCd is added to the stats table this function will change,
		// for now we assume all claim reinsurance is Ceded
		String reserveTypeCd = sourceBean.gets("ReserveTypeCd");
		boolean isIndemnityReserve = "Indemnity".equals(reserveTypeCd);
		boolean isAdjustmentReserve = "Adjustment".equals(reserveTypeCd);
		String paidAmt = sourceBean.gets("PaidAmt");

		if(isIndemnityReserve) {
			statBeans.add(sourceBean, paidAmt, "Ceded Paid Losses");
		}

		if(isAdjustmentReserve) {
			statBeans.add(sourceBean, paidAmt, "Ceded Paid Loss Expense");
		}

		boolean isReinsuranceRecoveryReserve = "ReinsuranceRecovery".equals(reserveTypeCd);
		String recoveryTransactionCd = sourceBean.gets("RecoveryTransactionCd");
		if(isReinsuranceRecoveryReserve && StringTools.in(recoveryTransactionCd, "Recovery Post,Reverse NSF,Reverse Stop Recovery")) {
			String postedRecoveryAmt = sourceBean.gets("PostedRecoveryAmt");
			statBeans.add(sourceBean, postedRecoveryAmt, "Ceded Paid Recoveries Received");
		}
		
		if(isReinsuranceRecoveryReserve && StringTools.in(recoveryTransactionCd, "NSF Recovery,Stop Recovery")) {
			String postedRecoveryAmt = sourceBean.gets("PostedRecoveryAmt");
			statBeans.add(sourceBean, postedRecoveryAmt, "Ceded Paid Recoveries Reversed");
		}

		if(originalSize == statBeans.getSize() && !StringTools.in(recoveryTransactionCd, IGNORED_CLAIM_TRANSACTIONS)) {
			missedBeans.add(sourceBean);
		}
	}

	protected void addClaimReinsuranceSummaryGLStatBean(GLStatsBuilder statBeans, List<ModelBean> missedBeans, ModelBean sourceBean) throws ModelBeanException, Exception {
		int originalSize = statBeans.getSize();

		String reserveTypeCd = sourceBean.gets("ReserveTypeCd");
		boolean isIndemnityReserve = "Indemnity".equals(reserveTypeCd);
		boolean isAdjustmentReserve = "Adjustment".equals(reserveTypeCd);
		String outstandingAmt = sourceBean.gets("OutstandingAmt");
		
		if(isIndemnityReserve) {
			statBeans.add(sourceBean, outstandingAmt, "Ceded Unpaid Losses");
		}
		
		if(isAdjustmentReserve) {
			statBeans.add(sourceBean, outstandingAmt, "Ceded Unpaid ALAE");
		}

		// We ignore recovery reserves, these are all picked up in the non-summary ClaimReinsuranceStats
		if(originalSize == statBeans.getSize() && !"ReinsuranceRecovery".equals(reserveTypeCd)) {
			missedBeans.add(sourceBean);
		}
	}

	protected void addClaimGLStatsBean(GLStatsBuilder statBeans, List<ModelBean> missedBeans, ModelBean sourceBean) throws ModelBeanException, Exception {
		int originalSize = statBeans.getSize();
		String insuranceType = sourceBean.gets("InsuranceTypeCd");
		String claimantTransactionCd = sourceBean.gets("ClaimantTransactionCd");
		String paidAmt = sourceBean.gets("PaidAmt");
		boolean isZeroPaid = (StringTools.parseBigDecimal(paidAmt).compareTo(BigDecimal.ZERO) == 0);
		String postedRecoveryAmt = sourceBean.gets("PostedRecoveryAmt");
		boolean isZeroPostedRecoveryAmt = (StringTools.parseBigDecimal(postedRecoveryAmt).compareTo(BigDecimal.ZERO) == 0);
		
		if (StringTools.in(insuranceType, "Assumed,Direct")) {
			String reserveTypeCd = sourceBean.gets("ReserveTypeCd");

			boolean isPaymentTransactionCd = StringTools.in(claimantTransactionCd, "Payment,Fast Track,Reverse Stop Payment,Reverse Void");
			boolean isPaymentReversedCd = StringTools.in(claimantTransactionCd, "Stop Payment,Void Payment");
			boolean isIndemnityReserve = "Indemnity".equals(reserveTypeCd);

			
			// Loss Payments
			if(isIndemnityReserve && isPaymentTransactionCd) {
				statBeans.add(sourceBean, paidAmt, insuranceType + " Loss Payments Requested");
			}
			
			// Loss Payments Reversed
			if(isIndemnityReserve && isPaymentReversedCd) {
				statBeans.add(sourceBean, paidAmt, insuranceType + " Loss Payments Reversed");
			}
			
			// Loss Expense Payments Requested
			boolean isAdjustmentReserve = "Adjustment".equals(reserveTypeCd);
			if( isAdjustmentReserve && isPaymentTransactionCd) {
				statBeans.add(sourceBean, paidAmt, insuranceType + " LAE Payments Requested");
			}
			
			// Loss Expense Payments Reversed				
			if( isAdjustmentReserve && isPaymentReversedCd) {
				statBeans.add(sourceBean, paidAmt, insuranceType + " LAE Payments Reversed");
			}
			
			
			// Claim Recoveries Received
			boolean isRecoveryReserve = sourceBean.valueEquals("ReserveTypeCd", "Recovery");
			boolean isRecoveriesReceivedTransactionCd = StringTools.in(claimantTransactionCd, "Recovery Post,Reverse NSF,Reverse Stop Recovery");
			if( isRecoveryReserve && isRecoveriesReceivedTransactionCd) {
				statBeans.add(sourceBean, postedRecoveryAmt, insuranceType + " Claim Recoveries Received");
			}
			
			// Claim Recoveries Reversed
			boolean isRecoveriesReversedTransactionCd = StringTools.in(claimantTransactionCd, "NSF Recovery,Stop Recovery");
			if( isRecoveryReserve && isRecoveriesReversedTransactionCd) {
				statBeans.add(sourceBean, postedRecoveryAmt, insuranceType + " Claim Recoveries Reversed");
			}
			
			// Claim Recovery Refunds Requested
			boolean isRecoveryRefundsTransactionCd = StringTools.in(claimantTransactionCd, "Payment,Fast Track,Reverse Stop Payment,Reverse Void");
			if( isRecoveryReserve && isRecoveryRefundsTransactionCd) {
				statBeans.add(sourceBean, postedRecoveryAmt, insuranceType + " Claim Refunds Requested");
			}
			
			// Claim Recovery Refunds Reversed
			boolean isRecoveryRefundsReversedTransactionCd = StringTools.in(claimantTransactionCd, "Stop Payment,Void Payment");
			if( isRecoveryReserve && isRecoveryRefundsReversedTransactionCd) {
				statBeans.add(sourceBean, postedRecoveryAmt, insuranceType + " Claim Refunds Reversed");
			}
			
			boolean isMiscReserveType = !StringTools.in(reserveTypeCd, "Indemnity,Adjustment,Recovery");
			boolean isMiscTransactionCd = !StringTools.in(claimantTransactionCd, "Payment,Fast Track,Stop Payment,Void Payment,Unstop Payment,Unvoid Payment,Reverse Stop Payment,Reverse Void");
			
			if((isMiscReserveType || isMiscTransactionCd) && !isZeroPaid) {
				statBeans.add(sourceBean, paidAmt, insuranceType + " Loss And ALAE Misc Paid Adjust");
			}
		}

		// Some claim transactions can be safely ignored
		if(originalSize == statBeans.getSize() && !StringTools.in(claimantTransactionCd, IGNORED_CLAIM_TRANSACTIONS)) {
			if (!isZeroPaid && !isZeroPostedRecoveryAmt){//No need to consider the bean as missed if there are no paid amounts
				missedBeans.add(sourceBean);
			}	
		}
	}

	protected void addClaimSummaryGLStatBean(GLStatsBuilder statBeans, List<ModelBean> missedBeans, ModelBean sourceBean)	throws ModelBeanException, Exception {
		int originalSize = statBeans.getSize();
		String insuranceType = sourceBean.gets("InsuranceTypeCd");
		String reserveTypeCd = sourceBean.gets("ReserveTypeCd");
		boolean isIndemnityReserve = "Indemnity".equals(reserveTypeCd);
		boolean isAdjustmentReserve = "Adjustment".equals(reserveTypeCd);
		boolean isRecoveryReserve = "Recovery".equals(reserveTypeCd);

		if(StringTools.in(insuranceType, "Direct,Assumed")) {
			// Unpaid Losses
			if(isIndemnityReserve) {
				statBeans.add(sourceBean, sourceBean.gets("OutstandingAmt"), insuranceType + " Unpaid Losses");
			}
			
			// Unpaid Loss Expense
			if(isAdjustmentReserve) {
				statBeans.add(sourceBean, sourceBean.gets("OutstandingAmt"), insuranceType + " Unpaid Loss Expense");
			}

			// Expected Recoveries
			if(isRecoveryReserve) {
				statBeans.add(sourceBean, sourceBean.gets("OutstandingExpectedRecoveryAmt"), insuranceType + " Expected Recoveries");
			}
		}

		if(originalSize == statBeans.getSize()) {
			missedBeans.add(sourceBean);
		}
	}

	protected void addAccountingGLStatBean(GLStatsBuilder statBeans, List<ModelBean> missedBeans, ModelBean sourceBean) throws ModelBeanException, Exception {
		int originalSize = statBeans.getSize();
		
		String billMethod = sourceBean.gets("BillMethod");
		if (StringTools.in(billMethod, "Insured,Mortgagee")){
			billMethod = "Direct";
		} else if (StringTools.in(billMethod, "Agent")){
			billMethod = "Agency";
		}
		
		String insuranceType = sourceBean.gets("InsuranceTypeCd");
		if(StringTools.in(insuranceType, "Direct,Assumed")) {
			statBeans.add(sourceBean, sourceBean.gets("WrittenPremiumAmt"), insuranceType + " Written Premium", billMethod);
			statBeans.add(sourceBean, sourceBean.gets("WrittenPremiumFeeAmt"), insuranceType + " Written Fees and Taxes", billMethod);
			statBeans.add(sourceBean, sourceBean.gets("WrittenCommissionAmt"), insuranceType + " Written Commission Expense", billMethod);
			statBeans.add(sourceBean, sourceBean.gets("WrittenCommissionFeeAmt"), insuranceType + " Written Commission Expense", billMethod);
		}
		
		if(statBeans.getSize() == originalSize) {
			missedBeans.add(sourceBean);
		}
	}
	
	@FunctionalInterface
	public interface TriConsumer<GLStatsBuilder, List, ModelBean> {
	    void apply(GLStatsBuilder statBeans, List missedBeans, ModelBean sourceBean) throws Exception;
	}
	
	protected List<ModelBean> insertGlStatsBatch(String startDt, String endDt, String reportPeriod, String tableName, TriConsumer<GLStatsBuilder, List, ModelBean> triConsumer) throws Exception {
		List<ModelBean> missedBeans = new ArrayList<ModelBean>();
		long minSystemId = 0;
		int resultCnt = 1;

		while (resultCnt > 0) {
			resultCnt = 0;
			ResultSet rs = null;
			PreparedStatement ps = null;
			try {
				boolean isSummary =false;
				if (reportPeriod != null && !reportPeriod.isEmpty()) {
					ps = fetchSummarySourceBeanPs(tableName, reportPeriod, minSystemId);
					rs = ps.executeQuery();
					isSummary = true;
				} else if (startDt != null && !startDt.isEmpty() && endDt != null && !endDt.isEmpty()){
					ps = fetchSourceBeanPs(tableName, startDt, endDt, minSystemId);
					rs = ps.executeQuery();
				}
				
				GLStatsBuilder statBeans = new GLStatsBuilder(tableName, isSummary);
				ModelBean sourceBean = null;
				
				while (rs.next()) {
					sourceBean = new ModelBean(tableName);
					this.relationHelper.selectBeanFromResult(rs, sourceBean);
					
					//Lambda for calling method. Like: addAccountingSummaryGLStatBean(statBeans, missedBeans, sourceBean);
					triConsumer.apply(statBeans, missedBeans, sourceBean);
					
					resultCnt++;
				}
				if (sourceBean != null) {
					minSystemId = Integer.parseInt(sourceBean.getSystemId()) + 1;
				}
				for (ModelBean bean: statBeans.getBeans()) {
					if (StringTools.parseBigDecimal(bean.gets("Amount")).compareTo(BigDecimal.ZERO) != 0) {
						relationHelper.saveBean(bean);
					}
				}
				this.data.commit();
			} catch (SQLException e) {
				Log.error(e);
				throw data.getDelegate().throwSQLException(e, null, "selectModelBean " + tableName);
			} finally {
				if (rs != null) {
					try {rs.close(); rs = null;} catch (Exception e) {Log.error(e);}
				}
				if (ps != null) {
					try {ps.close(); ps = null;} catch (Exception e) {Log.error(e);}
				}
			}
		}

		return missedBeans;
	}

	protected void addAccountingSummaryGLStatBean(GLStatsBuilder statBeans, List<ModelBean> missedBeans, ModelBean sourceBean) throws ModelBeanException, Exception {
		int originalSize = statBeans.getSize();
		
		String billMethod = sourceBean.gets("BillMethod");
		if (StringTools.in(billMethod, "Insured,Mortgagee")){
			billMethod = "Direct";
		} else if (StringTools.in(billMethod, "Agent")){
			billMethod = "Agency";
		}
		
		String insuranceType = sourceBean.gets("InsuranceTypeCd");
		if (StringTools.in(insuranceType, "Direct,Assumed")) {
			String amount = "";
			String ttdWrittenPremiumAmt = "";
			String ttdWrittenCommissionAmt = "";
			if (!sourceBean.gets("CoverageCd").isEmpty()){
				statBeans.add(sourceBean, sourceBean.gets("UnearnedAmt"), insuranceType + " Unearned Premium", billMethod);
				ttdWrittenPremiumAmt = sourceBean.gets("TTDWrittenPremiumAmt");
				ttdWrittenCommissionAmt = sourceBean.gets("TTDWrittenCommissionAmt");
			} else {
				statBeans.add(sourceBean, sourceBean.gets("UnearnedAmt"), insuranceType + " Unearned Fees", billMethod);
				ttdWrittenPremiumAmt = sourceBean.gets("TTDWrittenPremiumFeeAmt");
				ttdWrittenCommissionAmt = sourceBean.gets("TTDWrittenCommissionFeeAmt");
			}					
			
			String unearnedAmt = sourceBean.gets("UnearnedAmt");					
			String ratio = divide(unearnedAmt, ttdWrittenPremiumAmt);
			amount = StringTools.multiply(ratio, ttdWrittenCommissionAmt, 2);
			statBeans.add(sourceBean, amount, insuranceType + " Unearned Commission", billMethod);
		}
		
		if (originalSize == statBeans.getSize()) {
			missedBeans.add(sourceBean);
		}
	}

	protected void addPolicyReinsuranceSummaryGLStatBean(GLStatsBuilder statBeans, List<ModelBean> missedBeans, ModelBean sourceBean) throws ModelBeanException, Exception {
		int originalSize = statBeans.getSize();
		String insuranceType = sourceBean.gets("InsuranceTypeCd");
		
		if(StringTools.in(insuranceType, "Ceded,Assumed")) {
			statBeans.add(sourceBean, sourceBean.gets("MTDWrittenPremiumAmt"), insuranceType + " Written Premium");
			statBeans.add(sourceBean, sourceBean.gets("MTDWrittenCommissionAmt"), insuranceType + " Commission Income");
			
			String unearnedAmt = sourceBean.gets("UnearnedAmt");
			statBeans.add(sourceBean, unearnedAmt, insuranceType + " Unearned Premium");
			
			String ttdWrittenPremiumAmt = sourceBean.gets("TTDWrittenPremiumAmt");
			String ttdWrittenCommissionAmt = sourceBean.gets("TTDWrittenCommissionAmt");
			String ratio = divide(unearnedAmt, ttdWrittenPremiumAmt);
			String amount = StringTools.multiply(ratio, ttdWrittenCommissionAmt, 2);
			statBeans.add(sourceBean, amount, insuranceType + " Unearned Commission");
		}

		if(originalSize == statBeans.getSize()) {
			missedBeans.add(sourceBean);
		}
	}

	protected void addAccountSummaryGLStatBean(GLStatsBuilder statBeans, List<ModelBean> missedBeans,	ModelBean sourceBean) throws Exception, ModelBeanException {
		int originalSize = statBeans.getSize();
		statBeans.add(sourceBean, sourceBean.gets("FuturePaidEffectiveAmt"), "Advanced Cash Liability");
		statBeans.add(sourceBean, sourceBean.gets("Over90AmtEffectiveDt"), "Non-Admitted Receivables");

		if(originalSize == statBeans.getSize()) {
			missedBeans.add(sourceBean);
		}
	}
	
	protected static String getARReceiptType(ModelBean accountStat) throws ModelBeanException {
		String sourceCd = accountStat.gets("SourceCd");
		boolean isACHSourceCD = StringTools.equals(sourceCd, "ACH");
		boolean isACHLockBoxSourceCd = StringTools.in(sourceCd, "ACH,Lockbox");
		boolean isLockboxSourceCD = StringTools.equals(sourceCd, "Lockbox");

		String depositoryLocationCd = accountStat.gets("DepositoryLocationCd");
		boolean isDepositoryACH = StringTools.equals(depositoryLocationCd, "ACH");
		boolean isDepositoryCC = StringTools.equals(depositoryLocationCd, "CC");
		
		boolean isAgentTrust = StringTools.isTrue(accountStat.gets("ACHAgent"));

		if(isACHSourceCD && !isAgentTrust || (!isACHLockBoxSourceCd && isDepositoryACH)) {
			return "ACH";
		} else if(isACHSourceCD && isAgentTrust) {
			return "AgentTrust";
		} else if(!isACHLockBoxSourceCd && isDepositoryCC) {
			return "CC";
		} else if(isLockboxSourceCD) {
			return "Lockbox";
		} else {
			return "Other";
		} 
	}

	protected void addAccountGLStatBean(GLStatsBuilder statBeans, List<ModelBean> missedBeans, ModelBean sourceBean) throws ModelBeanException, Exception {
		int originalSize = statBeans.getSize();			
		
		String categoryCd = sourceBean.gets("CategoryCd");
		String chargeAmt = sourceBean.gets("ChargeAmt");
		String adjustmentAmt = sourceBean.gets("AdjustmentAmt");
		String adjustmentTypeCd = sourceBean.gets("AdjustmentTypeCd");
		String transactionTypeCd = sourceBean.gets("TransactionTypeCd");			
		boolean isPremiumCategory = StringTools.equals(categoryCd, "Premium");
		
		
		boolean isWaive = StringTools.in(adjustmentTypeCd, "WaiveDebit,WaiveCredit,WaiveDebitReversal,WaiveCreditReversal");

		// Billing Fees Charged
		if( !isPremiumCategory ){				
			for(MDAOption option : BILLING_FEE_CATEGORIES){
				if(( option.getValue().equals(categoryCd)) ){
					statBeans.add(sourceBean, chargeAmt,"Billing Fees Charged");
					break;
				}										
			}				
		}

		if( !isWaive && !isPremiumCategory) {
			for(MDAOption option : BILLING_FEE_CATEGORIES){
				if(( option.getValue().equals(categoryCd)) ){									
					statBeans.add(sourceBean, adjustmentAmt, "Billing Fees Charged");
					break;
				}
			}
		}
		
		// AR Receipts
		String transactionDesc = sourceBean.gets("TransactionDesc");
		String paidAmt = sourceBean.gets("PaidAmt");
		String commissionTakenAmt = sourceBean.gets("CommissionTakenAmt");
		boolean isReceiptTransaction = sourceBean.valueEquals("TransactionTypeCd", "Receipt");
		boolean isSuspendedCR = StringTools.equals(transactionDesc, "Suspended Cash Receipt");
		boolean isTransferredFrom = StringTools.contains(transactionDesc, "Transferred from");
		
		if(isReceiptTransaction && !isSuspendedCR && !isTransferredFrom) {
			String receiptType = getARReceiptType(sourceBean);

			ModelBean[] glStatsBeans = new ModelBean[] {statBeans.createStatBean(sourceBean, paidAmt, "AR Receipts"), statBeans.createStatBean(sourceBean, commissionTakenAmt, "AR Receipts-Comm Taken")};
			for(ModelBean bean: glStatsBeans) {
				//bean.setValue("ARReceiptTypeCd", receiptType);
				statBeans.add(bean);
			}
		}
		
		// AR Receipts Reversal - ACH
		String activityTypeCd = sourceBean.gets("ActivityTypeCd");
		String reverseReason = sourceBean.gets("ReverseReason");
		boolean isManualReversalTransaction =  StringTools.equals(transactionTypeCd, "ManualReversal");
		boolean isNotRefund = StringTools.notEqual(activityTypeCd, "Refund");
		boolean isACHReversalReason = StringTools.in(reverseReason, "Misapplied,Other,Transfer to Anouther Account");
		
		if( isManualReversalTransaction && isNotRefund && !isACHReversalReason){
			String receiptType = getARReceiptType(sourceBean);
			
			ModelBean[] glStatsBeans = new ModelBean[] {statBeans.createStatBean(sourceBean, paidAmt, "AR Receipts Reversal"), statBeans.createStatBean(sourceBean, commissionTakenAmt, "AR Receipts Reversal-Comm Taken")};
			for(ModelBean bean: glStatsBeans) {
				//bean.setValue("ARReceiptTypeCd", receiptType);
				statBeans.add(bean);
			}
		}
		
		// AR Refund Entered
		boolean isRefundTransaction = StringTools.in(transactionTypeCd,"AutomatedRefund,ManualRefund");
		if(isRefundTransaction){
			statBeans.add(sourceBean, paidAmt, "AR Refund Entered");
			statBeans.add(sourceBean, commissionTakenAmt, "AR Refund Entered-Comm Taken");
		}

		// AR Refund Reversal
		if(isManualReversalTransaction && !isNotRefund){
			statBeans.add(sourceBean, paidAmt, "AR Refund Reversal");
			statBeans.add(sourceBean, commissionTakenAmt, "AR Refund Reversal-Comm Taken");
		}
		
		// Suspended Cash Applied to Accounts Receivable - Apply to AR
		if(isReceiptTransaction && isSuspendedCR) {
			ModelBean glStatsBean = statBeans.createStatBean(sourceBean, paidAmt, "Susp Cash To AR-Apply To AR");
			statBeans.add(glStatsBean);

			glStatsBean = statBeans.createStatBean(sourceBean, commissionTakenAmt, "Susp Cash To AR-Apply To AR-Comm Taken");
			statBeans.add(glStatsBean);
		}
		
		// Transfer Between Accounts Receivable Accounts - Receipt
		if(isReceiptTransaction && transactionDesc.startsWith("Transferred from")) {		

			final String account = transactionDesc.replace("Transferred from ", "");
			String transferCarrierCd = findAccountCarrierCd(account);

			ModelBean glStatsBean = statBeans.createStatBean(sourceBean, paidAmt, "AR Transfer-Receipt");
			glStatsBean.setValue("TransferCarrier", transferCarrierCd);
			statBeans.add(glStatsBean);

			statBeans.createStatBean(sourceBean, commissionTakenAmt, "AR Transfer-Receipt-Comm Taken");
			glStatsBean.setValue("TransferCarrier", transferCarrierCd);
			statBeans.add(glStatsBean);
		}
		
		// Transfer Between Accounts Receivable Accounts - Transfer
		boolean isManualTransfer = "ManualTransfer".equals(transactionTypeCd);
		if( (isManualTransfer && transactionDesc.startsWith("Transferred to")) ||
			(isManualReversalTransaction && "Receipt".equals(activityTypeCd) && "Transferred to Another Account".equals(reverseReason)) ) {						

			final String account = transactionDesc.replace("Transferred to ", "");
			String transferCarrierCd = findAccountCarrierCd(account);

			ModelBean glStatsBean = statBeans.createStatBean(sourceBean, paidAmt, "AR Transfer-Transfer");
			glStatsBean.setValue("TransferCarrier", transferCarrierCd);
			statBeans.add(glStatsBean);

			glStatsBean = statBeans.createStatBean(sourceBean, commissionTakenAmt, "AR Transfer-Transfer-Comm Taken");
			glStatsBean.setValue("TransferCarrier", transferCarrierCd);
			statBeans.add(glStatsBean);
		}

		// Transfer From Accounts Receivable to Suspense - Remove from AR
		if( (isManualTransfer && !transactionDesc.startsWith("Transferred to")) || 
			(isManualReversalTransaction && StringTools.in(reverseReason, "Misapplied,Other"))) {
			String accountTransferTo = getAccountTransferTo(sourceBean);

			ModelBean glStatsBean = statBeans.createStatBean(sourceBean, paidAmt, "AR To Susp-Remove From AR");
			glStatsBean.setValue("TransferCarrier", accountTransferTo);
			statBeans.add(glStatsBean);

			glStatsBean = statBeans.createStatBean(sourceBean, commissionTakenAmt, "AR To Susp-Remove From AR-Comm Taken");
			glStatsBean.setValue("TransferCarrier", accountTransferTo);
			statBeans.add(glStatsBean);
		}
		
		// AR Adjustments
		adjustmentAmt = sourceBean.gets("AdjustmentAmt");
		if(isWaive) {
			statBeans.add(sourceBean, adjustmentAmt, "AR Adjustments");
		}
		
		// Misc. Reallocations - PaidAmt
		if(!StringTools.in(transactionTypeCd, "Receipt,AutomatedRefund,ManualRefund,ManualReversal,ManualTransfer")) {
			statBeans.add(sourceBean, paidAmt, "Misc Reallocations - PaidAmt");
			statBeans.add(sourceBean, commissionTakenAmt, "Misc Realloc-PaidAmt-Comm Taken");
		}
		
		// Misc. Reallocations - Adjustment Amt
		if(!isWaive && isPremiumCategory) {
			statBeans.add(sourceBean, adjustmentAmt, "Misc Realloc-AdjustmentAmt");				
		}
		
		if(originalSize == statBeans.getSize()) {
			missedBeans.add(sourceBean);
		}
	}
		
	protected String findAccountCarrierCd(String key) throws Exception {
		if (!(key == null || key.isEmpty())) {
			final ModelBean account = HPAccount.getAccountByAccountNumber(key, true, true);
			if( account == null) {
				return "";
			}
			final ModelBean arPolicy = account.getBean("ARPolicy");
			return arPolicy.gets("CarrierCd");
			
		} else {
			throw new IllegalStateException("Cannot find carrier for a null or blank account");
		}
	}
	
	/* Divide and round two values. Oracle ODBC driver returns NUMBER data types trimming the last precision digits if they are zeros. Other databases do not trim them.
	 * However, BigDecimal divide method decides the precision based on the number of decimal passed in. That means that the precision of the division can vary by database.
	 * That is why we need to tell the divider what precision to use (6 by default). 
	 * 
	 * @param value1 first value
	 * @param value2 second value
	 * @return rounded result of division
	 */
	public static String divide(String value1, String value2) {
		try {
			BigDecimal a1 = new BigDecimal(value1);
			BigDecimal a2 = new BigDecimal(value2);

			BigDecimal result = a1.divide(a2, 6, BigDecimal.ROUND_UP);

			return result.toString();

		} catch (Exception e) {
			return value1;
		}
	}
}

