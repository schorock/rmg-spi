package com.ric.insurance.interfaces.gl;

public class FieldDefinition {
	
	public int width() {
		return end - start;
	}
	
	protected String id = new String(); 
	protected int start = 0; 
	protected int end = 0;
	protected Boolean required = false;
	protected String type = "A";
	protected String format = null;
	protected String _default = null;
	protected int min = 1;
	protected int max = 1;
}
