package com.ric.insurance.interfaces.gl.output.handler;

import java.io.File;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;

import com.iscs.common.tech.bean.data.SQLRelationDelegate;
import com.iscs.common.tech.bean.data.SQLRelationHelper;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.common.utility.io.FileTools;
import com.iscs.common.utility.table.Cell;
import com.iscs.common.utility.table.Row;
import com.iscs.insurance.interfaces.gl.Dates;
import com.ric.insurance.interfaces.gl.FixedFileWriter;
import com.ric.insurance.interfaces.gl.FileWriter;
import com.ric.insurance.interfaces.gl.FileWriterFactory;
import com.iscs.insurance.interfaces.gl.GLCustomerTables;
import com.iscs.insurance.interfaces.gl.GLCustomerTables.GLMissingRowException;
import com.iscs.insurance.interfaces.gl.GLTools;
import com.iscs.insurance.interfaces.gl.GLTools.ModelBeanGroup;
import com.iscs.insurance.interfaces.gl.GLTransaction;
import com.iscs.insurance.interfaces.gl.stat.GLStatsDatabase;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

/**
 * Base class for GL Output jobs, should be extended with customer specific logic in the abstract methods
 * @author matthew.mcclellan
 *
 */
public abstract class GLOutputHandlerBase extends InnovationIBIZHandler {
	protected JDBCData data = null;
	protected StringDate runDt = null;
	protected SQLRelationHelper relationHelper = null;
	protected String period = null;
	private static HashMap<String, String> addlTransactionMap= new HashMap();
	 static
	    {
		 addlTransactionMap.put("AR Misc MGA Comm Taken", "Misc Realloc-Paid Amt-Comm Taken");
		 addlTransactionMap.put("AR Receipts MGA Comm Taken", "AR Receipts-Comm Taken");
		 addlTransactionMap.put("AR Receipts MGA Comm Taken Reversal", "AR Receipts Reversal-Comm Taken");
		 addlTransactionMap.put("AR Refund MGA Comm Taken", "AR Refund Entered-Comm Taken");
		 addlTransactionMap.put("AR Refund MGA Comm Taken Reversal", "AR Refund Reversal-Comm Taken");
		 addlTransactionMap.put("ARToSusp-RemoveFromAR-MGACommTaken", "AR To Susp-Remove From AR-Comm Taken");
		 addlTransactionMap.put("ARTransfer-Transfer-MGACommTaken", "AR Transfer-Transfer-Comm Taken");
		 addlTransactionMap.put("BillingFeesChargedMGA", "Billing Fees Charged");
		 addlTransactionMap.put("ClaimManagementFeeUnearned", "Direct Unearned Premium");
		 addlTransactionMap.put("DirectWrittenClaimManagementFee", "Direct Written Premium");
		 addlTransactionMap.put("ClaimsAdjustingIncome", "Direct Written Premium");
		 addlTransactionMap.put("ClaimsAdjustingIncomeUnearned", "Direct Unearned Premium");
		 //addlTransactionMap.put("DirectMGACommissionOverride", "Direct Written Premium");
		 addlTransactionMap.put("InspectionFeeMGAIncome", "Direct Written Fees And Taxes");
		 addlTransactionMap.put("ManagementFeeIncome", "Direct Written Premium");
		 addlTransactionMap.put("MGACommission", "Direct Written Premium");
		 addlTransactionMap.put("PolicyFeeMGAExpense", "Direct Written Fees And Taxes");
		 addlTransactionMap.put("PolicyFeeMGAIncome", "Direct Written Fees And Taxes");
		 addlTransactionMap.put("SuspCashToAR-ApplyToAR-MGACommTaken", "Susp Cash To AR-Apply To AR-Comm Taken");
	    }
	protected HashMap<String, FileWriter> fileWriters = new HashMap<String, FileWriter>(); 

	public static class CreditDebitAccounts {
		public final String credit;
		public final String debit;

		public CreditDebitAccounts(String credit, String debit) {
			this.credit = credit;
			this.debit = debit;
		}
	}
	
	/**
	 * GL Accounting Mapping Exceptions
	 */
	public static class GLAccountMappingException extends Exception {
		protected static final long serialVersionUID = -3994198022217629508L;

		public GLAccountMappingException(String message, Throwable cause) {
			super(message, cause);
		}

		public GLAccountMappingException(String message) {
			super(message);
		}
		
		public GLAccountMappingException(Exception e) {
			super(e);
		}
	}
	
	/**
	 * GL columns mapping exceptions
	 */
	public static class GLMappingError {
		protected final ModelBeanGroup group;
		protected final GLAccountMappingException exception;
		protected final String transactionName;

		public GLMappingError(ModelBeanGroup group, GLAccountMappingException exception, String transactionName) {
			super();
			this.group = group;
			this.exception = exception;
			this.transactionName = transactionName;
		}
		
		public ModelBeanGroup getGroup() {
			return group;
		}

		public GLAccountMappingException getException() {
			return exception;
		}

		public String getTransactionName() {
			return transactionName;
		}
		
	}

	/**
	 * Class containing GL transformation results
	 */
	public static class GLTransformResult {
		protected final HashMap<String, GLTransaction> transactionMap;
		protected final List mappingErrors;

		public GLTransformResult(HashMap<String, GLTransaction> transactionMap, List mappingErrors) {
			this.transactionMap = transactionMap;
			this.mappingErrors = mappingErrors;
		}
		
		public HashMap<String, GLTransaction> getTransactionMap() {
			return transactionMap;
		}

		public List getMappingErrors() {
			return mappingErrors;
		}
	}
	
    /** Creates a new instance of GeneralLedgerHandler
     * @throws Exception never
     */
    public GLOutputHandlerBase() throws Exception {

    }
    
	/** Create the general ledger export file requested. Do not allow any current files to be overridden
	 * @param settingsId The settings file section id
	 * @param fileName The generated filename to use
	 * @param append Indicator to pass on to determine whether to overwrite or append to file. The validation overrides this as we are calling a common method
	 * @return A FileWriter object to the generated file from the settings file
	 * @throws Exception when an error occurs
	 */
	protected FileWriter createFile(String settingsId, String fileName, boolean append, boolean allowOverwriteInd) throws Exception, IBIZException {
		if (FileTools.exists(fileName)) {
			if (!allowOverwriteInd) {
				throw new IBIZException("General Ledger File " + fileName + " currently exists and cannot be overwritten.");
			}
		} else {
			// Create the directory path if it doesn't exist
			File file = new File(fileName);
			File parentDirectory = file.getParentFile();
			FileTools.createPath(parentDirectory.toString());
		}

		return FileWriterFactory.create(settingsId, fileName, append);	
	}
    
    /** 
     * @param ap
     * @throws Throwable
     */
    protected void init(ModelBean ap) throws Throwable {
        this.period = ap.gets("Period");
        
        String runDate = ap.gets("RunDt");
        this.runDt = new StringDate(runDate);
        
        postInit(ap);
    }
    
    protected ArrayList<String> getEnabledTransactions() throws Exception {
    	return GLCustomerTables.getColumnValues("transactions", this.period);
    }
    
    protected String[] getGroupByForTransaction(String transactionName) throws Exception {
    	ArrayList<String> results = GLCustomerTables.getColumnValues("group-bys", getTransactionNameSimple(transactionName));
		return results.toArray(new String[results.size()]);
    }

	/**
	 * @param transactionName
	 * @return
	 */
	public String getTransactionNameSimple(String transactionName) {
		String capitalizedName = WordUtils.capitalize(transactionName);
		return capitalizedName.replaceAll(" ", "").replaceAll("/", "");
	}

    /** Process the General Ledger Stats
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process()
    throws ServiceHandlerException {
        try {   
        	Log.debug("Processing GLOutputHandler...");
            ModelBean rs = this.getHandlerData().getResponse();
    		ModelBean responseParams = rs.getBean("ResponseParams");
    		responseParams.getBean("Errors");

            ModelBean rq = this.getHandlerData().getRequest();
            data = this.getConnection();
            SQLRelationDelegate delegate = SQLRelationHelper.getDelegate(this.data);
            this.relationHelper = new SQLRelationHelper(this.data, delegate);
            
            AdditionalParams ap = new AdditionalParams(rq);
            
            init(ap);

            // Only process if the GLStats for the current run date has been completely processed
			ArrayList<ModelBean> errors = new ArrayList<ModelBean>();
            verifyRunCompleted(errors);
			if (errors.size() > 0) {
				for (ModelBean error: errors) {
	                addErrorMsg(error.gets("Name"), error.gets("Msg"), error.gets("Type"), error.gets("Severity"));
				}
				return rs;
			}
            
      		List<String> accountingDts = new Dates().getRunDatesFile(runDt.toString(), errors);
			if (errors.size() > 0) {
				for (ModelBean error: errors) {
	                addErrorMsg(error.gets("Name"), error.gets("Msg"), error.gets("Type"), error.gets("Severity"));
				}
				return rs;
			}

            List<Object> mappingErrors = processGL(accountingDts.get(0), accountingDts.get(accountingDts.size()-1));
            
            for (Object mappingErrorObject: mappingErrors) {
            	if (mappingErrorObject instanceof GLMappingError){
            		GLMappingError glMappingError = (GLMappingError)mappingErrorObject;
	            	ModelBean[] beans = glMappingError.group.getBeans();
	            	List<String> beanIds = new ArrayList<String>();
	            	
	            	for(int i = 0; i < 10 && i < beans.length; i++) {
	            		ModelBean bean = beans[i];
	            		beanIds.add(bean.gets("SystemId"));
	            	}
	            	
	            	if(beans.length > 10) {
	            		beanIds.add("...");
	            	}
	
					String errorMsg = String.format(
						"Error processing transaction [%s] - %s. GLStats ids [%s]", 
						glMappingError.transactionName, 
						glMappingError.exception.getMessage(), 
						StringUtils.join(beanIds.toArray(new String[beanIds.size()]), ","));
					
					// Add error message to the response so that its visible in the jobs history
					addErrorMsg("GL Account Mapping Error", errorMsg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
	
					// Output the errors to the log so that stack trace is available
					Log.error(glMappingError.exception);
            	} else if (mappingErrorObject instanceof GLMissingRowException){
            		GLMissingRowException glMissingRowException = (GLMissingRowException)mappingErrorObject;
            		addErrorMsg("GL Missing Row Mapping Error", glMissingRowException.getMessage(), ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
            	}
            }
            
			Log.debug("Finished GLOutputHandler...");
				
			return rs;
        }
        catch( Throwable e ) {
            throw new ServiceHandlerException(e);
        } finally {
        	try {
        		finalize();
        	} catch(Exception e) {
        		throw new ServiceHandlerException("Failed to execute finalize", e);
        	}
        }
    }
    
    /** Obtain the list of GLStat beans that are for this specific transaction in the range of dates for the output
     * 
     * @param transactionName The GL transaction name
     * @param startDate The start date to pick up source records
     * @param endDate The end date to pick up source records
     * @return The list of valid GL stats records that match
     * @throws Exception when an error occurs
     */
    protected ModelBean[] fetchGLStatsBeans(String transactionName, String startDate, String endDate) throws Exception {
    	if("Monthly".equals(this.period)) {
    		String periodDate = runDt.toStringDisplay("yyyyMM");
    		return relationHelper.selectModelBean("GLStats", new String[] {"TransactionName", "TransactionPeriod"}, new String[] {"=", "="}, new String[] {transactionName, periodDate}, 0);
    	} else if("Daily".equals(this.period)) {
    		return relationHelper.selectModelBean("GLStats", new String[] {"TransactionName", "TransactionDate", "TransactionDate"}, new String[] {"=", ">=", "<="}, new String[] {transactionName, startDate, endDate}, 0);
    	} else {
    		throw new IllegalStateException(String.format("Unknown period type of [%s], expected Daily or Monthly", period));
    	}
    }
    
    /** Process the GL output transactions during the period specified for file creation
     * 
     * @param startDate The start date to pick up the GL stat records
     * @param endDate The end date to pick up the GL stat records
     * @return The list of errors during processing
     * @throws Exception when an error occurs
     */
    protected List<Object> processGL(String startDate, String endDate) throws Exception {
    	List<String> enabledTransactions = getEnabledTransactions();
    	List<Object> errors = new ArrayList<Object>();

    	for(String transactionName: enabledTransactions) {
    		try {
    			String addlTransactionName=addlTransactionMap.get(transactionName)!=null?addlTransactionMap.get(transactionName):transactionName;
    			ModelBean[] glStatsBeans = fetchGLStatsBeans(addlTransactionName, startDate, endDate);
    			ModelBeanGroup[] groups= GLTools.groupBy(glStatsBeans, getGroupByForTransaction(transactionName));
    			Row transactionConstants = GLCustomerTables.getTransactionConstants(transactionName);
    			String fileFormat = getRowString(transactionConstants, "FileFormat");
    			String paymentMethodCd = "";
    			String systemId = "";
    			GLTransformResult result = null;
    			for (ModelBeanGroup group : groups){
        			ModelBean glStatBean = group.getBeans()[0];
        			systemId = glStatBean.gets("StatSourceId");
        			String SQL="Select PaymentMethodCd from PayableStats where SystemId ="+systemId;
        			ResultSet rSet = data.doSQL(SQL);
        			while (rSet.next()) {
        				paymentMethodCd = rSet.getString("PaymentMethodCd");
          			}
        			if (fileFormat.equalsIgnoreCase("GeneralLedgerJournalEntry")){
	    				result = transformStatBeanToTransaction(transactionName, group, paymentMethodCd, systemId);
		    			writeTransaction(transactionName, result.transactionMap);
		    			
		    			errors.addAll(result.mappingErrors);
        			}else{
	        			if(paymentMethodCd.equals("Check - Manual") || paymentMethodCd.equals("Credit Card")){
			    			result = transformStatBeanToTransaction(transactionName, group,paymentMethodCd, systemId);
			    			writeTransaction(transactionName, result.transactionMap);
			    			
			    			errors.addAll(result.mappingErrors);
		    			}
        			}
    			}
    		} catch(Exception e) {
    			Log.error(e);
    			String cause = e.getMessage();
    			if (!cause.isEmpty()){
    				throw new RuntimeException(String.format("Failed during output of transaction with name [%s], Cause: %s .", transactionName, cause), e);
    			} else {
    				throw new RuntimeException(String.format("Failed during output of transaction with name [%s], Exception", transactionName), e);
    			}	
    		}
    	}
    	
    	return errors;
    }
    
	public String getRowString(Row row, String property) throws Exception {
    	Cell cell = row.getCell(property);

    	if(cell.getValue() == null) {
    		throw mappingError("Column [%s] not set in row# [%s]", property, row.getRowId());
    	} else {
    		return cell.getString();
    	}
	}
	
	protected CreditDebitAccounts accountsFromDescriptions(String creditDesc, String debitDesc) throws Exception {
		return new CreditDebitAccounts(GLCustomerTables.mapDescriptionToAccount(creditDesc), GLCustomerTables.mapDescriptionToAccount(debitDesc));
	}
    
    public GLAccountMappingException mappingError(String messageTemplate, Object... args) throws GLAccountMappingException {
    	return new GLAccountMappingException(String.format(messageTemplate, args));
    }
    
    /** Determine if the GL Stats was run for the current run date. The output file generation should stop if this is the case
     * 
     * @param errors The list of errors if any to tell the user that the stats was not run.
     * @throws Exception when an error occurs
     */
    protected void verifyRunCompleted(ArrayList<ModelBean> errors) throws Exception {
        if (this.period.equals("Daily")) {
        	String transactionName = GLStatsDatabase.DAILY_MARKER_NAME;
        	String runDate = runDt.toStringDisplay("yyyy-MM-dd");
            ModelBean[] rowsFound = relationHelper.selectModelBean("GLStats", new String[] {"TransactionName", "TransactionDate"}, new String[] {"=", "="}, new String[] {transactionName, runDate}, 0);
            if (rowsFound.length == 0) {
            	Object[] msgArgs = {this.period, runDate}; 
				String msg = "The {0} GLStats run did not completely process for the run date of {1}. Re-run the job again when you have determined what caused the issue.";
            	ErrorTools.addError("GLOutput.StatsNotRun.Error", msg,  ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR, errors);
            }
        } else {
        	String transactionName = GLStatsDatabase.MONTHLY_MARKER_NAME;
        	String periodDate = runDt.toStringDisplay("yyyyMM");
            ModelBean[] rowsFound = relationHelper.selectModelBean("GLStats", new String[] {"TransactionName", "TransactionPeriod"}, new String[] {"=", "="}, new String[] {transactionName, periodDate}, 0);
            if (rowsFound.length == 0) {
            	Object[] msgArgs = {this.period, periodDate}; 
				String msg = "The {0} GLStats run did not completely process for the run date of {1}. Re-run the job again when you have determined what caused the issue.";
            	ErrorTools.addError("GLOutput.StatsNotRun.Error", msg,  ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR, errors);
            }
        }
    }
    
	
	/** Get the transaction to accounts mapping tab name
	 * @param transactionName The transaction name
	 * @return The transaction to accounts mapping tab name
	 * @throws Exception
	 */
	public String getTransactionAccountMappingTab(String transactionName) throws Exception{
    	Row transactionConstants = GLCustomerTables.getTransactionConstants(transactionName);
    	String transactionAccountTab =  getRowString(transactionConstants, "TransactionAccountTab");

		if (!transactionAccountTab.isEmpty()){
			return transactionAccountTab;
		}
		throw mappingError("No configuration to get account for transaction [%s]", transactionName);

	}
    
    // Abstract methods which are used to make client specific modifications
    
    /**
     * Makes modifications to the transaction necessary for the customer
     * @param transactionName name of the transaction
     * @return the modified GLTransaction
     */
    public abstract GLTransaction createTransactionForCustomer(String transaction) throws Exception;

    /**
     * Set the credit and debit account for a specific transaction group
     * @param transactionName name of the transaction
     * @param group {@link ModelBeanGroup}
     * @return a {@link CreditDebitAccounts} entry - This should not be null
     */
    public abstract CreditDebitAccounts getAccountsForGroup(String transactionName, ModelBeanGroup group) throws Exception;
    
    /**
     * Called after the base class initializes itself
     * @param additionalParams {@link AdditionalParams} that were passed in from the job
     * @throws Throwable 
     */
    protected abstract void postInit(ModelBean additionalParams) throws Exception, Throwable;
    
    /**
     * Called after all the transactions for this period have been processed, last function before exiting
     */
    protected abstract void finalize() throws Exception;
    
    /**
     * Writes out a transaction to the output location
     * @param transactionName name of the transaction to output
     * @param transactionMap 
     */
    protected abstract void writeTransaction(String transactionName, HashMap<String, GLTransaction> transactionMap) throws Exception;
    
    /** Transforms GLStat beans to transaction
     * @param transaction The transaction name
     * @param groups the groups array
     * @return results  
     * @throws Exception
     */
    protected abstract GLTransformResult transformStatBeanToTransaction(String transaction, ModelBeanGroup group,String paymentMethodCd, String systemId) throws Exception ;
    
    public HashMap<String, FileWriter> getFileWriters(){
    	return fileWriters;
    }
    
    public StringDate getRunDt(){
    	return runDt;
    }
}
