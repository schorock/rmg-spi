package com.ric.insurance.interfaces.gl.output.handler;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.iscs.common.tech.bean.data.SQLRelationDelegate;
import com.iscs.common.tech.bean.data.SQLRelationHelper;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.insurance.interfaces.gl.GLCustomerTables;
import com.iscs.insurance.interfaces.gl.GLCustomerTables.GLMissingRowException;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

public class GLOutputRegenerateHandler extends GLOutputHandler {

	public GLOutputRegenerateHandler() throws Exception {}
	
    /** Process the General Ledger output by regenerating the output files based on the given start and end dates
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
	@Override
    public ModelBean process()
    throws ServiceHandlerException {
        try {   
        	Log.debug("Processing GLOutputHandler...");
            ModelBean rs = this.getHandlerData().getResponse();
    		ModelBean responseParams = rs.getBean("ResponseParams");
    		responseParams.getBean("Errors");

            ModelBean rq = this.getHandlerData().getRequest();
            data = this.getConnection();
            SQLRelationDelegate delegate = SQLRelationHelper.getDelegate(this.data);
            this.relationHelper = new SQLRelationHelper(this.data, delegate);
            
            AdditionalParams ap = new AdditionalParams(rq);
            
            this.period = "Daily";
            
			String startDt = ap.gets("StartDt");
			String endDt = ap.gets("EndDt");
            this.runDt = new StringDate(endDt);
            
            postInit(ap);

            // Only process if the GLStats for the current run date has been completely processed
			ArrayList<ModelBean> errors = new ArrayList<ModelBean>();
            verifyRunCompleted(errors);
			if (errors.size() > 0) {
				for (ModelBean error: errors) {
	                addErrorMsg(error.gets("Name"), error.gets("Msg"), error.gets("Type"), error.gets("Severity"));
				}
				return rs;
			}
            
            List<Object> mappingErrors = processGL(startDt, endDt);
            
            for (Object mappingErrorObject: mappingErrors) {
            	if (mappingErrorObject instanceof GLMappingError){
            		GLMappingError glMappingError = (GLMappingError)mappingErrorObject;
	            	ModelBean[] beans = glMappingError.getGroup().getBeans();
	            	List<String> beanIds = new ArrayList<String>();
	            	
	            	for(int i = 0; i < 10 && i < beans.length; i++) {
	            		ModelBean bean = beans[i];
	            		beanIds.add(bean.gets("SystemId"));
	            	}
	            	
	            	if(beans.length > 10) {
	            		beanIds.add("...");
	            	}
	
					String errorMsg = String.format(
						"Error processing transaction [%s] - %s. GLStats ids [%s]", 
						glMappingError.getTransactionName(), 
						glMappingError.getException().getMessage(), 
						StringUtils.join(beanIds.toArray(new String[beanIds.size()]), ","));
					
					// Add error message to the response so that its visible in the jobs history
					addErrorMsg("GL Account Mapping Error", errorMsg, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
	
					// Output the errors to the log so that stack trace is available
					Log.error(glMappingError.getException());
            	} else if (mappingErrorObject instanceof GLMissingRowException){
            		GLMissingRowException glMissingRowException = (GLMissingRowException)mappingErrorObject;
            		addErrorMsg("GL Missing Row Mapping Error", glMissingRowException.getMessage(), ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
            	}
            }
            
			Log.debug("Finished GLOutputRegenerateHandler...");
				
			return rs;
        }
        catch( Throwable e ) {
            throw new ServiceHandlerException(e);
        } finally {
        	try {
        		finalize();
        	} catch(Exception e) {
        		throw new ServiceHandlerException("Failed to execute finalize", e);
        	}
        }
    }
}
