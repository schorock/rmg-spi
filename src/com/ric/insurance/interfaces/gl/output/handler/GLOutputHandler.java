package com.ric.insurance.interfaces.gl.output.handler;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.ric.insurance.interfaces.gl.FileWriter;
import com.ric.insurance.interfaces.gl.fileentry.APVoucherMemoEntry;
import com.ric.insurance.interfaces.gl.fileentry.GLFileEntryBase;
import com.ric.insurance.interfaces.gl.fileentry.GeneralLedgerJournalEntry;
import com.iscs.common.render.DateRenderer;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.table.Cell;
import com.iscs.common.utility.table.Column;
import com.iscs.common.utility.table.Row;
import com.iscs.common.utility.table.Table;
import com.iscs.insurance.interfaces.gl.GLCustomerTables;
import com.iscs.insurance.interfaces.gl.GLCustomerTables.GLMissingRowException;
import com.iscs.insurance.interfaces.gl.GLTools.ModelBeanGroup;
import com.iscs.insurance.interfaces.gl.GLTransaction;
import com.iscs.insurance.interfaces.gl.Settings;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.date.StringDate;

public class GLOutputHandler extends GLOutputHandlerBase {
	
	public GLOutputHandler() throws Exception {}

	protected HashMap<String, String> transactionAccountTabMap;
	
    /**
     * Makes modifications to the transaction necessary for the customer
     * @param transactionName name of the transaction
     * @return the modified GLTransaction
     */
	@Override
	public GLTransaction createTransactionForCustomer(String transactionName) throws Exception {
    	GLTransaction transaction = new GLTransaction();
    	Row transactionConstants = GLCustomerTables.getTransactionConstants(transactionName);

    	String transactionType = getRowString(transactionConstants, "TransactionType");
		transaction.setProperty("TransactionType", transactionType);
    	if("Reversing".equals(transactionType)) {
    		StringDate advanceMonth = StringDate.advanceMonth(this.runDt, 1);
    		transaction.setProperty("ReversingDt", advanceMonth.toStringDisplay("MM/01/yyyy"));
    	} else {
    		transaction.setProperty("ReversingDt", "");
    	}
    	
    	return transaction;
	}
	
    /** Transforms GLStat beans to transaction
     * @param transaction The transaction name
     * @param groups the groups array
     * @return results  
     * @throws Exception
     */
	@Override
    protected GLTransformResult transformStatBeanToTransaction(String transactionName, ModelBeanGroup group,String paymentMethodCd, String systemId) throws Exception {
		Row transactionConstants = GLCustomerTables.getTransactionConstants(transactionName);
		String fileFormat = getRowString(transactionConstants, "FileFormat");
    	
    	HashMap<String, GLTransaction> transactionMap = new HashMap<String, GLTransaction>();
    	List mappingErrors = new ArrayList();
		try {
			GLFileEntryBase fileEntry = null;
			if (fileFormat.equalsIgnoreCase("GeneralLedgerJournalEntry")){
				fileEntry = new GeneralLedgerJournalEntry(this);
			} else if (fileFormat.equalsIgnoreCase("APVoucherMemo")){	
    				fileEntry = new APVoucherMemoEntry(this);
			}
//    			} else if (fileFormat.equalsIgnoreCase("APVoidStop")){		
//    				fileEntry = new APVoidEntry(this);
//    			}
			else {
				throw new Exception("There is no GLFileEntry handler defined for the " + fileFormat + " file format.");
			}
			fileEntry.addEntryInfo(transactionName, transactionConstants, transactionMap, group);
		} catch(GLAccountMappingException mappingException) {
			mappingErrors.add(new GLMappingError(group, mappingException, transactionName));
		} catch (GLMissingRowException e){
			Log.error(e);
			mappingErrors.add(e);
		} catch (Exception e){
			Log.error(e);
			throw e;
		}
    	
    	return new GLTransformResult(transactionMap, mappingErrors);
    }

    /**
     * Set the credit and debit account for a specific transaction group
     * @param transactionName name of the transaction
     * @param group {@link ModelBeanGroup}
     * @return a {@link CreditDebitAccounts} entry - This should not be null
     */
	@Override
	public CreditDebitAccounts getAccountsForGroup(String transactionName, ModelBeanGroup item) throws Exception {
		String transactionNameSimple = this.getTransactionNameSimple(transactionName);
		String transactionAccountMappingTab = getTransactionAccountMappingTab(transactionName);
		Table table = GLCustomerTables.getTables().get(transactionAccountMappingTab);
		ArrayList<String> availableColumnsArray = new ArrayList<String>();
		ArrayList<String> availableValuesArray = new ArrayList<String>();
		availableColumnsArray.add("TransactionName");
		availableValuesArray.add(transactionNameSimple);
		
		Column[] columns = table.getColumns();
		for (int i=0; i< columns.length; i++){
			String columnName = columns[i].getId();
			if(StringRenderer.in(transactionNameSimple,"ARAdjustments,DirectClaimTransactions,DirectUnpaidLossExpense,DirectUnpaidLosses,DirectLossALAEMiscRecoveryAdjust,DirectLossALAEMiscPaidAdjust,"
					+ "DirectClaimRefundsReversed,DirectClaimRefundsRequested,DirectClaimRecoveriesReversed,DirectClaimRecoveriesReceived,DirectLAEPaymentsReversed,DirectLAEPaymentsRequested,DirectLossPaymentsReversed,DirectLossPaymentsRequested")){
				if (StringTools.in(columnName, "TransactionName,AccountDebit,AccountCredit")){
					continue;
				}
			} else{
				if (StringTools.in(columnName, "TransactionName,CategoryCd,AccountDebit,AccountCredit,ClassificationCd")){
					continue;
				}
			}
			if (!item.getFields().contains(columnName)){
				throw new GLAccountMappingException(
						String.format(
							"[%s] was not part of the group fields, group fields are [%s]",
							columnName,
							StringUtils.join(item.getFields().toArray(), ",")));
			}
			String groupValue = item.getGroupValue(columnName);
			if (groupValue.equals("")){
				groupValue = "All";
			}
			availableColumnsArray.add(columnName);
			availableValuesArray.add(groupValue);
		}
		
		String[] columnNames = availableColumnsArray.toArray(new String[availableColumnsArray.size()]);
		String[] values = availableValuesArray.toArray(new String[availableValuesArray.size()]);
		 
		Row accountRow = getTransactionAccountMappingRow(transactionAccountMappingTab, columnNames, values);
		
		String accountDebit = "";
		Cell cellDebit = accountRow.getCell("AccountDebit");
		if (cellDebit != null && cellDebit.getValue() != null){
			accountDebit = cellDebit.getString();
		}
		String accountCredit = "";
		Cell cellCredit = accountRow.getCell("AccountCredit");
		if (cellCredit != null && cellCredit.getValue() != null){
			accountCredit = cellCredit.getString();
		}
		
		return new CreditDebitAccounts(accountCredit, accountDebit);

	}
	
	/** Gets the Row containing the account mapping to transaction
	 * @param transactionAccountMappingTab The tab containing the account mapping to transaction
	 * @param paramNames Array with column names
	 * @param parmValues Array with values
	 * @return The Row containing the account mapping to transaction
	 * @throws Exception
	 */
	protected Row getTransactionAccountMappingRow(String transactionAccountMappingTab, String[] paramNames, String[] parmValues) throws Exception {
		Row row = GLCustomerTables.querySingleRow(transactionAccountMappingTab, paramNames, parmValues);
		if (row != null){
			return row;
		} else {
			String pairsEror = "";
			try {
				for (int i=0; i< paramNames.length; i++){
					pairsEror += paramNames[i] + " = " + parmValues[i];
				}
			} catch (Exception e){
				Log.error(e);//This can happen if paramNames and parmValues have different sizes
			}
			throw mappingError("Could not find account mapping row for transaction [%s] in gl-tables for pairs [%s].", paramNames[0], pairsEror);
		}
	}
	
	/**
	 * Reverses the transaction accounts if the amount is less than 0
	 * @param amount the amount of the transaction
	 * @param sourceAccounts the accounts before reversing
	 * @return the {@link CreditDebitAccounts} reversed if necessary
	 */
	protected CreditDebitAccounts reversableTransaction(String amount, CreditDebitAccounts sourceAccounts) throws Exception {
		if(StringTools.greaterThanEqual(amount, "0")) {
			return sourceAccounts;
		} else {
			return new CreditDebitAccounts(sourceAccounts.debit, sourceAccounts.credit);
		}
	}
	
	/**
	 * Reverses the transaction accounts if the amount is greater than 0 
	 * @param amount the amount of the transaction
	 * @param sourceAccounts the accounts before reversing
	 * @return the {@link CreditDebitAccounts} reversed if necessary
	 */
	protected CreditDebitAccounts reversableNegativeTransaction(String amount, CreditDebitAccounts sourceAccounts) throws Exception {
		if(StringTools.lessThanEqual(amount, "0")) {
			return sourceAccounts;
		} else {
			return new CreditDebitAccounts(sourceAccounts.debit, sourceAccounts.credit);
		}
	}

	/** Initialize all the output files
	 * @throws Throwable
	 */
	protected void startOutputFiles() throws Throwable {
		try {
        Settings settings = new Settings();
        settings.getMap().put("RunDt", runDt);

        startGLFile(settings);
        startAPVoucherMemoFile(settings);
		} catch(InvocationTargetException e){
			Log.error(e);
			if (e.getCause() != null){
				throw e.getCause();
			} else {
				throw e;
			}
		} catch(Throwable e){
			Log.error(e);
			throw e;
		}
	}
	public String getFilename(Settings settings) throws Exception {
		String fileSettingName = "";
        if("Monthly".equals(this.period)) {
        	fileSettingName = "monthlyGlFilePath";
        } else if("Daily".equals(this.period)) {
        	fileSettingName = "dailyGlFilePath";
        } 
		return settings.getSetting(fileSettingName);
	}
	
	/** Start the GeneralLedgerJournalEntry file
	 * @param settings The Settings object
	 * @throws Exception
	 */
	protected void startGLFile(Settings settings) throws Exception{
		String fileSettingName;
        if("Monthly".equals(this.period)) {
        	fileSettingName = "monthlyGlFilePath";
        } else if("Daily".equals(this.period)) {
        	fileSettingName = "dailyGlFilePath";
        } else {
        	throw new IllegalStateException(String.format("Unknown period type of [%s], expected Daily or Monthly", this.period));
        }
        
		FileWriter glWriter = createFile("GeneralLedgerJournalEntry", settings.getSetting(fileSettingName), false, true);
		//glWriter.write("Account Number\tAccounting Basis\tCalendar Accounting Month\tCalendar Accounting Year\tCalendar Accounting Day\tCompany Code\tConverted Amount\tDebit / Credit Indicator\tReference ID\tSource Code\tTransaction Day\tTransaction Month\tTransaction Year\tAnalysis Unit Code 1\tAnalysis Unit Code 2\tApproval ID\tBusiness Unit Code 1\tBusiness Unit Code 2\tBusiness Unit Code 3\tBusiness Unit Code 4\tBusiness Unit Code 5\tBusiness Unit Code 6\tBusiness Unit Code 7\tBusiness Unit Code 8\tCurrency Ext. Indicator\tDescription Detail\tEntry Operator\tExchange Rate\tHeader Description\tOperating Currency Code\tOriginal Amount\tPattern Ind\tSuspense Match\tUser Data" + newline);
		fileWriters.put("GeneralLedgerJournalEntry", glWriter);
	}
	
//	/** Start the APVoucherMemo file
//	 * @param settings The Settings object
//	 * @throws Exception
//	 */
	protected void startAPVoucherMemoFile(Settings settings) throws Exception{
		String fileSettingName;
        if("Monthly".equals(this.period)) {
        	fileSettingName = "monthlyAPVoucherMemoFilePath";
        } else if("Daily".equals(this.period)) {
        	fileSettingName = "dailyAPVoucherMemoFilePath";
        } else {
        	throw new IllegalStateException(String.format("Unknown period type of [%s], expected Daily or Monthly", this.period));
        }
        
        String newline = System.getProperty("line.separator");
        
		FileWriter apVoucherWriter = createFile("APVoucherMemo", settings.getSetting(fileSettingName), false, true);
		//apVoucherWriter.write("Vendor\tDescription\tDoc Date\tCheckBookId\tPayment Amount\tPayment Number\tPayment Date\tAccount Number" + newline);
		fileWriters.put("APVoucherMemo", apVoucherWriter);
	}
//	
	/** Start the APVoidStop file
	 * @param settings The Settings object
	 * @throws Exception
	 */
//	protected void startAPStopFile(Settings settings) throws Exception{
//		String fileSettingName;
//        if("Monthly".equals(this.period)) {
//        	fileSettingName = "monthlyAPStopFilePath";
//        } else if("Daily".equals(this.period)) {
//        	fileSettingName = "dailyAPStopFilePath";
//        } else {
//        	throw new IllegalStateException(String.format("Unknown period type of [%s], expected Daily or Monthly", this.period));
//        }
//        
//        String newline = System.getProperty("line.separator");
//        
//		FileWriter apVoidStopWriter = createFile("APStop", settings.getSetting(fileSettingName), false, true);
//		//apVoidStopWriter.write("Check Number\tCheck Amount\tVoid or Stop Date\tCheckbook ID" + newline);
//		fileWriters.put("APStop", apVoidStopWriter);
//	}
	
//	/** Start the APVoidStop file
//	 * @param settings The Settings object
//	 * @throws Exception
//	 */
//	protected void startAPVoidFile(Settings settings) throws Exception{
//		String fileSettingName;
//        if("Monthly".equals(this.period)) {
//        	fileSettingName = "monthlyAPVoidFilePath";
//        } else if("Daily".equals(this.period)) {
//        	fileSettingName = "dailyAPVoidFilePath";
//        } else {
//        	throw new IllegalStateException(String.format("Unknown period type of [%s], expected Daily or Monthly", this.period));
//        }
//        
//        String newline = System.getProperty("line.separator");
//        
//		FileWriter apVoidStopWriter = createFile("APVoid", settings.getSetting(fileSettingName), false, true);
//		//apVoidStopWriter.write("Check Number\tCheck Amount\tVoid or Stop Date\tCheckbook ID" + newline);
//		fileWriters.put("APVoid", apVoidStopWriter);
//	}
	
	/** Start the APVoidStop file
	 * @param settings The Settings object
	 * @throws Exception
	 */
	protected void startAPUnVoidUnStopFile(Settings settings) throws Exception{
		String fileSettingName;
        if("Monthly".equals(this.period)) {
        	fileSettingName = "monthlyAPUnVoidUnStopFilePath";
        } else if("Daily".equals(this.period)) {
        	fileSettingName = "dailyAPUnVoidUnStopFilePath";
        } else {
        	throw new IllegalStateException(String.format("Unknown period type of [%s], expected Daily or Monthly", this.period));
        }
        
        String newline = System.getProperty("line.separator");
        
		FileWriter apVoidStopWriter = createFile("APUnVoidUnStop", settings.getSetting(fileSettingName), false, true);
		//apVoidStopWriter.write("Check Number\tCheck Amount\tVoid or Stop Date\tCheckbook ID" + newline);
		fileWriters.put("APUnVoidUnStop", apVoidStopWriter);
	}

	@Override
	protected void finalize() throws Exception {
		for (FileWriter fr : fileWriters.values()){
			if (fr != null){
				try { fr.close(); } catch (Exception e) { Log.error(e); }
			}
		}
	}
	
    /**
     * Writes out a transaction to the output location
     * @param transactionName name of the transaction to output
     * @param transactionMap 
     */
	@Override
	protected void writeTransaction(String transactionName, HashMap<String, GLTransaction> transactionMap) throws Exception {
		try {
			if(transactionMap!=null){
				Collection<GLTransaction> values=transactionMap.values();
				if(values!=null){
					List<GLTransaction> glTransactions= new ArrayList<GLTransaction>();
					if(values!=null && values.size()>0){
						for (GLTransaction glTransaction : values){
							glTransactions.add(glTransaction);
						}
					}			
					Collections.sort(glTransactions,new GLTransactionComparator() );
					for (GLTransaction glTransaction : glTransactions){
						String fileFormat = glTransaction.getProperty("FileFormat");
						GLFileEntryBase fileEntry = null;
						
						if (fileFormat.equalsIgnoreCase("GeneralLedgerJournalEntry")){
							fileEntry = new GeneralLedgerJournalEntry(this);
						} else if (fileFormat.equalsIgnoreCase("APVoucherMemo")){	
							fileEntry = new APVoucherMemoEntry(this);
						}
//						} else if (fileFormat.equalsIgnoreCase("APVoid")){		
//							fileEntry = new APVoidEntry(this);
//						}
						else {
							throw new Exception("There is no GLFileEntry handler defined for the " + fileFormat + " file format.");
						}
						
						fileEntry.writeEntry(glTransaction);
					}
				}
			}
		} catch (Exception e){
			Log.error(e);
			throw e;
		}
	}
	
	
	 /** Comparator for Feature beans based on Claim Coverage DisplayOrder
     *  Inner class
     */
    public class GLTransactionComparator implements Comparator<GLTransaction>{
        private GLTransaction glTransaction;
        private String order;
        
        /** Constructor for the Comparator for Feature beans based on Claim Coverage DisplayOrder
         * @param glTransaction The ClaimCoverage beans array
         * @param order The order for sorting. Can be Ascending or Descending
         */
        public GLTransactionComparator() {           
        }
        
        /** Override of compare method for the Comparator for FeatureCda based on Claim Coverage DisplayOrder
         * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */
        public int compare(GLTransaction glTransaction1, GLTransaction glTransaction2) {
            try {
            	String refId1 = glTransaction1.getProperty("ReferenceId")!=null?glTransaction1.getProperty("ReferenceId"):"";
            	String refId2 = glTransaction2.getProperty("ReferenceId")!=null?glTransaction1.getProperty("ReferenceId"):"";          	
            	return refId1.compareTo(refId2);               
            }
            catch( Exception e ) {
                return 0;
            }
        }
    }
    
	@Override
	protected void postInit(ModelBean additionalParams) throws Throwable {
		startOutputFiles();
	}

}
