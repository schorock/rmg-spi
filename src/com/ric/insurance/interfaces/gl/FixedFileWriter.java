package com.ric.insurance.interfaces.gl;

import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.jdom.Element;

import com.iscs.common.tech.log.Log;
import com.ric.insurance.interfaces.gl.FieldDefinition;
import com.ric.insurance.interfaces.gl.FileWriter;
import com.ric.insurance.interfaces.gl.RecordDefinition;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.date.StringDate;

public class FixedFileWriter extends FileWriter {

	public FixedFileWriter(File file) throws IOException {
		super(file);
	}

	public FixedFileWriter(File file, boolean append) throws IOException {
		super(file, append);
	}

	public FixedFileWriter(FileDescriptor fd) {
		super(fd);
	}

	public FixedFileWriter(String fileName, boolean append) throws IOException {
		super(fileName, append);
	}

	public FixedFileWriter(String fileName) throws IOException {
		super(fileName);
	}
	
	protected Map<String, RecordDefinition> copy(Map<String, RecordDefinition> records, Element parent) throws Exception {		
		
		for (Iterator<?> children = parent.getChildren("RecordDefinition").iterator(); children.hasNext();) {
			Element child = (Element) children.next();
			
			if (child.getAttributeValue("Format").equalsIgnoreCase("Fixed")) {
				RecordDefinition record = copy(new RecordDefinition(), child);
			
				records.put(record.getKey(), record);
			}
		}	
		
		return records;
	}
	
	protected RecordDefinition copy(RecordDefinition record, Element parent) throws Exception {
		String value = null;
		
		record.id = parent.getAttributeValue("Id");
		record.variant = parent.getAttributeValue("Variant");
		record.format = parent.getAttributeValue("Format");
		
		if ((value = parent.getAttributeValue("Length")) != null)
			record.length = Integer.parseInt(value);
		
		if ((value = parent.getAttributeValue("Padding")) != null)
			record.padding = value.charAt(0);
		
		if ((value = parent.getAttributeValue("Base")) != null)
			record.base = Integer.parseInt(value);
		
		for (Iterator<?> children = parent.getChildren("FieldDefinition").iterator(); children.hasNext();) {
			FieldDefinition field = copy(new FieldDefinition(), (Element) children.next());
			
			field.start -= record.base;
			field.end   -= record.base;
	
			record.fields.add(field);
		}
		
		return record;
	}
	
	protected String id = new String(); 
	protected int start = 0; 
	protected int end = 0;
	protected Boolean required = false;
	protected String type = "A";
	protected String format = null;
	protected String _default = null;
	protected int min = 1;
	protected int max = 1;

	protected FieldDefinition copy(FieldDefinition field, Element parent) throws Exception {
		String value = null;
		
		field.id = parent.getAttributeValue("Id"); 
		
		if ((value = parent.getAttributeValue("Start")) != null)
			field.start = Integer.parseInt(value);
		
		if ((value = parent.getAttributeValue("End")) != null)
			field.end = Integer.parseInt(value);
		
		if ((value = parent.getAttributeValue("Required")) != null)
			field.required = Boolean.parseBoolean(value);
		
		field.type = parent.getAttributeValue("Type");		
		
		if (field.type == null)
			throw new Exception(String.format("Field %1$s is missing a type declaration", field.id));
		
		field.format = parent.getAttributeValue("Format");
		field._default = parent.getAttributeValue("Default");
		
		if (field.format == null) {
			if (field.type.equalsIgnoreCase("String"))
				field.format = String.format("%%1$-" + "%1$d" + "s", field.width());
			else if (field.type.equalsIgnoreCase("Date"))
				field.format = "ddMMyyyy";
			else if (field.type.equalsIgnoreCase("Integer")) 
				field.format = String.format("%%1$0" + "%1$d" + "d", field.width());
		}

		if ((value = parent.getAttributeValue("MinOccurs")) != null)
			field.min = Integer.parseInt(value);
		
		if ((value = parent.getAttributeValue("MaxOccurs")) != null)
			field.max = Integer.parseInt(value);
		
		return field;
	}
	
	protected void write(StringBuffer sb, int offset, ModelBean bean, RecordDefinition record) throws Exception {
				
		Log.debug("FileWriter::write(StringBuffer sb, int offset, ModelBean bean, RecordDefinition record) begin");
		
		if (enable_debug)
			Log.debug(String.format("Offset: %1$d", offset));
		
		try {
			for (FieldDefinition field : record.fields) {
				Object value = null;

				if (enable_debug)
					Log.debug(String.format("Processing field %1$s of type %2$s", field.id, field.type));
				
				if (records.containsKey(field.type)) {
					//
					// Process a sub record.
					//
					ModelBean[] beans = bean.getBeans(field.id);
					
					for (int index = 0, count = beans.length; index < count; ++index) {
						if (index < field.max)
							write(sb, (offset + field.start) + (field.width() * index), beans[index], getRecordDefintion(field.type));
					}
				}
				else {					
	
					try {
						if (bean.hasBeanField(field.id))
							value = bean.getValue(field.id);
	
						if (value == null && field._default != null) {
							if (enable_debug)
								Log.debug("Constructing value from default");
							
							Class<?> cls = getFieldType(field.type);
							Class<?>[] argtypes = { String.class };
							Object[] arglist = { field._default };
							Constructor<?> ctor = cls.getConstructor(argtypes);
	
							value = ctor.newInstance(arglist);
						}
	
						if (value == null) {
							if (field.required) {
								throw new Exception("Value for field %1$s is required and no default exists.");
							} else {
								continue; // There is no data, so get the next field.
							}
						}
						
						//
						// At this point a value exists, now it must be the correct length
						//
	
						String formatted = null;
	
						if (field.format != null) {
							if (enable_debug)
								Log.debug(String.format("Formatting to field %1$s value %2$s using format %3$s", field.id, value.toString(), field.format));
	
							try {
								if (value instanceof StringDate)
									formatted = ((StringDate) value).toStringDisplay(field.format);
								else
									formatted = String.format(field.format, value);
							}
							catch (Exception e) {
								Log.debug(String.format("Failed to field %1$s value %2$s using format %3$s", field.id, value.toString(), field.format));
								
								throw e;
							}
						}
	
						// Copy the value to the record buffer.
						if (formatted != null) {
							if (formatted.length() != field.width())
								throw new Exception(String.format("Formatted field %1$s length %2$d does not match expected field length %3$d", field.id, formatted.length(), field.width()));
							
							sb.replace(offset + field.start, offset + field.end, formatted);
						}
						
						if (enable_debug)
							Log.debug(String.format("Buffer Length %1$d, Record Length %2$d", sb.length(), record.length));
						
					}
					catch (Exception e) {
						Log.debug(String.format("Failed to write field %1$s in record %2$s from bean %3$s", field.id, record.getKey(), bean.toString()));

						throw e;
					}
				}
			}
		} catch (Exception e) {

			throw e;
		}
		
		Log.debug("FileWriter::write(StringBuffer sb, int offset, ModelBean bean, RecordDefinition record) complete");
	}
	
	public void write(ModelBean bean, RecordDefinition record) throws Exception {

		Log.debug("FileWriter::write begin");

		try {
			StringBuffer sb = new StringBuffer(record.length);

			// Initialize it to padding
			for (int index = 0, count = record.length; index < count; ++index) {
				sb.append(record.padding);
			}

			write(sb, 0, bean, record);

			write(sb.toString(), 0, record.length);
			write(rs);

		}
		catch (Exception e) {

			throw e;
		}

		Log.debug("FileWriter::write complete");
	}
	
	protected String rs = System.getProperty("line.separator");
	
	protected boolean enable_debug = false;
}
