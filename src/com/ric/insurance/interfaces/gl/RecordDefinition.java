package com.ric.insurance.interfaces.gl;

import java.util.ArrayList;

public class RecordDefinition {

	public String getKey() {
		return id + "::" + variant;
	}
	
	
	/**
	 * Unique Identifier, for the record<br>
	 * Used By: CSV, Fixed
	 */
	public String id = null;
	/**
	 * Variation of the record<br>
	 * Used By: CSV, Fixed
	 */
	public String variant = null;
	/**
	 * Specifies the type of record. One of:<br>
	 * <ul><li>CSV</li><li>Fixed</li></ul>
	 * Used By: CSV, Fixed
	 */
	public String format = null;

	/**
	 * Record Separator Character<br>
	 *
	 * Used By: CSV
	 */
	public String rs = System.getProperty("line.separator");
	/**
	 * Field Separator Character<br>
	 *
	 * Used By: CSV, Fixed
	 */
	public String fs = ",";
	/**
	 * Quote Character<br>
	 *
	 * Used By: CSV
	 */
	public String quote = "\"";
	/**
	 * Force Quote Character<br>
	 *
	 * Used By: CSV
	 */
	public boolean force_quote = false;
	/**
	 * Length of record.<br>
	 *
	 * Used By: Fixed
	 */
	public int length = 0;
	/**
	 * Padding Character<br>
	 *
	 * Used By: Fixed
	 */
	public char padding = ' ';
	/**
	 * Base offset<br>
	 *
	 * Used By: Fixed
	 */
	public int base = 0;

	public ArrayList<FieldDefinition> fields = new ArrayList<FieldDefinition>();
	

}
