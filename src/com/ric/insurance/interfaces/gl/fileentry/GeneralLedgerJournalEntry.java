package com.ric.insurance.interfaces.gl.fileentry;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.innovextechnology.service.Log;
import com.iscs.common.mda.Store;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.table.Row;
import com.iscs.insurance.interfaces.gl.Book;
import com.iscs.insurance.interfaces.gl.GLCustomerTables;
import com.iscs.insurance.interfaces.gl.GLCustomerTables.GLMissingRowException;
import com.iscs.insurance.interfaces.gl.GLTools.ModelBeanGroup;
import com.iscs.insurance.interfaces.gl.GLTransaction;
import com.iscs.insurance.interfaces.gl.Settings;
import com.iscs.insurance.interfaces.gl.output.handler.GLOutputHandlerBase;
import com.ric.insurance.interfaces.gl.output.handler.GLOutputHandler;

import net.inov.mda.MDAOption;
import net.inov.mda.MDATable;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.date.StringDate;
import java.io.File;

public class GeneralLedgerJournalEntry extends GLFileEntryBase{
	
	protected String period = null;
	public GeneralLedgerJournalEntry(GLOutputHandler handler) {
		super(handler);
	}
	protected void init(ModelBean ap) throws Throwable {
        this.period = ap.gets("Period");
	}
	
	/** Add the file entry information
	 * @param transactionName The transaction name
	 * @param transactionConstants The transaction constants
	 * @param transactionMap The transaction map
	 * @param group The groups containing the information
	 * @throws Exception
	 */
	@Override
	public void addEntryInfo(String transactionName, Row transactionConstants, HashMap<String, GLTransaction> transactionMap, ModelBeanGroup group) throws Exception {
		try {
			ModelBean glStatBean = group.getBeans()[0];
			String transactionDesc = glStatBean.gets("TransactionName");
			String arReceiptType = glStatBean.gets("ARReceiptTypeCd");
			String carrierCd = glStatBean.gets("CarrierCd");
			if( carrierCd.equals("") ){
				carrierCd = getDefaultCarrier();
			}
			String stateCd = glStatBean.gets("StateCd");
			String asl = glStatBean.gets("AnnualStatementLineCd");
			String referenceId = glOutputHandler.getRowString(transactionConstants, "ReferenceId");
			String source = glOutputHandler.getRowString(transactionConstants, "Source");
			String accountingBasis = glOutputHandler.getRowString(transactionConstants, "AccountingBasis");
			if(glStatBean.gets("TransactionName").equalsIgnoreCase("AR Receipts")){
				if(StringTools.in(arReceiptType,"Check,Bank Check,Finance,Cash"))
					transactionDesc = transactionDesc.concat(": Cash");
				else if(arReceiptType.contains("ACH")){
					transactionDesc = (arReceiptType.equalsIgnoreCase("ACH Customer")) ? transactionDesc.concat(": ECheck") : transactionDesc.concat(": Agency Sweep");
				}else{
					transactionDesc = transactionDesc.concat(": " + arReceiptType);
				}
					
			}
			String department = glOutputHandler.getRowString(transactionConstants, "Department");
			
			String[] propertiesKeys = new String[]{"CarrierCd", "StateCd", "AnnualStatementLineCd", "ReferenceId", "Source", "AccountingBasis", "TransactionName", "Department"};
			String[] propertiesValues =  new String[]{carrierCd, stateCd, asl, referenceId, source, accountingBasis, transactionDesc, department};
			
			addTransactionProperties(transactionName, transactionMap, group, propertiesKeys, propertiesValues);
		} catch (Exception e) {
			Log.error(e);
			throw e;
		}
	}

	/** Write the file entry to the file
	 * @param glTransaction The GL transaction object
	 * @throws Exception
	 */
	@Override
	public void writeEntry(GLTransaction glTransaction) throws Exception {
		for(String distribution: new String[] {Book.DISTRIBUTION_DEBITS, Book.DISTRIBUTION_CREDITS}) {
			for(Map.Entry<String, BigDecimal> entry: glTransaction.get(distribution).entrySet()) {
				BigDecimal amount = entry.getValue().setScale(2, BigDecimal.ROUND_HALF_UP).abs();
				
				// Some entries may be zero if they got reversed as the transaction was built up, don't print those
				if(amount.compareTo(BigDecimal.ZERO) != 0) {
					ModelBean bean = new ModelBean("GLJournal");
					
					
					StringDate runDate = glOutputHandler.getRunDt();
					String[] formatedDt = runDate.toStringDisplay("MM-dd-yyyy").split("-");
					String descDt = runDate.toStringDisplay("MMddyy");
					String carrierCd = "";
					String DCamount = "";
					bean.setValue("AccountNumber", entry.getKey());
					bean.setValue("AccountingBasis",  glTransaction.getProperty("AccountingBasis"));
					bean.setValue("CalendarAccountingMonth",  formatedDt[0]);
					bean.setValue("CalendarAccountingYear",  formatedDt[2]);
					if( glTransaction.getProperty("CarrierCd").equalsIgnoreCase("RIC"))
						carrierCd = "1RIC";
					else if( glTransaction.getProperty("CarrierCd").equalsIgnoreCase("RCC"))
						carrierCd = "2RCC";
					bean.setValue("CompanyCode",  carrierCd);
					if(Book.DISTRIBUTION_DEBITS.equals(distribution)) {
						DCamount = "+";
					} else if(Book.DISTRIBUTION_CREDITS.equals(distribution)) {
						DCamount = "-";
					}
					String convertAmount = StringRenderer.leftPad((amount.toString()).replace(".", ""), "0", 14);
					String[] refId = glTransaction.getProperty("ReferenceId").split("<");
					bean.setValue("DebitCreditIndicator",  DCamount);
					bean.setValue("ConvertedAmount",  convertAmount);
					bean.setValue("ReferenceID",  StringRenderer.concat(refId[0], descDt));
					bean.setValue("SourceCode",  glTransaction.getProperty("Source"));
					bean.setValue("TransactionDay",  formatedDt[1]);
					bean.setValue("TransactionMonth",  formatedDt[0]);
					bean.setValue("TransactionYear",  formatedDt[2]);
					bean.setValue("BusinessUnitCode1",  glTransaction.getProperty("Department"));
					String aslCd = "";
					String annualStmtLineCd = glTransaction.getProperty("AnnualStatementLineCd");
					if(StringRenderer.in(annualStmtLineCd, "BS1,EN1,MBD,SLE")){
						aslCd = "L01.FIRE";
					}else if(annualStmtLineCd.equals("030")){
						aslCd = "L03.FO";
					}else if(annualStmtLineCd.equals("040")){
						aslCd = "L04.HO";
					}else if(annualStmtLineCd.equals("051")){
						aslCd = "L05.1.CMP";
					}else if(annualStmtLineCd.equals("052")){
						aslCd = "L05.2.CMP";
					}else if(annualStmtLineCd.equals("171")){
						aslCd = "L17.1.OL";
					}else if(annualStmtLineCd.equals("192")){
						aslCd = "L19.2.PPA-LIAB";
					}else if(annualStmtLineCd.equals("211")){
						aslCd = "L21.1.PPA-PD";
					}else if(annualStmtLineCd.equals("034")){
						aslCd = "L34.IPP";
					}else if(annualStmtLineCd.equals("021")){
						aslCd = "L02.1.ALLIED";
					}else if(annualStmtLineCd.equals("LLL") || annualStmtLineCd.equals("PPL")){
						aslCd = "L17.1.OL";
					}
					bean.setValue("BusinessUnitCode3",  aslCd);
					bean.setValue("BusinessUnitCode5",  glTransaction.getProperty("StateCd"));
					
					bean.setValue("DescriptionDetail",  glTransaction.getProperty("TransactionName"));
					bean.setValue("EntryOperator",  "IN");
					Settings settings = new Settings();
					String[] fileName = glOutputHandler.getFilename(settings).split("gl/");
//					String[] finalName = fileName[1].split("//.");
					String finalName = fileName[1].substring(0, fileName[1].lastIndexOf("."));
					bean.setValue("HeaderDescription", finalName);
					
					
					getFileWriter().write(bean, "JournalRecord::Standard");
				}
			}
		}
		
	}

	@Override
	public String getFileFormat() {
		return "GeneralLedgerJournalEntry";
	}
	
	/** Get default carrier from coderef
	 * 
	 * @return The default carrier
	 * @throws Exception when an error occurs
	 */
	public String getDefaultCarrier() throws Exception {
		String carrierCd = "";
		try {
			MDATable table = (MDATable) Store.getModelObject("INGLStats::glstats-list::carriercd::all");
			MDAOption option = table.getOption("default");
			if( option!=null){
				carrierCd = option.getLabel();
			}
		    return carrierCd;
		} catch( Exception e){
			Log.error(e);
			return carrierCd;
		}

	}

}
