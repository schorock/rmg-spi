package com.ric.insurance.interfaces.gl.fileentry;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;

import com.ric.insurance.interfaces.gl.output.handler.GLOutputHandlerBase;
import com.innovextechnology.service.Log;
import com.iscs.ar.render.AccountRenderer;
import com.iscs.claims.claim.Claim;
import com.iscs.common.business.provider.render.ProviderRenderer;
import com.iscs.common.mda.Store;
import com.iscs.common.render.DateRenderer;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.render.VelocityTools;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.table.Row;
import com.iscs.insurance.interfaces.gl.GLTools.ModelBeanGroup;
import com.iscs.insurance.interfaces.gl.GLTransaction;
import com.iscs.payables.account.BankAccount;

import net.inov.mda.MDAOption;
import net.inov.mda.MDATable;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

public class APVoucherMemoEntry extends GLFileEntryBase{


	public APVoucherMemoEntry(GLOutputHandlerBase handler) {
		super(handler);
	}

	/** Add the file entry information
	 * @param transactionName The transaction name
	 * @param transactionConstants The transaction constants
	 * @param transactionMap The transaction map
	 * @param group The groups containing the information
	 * @throws Exception
	 */
	@Override
	public void addEntryInfo(String transactionName, Row transactionConstants, HashMap<String, GLTransaction> transactionMap, ModelBeanGroup group) throws Exception {
		try {
			ModelBean glStatBean = group.getBeans()[0];
			String carrierCd = glStatBean.gets("CarrierCd");
			if( carrierCd.equals("") ){
				carrierCd = getDefaultCarrier();
			}
			String vendorId = "";
			String description = "";
			String checkbookId = "";
			String stateCd = glStatBean.gets("StateCd");
			if(stateCd==null || stateCd.equals(""))
				stateCd="NA";
			String asl = glStatBean.gets("AnnualStatementLineCd");
			String insuranceTypeCd = glStatBean.gets("InsuranceTypeCd");
			if(insuranceTypeCd==null || insuranceTypeCd.equals(""))
				insuranceTypeCd="NA";
			String transactionDesc = glStatBean.gets("TransactionName");
			String productName = glStatBean.gets("ProductName");
			String statSourceId=glStatBean.gets("StatSourceId");
			String lossYear = glStatBean.gets("LossYear");
			String billMethod = glStatBean.gets("BillMethod");
			String paymentMethodCd = "";
			String statSource = glStatBean.gets("StatSource");
			if(statSource.equalsIgnoreCase("PayableStats")){
				String systemId = glStatBean.gets("StatSourceId");
				String SQL="Select PaymentMethodCd from PayableStats where SystemId ="+systemId;
				JDBCData data=VelocityTools.getConnection();
				ResultSet rSet = data.doSQL(SQL);
				while (rSet.next()) {
					paymentMethodCd = rSet.getString("PaymentMethodCd");
	  			}
			}
			if(billMethod==null || billMethod.equals(""))
				billMethod="NA";
			String paymentAmount = glStatBean.gets("ItemAmt");
			String paymentNumber = glStatBean.gets("ItemNumber");
			String paymentDt = glStatBean.getDate("ItemDt").toStringDisplay("MM-dd-yyyy");
			String paymentAccountCd = glStatBean.gets("PaymentAccountCd");
			String reserveCd = glStatBean.gets("ReserveCd");
			String classificationCd = glStatBean.gets("ClassificationCd");
			String policyNumber = glStatBean.gets("PolicyNumber");
			
			String referenceId = glOutputHandler.getRowString(transactionConstants, "ReferenceId");
			String accountingBasis = glOutputHandler.getRowString(transactionConstants, "AccountingBasis");
			String source = glOutputHandler.getRowString(transactionConstants, "Source");
			String department = glOutputHandler.getRowString(transactionConstants, "Department");
			String debitCreditInd = glOutputHandler.getRowString(transactionConstants, "SignExpectedGLStatsAmount");
			String[] propertiesKeys = new String[]{"PolicyNumber","VendorId", "Description", "CheckbookId", "PaymentAmount", "PaymentNumber", "PaymentDt", "PaymentAccountCd","CarrierCd","ReferenceId", "Source","StateCd", "AnnualStatementLineCd","AccountingBasis","InsuranceTypeCd","TransactionName","ProductName","LossYear","BillMethod","PolicyNumber","AgentNumber","AccountNumber","ClaimNumber","ClaimantNumber","PayToName","PayToCd","StatSourceId","ReserveCd","ClassificationCd","Department","DebitCreditInd","PaymentMethod"};
			String[] propertiesValues =  new String[]{policyNumber,vendorId, description, checkbookId, paymentAmount, paymentNumber, paymentDt, paymentAccountCd,carrierCd,referenceId, source,stateCd, asl, accountingBasis,insuranceTypeCd,transactionDesc,productName,lossYear,billMethod,glStatBean.gets("PolicyNumber"),glStatBean.gets("ProviderCd"),glStatBean.gets("AccountNumber"),glStatBean.gets("ClaimNumber"),glStatBean.gets("ClaimantNumber"),glStatBean.gets("PayToName"),glStatBean.gets("PayToCd"),statSourceId,reserveCd,classificationCd,department,debitCreditInd,paymentMethodCd};
			
			addTransactionProperties(transactionName, transactionMap, group, propertiesKeys, propertiesValues);
		} catch (Exception e) {
			Log.error(e);
			throw e;
		}
	}

	/** Write the file entry to the file
	 * @param glTransaction The GL transaction object
	 * @throws Exception
	 */
	@Override		
	public void writeEntry(GLTransaction glTransaction) throws Exception {
		ModelBean bean = new ModelBean("AccountsPayabaleHeader");
		JDBCData data=VelocityTools.getConnection();
		
		StringDate runDate = glOutputHandler.getRunDt();
		String reference = glTransaction.getProperty("ReferenceId");
		reference = reference.split("<MMDDYY>")[0];
		String formatedDt = runDate.toStringDisplay("MMddyy");
		reference = reference + formatedDt;
		String paymentAccountCd=glTransaction.getProperty("PaymentAccountCd");
		
		String transactionName=glTransaction.getProperty("TransactionName");
		String policyNumber = "";
		policyNumber = glTransaction.getProperty("AccountNumber");
		String payableId=glTransaction.getProperty("StatSourceId");
		String productName = "";
		String subTypeCd = "";
		String effectiveDt = "";
		String insuredPaymentMethod = "";
		String payableAccountNumber = "";
		String payablePolicyNumber = "";
		String insuredName = "";
		ModelBean payAccount = null;
		if(!policyNumber.contains("Refunds") && !policyNumber.equals("")){
			payAccount = AccountRenderer.getAccountByAccountNumber(policyNumber);
			if(payAccount !=null){
				ModelBean arpolicy = payAccount.getBean("ARPolicy");
				String policyRef = arpolicy.gets("PolicyRef");
				ModelBean policy = new ModelBean("Policy");
				policy = data.selectModelBean(policy, policyRef);
				ModelBean insured = policy.getBean("Insured");
				ModelBean partyInfo = insured.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "InsuredParty");
				ModelBean nameInfo = partyInfo.findBeanByFieldValue("NameInfo","NameTypeCd","InsuredName");
				insuredName = nameInfo.gets("CommercialName");
			}
		}
		
		String providerCd= "";
		providerCd=glTransaction.getProperty("AgentNumber");
		String accountNumber=glTransaction.getProperty("AccountNumber");
		String paymentMethod=glTransaction.getProperty("PaymentMethod");
		
		String paymentAmountString = glTransaction.getProperty("PaymentAmount");
		String paymentAmountDecString = glTransaction.getProperty("PaymentAmount");
		if (!paymentAmountString.isEmpty()){
			paymentAmountString = StringRenderer.formatValue(paymentAmountString, 2).replace(".", "");
		}
		
		String payDt = glTransaction.getProperty("PaymentDt");
		String payDate = "";
		String payMonth = "";
		String payYear = "";
		if (!paymentAmountString.isEmpty()){
			String[] paymentDate = payDt.split("-");
			payDate = paymentDate[1];
			payMonth = paymentDate[0];
			payYear = paymentDate[2];
		}
		
		String carrierCd = glTransaction.getProperty("CarrierCd");
		String carrierCode = "";
		if(carrierCd.equals("RIC")){
			carrierCode = "1" + carrierCd;
		}else if(carrierCd.equals("RCC")){
			carrierCode = "2" + carrierCd;
		}
		
		String fileName = "AP-" +carrierCd+"-Memo-Check-"+runDate.toStringDisplay("MM-dd-yyyy")+"_"+DateRenderer.getTime("HH-mm-ss");
		
		String formatedRunDt = runDate.toStringDisplay("MM-dd-yyyy");
		String runDt[] = formatedRunDt.split("-");
		String transactionDt = runDt[1];
		String transactionMonth = runDt[0];
		String transactionYear = runDt[2];
		String source = glTransaction.getProperty("Source");
		String claimNumber = glTransaction.getProperty("ClaimNumber");
		String payee = glTransaction.getProperty("PayToName");
		
		bean.setValue("RecordType", "1");
		bean.setValue("InvoiceAmt", String.format("%15s",paymentAmountString));
		bean.setValue("PayDay", transactionDt);
		bean.setValue("PayMonth", transactionMonth);
		bean.setValue("PayYear", transactionYear);
		bean.setValue("PayingCompany", carrierCode);
		bean.setValue("SourceCode", source);
		bean.setValue("TransactionDay", transactionDt);
		bean.setValue("TransactionMonth", transactionMonth);
		bean.setValue("TransactionYear", transactionYear);
		bean.setValue("CheckAmt",  String.format("%15s",paymentAmountString));
		bean.setValue("CheckDt", payDate);
		bean.setValue("CheckMonth", payMonth);
		bean.setValue("CheckYear", payYear);
		bean.setValue("PayeeCd",providerCd);
		if(paymentAccountCd.contains("Claims")){
			String irsBoxCd = "";
			bean.setValue("CheckDesc1", claimNumber);
			
			if(payee.length()<=60){
				bean.setValue("CheckDesc2", payee);
			}else if(payee.length()>60 && payee.length()<=120){
				bean.setValue("CheckDesc2", payee.substring(0, 59));
				bean.setValue("CheckDesc3", payee.substring(60, 119));
			}else if(payee.length()>120 && payee.length()<=180){
				bean.setValue("CheckDesc2", payee.substring(0, 59));
				bean.setValue("CheckDesc3", payee.substring(60, 119));
				bean.setValue("CheckDesc4", payee.substring(120, 179));
			}
			String paymentType = glTransaction.getProperty("ClassificationCd");
			ResultSet crs= getPayableData(data,payableId);
			String csouceRef = "";
			String cclaimantTransaction = "";
			if(crs!=null){
	  			while (crs.next()) {
	  				csouceRef = crs.getString("SourceRef");
	  				cclaimantTransaction = crs.getString("ClaimantTransactionIdRef");
	  			}
			}
			String [] csourceRefs = csouceRef.split("::");
			String cclaimRef = csourceRefs[1];
			ModelBean cclaim = new ModelBean("Claim");
			cclaim = data.selectModelBean(cclaim, cclaimRef);
			if(cclaim !=null){
				ModelBean cclaimant = cclaim.getBean("Claimant");
				if(cclaimant != null){
					ModelBean cclaimantTransactionBean = cclaimant.findBeanByFieldValue("ClaimantTransaction", "id", cclaimantTransaction);
					if(cclaimantTransactionBean != null){
						providerCd = cclaimantTransactionBean.gets("ProviderRef");
					}
				}
			}
			if(!providerCd.equals("")){
                ModelBean provider = ProviderRenderer.getProviderBySystemId(providerCd);
                String taxType = "";
                String taxNumber = "";
			if(provider != null){
			   String typeCd = provider.gets("ProviderTypeCd");
			   ModelBean partyInfo = provider.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "ProviderParty");
			   if(partyInfo != null){
			      ModelBean providerTaxInfo = partyInfo.findBeanByFieldValue("TaxInfo", "TaxTypeCd", "ProviderTaxInfo");
			      if(providerTaxInfo!= null){
			         String provider1099Ind = providerTaxInfo.gets("Required1099Ind");
			         if(provider1099Ind.equals("Yes")){
			        	 taxType = providerTaxInfo.gets("TaxIdTypeCd");
			        	 if(taxType.equals("FEIN")){
			        		 taxNumber = providerTaxInfo.gets("FEIN").replace("-", "");
			        	 }else{
			        		 taxNumber = providerTaxInfo.gets("SSN").replace("-", "");
			        	 }
			        	 if(paymentType.equals("Indemnity") && typeCd.equals("Medical")){
			        		 irsBoxCd = "1060";
			        	 }else if(paymentType.equals("Indemnity") && (typeCd.equals("Legal") || typeCd.equals("Outside Legal") )){
			        		 irsBoxCd = "1140";
			        	 }else if((paymentType.equals("Indemnity") && !(typeCd.equals("Legal") || typeCd.equals("Outside Legal") || typeCd.equals("Medical"))) || (!paymentType.equals("Indemnity"))){
			        		 irsBoxCd = "1070";
			             }
			         }
			      }
			   }
			}
          }
          bean.setValue("IRSBoxCd", irsBoxCd);
          if(!irsBoxCd.equals("")){
                bean.setValue("IRSForm", "1099MISC");
          }
		}else if(paymentAccountCd.contains("Refunds") || paymentAccountCd.contains("AccountBill")){
			bean.setValue("StubNotes1",insuredName);
			bean.setValue("StubNotes2",policyNumber);
			/*ModelBean transPayment = getPaymentBean(Integer.parseInt(payableId), data);
			if(transPayment != null){
				ModelBean paymentAllocation = transPayment.getBean("PaymentAllocation");
  				if(paymentAllocation != null){
  					String transDesc = paymentAllocation.gets("Memo");
  					bean.setValue("StubNotes3",transDesc);
  				}
  				ModelBean taxInfo = transPayment.findBeanByFieldValue("TaxInfo","TaxTypeCd","MailingTaxInfo");
  				String taxNumber = "";
  				if(taxInfo!=null){
  					String requiredInd = taxInfo.gets("Required1099Ind");
  					if(requiredInd.equals("Yes")){
  						String taxIdType = taxInfo.gets("TaxIdTypeCd");
  						if(taxIdType.equals("FEIN") ){
  							taxIdType = "C";
  							taxNumber = taxInfo.gets("FEIN").replace("-","");
  						}else{
  							taxIdType = "P";
  							taxNumber = taxInfo.gets("SSN").replace("-","");
  						}
  						bean.setValue("TaxIDNumber",taxNumber);
  						bean.setValue("TaxIDNumberType",taxIdType);
  					}else{
  						bean.setValue("TaxIDNumber","111111111");
  					}
  				}else{
  					bean.setValue("TaxIDNumber","111111111");
  				}
			}*/
		}
		if(!(paymentMethod.equalsIgnoreCase("Credit Card")))
			bean.setValue("CheckInd","C");
		bean.setValue("EntryOperator","IN");
		bean.setValue("HeaderDesc",fileName);
		bean.setValue("HeaderDescription",reference);
		bean.setValue("InvoiceDt",transactionDt);
		bean.setValue("InvoiceMonth",transactionMonth);
		bean.setValue("InvoiceYear",transactionYear);
		bean.setValue("MemoCheck","Y");
		
		if(payee.length()<=60){
			bean.setValue("PayeeName1", payee);
		}else if(payee.length()>60 && payee.length()<=120){
			bean.setValue("PayeeName1", payee.substring(0, 59));
			bean.setValue("PayeeName2", payee.substring(60, 119));
		}else if(payee.length()>120 && payee.length()<=180){
			bean.setValue("PayeeName1", payee.substring(0, 59));
			bean.setValue("PayeeName2", payee.substring(60, 119));
			bean.setValue("PayeeName3", payee.substring(120, 179));
		}
		
		int paymentId = 0;
		String claimantTransactionIdRef = "";
		String paymentMethodCd = "";
		String sourceRef = "";
		
		try {
			ResultSet rs= getPayableData(data,payableId);
			if(rs!=null){
  			while (rs.next()) {
  				sourceRef = rs.getString("SourceRef");
  				paymentId = rs.getInt("PaymentSystemId");
  				claimantTransactionIdRef = rs.getString("ClaimantTransactionIdRef");
  				paymentMethodCd = rs.getString("PaymentMethodCd");
  			}
  			String [] sourceRefs = sourceRef.split("::");
  			String creditCardNumber = "";
  			ModelBean arTrans = null;
  			if(sourceRefs.length == 3){
				String arTransIdRef = sourceRefs[2];
				if(arTransIdRef.startsWith("ARTrans"))
					arTrans = payAccount.findBeanById("ARTrans", arTransIdRef);
				if(arTrans != null){
					ModelBean electronicPayment = new ModelBean("ElectronicPayment");
					data.selectModelBean(electronicPayment, arTrans.gets("ARReceiptReference"));
					ModelBean epaymentSource = electronicPayment.getBean("ElectronicPaymentSource");
					creditCardNumber = epaymentSource.gets("CreditCardNumber");
				}
  			}
  			bean.setValue("ReferenceID",paymentId);
  			bean.setValue("AccrualMonth",transactionMonth);
  			bean.setValue("AccrualDay",transactionDt);
  			bean.setValue("AccrualYear",transactionYear);
  			if(paymentMethod.equalsIgnoreCase("Credit Card")){
  				bean.setValue("CheckNumber",arTrans.gets("ARReceiptReference") + creditCardNumber.substring(4));
  			}else{
  				bean.setValue("CheckNumber",glTransaction.getProperty("PaymentNumber"));
  			}
  			
  			ModelBean payment = getPaymentBean(paymentId, data);
  			if(payment!=null){
  				ModelBean paymentAllocation = payment.getBean("PaymentAllocation");
  				if(paymentAllocation != null){
  					String transDesc = paymentAllocation.gets("Memo");
  					if(paymentAccountCd.contains("Refunds") || paymentAccountCd.contains("AccountBill")) {
  						bean.setValue("StubNotes3",transDesc);
  					}
  				}
  				ModelBean mailAddr=payment.findBeanByFieldValue("Addr", "AddrTypeCd", "PayToMailingAddr");
  				if(mailAddr!=null){
  					bean.setValue("RemittanceAddr1",mailAddr.gets("Addr1"));
  					bean.setValue("RemittanceAddr2",mailAddr.gets("Addr2"));
  					bean.setValue("RemittanceCity",mailAddr.gets("City"));
  					bean.setValue("RemittanceState",mailAddr.gets("StateProvCd"));
  					bean.setValue("RemittanceZip",mailAddr.gets("PostalCode"));
  				}else{
  					mailAddr=payment.findBeanByFieldValue("Addr", "AddrTypeCd", "MailingAddr");
  	  				if(mailAddr!=null){
	  	  				bean.setValue("RemittanceAddr1",mailAddr.gets("Addr1"));
	  					bean.setValue("RemittanceAddr2",mailAddr.gets("Addr2"));
	  					bean.setValue("RemittanceCity",mailAddr.gets("City"));
	  					bean.setValue("RemittanceState",mailAddr.gets("StateProvCd"));
	  					bean.setValue("RemittanceZip",mailAddr.gets("PostalCode"));
  	  				}
  				}
  				
  				ModelBean taxInfo = payment.findBeanByFieldValue("TaxInfo","TaxTypeCd","MailingTaxInfo");
  				String paymentTaxNumber = "";
  				if(taxInfo!=null){
  					String requiredInd = taxInfo.gets("Required1099Ind");
  					if(requiredInd.equals("Yes")){
  						String payTaxIdType = taxInfo.gets("TaxIdTypeCd");
  						if(payTaxIdType.equals("FEIN") ){
  							payTaxIdType = "C";
  							paymentTaxNumber = taxInfo.gets("FEIN").replace("-","");
  						}else{
  							payTaxIdType = "P";
  							paymentTaxNumber = taxInfo.gets("SSN").replace("-","");
  						}
  						bean.setValue("TaxIDNumber",paymentTaxNumber);
  						bean.setValue("TaxIDNumberType",payTaxIdType);
  					}else{
  						bean.setValue("TaxIDNumber","111111111");
  					}
  				}else{
  					bean.setValue("TaxIDNumber","111111111");
  				}
  			}
			}
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		getFileWriter().write(bean, "AccountPayable::Header");
		
		String department = glTransaction.getProperty("Department");
		int counter = 0;
		
		if(paymentAccountCd.contains("Claims")){
		
			for(int i=0;i<=5;i++){
				bean = new ModelBean("AccountsPayUnlimited");
				bean.setValue("RecordType", "2");
				String lossDt = "";
				String invoiceNumber = "";
				String souceRef = "";
				String claimantTransaction = "";
				String memoCd = "";
				String featureCd = "";
				
				if(paymentAccountCd.contains("Claims")){
					ResultSet rs= getPayableData(data,payableId);
					if(rs!=null){
			  			while (rs.next()) {
			  				souceRef = rs.getString("SourceRef");
			  				claimantTransaction = rs.getString("ClaimantTransactionIdRef");
			  			}
					}
					String [] sourceRefs = souceRef.split("::");
					String claimRef = sourceRefs[1];
					ModelBean claim = new ModelBean("Claim");
					claim = data.selectModelBean(claim, claimRef);
					
					if(claim != null){
						ModelBean claimant = claim.getBean("Claimant");
						lossDt = claim.gets("LossDt");
						if(claimant != null){
							ModelBean claimantTransactionBean = claimant.findBeanByFieldValue("ClaimantTransaction", "id", claimantTransaction);
							if(claimantTransactionBean != null){
								memoCd = claimantTransactionBean.gets("Memo");
								invoiceNumber = claimantTransactionBean.gets("InvoiceNumber");
								ModelBean[] featureAllocations = claimantTransactionBean.getBeans("FeatureAllocation");
								if(featureAllocations != null){
									for(ModelBean featureAllocation : featureAllocations){
										String curFeatureCd = featureAllocation.gets("FeatureCd");
										featureCd = curFeatureCd+""+featureAllocation.gets("FeatureSubCd");
									}
								}
							}
						}
					}
					
				}
				
				String[] stubDescs = {claimNumber,invoiceNumber,lossDt,memoCd,featureCd,StringRenderer.formatValue(paymentAmountDecString, 2,"0.00", false, false)};
				
							
				String routingNumber = "";
				String bankAccountType = "";
				String bankAccountNumber = "";
				if(!paymentAccountCd.equals("")){
					BankAccount account= new BankAccount();
					ModelBean bankAccount=account.getAccount(VelocityTools.getConnection(), paymentAccountCd);
					if(bankAccount!=null){
						routingNumber=bankAccount.gets("BankRoutingNumber");
						bankAccountType=bankAccount.gets("AccountTypeCd");
						
						bankAccountNumber=bankAccount.gets("AccountNumber");
					}
				}
				
				
				bean.setValue("InvoiceAmount",  String.format("%15s",paymentAmountString));
				bean.setValue("PayDate", transactionDt);
				bean.setValue("PayMonth", transactionMonth);
				bean.setValue("PayYear", transactionYear);
				
				
				bean.setValue("PayingCompany", carrierCode);
				bean.setValue("ReferenceID", reference);
		
				
				bean.setValue("SourceCd", source);
				
				
				bean.setValue("TransactionDt", transactionDt);
				bean.setValue("TransactionMonth", transactionMonth);
				bean.setValue("TransactionYear", transactionYear);
				if(paymentAccountCd.contains("Claims")){
					bean.setValue("StunDescUnlimited",stubDescs[counter]);
				}
				counter++;
				getFileWriter().write(bean, "AccountPayable::Unlimited");
			}
		}
		
		bean = new ModelBean("AccountsPayabaleDetail");
		
		String debitCreditInd = glTransaction.getProperty("DebitCreditInd");
		if(debitCreditInd.equals("Positive")){
			bean.setValue("DeibtCreditInd","+");
		}else{
			bean.setValue("DeibtCreditInd","-");
		}
		
		
		
		String detailPaymentAmt = String.format("%14s",paymentAmountDecString);
		String aslCd = "";
		String annualStmtLineCd = glTransaction.getProperty("AnnualStatementLineCd");
		if(StringRenderer.in(annualStmtLineCd, "BS1,EN1,MBD,SLE")){
			aslCd = "L01.FIRE";
		}else if(annualStmtLineCd.equals("030")){
			aslCd = "L03.FO";
		}else if(annualStmtLineCd.equals("040")){
			aslCd = "L04.HO";
		}else if(annualStmtLineCd.equals("051")){
			aslCd = "L05.1.CMP";
		}else if(annualStmtLineCd.equals("052")){
			aslCd = "L05.2.CMP";
		}else if(annualStmtLineCd.equals("171")){
			aslCd = "L17.1.OL";
		}else if(annualStmtLineCd.equals("192")){
			aslCd = "L19.2.PPA-LIAB";
		}else if(annualStmtLineCd.equals("211")){
			aslCd = "L21.1.PPA-PD";
		}else if(annualStmtLineCd.equals("034")){
			aslCd = "L34.IPP";
		}else if(annualStmtLineCd.equals("021")){
			aslCd = "L02.1.ALLIED";
		}else if(annualStmtLineCd.equals("LLL") || annualStmtLineCd.equals("PPL")){
			aslCd = "L17.1.OL";
		}
		
		bean.setValue("ConvertedAmt", StringUtils.leftPad(paymentAmountString, 14,'0'));
		bean.setValue("TagValue", "A");
		bean.setValue("TagLength", "1");
		bean.setValue("TagStartPosition", "1");
		bean.setValue("AccountNumber", "25200");
		bean.setValue("SourceCode", source);
		bean.setValue("CalendarAccountingMonth", transactionMonth);
		bean.setValue("CalendarAccountingYear", transactionYear);
		bean.setValue("CompanyCd", carrierCode);
		bean.setValue("ReferenceID", reference);
		bean.setValue("TransactionDay", transactionDt);
		bean.setValue("TransactionMonth", transactionMonth);
		bean.setValue("TransactionYear", transactionYear);
		bean.setValue("DescDetail", transactionName);
		bean.setValue("BusinessUnitCode1", department);
		bean.setValue("BusinessUnitCode3", aslCd);
		//bean.setValue("ReportSort","");
		getFileWriter().write(bean, "AccountPayable::Detail");
	}	
	
	
	@Override
	public String getFileFormat() {
		return "APVoucherMemo";
	}
	
	/** Get default carrier from coderef
	 * 
	 * @return The default carrier
	 * @throws Exception when an error occurs
	 */
	public String getDefaultCarrier() throws Exception {
		String carrierCd = "";
		try {
			MDATable table = (MDATable) Store.getModelObject("INGLStats::glstats-list::carriercd::all");
			MDAOption option = table.getOption("default");
			if( option!=null){
				carrierCd = option.getLabel();
			} 
		    return carrierCd;
		} catch( Exception e){
			Log.error(e);
			return carrierCd;
		}

	}
	
	
	
	private ResultSet getPayableData(JDBCData data,String payableId)throws Exception{
		ResultSet rSet = null;
		if(payableId!=null && !payableId.equals("")){
			String SQL="Select SourceRef,PaymentSystemId,ClaimantTransactionIdRef,PaymentMethodCd from PayableStats where SystemId ="+payableId;
			rSet = data.doSQL(SQL);
		}	
		return rSet;
	}

	private ModelBean getPaymentBean(int paymentId, JDBCData data)
			throws ModelBeanException, Exception, SQLException {
		ModelBean payment=null;
		if(paymentId!=0){
			payment= new ModelBean("Payment");
			payment =data.selectModelBean(payment, paymentId);
		}
		return payment;
	}
	
	
}
