package com.ric.insurance.interfaces.gl.fileentry;

import java.math.BigDecimal;
import java.util.HashMap;

import com.ric.insurance.interfaces.gl.FileWriter;
import com.iscs.insurance.interfaces.gl.GLCustomerTables;
import com.iscs.insurance.interfaces.gl.GLTransaction;
import com.ric.insurance.interfaces.gl.output.handler.GLOutputHandler;
import com.ric.insurance.interfaces.gl.output.handler.GLOutputHandlerBase;
import com.ric.insurance.interfaces.gl.output.handler.GLOutputHandlerBase.CreditDebitAccounts;

import net.inov.tec.beans.ModelBeanException;

import com.iscs.common.utility.table.Row;
import com.iscs.insurance.interfaces.gl.GLTools.ModelBeanGroup;

public abstract class GLFileEntryBase {

	protected GLOutputHandler glOutputHandler;
	
	public GLFileEntryBase(GLOutputHandlerBase handler){
		glOutputHandler = (GLOutputHandler) handler;
	}
	
	/** Get the FileWriter from the map based on the file format
	 * @return The FileWriter
	 * @throws Exception
	 */
	public FileWriter getFileWriter() throws Exception{
		return glOutputHandler.getFileWriters().get(getFileFormat());
	}
	
	/** Add the transaction properties
	 * @param transactionName The transaction name
	 * @param transactionMap The transaction map
	 * @param group The group
	 * @param propertiesKeys Array with properties names
	 * @param propertiesValues Array with properties values
	 * @throws Exception
	 * @throws ModelBeanException
	 */
	protected void addTransactionProperties(String transactionName, HashMap<String, GLTransaction> transactionMap,
			ModelBeanGroup group, String[] propertiesKeys, String[] propertiesValues) throws Exception, ModelBeanException {
		
		CreditDebitAccounts creditDebit = glOutputHandler.getAccountsForGroup(transactionName, group);
		GLTransaction glTransaction = glOutputHandler.createTransactionForCustomer(transactionName);
		
		glTransaction.setProperty("FileFormat", getFileFormat());
		
		String key = creditDebit.debit + "::" + creditDebit.credit + "::";

		for (int i=0; i< propertiesKeys.length; i++){
			glTransaction.setProperty(propertiesKeys[i], propertiesValues[i]);
			key = key + propertiesValues[i] + "::";
		}
		
		String groupAmount = group.sum("Amount");
		BigDecimal amount = new BigDecimal(groupAmount);
		
		boolean isSignExpectedGLStatsAmount = isSignExpectedGLStatsAmountNegative(transactionName);
		boolean negativeAmount = amount.compareTo(new BigDecimal(0)) < 0;
		
		boolean reverseInd = isSignExpectedGLStatsAmount ^ negativeAmount;//XOR
		
		GLTransaction existingGlTransaction = transactionMap.get(key);
		if (existingGlTransaction == null){
			glTransaction.enter(creditDebit.debit, creditDebit.credit, amount, reverseInd);
			transactionMap.put(key, glTransaction);
		} else {
			existingGlTransaction.enter(creditDebit.debit, creditDebit.credit, amount, reverseInd);
		}
	}
	
	/** Find if the expected sign for the transaction in the GLStats is negative
	 * @param transactionName The transaction name
	 * @return True if the expected sign for the transaction in the GLStats is negative
	 * @throws Exception
	 */
	public boolean isSignExpectedGLStatsAmountNegative(String transactionName) throws Exception{
    	Row transactionConstants = GLCustomerTables.getTransactionConstants(transactionName);
    	String signExpectedGLStatsAmount =  glOutputHandler.getRowString(transactionConstants, "SignExpectedGLStatsAmount");

		if (!signExpectedGLStatsAmount.isEmpty()){
			return signExpectedGLStatsAmount.equals("Negative");
		}
		throw glOutputHandler.mappingError("No configuration to get SignExpectedGLStatsAmount for transaction [%s]", transactionName);
	}

	/** Get the file format (like: GeneralLedgerJournalEntry, APVoucherMemo, or APVoidStop. 
	 * @return the file format
	 */
	public abstract String getFileFormat(); 
	
	/** Add the file entry information
	 * @param transactionName The transaction name
	 * @param transactionConstants The transaction constants
	 * @param transactionMap The transaction map
	 * @param group The groups containing the information
	 * @throws Exception
	 */
	public abstract void addEntryInfo(String transactionName, Row transactionConstants, HashMap<String, GLTransaction> transactionMap, ModelBeanGroup group) throws Exception;
	
	/** Write the file entry to the file
	 * @param glTransaction The GL transaction object
	 * @throws Exception
	 */
	public abstract void writeEntry(GLTransaction glTransaction) throws Exception;

}	