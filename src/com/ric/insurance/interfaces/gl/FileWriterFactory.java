package com.ric.insurance.interfaces.gl;

import java.io.File;
import java.io.FileDescriptor;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.jdom.Element;

import com.iscs.insurance.interfaces.gl.ConfigurationException;
import com.iscs.insurance.interfaces.gl.FileDefinition;
import com.iscs.insurance.interfaces.gl.Settings;

import net.inov.mda.MDAException;
import net.inov.tec.xml.XmlDoc;

/**
 * @author peter.hose
 * 
 */
public class FileWriterFactory {

	public static FileWriter create(String id, String fileName) throws Exception {
		if (_isNullOrEmpty(id))
			throw new IllegalArgumentException("Argument 'id' is null or empty");

		if (_isNullOrEmpty(fileName))
			throw new IllegalArgumentException("Argument 'fileName' is null or empty");

		final FileDefinition definition = getFileDefinitions().get(id);

		if (definition == null)
			throw new ConfigurationException(String.format("File definition for %s was not configured", id));

		Class<?> cls = Class.forName(definition._class);
		Class<?>[] types = { String.class };
		Object[] arguments = { fileName };
		Constructor<?> ctor = cls.getConstructor(types);

		return (FileWriter) ctor.newInstance(arguments);
	}

	public static FileWriter create(String id, File file, boolean append) throws Exception {
		if (_isNullOrEmpty(id))
			throw new IllegalArgumentException("Argument 'id' is null or empty");

		final FileDefinition definition = getFileDefinitions().get(id);

		if (definition == null)
			throw new ConfigurationException(String.format("File definition for %s was not configured", id));

		Class<?> cls = Class.forName(definition._class);
		Class<?>[] types = { File.class, boolean.class };
		Object[] arguments = { file, append };
		Constructor<?> ctor = cls.getConstructor(types);

		return (FileWriter) ctor.newInstance(arguments);
	}

	public static FileWriter create(String id, File file) throws Exception {
		if (_isNullOrEmpty(id))
			throw new IllegalArgumentException("Argument 'id' is null or empty");

		final FileDefinition definition = getFileDefinitions().get(id);

		if (definition == null)
			throw new ConfigurationException(String.format("File definition for %s was not configured", id));

		Class<?> cls = Class.forName(definition._class);
		Class<?>[] types = { File.class };
		Object[] arguments = { file };
		Constructor<?> ctor = cls.getConstructor(types);

		return (FileWriter) ctor.newInstance(arguments);
	}

	public static FileWriter create(String id, FileDescriptor fd) throws Exception {
		if (_isNullOrEmpty(id))
			throw new IllegalArgumentException("Argument 'id' is null or empty");

		final FileDefinition definition = getFileDefinitions().get(id);

		if (definition == null)
			throw new ConfigurationException(String.format("File definition for %s was not configured", id));

		Class<?> cls = Class.forName(definition._class);
		Class<?>[] types = { FileDescriptor.class };
		Object[] arguments = { fd };
		Constructor<?> ctor = cls.getConstructor(types);

		return (FileWriter) ctor.newInstance(arguments);
	}

	public static FileWriter create(String id, String fileName, boolean append) throws Exception {
		if (_isNullOrEmpty(id))
			throw new IllegalArgumentException("Argument 'id' is null or empty");

		if (_isNullOrEmpty(fileName))
			throw new IllegalArgumentException("Argument 'fileName' is null or empty");

		final FileDefinition definition = getFileDefinitions().get(id);

		if (definition == null)
			throw new ConfigurationException(String.format("File definition for %s was not configured", id));

		Class<?> cls = Class.forName(definition._class);
		Class<?>[] types = { String.class, boolean.class };
		Object[] arguments = { fileName, append };
		Constructor<?> ctor = cls.getConstructor(types);

		return (FileWriter) ctor.newInstance(arguments);
	}

	protected static Map<String, FileDefinition> getFileDefinitions() throws MDAException, Exception {
		// Load on demand;
		if (files == null) {
			// Build the beans from the Record Definitions
			XmlDoc document = Settings.getFileDefinitions();
			if (document != null)
				files = copy(new HashMap<String, FileDefinition>(), document.getRootElement());
		}

		return files;
	}
	
	protected static Map<String, Class<?>> getFieldTypes() throws Exception {
		// Load on demand;
		if (types == null) {
			// Build the beans from the field types
			XmlDoc document = Settings.getFieldTypes();
			if (document != null) {
				types = new HashMap<String, Class<?>>();

				for (Iterator<?> children = document.getRootElement().getChildren("FieldType").iterator(); children.hasNext();) {
					Element child = (Element) children.next();

					types.put(child.getAttributeValue("Id"), Class.forName(child.getAttributeValue("Class")));
				}
			}
		}

		return types;
	}

	public static Class<?> getFieldType(String id) throws Exception {
		return getFieldTypes().get(id);
	}

	protected static Map<String, FileDefinition> copy(Map<String, FileDefinition> files, Element parent) throws Exception {

		for (Iterator<?> children = parent.getChildren("FileDefinition").iterator(); children.hasNext();) {
			Element child = (Element) children.next();

			FileDefinition file = copy(new FileDefinition(), child);

			files.put(file.id, file);
		}

		return files;
	}

	protected static FileDefinition copy(FileDefinition file, Element parent) throws Exception {
		file.id = parent.getAttributeValue("Id");
		file.format = parent.getAttributeValue("Format");
		file._class = parent.getAttributeValue("Writer");

		if (!file.validate())
			throw new ConfigurationException("File definition is invalid");

		return file;
	}

	private static boolean _isNullOrEmpty(String value) {
		return (value == null || value.isEmpty());
	}
	
	protected static Map<String, FileDefinition> files = null;
	protected static Map<String, Class<?>> types = null;
}
