package com.ric.insurance.interfaces.gl;


import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jdom.Attribute;
import org.jdom.Element;
import org.jdom.xpath.XPath;

import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDAXmlDoc;
import com.iscs.common.render.DynamicString;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.xml.XmlDoc;

public class Settings {

	public Settings() {
		super();
		map = new HashMap<String, Object>();
	}

	public Map<String, Object> getMap() {
		return map;
	}

	/**
	 * Lazy load of the general-ledger-settings.xml file, and execute any dynamic string using velocity
	 * 
	 * @return an XmlDoc object containing the contents of the XML file
	 * @throws Exception
	 */
	protected XmlDoc getSettings() throws Exception {
		if (settings == null) {
			MDAXmlDoc document = (MDAXmlDoc) Store.getModelObject("general-ledger-settings::*::*");

			if ((settings = document.getDocument()) != null) {
				List<?> elements = settings.getRootElement().getChildren();

				for (Iterator<?> element_iterator = elements.iterator(); element_iterator.hasNext();) {
					Element element = (Element) element_iterator.next();

					List<?> attributes = element.getAttributes();

					for (Iterator<?> attribute_iterator = attributes.iterator(); attribute_iterator.hasNext();) {
						Attribute attribute = (Attribute) attribute_iterator.next();

						if (attribute.getName().equalsIgnoreCase("Value")) {
							attribute.setValue(render(attribute.getValue()));
						}
					}
				}
			}
		}

		return settings;
	}

	public String getSetting(String variable) throws Exception {
		XmlDoc document = getSettings();

		if (variable == null || variable.isEmpty())
			throw new IllegalArgumentException("Argument 'variable' is null or empty");

		Object attribute = XPath.selectSingleNode(document.getRootElement(), String.format("./GeneralLedgerSetting[@Name='%1$s']/@Value", variable));

		if (attribute == null)
			throw new Exception(String.format("Setting %s not configured", variable));

		return render(((Attribute) attribute).getValue());
	}

	private String render(String value) throws ModelBeanException, Exception {
		if (value == null || value.isEmpty())
			throw new IllegalArgumentException("Argument 'value' is null or empty");

		return DynamicString.render(new ModelBean[] { new ModelBean("Company") }, map, value);
	}

	private static XmlDoc settings = null;

	private Map<String, Object> map;
}
