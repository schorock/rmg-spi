package com.ric.insurance.interfaces.model.render;

import java.io.File;
import net.inov.tec.beans.Helper_Beans;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import com.iscs.common.business.attachment.Attachment;
import com.iscs.common.render.DynamicString;
import com.iscs.common.render.Renderer;
import com.iscs.common.utility.io.FileTools;
import com.iscs.workflow.Task;

public class RICInterfaceRenderer implements Renderer {

	@Override
	public String getContextVariableName() {
		return "RICInterfaceHandler";
	}
	
	/**
	 * Adds attachment to a bean
	 * @param attachmentTemplate	Attachment template, full modelbeal
	 * @param userId				Current user id
	 * @param bean					the bean to add the attachment to
	 * @param todayDt				StringDate today date
	 * @param todayTm				String today current time
	 * @param file					File which is to be the attachment
	 * @param mda					The mda location of the template (UW, CL, etc)
	 * @return						Attachment modelbean
	 * @throws Exception	
	 */
	public static ModelBean addAttachment(ModelBean attachmentTemplate, String descPrefix, String userId, ModelBean bean, StringDate todayDt, String todayTm, File file, String mda) throws Exception {
		ModelBean attachment = new ModelBean("Attachment");
		attachment.setValue("TemplateId", attachmentTemplate.getId());
    	String desc = DynamicString.render(bean, attachmentTemplate.gets("Description"));
        attachment.setValue("Description", descPrefix + " " + desc);			               
        attachment.setValue("Status", "Active");
        attachment.setValue("AddUser", userId );
        attachment.setValue("AddDt", todayDt);
        attachment.setValue("AddTm", todayTm);
        String savedFile = Attachment.saveFile(attachment, file, mda, false);
        attachment.setValue("Filename", savedFile);			                         

        return attachment;
	}
	
	/**
	 * Adds a task to the bean
	 * @param data				JDBCData element
	 * @param taskTemplateId	template id ref for the task to add
	 * @param bean				The bean to add the task to
	 * @param userId			The user to be assigned to the task
	 * @param todayDt			The today date
	 * @param attachment		The attachment bean which refers the task
	 * @return
	 * @throws Exception
	 */
	public static ModelBean addTask(JDBCData data, String taskTemplateId, ModelBean bean, String userId, StringDate todayDt, ModelBean attachment) throws Exception {
        ModelBean task = Task.createTask(data, taskTemplateId, bean, userId, todayDt, attachment);
		String origDesc = task.gets("Description");
        task.setValue("Description", attachment.gets("Description") + " " + origDesc);
		task.setValue("Text", attachment.gets("Description") + " " + origDesc);
		
		Helper_Beans.setIds(task, task);

		return task;
	}
	
	/**
	 * Moves a file from original path to new directory
	 * @param file
	 * @param newDir
	 * @throws Exception
	 */
	public static void moveFile(File file, String newDir) throws Exception {
        StringBuilder fullname = new StringBuilder();
 		String filePath = file.getAbsolutePath();
		String extension = FileTools.getFileExtension(file);
		String oldFilename = filePath.substring(filePath.lastIndexOf(File.separator) + 1, filePath.lastIndexOf(extension) - 1);    		
		fullname.append(newDir)
				.append(File.separator)
				.append(oldFilename)
				.append(".")
				.append(extension);
		String newFilename = fullname.toString();		             	  
 		FileTools.copyFile(file.toURI().toURL(), newFilename);
 		file.delete();

	}
}