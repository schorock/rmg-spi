package com.ric.insurance.producer.render;

import java.util.ArrayList;

import com.iscs.common.utility.bean.BeanTools;
import com.iscs.insurance.producer.render.ProducerRenderer;

import net.inov.biz.server.ServiceContext;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;

public class RICProducerRenderer extends ProducerRenderer {
	
	/** Creates a new instance of RICProducerRenderer */
    public RICProducerRenderer() {
    }
	
    /** Context variable name getter
     */
    @Override
	public String getContextVariableName() {
        return "RICProducerRenderer";
    }
    
    /** 
     * Update producers' IVANS info by producer agency
     * @param producerAgency
     * @return list of updated producers
     */
    public static ModelBean[] updateIVANSProducerInfoByProducerAgency( String producerAgency, String policyExportInd, String policyExportStartDt, String policyExportEndDt ) throws Exception {
    	JDBCData data = ServiceContext.getServiceContext().getData();
    	ModelBean[] producers = BeanTools.selectModelBeansFromLookup(data, "Provider", "ProducerAgency", producerAgency);
    	
    	for( ModelBean producer : producers ) {
    		ModelBean producerInfo = producer.getBean("ProducerInfo");

    		producerInfo.setValue("PolicyExportInd", policyExportInd);
    		producerInfo.setValue("PolicyExportStartDt", policyExportStartDt);
    		producerInfo.setValue("PolicyExportEndDt", policyExportEndDt);
    		
    		BeanTools.saveBean(producer, data);
    	}

    	return producers;
    }
    
    /** 
     * Update producers' website info by producer agency
     * @param producerAgency
     * @return list of updated producers
     */
    public static ModelBean[] updateWebsiteAddressByProducerAgency( String producerAgency, String producerWebAddr ) throws Exception {
    	JDBCData data = ServiceContext.getServiceContext().getData();
    	ModelBean[] producers = BeanTools.selectModelBeansFromLookup(data, "Provider", "ProducerAgency", producerAgency);
    	
    	for( ModelBean producer : producers ) {
    		ModelBean producerInfo = producer.getBean("ProducerInfo");

    		producerInfo.setValue("ProducerWebAddr", producerWebAddr);
    		
    		BeanTools.saveBean(producer, data);
    	}

    	return producers;
    }

    /** 
     * Update Agent sweep account info by producer
     * @param producerAgency
     * @param source
     * @param locationNbr
     * @return list of updated producers
     */
    public static ArrayList<ModelBean> updateProducerSweepAccountByLocation(String producerAgency, ModelBean source, String locationNbr ) throws Exception {
    	if(producerAgency == null || locationNbr == null || locationNbr.equals(""))
    		return null;
    	
    	JDBCData data = ServiceContext.getServiceContext().getData();
    	ModelBean[] producers = BeanTools.selectModelBeansFromLookup(data, "Provider", "ProducerAgency", producerAgency);
    	ArrayList<ModelBean> producerAgents = new ArrayList<ModelBean>();
    	
    	for( ModelBean producer : producers ) {
    		String producerTypeCode = producer.getBean("ProducerInfo").gets("ProducerTypeCd");
    		if( !(locationNbr.equals(producer.gets("LocationNum")) && "Agent".equals(producerTypeCode)) )
    				continue;
    		
    		System.out.println(producer.readableDoc());
    		
    		ModelBean paymentSoruce = producer.getBean("ElectronicPaymentContract", "ContractTypeCd", "Premium").getBean("ElectronicPaymentSource");
    		paymentSoruce.setValue("ACHStandardEntryClassCd", source.gets("ACHStandardEntryClassCd"));
    		paymentSoruce.setValue("ACHBankAccountTypeCd", source.gets("ACHBankAccountTypeCd"));
    		paymentSoruce.setValue("ACHBankName", source.gets("ACHBankName"));
    		paymentSoruce.setValue("ACHBankAccountNumber", source.gets("ACHBankAccountNumber"));
    		paymentSoruce.setValue("ACHRoutingNumber", source.gets("ACHRoutingNumber"));
    		
    		BeanTools.saveBean(producer, data);
    		producerAgents.add(producer);
    	}

    	return producerAgents;
    }
}
