package com.ric.insurance.producer.handler;

import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.Helper_Beans;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

/**
 * RMGUAT-556 Overriding original process() and added fields, EORetroDt and EODeductible.
 *
 * @author rhoush
 */
public class ProducerComplete extends com.iscs.insurance.producer.handler.ProducerComplete {

  // Create an object for logging messages


  /** Creates a New Instance of ProviderComplete
   * @throws Exception never
   */
  public ProducerComplete() throws Exception {
  }

  /** Processes a generic service request.
   * @return the current response bean
   * @throws IBIZException when an error requiring user attention occurs
   * @throws ServiceHandlerException when a critical error occurs
   */
  public ModelBean process() throws IBIZException, ServiceHandlerException {
    try {
      // Log a Greeting
      Log.debug("Processing ProducerComplete...");

      ModelBean rs = getHandlerData().getResponse();
      ModelBean rq = this.getHandlerData().getRequest();
      AdditionalParams ap = new AdditionalParams(rq);
      ModelBean xfdf = rs.getBean("ResponseParams").getBean("XFDF");
      JDBCData data = this.getHandlerData().getConnection();

      ModelBean provider = rs.getBean("Provider");

      // Complete ProducerInfo bean
      ModelBean producerInfo = provider.getBean("ProducerInfo");
      String producerTypeCd = ap.gets("ProducerTypeCd");
      String producerAgency = ap.gets("ProducerAgency");
      String producerGroup = ap.gets("ProducerGroup");
      String appointedDt = StringDate.formatEntryDate(ap.gets("AppointedDt"), true);
      String branchCd = ap.gets("BranchCd");
      String eoCarrier = ap.gets("EOCarrier");
      String producerWebAddr = ap.gets("ProducerWebAddr");
      String eoPolicyNumber = ap.gets("EOPolicyNumber");
      String eoLimit = ap.gets("EOLimit");
      String eoDeductible = ap.gets("EODeductible");
      String eoExpirationDt = StringDate.formatEntryDate(ap.gets("EOExpirationDt"), true);
      String eoRetroDt = StringDate.formatEntryDate(ap.gets("EORetroDt"), true);
      String submitTo = ap.gets("SubmitTo");
      String submitToCd = ap.gets("SubmitToCd");
      String payToCd = ap.gets("PayToCd");
      String cpAccountNumber = ap.gets("CPAccountNumber");
      String cpAccountSuffix = ap.gets("CPAccountSuffix");
      String directPortalInd = ap.gets("DirectPortalInd");
      String policyExportInd = ap.gets("PolicyExportInd");
      String policyExportStartDt =  StringDate.formatEntryDate(ap.gets("PolicyExportStartDt"), true);
      String policyExportEndDt = StringDate.formatEntryDate( ap.gets("PolicyExportEndDt"), true);

      producerInfo.setValue("ProducerTypeCd",producerTypeCd);
      producerInfo.setValue("ProducerAgency",producerAgency);
      producerInfo.setValue("ProducerGroup",producerGroup);
      producerInfo.setValue("AppointedDt",appointedDt);
      producerInfo.setValue("BranchCd",branchCd);
      producerInfo.setValue("EOCarrier",eoCarrier);
      producerInfo.setValue("ProducerWebAddr",producerWebAddr);
      producerInfo.setValue("EOPolicyNumber",eoPolicyNumber);
      producerInfo.setValue("EOLimit",eoLimit);
      producerInfo.setValue("EODeductible",eoDeductible);
      producerInfo.setValue("EOExpirationDt",eoExpirationDt);
      producerInfo.setValue("EORetroDt",eoRetroDt);
      producerInfo.setValue("SubmitTo",submitTo);
      producerInfo.setValue("SubmitToCd",submitToCd);
      producerInfo.setValue("PayToCd", payToCd);
      producerInfo.setValue("CPAccountNumber", cpAccountNumber);
      producerInfo.setValue("CPAccountSuffix", cpAccountSuffix);
      producerInfo.setValue("DirectPortalInd", directPortalInd);
      producerInfo.setValue("PolicyExportInd", policyExportInd);
      producerInfo.setValue("PolicyExportStartDt", policyExportStartDt);
      producerInfo.setValue("PolicyExportEndDt", policyExportEndDt);

      // Tax id is a masked field and will not be in the XFDF if user did not change it
      ModelBean[] params = Helper_Beans.findBeansHavingFieldWithValue(xfdf, "Param", "Name", "TaxId");
      if( params.length > 0) {
        // Complete TaxInfo bean
        ModelBean taxInfo = provider.getBean("ProviderAcct").getBean("PartyInfo").getBean("TaxInfo");
        String taxId = params[0].gets("Value");

        if( taxInfo.gets("TaxIdTypeCd").equals("FEIN")) {
          taxInfo.setValue("FEIN", ModelSpecification.indexString(taxId) );
          taxInfo.setValue("SSN","");
        }
        else {
          taxInfo.setValue("FEIN","");
          taxInfo.setValue("SSN", ModelSpecification.indexString(taxId) );
        }
      }

      String pref = ap.gets("PaymentPreferenceCd");
      if (!pref.equals("")){
        provider.setValue("PaymentPreferenceCd",pref);
      }

      String providerOnCommissionPlan = ap.gets("ProviderOnCommissionPlan");

      if (providerOnCommissionPlan.isEmpty() || StringTools.isFalse(providerOnCommissionPlan)){
        provider.setValue("ProviderOnCommissionPlan", "No");
        validateSwitchFromCommissionPlan(data, provider);
      } else {
        provider.setValue("ProviderOnCommissionPlan", "Yes");
        updateCommissionPlans(ap, provider);
      }

      // Return the Response ModelBean
      return rs;
    }
    catch( Exception e ) {
      throw new ServiceHandlerException(e);
    }
  }
}
