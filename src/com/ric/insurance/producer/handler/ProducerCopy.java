/*
 * ProducerCopy.java
 *
 */

package com.ric.insurance.producer.handler;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

import com.iscs.common.business.rule.processor.velocity.RuleProcessorVelocity;
import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDATemplate;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.error.ErrorTools;


/** Copy another producer information into the current one. Service to allow duplicating when one producer has very similar data to another.
 *
 *
 * @author alavu
 */
public class ProducerCopy extends InnovationIBIZHandler {

	/** Creates a New Instance of ProducerCopy
     * @throws Exception never
     */
    public ProducerCopy() throws Exception {
    }
    
    /** /** Copy another producer information into the current one.  Service to allow duplicating  when one producer has very similar data to another.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
   
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
           
        	// Log a Greeting
            Log.debug("Processing ProducerCopy...");
            
            ModelBean rs = getHandlerData().getResponse();
            JDBCData data = this.getHandlerData().getConnection();
            AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest() );
            ModelBean responseParams = rs.getBean("ResponseParams");
            
            // Get the Producer bean to copy
            int systemId = Integer.parseInt(ap.gets("ProducerToCopy"));         
            ModelBean provider = new ModelBean("Provider");
            provider = data.selectModelBean(provider,systemId); 
            
            // Create a new provider ModelBean
            ModelBean newProvider = provider.cloneBean();            
            
            newProvider.setValue("SystemId","");           
            newProvider.setValue("IndexName","");
            newProvider.setValue("StatusCd","Active");           
            StringDate todayDt = DateTools.getStringDate(responseParams); 
            newProvider.setValue("StatusDt",todayDt);   
            newProvider.setValue("Comments","");
            newProvider.setValue("PreferredDeliveryMethod","");
            
            //Remove all ElectronicPaymentContract and Note beans
            newProvider.deleteBeans("ElectronicPaymentContract");
            newProvider.deleteBeans("Note");
            
            //Get the ProducerInfo and clear the fields
            ModelBean producerbean = newProvider.getBean("ProducerInfo");                    
            producerbean.setValue("EOCarrier","");
            producerbean.setValue("EOPolicyNumber","");
            producerbean.setValue("EOLimit","");
            producerbean.setValue("EOExpirationDt","");            
            producerbean.setValue("CommissionPayThroughDt","");
            producerbean.setValue("CommissionPayBankAccount","");
            producerbean.setValue("CommissionPayBankRouteId","");
            producerbean.setValue("CommissionPayBankType","");          

            //Get the provider account info and clear the fields
            ModelBean acctPartyInfo = newProvider.getBean("ProviderAcct").getBean("PartyInfo", "PartyTypeCd", "AcctParty"); 
            if( acctPartyInfo !=null ){            	
            	acctPartyInfo.deleteBeans("EmailInfo");
            	acctPartyInfo.deleteBeans("PhoneInfo");
            	acctPartyInfo.deleteBeans("NameInfo");
            	acctPartyInfo.deleteBeans("TaxInfo"); //RMGPROD-102
            }
            
            //Get the provider info and clear the fields
            ModelBean providerPartyInfo = newProvider.getBean("PartyInfo", "PartyTypeCd", "ProviderParty"); 
            if( providerPartyInfo !=null ){
            	providerPartyInfo.deleteBeans("EmailInfo");
            	providerPartyInfo.deleteBeans("PhoneInfo");
            	providerPartyInfo.deleteBeans("NameInfo");
            	providerPartyInfo.deleteBeans("TaxInfo"); //RMGPROD-102
            }
            
            // Get ProducerId Rule 
			String ruleTemplateMDA = "INProducer::rule::RuleTemplate::producerId-rule";		
			MDATemplate mdaTemplate = (MDATemplate) Store.getModelObject(ruleTemplateMDA);
			ModelBean ruleTemplate = mdaTemplate.getBean();
			
			// Run the rule through velocity to generate the next producer Number
			ModelBean error = new RuleProcessorVelocity().process(ruleTemplate, newProvider, null, null, data);
			if( !error.gets("Msg").equals("") ) {
				addErrorMsg("General", error.gets("Msg"), ErrorTools.GENERIC_BUSINESS_ERROR);
			} 
			
			//Save the provider	
            rs.setValue(newProvider);
            
            // Return the Response ModelBean
            return rs;           
        }
        catch( IBIZException bize ) {
            throw bize;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }

}
