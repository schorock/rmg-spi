package com.ric.insurance.producer.conversion;

import com.ric.common.business.provider.conversion.ProviderDataConversion;

import net.inov.tec.beans.ModelBean;

public class ProducerDataConversion extends ProviderDataConversion {

	public ProducerDataConversion() throws Exception {
		super();
	}
	
	/** Process the ModelBean by invoking it through a Service Chain or other methods to process the data before
	 * it is saved to Innovation
     * @param dtoBean The original DTO ModelBean
	 * @param bean The ModelBean in question
	 * @return ModelBean result of the Bean Processing
	 * @throws Exception when an error occurs
	 */
	protected ModelBean processModelBean(ModelBean dtoBean, ModelBean bean)
	throws Exception {
		return processModelBean(dtoBean, bean, "INExternalProducerLoadRq");		
	}
			
}