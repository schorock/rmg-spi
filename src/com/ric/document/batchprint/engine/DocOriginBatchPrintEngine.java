package com.ric.document.batchprint.engine;

import java.io.File;
import java.util.ArrayList;

import com.iscs.common.mda.Store;
import com.iscs.common.tech.storage.Storage;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.common.utility.io.FileTools;
import com.iscs.document.batchprint.engine.BatchPrintEngine;
import com.iscs.document.control.Output;
import com.iscs.document.print.PrintDriver;
import com.iscs.document.print.PrintDriver.MergeResult;
import com.iscs.document.print.engine.DocOriginPrintEngine;

import net.inov.mda.MDAOption;
import net.inov.mda.MDATable;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.file.FileIO;
import net.inov.tec.thread.ManagedTask;
import net.inov.tec.xml.XmlDoc;

public class DocOriginBatchPrintEngine extends com.iscs.document.batchprint.engine.DocOriginBatchPrintEngine implements BatchPrintEngine {

	private final String PRINT_ENGINE = "DocOrigin";
    private final String NEW_LINE = System.getProperty("line.separator");
	
    /** Call the Batch Print Engine to process the set of print documents
	 * @param data JDBCData Connection Object
	 * @param batchPrint BatchPrint ModelBean
	 * @param requestParams RequestParams ModelBean
	 * @param batchPrintCategoryTemplate BatchPrintCategoryTemplate ModelBean
	 * @param printerTemplate Output Printer Template
	 * @param outputType Output File Type (i.e. PDF, Printer)     
	 * @param errors ArrayList of Errors    
	 * @param managedTask The Managed Task
	 * @param totalPagesInBatch The total number of pages in the batch
	 * @param totalDocumentsInBatch The total number of documents in the batch
	 * @return list of BatchPrintDocument beans
	 * @throws Exception if an Unexpected Error Occurs
     */   
    public ModelBean[] printDocuments(JDBCData data, ModelBean batchPrint, ModelBean requestParams, ModelBean batchPrintCategoryTemplate, ModelBean printerTemplate, String outputType, ArrayList<ModelBean> errors, ManagedTask managedTask, String totalPagesInBatch, String totalDocumentsInBatch) throws Exception {
		// Get Request Params
		String userId = requestParams.gets("UserId");
		String sessionId = requestParams.gets("SessionId");
		String securityId = requestParams.gets("SecurityId");
		String conversationId = requestParams.gets("ConversationId");

		// Initialize Print Driver
		PrintDriver driver = new PrintDriver();

		// Retrieve the file from Storage Server
		MDATable table = (MDATable) Store.getModelObject("DCControl::output::path::all");                
		MDAOption option = table.getOption("Output");
		String outputPath = option.getLabel();

		// Create Array List Batch Print Document ModelBeans
		ArrayList<ModelBean> array = new ArrayList<ModelBean>();    
		XmlDoc blankXml = null;

		// Set the combined file to be used later
		DocOriginPrintEngine doEngine = new DocOriginPrintEngine();
		File combinedFile = FileTools.createUniqueFile(doEngine.getAttachDirectory(), "combined_output_" + userId, "pdf");

		// Loop Through BatchPrint LinkReferences
		boolean printError = false;
		ArrayList<ModelBean> documentArray = new ArrayList<ModelBean>();
		ModelBean[] linkReferenceArray = batchPrint.getBeans("LinkReference");
		for( int i = 0; i < linkReferenceArray.length; i++ ) {
			ModelBean linkReference = linkReferenceArray[i];
			
			// Create the Xml document that will merge all of the Xml document components
			XmlDoc xmlDocument = getXmlPrintDocument();

			if( linkReference.gets("Status").equals("") ) {	            		

				// Select Batch Print Document From Database 	            			
				ModelBean batchPrintDocument = new ModelBean("BatchPrintDocument");
				data.selectModelBean(batchPrintDocument, Integer.parseInt(linkReference.gets("SystemIdRef")));
				
				String printEngineCd = batchPrintDocument.gets("PrintEngineCd");
				String documentId = Integer.toString(i + 1);
				// Write Barcode Merge Data Out to StringBuffer
				XmlDoc barcodeXml = null;
				if( printEngineCd.equals(PRINT_ENGINE) ){		            	
					barcodeXml = buildBarcodeMergeData(batchPrint, batchPrintDocument, documentId, totalPagesInBatch, totalDocumentsInBatch);
					mergeXmlDocument(xmlDocument, barcodeXml);
				}

				// Set the Print Driver's Print Engine
				driver.setPrintEngine(printEngineCd);			            			           

				// Set the Print Driver's Print Executable Timeout
				if( batchPrintCategoryTemplate.valueEquals("CombineLevelCd","Category") ) {
					driver.setPrintExecutableTimeout(Output.CATEGORY_PRINT_EXECUTABLE_TIMEOUT);
				} else {
					driver.setPrintExecutableTimeout(Output.DOCUMENT_PRINT_EXECUTABLE_TIMEOUT);
				}

				// Obtain the file from the Storage Server
				String filename = Storage.retrieveFileToPrefs(batchPrintDocument.gets("MergeFileURL"), "output", outputPath);

				// Obtain the main Xml document details
				File file = new File(filename);
				XmlDoc mainDoc = new XmlDoc(file);

				// Initialize Beans Parameter
				ModelBean[] beans = new ModelBean[] {batchPrintDocument.cloneBean()};

				// Check for Coversheet
				String formTemplateIdRef = batchPrintDocument.gets("CoversheetFormTemplateIdRef");
				if( !formTemplateIdRef.equals("") && printEngineCd.equals(PRINT_ENGINE) ) {
					// Obtain the mapped coversheet if defined
					formTemplateIdRef = getEngineCoversheet(printEngineCd, formTemplateIdRef);
					
					// Render Coversheet Merge Data
					String mergeData = driver.renderMergeFile(data, formTemplateIdRef, beans, userId, sessionId, conversationId, securityId, "");

		            // Wrap the merge data with the needed XML root nodes
		            String finalMergeData = null;
		            try {
		            	new XmlDoc(mergeData);
		            	finalMergeData = mergeData;
		            } catch (Exception e) {
		                finalMergeData = "<XmlData>" + NEW_LINE + "<Document>" + NEW_LINE + mergeData + "</Document>" + NEW_LINE + "</XmlData>";
		            }
					
					// Write Coversheet Merge Data Out to the main Xml document
					XmlDoc coverXml = new XmlDoc(finalMergeData);
					mergeXmlDocument(xmlDocument, coverXml);
				}

				// Merge the main Document Merge Data into the Xml document
				mergeXmlDocument(xmlDocument, mainDoc);
				
				// Insert Blank Page if Simplex Page Count is an Odd Number
				if( StringTools.isTrue(batchPrintCategoryTemplate.gets("ForceEvenPageInd")) && printEngineCd.equals(PRINT_ENGINE)) {
					File mergeFile = FileTools.createTempFile("TMP");
					FileIO.stringToFile(xmlDocument.toString(), mergeFile.getPath(), "UTF-8");

					MergeResult mergeResult = new MergeResult();
					try {
						driver.countNumPrintPages(mergeFile, userId, mergeResult);

						int numSimplexPrintPages = mergeResult.getNumSimplexPrintPages();
						if( numSimplexPrintPages % 2 != 0 ) {
							if( blankXml == null ){
								String blankPageData = driver.renderMergeFile(data, "BlankPageDocOrigin", beans, userId, sessionId, conversationId, securityId, "");
								blankXml = new XmlDoc(blankPageData);
							}										    		
				    		mergeXmlDocument(xmlDocument, blankXml);				    							    	
						}						
					} catch( Exception e ) {
						printError = true;
					}
					mergeFile.delete();
				}

				// If Combine Level is Blank or Document, Create Output For Each Document
				if( batchPrintCategoryTemplate.valueEquals("CombineLevelCd","") || batchPrintCategoryTemplate.valueEquals("CombineLevelCd","Document") ) {
					try {
						// Merge Document and Send Output to Destination
						MergeResult mergeResult = driver.merge(xmlDocument.toString(), userId, printerTemplate, outputType, batchPrintCategoryTemplate, "");			            		

						// Update Link Reference Status
						linkReference.setValue("Status", "Printed");

						// Update Batch Print Document Status
						batchPrintDocument.setValue("StatusCd", "Printed");						
						
						// Distribute Output
						if( !batchPrintCategoryTemplate.valueEquals("FilenameFormat","") ) {						
							String outputFilename = distribute(data, batchPrintDocument, batchPrintCategoryTemplate, mergeResult.getFormURL());	
							if( !batchPrintCategoryTemplate.valueEquals("ZipFilenameFormat", "") ){
								batchPrintDocument.setValue("Filename", outputFilename);
								documentArray.add(batchPrintDocument);
							}
						}
					} catch( Exception e ) {
						// Update Link Reference Status
						linkReference.setValue("Status", "Error");

						// Update Batch Print Document Status
						batchPrintDocument.setValue("StatusCd", "Error");

						printError = true;
					}	

					// Save Batch Print Document
					data.saveModelBean(batchPrintDocument);
				} else if (batchPrintCategoryTemplate.valueEquals("CombineLevelCd","Category")) {
					if( batchPrintCategoryTemplate.valueEquals("OutputTypeCd","PDF") ) {
						File pdfFile = null;
						try {
							// Merge Document and Send Output to Destination
							MergeResult mergeResult = driver.merge(xmlDocument.toString(), userId, printerTemplate, outputType, batchPrintCategoryTemplate, "");	
							
							// Combine the PDF merge result into the combined file
							pdfFile = new File(mergeResult.getFormFilename());
							doEngine.executeCombinePDFCommand(combinedFile, pdfFile);
						} catch( Exception e ) {
							printError = true;
						} finally {
							// Clean up the current individual document file(s), Nerge the log files
							pdfFile.delete();
						}
					} else {
						// Combined Level of Category is only allowed for output type of PDF, so error out here.
						printError = true;
		            	Object[] msgArgs = {batchPrintCategoryTemplate.gets("Name")}; 
						String msg = "The current Batch Print Category of {0} has been set to be combined as one file. The only accepted output type is PDF.";
						ErrorTools.addError("General", msg, msgArgs, ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR, errors);
					}
					if (printError) {
						break;
					}
					array.add(batchPrintDocument);
				}
			}
		}
		
		// If Combine Level is Category, Create One Output For Each Category/Batch
		boolean firstDocument = true;
		if( batchPrintCategoryTemplate.valueEquals("CombineLevelCd","Category") && !printError) {
			try {
				// Update Link Reference Status
				for( ModelBean linkReference : linkReferenceArray ) {
					linkReference.setValue("Status", "Printed");
				}
				
				String outputFilename = "";
				// Distribute Output
				if( !batchPrintCategoryTemplate.valueEquals("FilenameFormat","") ){
					outputFilename = distribute(data, batchPrint, batchPrintCategoryTemplate, combinedFile.getPath());					
				}				
				
				// Update Batch Print Document Status
				ModelBean[] batchPrintDocumentArray = (ModelBean[]) array.toArray(new ModelBean[array.size()]);
				for( ModelBean batchPrintDocument : batchPrintDocumentArray ) {
					batchPrintDocument.setValue("StatusCd", "Printed");
					if( !batchPrintCategoryTemplate.valueEquals("ZipFilenameFormat", "") ){
						if( !outputFilename.equals("") ){
	 						if( firstDocument){
	 							batchPrintDocument.setValue("Filename", outputFilename);
	 							documentArray.add(batchPrintDocument);
	 							// if we are combining the documents, there is only one resulting file
	 							firstDocument = false;						
	 						}
						}					
						data.saveModelBean(batchPrintDocument);
					}
				}
				
			} catch( Exception e ) {
				// Update Link Reference Status
				for( ModelBean linkReference : linkReferenceArray ) {
					linkReference.setValue("Status", "Error");
				}

				// Update Batch Print Document Status
				ModelBean[] batchPrintDocumentArray = (ModelBean[]) array.toArray(new ModelBean[array.size()]);
				for( ModelBean batchPrintDocument : batchPrintDocumentArray ) {
					batchPrintDocument.setValue("StatusCd", "Error");
					data.saveModelBean(batchPrintDocument);
				}	

				printError = true;
			}	
		}

		// Check for Print Error 
		if( printError ) {
			ErrorTools.addError("General", "One or more documents failed to print. Review the batch just printed for errors.", ErrorTools.GENERIC_BUSINESS_ERROR, errors);
		}
		
		return documentArray.toArray(new ModelBean[documentArray.size()]);
    }	
 }
