package com.ric.ar;

import java.io.BufferedReader;
import java.io.File;
import java.util.Arrays;
import java.util.Locale;

import net.inov.biz.server.IBIZException;
import net.inov.mda.MDAOption;
import net.inov.mda.MDATable;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.file.FileIO;
import net.inov.tec.xml.XmlDoc;

import com.ibm.math.BigDecimal;
import com.innovextechnology.service.Log;
import com.iscs.ar.HPAccount;
import com.iscs.common.mda.Store;
import com.iscs.common.utilities.HPBookDt;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.io.FileTools;



/*
 * author patriciat
 */

public class BatchReceiptLoad   {
	
//	Set Array Index Position For Each of the Lock box File Fields
	public static int AccountNumber = 0;
	public static int TypeCd = 1;
	public static int Reference = 2;
	public static int ReceiptDt = 3;
	public static int ReceiptAmt = 4;
	public static int CheckAmt = 5;
	public static int Memo = 6;
	public static int PayBy = 7;
	public static int TotalAmount = 1;
	public static int TotalCount = 2;
	
	public BatchReceiptLoad() {
		
	}
	
	/**
	 * Validate each detail record
	 * @param the ModelBean Batch Receipt Detail	 
	 * @throws IBIZException, Exception
	 */
	
	private static void batchValidate(ModelBean batchReceiptDetail)
	throws IBIZException, Exception{			
		String umol;
		
		if (!StringTools.isNumeric(batchReceiptDetail.getValueString("CheckAmt"))) {
			throw new IBIZException ("Check Amount not numeric");
		}
		
		if (!StringTools.isNumeric(batchReceiptDetail.getValueString("ReceiptAmt"))){
			throw new IBIZException ("Receipt Amount not numeric");
		}				
		
		BigDecimal n1 = batchReceiptDetail.getValueDecimal("CheckAmt");
		BigDecimal n2 = batchReceiptDetail.getValueDecimal("ReceiptAmt");
		
		if (n1.compareTo(n2) < 0 ) {
			throw new IBIZException ("Check Amount less than Receipt Amount");
		}						
		
		if ((batchReceiptDetail.getValueString("TypeCd").equalsIgnoreCase("Check")) &&
				(batchReceiptDetail.getValueString("Reference") .equals(""))) {
			throw new IBIZException ("Check number is required if payment is by check");
		}
		
		// Fetch the coderef and validate pay type field
		boolean matchFound = false;
		umol = "AR::account::receipt::directtype";
		MDATable table = (MDATable) Store.getModelObject(umol);
		
		MDAOption[] options = table.getOptions();
		// commented below TypeCd and PayBy validation condition for RG-457 requirement
		
		/*for(int i=0; i<options.length; i++){				
			if (batchReceiptDetail.getValueString("TypeCd").equalsIgnoreCase(options[i].getValue())){
				matchFound = true;
				break;
			}				
		}
		
		if (!matchFound){
			throw new IBIZException ("Invalid Payment Type");
		}*/
		
		// Fetch the coderef and validate pay by field
		/*matchFound = false;
		umol = "AR::account::receipt::payby";
		table = (MDATable) Store.getModelObject(umol);
		
		options = table.getOptions();
		for(int i=0; i<options.length; i++){				
			if (batchReceiptDetail.getValueString("PayBy"). equalsIgnoreCase (options[i].getValue())){
				matchFound = true;
				break;
			}				
		}
		
		if (!matchFound){
			throw new IBIZException ("Invalid Pay By");
		}*/			
		
	}	
	
	/** Processes each detail record
	 * @param the ModelBean ARReceipt
	 * @param data
	 * @param the ModelBean batchReceipt
	 * @param the response
	 * @param the requests
	 * @throws Exception
	 */
	public static void processDetail(ModelBean ARReceipt, JDBCData data, ModelBean batchReceipt, ModelBean rs, ModelBean rq) 	
	throws Exception {		
		
		HPAccount.validateReceipt(data, ARReceipt, rs, rq, true );
		batchReceipt.addValue(ARReceipt);
		batchReceipt.setValue("AppliedDt", HPBookDt.getBookDt(data) );
		HPAccount.updateValidBatch(data, batchReceipt, ARReceipt, batchReceipt.getDate("AppliedDt") );						
		
	}
	
	/**
	 * Validate each detail record in the ModelBean BatchReceiptLoadDetail
	 * @param ModelBean batchReceiptLoad	 
	 * @throws Exception
	 */	
	public static void validateBatchReceiptLoad(ModelBean batchReceiptLoad) 
	throws Exception{
		validCountAndSum(batchReceiptLoad);
		ModelBean[] requests = batchReceiptLoad.getBeans("BatchReceiptLoadDetail");
		for (int i=0; i<requests.length; i++) {
			batchValidate(requests[i]);			
		}
	}
	
	/**
	 * Validate the count and the sum in the header/trailer with the count/sum of all the detail records
	 * @param ModelBean batchReceiptLoad	 
	 * @throws IBIZException,Exception
	 */
	private static void validCountAndSum(ModelBean batchReceiptLoad)
	throws IBIZException, Exception {		
		BigDecimal totalAmt = new BigDecimal("0.00");
		BigDecimal totalReceiptsAmt = new BigDecimal("0.00");			
		int totalRecs = 0;
		int totalReceiptsCount = 0;	 
		
		totalReceiptsAmt = batchReceiptLoad.getValueDecimal("TotalReceiptsAmounts");
		totalReceiptsCount = batchReceiptLoad.getValueInt("TotalReceiptsCount");
		ModelBean[] requests = batchReceiptLoad.getBeans("BatchReceiptLoadDetail");
		for (int i=0; i<requests.length; i++) {
			totalRecs++;				
			BigDecimal amount = requests[i].getValueDecimal("ReceiptAmt");
			totalAmt = totalAmt.add(amount);    	   		
		}
		
		if (totalAmt.compareTo(totalReceiptsAmt) != 0){
			throw new IBIZException ("Amount does not match trailer");    	
		}else if (totalRecs != totalReceiptsCount) {    			
			throw new IBIZException ("Count does not match trailer");
		}					
	}
	
	/**
	 * Import a file to the bean format	 
	 * @param pathFilename String The filename with path
	 * @param filename String The filename
	 * @param data JDBCData the data connection
	 * @return the Batch Receipt Load bean
	 * @throws Exception
	 */
	public static ModelBean importFile(String pathFilename, String filename, JDBCData data)
	throws Exception, IBIZException {
		ModelBean batchReceiptLoad = null;
		if (pathFilename.indexOf(".csv") > 0){
			batchReceiptLoad = convertCSVToXML(pathFilename);
		} else {
			XmlDoc doc = new XmlDoc( new java.io.File(pathFilename) );
			batchReceiptLoad = new ModelBean("BatchReceiptLoad", doc);
		}
		batchReceiptLoad.setValue("Filename", filename);
		// Obtain the file checksum to pass along to be saved later in the BatchReceipt
		File file = new File(pathFilename);
		batchReceiptLoad.setValue("FileChecksum", FileTools.getChecksum(file));
		
		Locale en_US = new Locale("en", "US");
		ModelBean[] batchReceiptLoadDetails = batchReceiptLoad.getBeans("BatchReceiptLoadDetail");
		for( ModelBean batchReceiptLoadDetail:batchReceiptLoadDetails){
			/**SPI Base's scan lines and MICR code contains Account System Ids, therefore BatchReceipt files' 
			 * AccountNumber field can contain an Account System Id or an Account Number(for instances
			 * when the MICR was not used in the BatchReceipt file's entry). 
			 * 
			 * Try to see if the BatchReceipt file's AccountNumber field actually contains Account System Ids, 
			 * if so, update the loaded BatchReceipt bean's AccountNumber field with actual Account Number
			 * 
			 * Consequently, a customer's buildout may not use Account Numbers with no letter prefixes 
			 * if they are to use base's Batch Receipt file loading feature
			 */
			String accountRef = batchReceiptLoadDetail.gets("AccountNumber");
			try {
				ModelBean account[] = BeanTools.selectModelBeansFromLookup(data, "Account", "AccountNumber", accountRef);
//				data.selectModelBean(account, Integer.parseInt(accountRef));	
				if( HPAccount.isBillingEntity(account[0]))
					batchReceiptLoadDetail.setValue("AccountNumber", account[0].gets("AccountDisplayNumber"));
				else
					batchReceiptLoadDetail.setValue("AccountNumber", account[0].gets("AccountNumber"));
			} catch (Exception e){
				Log.error("Can not find account with system id " + accountRef);
				//swallow exception and let the account number as is
			}
			
			/**
			 * Convert ReceiptDt from LockBox file to internal format. Assume all incoming LockBox file have dates in en_US format (for testing); 
			 * Real LockBox files can differ from Bank to Bank, build-outs will normally overwrite this.
			 */
			String receiptDt = batchReceiptLoadDetail.gets("ReceiptDt");
			if( !receiptDt.isEmpty() ){
				try {
					batchReceiptLoadDetail.setValue("ReceiptDt", StringDate.formatEntryDate(receiptDt, false, en_US));
				} catch( IllegalArgumentException e ){
					new IBIZException("Invalid ReceiptDt: "+e.getMessage());
				}
			}
		}
		return batchReceiptLoad;
	}	
	
	
	/**
	 * Convert a CSV file to ModelBean format
	 * @return the current response bean
	 * @param the csv filename	 
	 * @throws Exception
	 */
	
	private static ModelBean convertCSVToXML(String filename)
	throws  Exception {
		
		ModelBean batchReceiptLoad = new ModelBean("BatchReceiptLoad");
		BufferedReader reader = FileIO.openFile(filename);
		String thisLine;
		int lineCount = 0;
		try{
			while( (thisLine = reader.readLine()) != null ) {
				if( thisLine.indexOf("TRAILER") == -1 ) {
					lineCount ++;
					if(lineCount != 1){
						String[] requests = thisLine.split(",",8); 
						ModelBean batchReceiptLoadDetail = new ModelBean("BatchReceiptLoadDetail");          			    				    			
						batchReceiptLoadDetail.setValue("AccountNumber", requests[AccountNumber]);
						batchReceiptLoadDetail.setValue("TypeCd", requests[TypeCd]);
						batchReceiptLoadDetail.setValue("Reference", requests[Reference]);
						batchReceiptLoadDetail.setValue("ReceiptDt", requests[ReceiptDt]);
						batchReceiptLoadDetail.setValue("ReceiptAmt", requests[ReceiptAmt]);
						batchReceiptLoadDetail.setValue("CheckAmt", requests[CheckAmt]);
						batchReceiptLoadDetail.setValue("Memo", requests[Memo]);
						batchReceiptLoadDetail.setValue("PayBy", requests[PayBy]);
						batchReceiptLoad.addValue(batchReceiptLoadDetail);
					}
				} else {
					String[] requests = thisLine.split(","); 
					batchReceiptLoad.setValue ("TotalReceiptsAmounts", requests[TotalAmount]);
					batchReceiptLoad.setValue ("TotalReceiptsCount", requests[TotalCount]);
				}
			}
			reader.close();
			return batchReceiptLoad;
			
		} catch (Exception e) {
			reader.close();
			throw new IBIZException ("Unable to process file. Error on line " + lineCount);	
		}
		
		
	}
	
}




