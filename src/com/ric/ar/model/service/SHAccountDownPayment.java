/*
 * SHAccountDownPayment
 *
 */

package com.ric.ar.model.service;

import java.util.ArrayList;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.IBIZHandler;
import net.inov.biz.server.IBIZHandlerModel;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

import com.iscs.ar.Billing;
import com.iscs.ar.HPAccount;
import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDATemplate;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.table.CodeRefTools;
import com.iscs.uw.policy.Policy;

/** Creates a batch receipt from a down payment collected in underwriting
 *
 * @author  patriciat
 * Overriden to add payment receipt for down payment. 
 */
public class SHAccountDownPayment extends IBIZHandlerModel implements IBIZHandler {

	/** Creates a new instance of SHAccountDownPayment
	 * @throws Exception never
	 */
	public SHAccountDownPayment() throws Exception {
	}

	/** Processes a generic service request.
	 * @return the current response bean
	 * @throws IBIZException when an error requiring user attention occurs
	 * @throws ServiceHandlerException when a critical error occurs
	 */
	public ModelBean process() 
	throws IBIZException, ServiceHandlerException {
		try {
			// Log a greeting
			Log.debug("Processing SHAccountDownPayment...");

			// Get service handler data
			JDBCData data = getHandlerData().getConnection();
			ModelBean rs = getHandlerData().getResponse();        
            ModelBean responseParams = rs.getBean("ResponseParams");
			ModelBean additionalParams = rs.getBean("ResponseParams").getBean("AdditionalParams");
			ModelBean rq = getHandlerData().getRequest();
			AdditionalParams ap = new AdditionalParams(rq);
			String userId = rs.getBean("ResponseParams").getBean("UserInfo").gets("LoginId"); 
			StringDate todayDt = DateTools.getStringDate(rs); 

			ModelBean dtoPolicy = rs.getBean("DTOPolicy");    
			ModelBean account = rs.getBean("Account");			

			String transactionCd = dtoPolicy.getBean("DTOBasicPolicy").gets("TransactionCd"); 
			
			String billingEntityRef = "";
			ModelBean param = additionalParams.findBeanByFieldValue("Param", "Name", "BillingEntityRef");
			if( param!=null)
				billingEntityRef = param.gets("Value");
            
            ModelBean dtoBillingEntity = null;
            String entityTypeCd = "";
            
            if( !billingEntityRef.equals("") ) {
            	dtoBillingEntity = dtoPolicy.findBeanByFieldValue("DTOBillingEntity", "SequenceNumber", billingEntityRef);
                entityTypeCd = dtoBillingEntity.gets("TypeCd");
            }

			if( transactionCd.equals("New Business") && account!=null ) {        	            	
				ModelBean arReceipt = HPAccount.hasDownPayment(data, dtoPolicy, account);
				//post down payment when not on split bill, or to the insured party only when on split bill
				if( arReceipt != null && (entityTypeCd.equals("") || entityTypeCd.equals("Insured"))) {					
					// Build Batch Receipt ID
					String typeCd = arReceipt.gets("TypeCd");
					String batchCd = CodeRefTools.getOptionLabel("AR::account::receipt::batch", typeCd);
					String batchReceiptId = "downpayment-" + batchCd;
					MDATemplate templateMDA = (MDATemplate) Store.getModelObject("batch-receipt-template", "BatchReceipt", batchReceiptId);
					if( templateMDA == null ) {	
						batchReceiptId = "downpayment";
					}
					
					Log.debug("...creating a batch receipt for down payment");
					arReceipt.setValue("ReceiptAmt", StringTools.formatMoney(arReceipt.gets("ReceiptAmt"), false, true, false));
					arReceipt.setValue("SpecificType", ap.gets("TransactionInfo.SpecificType"));
					arReceipt.setValue("CheckNumber", ap.gets("TransactionInfo.AgencySweepCheckNumber"));
					ModelBean batchReceipt = null;
					String paymentTypeCd = ap.gets("TransactionInfo.PaymentTypeCd") ;
					if( arReceipt.valueEquals("MethodCd", "Credit Card") ) {
						batchReceipt = HPAccount.addARReceipt(data, new ModelBean[] {account}, new ModelBean[] {arReceipt}, batchReceiptId, userId, true);
					} else {
						if( paymentTypeCd.equals(Billing.TRUST_ACCOUNT)) {
		            		ModelBean paymentSource = arReceipt.getBean("ElectronicPaymentSource");
		            		// Get ACHAgent
		            		String achAgent = ap.gets("ACHAgent");
		            		paymentSource.setValue("AgentTrustInd", true);
		            		arReceipt.setValue("ACHAgent", achAgent);
						}	
						batchReceipt = HPAccount.addARReceipt(data, new ModelBean[] {account}, new ModelBean[] {arReceipt}, batchReceiptId, userId, false);
					}
					
					param = additionalParams.getBean("Param", "Name", "BatchReceiptRef");
					if( param == null) {
						param = new ModelBean("Param");
						param.setValue("Name","BatchReceiptRef");
						additionalParams.addValue(param);
					}
						
					param.setValue("Value", batchReceipt.getSystemId());
					
					
					//SPECIFIC CODE - general EFT payment receipt
					SHAREFTPaymentReceipt eftGen = new SHAREFTPaymentReceipt();
					if (eftGen.requiresACHReceipt(arReceipt)) {
						
						ModelBean policy = rs.getBean("Policy");
						if (policy == null)
							policy = Policy.getPolicyBySystemId(data, Integer.parseInt(rs.getBean("Account").getBean("ARPolicy").gets("PolicyRef")));
			    		ModelBean insured = policy.getBean("Insured"); 
			    		String commercialName = insured.getBeanByAlias("InsuredName").gets("CommercialName"); 
						ModelBean questionReplies = eftGen.buildQuestionReplies(arReceipt, paymentTypeCd, commercialName); 
						ArrayList<ModelBean> achOutputs = new ArrayList<ModelBean>(); 
			    		achOutputs.add(eftGen.addACHOutput(data, arReceipt, policy, userId, todayDt, questionReplies)); 
			    		eftGen.printACHReceipt(data, responseParams, policy, userId, todayDt, achOutputs); 
					}
					//END SPECIFIC CODE 

					
				}
			}          
			
			// Return the response bean
			return null;

		} catch( Exception e ) {
			throw new ServiceHandlerException(e);
		}
	}
}