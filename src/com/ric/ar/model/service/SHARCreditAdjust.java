/*
 * SHARCreditAdjust
 *
 */

package com.ric.ar.model.service;

import java.util.ArrayList;

import com.iscs.ar.Billing;
import com.iscs.ar.ElectronicPayment;
import com.iscs.ar.HPAccount;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utilities.HPBookDt;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.interfaces.electronicpayment.ach.ACH;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanField;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;
import net.inov.tec.xml.XmlDoc;

/** Processes a credit refund request
 *
 * @author  Todd D. McPartlin
 */
public class SHARCreditAdjust extends InnovationIBIZHandler {

    private static final String TEMPLATE_NAME = "template/payto-party.xml";
    
    
    /** Creates a new instance of SHARCreditAdjust
     * @throws Exception never
     */
    public SHARCreditAdjust() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() 
    throws IBIZException, ServiceHandlerException {
        try {
            // Log a greeting
            Log.debug("...account credit adjustment");
            
            // Get service handler data
            JDBCData data = getHandlerData().getConnection();
            ModelBean rs = getHandlerData().getResponse();
            AdditionalParams ap = new AdditionalParams(rs);
            ModelBean responseParams = rs.getBean("ResponseParams");
            StringDate todayDt = DateTools.getStringDate(responseParams);  
            String todayTm = DateTools.getTime(responseParams);
            
            ModelBean account = rs.getBean("Account");
            
            // Get the AR Tools Class
			Billing ar = HPAccount.getARObject(account);
            
            ////////////////////////////////////////////////////////////////
            // Create a new transaction
            ModelBean arTrans = (ModelBean)getHandlerData().getCmmBean().clone();
            
            // CMM workaround - remove bean generated from ADD operation to avoid duplicates
            account.deleteBeanById( arTrans.getId() );
            
            // Check required values
            if( arTrans.valueEquals("TypeCd","") )
                addErrorMsg("ARTrans.TypeCd", "Transaction type is required", ErrorTools.GENERIC_BUSINESS_ERROR);
                
            if( arTrans.valueEquals("Desc","") )
                addErrorMsg("ARTrans.Desc", "Transaction description is required", ErrorTools.GENERIC_BUSINESS_ERROR);
            
            if( arTrans.valueEquals("RequestedAmt","") )
            	    addErrorMsg("ARTrans.RequestedAmt", "Transaction amount is required", ErrorTools.GENERIC_BUSINESS_ERROR);
            
            //check manual refund required fields
            if( arTrans.valueEquals("TypeCd","ManualRefund") ) {
            	    if( arTrans.valueEquals("ARReceiptReference", "") )
            	        addErrorMsg("ARTrans.ARReceiptReference", "Check number is required for manual refund", ErrorTools.GENERIC_BUSINESS_ERROR);
            	    if( arTrans.valueEquals("ARReceiptDt", "") )
            	        addErrorMsg("ARTrans.ARReceiptDt", "Check date is required for manual refund", ErrorTools.GENERIC_BUSINESS_ERROR);
                 arTrans.setValue("ARReceiptAmt",arTrans.getDecimal("RequestedAmt").abs());
                 arTrans.setValue("CheckAmt", arTrans.getDecimal("ARReceiptAmt"));
                 arTrans.setValue("ARReceiptTypeCd", "Check");
            }else if(arTrans.valueEquals("TypeCd", "AutomatedRefund") ) {
            	arTrans.setValue("ARReceiptAmt",arTrans.getDecimal("RequestedAmt").abs());
                arTrans.setValue("CheckAmt", arTrans.getDecimal("ARReceiptAmt"));
                arTrans.setValue("ARReceiptTypeCd", "Check");
            }else if(arTrans.valueEquals("TypeCd", "ACHRefund") ) {
            	ModelBean destination = arTrans.getBeanByAlias("RefundDestination");	            	
				if (destination.gets("ACHRoutingNumber").length() != 9 ){
					addErrorMsg("RefundDestination.ACHRoutingNumber", "The routing number must be 9 digits", ErrorTools.GENERIC_BUSINESS_ERROR);
				} else {
					// Validate the routing number            	
					if (!ACH.isValidRoutingNumber(data, destination.gets("ACHRoutingNumber"))) {
						addErrorMsg("RefundDestination.ACHRoutingNumber", "The routing number is not a valid routing number.", ErrorTools.FIELD_CONSTRAINT_ERROR);              		
					}
				}
				if (destination.gets("ACHStandardEntryClassCd").equals("")){
					addErrorMsg("RefundDestination.ACHStandardEntryClassCd", "Bank Account Type code is required", ErrorTools.FIELD_CONSTRAINT_ERROR);
				}
				if (destination.gets("ACHBankAccountTypeCd").equals("")){
					addErrorMsg("RefundDestination.ACHBankAccountTypeCd", "Bank Account Type code is required", ErrorTools.FIELD_CONSTRAINT_ERROR);
				}
				if (destination.gets("ACHName").equals("")){
					addErrorMsg("RefundDestination.ACHName", "Name is required", ErrorTools.FIELD_CONSTRAINT_ERROR);
				}
				if (destination.gets("ACHBankAccountNumber").equals("")){
					addErrorMsg("RefundDestination.ACHBankAccountNumber", "Bank Account Number is required", ErrorTools.FIELD_CONSTRAINT_ERROR);
				}		            	            	
                arTrans.setValue("TypeCd", "AutomatedRefund"); // Change the true transaction type code back to AutomatedRefund for ACH
                arTrans.setValue("ARReceiptTypeCd", "ACH");
            }else if(arTrans.valueEquals("TypeCd", "CreditCardRefund") ) {
                String customerProfileId = ap.gets("CustomerProfileId");
                String customerPaymentProfileId = ap.gets("CustomerPaymentProfileId");
                String maskedCreditCardNumber = ap.gets("MaskedCreditCardNumber");
                String paymentServiceAccountId = ap.gets("PaymentServiceAccountId");
            	ModelBean source = arTrans.getBeanByAlias("RefundSource");
            	source.setValue("SourceName", "Refund");
            	source.setValue("MethodCd", "Credit Card");
            	source.setValue("CustomerProfileId", customerProfileId);
            	source.setValue("CustomerPaymentProfileId", customerPaymentProfileId);
            	source.setValue("PaymentServiceAccountId", paymentServiceAccountId);
            	source.setValue("CreditCardNumber", maskedCreditCardNumber);
                arTrans.setValue("TypeCd", "AutomatedRefund"); // Change the true transaction type code back to AutomatedRefund for ACH
            	arTrans.setValue("ARReceiptAmt",arTrans.getDecimal("RequestedAmt").abs());
                arTrans.setValue("CheckAmt", arTrans.getDecimal("ARReceiptAmt"));
                arTrans.setValue("ARReceiptTypeCd", "Credit Card");
                arTrans.setValue("ARReceiptDt", HPBookDt.getBookDt(data));
                ModelBean electronicPayment = new ElectronicPayment().buildRefundReceipt(arTrans, account.getSystemId(), todayDt, todayTm);
                data.saveModelBean(electronicPayment);
                arTrans.setValue("ARReceiptReference", electronicPayment.getSystemId());
                rs.addValue(electronicPayment);
            }else {
                arTrans.setValue("ARReceiptTypeCd","");
            }
            
            String payTo = ap.gets("PayTo");
            String payToDescription = ap.gets("PayToDescription");
            String nameTypeCd = ap.gets("NameTypeCd");
            
            if( nameTypeCd.equals(""))
                addErrorMsg("Pay To Type", "Pay To Type is required", ErrorTools.GENERIC_BUSINESS_ERROR);
            	
            if(  nameTypeCd.equals("Other") && payToDescription.equals(""))
                addErrorMsg("Pay To Type Description", "'Pay To Type Description' is required when 'Pay To Type' is 'Other'", ErrorTools.GENERIC_BUSINESS_ERROR);
            	
            if( payTo.equals(""))
                addErrorMsg("PayTo", "Pay To is required", ErrorTools.GENERIC_BUSINESS_ERROR);
            	

            // Add additional required fields to ARTrans
            String userId = getResponse().getBean("ResponseParams").getBean("UserInfo").gets("LoginId");
            arTrans.setValue("TransactionUserId",userId);
            arTrans.setValue("BookDt",HPBookDt.getBookDt(data));
            arTrans.setValue("SourceCd",Billing.SOURCE_MANUAL);
            
            //Add party info 
//          Update the Pay To and address
            ModelBean party = arTrans.getBean("PartyInfo");
            if (party == null) {
                java.net.URL url = this.getClass().getResource(TEMPLATE_NAME);
                
                if( url == null )
                    throw new Exception("Could not find "+TEMPLATE_NAME);
                
                party = new ModelBean("PartyInfo", new XmlDoc(url));
                arTrans.setValue(party);
            }
            ModelBean nameInfo = party.getBean("NameInfo");
            if(nameTypeCd.equals("Other")) {
				nameInfo.setValue("NameTypeCd", payToDescription);
            } else {
				nameInfo.setValue("NameTypeCd", nameTypeCd);
            }
            nameInfo.setValue("CommercialName", payTo);
            ModelBean addr = party.getBean("Addr");
            //set the refund address
            setAddress(addr, ap, "PayToMailingAddr");
            
            // Debit Account
            try {
            	// Replacing basic ar.arTransaction(arTrans) call for Stat Integration
                HPAccount.createTransaction(data, ar, account, arTrans); 
            }
            catch( Exception e ) {
            	rs.addValue(arTrans);
            	if( arTrans.getBeanByAlias("PayToMailingAddr") == null) {
            		setAddress(addr, ap, "PayToMailingAddr");
            	}
            	ModelBean insuredParty = new ModelBean("PartyInfo");
            	insuredParty.setValue("PartyTypeCd","InsuredParty");
            	ModelBean insuredAddr = insuredParty.getBean("Addr");
            	insuredAddr.setValue("AddrTypeCd", "InsuredMailingAddr");
            	setAddress(insuredAddr, ap, "InsuredMailingAddr");            	
            	arTrans.addValue(insuredParty);           	            				
            	
            	addErrorMsg("", e.getMessage(), ErrorTools.GENERIC_BUSINESS_ERROR);
                throw new IBIZException();
            }
            
            // if manual transfer then create suspense bean
            if( arTrans.valueEquals("TypeCd", "ManualTransfer") ) {
                HPAccount.transferCash( 
                        data, // Handle to connection 
                        ar.getAccount().getSystemId(), // Source bean id 
                        account.gets("AccountDisplayNumber"),  // Source bean reference number
                        arTrans.getDecimal("RequestedAmt"), // Amount to transfer to suspense
                        userId, // Transaction user id
                        arTrans.getDate("BookDt"),
                        arTrans.gets("ARReceiptReference"), // Check number? 
                        arTrans.getDate("ARReceiptDt"),
                        arTrans.getValue("CheckAmt") == null ? null : arTrans.getDecimal("CheckAmt"),
                        arTrans.gets("ARReceiptId"),
                        arTrans.gets("DepositoryLocationCd"));
            }
                       
            // Save the Account
            data.saveModelBean(account);            

            // Return the response bean)
            return null;
        }
        // Re-throw IBIZExceptions that have already been thrown
        catch( IBIZException e ) {
            throw e;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
    
    /** Set the Address
     * 
     * @param addr ModelBean The address bean
     * @param ap AdditionalParams bean
     * @param addressType String The address type we are looking for in the additional params, i.e. InsuredMailingAddr, PayToMailingAddr
     * @throws Exception when an error occurs
     */
    private void setAddress(ModelBean addr, AdditionalParams ap, String addressType) throws Exception {
    	ArrayList<ModelBeanField> fields = addr.getAllFields();
    	for( ModelBeanField field:fields){
    		if( field.getName().equals("id") || field.getName().equals("AddrTypeCd")){
    			continue;
    		}
	    	ModelBean[] params = ap.getBeans("Param");
	    	for( ModelBean param:params){
	    		String name = param.gets("Name");
	    		String compareValue = addressType +"." + field.getName();
	    		if( name.equals(compareValue) ){
	    			addr.setValue(field.getName(), param.gets("Value"));
	    			break;
	    		}
	    	}
    	}    	    	
    }    
}

