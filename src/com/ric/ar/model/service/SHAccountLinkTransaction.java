/*
 * SHAccountLinkTransaction
 *
 */

package com.ric.ar.model.service;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

import com.iscs.ar.Billing;
import com.iscs.ar.HPAccount;

import com.ibm.math.BigDecimal;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;
import com.iscs.uw.policy.PM;
import com.iscs.uw.policy.Policy;

/** Initiates an Account transaction from a policy transaction
 *
 * @author  Douglas A. Moore
 */
public class SHAccountLinkTransaction extends InnovationIBIZHandler {
    
	private static String chargeLateFeeTrans  = "NSF, NSF Fee, Reverse Credit Card Transaction, Reverse - Cash Receipt";
	
	/** Creates a new instance of SHAccountLinkTransaction
     * @throws Exception never
     */
    public SHAccountLinkTransaction() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() 
    throws IBIZException, ServiceHandlerException {
        try {
            // Log a greeting
            Log.debug("...adding account transaction from policy");
                           
            // Get service handler data
            JDBCData data = getHandlerData().getConnection();
            ModelBean rs = getHandlerData().getResponse();
            AdditionalParams ap = new AdditionalParams(getResponse());    
            
            ModelBean policy = rs.getBean("Policy");  
            ModelBean dtoPolicy = rs.getBean("DTOPolicy");
            
            String billingEntityRef = ap.gets("BillingEntityRef");
            
            String billToChangeInd = ap.gets("BillToChangeInd");  
            
            String shortTermInd = ap.gets("ShortTermInd");
            String fullTermExpirationDt = ap.gets("FullTermExpirationDt");
            
            String auditPayPlanCd = ap.gets("AuditPayPlanCd", "");
            
            boolean directPayplanChangeInd = StringTools.isTrue(ap.gets("DirectPayplanChangeInd"));
            
            //make call to create the new transaction
            String userId = getHandlerData().getRequest().getAllBeans("UserInfo")[0].gets("LoginId");
            
            String transactionCd = dtoPolicy.getBean("DTOBasicPolicy").gets("TransactionCd");
            String createAccount = "New Business,Renewal,Renewal Start,Rewrite-Renewal";
            
            String validTransactions = "New Business,Cancellation Notice,Cancellation,Reinstatement,Reinstatement With Lapse,Renewal,Renewal Start,Renewal Activate,Rewrite-Renewal,Re-Underwrite,Unapply,Future Expire,Change Date,Commission Reversal,Billing Entity Change";
            String[] endorsementTransactions = Policy.getEndorsementTypeTransactionList();
            String endorsementTypeCds = "";
            BigDecimal changeWrittenAmt = new BigDecimal("0.00");
                             
            ModelBean dtoBillingEntity = null;
            
            if( !billingEntityRef.equals("") )
            	dtoBillingEntity = dtoPolicy.findBeanByFieldValue("DTOBillingEntity", "SequenceNumber", billingEntityRef);
            
            boolean firstInd = true;
            for( int i=0; i<endorsementTransactions.length; i++) {
            	validTransactions = validTransactions + "," + endorsementTransactions[i];
            	if( firstInd){
            		endorsementTypeCds = endorsementTransactions[i];
            		firstInd = false;
            	} else {
            		endorsementTypeCds = endorsementTypeCds + "," + endorsementTransactions[i];            		
            	}
            }
            
            // For now only allow transactions that have been configured
            if( StringTools.in(transactionCd, validTransactions) ) {
            	
            	StringDate bookDt = HPAccount.getTransactionBookDt(dtoPolicy);
	            // check to see if a new account should be created
            	if( StringTools.in(transactionCd, createAccount) ) {
            		
            		boolean createAccountInd = true;
            		
            		if( dtoBillingEntity !=null) {
	            		// If Billing Entity has no premium or fees, do not create an account
	            		if( !HPAccount.hasPremiumOrFeeAccount(dtoPolicy, dtoBillingEntity) ){
	            			createAccountInd = false;
	            		}
            		}	
            		
            		if( createAccountInd) {
	            	
	            		// Create Account
		    			ModelBean account = HPAccount.createAccount(data, policy, dtoPolicy, dtoBillingEntity);
		    			
		    			// Create ARPolicy
		    			ModelBean arPolicy = HPAccount.createARPolicy(data, dtoPolicy, dtoBillingEntity);
	
		    			// Create Transaction	    			
		    			
		    			String payPlanCd = ""; 	    			
		    			if( dtoBillingEntity != null ) {
		    				payPlanCd = dtoBillingEntity.gets("PayPlanCd");	    				
		    			}	    			
		    			
		    			account = HPAccount.createTransaction(data, dtoPolicy, policy, arPolicy, HPAccount.getPayplan(policy, dtoPolicy, null, payPlanCd, dtoBillingEntity), userId, true, false, bookDt, shortTermInd, fullTermExpirationDt, dtoBillingEntity, false);
		    	    		    			
		    			rs.addValue(account);
            		}
	            }else {
	            	// If an AccountRef has not already been established, do not try to
	                // add a transaction
	                if( dtoPolicy.valueEquals("AccountRef","") ) {
	                    Log.error("...no account: skipping requested tranaction");
	                    return null;
	                }	
	                
	                boolean addTransactionInd = true;
	                ModelBean account = null;	                
	                if( dtoBillingEntity !=null) {
	                	if( dtoBillingEntity.gets("AccountRef", "").equals("")){	
	                		// If Billing Entity has no premium or fees, do not create an account
	                		if( !HPAccount.hasPremiumOrFeeAccount(dtoPolicy, dtoBillingEntity) ){
	                			addTransactionInd = false;
	                		} else { 
		                		// This is a new Billing Entity added after the 1st transaction, determine if this is a BOT or Mid-term Addition. 
		                		//BOT creates an account with a schedule built according to the pay plan setup
		                		//Mid-term creates an account with zero dollar premium, followed by an endorsement that spreads the premium equally among unbilled installments + 1
		                		ModelBean basicPolicy = dtoPolicy.getBean("DTOBasicPolicy");	                		
		                		String policyEffectiveDt = basicPolicy.gets("EffectiveDt");
		                		String transactionEffectiveDt =  basicPolicy.getBean("DTOTransactionHistory","TransactionNumber",basicPolicy.gets("TransactionNumber")).gets("TransactionEffectiveDt");
		                		
		                		boolean botInd = false;
		                		BigDecimal writtenPremiumAmt = new BigDecimal("0.00");
		                		if( policyEffectiveDt.compareTo(transactionEffectiveDt) == 0){
		                			botInd = true;
		                		}
		                		
		                		// Create Account
		    	    			HPAccount.createAccount(data, policy, dtoPolicy, dtoBillingEntity);
		    	    			
		    	    			//if mid-term addition, save the original written amount before setting it to zero
		    	    			if( !botInd){
			    	    			writtenPremiumAmt = dtoPolicy.getBean("DTOBasicPolicy").getDecimal("WrittenPremiumAmt");		    	    			
			    	    			dtoPolicy.getBean("DTOBasicPolicy").setValue("WrittenPremiumAmt", "0.00");
		    	    			} else {
		    	    				addTransactionInd = false;
		    	    			}
		    	    			
		    	    			// Create ARPolicy
		    	    			ModelBean arPolicy = HPAccount.createARPolicy(data, dtoPolicy, dtoBillingEntity);
		    	    			
		    	    			// Clear out the all the premium amounts for mid-term addition so an account can be created with zero dollar premium
		    	    			if( !botInd){
		    	    				arPolicy.setValue("WrittenAmt", "0.00");
		    	    				arPolicy.setValue("FinalPremiumAmt", "0.00");
		    	    				arPolicy.setValue("TotalWrittenAmt", "0.00");
		    	    			}
		    	    			
		    	    			// Create Transaction    			    	    			
		    	    			String payPlanCd = dtoBillingEntity.gets("PayPlanCd");   	
		    	    			ModelBean arPayPlan = null;
		    	     			//restore the original written premium amount
		    	    			if( !botInd){
		    	    				ModelBean[] fees = new ModelBean[0];
		    	    				arPayPlan = HPAccount.getPayplan(policy, dtoPolicy, fees, payPlanCd, dtoBillingEntity);
		    	    	 			account = HPAccount.createTransaction(data, dtoPolicy, policy, arPolicy, arPayPlan, userId, true, bookDt, false, shortTermInd, fullTermExpirationDt, dtoBillingEntity, true, false, false, directPayplanChangeInd);
		    	    				dtoPolicy.getBean("DTOBasicPolicy").setValue("WrittenPremiumAmt", writtenPremiumAmt);
		    	    			} else {
		    	    				arPayPlan = HPAccount.getPayplan(policy, dtoPolicy, null, payPlanCd, dtoBillingEntity);
		    	    	 			account = HPAccount.createTransaction(data, dtoPolicy, policy, arPolicy, arPayPlan, userId, true, bookDt, false, shortTermInd, fullTermExpirationDt, dtoBillingEntity, true, true, false, directPayplanChangeInd);    	    			 
		     	    			}
	                		}
	                	}
	                }
	     
	                // Create Audit account/transaction if asked by Underwriting
	                if( !auditPayPlanCd.equals("") ){
	                   	account = addTransactionToAuditAccount(data, rs, policy, dtoPolicy, dtoBillingEntity, userId, bookDt, shortTermInd, fullTermExpirationDt, directPayplanChangeInd, auditPayPlanCd);
	                   	addTransactionInd = false;	                   	
	                }
	                
	                // process the transaction              	               
	                if( addTransactionInd) {
	                	 // create ARPolicy
			    		ModelBean arPolicy = HPAccount.createARPolicy(data, dtoPolicy, dtoBillingEntity);
			    	
			    		boolean triggerInvoiceInd = false;
			    		
			    		//Force an invoice when Bill To changes or on a ChangeDate transaction
			    		if( billToChangeInd.equals("true") || transactionCd.equals(PM.TX_CHANGE_DATE) )
			    			triggerInvoiceInd = true;
			    		
			    		ModelBean accountMini = null;
			    		boolean chargeFee = false;
			    		if( transactionCd.equals("Reinstatement") || transactionCd.equals("Reinstatement With Lapse") ) {
			    			// See if underwriting wants to charge a fee to reinstate
			    			if( HPAccount.getLateOrReinstatementFeeInd(dtoPolicy) ){
				    			// get the account bean before the cancel rescind/reinstatement transaction
				    			String accountRef = "";
				    			if( dtoBillingEntity !=null){
				    	    		accountRef = dtoBillingEntity.gets("AccountRef");                    		        	        	
				    	    	} else {
				    	    		accountRef = dtoPolicy.gets("AccountRef");        	                       
				    	    	}
				    			accountMini = data.selectMiniBean("AccountMini", accountRef);
				    			chargeFee = true;
					        }	
				    	}	
			    		
			    		account = HPAccount.createTransaction(data, dtoPolicy, policy, arPolicy, userId, triggerInvoiceInd, bookDt, shortTermInd, fullTermExpirationDt, dtoBillingEntity, directPayplanChangeInd);	
			    		
			    		if(transactionCd.equalsIgnoreCase("Cancellation Notice") ) {
			    			ModelBean arTran = HPAccount.getLastARTransaction(account, Billing.TRANSACTIONTYPE_LEGAL);
			    			if(arTran.gets("TransactionReasonCd").equals("NonPay")) { 
				    			ModelBean arPayPlan = account.getBean("ARPayPlan"); 
				    			String lateFee = arPayPlan.findBeanByFieldValue("ARCategory", "CategoryCd", "LateFee").gets("Amount");
				    			arTran.setValue("DueAmt",lateFee);
				    			arTran.setValue("Amount",lateFee);
			    			}
			    		}
			    		
			    		if(account != null) {
		    				ModelBean lastManualReversalARTran = HPAccount.getLastARTransaction(account, "ManualReversal");
			        		if(lastManualReversalARTran != null) {
			        			if( StringTools.in(lastManualReversalARTran.gets("Desc"), chargeLateFeeTrans) ) {
			        				if( transactionCd.equals("Reinstatement") || transactionCd.equals("Reinstatement With Lapse"))
			        					chargeFee = false;		
			        			}
			        			
			        		}	
		    			}	
			    		
			    		// Get the change in written
		                ModelBean lastTran = HPAccount.getLastARTransaction(account);
		                changeWrittenAmt = lastTran.getDecimal("ChangeWrittenAmt");
			    		
			    		if( chargeFee){
			    			if( dtoBillingEntity!=null){
			    				// make sure the reinstatement fee hasn't been already charged for another billing entity
			    				if( HPAccount.hasFeeBeenCharged(data, accountMini, policy.getSystemId(), bookDt) ){
			    					chargeFee = false;
			    				}
			    			}
			    			if( chargeFee){
					    		ModelBean[] errors = HPAccount.chargeLateOrReinstatementFee(data, dtoPolicy,account,accountMini, userId, bookDt);
				    			if( errors.length > 0 ) {
				                 	for( ModelBean error : errors ) {
				                 		addErrorMsg(error.gets("Name"), error.gets("Msg"));
				                 	}	               		
				                 }
				    			
				    			// If Response has Errors, Throw IBIZException
				    			 if( rs.hasErrors("Error") ){
				                    throw new IBIZException();
				    			 }
				    		}
			    		}
			    	}
            	
		    		// The response may contain a copy of the account bean already, so replace it
	                if( account!=null){
			    		if(rs.getBeanById("Account", account.getId()) != null){
			    			rs.deleteBeanById(account.getId());
			    		}
			    		rs.addValue(account);
	                }
	            }    
            }
            
            if( StringTools.in(transactionCd, endorsementTypeCds) && changeWrittenAmt.compareTo(new BigDecimal("0.00"))< 0 ){
            	// if this is a credit endorsement, set the CreditEndorsementParam for later handler use	            	
            	ModelBean param = new ModelBean("Param");
            	param.setValue("Name", "CreditEndorsementParam");
            	param.setValue("Value", "Yes");
            	
            	ModelBean additionalParams = rs.getBean("ResponseParams").getBean("AdditionalParams");
            	if( additionalParams == null){
            		additionalParams = new ModelBean("AdditionalParams");
            		 rs.getBean("ResponseParams").addValue(additionalParams);
            	}
            	additionalParams.addValue(param);
        }
                        
            // Return the response bean
            return null;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }      
    
    /** Add a transaction to an Audit account
     * 
     * @param data JDBCData The data connection
     * @param rs ModelBean The response bean
     * @param policy ModelBean The policy bean
     * @param dtoPolicy ModelBean The dtoPolicy bean
     * @param dtoBillingEntity ModelBean The dtoBillingEntity bean
     * @param userId String the userId
     * @param bookDt StringDate The book date
     * @param shortTermInd String Indicator if this is a short term policy
     * @param fullTermExpirationDt String The full term expiration date
     * @param directPayplanChangeInd Flag to indicate the transaction is a direct payplan change (without an application)
     * @param auditPayPlanCd String The Audit pay plan code
     * @return the Audit account bean
     * @throws Exception when an unexpected error occurs
     */
    public ModelBean addTransactionToAuditAccount(JDBCData data, ModelBean rs, ModelBean policy, ModelBean dtoPolicy, ModelBean dtoBillingEntity, String userId, StringDate bookDt, String shortTermInd, String fullTermExpirationDt, boolean directPayplanChangeInd, String auditPayPlanCd) throws Exception {
    	String payPlanCd = ""; 
    	boolean hasAuditAccount = true;
    	boolean hasAuditPremium = true;
    	// Check if Audit account exists
    	if( dtoBillingEntity !=null) {
    		if( dtoBillingEntity.gets("AuditAccountRef", "").equals("") ){    	
    			hasAuditAccount = false;
    		}
    	} else {
    		if( policy.gets("AuditAccountRef", "").equals("") ){
    			hasAuditAccount = false;
    		}
    	}
    	
    	// Create Audit account if it doesn't exist
    	if( !hasAuditAccount) {
    		hasAuditPremium = HPAccount.hasWrittenPremium(policy.getBean("BasicPolicy"), dtoPolicy, dtoBillingEntity);
    		if( !hasAuditPremium){
    			//return the main account if there is no audit premium
    			ModelBean account = null;
    			if( dtoBillingEntity !=null) {
    				if( !dtoBillingEntity.gets("AccountRef").equals("") ) {
    					account = new ModelBean("Account");
    					data.selectModelBean(account, dtoBillingEntity.gets("AccountRef"));
    				}
    			} else {
    				account = new ModelBean("Account");
    				data.selectModelBean(account, policy.gets("AccountRef"));
    			}
    			return account;
    		}    		    		
    		
    	}
    	// Indicator whether or not to use the first transaction. If the Audit Account doesn't exist, the first transaction should be CreateAccount regardless of what the transaction is
    	// on the main account. If the Audit account already exists, add the underwriting transaction to the Audit account
    	boolean useFirstTransaction = false;
    
    	if( dtoBillingEntity !=null) {
    		if( dtoBillingEntity.gets("AuditAccountRef", "").equals("") ){    			
    			HPAccount.createAccount(data, policy, dtoPolicy, dtoBillingEntity, true);	  
    			useFirstTransaction = true;    			    			
    		}    		
    	} else {
    		if( policy.gets("AuditAccountRef", "").equals("") ){
    			HPAccount.createAccount(data, policy, dtoPolicy, dtoBillingEntity, true);
    			useFirstTransaction = true;
    		}    		
    	}    	
    	
    	payPlanCd = auditPayPlanCd;
		//Create a zero dollar transaction first    	
		BigDecimal writtenPremiumAmt = dtoPolicy.getBean("DTOBasicPolicy").getDecimal("WrittenPremiumAmt");
		if( useFirstTransaction){							    	    			
			dtoPolicy.getBean("DTOBasicPolicy").setValue("WrittenPremiumAmt", "0.00");
			ModelBean arPolicy = HPAccount.createARPolicy(data, dtoPolicy, dtoBillingEntity, true, true);
			arPolicy.setValue("WrittenAmt", "0.00");
			arPolicy.setValue("FinalPremiumAmt", "0.00");
			arPolicy.setValue("TotalWrittenAmt", "0.00");
			
			ModelBean[] fees = new ModelBean[0];
			ModelBean arPayPlan = HPAccount.getPayplan(policy, dtoPolicy, fees, payPlanCd, dtoBillingEntity);
 			HPAccount.createTransaction(data, dtoPolicy, policy, arPolicy, arPayPlan, userId, true, bookDt, false, shortTermInd, fullTermExpirationDt, dtoBillingEntity, true, false, true, directPayplanChangeInd);
			//restore the original written amount
 			dtoPolicy.getBean("DTOBasicPolicy").setValue("WrittenPremiumAmt", writtenPremiumAmt);	
 			useFirstTransaction = false;
		}				
		
    	// Create ARPolicy
		ModelBean arPolicy = HPAccount.createARPolicy(data, dtoPolicy, dtoBillingEntity, true, true);

		// Create Transaction 
		ModelBean[] fees = new ModelBean[0]; 					    					    			  					    			
		ModelBean arPayPlan = HPAccount.getPayplan(policy, dtoPolicy, fees, payPlanCd, dtoBillingEntity);
		ModelBean account = HPAccount.createTransaction(data, dtoPolicy, policy, arPolicy, arPayPlan, userId, true, bookDt, false, shortTermInd, fullTermExpirationDt, dtoBillingEntity, useFirstTransaction, true, true, directPayplanChangeInd);
				
		return account;
    }
   

}
