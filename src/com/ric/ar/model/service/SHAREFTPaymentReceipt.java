package com.ric.ar.model.service;

import java.util.ArrayList;

import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.document.render.DCRenderer;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

public class SHAREFTPaymentReceipt {


    public boolean requiresACHReceipt(ModelBean arReceipt) {
    	try {
    		if(arReceipt.gets("MethodCd").equals("ACH")) {
    				return true;
    		}else if(arReceipt.gets("MethodCd").equals("Credit Card")) {
    			return true;
    		}else if(arReceipt.gets("MethodCd").equals("Check")) {
    			return true;
    		}else if(arReceipt.gets("MethodCd").equals("Cash")) {
    			return true;
    		}
    		
    	} catch (ModelBeanException e) {
    		e.printStackTrace();
    	}
    	
    	return false; 
    }
    
    
    /**
     * Adds the output, but does not send it. 
     * @return
     * @throws ModelBeanException 
     */
    public ModelBean addACHOutput(JDBCData data, ModelBean arReceipt, ModelBean container, String userId, StringDate todayDt, ModelBean questionReplies) throws ModelBeanException {
    	
    	try {
    		ModelBean output = null;
    		output = DCRenderer.addCorrespondence(data, container, "CashWithApplicationReceipt", "UWPolicy", "", userId, todayDt, "") ;
    		ModelBean outputItemEmail = output.findBeanByFieldValue("OutputItem","DeliveryCd","Email");	
    		
    		//change output delivery to local printer if no email is present
    		if( outputItemEmail != null && outputItemEmail.gets("DestinationCd").equals("")){
    			outputItemEmail.setValue("DeliveryCd", "LocalPrinter") ;
        	}

        	output.setValue("AddUser", userId);
        	output.setValue("AddDt", todayDt);
        	output.setValue("AddTm", DateTools.getTime());
        	output.addValue(questionReplies);
        	return output; 
    	} catch (Exception e) {
    		Log.debug("Problem with creating correspondence for ACH payment submission");
    		e.printStackTrace();
    		return null; 
    	}
    }

    
    
    /**
	 * Method for Payment Receipt generation
	 * @param data
	 * @param container
	 * @param templateId
	 * @param packageId
	 * @param submissionNumber
	 * @param userId
	 * @param todayDt
	 * @param logKey
	 * @return
	 * @throws Exception
	 */
    public void printACHReceipt(JDBCData data, ModelBean responseParams, ModelBean container, String userId, StringDate todayDt, ArrayList<ModelBean> outputs) 
    throws Exception { 
    	String sessionId = responseParams.gets("SessionId");
    	if(sessionId == null || sessionId.length() == 0) {
    		ModelBean xfdf = responseParams.getBean("XFDF");
    		if (xfdf != null) {
    			ModelBean param = xfdf.getBean("Param","Name","SessionId");
    			if (param != null) sessionId = param.gets("Value"); 
    		}
    	}
    	
    	for (ModelBean output : outputs) {
        	if( sessionId.length() > 0) { // Have session Id print receipt
        		String securityId = responseParams.getBean("XFDF").findBeanByFieldValue("Param", "Name", "SecurityId").gets("Value");
        		DCRenderer.processOutput(data, userId, securityId, sessionId, "", container, output, todayDt, DateTools.getTime());
        		String rsName = responseParams.getParentBean().getBeanName(); 
        		if (!rsName.equals("UWApplicationUpdateProcessRs")) {
            		responseParams.getParentBean().addValue(output); 
        		}
        	}
    	}
    }
    
    
    public ModelBean buildQuestionReplies(ModelBean arReceipt, String paymentTypeCd, String insuredName) throws Exception {
    	ModelBean replies = new ModelBean("QuestionReplies");
    	String amount = arReceipt.gets("CheckAmt");
		addQuestion(replies, "CheckAmt", amount);
		addQuestion(replies, "InsuredName", insuredName);
		addQuestion(replies, "PaymentTypeCd", paymentTypeCd);
		addQuestion(replies, "ReceiptDt", arReceipt.gets("ReceiptDt"));
		
		ModelBean electronicPaymentSource = arReceipt.getBean("ElectronicPaymentSource"); 
		
		addQuestion(replies, "ACHBankAccountNumber", electronicPaymentSource.gets("ACHBankAccountNumber"));
		addQuestion(replies, "ACHStandardEntryClassCd", electronicPaymentSource.gets("ACHStandardEntryClassCd"));
		addQuestion(replies, "ACHRoutingNumber", electronicPaymentSource.gets("ACHRoutingNumber"));
		addQuestion(replies, "ACHName", electronicPaymentSource.gets("ACHName"));
		addQuestion(replies, "ACHBankName", electronicPaymentSource.gets("ACHBankName"));
		addQuestion(replies, "ACHBankAccountTypeCd", electronicPaymentSource.gets("ACHBankAccountTypeCd"));
		addQuestion(replies, "ACHTransactionId", arReceipt.gets("ReceiptId"));
		addQuestion(replies, "SpecificType", arReceipt.gets("SpecificType"));
		addQuestion(replies, "CheckNumber", arReceipt.gets("CheckNumber"));
		addQuestion(replies, "CreditCardNumber", electronicPaymentSource.gets("CreditCardNumber"));
		addQuestion(replies, "CreditCardTypeCd", electronicPaymentSource.gets("CreditCardTypeCd"));
		addQuestion(replies, "TransactionId", electronicPaymentSource.gets("TransactionId"));
		
		return replies; 
    }
    
    
    private void addQuestion(ModelBean replies, String name, String value) {
    	
    	try {
    		ModelBean reply = new ModelBean("QuestionReply");
    		reply.setValue("Name", name);
    		reply.setValue("Value", value);
    		replies.addValue(reply);
    	} catch (Exception e) {
    		Log.debug("Problem with adding question " + name); 
    		e.printStackTrace();
    	}
	}
    
}
