package com.ric.ar.model.service;

import java.util.ArrayList;
import com.iscs.ar.AR;
import com.iscs.ar.Billing;
import com.iscs.ar.HPAccount;
import com.iscs.common.business.note.Note;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.counter.Counter;
import com.iscs.uw.policy.Policy;
import com.iscs.workflow.Task;
import com.iscs.workflow.render.WorkflowRenderer;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

/** Save a batch receipt initiated from Policy
*
* @author  patriciat
*
* RGUAT-360 Add Description as a field for other payment. 
*/
public class RICSHARMakePaymentSubmit extends InnovationIBIZHandler {
    
    /** Creates a new instance of SHARMakePaymentSubmit
     * @throws Exception never
     */
    public RICSHARMakePaymentSubmit() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a greeting
            Log.debug("Processing SHARMakePaymentSubmit..."); 
            ModelBean rq = getHandlerData().getRequest();
            ModelBean rs = getHandlerData().getResponse();		
            JDBCData data = getHandlerData().getConnection();
            AdditionalParams ap = new AdditionalParams(rq);
            ModelBean responseParams = rs.getBean("ResponseParams");
			ModelBean xfdf = responseParams.getBean("XFDF");
			ModelBean[] paramArray = xfdf.getBeans("Param");
			ArrayList<ModelBean> achOutputs = new ArrayList<ModelBean>(); 
			String userId = rq.getBean("RequestParams").gets("UserId");
			StringDate todayDt = DateTools.getStringDate(responseParams);
			SHAREFTPaymentReceipt eftGen = new SHAREFTPaymentReceipt();
			
			// Get Account 
            ModelBean account = rs.getBean("Account");
            
            // Get Policy 
            ModelBean policy = Policy.getPolicyBySystemId(data, Integer.parseInt(account.getBean("ARPolicy").gets("PolicyRef")));         
            
            // Get Batch Receipt
    		ModelBean batchReceipt = rs.getBean("BatchReceipt");
    		
    		// Get Payment Type Code 
    		String paymentTypeCd = ap.gets("PaymentTypeCd");    		    		
    		
    		// Get New ARReceipts
			ModelBean[] arReceiptArray = batchReceipt.findBeansByFieldValue("ARReceipt", "StatusCd", "New");
			
		    // Obtain the unique identifier for this payment receipt            
			String paymentReceiptNo = "SC" + StringTools.leftPad(Counter.nextNumber(data, "systemreceipt"), "0", 10);    		
  
			for( ModelBean arReceipt : arReceiptArray ) {
				
				// Delete Receipt if Amount is Zero
				String receiptAmt = arReceipt.gets("ReceiptAmt");
                if( receiptAmt.equals("") || StringTools.equal(receiptAmt, "0") ) {
                	boolean deleteBlankReceipt = true;
                	// if installment fee is based on collected and the applied amount is zero, force it through
                	ModelBean mini = data.selectMiniBean("AccountMini", arReceipt.gets("SourceRef"));                	
                	ModelBean arCategory = mini.getBean("ARPayPlan").getBean("ARCategory", "CategoryCd", "InstallmentFee");
                	if( arCategory.getBeans("ARPaymentMethod").length > 0 ){
                		AR ar = HPAccount.getARObject(mini);
                		if( ar.isFeeAccount(mini) )
                			deleteBlankReceipt = false;	
                	} 
                	if( deleteBlankReceipt){
                		batchReceipt.deleteBeanById("ARReceipt", arReceipt.getId());
                		continue;
                	}
                }
                
				// Set Gross/Net Indicator
				String grossNetInd = account.getBean("ARPayPlan").gets("StatementTypeCd");
				arReceipt.setValue("GrossNetInd", grossNetInd);
				
				//RGUAT-360
				//Set Amount Type for AR receipt
				//There are 3 amount types, which are "Minimum", "Full", and "Other"
				arReceipt.setValue("AmountType", ap.gets("AmountType"));
				//Set Other Amount Description for AR receipt 
				arReceipt.setValue("OtherAmountDescription", ap.gets("OtherAmountDescription"));
				
				//RMG-572
				arReceipt.setValue("SpecificType", ap.gets("SpecificType"));
				arReceipt.setValue("CheckNumber", ap.gets("CheckNumber"));
				
				/*
				RMGUAT-624 - Add a new Note should be automatically created in the policy file, 
				to document when "Other Amount" payments are made. 
                Trigger: whenever "Other Amount" Payment type is made.
                
				Other Payment Amount: <OtherPaymentAmount>
				Other Payment Description: <Description>
				*/
				if (!(ap.gets("OtherAmountDescription").isEmpty())) {
					receiptAmt = StringRenderer.formatMoney(arReceipt.gets("ReceiptAmt"), true);
					Note.create("OtherPaymentNote", policy, responseParams, userId,
							    "Other Payment Amount: " + receiptAmt + "\n" + "Other Payment Description: "
								+ ap.gets("OtherAmountDescription"));
					// Update the Container ModelBean
					data.saveModelBean(policy);
				}
				
				//RMG-1217 Add task if a manual payment was made on a Automated Pay Plan
				if( account.gets("PayPlanCd").contains("Automated") ) {
					ModelBean task = WorkflowRenderer.createTask(data, "NonAutomatedPaymentMadeTask", policy, new StringDate());
					
					// Save task
					Task.insertTask(data, task);
				}
				
				// Set Agent Trust Indicator & agent code
				if( paymentTypeCd.equals(Billing.TRUST_ACCOUNT)) {
            		ModelBean paymentSource = arReceipt.getBean("ElectronicPaymentSource");
            		// Get ACHAgent
            		String achAgent = ap.gets("ACHAgent");
            		paymentSource.setValue("AgentTrustInd", true);
            		arReceipt.setValue("ACHAgent", achAgent);
				}				
				
				// Link Receipts Together
				arReceipt.setValue("ARSystemReceiptReference", paymentReceiptNo);
				
				// Update Status
				arReceipt.setValue("StatusCd", "Validated");
				if (eftGen.requiresACHReceipt(arReceipt)) {
		    		ModelBean insured = policy.getBean("Insured"); 
		    		String commercialName = insured.getBeanByAlias("InsuredName").gets("CommercialName"); 
		    		// For both agency sweep and echeck payment MethodCd is ACH, below customization is added to differentiate both payment
		    		ModelBean electronicPaymentSource = arReceipt.getBean("ElectronicPaymentSource"); 
		    		if(!paymentTypeCd.equalsIgnoreCase("ACH Trust Account")  ){
		    			paymentTypeCd=electronicPaymentSource.gets("MethodCd");
		    		}
					ModelBean questionReplies = eftGen.buildQuestionReplies(arReceipt, paymentTypeCd, commercialName); 
		    		achOutputs.add(eftGen.addACHOutput(data, arReceipt, policy, userId, todayDt, questionReplies)); 
				}
			}
			
			// Save BatchReceipt
    		BeanTools.saveBean(batchReceipt, data);
    		
    		eftGen.printACHReceipt(data, responseParams, policy, userId, todayDt, achOutputs);
			// Update Batch Receipt in the Response
			rs.setValue(batchReceipt);
    		
			// Clear the XFDF fields that populate the AppliedAmt on the screen
			for( ModelBean param : paramArray ) {
    			String name = param.gets("Name");
                if( name.endsWith("_AppliedAmt") ) {
                	param.setValue("Value","");
                }
			}
			
            return rs;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }   
}