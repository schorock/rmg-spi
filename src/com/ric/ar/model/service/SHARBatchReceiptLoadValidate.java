package com.ric.ar.model.service;

import java.io.File;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.web.cmm.AdditionalParams;

import com.ric.ar.BatchReceiptLoad;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.error.ErrorTools;

/** Loads the contents of a Lock Box file into a BatchReceiptLoad ModelBean and validates BatchReceiptLoadDetail
 * 
 * @author patriciat
 */

public class SHARBatchReceiptLoadValidate extends InnovationIBIZHandler {
	
	/** Creates a new instance of SHARBatchReceiptLoadValidate
	 * @throws Exception never
	 */
	public SHARBatchReceiptLoadValidate() throws Exception {
	}
	
	/** Processes a generic service request.
	 * @return the current response bean
	 * @throws IBIZException when an error requiring user attention occurs
	 * @throws ServiceHandlerException when a critical error occurs
	 */
	public ModelBean process()
	throws IBIZException, ServiceHandlerException {
		String uploadFilename = null;
		try {
			Log.debug("Processing SHARBatchReceiptLoadValidate...");
			ModelBean rs = this.getHandlerData().getResponse();		
			JDBCData data = this.getHandlerData().getConnection();
			AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest());
			
			// Get Uploaded Lock Box Filename
			uploadFilename = ap.gets("UploadFilename");
			
			// Extract the File Name from the Path
			String fileName = uploadFilename.substring(uploadFilename.lastIndexOf(File.separator) + 1);
			
			// Load Lock Box Data into BatchReceiptLoad
			ModelBean batchReceiptLoad = BatchReceiptLoad.importFile(uploadFilename, fileName, data);   
			rs.addValue(batchReceiptLoad);
		           
			// Validate	BatchReceiptLoad							
			BatchReceiptLoad.validateBatchReceiptLoad(batchReceiptLoad);   

			return rs;
		
		} catch( IBIZException e){	   
			addErrorMsg("Validation", e.getMessage(), ErrorTools.GENERIC_BUSINESS_ERROR);
			Log.error(e);
			throw e;
		} catch( Exception e ) {           				
			throw new ServiceHandlerException(e);
		} finally{
			File file = new File(uploadFilename);
			file.delete();
		} 
	}
}