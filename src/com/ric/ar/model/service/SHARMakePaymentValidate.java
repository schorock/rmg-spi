package com.ric.ar.model.service;

import java.math.BigDecimal;

import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.common.utility.validation.render.ValidationRenderer;
import com.iscs.interfaces.electronicpayment.ach.ACH;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.web.cmm.AdditionalParams;

/** Validate the batch receipt before submitting it to Billing
 *
 * @author  patriciat
 */
public class SHARMakePaymentValidate extends InnovationIBIZHandler  {

	/** Creates a new instance of SHARMakePaymentValidate
	 * @throws Exception never
	 */
	public SHARMakePaymentValidate() throws Exception {
	}

	/** Processes a generic service request.
	 * @return the current response bean
	 * @throws IBIZException when an error requiring user attention occurs
	 * @throws ServiceHandlerException when a critical error occurs
	 */
	public ModelBean process() throws IBIZException, ServiceHandlerException {
		try {
			// Log a greeting
			Log.debug("Processing SHARMakePaymentValidate...");
			ModelBean rq = getHandlerData().getRequest();
			ModelBean rs = getHandlerData().getResponse();		
			JDBCData data = this.getHandlerData().getConnection();
			AdditionalParams ap = new AdditionalParams(rq);

			// Validate required fields
			String paymentTypeCd = ap.gets("PaymentTypeCd");
			if( paymentTypeCd.equals("") )
				addErrorMsg("PaymentTypeCd", "Payment type required", ErrorTools.MISSING_FIELD_ERROR, ErrorTools.SEVERITY_ERROR);

			String amount = ap.gets("ReceiptAmt");
			if( amount.equals("") || StringTools.equal(amount, "0.00") )
				addErrorMsg("ReceiptAmt", "Payment required", ErrorTools.MISSING_FIELD_ERROR, ErrorTools.SEVERITY_ERROR);  
			else if( StringTools.lessThan(amount, "0.00"))
				addErrorMsg("ReceiptAmt", "Payment can not be negative", ErrorTools.FIELD_CONSTRAINT_ERROR, ErrorTools.SEVERITY_ERROR);

			if( paymentTypeCd.equals("ACH") || paymentTypeCd.equals("ACH Trust Account")){
				String routingNumber = ap.gets("ACHRoutingNumber");
				if( routingNumber.equals("")){
					addErrorMsg("ACHRoutingNumber", "Routing number required", ErrorTools.MISSING_FIELD_ERROR, ErrorTools.SEVERITY_ERROR); 
				} else {
					if( !ACH.isValidRoutingNumber(data, routingNumber) )
						addErrorMsg("ACHRoutingNumber", "Invalid routing number", ErrorTools.FIELD_CONSTRAINT_ERROR, ErrorTools.SEVERITY_ERROR);   
				} 
				String achBankAccountNumber = ap.gets("ACHBankAccountNumber");
				if( achBankAccountNumber.equals("")){
					addErrorMsg("ACHBankAccountNumber", "Bank account number required", ErrorTools.MISSING_FIELD_ERROR, ErrorTools.SEVERITY_ERROR); 
				} else if( !ValidationRenderer.isValidValue("BankAccountNumber", achBankAccountNumber)) {
					addErrorMsg("ACHBankAccountNumber", "Improper Bank Account Number format", ErrorTools.FIELD_CONSTRAINT_ERROR, ErrorTools.SEVERITY_ERROR); 
				}
				String achStandardEntryClassCd = ap.gets("ACHStandardEntryClassCd");
				String achBankAccountTypeCd = ap.gets("ACHBankAccountTypeCd");
				if( achBankAccountTypeCd.equals("") )
					addErrorMsg("ACHBankAccountTypeCd", "Bank account type required", ErrorTools.MISSING_FIELD_ERROR, ErrorTools.SEVERITY_ERROR);
				if( achStandardEntryClassCd.equals("") )
					addErrorMsg("ACHStandardEntryClassCd", "ACH Authorization Type required", ErrorTools.MISSING_FIELD_ERROR, ErrorTools.SEVERITY_ERROR);  
				String achName = ap.gets("ACHName");
				if( achName.equals(""))
					addErrorMsg("ACHName", "Name required", ErrorTools.MISSING_FIELD_ERROR, ErrorTools.SEVERITY_ERROR); 
			} else if( paymentTypeCd.equals("Credit Card")) {
				String creditCardNumber = ap.gets("CreditCardNumber");
				if( creditCardNumber.equals("") ) {
					addErrorMsg("CreditCardNumber", "Credit Card Number required", ErrorTools.MISSING_FIELD_ERROR, ErrorTools.SEVERITY_ERROR); 
				}	
			}

			// Sum all Applied Amounts and compare to Payment Amount
			boolean multipleAccountsInd = false;
			BigDecimal totalApplied = new BigDecimal("0.00");
			ModelBean xfdf = rs.getBean("ResponseParams").getBean("XFDF"); 
			ModelBean[] params = xfdf.getBeans("Param");                
			for (int cnt=0; cnt<params.length; cnt++) {
				String name = params[cnt].gets("Name");
				if( name.endsWith("_AppliedAmt") ) {  
					multipleAccountsInd = true;
					if( !params[cnt].gets("Value").equals("") ) {
						totalApplied = totalApplied.add(new BigDecimal(StringTools.formatMoney(params[cnt].gets("Value"), false, true, false)));
					}					
				}
			}

			if( multipleAccountsInd && !amount.equals("") && new BigDecimal(StringTools.formatMoney(amount, false, true, false)).compareTo(totalApplied) != 0 ) {
				addErrorMsg("ReceiptAmt", "Payment amount does not equal total applied amount", ErrorTools.FIELD_CONSTRAINT_ERROR, ErrorTools.SEVERITY_ERROR);
			}

			//RGUAT-360
			//Other Amount Description Validation
			String otherAmountDescription = ap.gets("OtherAmountDescription");
			ModelBean otherAmountDescriptionMetaField = xfdf.getBean("Param", "Name", "METAFIELD__OtherAmountDescription__EXT");
			if( otherAmountDescriptionMetaField != null && otherAmountDescriptionMetaField.gets("Value").contains("Required=True") && otherAmountDescription.isEmpty() ){
				addErrorMsg("OtherAmountDescription", "Other Payment Amount requires an explanation for paying an amount other than the remaining balance or minimum due.", ErrorTools.FIELD_CONSTRAINT_ERROR, ErrorTools.SEVERITY_ERROR);
			}

			if( hasErrors() ) {
				throw new IBIZException();
			}

			return rs;      

		} 
		catch( IBIZException e ) {
			throw e;
		}
		catch( Exception e ) {
			throw new ServiceHandlerException(e);
		}
	}
}