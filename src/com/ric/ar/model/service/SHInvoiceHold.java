package com.ric.ar.model.service;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.IBIZHandler;
import net.inov.biz.server.IBIZHandlerModel;
import net.inov.biz.server.ServiceHandlerData;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;

import com.iscs.ar.HPAccount;
import com.iscs.common.tech.log.Log;
import com.iscs.uw.policy.PM;

/** 
 * Adds a document bean to a container
 */
public class SHInvoiceHold extends IBIZHandlerModel implements IBIZHandler{
    
    /** Creates a new instance of SHInvoiceAdd
     * @throws Exception for errors
     */
    public SHInvoiceHold() throws Exception {
    }
    
    /** Adds a document bean to the container
     * @return ModelBean bean
     * @throws IBIZException for business errors
     * @throws ServiceHandlerException for serious errors
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a greeting
            Log.debug("Processing SHInvoiceHold...");
  
            ServiceHandlerData dataBus = getHandlerData();
            ModelBean rs = dataBus.getResponse();
            JDBCData data = dataBus.getConnection();
            ModelBean ap = rs.getBean("ResponseParams").getBean("AdditionalParams");            
           
            // Get the current Policy/Account beans
            ModelBean policy = rs.getBean("Policy");
            ModelBean account = rs.getBean("Account");
            
            String billingEntityRef = "";
            ModelBean billingEntityRefBean = ap.findBeanByFieldValue("Param", "Name", "BillingEntityRef");
            if (billingEntityRefBean != null) {
            	billingEntityRef = billingEntityRefBean.gets("Value");
            }
            
            if( account == null){
            	if( billingEntityRef.equals("")){
            		account = HPAccount.getAccount(data, policy.gets("AccountRef"));
            	} else {
            		ModelBean billingEntity = policy.findBeanByFieldValue("BillingEntity", "SequenceNumber", billingEntityRef);
            		account = HPAccount.getAccount(data, billingEntity.gets("AccountRef"));
            	}
            }
            
            ModelBean basicPolicy = policy.getBean("BasicPolicy");
            ModelBean lastTransactionHistory = basicPolicy.findBeanByFieldValue("TransactionHistory", "TransactionNumber", basicPolicy.gets("TransactionNumber"));
            if (lastTransactionHistory.gets("TransactionCd").equals(PM.TX_CANCELLATION) && lastTransactionHistory.gets("WaiveCancellationAuditInd").equals("No")) {
            	ModelBean invoice = account.findBeanByFieldValue("ARInvoice", "StatusCd", "Open");
            	if (invoice == null) {
                	return null;
                }
            	invoice.setValue("StatusCd", "Hold");
            } else if (lastTransactionHistory.gets("TransactionCd").equals(PM.TX_CANCELLATION) && lastTransactionHistory.gets("WaiveCancellationAuditInd").equals("Yes")) {
            	ModelBean param = ap.findBeanByFieldValue("Param", "Name", "PrintInvoiceInd");
            	if (param == null) {
            		param = new ModelBean("Param");
        	        param.setValue("Name","PrintInvoiceInd");
        	        ap.addValue(param);
            	}
    	        param.setValue("Value","true");
     	        
        	} else if (lastTransactionHistory.gets("TransactionCd").equals(PM.TX_AUDIT)) {
        		Integer trNumber = Integer.parseInt(basicPolicy.gets("TransactionNumber")) - 1; 
        		ModelBean prevTransactionHistory = basicPolicy.findBeanByFieldValue("TransactionHistory", "TransactionNumber", trNumber + "");
        		if (prevTransactionHistory.gets("TransactionCd").equals(PM.TX_CANCELLATION) && prevTransactionHistory.gets("WaiveCancellationAuditInd").equals("No")) {
        			ModelBean invoice = account.findBeanByFieldValue("ARInvoice", "StatusCd", "Hold");
                	if (invoice == null) {
                    	return null;
                    }
        			invoice.setValue("StatusCd", "Open");
        			
        			ModelBean param = ap.findBeanByFieldValue("Param", "Name", "PrintInvoiceInd");
                	if (param == null) {
                		param = new ModelBean("Param");
            	        param.setValue("Name","PrintInvoiceInd");
            	        ap.addValue(param);
                	}
        	        param.setValue("Value","true");
        		}
        	}
              
            // Add account to response
            rs.setValue(account);
            
            // Save the Account
	        data.saveModelBean(account);
            
            return null;
        }
        catch ( IBIZException ibe ) {
            throw new IBIZException (ibe);
        }
        catch( Exception e ) {
            e.printStackTrace();
            throw new ServiceHandlerException(e);
        }
    }
    

}
