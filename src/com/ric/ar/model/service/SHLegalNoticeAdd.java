/*
 * SHLegalNoticeAdd
 *
 */

package com.ric.ar.model.service;

import java.util.ArrayList;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.IBIZHandler;
import net.inov.biz.server.IBIZHandlerModel;
import net.inov.biz.server.ServiceHandlerData;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.mda.MDA;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.data.JDBCData.QueryResult;

import com.ibm.math.BigDecimal;
import com.iscs.ar.Billing;
import com.iscs.ar.HPAccount;
import com.iscs.ar.HPWFAccount;
import com.iscs.common.tech.log.Log;
import com.iscs.workflow.Task;

/** Updates the billing cycle status of an Account to Legal
 *
 * @author  Todd D. McPartlin
 */
public class SHLegalNoticeAdd extends IBIZHandlerModel implements IBIZHandler {

	/** Creates a new instance of SHLegalNoticeAdd
	 * @throws Exception never
	 */
	public SHLegalNoticeAdd() throws Exception {
	}

	/** Processes a generic service request.
	 * @return the current response bean
	 * @throws IBIZException when an error requiring user attention occurs
	 * @throws ServiceHandlerException when a critical error occurs
	 */
	public ModelBean process() 
	throws IBIZException, ServiceHandlerException {
		try {
			// Log a greeting
			Log.debug("...adding legal notice");

			ServiceHandlerData dataBus = getHandlerData();
			ModelBean rs = dataBus.getResponse();
			ModelBean rq = dataBus.getRequest();
			JDBCData data = dataBus.getConnection();

			String userId = getHandlerData().getUserId();

			ModelBean policy = rs.getBean("Policy");
			ModelBean account = rq.getBean("Account");
			Billing ar = HPAccount.getARObject(account);
			ModelBean payplan = ar.getPayPlan();

			// Get the Task Template from the account
			String taskTemplate = payplan.gets("CancelNoticeTaskIdRef"); 

			// Get the Task Template from the pay plan template
			if( taskTemplate.equals("") ) {
				String payPlanCd = "";
				ModelBean arPayPlan = null;
				try {
					// Attempt to get the pay plan using the Billing pay plan code
					payPlanCd = payplan.gets("PayPlanCd").replaceAll(" ","-");
					arPayPlan = (ModelBean)MDA.getModelObject("AR","payplan","All",payPlanCd);            		
				} catch (Exception e) {
					// Attempt to get the pay plan using the Underwriting pay plan code
					if( !HPAccount.isBillingEntity(account) ) {
						payPlanCd = policy.getBean("BasicPolicy").gets("PayPlanCd").replaceAll(" ", "-");
					}else{
						ModelBean billingEntity = policy.findBeanByFieldValue("BillingEntity", "SequenceNumber", account.gets("BillingEntitySequenceNumber"));
						payPlanCd = billingEntity.gets("PayPlanCd").replaceAll(" ", "-");
					}

					arPayPlan = (ModelBean)MDA.getModelObject("AR","payplan","All",payPlanCd); 
				}
				taskTemplate = arPayPlan.gets("CancelNoticeTaskIdRef");
			}

			if( taskTemplate.equals("") ) {
				throw new Exception("CancelNoticeTaskIdRef field not set in account pay plan");
			}

			// Delete any pending Cancel Notice task
			String taskQuickKey = "Policy" + policy.getSystemId();
			QueryResult[] result = data.doQueryFromLookup("Task", new String[]{"TaskQuickKey", "TemplateId", "Status"},new String[]{"=", "=", "="},new String[]{taskQuickKey, taskTemplate, "Open"}, 1);

			//Create Cancel Notice task only if there isn't one
			if( result.length == 0){

				// Create a Cancel Notice task
				ModelBean task = HPWFAccount.createTask(data, taskTemplate, policy, userId);

				// Obtain the rule bean to set the parameters
				ModelBean rule = HPWFAccount.getTaskRule(task, "policy-task-automated-cancel-notice-service");

				// Set parameters
				if( rule != null ){
					HPWFAccount.setRuleParam(rule, "TransactionLongDescription", "Cancel Notice for Non-Payment");

					// Determine any Receipts AND occurrence of NSF (Total ReceivedAmt excluding any non-NSF reversal > 0 and PaidAmt = 0)					
					ArrayList<ModelBean> accountList = new ArrayList<ModelBean>();
					BigDecimal paidAmt = new BigDecimal("0.00");
					if( HPAccount.isBillingEntity(account) ) {
						String billingEntityNumber = account.gets("BillingEntitySequenceNumber");						
						ArrayList<ModelBean> billingEntityList = ar.getAccountListForBillingEntity(billingEntityNumber);
						for( ModelBean billingEntity:billingEntityList){
							ModelBean arTrans = billingEntity.getBean("ARTrans","TypeCd",Billing.TRANSACTIONTYPE_RECEIPT);
							if( arTrans!=null){
							  accountList.add(billingEntity);
							}
							paidAmt = paidAmt.add(billingEntity.getDecimal("PaidAmt"));
						}
					} else {
					  ModelBean arTrans = account.getBean("ARTrans","TypeCd",Billing.TRANSACTIONTYPE_RECEIPT);
					  if( arTrans!=null){
						  accountList.add(account);
					  }
					  paidAmt = account.getDecimal("PaidAmt");
					}
					boolean paymentReversed = false;
					boolean latestNSF = false;
					ModelBean[] arTransReceipts = HPAccount.getAccountTransactions(accountList, Billing.TRANSACTIONTYPE_RECEIPT);
					ModelBean[] arTransReversals = HPAccount.getAccountTransactions(accountList, Billing.TRANSACTIONTYPE_MANUALREVERSAL);
					ModelBean lastPaymentNSFReversal = null;
					if (accountList.size() > 0) {						
						ArrayList<ModelBean> arTransReversedReceipts = new ArrayList<ModelBean>();
						for( ModelBean arTransReceipt:arTransReceipts) {
							for( ModelBean arTransReversal:arTransReversals) {	
								boolean isPaymentReversed = HPAccount.isPaymentReversed(arTransReversal.gets("ReasonCd"));						
								if (arTransReceipt.gets("Id").equals(arTransReversal.gets("ARTransIdRef")) && isPaymentReversed ){
									arTransReversedReceipts.add(arTransReceipt);
									if( arTransReversal.gets("ReasonCd").equals(Billing.CATEGORYCD_NSFFEE) ){
										lastPaymentNSFReversal = arTransReversal;
									}
								}
							}
						}
						ModelBean[] beans = (ModelBean[]) arTransReversedReceipts.toArray( new ModelBean[arTransReversedReceipts.size()] );
						if (beans.length > 0 && paidAmt.equals(new BigDecimal("0.00"))) {
							paymentReversed = true;
						}

						// If the last Payment NSF Reversal is after all the current Payment transactions, then this should cause an NSF reason
						if (arTransReceipts.length > 0 && beans.length > 0) {							
							ModelBean latestReceipt = arTransReceipts[arTransReceipts.length-1];
							if( lastPaymentNSFReversal !=null){
								if (lastPaymentNSFReversal.gets("Reference").compareTo(latestReceipt.gets("Reference")) > 0) {
									latestNSF = true;
								}
							}
						}
						
					}

					// If No Payment & Not Renewal, Then Cancel Flat (EffectiveDt), otherwise if payments are not reversed then Cancel Pro-Rate (CancelDt) else Cancel Flat (EffectiveDt)
					String transactionCd = policy.getBean("BasicPolicy").gets("TransactionCd");
					String effectiveDt = account.getBean("ARPolicy").gets("EffectiveDt");
					if( accountList.size() == 0 && (!transactionCd.equals("Renewal") && !transactionCd.equals("Rewrite-Renewal")) ) {
						HPWFAccount.setRuleParam(rule, "CancelTypeCd", "Flat");
						HPWFAccount.setRuleParam(rule, "TransactionEffectiveDt", effectiveDt );
						HPWFAccount.setRuleParam(rule, "CancelRequestedByCd", "NonPay");						
						HPWFAccount.setRuleParam(rule, "ReasonCd", "NonPay");
						
					}
					else {
						HPWFAccount.setRuleParam(rule, "CancelTypeCd", "Pro-Rate");
						HPWFAccount.setRuleParam(rule, "TransactionEffectiveDt", account.getDate("CancelDt").toStringDisplay() );
						HPWFAccount.setRuleParam(rule, "CancelRequestedByCd", "NonPay");
						if (latestNSF) {
							HPWFAccount.setRuleParam(rule, "ReasonCd", "NonPayNSF");
						} else {
							HPWFAccount.setRuleParam(rule, "ReasonCd", "NonPay");
						}
					}
				}

				// Save Task
				Task.insertTask(data, task);
			}
			
			if( rs.hasErrors() ){
				throw new IBIZException();
			}

			// Return the response bean
			return null;
		}
		catch( IBIZException e ) {
			throw e;
		}
		catch( Exception e ) {
			throw new ServiceHandlerException(e);
		}
	}

}
