/*
 * SHARBatchReceiptAdd
 *
 */

package com.ric.ar.model.service;

import java.math.BigDecimal;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

import com.iscs.ar.AR;
import com.iscs.ar.HPAccount;
import com.iscs.ar.HPAudit;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utilities.HPCommonUtils;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.counter.Counter;

/** Processes a account receipt
 *
 * @author  Todd D. McPartlin
 */
public class RICSHARBatchReceiptAdd extends InnovationIBIZHandler {
    
    /** Creates a new instance of SHARBatchReceiptAdd
     * @throws Exception never
     */
    public RICSHARBatchReceiptAdd() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() 
    throws IBIZException, ServiceHandlerException {
        try {
            // Log a greeting
            Log.debug("Processing SHARBatchReceiptAdd...");
            
            // Get service handler data
            JDBCData data = getHandlerData().getConnection();
            ModelBean rs = getHandlerData().getResponse();
            AdditionalParams ap = new AdditionalParams(getRequest());
            ModelBean additionalParams = rs.getBean("ResponseParams").getBean("AdditionalParams");  
            ModelBean cmm = rs.getBean("ResponseParams").getBean("CMMParams");            
            ModelBean batchReceipt = rs.getBean("BatchReceipt"); 
            BigDecimal zero = new BigDecimal("0.00");
            
            // Populated receipt beans from the screen fields
            String typeCd = ap.gets("TypeCd");
            String payBy = ap.gets("PayBy");
            String reference = ap.gets("Reference");
            StringDate receiptDt = StringDate.entryDateToStringDate(ap.gets("ReceiptDt"), true);
            String checkAmt = ap.gets("CheckAmt");  
            String payByOtherDescription = ap.gets("PayByOtherDescription");  
            
            // Obtain the unique identifier for this payment receipt            
			String paymentReceiptNo = "SC" + StringTools.leftPad(Counter.nextNumber(data, "systemreceipt"), "0", 10);
            
			int beanCount = batchReceipt.countBeansByFieldValue("ARReceipt", "StatusCd", "Validated");
            ModelBean[] arReceipts = batchReceipt.findBeansByFieldValue("ARReceipt","StatusCd","Pending");
            for( ModelBean arReceipt : arReceipts ) {
            	
            	boolean isBlankReceipt = false;
            	if( arReceipt.gets("AccountNumber").equals("") ) {
            		isBlankReceipt = true;            		
            	} else if( arReceipt.gets("ReceiptAmt").equals("") || zero.compareTo(new BigDecimal(arReceipt.gets("ReceiptAmt"))) == 0) {
            		if( arReceipt.gets("SourceRef").equals("") ) {
            			isBlankReceipt = true;
            		} else {
            			//allow zero dollar receipt for fee account only under new pay plan type, e.g the ones that allow different installment fee to be collected based on payment method
	            		ModelBean mini = data.selectMiniBean("AccountMini", arReceipt.gets("SourceRef"));
	            		if( mini!=null){
		                	ModelBean arCategory = mini.getBean("ARPayPlan").getBean("ARCategory", "CategoryCd", "InstallmentFee");
		                	if( arCategory.getBeans("ARPaymentMethod").length > 0 ){
		                		AR ar = HPAccount.getARObject(mini);
		                		if( !ar.isFeeAccount(mini))
		                			isBlankReceipt = true;	                		
		                	} else {
		                		isBlankReceipt = true;
		                	}
	            		} else {
	            			isBlankReceipt = true;
	            		}
            		}
            	}
            	
            	if( isBlankReceipt) {
            		batchReceipt.deleteBeanById(arReceipt.getId());
            	} else {
            		
            		arReceipt.setValue("TypeCd",typeCd);
	            	arReceipt.setValue("PayBy",payBy);
	            	arReceipt.setValue("Reference",reference);
	            	arReceipt.setValue("ReceiptDt",receiptDt);
	            	arReceipt.setValue("CheckAmt",checkAmt);
	            	arReceipt.setValue("PayByOtherDescription",payByOtherDescription);

	            	HPAccount.validateReceipt(data, arReceipt, rs, getHandlerData().getRequest() );
							            	
	            	arReceipt.setValue("ARSystemReceiptReference", paymentReceiptNo);
	            		            	
	            	beanCount++;
	            	String receiptId = HPCommonUtils.padString( Integer.toString(beanCount) , 4, '0', true );
	        		arReceipt.setValue("ReceiptId", batchReceipt.gets("BatchId")+"-"+receiptId);
	        		arReceipt.setValue("BookDt", batchReceipt.getValue("AppliedDt"));
	        		arReceipt.setValue("StatusCd", "Validated");
            	}
            }			
			
            // Update BatchReceipt After Setting Values
            ModelBean originalBatchReceipt = new ModelBean("BatchReceipt");
            originalBatchReceipt = data.selectModelBean(originalBatchReceipt, cmm.gets("SystemId"));
            originalBatchReceipt.setValue("AppliedDt", batchReceipt.getValue("AppliedDt"));
            originalBatchReceipt.setValue("TotalAmt", batchReceipt.gets("TotalAmt"));
            originalBatchReceipt.setValue("DepositoryLocationCd", batchReceipt.gets("DepositoryLocationCd"));
            
			beanCount = originalBatchReceipt.countBeansByFieldValue("ARReceipt", "StatusCd", "Validated"); // Get the bean count of the newly read batch 
            for( ModelBean receipt : batchReceipt.getBeans("ARReceipt") ) {
            	// Add audit warning message if necessary
            	HPAudit.addAuditWarningMessage(data, batchReceipt, receipt);
				if( originalBatchReceipt.findBeanById("ARReceipt", receipt.getId()) == null ) {
					// Re-order the number as potentially it may have been used in another batch receipt add
					beanCount++;
	            	String receiptId = HPCommonUtils.padString( Integer.toString(beanCount) , 4, '0', true );
	            	receipt.setValue("ReceiptId", batchReceipt.gets("BatchId")+"-"+receiptId);
					originalBatchReceipt.addValue(receipt);
				}
            }
			data.saveModelBean(originalBatchReceipt);
            
			// Set the Page Action to New
			ModelBean action = additionalParams.getBean("Param", "Name", "Action");
			action.setValue("Value", "New");
				
            // Return the response bean
            return null;
        }
        // Re-throw IBIZExceptions that have already been thrown
        catch( IBIZException e ) {
            throw e;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
    
}
