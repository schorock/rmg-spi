/*
 * LateFeeAdd.java
 *
 */

package com.ric.ar.model.service;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

import com.ibm.math.BigDecimal;
import com.iscs.ar.Billing;
import com.iscs.ar.HPAccount;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utilities.HPBookDt;
import com.iscs.common.utility.StringTools;
import com.iscs.uw.common.statementaccount.StatementAccount;

/** Add Late Fee
 *
 * @author  justinr
 */
public class SHLateFeeAdd extends InnovationIBIZHandler {
    // Create an object for logging messages
    
	protected static BigDecimal zeroAmt = new BigDecimal("0.00");
    
    /** Creates a new instance of LateFeeAdd
     * @throws Exception never
     */
    public SHLateFeeAdd() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
    	String userId="";
        try {
            // Log a greeting
            Log.debug("Processing SHLateFeeAdd...");
            // Get service handler data
            JDBCData data = getHandlerData().getConnection();
            ModelBean rs = getHandlerData().getResponse();
            ModelBean rq = getHandlerData().getRequest();
            
			ModelBean responseParams = rs.getBean("ResponseParams");
            
        	userId = rq.getBean("RequestParams").gets("UserId");
			
			// Get the dtoPolicy 
            ModelBean dtoPolicy = rs.getBean("DTOPolicy");
            
            String transactionCd = dtoPolicy.getBean("DTOBasicPolicy").gets("TransactionCd");
            // Add the Late Fee during Cancellation Notice transaction
    		if( transactionCd.equals("Cancellation Notice")) {
    			// Cancellation Notice transaction must not be a reapply
    			ModelBean transactionHistory = dtoPolicy.getBean("DTOBasicPolicy").findBeanByFieldValue("DTOTransactionHistory","TransactionNumber", dtoPolicy.getBean("DTOBasicPolicy").gets("TransactionNumber"));
	            if( transactionHistory != null && transactionHistory.valueEquals("ReplacementOfTransactionNumber","") ) {
	    			ModelBean account = rs.getBean("Account");
	    			if(account==null)
	    			{
	    				return null;
	    			}
		            // Get the AR Tools Class
					Billing ar = HPAccount.getARObject(account);
					ar.setRebuildInvoiceInd("Yes");
					ModelBean payplan = ar.getPayPlan();
					String lateFee = payplan.findBeanByFieldValue("ARCategory", "CategoryCd", "LateFee").gets("Amount");
					
					String reasonCd = "";
					boolean chargeLateFeeInd = false;
		            String categoryCd = Billing.CATEGORYCD_LATEFEE;
		            String type = Billing.TRANSACTIONTYPE_LEGAL;
		            ModelBean arPolicy = account.getBean("ARPolicy");
		            
		            // check last Manual Reversal transaction
	        		ModelBean lastTrans = HPAccount.getLastARTransaction(account, type);
	        		if(lastTrans != null) {
	        			if(lastTrans.valueEquals("TransactionReasonCd", "NonPay") )
	        				chargeLateFeeInd = true;
	        		}
	        		if(chargeLateFeeInd) { 
	        			// Statement Accounts should charge a single Late/Rein fee to the Fee Account
						if( !ar.getAccount().gets("StatementAccountRef").equals("") && !ar.isFeeAccount(ar.getAccount()) ) {
							StringDate bookDt = HPBookDt.getBookDt(data);
							// Get the Fee Account
							StatementAccount sa = new StatementAccount();
							ModelBean statementAccount = sa.getStatementAccount(data, ar.getAccount().gets("StatementAccountRef")); 
							ModelBean feeAccount = new ModelBean("Account");
							data.selectModelBean(feeAccount, statementAccount.gets("StatementFeeAccountRef"));     
							
							// Determine if a 'System' fee has already been charged today
							boolean transOnSameDay = false;
							ModelBean[] arTransArray = feeAccount.getBeans("ARTrans");
							for( ModelBean arTrans : arTransArray ) {
								if( arTrans.gets("TypeCd").equals("ItemAdjustment") && arTrans.gets("SourceCd").equals("System") && arTrans.gets("AdjustmentCategoryCd").equals(categoryCd) ) {
									if( arTrans.getDate("TransactionDt").compareTo(bookDt) == 0 ) {
										transOnSameDay = true;
									}
								}
							}
							
							Billing feeAr = HPAccount.getARObject(feeAccount);
							
							// check to see if there is a category to apply
							ModelBean arItem = feeAccount.getBean("ARItem","CategoryCd",categoryCd);
					        
							// Exclude same day or zero dollar Late/Rein Fee
					        if( arItem != null && !transOnSameDay && feeAr.getCategoryAmount(categoryCd).compareTo(new BigDecimal("0.00")) > 0 ) {
								ModelBean arTransFee = new ModelBean("ARTrans");
								arTransFee.setValue("SourceCd",Billing.SOURCE_SYSTEM);
					            arTransFee.setValue("TypeCd","ItemAdjustment");
					            arTransFee.setValue("AdjustmentCategoryCd", categoryCd);
					            arTransFee.setValue("AdjustmentTypeCd","Add");
					            arTransFee.setValue("RequestedAmt", feeAr.getCategoryAmount(categoryCd));
					            arTransFee.setValue("TransactionUserId", getHandlerData().getUserId() );
					            arTransFee.setValue("Desc","Late Fee");
					            arTransFee.setValue("BookDt",bookDt);
					            arTransFee.setValue("DueAmt",lateFee);
					            arTransFee.setValue("Amount",lateFee);
					            
					            if(arPolicy != null){
					            	 String arPolicyIdRef = arPolicy.gets("id");
							         arTransFee.setValue("ARPolicyIdRef",arPolicyIdRef);
					            }
								
								try {
						        	// Replacing basic ar.arTransaction(arTrans) call for Stat Integration
						            HPAccount.createTransaction(data, feeAr, feeAccount, arTransFee); 
					            }
					            catch( Exception e ) {
					                addErrorMsg("",e.getMessage());
					                throw new IBIZException();
					            }
					            
					            data.saveModelBean(feeAccount);
					        }
						}
			           	//check to see if there is a category to apply
						else{
							ModelBean arItem = ar.getAccount().getBean("ARItem","CategoryCd",categoryCd);
							if(arItem != null ){
								
								// Get the Logged in user id
								String loginId=getHandlerData().getResponse().getBean("ResponseParams").getBean("UserInfo").gets("LoginId");
								
								ModelBean arTransFee = new ModelBean("ARTrans");
								arTransFee.setValue("SourceCd",Billing.SOURCE_SYSTEM);
					            arTransFee.setValue("TypeCd","ItemAdjustment");
					            arTransFee.setValue("AdjustmentCategoryCd", categoryCd);
					            arTransFee.setValue("AdjustmentTypeCd","Add");
					            arTransFee.setValue("RequestedAmt", ar.getCategoryAmount(categoryCd));
					            arTransFee.setValue("transactionUserId", loginId );
					            arTransFee.setValue("Desc","Late Fee");
					            arTransFee.setValue("BookDt", com.iscs.common.utilities.HPBookDt.getBookDt(getConnection()) );
					            arTransFee.setValue("DueAmt",lateFee);
					            arTransFee.setValue("Amount",lateFee);
					            		            
								try {
						        	// Replacing basic ar.arTransaction(arTrans) call for Stat Integration
						            HPAccount.createTransaction(data, ar, account, arTransFee); 
					            } catch( Exception e ) {
					                addErrorMsg("",e.getMessage());
					                throw new IBIZException();
					            }	
					            
					            // Save the Account
					            data.saveModelBean(account);
					        }
						}
					}
	    		}
    		}
    		
            // Return the response bean
            return rs;
        }
        // Re-throw IBIZExceptions that have already been thrown
        catch( IBIZException e ) {
            throw e;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
}