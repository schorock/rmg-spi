package com.ric.ar.render;

import com.iscs.common.render.Renderer;

import net.inov.mda.MDA;
import net.inov.mda.MDAOption;
import net.inov.mda.MDATable;

public class RICARRenderer implements Renderer{

	public RICARRenderer() {}
	
	public String getContextVariableName() {
		return "RICARRenderer";
	}
	
	public String getFirstOptionValue( String coderef ) {
		try{
			MDATable table = (MDATable)MDA.getModelObject(coderef);
            MDAOption[] options = table.getOptions();

            if( options.length > 0 )
				return options[0].getValue();

			return "";
		}catch ( Exception e ){
			return "";
		}
	}
}
