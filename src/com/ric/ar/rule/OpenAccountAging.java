/*
 * OpenAccountAging.java
 *
 */

package com.ric.ar.rule;

import net.inov.mda.MDAOption;
import net.inov.mda.MDATable;
import net.inov.tec.batch.MBPException;
import net.inov.tec.beans.Helper_Beans;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.math.Money;

import com.ibm.math.BigDecimal;
import com.iscs.ar.AccountAsOf;
import com.iscs.ar.rule.OpenAccountAgingRule;
import com.iscs.common.mda.Store;
import com.iscs.common.tech.bean.data.SQLRelationDelegate;
import com.iscs.common.tech.bean.data.SQLRelationHelper;

/** OpenAccountAging implements OpenAccountAgingRule and populates a OpenAccount model bean
 *
 * @author  Benjamin S. Olsen
 */
public class OpenAccountAging implements OpenAccountAgingRule {
	// Create an object for logging messages


	/** Creates a new instance of OpenAccountAging */
	public OpenAccountAging() {

	}    

	/* Retrieves all open accounts for a provider and determines if they are due
	 *  @param data JDBCData connection
	 *  @param account The open account ModelBean
	 *  @param runDt The batch run date
	 *  @return true if no error occurs 
	 */
	public boolean process(JDBCData data, ModelBean provider, ModelBean account, String runMonth, String runYear) throws Exception {
		try {     

			// Set up the sql helpers
			SQLRelationDelegate delegate = SQLRelationHelper.getDelegate(data);
			SQLRelationHelper sqlHelp = new SQLRelationHelper(data, delegate);  

			ModelBean[] trans = account.getBeans("ARTrans"); 

			BigDecimal lastOpenAmt = new BigDecimal("0.00");
			BigDecimal lastNetOpenAmt = new BigDecimal("0.00");				

			// Load Policy Data
			ModelBean policy = new ModelBean("Policy");
			data.selectModelBean(policy,Integer.parseInt(account.getBean("ARPolicy").gets("PolicyRef")));

			for( int transCnt=0; transCnt<trans.length; transCnt++ ) {

				ModelBean openAccount = new ModelBean("OpenAccount");
				openAccount.setValue("RunMonth",runMonth);
				openAccount.setValue("RunYear",runYear);

				// Load Provider Data
				openAccount.setValue("ProviderNumber",provider.gets("ProviderNumber"));
				openAccount.setValue("ProviderRef",provider.getSystemId());
				openAccount.setValue("PolicyNumber",policy.getBean("BasicPolicy").gets("PolicyNumber"));
				openAccount.setValue("EffectiveDt",policy.getBean("BasicPolicy").gets("EffectiveDt"));
				openAccount.setValue("InsuredName", policy.getBean("Insured").getBean("PartyInfo").getBean("NameInfo").gets("CommercialName"));


				boolean billingTransactionInd = false;
				Money commission = new Money("0.00");
				// Load Transaction Data
				if( !trans[transCnt].gets("TransactionCd").equals("") && !trans[transCnt].gets("TypeCd").equals("BalanceForward") && !trans[transCnt].gets("TypeCd").equals("BalanceBack")) {
					openAccount.setValue("TransactionCd",trans[transCnt].gets("TransactionCd"));
					Money premium = new Money(trans[transCnt].gets("ChangeWrittenAmt"));
					commission = new Money(trans[transCnt].gets("ChangeCommissionAmt"));
					openAccount.setValue("GrossPrem",premium);

					if( premium.compareTo(new Money("0.00")) != 0 ) {
						openAccount.setValue("CommissionPct",commission.divide(premium).multiply(new Money("100")));
					}
					else {
						openAccount.setValue("CommissionPct","0.00");
					}
				}
				else {
					billingTransactionInd = true;
					openAccount.setValue("TransactionCd",trans[transCnt].gets("TypeCd"));                	
					openAccount.setValue("CommissionAmt","0.00");
				}

				//Sum up Fees
				Money premium = new Money("0.00");
				Money fees = new Money("0.00");
				Money addlCommAmt = new Money("0.00");
				ModelBean[] activities = Helper_Beans.findBeansHavingFieldWithValue( account, "ARActivity", "ARTransIdRef", trans[transCnt].getId() );
				for( ModelBean activity:activities){
					if( activity.gets("CategoryCd").equals("Premium")){
						premium = premium.add(activity.getDecimal("ActivityAmt"));
						if( trans[transCnt].gets("TypeCd").equals("BalanceForward") && activity.getDecimal("ActivityAmt").compareTo(new BigDecimal("0.00")) > 0 || 
								trans[transCnt].gets("TypeCd").equals("BalanceCleared") && activity.getDecimal("ActivityAmt").compareTo(new BigDecimal("0.00")) < 0 ||
								trans[transCnt].gets("TypeCd").equals("BalanceBack") && activity.getDecimal("ActivityAmt").compareTo(new BigDecimal("0.00")) > 0)
							addlCommAmt = addlCommAmt.add(activity.getDecimal("ActivityCommissionAmt"));
					}else{
						fees = fees.add(activity.getDecimal("ActivityAmt"));
						// add commission for charges only
						if( !trans[transCnt].gets("TransactionCd").equals("") || trans[transCnt].gets("TypeCd").equals("BalanceCleared") )
							addlCommAmt = addlCommAmt.add(activity.getDecimal("ActivityCommissionAmt"));
					}
				}

				if( billingTransactionInd)
					openAccount.setValue("GrossPrem",premium);

				// add any additional commission, ie from fees or balance-carried forward
				commission = commission.add(addlCommAmt);
				openAccount.setValue("CommissionAmt",commission);

				openAccount.setValue("FeeAmt",fees);

				openAccount.setValue("TransactionEffectiveDt",trans[transCnt].gets("TransactionDt"));
				openAccount.setValue("TransactionEntryDt",trans[transCnt].gets("BookDt"));

				// Load Account Data
				openAccount.setValue("BillMethod",account.getBean("ARPayPlan").gets("BillMethod")); 

				String month = "";
				String coderef = "CO::common::months::numeric-value";        		
				MDATable table = (MDATable) Store.getModelObject(coderef);
				MDAOption[] options = table.getOptions();   
				for( int i=0; i<options.length; i++ ) {
					if( options[i].getLabel().equals(runMonth) ) {
						month = options[i].getValue();
						break;
					}
				} 
				String currDateString = runYear + month + "15";
				StringDate currDate = new StringDate(currDateString);
				StringDate prevDate = StringDate.advanceMonth(currDate,-1);           
				
				String statementTypeCd = "";
				ModelBean[] invoice = null;
				BigDecimal openAmt = new BigDecimal("0.00");
				BigDecimal openNetAmt = new BigDecimal("0.00");
				
				if( transCnt < trans.length - 1) {
					// rollback account so it would age properly
					AccountAsOf asOf = new AccountAsOf(account);
					ModelBean accountAsOf = asOf.getAccountAsOfTransaction(Integer.parseInt(trans[transCnt].gets("Reference")));	
	
					openAmt = accountAsOf.getDecimal("OpenAmt").subtract(lastOpenAmt);
					openNetAmt = accountAsOf.getDecimal("NetOpenAmt").subtract(lastNetOpenAmt);				
	
					statementTypeCd = accountAsOf.getBean("ARPayPlan").gets("StatementTypeCd");
					invoice = accountAsOf.getBeansSorted("ARInvoice","Reference","Descending");
					
					lastOpenAmt = accountAsOf.getDecimal("OpenAmt");
					lastNetOpenAmt = accountAsOf.getDecimal("NetOpenAmt");
					
				} else {
					openAmt = account.getDecimal("OpenAmt").subtract(lastOpenAmt);
					openNetAmt = account.getDecimal("NetOpenAmt").subtract(lastNetOpenAmt);
					statementTypeCd = account.getBean("ARPayPlan").gets("StatementTypeCd");
					invoice = account.getBeansSorted("ARInvoice","Reference","Descending");
				}
				
				if( invoice.length > 0) {
					StringDate dueDt = invoice[0].getDate("DueDt");
					if( dueDt.compareTo(prevDate) < 0 ){
						if( statementTypeCd.equals("Net")){
							openAccount.setValue("DueAmt1",openNetAmt);
						}else{
							openAccount.setValue("DueAmt1",openAmt);
						}
					}else if( dueDt.compareTo(prevDate) >= 0 && dueDt.compareTo(currDate) < 0 ) {
						if( statementTypeCd.equals("Net")) {
							openAccount.setValue("DueAmt2",openNetAmt);
						}else{
							openAccount.setValue("DueAmt2", openAmt);	
						}
					}else if( dueDt.compareTo(currDate) >= 0 ){
						if( statementTypeCd.equals("Net")){
							openAccount.setValue("DueAmt3",openNetAmt);
						}else{
							openAccount.setValue("DueAmt3",openAmt);
						}
					}								
				} 				
				String[] jdbckey = new String[] {"PolicyNumber","RunMonth","RunYear"};
				String[] jdbcop = new String[] {"=","=", "="};
				String[] jdbcval = new String[] {policy.getBean("BasicPolicy").gets("PolicyNumber"), runMonth,runYear};


				ModelBean[] previousOpenAccounts = sqlHelp.selectModelBean("OpenAccount", jdbckey, jdbcop, jdbcval, 0);

				for( ModelBean prevOpenAccount:previousOpenAccounts) {
					boolean updateInd = false;
					if( !openAccount.gets("DueAmt1").equals("")){
						if( !prevOpenAccount.gets("DueAmt1").equals("")){
							openAccount.setValue("DueAmt1", openAccount.getDecimal("DueAmt1").add(prevOpenAccount.getDecimal("DueAmt1")));
							prevOpenAccount.setValue("DueAmt1","0.00");
							updateInd = true;
						}
					}else if( !openAccount.gets("DueAmt2").equals("")){
						if( !prevOpenAccount.gets("DueAmt2").equals("")) {
							openAccount.setValue("DueAmt2", openAccount.getDecimal("DueAmt2").add(prevOpenAccount.getDecimal("DueAmt2")));
							prevOpenAccount.setValue("DueAmt2","0.00");
							updateInd = true;
						}
					}else if( !openAccount.gets("DueAmt3").equals("")) {
						if( !prevOpenAccount.gets("DueAmt3").equals("")){
							openAccount.setValue("DueAmt3", openAccount.getDecimal("DueAmt3").add(prevOpenAccount.getDecimal("DueAmt3")));
							prevOpenAccount.setValue("DueAmt3","0.00");
							updateInd = true;
						}
					}					
					if( updateInd)
						sqlHelp.saveBean(prevOpenAccount);
				}											
				
				// Move any credit to current due
				if( openAccount.getDecimal("DueAmt1").compareTo(new BigDecimal("0.00")) < 0 ) {
					openAccount.setValue("DueAmt2",openAccount.gets("DueAmt1"));
					openAccount.setValue("DueAmt1","0.00");
				} else if (openAccount.getDecimal("DueAmt3").compareTo(new BigDecimal("0.00")) < 0 ) {
					openAccount.setValue("DueAmt2",openAccount.gets("DueAmt3"));
					openAccount.setValue("DueAmt3","0.00");
				}

				// Save the new OpenAccount record
				sqlHelp.saveBean(openAccount);
				
			}

			return true;			

		} catch (Exception e) {
			throw new MBPException(e);
		} 
	}

}
