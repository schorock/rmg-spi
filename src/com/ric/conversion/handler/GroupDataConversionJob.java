/**
 * 
 */
package com.ric.conversion.handler;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.IBIZServer;
import net.inov.biz.server.OpenFrameworkException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.batch.BatchJob;
import net.inov.tec.batch.BatchJobManager;
import net.inov.tec.batch.BatchJobTable;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.file.FileIO;
import net.inov.tec.obj.ClassHelper;
import net.inov.tec.web.cmm.AdditionalParams;

import org.apache.commons.io.FileUtils;

import com.iscs.common.admin.operation.Operation;
import com.iscs.common.render.DynamicString;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.tech.service.ModelBeanProcessorBaseSPI;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.InnovationUtils;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.common.utility.io.FileTools;

/**
 * @author ashutosh.shukla
 *
 */
public class GroupDataConversionJob extends InnovationIBIZHandler {

	private String loginId = null;
	private String templateName = null;
	private static final String CUSTOMER_HEADER = "\nAUDITSUMMARY\nGROUP\tFILE\t#\tDTONAME\tPASS/FAIL\tID\tProvider Type\tProvider Number\n";
	private static final String UPLOAD_DIR = FileTools.cleanPath(net.inov.tec.prefs.PrefsDirectory.getLocation()) + "upload";
	private static final String RESULTS_DIR = FileTools.cleanPath(net.inov.tec.prefs.PrefsDirectory.getLocation()) + "data" + File.separator + "conversion";
			
	ModelBean rq = null;
	ModelBean rs = null;
	ModelBean responseParams = null;
	AdditionalParams ap = null;
	String fileName = null;           	
	String auditText = null;
	String pass = null;
	String extractDate = null;
	String leaveFailedApps = null;
	String maxThreadNum = null;

	/** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */	
	public ModelBean process() throws IBIZException, ServiceHandlerException {
		FileOutputStream fos = null;
		FileOutputStream convReportFOS = null;
		File tempConversionDirectory = null;
		File tempFile = null;
        try {
        	
            Log.debug("Processing GroupDataConversionJob...");
            rq = this.getHandlerData().getRequest();
            rs = getHandlerData().getResponse();
            responseParams = rs.getBean("ResponseParams");
            
            ap = new AdditionalParams(this.getHandlerData().getRequest() );
            templateName = ap.gets("TableName");
           	String fileNameOrDirectory = ap.gets("FileNameOrDirectory");           	
           	extractDate = ap.gets("RunDt");
           	String pollTime = ap.findBeanByFieldValue("Param", "Name", "PollTime") != null ? ap.findBeanByFieldValue("Param", "Name", "PollTime").gets("Value") : null;
           	leaveFailedApps = ap.gets("LeaveFailedApps");
           	maxThreadNum = ap.findBeanByFieldValue("Param", "Name", "MaxThread").gets("value");
           	if(pollTime == null || pollTime.equals("")){
           		// if poll time is not configured in the job, use 30 secs as a default value for polling the initiated conversion jobs
           		pollTime = "30000"; 
           	}           	
           	long pollTimeValue = Long.parseLong(pollTime);
           	           	
           	// Get the user from the request
           	loginId =  getRequest().getBean("RequestParams").gets("UserId");
        	
        	// Ensure the Errors bean exists in the response
        	ModelBean responseParams = getResponse().getBean("ResponseParams");
        	ModelBean errorsBean = responseParams.getBean("Errors");
        	if(errorsBean == null){
        		errorsBean = new ModelBean("Errors");
        		responseParams.addValue(errorsBean);
        	}
    		 		
    		    		
    		//Below check is done to use same file name. In Linux server, the PreferenceDirectory is set and same is prefixed when file has been loaded. So we don't need
    		//to build for any Test Server or PROD. However, for windows(Local Developer) need to build. The Below check will be bypassed in Windows based server(for developer) and 
    		//will be executed for Test/PROD servers.
    		/*if(fileNameOrDirectory.contains(UPLOAD_DIR)){
    		   uploadedFileName =  fileNameOrDirectory;
    		}*/ 
    		
    		// Check if the file uploaded is of type .zip extn    		
    		if(!fileNameOrDirectory.toLowerCase().endsWith(".zip") ) {
           		throw new IBIZException("Error :: Only Zip files containing conversion xmls can be uploaded.");
           	}
    		
    		// Copy the uploaded File to a temporary file so that there is no conflict when any other user uploads the file with same name  
    		String tempFileName = UPLOAD_DIR + File.separator + "Conversion" + loginId.replaceAll("[^\\w\\s]","") + System.currentTimeMillis() + ".zip";
           	FileIO.copyFile(fileNameOrDirectory, tempFileName);
           	
           	// Delete the actual uploaded file
           	FileUtils.forceDelete(new File(fileNameOrDirectory));
           	
           	tempFile = new File(tempFileName);
           	
           	// Create a temp directory to unzip the files.
			String tempConversionDirectoryName = UPLOAD_DIR + File.separator + "Conversion_Inbound_" + getServerName().replaceAll("[^\\w\\s]","") + System.currentTimeMillis();
			tempConversionDirectory = new File(tempConversionDirectoryName);
			if(!tempConversionDirectory.exists()){
				tempConversionDirectory.mkdir();
			}			
			
			// Create a directory to store the results
			String reportDirectory = tempConversionDirectoryName + File.separator + "Results";
			new File(reportDirectory).mkdir();
			
			// Unzip the file to the new directory
			unzip(tempFileName, tempConversionDirectoryName, 1024);
			if(hasErrors()){
				throw new IBIZException();
			}
			
           	// Initiate Conversion jobs to Load the file(s)
           	ArrayList<ModelBean> jbList = null;
           	if(tempConversionDirectory.isDirectory()){
           		jbList = loadDirectoryOfFiles(tempConversionDirectory, maxThreadNum);
           	} else{
           		throw new IBIZException("Invalid conversion files directory: '" + fileNameOrDirectory + "'");
           	}
           	
           	// monitor the initiated data conversion jobs
           	ModelBean[] completedJobs = null;
           	ModelBean completedBatchJob = null;
           	if(jbList != null && !jbList.isEmpty()){
           		completedJobs = new ModelBean[jbList.size()];
           		int counter = 0;
           		while(jbList.size() > 0){
	           		for(int i=0; i < jbList.size(); i++) {
	           			completedBatchJob = jbList.get(i);
	           			ModelBean processBean = new ModelBean ("Job");
	           			// refresh every job to obtain the updated status and check if it is still running
	           			this.getHandlerData().getConnection().selectModelBean(processBean, Integer.parseInt(completedBatchJob.getSystemId()));	           			
	           			if(!processBean.gets("Status").equals("Running") && !processBean.gets("Status").equals("Started")){
	           				completedJobs[counter] = processBean;
	           				counter ++;
	           				jbList.remove(i);
	           			}
	           		}
	           		// this is to make sure the refresh of job happens per defined poll time. Reducing this might impact the performance.
	           		Thread.sleep(pollTimeValue);
           		}
           	}
           	
           	// Write the job audit data to a spreadsheet
           	if(completedJobs != null){           		
           		StringBuffer sb = null;
           		StringBuffer conversionReport = new StringBuffer();
           		conversionReport.append("Category\tServer Name\tFile Name(s)\tTxn Total\tTxn Fail\tTxn Pass\tPolicy Count\tPolicy Terms\tStart Time\tEnd Time\tDuration\tStatus\t% Success\n");           		
           		for(int j = 0; j < completedJobs.length; j++){
           			sb = new StringBuffer();
           			try{ 
	           			processJobLogFile(completedJobs[j], sb, conversionReport, j);
	           			fos = new FileOutputStream(reportDirectory + "/"+fileName+".xls");
	           			fos.write(sb.toString().getBytes());	           				           			
           			} catch(Exception ex){
           				// If any exception occurs,ignore it and proceed with the next job
           				Log.error(ex);
           				super.addErrorMsg("Processing Error", reportDirectory + "/"+fileName+".xls was not created due to error:" +ex.getMessage(), "Error");
           			} finally {
           				try{
           	        		if(fos != null){
           	        			fos.close();
           	        		}
           	        	}catch(Exception ex){
           	        		// ignore this exception
           	        		Log.error(ex);
           	        	}
           			}
           			conversionReport.append("\n");
           		}
           		convReportFOS = new FileOutputStream(reportDirectory + "/"+templateName+"ConversionReport_"+System.currentTimeMillis()+".xls");
           		convReportFOS.write(conversionReport.toString().getBytes());           		
           	}
           	if(completedJobs != null){
           		// Create a zip file of the results file along with all the error files and conversion xml files
           		String reportResultFileName = RESULTS_DIR + File.separator + "Results-"+System.currentTimeMillis()+".zip";
           		FileIO.zipDirectory(tempConversionDirectoryName, reportResultFileName, tempConversionDirectoryName, 1024);
           		// Encrypt the file name 
           		String payload = "Filename=" + reportResultFileName;
           		payload = InnovationUtils.Crypto.encrypt((payload));
           		payload = URLEncoder.encode(payload, "UTF-8");
           		// Create the URL for fetching the results file using the encrypted file name
           		String baseURL = DynamicString.render(new ModelBean("Company"), com.iscs.common.utility.InnovationUtils.getEnvironmentVariable("ServerURL", "Base", ""));
           		String url = baseURL + "?rq=STConversionResultFileToken&Payload=" + payload;
           		// Add a message the conversion job to display the results file as a link
           		String resultFilePath = "<h4><a class='actionLink' target=\"_blank\" href='"+url+"' onClick='javascript:hideLink();'>Click here</a> to download the results zip file.</h4><font color=\"red\">(This link will not work after 24 hrs of completion of the job)</font>";
           		super.addErrorMsg("Processing Completed", resultFilePath, "Info", "Info");
           	}
        } catch( IBIZException ibizEx ) {
        	Log.error(ibizEx);
            throw ibizEx;
        } catch( Exception ex ) {
        	Log.error(ex);
            throw new ServiceHandlerException(ex);
        } finally{
        	try{
        		if(convReportFOS != null){
        			convReportFOS.close();
        		}
        		// Delete the folders and files created on upload directory
        		if(tempConversionDirectory != null){
        			FileUtils.deleteDirectory(tempConversionDirectory);
        		}
        		if(tempFile != null){
        			FileUtils.forceDelete(tempFile);
        		}
        	}catch(Exception ex){
        		// Log & ignore this exception
        		Log.error(ex);
        	}
        }
        return rs;
    }
	
	/** This method processes the job and writes the details of the job into a string buffer. 
	 * @param job
	 * @param sb 
	 * @param conversionReport
	 * @param counter
	 * @throws Exception
	 */
	
	private void processJobLogFile(ModelBean job, StringBuffer sb, StringBuffer conversionReport, int counter) throws Exception {
		// The below logic reverse engineers the HTML output of the job audit data to fetch the results and export to a spreadsheet
		conversionReport.append(templateName + "\t");
		conversionReport.append(getServerName() + "\t");		
		fileName = null;           	
		auditText = null;
		sb.append("JOB ACTION DETAIL\nACTION SUMMARY\nNAME\tData Conversion Load\nDESCRIPTION\tData Conversion Load\nSTART\t");
		if(job != null){
			sb.append(job.gets("StartDt") + " ");
			sb.append(job.gets("StartTm") + "\t");
			sb.append("END\t");
			sb.append(job.gets("EndDt") + " ");
			sb.append(job.gets("EndTm") + "\t");
			sb.append("STATUS\t");
			sb.append(job.gets("Status") + "\n");
			sb.append("ERRORS\n");
			sb.append("SEVERITY\t");
			sb.append("ERROR NAME\t");
			sb.append("ERROR DESCRIPTION\n");
			
			ModelBean jobAction = job.getBeans("JobActions")[0].getBean("JobAction");
			ModelBean[] errors = jobAction.getAllBeans("Error");
			for(ModelBean error : errors){			
				if(error.gets("Msg").contains("Processing file:")){
					String msg = error.gets("Msg");	           					
					int slashIndex = msg.lastIndexOf("/");	           				
					if(slashIndex == -1){
						slashIndex = msg.lastIndexOf("\\"); // this is for processing in windows OS
					}
					String k = (msg.contains(".xml")) ? (k=".xml") : (msg.contains(".XML")) ? (k=".XML") : "";
					if(k != ""){
						fileName = msg.substring(slashIndex+1, msg.indexOf(k));
					} else {
						fileName = msg.substring(slashIndex+1);
					}
					conversionReport.append(error.gets("Msg").substring(slashIndex+1)+"\t");
					conversionReport.append("=E"+(counter+2)+"+F"+(counter+2)+"\t");
				}
				if(error.gets("Name").equalsIgnoreCase("AuditSummary")){
					auditText = processAuditText(error.gets("Msg"), fileName);
					if(auditText != null) {
						sb.append(auditText);
					} else {
						sb.append("NO AUDIT SUMMARY");
					}
					break;
				}
				sb.append(error.gets("Severity") + "\t");
				sb.append(error.gets("Name") + "\t");
				sb.append(error.gets("Msg") + "\n");
			}
		}
		
		int passIndex = sb.toString().indexOf("Pass/Fail:");
		pass = null;
		if(passIndex != -1){
			pass = sb.toString().substring( passIndex + 10);
			conversionReport.append(pass.substring(pass.indexOf("/") + 1, pass.indexOf("\n")) + "\t");
			conversionReport.append(pass.substring(0,pass.indexOf("/")) + "\t");
		} else {
			// assume the file has no policies to process
			conversionReport.append("0\t");
			conversionReport.append("0\t");
		}
		conversionReport.append("0\t");
		conversionReport.append("0\t");
		conversionReport.append(job.gets("StartDt") + " ");
		conversionReport.append(job.gets("StartTm") + "\t");
		conversionReport.append(job.gets("EndDt") + " ");
		conversionReport.append(job.gets("EndTm") + "\t");
		conversionReport.append("=J"+(counter+2)+"-I"+(counter+2)+"\t");           			
		try{
			if(pass != null && Integer.parseInt(pass.substring(pass.indexOf("/") + 1, pass.indexOf("\n")))> 0){
				conversionReport.append("Completed W/Errors\t");
			} else {
				conversionReport.append("Completed\t");
			}
		} catch(Exception ex){
			conversionReport.append("Completed W/Errors\t");
		}
		conversionReport.append("=(F"+(counter+2)+"/D"+(counter+2) + ")*100");
	}
	
	/** This method processes the audit text and writes it into a string buffer
	 * @param message
	 * @return auditText
	 */
	
	private String processAuditText(String message, String fileName){
		StringBuffer sb = new StringBuffer();
		String template = null;
		if(templateName.equalsIgnoreCase("Customer")){
			template = CUSTOMER_HEADER;
		} 
		
		try {
			int i = message.indexOf(fileName);
			message = message.substring(i);
			i = message.indexOf("</table>");
			String msg2 = message.substring(i + 8);
			message = message.substring(0,i);
			String[] rows = message.split("</tr>");
			for(String row : rows){
				row = row.replace("<tr>", "");
				String[] columns = row.split("</td>"); 
				for(String column : columns){
					column = column.replace("<td>", "");
					if(column.contains(">")){
						i = column.indexOf(">");
						column = column.substring(i + 1);
					}
					sb.append(column + "\t");
				}
				sb.append("\n");
			}
			i = msg2.indexOf("Pass/Fail:");
			int k = msg2.indexOf("</td>");
			sb.append(msg2.substring(i, k) + "\n");
			i = msg2.indexOf("Fail/Rolled-back/Skipped:");
			msg2 = msg2.substring(i);
			k = msg2.indexOf("</td>");
			sb.append(msg2.substring(0, k));
		} catch(Exception ex){
			Log.error(ex);
		}
		if(sb.length() > 0){
			return template + sb.toString();
		} else {
			return null;
		}		
	}
	
	/** This method fetches the server name the job is runnning
	 * @return hostname
	 */
	
	private String getServerName(){
		String hostname = "UNKNOWN";
		try {
			InetAddress addr = InetAddress.getLocalHost();
			hostname = addr.getHostName();
		} catch(Exception ex){
			Log.error(ex);
		}
		return hostname;
	}
    
    
    /** Loads a directory of files
     * 
     * @param f File
     * @param maxThreadNum String maximum thread number
     * @throws Exception for errors
     */
    private ArrayList<ModelBean> loadDirectoryOfFiles(File f, String maxThreadNum) throws Exception{
    	
    	String[] children = f.list();
    	ArrayList<ModelBean> jobList = new ArrayList<ModelBean>(); 
    	if (children == null) {
    		throw new Exception("Either the directory '" + f.getAbsolutePath() + "' doesn't exist or there are no files");
    	} 
    	else if (children.length > 0) {
    		//Checking the number of files against the number of thread +1(children object has results as well, thats why +1 )
    		if(children.length > Integer.parseInt(maxThreadNum)+1){    			
    			super.addErrorMsg("FileProcessing", ("The number of files '"+ (children.length-1) +"' are more than the maximum '" + maxThreadNum + "' allowed. A suggestion is to include more transactions per file, resulting in fewer files."), "", ErrorTools.SEVERITY_ERROR.toString());
    			return jobList;
    		}
    		for (int i=0; i<children.length; i++) {
    			
    			// Get filename of file or directory
    			String thisFileName = f.getAbsolutePath() + "/" + children[i];
    			
    			File thisFile = new File(thisFileName);
    			if(thisFile.isFile()){
    				Log.debug("------ Processing file " + thisFile.getAbsolutePath() + "------");
    				try{
    					if(!thisFile.getName().contains("error")){
    						// 	start the new job
    						jobList.add(createNewJob(thisFile));
    						super.addErrorMsg("Processing Completed", "Processed file successfully:" + thisFile.getAbsolutePath(), "Info", "Info");
    					} else {
    						Log.debug("------ Ignoring file " + thisFile.getAbsolutePath() + "------");
    						super.addErrorMsg("Processing Error", "Ignoring file " + thisFile.getAbsolutePath() + ": Files with name 'error' will not be processed", "Error");
    					}
    				}
    				catch(Exception e){
    					Log.error(e);
    					// Don't die if one file fails. Just move on to the next
    		     		super.addErrorMsg("ProcessingError", "Error processing file " + thisFile.getAbsolutePath() + ": " + e.getMessage(), "Error");
    				}
    				finally{
    					Log.debug("------ Finished Processing file " + thisFile.getAbsolutePath() + "------");    					
    				}
    			}
    		}
    	}
    	else {
       		throw new Exception("The directory '" + f.getAbsolutePath() + "' is empty");
    	}       
    	return jobList;
    }
    
    /** This method creates a new data conversion job and returns the job bean
     * @param convFile 
     * @return job
     * @throws Exception
     */
    
    private ModelBean createNewJob(File convFile) throws Exception{
    	ModelBean job = new ModelBean("Job");
        JDBCData data = this.getHandlerData().getConnection();
        
    	job.setValue("StartDt",DateTools.getStringDate());
    	job.setValue("StartTm",DateTools.getTime());
    	//job.setValue("StartUserId",responseParams.getBean("UserInfo").gets("LoginId"));
    	job.setValue("JobTemplateIdRef","DataConverterJob");    	
    	
    	job.setValue("Status","Started");
    	
    	ModelBean jobTemplate = Operation.getJobTemplate("DataConverterJob");
    	ModelBean[] jobActionTemplate  = jobTemplate.getAllBeans("JobActionTemplate");            	
    	ModelBean jobActions = new ModelBean("JobActions");
    	
    	if( jobActionTemplate.length > 0 ) {            		
    		job.addValue(jobActions);
    	}
    	
    	for( int i=0;i<jobActionTemplate.length; i++ ) {
    		ModelBean jobAction = new ModelBean("JobAction");
    		// Set the id to the Job Action's id so that it is predictable.  This makes it easier for a web service call to set the Skip Indicator.
    		jobAction.setValue("id",jobActionTemplate[i].getId());

    		jobAction.setValue("JobActionTemplateIdRef",jobActionTemplate[i].getId());
    		jobAction.setValue("SkipInd","No");
    		jobAction.setValue("Status","Pending");    		
    		jobActions.addValue(jobAction);
    	}
    	// Set up the job parameters
        ModelBean questionReplies = new ModelBean("QuestionReplies");
        
    	ModelBean[] questionReply = new ModelBean[5];
    	questionReply[0] = new ModelBean("QuestionReply");
    	questionReply[0].setValue("Name", "TableName");
    	questionReply[0].setValue("Value", templateName);
    	questionReplies.addValue(questionReply[0]);
    	   	
    	questionReply[1] = new ModelBean("QuestionReply");
    	questionReply[1].setValue("Name", "FileNameOrDirectory");
    	questionReply[1].setValue("Value", convFile.getAbsolutePath());
    	questionReplies.addValue(questionReply[1]);
    	    	
    	questionReply[2] = new ModelBean("QuestionReply");
    	questionReply[2].setValue("Name", "RunDt");
    	questionReply[2].setValue("Value", extractDate);
    	questionReplies.addValue(questionReply[2]);   	
    	
    	questionReply[3] = new ModelBean("QuestionReply");
    	questionReply[3].setValue("Name", "ConversionGroup");
    	questionReply[3].setValue("Value", convFile.getName().replace(".xml", ""));
    	questionReplies.addValue(questionReply[3]);
    	
    	questionReply[4] = new ModelBean("QuestionReply");
    	questionReply[4].setValue("Name", "LeaveFailedApps");
    	questionReply[4].setValue("Value", leaveFailedApps);
    	questionReplies.addValue(questionReply[4]);
    	   	    	
    	job.addValue(questionReplies);
    	
    	
    	if( job.gets("SystemId").length() == 0){
    		// save the job bean to generate a system ID-- needed for jobs started
    		// from the command line processor
        	BeanTools.saveBean(job, data);
        	data.commit();
    	}

    	// Set up the batch job manager and call the appropriate Task Processor
        BatchJobManager manager = new BatchJobManager();
        
        // Get custom class loader
        ClassHelper helper = new ClassHelper();
                        		
        // Create a ModelBeanProcessorBaseSPI
        String procName = "com.iscs.common.admin.operation.processor.GenericProcessor";
        	                	
        ModelBeanProcessorBaseSPI mbp = (ModelBeanProcessorBaseSPI) helper.createThis(procName, null);
    
        try {
            mbp.setUserID(rs.getBean("ResponseParams").gets("UserId"));
            
            // Set up the job parameters
            mbp.addParam("JobRef", job.gets("SystemId"));
            
            // Set up the parameters needed for webpath security
            // Use the interal-only SessionId rather than the starting users's SessionId.
            // This isolates the batch job's internal webpath calls from any changes in
            // UserInfo.UserSession beans that may occur as the user logs in and out independent
            // of the batch job. Instead, they use the UserId directly.
            ModelBean resParams = rs.getBean("ResponseParams");
            mbp.addParam("RqUID", rq.getBean("RequestParams").gets("RqUID"));
            mbp.addParam("SecurityId", rq.getBean("RequestParams").gets("SecurityId"));
            mbp.addParam("SessionId", IBIZServer.SESSIONID_SECURITY_USE_USERID_DIRECTLY);
            mbp.addParam("ConversationId", IBIZServer.SESSIONID_SECURITY_USE_USERID_DIRECTLY);
            mbp.addParam("UserId", resParams.gets("UserId"));
            mbp.addParam("UserSessionId", resParams.gets("SessionId"));
            mbp.addParam("TransactionResponseDt", resParams.getDate("TransactionResponseDt").toString());
            mbp.addParam("TransactionResponseTm", resParams.gets("TransactionResponseTm"));
            mbp.addParam("RunUser", resParams.gets("UserId"));     	
        	mbp.addParam("LeaveFailedApps", leaveFailedApps);
        	ModelBean[] replies = questionReplies.getBeans("QuestionReply");
            for( ModelBean reply:replies){
            	mbp.addParam(reply.gets("Name"), reply.gets("Value"));
            } 
        	// Call the ModelBeanProcessorBaseSPI initializations                
            mbp.initialCall();
        }
        catch (Exception e){
            throw new IBIZException("Cannot load the Task batch processor", e);
        }
        
        // Create a BatchJobTable object
        BatchJob batchJob = new BatchJobTable("Job", mbp);

    	// Update the job record and commit it
        job.setValue("LogFilename", batchJob.getLogFileName());
    	BeanTools.saveBean(job, data);
    	data.commit();
        
    	// Set the title of the batch job (shows up as part of thread name)
    	batchJob.setTitle("BatchJob - " + jobTemplate.getId());

        // Add job to manager
        manager.add(batchJob);
        
        // Update the flag to tell the user the batch job is currently running
        ModelBean addl = rs.getBean("ResponseParams").getBean("AdditionalParams");
        setParam(addl, "CycleProcessing", "Yes", true);
        
        return job;
    }
    
    /** Set an additional parameter in the response. Utility routine
     * @param parent ModelBean containing the parent of the parameter
     * @param name String holding the name of the parameter to set or add
     * @param value String containing the value of the paramter
     * @param useOld boolean determining whether to use an existing parameter or add a new one
     * @throws Exception when an error occurs
     */
    private void setParam(ModelBean parent, String name, String value, boolean useOld) 
    throws Exception {
        ModelBean param = null;
        if (useOld)
            param = parent.getBean("Param", "Name", name);
        if (param == null) {
            param = new ModelBean("Param");
            param.setValue("Name", name);
            parent.addValue(param);
        }
        param.setValue("Value", value);
    }
    
    /**
     * Unzips a file into the path given
     * @param zipFilePath
     * @param toDir
     * @param buffersize
     * @return true if zip file; false for error or is not a .zip file
     * @throws ContentWriterException
     */
    protected void unzip(String uploadFile, String toDir, int buffersize) 
	throws Exception {		
		ZipInputStream zisForFolder = null;
		FileInputStream fisForFolder = null;
		ZipInputStream zis = null;
		FileInputStream fis = null;
		boolean isFolderAvailable = false;
		int entryCount = 0;		
		try {    	  
    	  BufferedOutputStream dest = null;
    	  fisForFolder = new FileInputStream(uploadFile);
    	  zisForFolder = new ZipInputStream(new BufferedInputStream(fisForFolder));
    	  ZipEntry folderEntry;	    	
    	  // First read the folder(s) by iterating through all entries. Then unzip the files
    	  while( (folderEntry = zisForFolder.getNextEntry()) != null) {
    		  if(folderEntry.isDirectory() ) {
    			  super.addErrorMsg("Processing Error", "The zip file should not contain any directories.", "Error");
    			  isFolderAvailable = true;
    		  }   		  
    	  }	    	
    	  if(!isFolderAvailable){	    	  
			  fis = new FileInputStream(uploadFile);    		  
	    	  zis = new ZipInputStream(new BufferedInputStream(fis));
	    	  ZipEntry entry;		
	    	  while( (entry = zis.getNextEntry()) != null) {
	    		  if(entry.isDirectory() ) {
	    			  continue;
	    		  }else {
	    			  entryCount++;
		    		  int count;
		    		  byte data[] = new byte[buffersize];
		    		  // write the files to the disk
		    		  FileOutputStream fos = new FileOutputStream(toDir+"/"+entry.getName() );
		    		  dest = new BufferedOutputStream(fos, buffersize);
		    		  while ( (count = zis.read(data, 0, buffersize) ) != -1) {
		    			  dest.write(data, 0, count);
		    		  }
		    		  dest.flush();
		    		  dest.close();
	    		  }
	    	  }	  
	    	  if(entryCount == 0){
	    		  super.addErrorMsg("Processing Error", "The Zip File should contain atleast one valid conversion xml file.", "Error");
	          }  
    	  }    	    	   
      } catch(Exception e) {
         throw new Exception(e);
      } finally {
    	  try {
    		  if(zisForFolder != null) zisForFolder.close();
			  if(fisForFolder != null) fisForFolder.close();
    		  if(zis != null) zis.close();
			  if(fis != null) fis.close();
    	  } catch (IOException e) {
    		  throw new OpenFrameworkException(e);
    	  }
      }
	}

}
