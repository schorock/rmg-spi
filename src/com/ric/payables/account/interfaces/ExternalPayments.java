package com.ric.payables.account.interfaces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.payables.account.AccountPayable;
import com.iscs.payables.account.AccountPayableException;
import com.iscs.payables.common.SessionInfo;
import com.iscs.payables.common.stat.PayableStats;

public class ExternalPayments {

	/** Process the payable request and return the resulting conforming request ModelBean
	 * @param data The data connection
	 * @param request The initial request Modelbean
	 * @param info The SessionInfo object containing session information
	 * @return The Errors ModelBean if business error(s) occurred 
	 * @throws Exception when an error occurs
	 * @throws AccountPayableException when a known error occurs
	 */
	public void processRequest(JDBCData data, AccountPayable payable, ModelBean externalPayment, SessionInfo sessionInfo, ArrayList<ModelBean> errorList) 
	throws Exception {

		if (payable == null) {
			payable = new AccountPayable();
		}
		
		// Pull out the external PayableBatchRequest bean
		ModelBean externalPayableBatchRequest = externalPayment.getBean("PayableBatchRequest");
		String batchRequestSystemId = externalPayableBatchRequest.getSystemId();

		// Find the corresponding PayableBatchRequest
		ModelBean payableBatchRequest = null;
		try {
			payableBatchRequest = new ModelBean("PayableBatchRequest");
			data.selectModelBean(payableBatchRequest, Integer.parseInt(batchRequestSystemId));
		} catch (Exception e) {
			errorList.add(ErrorTools.createError("PayableBatchRequestError", "The corresponding PayableBatchRequest record was not located for the External Payment record of id " + externalPayment.getId(), ErrorTools.GENERIC_BUSINESS_ERROR));
		}

		ModelBean payment = null;
		// Run the correct payment type as if it was done in Innovation
		if( externalPayment.gets("TransactionCd").equals(AccountPayable.TRANS_PAYMENT) ) {
			
    		// Create the actual Innovation Payment
            payment = payable.createExternalPayment(data, payableBatchRequest);
			payable.pyAddPaymentTransaction(payment, sessionInfo, AccountPayable.STATUS_OPEN);
				                               
            // Create the print transaction
            if( payment.gets("PaymentMethodCd").equals("ACH") ) {
               	payable.pyAddPaymentTransaction(payment, sessionInfo, AccountPayable.TRANS_OPEN, true);
            }
            else {
               	payable.pyAddPaymentTransaction(payment, sessionInfo, AccountPayable.TRANS_PRINT, true);
            }
			
			// Update Payment fields from the External Payment
			if( externalPayableBatchRequest.gets("ItemNumber").isEmpty() ) {
				payment.setValue("ItemNumber", externalPayableBatchRequest.getSystemId());
    			payment.setValue("ItemDt", sessionInfo.getBookDate());  
			}
			else {
				payment.setValue("ItemNumber", externalPayableBatchRequest.gets("ItemNumber"));
    			payment.setValue("ItemDt", externalPayableBatchRequest.gets("ItemDt")); 
			}  			
			
			// Update the Batch Request fields from the External Payment
			payableBatchRequest.setValue("StatusCd", AccountPayable.STATUS_PROCESSED);
			payableBatchRequest.setValue("ItemNumber", externalPayableBatchRequest.gets("ItemNumber"));
			payableBatchRequest.setValue("ItemDt", externalPayableBatchRequest.gets("ItemDt"));    	
			BeanTools.saveBean(payableBatchRequest, data);
			
		} else { 

			// Find the linked PayableRequest
			int count = 0;
			ModelBean[] payableRequestAllocations = payableBatchRequest.getBeans("PayableBatchRequestAllocation");
			for( ModelBean payableRequestAllocation : payableRequestAllocations ) {
    			ModelBean payableRequest = new ModelBean("PayableRequest");
	    		data.selectModelBean(payableRequest, Integer.parseInt( payableRequestAllocation.gets("PayableRequestRef") ));
	    			 
	    		ModelBean errors = new ModelBean("Errors");
    			if( externalPayment.gets("TransactionCd").equals(AccountPayable.TRANS_VOID) ) {
    				payable.pyVoidExternalPayment(data, payableRequest, sessionInfo, errors);
    			} else if (externalPayment.gets("TransactionCd").equals(AccountPayable.TRANS_UNVOID)) {
    				payable.pyUnvoidExternalPayment(data, payableRequest, sessionInfo, errors);
    			} else if (externalPayment.gets("TransactionCd").equals(AccountPayable.TRANS_STOP)) {
    				payable.pyStopExternalPayment(data, payableRequest, sessionInfo, errors);
    			} else if (externalPayment.gets("TransactionCd").equals(AccountPayable.TRANS_UNSTOP)) {
    				payable.pyUnstopExternalPayment(data, payableRequest, sessionInfo, errors);
    			}
    			
    			// Do not throw exceptions for allocations after the first one...
    			// Ex: There may not be an 'Open' payment to void after the first allocation causes the void
    			if( count == 0 ) {
    				errorList.addAll(Arrays.asList(errors.getBeans("Error")));
    			}
    			count++;
			}

			payment = payable.getExternalPayment();    			
			
			if( payment != null ) {
				externalPayment.setValue("PaymentRef",payment.getSystemId());
				String itemNumber = payment.gets("ItemNumber");
				String itemAmt = payment.gets("ItemAmt");
				String itemDt = payment.gets("ItemDt");
				payableBatchRequest = externalPayment.getBean("PayableBatchRequest");
    			
    			if( externalPayment.gets("TransactionCd").equals(AccountPayable.TRANS_VOID) || externalPayment.gets("TransactionCd").equals(AccountPayable.TRANS_STOP) ) {
    				payableBatchRequest.setValue("ItemNumber", itemNumber);
	    			payableBatchRequest.setValue("ItemAmt", "-"+itemAmt);
	    			payableBatchRequest.setValue("ItemDt", itemDt);
    			}
    			payableBatchRequest.setValue("StatusCd", AccountPayable.STATUS_PROCESSED);
			}
		}
		
		if (errorList.size() > 0) {
			return;
		}
		
		payment.setValue("CheckUpdatedInd", "Yes");
		BeanTools.saveBean(payment, data);
		
		// Call Stats to process this payment transaction
		PayableStats stats = new PayableStats();
		stats.processStats(data, payment, sessionInfo.getDate());
		
		externalPayment.setValue("ExternalPaymentStatusCd",AccountPayable.STATUS_PROCESSED);
		externalPayment.setValue("CompleteDt",sessionInfo.getDate());
		externalPayment.setValue("CompleteTm",sessionInfo.getTime());

		// Set the date and time the same for the entire batch
		BeanTools.saveBean(externalPayment, data);	
		
	}

	/** Obtain the list of prior external payment files. This data is not stored anywhere, but rather each external payment file run, all of the 
	 * external payments are set with a date and time stored in the CreateDt and CreateTm fields respectively. This will return a single ExternalPayment
	 * model representing each run. 
	 * @param data The data connection
	 * @param accountCd The bank account code if specified to obtain only the file list based on that bank account
	 * @param dateStart The date to start looking for files
	 * @param dateEnd The date up to the end to look for files
	 * @return The list of ExternalPayments representing the date and times of each file
	 * @throws Exception when an error occurs
	 */
	public ModelBean[] getExternalPaymentFilesList(JDBCData data, String accountCd, StringDate dateStart, StringDate dateEnd) throws Exception {
		ArrayList<ModelBean> externalPaymentList = new ArrayList<ModelBean>();
		HashMap<String, String> dateTimeMap = new HashMap<String, String>();

		String statusList = AccountPayable.STATUS_PROCESSED + "," + AccountPayable.STATUS_RELEASED;
		String dateRange = dateStart.toString() + "," + dateEnd.toString();
		
		JDBCData.QueryResult[] results = data.doQueryFromLookup("ExternalPayment", new String[] {"PaymentAccountCd", "ExternalPaymentStatusCd", "CreateDt", "CreateTm"}, new String[]{"=", "IN", "BETWEEN", "INFO"}, new String[] {accountCd, statusList, dateRange, ""}, 0);
		for (JDBCData.QueryResult result : results) {
			String dateTime = result.getLookupValue("CreateDt") + result.getLookupValue("CreateTm");
			String systemId = dateTimeMap.get(dateTime);
			if (systemId == null) {
				dateTimeMap.put(dateTime, result.getSystemId());
				ModelBean externalPayment = new ModelBean("ExternalPayment");
				data.selectModelBean(externalPayment, result.getSystemId());
				externalPaymentList.add(externalPayment);
			}
		}
		return externalPaymentList.toArray(new ModelBean[externalPaymentList.size()]);
	}
}
