package com.ric.payables.account.interfaces.handler;

import java.io.BufferedReader;
import java.io.File;
import java.util.ArrayList;

import com.iscs.common.mda.Store;
import com.iscs.common.shared.SystemBookDt;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.common.utility.io.FileTools;
import com.iscs.payables.account.AccountPayable;
import com.iscs.payables.account.interfaces.ExternalPaymentProcessorHandler;
import com.ric.payables.account.interfaces.ExternalPayments;
import com.iscs.payables.common.SessionInfo;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.mda.MDAOption;
import net.inov.mda.MDATable;
import net.inov.tec.beans.Helper_Beans;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.file.FileIO;
import net.inov.tec.obj.ClassHelper;
import net.inov.tec.web.cmm.AdditionalParams;

/** Process the incoming external payment system response file
 * 
 *
 */
public class ExternalPaymentSystemProcess extends InnovationIBIZHandler {
	
    /** Creates a new instance of ExternalPaymentSystemProcess
     * @throws Exception never
     */
    public ExternalPaymentSystemProcess() throws Exception {
    }

	private static final String UPLOAD_DIR = FileTools.cleanPath(net.inov.tec.prefs.PrefsDirectory.getLocation()) + "upload";
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a greeting
            Log.debug("Processing ExternalPaymentSystemProcess...");
            ModelBean rq = this.getHandlerData().getRequest();
            ModelBean rs = this.getHandlerData().getResponse();
            JDBCData data = this.getHandlerData().getConnection();
            ModelBean requestParams = rq.getBean("RequestParams");
            ModelBean responseParams = rs.getBean("ResponseParams");
            AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest());

            String responseFilename = ap.gets("ResponseFilename");
					          
            // Extract the File Name from the Path
			String fileName = "";

			// Determine which file separator the client uses
			if (responseFilename.indexOf("/") >= 0) {
				fileName = responseFilename.substring(responseFilename.lastIndexOf("/") + 1);
			} else {
				fileName = responseFilename.substring(responseFilename.lastIndexOf("\\") + 1);
			}
			Log.debug("fileName: " + fileName);
			
    		// Build the uploaded file name with "upload" directory path
        	// Build the path to the uploaded file
            String uploadedFileName = FileTools.cleanPath(net.inov.tec.prefs.PrefsDirectory.getLocation()) + "upload" + File.separator + fileName;
            Log.debug("uploadFile: " + uploadedFileName);
    		
    		ArrayList<ModelBean> errorList = new ArrayList<ModelBean>();

            // Setup the Session Information
            StringDate bookDt = SystemBookDt.getBookDt(data);
            SessionInfo sessionInfo = new SessionInfo(requestParams, responseParams, bookDt);

            AccountPayable payable = new AccountPayable();
    		ExternalPayments externalPayable = new ExternalPayments();
    		if (uploadedFileName.endsWith(".csv") || uploadedFileName.endsWith(".txt") || uploadedFileName.endsWith(".xls") || uploadedFileName.endsWith(".xlsx")){
    			
    			BufferedReader reader = FileIO.openFile(uploadedFileName);
    			
    			String thisLine;
    			String externalPaymentId = null;
    			int lineCount = 0;
    				try{
    					while( (thisLine = reader.readLine()) != null ) {        	    	
    						lineCount ++;
    						String[] requests = thisLine.split(",");
    						if(requests.length > 0){
	    						externalPaymentId = requests[SystemId];
	    						// Find the corresponding ExternalPayment record and process it
	    		    			ModelBean externalPaymentBean = new ModelBean("ExternalPayment");
	    		    			
	    		    			data.selectModelBean(externalPaymentBean, Integer.parseInt(externalPaymentId));
	    		    			ModelBean payableBatchRequest = externalPaymentBean.getBean("PayableBatchRequest");
	    		    			if(payableBatchRequest.gets("TransactionCd").equalsIgnoreCase("Payment")){
	    		    				payableBatchRequest.setValue("ItemNumber", requests[ItemNumber]);
		    		    			payableBatchRequest.setValue("ItemAmt", requests[ItemAmt]);
		    		    			payableBatchRequest.setValue("ItemDt", requests[ItemDt]);
		    		    			payableBatchRequest.setValue("PayToName", requests[PayeeName]);
		    		    			payableBatchRequest.setValue("TransactionCd", "Payment");
	    		    			}
	    		    			
	    		    				externalPayable.processRequest(data, payable, externalPaymentBean, sessionInfo, errorList);
    						}	
    				}
    				reader.close();
    			} catch (Exception e) {
    				reader.close();
    				throw new IBIZException ("Unable to process file. Error on line " + lineCount);	
    			}
    		}
    		
    		for (ModelBean error : errorList) {
    			addErrorMsg(error.gets("Name"), error.gets("Msg"), ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
    		}
    		
            // Return the response bean
            return rs;
        }
        
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
    
    /** Gets the correspondence template to send Escheat notification letter for a Check Payment based on its source(Claim/Billing)
	 * @param sourceCd The Payment Source ( SourceCd field of the Payment Bean) 
	 * @return String The correspondence template
	 * @throws Exception
	 */
	public ExternalPaymentProcessorHandler getFileProcessor() throws Exception {
		String handler = null;
		String mdaPackage = "*::external-payment::external-system::configuration::all";
		MDATable table = (MDATable) Store.getModelObject(mdaPackage);		
		MDAOption option = table.getOption("response-handler");
		if (option != null) {
			handler =  option.getLabel();
		}

		if (handler != null) {
			Object obj = ClassHelper.loadObject(handler);
			Class<?> classObject = obj.getClass();
			Class<?>[] classInterfaceArray = classObject.getInterfaces();
			boolean hasInterface = false;
			
			for( Class<?> classInterface : classInterfaceArray ) {
				if( classInterface.getName().equals("com.iscs.payables.account.interfaces.ExternalPaymentProcessorHandler") ) {
					hasInterface = true;
					break;
				}
			}
			
			if( hasInterface ) {
				return (com.iscs.payables.account.interfaces.ExternalPaymentProcessorHandler) obj;
			} else {
				return null;
			}        	
		}
		return null;
	}
  
	
	
	
	

	public static int SystemId = 0;
	public static int Payment = 1;
	public static int ItemNumber = 2;
	public static int ItemAmt = 3;
	public static int ItemDt = 4;
	public static int PayeeName = 5;
	
	

	
}
