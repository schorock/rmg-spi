package com.ric.payables.account.interfaces.handler;

import java.io.File;
import java.util.ArrayList;

import com.iscs.common.utility.error.ErrorTools;
import com.iscs.payables.account.interfaces.ExternalPaymentProcessorHandler;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.xml.XmlDoc;


public class ExternalPaymentFileProcessor implements ExternalPaymentProcessorHandler {

	private ModelBean[] paymentList = null;
	private int recordCount = 0;
	
	/** Process the external Payables response file. The loading, verification and initial setup is done here. 
	 * @param data The data connection if needed for verification
	 * @param filename The filename of the external file
	 * @param errorList The list of errors to report back to the external processor
	 * @throws Exception when an error occurs
	 */
	public void processExternalFile(JDBCData data, String filename, ArrayList<ModelBean> errorList) throws Exception {
		// Load the response file
		
		File f = null;
		f = new File(filename);

		if (!f.exists()) {
			ErrorTools.addError("FileError", "The file specified does not exist.", ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR, errorList);
			return;
		}

		XmlDoc xmlDoc = new XmlDoc(f);
		
        ModelBean externalPayments = new ModelBean("ExternalPayments", xmlDoc);
        
        paymentList = externalPayments.getBeans("ExternalPayment");
	}
	
	/** Read the next external file entry to process
	 * @param account The Account ModelBean
	 * @param summary The Account Summary Stat ModelBean
	 * @throws Exception when an error occurs
	 */
	public boolean next() throws Exception {
		return paymentList.length > recordCount;
	}
	
	public ModelBean getNextPayment() throws Exception {
		if (recordCount > paymentList.length - 1) {
			return null;
		} else {
			return paymentList[recordCount++];
		}
	}
	
}