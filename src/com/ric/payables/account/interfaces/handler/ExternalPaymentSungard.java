package com.ric.payables.account.interfaces.handler;

import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.error.ErrorTools;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.thread.ManagedTask;
import net.inov.tec.thread.MasterThreadManager;
import net.inov.tec.web.cmm.AdditionalParams;
/** Generate a file for an external payment system in Sungard format
 * 
 * @author beno
 *
 */
public class ExternalPaymentSungard extends InnovationIBIZHandler {
	
    /** Creates a new instance of ExternalPaymentSungard
     * @throws Exception never
     */
    public ExternalPaymentSungard() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a greeting
            Log.debug("Processing ExternalPaymentSungard...");
            
            // Initialize variables
            ModelBean rq = this.getHandlerData().getRequest();
            ModelBean rs = this.getHandlerData().getResponse();
            JDBCData data = this.getHandlerData().getConnection();
            AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest());
                		
            // Update Managed Task Progress
            String managedTaskId = ap.gets("ManagedTaskId");
			ManagedTask managedTask = MasterThreadManager.getInstance().getTask(managedTaskId);
            updateProgress(managedTask);
            
            // Generate Sungard files
            com.ric.interfaces.gl.handler.APGeneralLedgerProcessor apDaily = new com.ric.interfaces.gl.handler.APGeneralLedgerProcessor();
            apDaily.process(data, rq, rs);
    		
            // Return the response bean
            return rs;
        }
        catch( IBIZException ibe) {
        	addErrorMsg("SungardError", ibe.getMessage(), ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
        	throw ibe;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
    
    /** Update Managed Task Progress
     * @param managedTask Managed Task
     * @param checksCompleteCount Number of Checks Printed
     * @param checksTotalCount Total Number of Checks 
     * @throws Exception if an Unexpected Error Occurs 
     */
    private void updateProgress(ManagedTask managedTask) 
    throws Exception {
    	try {
    		if(managedTask!=null){
	    		String progress = "Building external payment file in Sungard format.";
	    		
	    		managedTask.setProgress(progress);
    		}
    	}
        catch( Exception e ) {
            throw new Exception(e);
        }
    }
    
}