package com.ric.payables.account.processor;

import java.sql.SQLException;

import com.iscs.ar.render.AccountRenderer;
import com.iscs.common.render.DateRenderer;
import com.iscs.common.render.NumberRenderer;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.common.tech.service.ModelBeanProcessorBaseSPI;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.uw.policy.Policy;
import com.iscs.workflow.Task;
import com.iscs.workflow.render.WorkflowRenderer;

import net.inov.biz.server.IBIZXmlController;
import net.inov.tec.batch.MBPException;
import net.inov.tec.batch.MBPLookupSpec;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

public class ReviewShortPaymentTaskProcessor extends ModelBeanProcessorBaseSPI {

	public ReviewShortPaymentTaskProcessor() throws Exception {
		super();
		this.spec = ModelSpecification.getSharedModel();
	}

	/**
	 * Bean processor main routine. Each bean in the data repository table will be
	 * presented to this processor. The criteria for task escalation will be handled
	 * and then a resultant success or fail indicator will be passed back.
	 * 
	 * @param application Each application bean in the data repository that is
	 *                    passed for evaluation
	 * @param data        JDBC data repository connection
	 * @return boolean of whether this bean has been processed or not
	 * @throws MBPException when an error has occurred.
	 */
	public boolean processBean(ModelBean electronicPayment, JDBCData data) throws MBPException {
		try {
			// Log a Greeting
			Log.debug("Processing ReviewShortPaymentTaskProcessor...");
			
			String accountNumber = electronicPayment.gets("SourceIdRef");
			
			//Ignore Statement Accounts
			if( !electronicPayment.gets("SourceModelName").equals("Account") || accountNumber.substring(0,2).equals("SA") )
				return true;
			
			String receiptIdRef = electronicPayment.gets("ReceiptIdRef");
			String transactionId = electronicPayment.getBean("ElectronicPaymentSource").gets("TransactionId");
			ModelBean batchReceipt = BeanTools.selectModelBeanFromLookup(data, "BatchReceipt", "TransactionId", transactionId);
			ModelBean arReceipt = batchReceipt.getBean("ARReceipt", "id", receiptIdRef);
			String amount = arReceipt.gets("ReceiptAmt");
			StringDate paymentDt = electronicPayment.getDate("SentDt");
			
			//Only run when amounts are not 0.00.
			if( amount.isEmpty() || amount.equals("0.00") )
				return true;

			//Need to figure out what schedule we are on.
			ModelBean account = AccountRenderer.getAccountByAccountNumber(accountNumber);
			ModelBean[] arSchedules = account.getBean("ARPayPlan").getBeans("ARSchedule");
			ModelBean currentARSchedule = null;
			
			for ( ModelBean arSchedule : arSchedules ) {
				if( DateRenderer.lessThanEqual(paymentDt, arSchedule.getDate("DueDt")) ) {
					currentARSchedule = arSchedule;
					break;
				}
			}
			
			if ( currentARSchedule == null )
				return true;
			
			//Calculate amount due for the schedule.
			String amountDue = "0.00";
			for ( ModelBean arApply : currentARSchedule.getBeans("ARApply") ) {
				amountDue = NumberRenderer.addBigDecimal(amountDue, arApply.gets("Amount"));
			}
			
			if ( amountDue.equals("0.00") || StringRenderer.greaterThanEqual(amount, amountDue) )
				return true;
				
			ModelBean policy = Policy.searchByPolicyNumber(data, accountNumber, true, false)[0];
			ModelBean task = Task.getTask(data, "ReviewShortPaymentTask", policy, "Open");

			if (task != null)
				return true;

			task = WorkflowRenderer.createTask(data, "ReviewShortPaymentTask", policy, new StringDate());

			// Save Task
			Task.insertTask(data, task);

			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Data repository selection criteria. To look up any InProcess, NewBusiness
	 * Quote having EffectiveDt older than the runDt - ExpireDays (which is
	 * configured in the job.xml)
	 * 
	 * @return MBPLookupSpec The lookup specification object containing the fields
	 *         to be used for data selection
	 * @throws MBPException when an error occurs
	 */
	public MBPLookupSpec getLookupSpec() throws MBPException {
		try {
			StringDate runDt = getRunDate();
			return new MBPLookupSpec(
					new String[] { "SentDt" }, 
					new String[] { "=" },
					new String[] { runDt.toString() });
		} catch (Exception e) {
			throw new MBPException(e);
		}
	}

	/**
	 * Find out the run date of the current batch process
	 * 
	 * @return StringDate The date of the current batch run is set to.
	 * @throws Exception when an error occurs
	 */
	public StringDate getRunDate() throws Exception {
		// Find if the run date has been passed in
		if (this.getParam("RunDt", "").equals("")) {
			return DateTools.getStringDate();
		} else {
			return new StringDate(this.getParam("RunDt"));
		}
	}

	protected ModelBean callService(ModelBean rq) throws Exception {

		// Call the service
		return IBIZXmlController.processRequest(rq, true);
	}

	/**
	 * Set up the processor before the batch job is executed. <BR>
	 * <BR>
	 * Instance variables runDt, myName, and templateId in base TaskReminderProcess
	 * are private, so must get my own.
	 *
	 * @throws Exception when an error occurs
	 */
	public void initialCall() throws MBPException {
		try {
			// Set the error reporting to use the new JobProgress class instead of
			// immediately
			// reporting to the Processor table.
			this.setReportErrorsImmediately(false);
		} catch (Exception e) {
			// If the job has been entered into the process table, go ahead and mark it as
			// an error
			try {
				jobActionProgress.addExceptionError(e.getMessage());
				jobActionProgress.updateJobActionStatus("Error");
			} catch (Exception ec) {
				throw new MBPException(ec);
			}

			throw new MBPException(e);
		}
	}

	/**
	 * Update the process control manager: this process is now complete.
	 * 
	 * @param beanData The data repository connection
	 * @throws MBPException when an error occurs
	 */
	public void finalCall(JDBCData beanData) throws MBPException {
		try {
			// Make an entry in the processor table to show that we have finished.
			// This is for information purposes only.
			jobActionProgress.updateJobActionStatus("Complete");
		} catch (SQLException sqle) {
			// Do not throw an exception if its because the result of the search is empty
			if (sqle.getErrorCode() != 0)
				throw new MBPException(sqle);
		} catch (Exception e) {
			throw new MBPException(e);
		}
	}
}