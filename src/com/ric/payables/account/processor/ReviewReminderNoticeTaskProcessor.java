package com.ric.payables.account.processor;

import java.sql.SQLException;

import com.iscs.ar.render.AccountRenderer;
import com.iscs.common.render.DateRenderer;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.common.tech.service.ModelBeanProcessorBaseSPI;
import com.iscs.common.utility.DateTools;
import com.iscs.document.render.DCRenderer;
import com.iscs.uw.policy.Policy;
import com.iscs.workflow.Task;
import com.iscs.workflow.render.WorkflowRenderer;
import net.inov.biz.server.IBIZXmlController;
import net.inov.biz.server.ServiceContext;
import net.inov.tec.batch.MBPException;
import net.inov.tec.batch.MBPLookupSpec;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.security.SecurityManager;

public class ReviewReminderNoticeTaskProcessor extends ModelBeanProcessorBaseSPI {

	public ReviewReminderNoticeTaskProcessor() throws Exception {
		super();
		this.spec = ModelSpecification.getSharedModel();
	}

	/**
	 * Bean processor main routine. Each bean in the data repository table will be
	 * presented to this processor. The criteria for task escalation will be handled
	 * and then a resultant success or fail indicator will be passed back.
	 * 
	 * @param application Each application bean in the data repository that is
	 *                    passed for evaluation
	 * @param data        JDBC data repository connection
	 * @return boolean of whether this bean has been processed or not
	 * @throws MBPException when an error has occurred.
	 */
	
	//To Generate this task correctly, please run this job along with Incremental Datamart Export Job to pick up the latest transactions
	public boolean processBean(ModelBean electronicPayment, JDBCData data) throws MBPException {
		try {
			// Log a Greeting
			Log.debug("Processing ReviewReminderNoticeTaskProcessor...");

			String accountNumber = electronicPayment.gets("SourceIdRef");
			ModelBean account = AccountRenderer.getAccountByAccountNumber(accountNumber);
			StringDate noticeDt = account.getDate("NoticeDt"); 
			String nextCycleActionCd = account.gets("NextCycleActionCd");
			StringDate runDt = getRunDate();			
			StringDate afterSixDays = DateRenderer.advanceDate(runDt, "6");
			String todayDt = DateRenderer.getDate();
			
			//Check if Batch Job RunDt is more than 6 days prior to the Legal Notice Date on Account and Next Action is NOT Legal
			if( DateRenderer.greaterThanEqual(noticeDt, afterSixDays) ) {
				int nsfCnt = 0; 
				int nsfReversalCnt = 0;
				int achOrCheckCnt = 0;
				int reverseCCCnt = 0; 
				int unreverseCCCnt = 0; 
				int reverseCashReceiptCnt = 0;
				int unreverseCashReceiptCnt = 0;
				boolean nsf = false;
				boolean reverseCC = false;
				boolean reverseCashReceipt = false;
				ModelBean accountInquiry = AccountRenderer.getAccountInquiry(account);
				ModelBean [] arTrans = accountInquiry.getBeans("ARTrans"); 

				for (ModelBean arTr : arTrans) {
					if(arTr.gets("TypeCd").equalsIgnoreCase("ManualReversal") ) {
						if(arTr.gets("Desc").equalsIgnoreCase("NSF") ) {
							nsf = true;
							nsfCnt++; 
							if(arTr.gets("ARReceiptTypeCd").equalsIgnoreCase("ACH") || arTr.gets("ARReceiptTypeCd").equalsIgnoreCase("Check") )
								achOrCheckCnt++; 
						} else if (arTr.gets("Desc").equalsIgnoreCase("Reverse NSF Fee")) {
							nsfReversalCnt++;
						} else if (arTr.gets("Desc").equalsIgnoreCase("Reverse Credit Card Transaction")) {
							reverseCC = true; 
							reverseCCCnt++;
						} else if (arTr.gets("Desc").equalsIgnoreCase("Unreverse Credit Card Transaction")) {
							unreverseCCCnt++;
						} else if (arTr.gets("Desc").equalsIgnoreCase("Reverse - Cash Receipt")) {
							reverseCashReceipt = true; 
							reverseCashReceiptCnt++;
						} else if (arTr.gets("Desc").equalsIgnoreCase("Unreverse"))
							unreverseCashReceiptCnt++;
					}
				}		
				
				//Check if account has at least one NSF that is not reversed OR if there is at least one Credit Card Reversal Transaction and there is at least one Credit Card Reversal that is not reversed
				if((reverseCC && reverseCCCnt > unreverseCCCnt) || (nsf && nsfCnt > nsfReversalCnt) || (reverseCashReceipt && reverseCashReceiptCnt > unreverseCashReceiptCnt)) {
					ModelBean policy = Policy.searchByPolicyNumber(data, accountNumber, true, false)[0];
					String policyNumber = policy.getBean("BasicPolicy").gets("PolicyNumber"); 
					ModelBean[] tasks = WorkflowRenderer.getTasks(data, "ReviewReminderNoticeTask", policy);

					for (ModelBean arTran: arTrans) {
						if(arTran.gets("TypeCd").equalsIgnoreCase("ManualReversal") && (arTran.gets("Desc").equalsIgnoreCase("NSF") || arTran.gets("Desc").equalsIgnoreCase("Reverse Credit Card Transaction") || arTran.gets("Desc").equalsIgnoreCase("Reverse - Cash Receipt")) ) {
							StringDate arReceiptDt = arTran.getDate("ARReceiptDt"); 
							String reference = arTran.gets("Reference");
							boolean isTaskExisting = false; 
							for(ModelBean task : tasks ) {
								String text = task.gets("Text"); 

								//Check if task is already created for this Manual Reversal Transaction
								if(text.contains(reference))
									isTaskExisting = true; 
							}

							if(!isTaskExisting) {
								//Create Reminder Notice Task for this Manual Reversal Transaction
								ModelBean remindertask = WorkflowRenderer.createTask(data, "ReviewReminderNoticeTask", policy, new StringDate());
								remindertask.setValue("Text", "Generate Reminder Notice Correspondence for " + reference); 

								Log.debug("A ReviewReminderNoticeTask was created for policy");
								Log.debug(policyNumber);
								
								// Save Task
								Task.insertTask(data, remindertask);
								
								ModelBean arPayPlan = account.getBean("ARPayPlan"); 
								String payPlanCd = arPayPlan.gets("PayPlanCd");
						       	StringDate todayStringDt = new StringDate(DateRenderer.getDate());
						       	ModelBean user = ServiceContext.getServiceContext().getUser();
								String securityId = SecurityManager.getSecurityManager().generateSecurityId(user);
								String userId = user.gets("LoginId");
						        String sessionId = "";
						        String conversationId = "";
						        String submissionNumber = "";
								String logKey = "";
							
								if( StringRenderer.in(payPlanCd,"Direct Bill Full Pay,Direct Bill 2 Pay,Direct Bill 4 Pay") || (achOrCheckCnt > 0 )) {
									//generate ach-check-returned-payment-reminder-notice-insured-copy correspondence
									ModelBean output = DCRenderer.addCorrespondence(data, policy, "ACHorCheckReturnedPaymentReminderNotice", "UWPolicy", submissionNumber, userId, todayStringDt, logKey);
								    output.setValue("AddUser","System");
								    output.setValue("AddDt",todayDt);
								    output.setValue("AddTm","");
								    DCRenderer.processOutput(data,userId,securityId, sessionId, conversationId,policy,output,todayStringDt,"");
								}
								else if( StringRenderer.in(payPlanCd,"Automated Full Pay,Automated 2 Pay,Automated 4 Pay,Automated 12 Pay")) {
									//generate automated-returned-payment-reminder-notice-insured-copy correspondence
									ModelBean output = DCRenderer.addCorrespondence(data, policy, "AutomatedReturnedPaymentReminderNotice", "UWPolicy", submissionNumber, userId, todayStringDt, logKey);
								    output.setValue("AddUser","System");
								    output.setValue("AddDt",todayDt);
								    output.setValue("AddTm","");
								    DCRenderer.processOutput(data,userId,securityId, sessionId, conversationId,policy,output,todayStringDt,"");
								}
								
								//Complete task afterwards
								WorkflowRenderer.completeTask(data, remindertask);
							}
						}
					}
				}
			}

			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Data repository selection criteria. To look up any InProcess, NewBusiness
	 * Quote having EffectiveDt older than the runDt - ExpireDays (which is
	 * configured in the job.xml)
	 * 
	 * @return MBPLookupSpec The lookup specification object containing the fields
	 *         to be used for data selection
	 * @throws MBPException when an error occurs
	 */
	public MBPLookupSpec getLookupSpec() throws MBPException {
		try {
			StringDate runDt = getRunDate();
			StringDate startDt = runDt.advanceDate(runDt, -7);
			
			return new MBPLookupSpec(
					new String[] {"StatusCd"}, 
					new String[] {"IN"},
					new String[] {"Sent,Reversed"});
		} catch (Exception e) {
			throw new MBPException(e);
		}
	}

	/**
	 * Find out the run date of the current batch process
	 * 
	 * @return StringDate The date of the current batch run is set to.
	 * @throws Exception when an error occurs
	 */
	public StringDate getRunDate() throws Exception {
		// Find if the run date has been passed in
		if (this.getParam("RunDt", "").equals("")) {
			return DateTools.getStringDate();
		} else {
			return new StringDate(this.getParam("RunDt"));
		}
	}

	protected ModelBean callService(ModelBean rq) throws Exception {

		// Call the service
		return IBIZXmlController.processRequest(rq, true);
	}

	/**
	 * Set up the processor before the batch job is executed. <BR>
	 * <BR>
	 * Instance variables runDt, myName, and templateId in base TaskReminderProcess
	 * are private, so must get my own.
	 *
	 * @throws Exception when an error occurs
	 */
	public void initialCall() throws MBPException {
		try {
			// Set the error reporting to use the new JobProgress class instead of
			// immediately
			// reporting to the Processor table.
			this.setReportErrorsImmediately(false);
		} catch (Exception e) {
			// If the job has been entered into the process table, go ahead and mark it as
			// an error
			try {
				jobActionProgress.addExceptionError(e.getMessage());
				jobActionProgress.updateJobActionStatus("Error");
			} catch (Exception ec) {
				throw new MBPException(ec);
			}

			throw new MBPException(e);
		}
	}

	/**
	 * Update the process control manager: this process is now complete.
	 * 
	 * @param beanData The data repository connection
	 * @throws MBPException when an error occurs
	 */
	public void finalCall(JDBCData beanData) throws MBPException {
		try {
			// Make an entry in the processor table to show that we have finished.
			// This is for information purposes only.
			jobActionProgress.updateJobActionStatus("Complete");
		} catch (SQLException sqle) {
			// Do not throw an exception if its because the result of the search is empty
			if (sqle.getErrorCode() != 0)
				throw new MBPException(sqle);
		} catch (Exception e) {
			throw new MBPException(e);
		}
	}
}