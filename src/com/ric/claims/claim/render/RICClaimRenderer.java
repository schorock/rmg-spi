package com.ric.claims.claim.render;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.iscs.claims.claim.render.ClaimRenderer;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.tech.form.field.FormFieldRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;
import com.iscs.uw.policy.Policy;
import com.iscs.workflow.Task;
import com.iscs.workflow.render.WorkflowRenderer;
import com.ric.claims.claim.OpenWork;

import net.inov.mda.MDAOption;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

public class RICClaimRenderer extends ClaimRenderer {
	
	public RICClaimRenderer() {
		super();
	}

	public String getContextVariableName() {
		return "RICClaimRenderer";
	}
	
	/**
     * Create Claims Open Work object
     * @param selectionType User or Provider
     * @param userProviderList relevant ProviderRefs
     * @param selectedProvider selected Provider (if selectionType is Provider)
     * @param data
     * @param user
     * @param taskOwnerList Task Owner list relevant to selected User/Provider
     * @param todayDt
     * @return OpenWork object
     * @throws Exception
     */
	public static OpenWork createOpenWork(String selectionType, String[] userProviderList, String selectedProvider, JDBCData data, ModelBean user, String taskOwnerList, StringDate todayDt)
	throws Exception {
		return new OpenWork(selectionType, userProviderList, selectedProvider, data, user, taskOwnerList, todayDt);
	} 
	
	/**
	 * Creates and updates SIU task depending on the status.  
	 * @param data
	 * @param claim
	 * @throws Exception
	 */
	public static void createAndUpdateSIUTask( JDBCData data, ModelBean claim ) throws Exception {
		if( StringRenderer.isFalse( claim.gets("InSIUInd") ) )
			return;
		
		try{
			ModelBean task = Task.getTask(data, "ClaimMarkedAsSIUTask", claim, "Open");
			
			if( task == null && claim.gets("SIUStatus").equals("Open") ) {
				// Create SIU group Task
				task = Task.createTask(data, "ClaimMarkedAsSIUTask", new ModelBean[] {claim}, null, null,  new StringDate(), null, "");
				Task.insertTask(data, task);
				
				// Create ClaimsMgr group Task
				task = Task.createTask(data, "ClaimMarkedAsSIUTask-ClaimsMgr", new ModelBean[] {claim}, null, null,  new StringDate(), null, "");
				Task.insertTask(data, task);
				
				return;
			}
			
			if( task != null && claim.gets("SIUStatus").equals("Closed") ) {
				// Complete SIU Group task
				WorkflowRenderer.completeTask(data, task);
				
				//Complete ClaimsMgr Group task
				task = Task.getTask(data, "ClaimMarkedAsSIUTask-ClaimsMgr", claim, "Open");
				WorkflowRenderer.completeTask(data, task);
			}
			
			return;
		} catch( Exception e ){
			return;
		}
	}
	
	/**
	 * Create Severity Level Tasks
	 * @param data
	 * @param claim
	 * @throws Exception
	 */
	public static void createSeverityLevelTasks( JDBCData data, ModelBean claim ) throws Exception {
		try{
			List<String> severityLevelTaskTemplateIds = new ArrayList<>(
				    Arrays.asList(
				    		"S1SeverityLevelGeneratedTask", "S1SeverityLevelGeneratedTask-ClaimsMgr",
				    		"P1SeverityLevelGeneratedTask", "P1SeverityLevelGeneratedTask-ClaimsMgr", "P1SeverityLevelGeneratedTask-PropertyInternalAdjuster",
				    		"P2SeverityLevelGeneratedTask", "P2SeverityLevelGeneratedTask-ClaimsMgr", "P2SeverityLevelGeneratedTask-PropertyInternalAdjuster",
				    		"PLSeverityLevelGeneratedTask", "PLSeverityLevelGeneratedTask-ClaimsMgr", "PLSeverityLevelGeneratedTask-PropertyInternalAdjuster",
				    		"P3SeverityLevelGeneratedTask", "P3SeverityLevelGeneratedTask-ClaimsMgr", "P3SeverityLevelGeneratedTask-PropertyInternalAdjuster",
				    		"P4SeverityLevelGeneratedTask", "P4SeverityLevelGeneratedTask-ClaimsMgr", "P4SeverityLevelGeneratedTask-PropertyInternalAdjuster",
				    		"P5SeverityLevelGeneratedTask", "P5SeverityLevelGeneratedTask-ClaimsMgr", "P5SeverityLevelGeneratedTask-PropertyInternalAdjuster", 
				    		"UndeterminedSeverityLevelGeneratedTask", "UndeterminedSeverityLevelGeneratedTask-ClaimsMgr", "UndeterminedSeverityLevelGeneratedTask-PropertyInternalAdjuster"));
			String severityLevel = !claim.gets("OverriddenSeverityLevel").isEmpty() ? claim.gets("OverriddenSeverityLevel") : claim.gets("SeverityLevel");
			String[] taskTemplateIds = {};
			
			if( severityLevel.isEmpty() )
				severityLevel = "Undetermined";
			
			if( severityLevel.equals("S1") ) {
				taskTemplateIds = new String[]{"S1SeverityLevelGeneratedTask", "S1SeverityLevelGeneratedTask-ClaimsMgr"};
			} else if( severityLevel.equals("P1") ) {
				taskTemplateIds = new String[]{"P1SeverityLevelGeneratedTask", "P1SeverityLevelGeneratedTask-ClaimsMgr", "P1SeverityLevelGeneratedTask-PropertyInternalAdjuster"};
			} else if( severityLevel.equals("P2") ) {
				taskTemplateIds = new String[]{"P2SeverityLevelGeneratedTask", "P2SeverityLevelGeneratedTask-ClaimsMgr", "P2SeverityLevelGeneratedTask-PropertyInternalAdjuster"};
			} else if( severityLevel.equals("PL") ) {
				taskTemplateIds = new String[]{"PLSeverityLevelGeneratedTask", "PLSeverityLevelGeneratedTask-ClaimsMgr", "PLSeverityLevelGeneratedTask-PropertyInternalAdjuster"};
			} else if( severityLevel.equals("P3") ) {
				taskTemplateIds = new String[]{"P3SeverityLevelGeneratedTask", "P3SeverityLevelGeneratedTask-ClaimsMgr", "P3SeverityLevelGeneratedTask-PropertyInternalAdjuster"};
			} else if( severityLevel.equals("P4") ) {
				taskTemplateIds = new String[]{"P4SeverityLevelGeneratedTask", "P4SeverityLevelGeneratedTask-ClaimsMgr", "P4SeverityLevelGeneratedTask-PropertyInternalAdjuster"};
			} else if( severityLevel.equals("P5") ) {
				taskTemplateIds = new String[]{"P5SeverityLevelGeneratedTask", "P5SeverityLevelGeneratedTask-ClaimsMgr", "P5SeverityLevelGeneratedTask-PropertyInternalAdjuster"};
			} else if( severityLevel.equals("Undetermined") ) {
				taskTemplateIds = new String[]{"UndeterminedSeverityLevelGeneratedTask", "UndeterminedSeverityLevelGeneratedTask-ClaimsMgr", "UndeterminedSeverityLevelGeneratedTask-PropertyInternalAdjuster"};
			}
			
			//Add new tasks
			for( String taskTemplateId : taskTemplateIds ) {
				severityLevelTaskTemplateIds.remove(taskTemplateId);
				ModelBean newTask = Task.getTask(data, taskTemplateId, claim, "Open");
						
				if( newTask == null ) {
					newTask = Task.createTask(data, taskTemplateId, new ModelBean[] {claim}, null, null,  new StringDate(), null, "");
					Task.insertTask(data, newTask);
				}
			}
			
			//Close out irrelevant tasks
			ModelBean[] allTasks = Task.fetchTasks("Claim" + claim.getSystemId(), data);
			
			for( ModelBean task : allTasks ) {
				String taskTemplateId = task.gets("TemplateId");
				
				if( severityLevelTaskTemplateIds.contains(taskTemplateId) ) {
					WorkflowRenderer.closeTask(data, task);
				}
			}
			
			return;
		} catch( Exception e ){
			return;
		}
	}
	
	/** Create tasks SeverityLevelClaimTask based on Severity Level for Claims Manager Group and adjusters attached to the claim.
	 * @param data JDBCData Connection Object
	 * @param claim Claim ModelBean
	 */
	public static void createSeverityLevelTasksForAdjusterAndClaimsMgr( JDBCData data, ModelBean claim ) throws Exception {
		if( claim.gets("SeverityLevel").isEmpty() && claim.gets("OverriddenSeverityLevel").isEmpty() )
			return;
		
		try{
			ArrayList<String> adjusterList = new ArrayList<String>();	
			ModelBean[] claimants = ClaimRenderer.getOpenClaimants(claim);
			ModelBean[] claimTasks = Task.fetchTasks("Claim", claim.getSystemId(), "Open", data);
			
			for( ModelBean claimant : claimants ){
				ModelBean[] adjusters = ClaimRenderer.getProviders(data, claimant, "");
				
				for( ModelBean adjuster : adjusters ){
					if( !StringRenderer.contains(adjuster.gets("ProviderNumber"), adjusterList ) )
						StringRenderer.addToList(adjusterList, adjuster.gets("ProviderNumber"));
				}
			}
		
			for( String providerNum : adjusterList ){
				Boolean hasClaimTask = false;
				
				for( ModelBean claimTask : claimTasks ){
					if( claimTask.gets("TemplateId").equals("SeverityLevelAdjusterClaimTask") && claimTask.gets("OriginalOwner").equals(providerNum) )
						hasClaimTask = true;
				}
				
				if( !hasClaimTask ){
					ModelBean adjusterTask = Task.createTask(data, "SeverityLevelAdjusterClaimTask", new ModelBean[] {claim}, null, null,  new StringDate(), null, "");
					adjusterTask.setValue("CurrentOwner", providerNum );
					adjusterTask.setValue("OriginalOwner", providerNum );
					Task.insertTask(data, adjusterTask);
				}
			}
			
			ModelBean claimsMgrTask = Task.getTask(data, "SeverityLevelClaimsMgrClaimTask", claim, "Open");

			if( claimsMgrTask != null )
				return;

			claimsMgrTask = Task.createTask(data, "SeverityLevelClaimsMgrClaimTask", new ModelBean[] {claim}, null, null,  new StringDate(), null, "");

			// Save Task
			Task.insertTask(data, claimsMgrTask);

			return;
		} catch( Exception e ){
			return;
		}
	}

	/** Create task UWRiskReview from Claims if Claim.UWRiskReview is true.
	 * @param data JDBCData Connection Object
	 * @param claim Claim ModelBean
	 */
	public static void createUWRiskReviewTask( JDBCData data, ModelBean claim ) throws Exception {
		if( StringRenderer.isFalse( claim.gets("UWRiskReview") ) )
			return;
		
		try{
			String policyRef = claim.getBean("ClaimPolicyInfo").gets("PolicyRef");
			ModelBean policy = Policy.getPolicyBySystemId(data, Integer.parseInt(policyRef));
			ModelBean task = Task.getTask(data, "UWRiskReviewTask", policy, "Open");

			if( task != null )
				return;

			task = Task.createTask(data, "UWRiskReviewTask", new ModelBean[] {policy}, null, null,  new StringDate(), null, "");
			task.setValue("Description", claim.gets("UWRiskReviewExplanation"));
			task.setValue("Text", claim.gets("UWRiskReviewExplanation"));

			// Save Task
			Task.insertTask(data, task);

			return;
		} catch( Exception e ){
			return;
		}
	}
	
	
	/** Get Risk Select List
	 * @param fieldId Field Identifier
	 * @param defaultValue Select List Default Value
	 * @param claim Claim ModelBean Containing the Risks
	 * @param meta Parameters
	 * @return String HTML Select Tag
	 * @throws Exception if an Unexpected Error Occurs
	 */
	public static String riskSelectField( String fieldId, String defaultValue, ModelBean claim, String meta )
	throws Exception {
		try {
			// Build Option List
			List<MDAOption> array = new ArrayList<MDAOption>();
			
			// Get Vehicle Involved Indicator
			String vehicleInvolvedInd = claim.gets("VehicleInvolvedInd");
				
			ModelBean claimPolicyInfo = claim.getBean("ClaimPolicyInfo");	
			ModelBean[] policyRiskArray = claimPolicyInfo.getBean("PolicyRisks").getBeansSorted("PolicyRisk","Ascending","RiskNumber");
			for( ModelBean policyRisk : policyRiskArray ) {
				
				String vehicleInd = policyRisk.gets("VehicleInd");
				if( StringTools.isTrue(vehicleInvolvedInd) == StringTools.isTrue(vehicleInd) ) {
					String description = policyRisk.gets("Description"); 
					if(description.equals("")) {
						ModelBean riskAddr = policyRisk.getBean("Addr");
						description = riskAddr.gets("Addr1") + riskAddr.gets("Addr2") + " " + riskAddr.gets("City") + " " +  riskAddr.gets("StateProvCd") + " " +  riskAddr.gets("PostalCode"); 
						policyRisk.setValue("Description", description);
					}
					MDAOption mdaOption = new MDAOption();
					mdaOption.setValue(policyRisk.getId());
					mdaOption.setLabel(policyRisk.gets("Description"));
					array.add(mdaOption);
				}
			}
			
			MDAOption[] mdaOptions = array.toArray(new MDAOption[array.size()]);
			return FormFieldRenderer.renderSelect(fieldId, defaultValue, mdaOptions, "SelectList", meta);
		}
		catch( Exception e ) {
			e.printStackTrace();
			Log.error(e);
			return "";
		}
	}
}
