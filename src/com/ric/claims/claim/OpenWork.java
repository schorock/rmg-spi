package com.ric.claims.claim;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.iscs.claims.claim.Claim;
import com.iscs.claims.claim.Claimant;
import com.iscs.claims.transaction.TransactionTask;
import com.iscs.common.admin.user.ContainerSecurityResult;
import com.iscs.common.admin.user.User;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.StringTools;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

public class OpenWork extends com.iscs.claims.claim.OpenWork {
	
	public OpenWork(String selectionType, String[] userProviderList, String selectedProvider, JDBCData data,
			ModelBean user, String taskOwnerList, StringDate todayDt) throws Exception {
		super(selectionType, userProviderList, selectedProvider, data, user, taskOwnerList, todayDt);
		// TODO Auto-generated constructor stub
	}

	/** Get a list of open work for adjuster
	 * 
	 * @param data JDBCData The data connection
	 * @param optionSelected String The selected option
	 * @param user ModelBean The logged on user info bean
	 * @param selectedUser ModelBean The selected user info bean
	 * @param claimList ModelBean List of claim mini beans
	 * @param providerArray String List of provider system ids
	 * @param todayDt StringDate today's date
	 * @param sortBy String How to sort 
	 * @param sortOrder String The sort order
	 * @param userTaskOwnerList String List of task owner codes for the selected user
	 * @param providerTaskOwnerList String List of task owner codes for the selected provider
	 * @return list of open work for adjusters
	 * @throws Exception when an error occurs
	 */
	public ModelBean[] getAdjusterOpenWorkList(JDBCData data, String optionSelected, ModelBean user, ModelBean selectedUser, ModelBean[] claimList, String[] providerArray, StringDate todayDt, String sortBy, String sortOrder, String userTaskOwnerList, String providerTaskOwnerList) throws Exception {
		List<ModelBean> sortList = new ArrayList<ModelBean>();				
		for( ModelBean claim:claimList){
			//Get container security
			ContainerSecurityResult containerSecurity = User.getContainerSecurity(data, claim, user, todayDt);			
			for( String providerSysId:providerArray){
				ModelBean[] claimants = Claimant.getActiveClaimants(claim);
				for( ModelBean claimant:claimants){
					ModelBean[] assignedProviders = claimant.getBeans("AssignedProvider");
					for( ModelBean provider:assignedProviders) {
						if( provider.getValueInt("ProviderRef") > 0 && provider.gets("ProviderRef").equals(providerSysId) ){
							sortList = addOpenWork(data, containerSecurity, optionSelected, claim, claimant, user, selectedUser, provider.getId(), "", todayDt, sortList, userTaskOwnerList, providerTaskOwnerList);							
						}
					}
				}
			}			
		}
		if( !sortBy.equals("") ){
			return sortList(sortList, sortBy, sortOrder);
		} else {
			return sortList.toArray(new ModelBean[sortList.size()]); 
		}			
	}	


	/** Get a list of open work for subrogation adjusters
	 * 
	 * @param data JDBCData The data connection
	 * @param optionSelected String The selected option
	 * @param user ModelBean The logged on user info bean
	 * @param selectedUser ModelBean The selected user info bean
	 * @param claimList ModelBean List of claim mini beans
	 * @param providerArray String List of provider system ids
	 * @param todayDt StringDate today's date
	 * @param sortBy String How to sort 
	 * @param sortOrder String The sort order
	 * @param userTaskOwnerList String List of task owner codes for the selected user
	 * @param providerTaskOwnerList String List of task owner codes for the selected provider
	 * @return list of open work for subrogation adjusters
	 * @throws Exception when an error occurs
	 */
	public ModelBean[] getSubrogationAdjusterOpenWorkList(JDBCData data, String optionSelected, ModelBean user, ModelBean selectedUser, ModelBean[] claimList, String[] providerArray, StringDate todayDt, String sortBy, String sortOrder, String userTaskOwnerList, String providerTaskOwnerList) throws Exception {
		List<ModelBean> sortList = new ArrayList<ModelBean>();		
		for( ModelBean claim:claimList){
			//Get container security
			ContainerSecurityResult containerSecurity = User.getContainerSecurity(data, claim, user, todayDt);
			ModelBean[] claimants = Claimant.getActiveClaimants(claim);
			if( claimants.length > 0){
				ModelBean[] subrogationList = claim.findBeansByFieldValue("Subrogation", "StatusCd", "Active");
				for( ModelBean subrogation:subrogationList){									
					for( String providerSysId:providerArray){
						if( subrogation.gets("SubrogationProviderRef").equals(providerSysId) ){
							sortList = addOpenWork(data, containerSecurity, optionSelected, claim, claimants[0], user, selectedUser, "", subrogation.getId(), todayDt, sortList, userTaskOwnerList, providerTaskOwnerList);
						}						
					}
				}
			}						
		}
		if( !sortBy.equals("") ){
			return sortList(sortList, sortBy, sortOrder);
		} else {
			return sortList.toArray(new ModelBean[sortList.size()]); 
		}			
	}

	/** Get a list of open work for Examiner/SIU adjusters
	 * 
	 * @param data JDBCData The data connection
	 * @param optionSelected String The selected option
	 * @param user ModelBean The logged on user info bean
	 * @param selectedUser ModelBean The selected user info bean
	 * @param claimList ModelBean List of claim mini beans
	 * @param typeCd String The adjuster type code
	 * @param todayDt StringDate today's date
	 * @param sortBy String How to sort 
	 * @param sortOrder String The sort order
	 * @param userTaskOwnerList String List of task owner codes for the selected user
	 * @param providerTaskOwnerList String List of task owner codes for the selected provider
	 * @return list of open work for examiner/SIU adjusters
	 * @throws Exception when an error occurs
	 */
	public ModelBean[] getExaminerOpenWorkList(JDBCData data, String optionSelected, ModelBean user, ModelBean selectedUser, ModelBean[] claimList, String typeCd, StringDate todayDt, String sortBy, String sortOrder,  String userTaskOwnerList, String providerTaskOwnerList) throws Exception {
		List<ModelBean> sortList = new ArrayList<ModelBean>();				
		for( ModelBean claim:claimList){
			//Get container security
			ContainerSecurityResult containerSecurity = User.getContainerSecurity(data, claim, user, todayDt);
			ModelBean[] claimants = Claimant.getActiveClaimants(claim);
			boolean addInd = false;	
			if( claimants.length > 0){					
				if(typeCd.equals("Examiner") || typeCd.equals("SIU") || typeCd.equals("Transaction")){
					addInd = true;
				}
				if( addInd ){
					sortList = addOpenWork(data, containerSecurity, optionSelected, claim, claimants[0], user, selectedUser, "", "", todayDt, sortList, userTaskOwnerList, providerTaskOwnerList);
				}

			}						
		}
		if( !sortBy.equals("") ){
			return sortList(sortList, sortBy, sortOrder);
		} else {
			return sortList.toArray(new ModelBean[sortList.size()]); 
		}		
	}
	
	/** Add open work record to list
	 * 
	 * @param data JDBCData The data connection
	 * @param containerSecurity ContainerSecurity
	 * @param optionSelected String The selected option
	 * @param claim ModelBean The claim bean
	 * @param claimant ModelBean The claimant bean
	 * @param user ModelBean The logged onuser info bean
	 * @param selectedUser ModelBean The selected user info bean	
	 * @param assignedProvider String The id of the AssignedProvider bean
	 * @param subrogationId String The id of the Subrogation bean
	 * @param todayDt StringDate today's date
	 * @param sortList List The open work list of adjusters/examiners
	 * @param userTaskOwnerList String List of task owner codes for the selected user
	 * @param providerTaskOwnerList String List of task owner codes for the selected provider
	 * @return List of open work records
	 * @throws Exception when an error occurs
	 */
	public List<ModelBean> addOpenWork(JDBCData data, ContainerSecurityResult containerSecurity, String optionSelected, ModelBean claim, ModelBean claimant, ModelBean user, ModelBean selectedUser, String assignedProvider, String subrogationId, StringDate todayDt, List<ModelBean> sortList, String userTaskOwnerList, String providerTaskOwnerList) throws Exception {
		String claimSystemId = claim.getSystemId();
		
		//Check security based on logged on user credentials
		
		// if no container security don't add the OpenWork record
		if( !containerSecurity.canDisplay() ){
			return sortList;
		}		

		//Check branch security
		boolean canInquire = Claim.canInquire(claim, user, todayDt);	
		boolean canEdit = Claim.canEdit(claim, user, todayDt);

		//Check provider security		
		boolean canEditClaimant = Claimant.canEdit(claim, null, user, todayDt);

		ModelBean openWork = new ModelBean("OpenWork");
		if( containerSecurity.canInquire() && canInquire){		
			openWork.setValue("ShowClaimLinkInd", "Yes");
		} else {	
			openWork.setValue("ShowClaimLinkInd", "No");
		}
		if( canEdit ) {
			openWork.setValue("CanEditClaimInd", "Yes");
			if( canEditClaimant){
				openWork.setValue("CanEditClaimantInd", "Yes");
			} else {				
				openWork.setValue("CanEditClaimantInd", "No");
			}
		} else {
			openWork.setValue("CanEditClaimInd", "No");
			openWork.setValue("CanEditClaimantInd", "No");
		}
		ModelBean policyInfo = claim.getBean("ClaimPolicyInfo");
		String customerRef = claim.gets("CustomerRef");
		openWork.setValue("ClaimNumber", claim.gets("ClaimNumber"));
		openWork.setValue("PolicyNumber", policyInfo.gets("PolicyNumber"));
		openWork.setValue("CustomerRef", customerRef);
		openWork.setValue("ClaimantId", claimant.getId());
		String claimantName = claimant.getBeanByAlias("ClaimantName").gets("CommercialName");
		openWork.setValue("ClaimantName", claimantName);
		openWork.setValue("SeverityLevel", claim.gets("OverriddenSeverityLevel").isEmpty() ? claim.gets("SeverityLevel") : claim.gets("OverriddenSeverityLevel") );
		openWork.setValue("FraudScore", claim.gets("FraudScore"));
		openWork.setValue("DOIComplaintInd", claim.gets("DOIComplaintInd"));
		openWork.setValue("InSIUInd", claim.gets("InSIUInd"));
		ModelBean transaction = null;
		if( claim.gets("TypeCd").equals("Transaction") ){
			transaction = claim;
		} else {		
			transaction = Claim.getPendingTransaction(data, claim, true);
		}
		openWork.setValue("ClaimRef", claim.getSystemId());
		openWork.setValue("ClaimStatusCd", claim.gets("StatusCd"));
		if( transaction == null){			
			openWork.setValue("IsTransactionInd", "No");
			openWork.setValue("HasLockInd", "Yes");
			openWork.setValue("HasLockUser", user.gets("LoginId"));
		} else {
			openWork.setValue("ClaimRef", claim.getSystemId());
			openWork.setValue("ClaimTransactionRef", transaction.getSystemId());
			openWork.setValue("IsTransactionInd", "Yes");	
			String[] taskLocks = TransactionTask.hasTaskLock(data, transaction, user, todayDt);
			if( StringTools.isTrue(taskLocks[0]) ){
				openWork.setValue("HasLockInd", "Yes");
			} else {
				openWork.setValue("HasLockInd", "No");
			}					
			openWork.setValue("HasLockUser", taskLocks[1]);		
			openWork.setValue("ClaimStatusCd", transaction.gets("StatusCd"));
		}
		int days = 0;
		StringDate activityDt = Claim.getLastActivityDt(data, claim);
		if( activityDt !=null){
			days = (int) DateTools.diffDays(activityDt, todayDt);						
		}

		openWork.setValue("DaysSinceLastActivity", String.valueOf(days));
		StringDate reportedDt = claim.getValueDate("ReportedDt");
		days = (int) DateTools.diffDays(reportedDt, todayDt);
		openWork.setValue("DaysOpen", String.valueOf(days));
		
		String taskCnt;
		if(optionSelected.equals("User") ){
			taskCnt = Claim.getOpenTaskCount(data, claim, transaction, userTaskOwnerList);
    	} else {
    		taskCnt = Claim.getOpenTaskCount(data, claim, transaction, providerTaskOwnerList);
    	}
		String[] cnt = taskCnt.split("/");
		openWork.setValue("NumberOfMyOpenTasks", String.valueOf(cnt[0]));
		openWork.setValue("NumberOfAllOpenTasks", String.valueOf(cnt[1]));				

		ModelBean claimantSummaryInfo = claim.findBeanByFieldValue("ClaimantSummaryInfo", "ClaimantIdRef", claimant.getId());
		if( claimantSummaryInfo!=null){
			openWork.setValue("IndemnityPaymentInd", claimantSummaryInfo.gets("IndemnityPaymentInd"));
			openWork.setValue("DeniedFeatureInd", claimantSummaryInfo.gets("DeniedFeatureInd"));
			openWork.setValue("SuitFiledInd", claimantSummaryInfo.gets("SuitFiledInd"));
		}			
		if( !assignedProvider.equals("")){
			openWork.setValue("ProviderId", assignedProvider);
		}
		if( !subrogationId.equals("")){
			openWork.setValue("SubrogationId", subrogationId);
		}		

		sortList.add(openWork);


		return sortList;
	}
	
	/** Sort the open work list
	 * 
	 * @param unsortedList ModelBean List of open work Model bean
	 * @param sortBy String What to sort by
	 * @param sortOrder String The sort order, either Ascending or Descending
	 * @return Sorted open work list
	 * @throws Exception when an error occurs
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static ModelBean[] sortList(List<ModelBean> unsortedList, String sortBy, String sortOrder) throws Exception {
		final String sortString = sortBy;
		final String orderBy = sortOrder;
		Collections.sort(unsortedList, new Comparator() {
			public int compare(Object o1, Object o2) throws ClassCastException {
				try {
					ModelBean bean1 = (ModelBean) o1;
					ModelBean bean2 = (ModelBean) o2;
					int comparisonValue = 0;
					if( sortString.equals("ClaimNumber")){
						if( orderBy.equals("Ascending") )
							comparisonValue = bean1.gets("ClaimNumber").compareTo(bean2.gets("ClaimNumber"));
						else
							comparisonValue = bean2.gets("ClaimNumber").compareTo(bean1.gets("ClaimNumber"));
					} else if( sortString.equals("PolicyNumber")){
						if( orderBy.equals("Ascending") )
							comparisonValue = bean1.gets("PolicyNumber").compareTo(bean2.gets("PolicyNumber"));
						else
							comparisonValue = bean2.gets("PolicyNumber").compareTo(bean1.gets("PolicyNumber"));
					} else if( sortString.equals("ClaimantName")){
						if( orderBy.equals("Ascending") )
							comparisonValue = bean1.gets("ClaimantName").compareTo(bean2.gets("ClaimantName"));
						else
							comparisonValue = bean2.gets("ClaimantName").compareTo(bean1.gets("ClaimantName"));
					} else if( sortString.equals("SeverityLevel")){
						String[] severityLevels = {"","P5","P4","P3","C2","C1","PL","P2","P1","S1"};
						Integer bean1SeverityLevel = 0, bean2SeverityLevel = 0;
						
						for( int i = 0; i < severityLevels.length; i++ ){
							if( bean1.gets("SeverityLevel").equals( severityLevels[i] ) )
								bean1SeverityLevel = i;
							
							if( bean2.gets("SeverityLevel").equals( severityLevels[i] ) )
								bean2SeverityLevel = i;
						}
						
						if( orderBy.equals("Ascending") )
							comparisonValue = bean1SeverityLevel.compareTo(bean2SeverityLevel);
						else 
							comparisonValue = bean2SeverityLevel.compareTo(bean1SeverityLevel);
					} else if( sortString.equals("DaysSinceLastActivity")){
						if( orderBy.equals("Ascending") )
							comparisonValue = bean1.getValueInt("DaysSinceLastActivity") - bean2.getValueInt("DaysSinceLastActivity");
						else
							comparisonValue = bean2.getValueInt("DaysSinceLastActivity") - bean1.getValueInt("DaysSinceLastActivity");
					} else if( sortString.equals("DaysOpen")){
						if( orderBy.equals("Ascending") )
							comparisonValue = bean1.getValueInt("DaysOpen") - bean2.getValueInt("DaysOpen");
						else
							comparisonValue = bean2.getValueInt("DaysOpen") - bean1.getValueInt("DaysOpen");
					} else if( sortString.equals("NumberOfMyOpenTasks")){
						if( orderBy.equals("Ascending") )
							if( bean1.getValueInt("NumberOfMyOpenTasks")  == bean2.getValueInt("NumberOfMyOpenTasks") ){
								comparisonValue = bean1.getValueInt("NumberOfAllOpenTasks") - bean2.getValueInt("NumberOfAllOpenTasks");
							} else {
								comparisonValue = bean1.getValueInt("NumberOfMyOpenTasks") - bean2.getValueInt("NumberOfMyOpenTasks");
							}
						else
							if( bean1.getValueInt("NumberOfMyOpenTasks")  == bean2.getValueInt("NumberOfMyOpenTasks") ){
								comparisonValue = bean2.getValueInt("NumberOfAllOpenTasks") - bean1.getValueInt("NumberOfAllOpenTasks");
							} else {
								comparisonValue = bean2.getValueInt("NumberOfMyOpenTasks") - bean1.getValueInt("NumberOfMyOpenTasks");
							}
					} else if( sortString.equals("IndemnityPaymentInd")){
						if( orderBy.equals("Ascending") )
							comparisonValue = bean1.gets("IndemnityPaymentInd").compareTo(bean2.gets("IndemnityPaymentInd"));
						else
							comparisonValue = bean2.gets("IndemnityPaymentInd").compareTo(bean1.gets("IndemnityPaymentInd"));
					} else if( sortString.equals("DeniedFeatureInd")){
						if( orderBy.equals("Ascending") )
							comparisonValue = bean1.gets("DeniedFeatureInd").compareTo(bean2.gets("DeniedFeatureInd"));
						else
							comparisonValue = bean2.gets("DeniedFeatureInd").compareTo(bean1.gets("DeniedFeatureInd"));
					} else if( sortString.equals("InSIUInd")){
						if( orderBy.equals("Ascending") )
							comparisonValue = bean1.gets("InSIUInd").compareTo(bean2.gets("InSIUInd"));
						else
							comparisonValue = bean2.gets("InSIUInd").compareTo(bean1.gets("InSIUInd"));
					} else if( sortString.equals("DOIComplaintInd")){
						if( orderBy.equals("Ascending") )
							comparisonValue = bean1.gets("DOIComplaintInd").compareTo(bean2.gets("DOIComplaintInd"));
						else
							comparisonValue = bean2.gets("DOIComplaintInd").compareTo(bean1.gets("DOIComplaintInd"));
					} else if( sortString.equals("SuitFiledInd")){
						if( orderBy.equals("Ascending") )
							comparisonValue = bean1.gets("SuitFiledInd").compareTo(bean2.gets("SuitFiledInd"));
						else
							comparisonValue = bean2.gets("SuitFiledInd").compareTo(bean1.gets("SuitFiledInd"));
					} else if( sortString.equals("FraudScore")){
						if( orderBy.equals("Ascending") )
							comparisonValue = bean1.getValueInt("FraudScore") - bean2.getValueInt("FraudScore");
						else
							comparisonValue = bean2.getValueInt("FraudScore") - bean1.getValueInt("FraudScore");
					}

					return comparisonValue;


				} catch (Exception e) {
					throw new ClassCastException("Error occurred trying to compare fields for sorting." + e.toString() );
				}	
			}
		});

		return unsortedList.toArray(new ModelBean[unsortedList.size()]);

	}
}
