package com.ric.claims.common.product.model.template.ric.dwellingproperty.handler;

import java.util.ArrayList;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.math.Money;
import net.inov.tec.web.webpath.FrontController;

import com.iscs.claims.interfaces.PolicyInfoRequest;
import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.insurance.product.ProductMaster;
import com.iscs.insurance.product.ProductSetup;
import com.iscscl.claims.common.product.ClaimPolicyReinsuranceBuilder;
import com.iscscl.claims.common.product.personal.PolicyInfoBuilderBase;

/** Loads Policy Information Specific for the Claim
 * 
 * @author Debasish
 */
public class DwellingClaimPolicyInfoBuilder extends PolicyInfoBuilderBase implements RuleProcessor, PolicyInfoRequest {

	
	
	// Rule processor interface
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user, JDBCData data) 
	throws RuleException {	
		ModelBean errors = null;
		ArrayList<ModelBean> errorList = new ArrayList<ModelBean>();
		try{
			// Load the coverage information for the claim risk 
			Log.debug("Processing DwellingClaimPolicyInfoBuilder...");
			
			// Build the Errors ModelBean
			errors = new ModelBean("Errors");
			
			// Build Service Request ModelBean
            ModelBean serviceRq = buildRequest(ruleTemplate, bean);
            
            // Turn Off Debug Messages
            Log.setLoggingLevelError();
            
            // Send Webpath Request
			FrontController controller = new FrontController();
	        ModelBean response = controller.sendRequest(serviceRq, data);
	        ModelBean dtoPolicy = response.getBean("DTOPolicy");
	        
			// Load the Policy Information
			Log.debug("DTOPolicy: " + dtoPolicy.readableDoc());

			loadCoverageInfo(dtoPolicy, bean); 
			
			// Load any reinsurance information if needed also
			ClaimPolicyReinsuranceBuilder.buildClaimReinsurance(bean, errorList);
			
	        for( ModelBean error : errorList ) {
	            errors.addValue(error);
	        }            

	        Log.debug("Claim: " + bean.readableDoc());
							
		}
		catch(Exception e){	
			throw new RuleException(e);
		} finally {
			// Set Log Level back after processing the FrontController request
			Log.setLoggingLevelDefault();
		}
		return errors;		
	}
	
	/** Load the coverage(s) information that will apply to the claim 
	 * @param dtoPolicy The DTO Policy data Bean obtained from the external system
	 * @param claim The current claim container
	 * @throws RuleException when an unexpected error occurs 
	 */
	public void loadCoverageInfo(ModelBean dtoPolicy, ModelBean claim)
	throws RuleException {
		try {
			ArrayList<ModelBean> lineCoverages = new ArrayList<ModelBean>();
			ArrayList<ModelBean> riskCoverages = new ArrayList<ModelBean>();
			String lossCauseCd = claim.gets("LossCauseCd");
			ModelBean bldg = dtoPolicy.findBeanByFieldValue("DTOBuilding","Status","Active");
			String windHailDed = bldg.gets("WindHailDed");
			String allPerilDed = bldg.gets("AllPerilDed");
			// Get All Active Coverages
			ModelBean[] coverages = dtoPolicy.findBeansByFieldValue("DTOCoverage","Status","Active");
			for (ModelBean coverage : coverages) {
				
				// Get the Parent to the Coverage (i.e. Line, Risk)
				ModelBean parent = coverage.getParentBean();
				
				// Load Line Level Coverages
				if( parent.getBeanName().equals("DTOLine") ) {
					ModelBean limit = buildCoverage(coverage,lossCauseCd,windHailDed,allPerilDed);
					lineCoverages.add(limit);		
				}
				
				// Load Risk Level Coverages
				if( parent.getBeanName().equals("DTORisk") ) {
					ModelBean risk = claim.getBeanById(claim.gets("RiskIdRef"));
					if( risk != null ) {
						String riskNumber = risk.gets("RiskNumber");
						ModelBean building = parent.getBean("DTOBuilding");
						if( building.gets("BldgNumber").equals(riskNumber) ) {
							if(coverage.gets("CoverageCd").equals("EB")){																
								ModelBean limit = new ModelBean("ClaimPolicyLimit");
								limit.setValue("SourceCd","Policy");
								limit.setValue("OriginalCoverageCd", coverage.gets("CoverageCd"));
								limit.setValue("CoverageCd", "EB");
								limit.setValue("Description", "Equipment Breakdown");
								String ebLimit = coverage.findBeanByFieldValue("DTOLimit", "LimitCd", "Limit1").gets("Value");
								limit.setValue("Limit", ebLimit);
								limit.setValue("LimitDescription", "Policy Limit");
								ModelBean deductible = coverage.findBeanByFieldValue("DTODeductible", "DeductibleCd", "Deductible1");								
								if (deductible != null) {
									limit.setValue("Deductible", deductible.gets("Value"));
									limit.setValue("DeductibleDescription", "Policy Deductible");
									limit.setValue("DeductibleAllocationCd", coverage.gets("CoverageCd"));
									limit.setValue("DeductibleAllocationTypeCd", "Feature");
								}
								buildSubCoverage(coverage, limit);
								// Add the line code from the Line
								ModelBean line = BeanTools.getParentBean(coverage, "DTOLine");
								if (line != null)
									limit.setValue("LineCd", line.gets("LineCd"));							
								riskCoverages.add(limit);
							}else if(coverage.gets("CoverageCd").equals("DL2419")){
								ModelBean limit = new ModelBean("ClaimPolicyLimit");
								limit.setValue("SourceCd","Policy");
								limit.setValue("OriginalCoverageCd", coverage.gets("CoverageCd"));
								limit.setValue("Description", "DL 24 19.  Home Day Care Coverage");
								limit.setValue("CoverageCd", "DL2419");
							    limit.setValue("Limit","300000");
							    limit.setValue("LimitDescription", "Policy Limit");
								// Build any sub limits
								buildSubCoverage(coverage, limit);				
								// Add the line code from the Line
								ModelBean line = BeanTools.getParentBean(coverage, "DTOLine");
								if (line != null)
									limit.setValue("LineCd", line.gets("LineCd"));		
								
								riskCoverages.add(limit);
							}else if(coverage.gets("CoverageCd").equals("CovL")){
								ModelBean limit = new ModelBean("ClaimPolicyLimit");
								limit.setValue("SourceCd","Policy");
								limit.setValue("OriginalCoverageCd", coverage.gets("CoverageCd"));
								limit.setValue("Description", coverage.gets("Description"));
								limit.setValue("CoverageCd", "CovL");
								String lim1 = coverage.findBeanByFieldValue("DTOLimit", "LimitCd", "Limit1").gets("Value");	
							    limit.setValue("Limit",lim1);
							    limit.setValue("LimitDescription", "Policy Limit");
								// Build any sub limits
								buildSubCoverage(coverage, limit);				
								// Add the line code from the Line
								ModelBean line = BeanTools.getParentBean(coverage, "DTOLine");
								if (line != null)
									limit.setValue("LineCd", line.gets("LineCd"));		
								
								riskCoverages.add(limit);
							}else if(coverage.gets("CoverageCd").equals("ELFL")){
								ModelBean limit = new ModelBean("ClaimPolicyLimit");
										
								// Add the line code from the Line
								ModelBean line = BeanTools.getParentBean(coverage, "DTOLine");
								if (line != null)
									limit.setValue("LineCd", line.gets("LineCd"));		
								
								riskCoverages.add(limit);
								
								limit = new ModelBean("ClaimPolicyLimit");
								limit.setValue("SourceCd","Policy");
								limit.setValue("OriginalCoverageCd", coverage.gets("CoverageCd"));
								limit.setValue("Description", "ELFL. Escaped Liquid Fuel Liability");
								limit.setValue("CoverageCd", "ELFLLiability");
								ModelBean limLiability = coverage.findBeanByFieldValue("DTOLimit", "LimitCd", "Limit2");
								if(limLiability!=null){
									limit.setValue("Limit",limLiability.gets("Value"));
									limit.setValue("LimitDescription", "Policy Limit");
								}								
								
								// Build any sub limits
								buildSubCoverage(coverage, limit);				
								// Add the line code from the Line
								line = BeanTools.getParentBean(coverage, "DTOLine");
								if (line != null)
									limit.setValue("LineCd", line.gets("LineCd"));		
								
								riskCoverages.add(limit);	
							}else if(coverage.gets("CoverageCd").equals("DL2452")){
									ModelBean limit = new ModelBean("ClaimPolicyLimit");									
									limit.setValue("SourceCd","Policy");
									limit.setValue("OriginalCoverageCd", coverage.gets("CoverageCd"));
									limit.setValue("Description", "DL 24 52.  Coverage for Lead Liability");
									limit.setValue("CoverageCd", "DL2452");
									limit.setValue("LimitDescription", "Policy Limit");
									buildSubCoverage(coverage, limit);							
									// Add the line code from the Line
									ModelBean line = BeanTools.getParentBean(coverage, "DTOLine");
									if (line != null)
										limit.setValue("LineCd", line.gets("LineCd"));	
									riskCoverages.add(limit);									
							}else if(coverage.gets("CoverageCd").equals("WB")){
								ModelBean limit = new ModelBean("ClaimPolicyLimit");
								limit.setValue("SourceCd","Policy");
								limit.setValue("OriginalCoverageCd", coverage.gets("CoverageCd"));
								limit.setValue("CoverageCd", "WB");
								limit.setValue("Description", "DPC 105 Water Backup and Sump discharge or overflow Coverage");
								String waterBackupLimit = coverage.findBeanByFieldValue("DTOLimit", "LimitCd", "Limit1").gets("Value");
								ModelBean deductible = coverage.findBeanByFieldValue("DTODeductible", "DeductibleCd", "Deductible1");								
								limit.setValue("Limit", waterBackupLimit);
								limit.setValue("LimitDescription", "Policy Limit");
								if (deductible != null) {
									limit.setValue("Deductible", deductible.gets("Value"));
									limit.setValue("DeductibleDescription", "Policy Deductible");
           							limit.setValue("DeductibleAllocationCd", coverage.gets("CoverageCd"));
	                    			limit.setValue("DeductibleAllocationTypeCd", "Feature");
								}
								ModelBean line = BeanTools.getParentBean(coverage, "DTOLine");
								if (line != null)
									limit.setValue("LineCd", line.gets("LineCd"));							
								riskCoverages.add(limit);

							}else{
							ModelBean limit = buildCoverage(coverage,lossCauseCd,windHailDed,allPerilDed);
							riskCoverages.add(limit);
							}
							
						}
					}
				}
			}
			
			ModelBean claimPolicyInfo = claim.getBean("ClaimPolicyInfo");
			
			ModelBean[] coverageActivities = findCoveragesWithActivity(claim, claimPolicyInfo);
			
			// Delete Existing ClaimPolicyLimit ModelBeans
			claimPolicyInfo.deleteBeans("ClaimPolicyLimit");
			
			// Add Line Coverages to ClaimPolicyInfo
			for (ModelBean lineCoverage : lineCoverages) {
				claimPolicyInfo.addValue(lineCoverage);
			}
			
			// Add Risk Coverages to ClaimPolicyInfo
			for (ModelBean riskCoverage : riskCoverages) { 
				claimPolicyInfo.addValue(riskCoverage);		
			}
			
			addCoveragesWithActivity(claim, claimPolicyInfo, coverageActivities);
			
		} catch (Exception e) {
			throw new RuleException(e);
		}
	}
	
	/** Build the claim limit using policy coverage information
	 * 
	 * @param coverage Policy coverage ModelBean
	 * @return Claim Limit ModelBean
	 * @throws Exception when an unexpected error occurs
	 */
	private static ModelBean buildCoverage(ModelBean coverage, String lossCauseCd, String windHailDed, String allPerilDed)
	throws Exception {
		ModelBean container = null;
		if(coverage.getParentBean().getBeanName().equals("DTORisk")){
			container=coverage.getParentBean().getParentBean().getParentBean(); 
		}else{
			container=coverage.getParentBean().getParentBean();
		}
		ModelBean building = container.findBeanByFieldValue("DTOBuilding","Status","Active");
		ModelBean limit = new ModelBean("ClaimPolicyLimit");
		limit.setValue("SourceCd","Policy");
		limit.setValue("OriginalCoverageCd", coverage.gets("CoverageCd"));
		limit.setValue("CoverageCd", coverage.gets("CoverageCd"));
		limit.setValue("Description", coverage.gets("Description"));
		
		
		// Obtain the coverage limit
		ModelBean[] limits = coverage.getBeans("DTOLimit");
		for (int cnt=0; cnt<limits.length; cnt++) {
			if( !limits[cnt].gets("Value").equals("") ) {				
				if (limit.gets("Limit").equals("")){
						limit.setValue("Limit", limits[cnt].gets("Value"));
				}
			}
		}
		
		//Obtaining the deductible
		if(!coverage.gets("CoverageCd").equals("CovM")){
			if(lossCauseCd.equals("Windstorm")|| lossCauseCd.equals("Hail")) {
			  if(windHailDed!=null && !windHailDed.equals("")){
				if(!windHailDed.equalsIgnoreCase("None")){
					//windHailDed = windHailDed.concat("%");
					if(limit.gets("Limit")!=null && !limit.gets("Limit").equals("")){
					String covALimit = building.gets("CovALimit");
					String ded = StringRenderer.multiplyMoney(windHailDed,covALimit);
					String deductible = StringRenderer.divideMoney(ded, "100");
					limit.setValue("Deductible",deductible);
					limit.setValue("DeductibleDescription", "Wind/Hail Ded");
							limit.setValue("DeductibleAllocationTypeCd", "Risk");
							limit.setValue("DeductibleAllocationCd", "Wind/Hail Ded");				
					}}}	
			}else {
				limit.setValue("Deductible",allPerilDed );
				limit.setValue("DeductibleDescription", "All Peril");
				limit.setValue("DeductibleAllocationTypeCd", "Risk");
				limit.setValue("DeductibleAllocationCd", "AllPeril");
			}
		}
				
		// Build any sub limits
		buildSubCoverage(coverage, limit);		
		// Build the limit and deductible information fields
		buildCoverageDetails(coverage, limit,lossCauseCd, windHailDed);		
		// Add the line code from the Line
		ModelBean line = BeanTools.getParentBean(coverage, "DTOLine");
		if (line != null)
			limit.setValue("LineCd", line.gets("LineCd"));		
		return limit;
	}

	/** Build the limit items using Policy Coverage Item information
	 * 
	 * @param coverage The policy coverage ModelBean
	 * @param limit The claim limit ModelBean
	 * @throws Exception when an unexpected error occurs
	 */
	private static void buildSubCoverage(ModelBean coverage, ModelBean limit)
	throws Exception {
		ModelBean container = null;
		if(coverage.getParentBean().getBeanName().equals("DTORisk")){
			container=coverage.getParentBean().getParentBean().getParentBean(); 
		}else{
			container=coverage.getParentBean().getParentBean();
		}
		ModelBean basicPolicy = container.getBean("DTOBasicPolicy");
		String subType =basicPolicy.gets("SubTypeCd");
		ModelBean[] items = coverage.getBeans("DTOCoverageItem");
		if(coverage.gets("CoverageCd").equals("CovA")){	
			String itemNumber = "0";
			String covALimit = limit.gets("Limit");
			ModelBean item = new ModelBean("ClaimPolicySubLimit");
			itemNumber= StringRenderer.addInt(itemNumber, "1");
			item.setValue("CoverageSubCd", "CovA");
			item.setValue("Description", "Dwelling: "+subType);
		    item.setValue("ItemNumber", itemNumber);
		    item.setValue("Limit",covALimit);	
		    limit.addValue(item);
			item = new ModelBean("ClaimPolicySubLimit");
			itemNumber= StringRenderer.addInt(itemNumber, "1");
			item.setValue("CoverageSubCd", "DP0422");
			item.setValue("Description", "DP0422 Limited Fungi-Prop");
		    item.setValue("ItemNumber", itemNumber);
		    item.setValue("Limit","10000");	
		    limit.addValue(item);
		    limit.setValue("Limit",covALimit);
		}else if(coverage.gets("CoverageCd").equals("CovB")){			
			String covBLimit = limit.gets("Limit");
		    limit.setValue("Limit",covBLimit); 
		}else if(coverage.gets("CoverageCd").equals("CovC")){			
			String covCLimit = limit.gets("Limit");
		    limit.setValue("Limit",covCLimit);
		}else if(coverage.gets("CoverageCd").equals("CovD")){			
			String covDLimit = limit.gets("Limit");
			limit.setValue("Limit",covDLimit);
		}else if(coverage.gets("CoverageCd").equals("CovE")){			
			String covELimit = limit.gets("Limit");
			limit.setValue("Limit",covELimit);
		}else if(coverage.gets("CoverageCd").equals("DP1914")){			
			String itemNumber = "0";
			String dp1914Limit = limit.gets("Limit");
			ModelBean item = new ModelBean("ClaimPolicySubLimit");
			itemNumber= StringRenderer.addInt(itemNumber, "1");
			item.setValue("CoverageSubCd", "DP1914Fire");
			item.setValue("Description", "Fire");
		    item.setValue("ItemNumber", itemNumber);
		    limit.addValue(item);
		    item = new ModelBean("ClaimPolicySubLimit");
		    itemNumber= StringRenderer.addInt(itemNumber, "1");
			item.setValue("CoverageSubCd", "DP1914EC");
			item.setValue("Description", "EC");
		    item.setValue("ItemNumber", itemNumber);	    
		    limit.addValue(item);
		    item = new ModelBean("ClaimPolicySubLimit");
		    itemNumber= StringRenderer.addInt(itemNumber, "1");
			item.setValue("CoverageSubCd", "DP1914VMM");
			item.setValue("Description", "VMM");
		    item.setValue("ItemNumber", itemNumber);
		    limit.addValue(item);
		    limit.setValue("Limit",dp1914Limit);		
		}else if(coverage.gets("CoverageCd").equals("DP0471")){
			String limitValue="0";
			String itemNumber = "0";			
		    if(isActive(container, "CovA")){		    	
		    	String lim1 = coverage.findBeanByFieldValue("DTOLimit", "LimitCd", "Limit1").gets("Value");itemNumber= StringRenderer.addInt(itemNumber, "1");
			    ModelBean item = new ModelBean("ClaimPolicySubLimit");
				item.setValue("CoverageSubCd", "CovA");
				item.setValue("Description", "Dwelling");
			    item.setValue("ItemNumber", itemNumber);			    
			    item.setValue("Limit", lim1);
			    limit.addValue(item);	
			    limitValue = StringRenderer.addFloat(lim1, limitValue);
		   }
		   if(isActive(container, "CovB")){		    	
			   	String lim2 = coverage.findBeanByFieldValue("DTOLimit", "LimitCd", "Limit2").gets("Value");		    
			    itemNumber= StringRenderer.addInt(itemNumber, "1");
			    ModelBean item = new ModelBean("ClaimPolicySubLimit");
				item.setValue("CoverageSubCd", "CovB");
				item.setValue("Description", "Other Structure");
			    item.setValue("ItemNumber", itemNumber);
			    item.setValue("Limit", lim2);
			    limit.addValue(item);
			    limitValue = StringRenderer.addFloat(lim2, limitValue);
		   }		   
		}else if(coverage.gets("CoverageCd").equals("ELFL")){
				String itemNumber = "0";
				//BI
			    itemNumber= StringRenderer.addInt(itemNumber, "1");
			    ModelBean item = new ModelBean("ClaimPolicySubLimit");
				item.setValue("CoverageSubCd", "ELFLBI");
				item.setValue("Description", "Bodily Injury");
				item.setValue("ItemNumber", itemNumber);
			    limit.addValue(item);
			    //PD
			    itemNumber= StringRenderer.addInt(itemNumber, "1");
			    item = new ModelBean("ClaimPolicySubLimit");
				item.setValue("CoverageSubCd", "ELFLPD");
				item.setValue("Description", "Property Damage");
				item.setValue("ItemNumber", itemNumber);
			    limit.addValue(item);
		}else if(coverage.gets("CoverageCd").equals("CovL")){
			String itemNumber = "0";
			ModelBean limLiability = coverage.findBeanByFieldValue("DTOLimit", "LimitCd", "Limit1");
			//BI
		    itemNumber= StringRenderer.addInt(itemNumber, "1");
		    ModelBean item = new ModelBean("ClaimPolicySubLimit");
			item.setValue("CoverageSubCd", "BI");
			item.setValue("Description", "Bodily Injury");
			item.setValue("ItemNumber", itemNumber);
			if(limLiability!=null){
				item.setValue("Limit",limLiability.gets("Value"));
			}
		    limit.addValue(item);
		    //PD
		    itemNumber= StringRenderer.addInt(itemNumber, "1");
		    item = new ModelBean("ClaimPolicySubLimit");
			item.setValue("CoverageSubCd", "PD");
			item.setValue("Description", "Property Damage");
			item.setValue("ItemNumber", itemNumber);
			if(limLiability!=null){
				item.setValue("Limit",limLiability.gets("Value"));
			}
		    limit.addValue(item);
		    //DL2471
		    if( StringRenderer.greaterThan(limLiability.gets("Value"), "0") ){ 
		    	itemNumber= StringRenderer.addInt(itemNumber, "1");
			    item = new ModelBean("ClaimPolicySubLimit");
				item.setValue("CoverageSubCd", "DL2471");
				item.setValue("Description", "DL2471 Limited Fungi-Liab");
				item.setValue("ItemNumber", itemNumber);
				item.setValue("Limit","50000");	
			    limit.addValue(item);
			} 
		    
		}else if(coverage.gets("CoverageCd").equals("EB")){
			String itemNumber = "1";
			String limitValue="0";
			String ebLimit = limit.gets("Limit");
			ModelBean item = null;
			limitValue=StringRenderer.addFloat(limitValue, ebLimit);
			if(isActive(container, "CovA")){
			itemNumber= StringRenderer.addInt(itemNumber, "1");
			item = new ModelBean("ClaimPolicySubLimit");
			item.setValue("CoverageSubCd", "Cov A");
			item.setValue("Description", "Dwelling");
			item.setValue("ItemNumber", itemNumber);
			// item.setValue("Limit", "100000");
			limit.addValue(item);
			}
			if(isActive(container, "CovC")){
			itemNumber= StringRenderer.addInt(itemNumber, "1");
		    item = new ModelBean("ClaimPolicySubLimit");
			item.setValue("CoverageSubCd", "Cov C");
			item.setValue("Description", "Contents");
		    item.setValue("ItemNumber", itemNumber);
		    //item.setValue("Limit", "50000");
		    limit.addValue(item);
			}
		    limit.setValue("Limit", limitValue);
		}else{
		for (int cnt=0; cnt<items.length; cnt++) {
			if (!items[cnt].gets("Status").equals("Active")) continue;
			ModelBean item = new ModelBean("ClaimPolicySubLimit");
			item.setValue("CoverageSubCd", items[cnt].gets("CoverageItemCd"));
			item.setValue("Description", items[cnt].gets("ItemDesc"));
			
			// Get the Item Category or Code
			ModelBean code = items[cnt].getBean("DTOCoverageData", "CoverageDataCd", "ClassCd");
			if (code != null) {
				//item.setValue("Category", code.gets("Value").replaceAll(",","").replaceAll(" ",""));
				//item.setValue("CategoryDescription", code.gets("Value"));
			    //item.setValue("OriginalCoverageSubCd", code.gets("Value").replaceAll(",",""));
			}
			
			item.setValue("ItemNumber", items[cnt].gets("SequenceNumber"));
			
			// Get the limit
			ModelBean itemLimit = items[cnt].getBean("DTOLimit");
			item.setValue("Limit", itemLimit.gets("Value"));
			
			// Get the deductible
			ModelBean itemDeduct = items[cnt].getBean("DTODeductible");
			if (itemDeduct != null)
				item.setValue("Deductible", itemDeduct.gets("Value"));
			
			
			// Add the item to the limit
			limit.addValue(item);
			
			// Build the coverage item limit and deductible details
			//buildSubCoverageDetails(coverage, item);
		}
		}
	}
	
	/** Create the unique description fields for the limits and deductibles for each coverage 
	 * 
	 * @param coverage The policy coverage ModelBean
	 * @param limit The claim policy limit ModelBean
	 * @throws Exception when an error occurs
	 */
	private static void buildCoverageDetails(ModelBean coverage, ModelBean limit,String lossCauseCd, String windHailDed) 
	throws Exception {
		
		// Pseudo switch structure for limits
		for (int cnt=0; cnt<1; cnt++) {
			
			String lim = limit.gets("Limit");
			if (StringTools.isNumeric(lim)) {
				Money limitAmt = new Money(lim);
				if (limitAmt.compareTo("0.00") > 0)
					limit.setValue("LimitDescription", "Policy Limit");
			}
			
		}
		
		// Pseudo switch structure for deductibles
		for (int cnt=0; cnt<1; cnt++) {
			String covCd = coverage.gets("CoverageCd");
			if(!covCd.equals("CovM")){
				if(lossCauseCd.equals("Windstorm")|| lossCauseCd.equals("Hail")) {
				  if(windHailDed!=null && !windHailDed.equals("")){
					if(!windHailDed.equalsIgnoreCase("None")){					
						limit.setValue("DeductibleDescription", "Wind/Hail Ded");
					}
				  }
				}else {				
					limit.setValue("DeductibleDescription", "All Peril");
				}
			}			
		}
			
	}
	
	/** Create the unique description fields for the limits and deductibles for each coverage item 
	 * 
	 * @param coverage The policy coverage ModelBean
	 * @param limit The claim policy limit ModelBean
	 * @throws Exception when an error occurs
	 
	private static void buildSubCoverageDetails(ModelBean coverage, ModelBean limit) 
	throws Exception {
		String lim = limit.gets("Limit");		
		
		// Pseudo switch structure for description of coverage
		for (int cnt=0; cnt<1; cnt++) {
			String covCd = coverage.gets("CoverageCd");
			StringBuffer description = new StringBuffer();
			
			// Business Pursuits
			if (covCd.equals("BUP")) {
				ModelBean covItem = coverage.getBean("DTOCoverageItem", "SequenceNumber", limit.gets("ItemNumber"));
				ModelBean nameVal = covItem.findBeanByFieldValue("QuestionReply", "Name", "InsName");
				ModelBean occVal = covItem.findBeanByFieldValue("QuestionReply", "Name", "Occupation");
				ModelBean classVal = covItem.findBeanByFieldValue("QuestionReply", "Name", "ClassCode");
				
				description.append(nameVal.gets("Value"))
					.append("-")
					.append(occVal.gets("Value"))
					.append("-")
					.append(classVal.gets("Value"));
				limit.setValue("Description", description.toString());				
			}
		}
		
	}
	
	/** Obtain the Policy Line Code from the coverage code
	 * @param policyProductVersionIdRef The policy product version id reference name 
	 * @param coverageCd The coverage code
	 * @return the line code
	 * @throws Exception when an error occurs
	 */
	public String getPolicyLineCd(String policyProductVersionIdRef, String coverageCd) 
	throws Exception {
		ModelBean dtoLine = new ModelBean("DTOLine");
        // Get the Line Code based on the Coverage code from the product
        ProductSetup productSetup = ProductMaster.getProductMaster("UW").getProductVersion(policyProductVersionIdRef).getProductSetup();
        if (productSetup != null) {
            ModelBean productCov = productSetup.getBean().findBeanByFieldValue("ProductCoverage", "CoverageCd", coverageCd);
            if (productCov != null) {
                ModelBean productLine = BeanTools.getParentBean(productCov, "ProductLine");
                if (productLine != null) {
                  dtoLine.setValue("LineCd", productLine.gets("LineCd"));
                }
            }
        }                
        
        return dtoLine.gets("LineCd");
        	
	}
	/** Returns true if coverage is active
     * @param container ModelBean holding the container of the coverage.
     * @param coverageCd String containing the coverage code
     * @return true/false if the coverage is active
     */    
    public static boolean isActive( ModelBean container, String coverageCd ) {
        try {
            ModelBean[] coverages = getActiveCoverages(container, coverageCd);
            if( coverages.length == 0 )
                return false;
            else
                return true;
        }
        catch( Exception e ) {
            e.printStackTrace();
            Log.error(e);
            return false;
        }
    }
    
    /** Returns an array of Coverage ModelBeans for all coverages that are active
     * @return ModelBean[] array of active coverages
     * @param coverageCd String containing the coverage code
     * @param container ModelBean holding the container of the coverage, application/policy/quote.
     */    
    public static ModelBean[] getActiveCoverages( ModelBean container, String coverageCd ) {
        try {
            ArrayList<ModelBean> arr = new ArrayList<ModelBean>();
            
            ModelBean[] coverage = container.findBeansByFieldValue("DTOCoverage", "CoverageCd", coverageCd);   
            for( int i = 0; i < coverage.length; i++ ) {
                if( coverage[i].valueEquals("Status", "Active") )
                    arr.add(coverage[i]);
            }
            ModelBean[] coverages = (ModelBean[]) arr.toArray( new ModelBean[arr.size()] );
            
            return coverages;
        }
        catch( Exception e ) {
            e.printStackTrace();
            Log.error(e);
            return null;
        }
    }
    
    
}
