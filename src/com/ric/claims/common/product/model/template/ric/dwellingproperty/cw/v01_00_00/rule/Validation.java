package com.ric.claims.common.product.model.template.ric.dwellingproperty.cw.v01_00_00.rule;

import com.iscs.claims.claim.Claim;
import com.iscs.claims.claim.render.ClaimRenderer;
import com.iscs.claims.notice.render.NoticeRenderer;
import com.iscs.claims.transaction.render.ClaimantTransactionRenderer;
import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.render.DateRenderer;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.render.VelocityTools;
import com.iscs.common.utility.bean.render.BeanRenderer;
import com.iscs.common.utility.validation.render.ValidationRenderer;
import com.iscs.uw.policy.render.PolicyRenderer;
import com.ric.claims.claim.render.RICClaimRenderer;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

public class Validation implements RuleProcessor {

  private String claimantParam;
  private String noticeParam;
  private String detailParam;
  private String summaryParam;
  private ModelBean claim;
  private ModelBean claimPolicyInfo;
  private ModelBean response;
  private ModelBean request;
  private StringDate todayDt;
  private StringDate lossDt;
  private ModelBean errors;
  private JDBCData data;

  @Override
  public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user,
                           JDBCData data) throws RuleException {

    try {
      claim = bean;
      todayDt = new StringDate(DateRenderer.getDate());
      request = additionalBeans[1];
      response = additionalBeans[2];
      errors = response.getBean("ResponseParams").getBean("Errors");
      this.data = data;
      claimPolicyInfo = claim.getBean("ClaimPolicyInfo");

      processRules();

    } catch (Exception e) {
      throw new RuleException(e);
    }

    return errors;
  }

  public void processRules() throws Exception {
    if (Claim.isALossNotice(claim)) {
      triggerLossNoticeIncomplete();

      //Set Link for Notice Tab
      noticeParam = "CodeRefOptionsKey=claim-general&TabOption=Notice&ShowRequiredFieldsInd=Yes";

      //Set Link for Claimants Tab
      claimantParam = "CodeRefOptionsKey=claim-general&TabOption=Claimants&ShowRequiredFieldsInd=Yes";
    } else {
      //Set Link for Summary Tab
      summaryParam = "CodeRefOptionsKey=claim-general&TabOption=Summary&ShowRequiredFieldsInd=Yes";

      //Set Link for Detail Tab
      detailParam = "CodeRefOptionsKey=claim-general&TabOption=Detail&ShowRequiredFieldsInd=Yes";

      noticeParam = detailParam;
    }

    triggerLossDtValidation();
    triggerDOIComplaintIndWarning();
    triggerSIUActiveWarning();
    triggerAssignmentOfBenefitsActiveWarning();
    triggerLossCauseByEarthquakeWarning();
    triggerLossCauseByFireOrSmokeWarning();
    triggerReportedByError();
    triggerReportedToError();

    //Validate Reported Date/Time
    triggerReportedDtErrors();
    triggerReportedTmError();
    triggerPolicyError();
    triggerProductLineError();
    triggerRiskError();
    triggerLossCauseError();
    triggerSubLossCausesError();
    triggerClaimantsValidation();
    triggerNoClaimantsError();
    triggerThirdPartyClaimantWindstormClaimError();
    triggerValidateWitnesses();
    triggerSumOfIndividualLiabilityAndVerifySubrogations();
    triggerFirstDueDateForSubrogation();
  }

  private void triggerLossNoticeIncomplete() throws Exception {
    if (StringRenderer.isTrue(claim.gets("DamagedInd"))) {
      ValidationRenderer.addValidationError(claim, "IncompleteLossNotice", "WARNING", "Select Edit to verify claimant information.");

      //Always present a warning that Loss Notice is not submitted until Finalized and Submitted
      ValidationRenderer.addValidationError(claim, "IncompleteLossNotice", "WARNING", "This loss notice is incomplete until you click the Complete action followed by the Submit button.");
    } else {
      if (VelocityTools.beanArrayLength(ClaimRenderer.getOpenClaimants(claim)) > 0)
      //Always present a warning that Loss Notice is not submitted until Finalized and Submitted
      {
        ValidationRenderer.addValidationError(claim, "IncompleteLossNotice", "WARNING", "This loss notice is incomplete until you click the Complete action followed by the Submit button.");
      } else {
        ValidationRenderer.addValidationError(claim, "IncompleteLossNotice", "ERROR", "Select New Claimant under Actions to add claimant information.");
      }
    }
  }

  //Validate Loss Date
  private void triggerLossDtValidation() throws Exception {
    StringDate lossDt = claim.getDate("LossDt");
    Boolean outOfBounds = false;

    if (!claim.gets("LossDt").equals("")) {
      StringDate asOfDt = claimPolicyInfo.getDate("AsOfDt");
      StringDate inceptionDt = claimPolicyInfo.getDate("InceptionDt");
      String inceptionTm = claimPolicyInfo.gets("InceptionTm");
      StringDate expirationDt = claimPolicyInfo.getDate("ExpirationDt");
      StringDate cancelDt = claimPolicyInfo.getDate("CancelDt");
      String policyNumber = claimPolicyInfo.gets("PolicyNumber");
      String securityId = request.gets("SecurityId");
      String sessionId = request.gets("SessionId");
      String userId = request.gets("UserId");
      String conversationId = request.gets("ConversationId");

      //Normal date validations are done for just Loss Notice
      if (ClaimRenderer.isALossNotice(claim)) {
        String action = String.format("javascript:cmmXFormSafe('CLClaimSync','%s','%s','%s','%s')",
            claim.getSystemId(), claim.getId(), claim.getId(), noticeParam + "&HighlightFields=Claim.LossDt");
        String noticeSummaryTabLink = ValidationRenderer.createActionLink(action);

        if (DateRenderer.compareTo(todayDt, lossDt) < 0) {
          ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR", "Loss Date can not be after Today's Date", noticeSummaryTabLink);
        }

        if (!claimPolicyInfo.gets("InceptionDt").equals("")) {
          if (DateRenderer.compareTo(inceptionDt, lossDt) > 0) {
            ValidationRenderer.addValidationErrorWithLink(claim, "ValidationPersist", "WARNING", "Loss Date is not within the policy period", noticeSummaryTabLink);
          } else if (DateRenderer.compareTo(inceptionDt, lossDt) == 0) {
            //Warn the user that the loss date is the same as the inception date if inception time is not blank and not 12:01am
            if (!inceptionTm.equals("12:01am") && !inceptionTm.equals("")) {
              ValidationRenderer.addValidationError(claim, "ValidationPersist", "WARNING", "Loss date is the same as policy inception date. Loss time should be manually verified");
            }
          }
        }

        if (!claimPolicyInfo.gets("ExpirationDt").equals("") && DateRenderer.compareTo(expirationDt, lossDt) <= 0) {
          ValidationRenderer.addValidationErrorWithLink(claim, "ValidationPersist", "WARNING", "Loss Date is not within the policy period", noticeSummaryTabLink);
        }

        //Determine if the loss date is within a lapse period
        if (NoticeRenderer.inLapsePeriod(data, policyNumber, lossDt, securityId, sessionId, userId, conversationId)) {
          ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "WARNING", "Loss Date is within a lapse period", noticeSummaryTabLink);
        } else if (!claimPolicyInfo.gets("CancelDt").equals("")) {
          if (DateRenderer.compareTo(cancelDt, lossDt) <= 0) {
            ValidationRenderer.addValidationErrorWithLink(claim, "Validation",
                "WARNING", "Loss Date is after the policy cancellation date of " + StringRenderer.formatDate(cancelDt.toString()), noticeSummaryTabLink);
          }
        }
      } else {
        String action = String.format("javascript:cmmXFormSafe('CLClaimSync','%s','%s','%s','%s')",
            claim.getSystemId(), claim.getId(), claim.getId(), summaryParam + "&HighlightFields=Claim.LossDt");
        String noticeSummaryTabLink = ValidationRenderer.createActionLink(action);

        //Validate for potential loss date changes
        if (DateRenderer.compareTo(todayDt, lossDt) < 0) {
          ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR", "Loss Date can not be after Today's Date", noticeSummaryTabLink);
        }

        if (!claimPolicyInfo.gets("InceptionDt").equals("") && DateRenderer.compareTo(inceptionDt, lossDt) > 0) {
          ValidationRenderer.addValidationErrorWithLink(claim, "ValidationPersist", "WARNING", "Loss Date is not within the policy period", noticeSummaryTabLink);
          outOfBounds = true;
        }

        if (!claimPolicyInfo.gets("ExpirationDt").equals("") && DateRenderer.compareTo(expirationDt, lossDt) <= 0) {
          ValidationRenderer.addValidationErrorWithLink(claim, "ValidationPersist", "WARNING", "Loss Date is not within the policy period", noticeSummaryTabLink);
          outOfBounds = true;
        }

        if (!DateRenderer.equals(asOfDt, lossDt)) {
          outOfBounds = false;
          action = String.format("javascript:cmmXFormSafe('CLClaimSync','%s','%s','%s','%s')", claim.getSystemId(), claim.getId(), claim.getId(), noticeParam + "&HighlightFields=Claim.LossDt");
          noticeSummaryTabLink = ValidationRenderer.createActionLink(action);

          // The Loss Date just changed this session, so check to see if any Policy changes affected this
          if (!claimPolicyInfo.gets("InceptionDt").equals("") && DateRenderer.compareTo(inceptionDt, lossDt) > 0) {
            outOfBounds = true;
          }

          if (!claimPolicyInfo.gets("ExpirationDt").equals("") && DateRenderer.compareTo(expirationDt, lossDt) <= 0) {
            outOfBounds = true;
          }

          //Determine if the loss date is within a lapse period
          if (NoticeRenderer.inLapsePeriod(data, policyNumber, lossDt, securityId, sessionId, userId, conversationId)) {
            ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "WARNING", "Loss Date is within a lapse period", noticeSummaryTabLink);
          } else if (!claimPolicyInfo.gets("CancelDt").equals("") && DateRenderer.compareTo(cancelDt, lossDt) <= 0) {
            ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "WARNING", "Loss Date is after the policy cancellation date of " + StringRenderer.formatDate(cancelDt.toString()), noticeSummaryTabLink);
          } else if (outOfBounds == false) {
            Boolean hasTransactionChanges = PolicyRenderer.hasTransactionChanges(data, claimPolicyInfo.gets("PolicyNumber"), asOfDt, lossDt);
            if (hasTransactionChanges) {
              ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "WARNING", "The new loss date falls within a different policy change, and therefore may affect coverages. Please check the Features and Reserves.", noticeSummaryTabLink);
            }
          }
        }
        //Warn the user that the loss date is the same as the inception date if inception time is not blank and not 12:01am
        if (DateRenderer.equals(inceptionDt, lossDt)) {
          if (!inceptionTm.equals("12:01am") && !inceptionTm.equals("")) {
            ValidationRenderer.addValidationError(claim, "ValidationPersist", "WARNING", "Loss date is the same as policy inception date. Loss time should be manually verified");
          }
        }
      }
    }
  }

  public void triggerReportedByError() throws Exception {
    String action = String.format("javascript:cmmXFormSafe('CLClaimSync','%s','%s','%s','%s&HighlightFields=Claim.ReportedBy')",
        claim.getSystemId(), claim.getId(), claim.getId(), noticeParam);
    String noticeSummaryTabLink = ValidationRenderer.createActionLink(action);

    if (claim.gets("ReportedBy").isEmpty()) {
      ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR", "Reported By required", noticeSummaryTabLink);
    }
  }

  public void triggerReportedToError() throws Exception {
    String action = String.format("javascript:cmmXFormSafe('CLClaimSync','%s','%s','%s','%s&HighlightFields=Claim.ReportedTo')",
        claim.getSystemId(), claim.getId(), claim.getId(), noticeParam);
    String noticeSummaryTabLink = ValidationRenderer.createActionLink(action);

    if (claim.gets("ReportedTo").isEmpty()) {
      ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR", "Reported To required", noticeSummaryTabLink);
    }
  }

  private boolean triggerReportedDtErrors() throws Exception {
    String action = String.format("javascript:cmmXFormSafe('CLClaimSync','%s','%s','%s','%s&HighlightFields=Claim.ReportedDt')",
        claim.getSystemId(), claim.getId(), claim.getId(), noticeParam);
    String noticeSummaryTabLink = ValidationRenderer.createActionLink(action);

    if (claim.gets("ReportedDt").isEmpty()) {
      ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR", "Reported Date Required", noticeSummaryTabLink);
    } else {
      StringDate reportedDt = claim.getDate("ReportedDt");
      lossDt = claim.getDate("LossDt");

      if (DateRenderer.compareTo(todayDt, reportedDt) < 0) {
        ValidationRenderer.addValidationError(claim, "Validation", "ERROR", "Reported Date can not be after Today's Date", noticeSummaryTabLink);
      }

      if (!lossDt.equals("") && DateRenderer.compareTo(lossDt, reportedDt) > 0) {
        ValidationRenderer.addValidationError(claim, "Validation", "ERROR", "Reported Date can not be before Loss Date", noticeSummaryTabLink);
      }
    }

    return false;
  }

  private boolean triggerReportedTmError() throws Exception {
    if (claim.gets("ReportedTm").isEmpty()) {
      String action = String.format("javascript:cmmXFormSafe('CLClaimSync','%s','%s','%s','%s&HighlightFields=Claim.ReportedTm')",
          claim.getSystemId(), claim.getId(), claim.getId(), noticeParam);
      String noticeSummaryTabLink = ValidationRenderer.createActionLink(action);
      ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR", "Reported Time Required", noticeSummaryTabLink);
      return true;
    }

    return false;
  }

  //Validate Product Line
  private void triggerProductLineError() throws Exception {
    if (claim.gets("ProductLineCd").equals("")) {
      String action = String.format("javascript:cmmXFormSafe('CLClaimSync','%s','%s','%s','%s')",
          claim.getSystemId(), claim.getId(), claim.getId(), noticeParam);
      String noticeSummaryTabLink = ValidationRenderer.createActionLink(action);
      ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR", "Line Required", noticeSummaryTabLink);
    }
  }

  // Validate Policy
  private void triggerPolicyError() throws Exception {
    if (claimPolicyInfo.gets("PolicyRef").equals("")) {
      String action = String.format("javascript:cmmXFormSafe('CLClaimSync','%s','%s','%s','%s')",
          claim.getSystemId(), claim.getId(), claim.getId(), noticeParam);
      String noticeSummaryTabLink = ValidationRenderer.createActionLink(action);
      ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR", "Policy Required", noticeSummaryTabLink);
    }
  }

  // Validate Risk
  private void triggerRiskError() throws Exception {
    if (claim.gets("RiskIdRef").equals("")) {
      String action = String.format("javascript:cmmXFormSafe('CLClaimSync','%s','%s','%s','%s&HighlightFields=Claim.RiskIdRef')",
          claim.getSystemId(), claim.getId(), claim.getId(), noticeParam);
      String noticeSummaryTabLink = ValidationRenderer.createActionLink(action);
      ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR", "Property Required", noticeSummaryTabLink);
    }
  }

  // Validate Loss Cause
  private void triggerLossCauseError() throws Exception {
    if (claim.gets("LossCauseCd").equals("")) {
      String action = String.format("javascript:cmmXFormSafe('CLClaimSync','%s','%s','%s','%s&HighlightFields=Claim.LossCauseCd')",
          claim.getSystemId(), claim.getId(), claim.getId(), noticeParam);
      String noticeSummaryTabLink = ValidationRenderer.createActionLink(action);
      ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR", "Loss Cause Required", noticeSummaryTabLink);
    }
  }

  // Validate mandatory Sub loss causes
  private void triggerSubLossCausesError() throws Exception {
    ModelBean[] priorTransactions = claim.findBeansByFieldValue("ClaimantTransaction", "TransactionCd", "Adjust Reserve");
    Boolean reservesAdjusted = false;
    String lossCauseCd = claim.gets("LossCauseCd");

    if (VelocityTools.beanArrayLength(priorTransactions) > 0) {
      reservesAdjusted = true;
    }

    Boolean flag = true;
    for (ModelBean feature : claim.getAllBeans("Feature")) {
      if (!feature.valueEquals("StatusCd", "Closed")) {
        flag = false;
      }
    }

    if (!lossCauseCd.isEmpty() && StringRenderer.in(lossCauseCd, "Fire,Water,Theft,WORKERS COMP,Windstorm,Flood")) {
      String sublossCd = claim.gets("SubLossCauseCd");
      if (sublossCd.equals("")) {
        if (flag && reservesAdjusted) {
          String action = String.format("javascript:cmmXFormSafe('CLClaimSync','%s','%s','%s','%s&HighlightFields=Claim.SubLossCauseCd')",
              claim.getSystemId(), claim.getId(), claim.getId(), noticeParam);
          String noticeSummaryTabLink = ValidationRenderer.createActionLink(action);
          ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR", "Sub Loss Cause is required when Claim is Closed", noticeSummaryTabLink);
        }
      }
    }
  }

  private void triggerDOIComplaintIndWarning() throws Exception {
    if (claim.gets("DOIComplaintInd").equals("Yes")) {
      ValidationRenderer.addValidationError(claim, "ValidationPersist", "WARNING", "DOI Complaint Active");
    }
  }

  private void triggerSIUActiveWarning() throws Exception {
    if (claim.gets("InSIUInd").equals("Yes")) {
      ValidationRenderer.addValidationError(claim, "ValidationPersist", "WARNING", "Special Investigative Unit Active");
    }
  }

  private void triggerAssignmentOfBenefitsActiveWarning() throws Exception {
    if (claim.gets("AssignmentOfBenefits").equals("Yes")) {
      ValidationRenderer.addValidationError(claim, "ValidationPersist", "WARNING", "Assignment of Benefits Active");
    }
  }

  private void triggerLossCauseByEarthquakeWarning() throws Exception {
    if (claim.gets("LossCauseCd").equals("Earthquake")) {
      ValidationRenderer.addValidationError(claim, "Validation", "WARNING", "Describe all of damages to the property (including if any holes in the roof or water leaking in)");
    }
  }

  //No claimants setup
  private void triggerNoClaimantsError() throws Exception {
    if (claim.gets("TypeCd").equals("Transaction")) {
      ModelBean[] openClaimants = ClaimRenderer.getOpenClaimants(claim);
      ModelBean[] closedClaimants = claim.findBeansByFieldValue("Claimant", "StatusCd", "Closed");

      if (VelocityTools.beanArrayLength(openClaimants) == 0 && VelocityTools.beanArrayLength(closedClaimants) == 0) {
        String action = String.format("javascript:cmmXFormSafe('CLClaimSync','%s','%s','%s','CodeRefOptionsKey=claim-general&amp;TabOption=Detail')",
            claim.getSystemId(), claim.getId(), claim.getId());
        String noticeSummaryTabLink = ValidationRenderer.createActionLink(action);
        ValidationRenderer.addValidationErrorWithLink(claim, "IncompleteLossNotice", "ERROR", "No Claimants Have Been Setup", noticeSummaryTabLink);
      }
    }
  }

  // Validate Witnesses
  private void triggerValidateWitnesses() throws Exception {
    String action;
    String witnessLink;
    ModelBean[] witnesses = claim.findBeansByFieldValue("Witness", "StatusCd", "Active");

    for (ModelBean witness : witnesses) {
      //Set Param for Witness
      String witnessParam = "TabOption=Witnesses&ShowRequiredFieldsInd=Yes";

      if (witness.gets("TypeCd").equals("")) {
        action = String.format("javascript:cmmXFormSafe('CLWitnessSync','%s','%s','%s','%s')", claim.getSystemId(), claim.getId(), witness.getId(), witnessParam + "&HighlightFieldsWitness.TypeCd");
        witnessLink = ValidationRenderer.createActionLink(action);
        ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR", "Type Required on Witness #" + witness.gets("WitnessNumber"), witnessLink);
      }

      String witnessName = witness.getBean("NameInfo", "NameTypeCd", "WitnessName").gets("CommercialName");
      if (witnessName.equals("")) {
        action = String.format("javascript:cmmXFormSafe('CLWitnessSync','%s','%s','%s','%s')", claim.getSystemId(), claim.getId(), witness.getId(), witnessParam + "&HighlightFields=WitnessName.CommercialName");
        witnessLink = ValidationRenderer.createActionLink(action);
        ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR", "Name Required on Witness #" + witness.gets("WitnessNumber"), witnessLink);
      }
    }
  }

  // Check for Prior Adjust Reserve Transactions
  private void triggerSumOfIndividualLiabilityAndVerifySubrogations() throws Exception {
    Boolean reserveAdjusted = false;
    ModelBean[] priorTransactions = claim.findBeansByFieldValue("ClaimantTransaction", "TransactionCd", "Adjust Reserve");

    for (ModelBean priorTransaction : priorTransactions) {
      if (priorTransaction.valueEquals("StatusCd", "Active") || priorTransaction.valueEquals("StatusCd", "Open")) {
        reserveAdjusted = true;
      }
    }

    // Verify if any active reserves exists and then validate the liability percentage
    if (reserveAdjusted) {
      if (!claim.gets("TypeCd").equals("LossNotice")) {
        String liabilityPct = "0";
        Boolean liabilityLessThan100 = false;
        Boolean liabilitygreaterThan100 = false;
        String showMsg;

        ModelBean[] subrogations = claim.getAllBeans("Subrogation");
        for (ModelBean subrogation : subrogations) {
          if (!subrogation.valueEquals("StatusCd", "Deleted")) {
            liabilityPct = StringRenderer.addFloat(liabilityPct, subrogation.gets("ResponsiblePartyLiabilityPct"));

            if (StringRenderer.lessThanEqual(liabilityPct, "0")) {
              liabilityLessThan100 = true;
            } else if (StringRenderer.greaterThan(liabilityPct, "100")) {
              liabilitygreaterThan100 = true;
            }
          }
        }

        if (liabilityLessThan100) {
          showMsg = "The sum of all the individual liability % cannot be 0";
          ValidationRenderer.addValidationError(claim, "Validation", "ERROR", showMsg);
        }

        if (liabilitygreaterThan100) {
          showMsg = "The sum of all the individual liability % cannot be greater than 100";
          ValidationRenderer.addValidationError(claim, "Validation", "ERROR", showMsg);
        }

        //verify is responsible party and owner address has been recorded
        for (ModelBean subrogation : subrogations) {
          if (!subrogation.valueEquals("StatusCd", "Deleted") && !subrogation.valueEquals("StatusCd", "Closed")) {

            if (StringRenderer.isTrue(subrogation.gets("InstallmentPaymentInd"))) {
              ModelBean installments = subrogation.getBean("Installments");

              if (installments != null) {
                ModelBean responsibleParty = subrogation.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "ResponsibleParty");
                ModelBean responsibleOwner = subrogation.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "ResponsibleOwner");
                ModelBean addr = responsibleParty.getBean("Addr");

                //validate Responsible party address
                if (addr.gets("Addr1").equals("") || addr.gets("StateProvCd").equals("") || addr.gets("PostalCode").equals("")) {
                  String partyName = responsibleParty.getBean("NameInfo").gets("CommercialName");
                  showMsg = StringRenderer.concat("Responsible Party address is incomplete for ", partyName);
                  ValidationRenderer.addValidationError(claim, "Validation", "ERROR", showMsg);
                }

                //validate Responsible Owner address
                if (addr.gets("Addr1").equals("") || addr.gets("StateProvCd").equals("") || addr.gets("PostalCode").equals("")) {
                  String ownerName = responsibleOwner.getBean("NameInfo").gets("CommercialName");
                  showMsg = StringRenderer.concat("Responsible Owner address is incomplete for ", ownerName);
                  ValidationRenderer.addValidationError(claim, "Validation", "ERROR", showMsg);
                }
              }
            }
          }
        }
      }
    }
  }

  //verify is firstDueDate is set on save subrogation SubrogationProviderNumber
  private void triggerFirstDueDateForSubrogation() throws Exception {
    ModelBean[] subrogations = claim.getAllBeans("Subrogation");
    for (ModelBean subrogation : subrogations) {
      if (!subrogation.valueEquals("StatusCd", "Active")) {
        StringDate firstDueDate = subrogation.getDate("FirstDueDate");
        if (firstDueDate.equals("")) {
          ValidationRenderer.addValidationError(claim, "Validation", "ERROR", "Subrogation Information -- First Due Date is required. ");
        }
      }
    }
  }

  private boolean triggerLossCauseByFireOrSmokeWarning() throws Exception {
    if (claim.gets("Description").isEmpty() &&
        (claim.gets("LossCauseCd").equals("Fire") || claim.gets("LossCauseCd").equals("Sudden And Accidental Damage From Smoke"))) {
      ValidationRenderer.addValidationError(claim, "Validation", "WARNING", "Please update Detailed Description with how the fire started.");
      return true;
    }

    return false;
  }

  private boolean triggerThirdPartyClaimantWindstormClaimError() throws Exception {
    if (claim.gets("LossCauseCd").equals("Windstorm") && RICClaimRenderer.getOpenClaimants(claim, "Third Party").length > 0) {
      ValidationRenderer.addValidationError(claim, "Validation", "ERROR", "Third Party Claimant Not Allowed on Windstorm Claim");
      return true;
    }

    return false;
  }

  private void triggerClaimantsValidation() throws Exception {
    ModelBean[] claimants = ClaimRenderer.getOpenClaimants(claim);

    if( claimants.length == 0 && Claim.isALossNotice(claim) ) {      
      String action = String.format("javascript:cmmXFormSafe('CLClaimSync','%s','%s','%s','CodeRefOptionsKey=claim-general&amp;TabOption=Detail')",
              claim.getSystemId(), claim.getId(), claim.getId());
      String noticeSummaryTabLink = ValidationRenderer.createActionLink(action);
      ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR", "Claimant information has not yet been completed, please fill in claimant information.", noticeSummaryTabLink);
    } else {
      for (ModelBean claimant : claimants) {
        triggerCompletedRequiredClaimantValidation(claimant);
        triggerBestWayToContactValidation(claimant);
        triggerInjuryForMajorTraumaError(claimant);
        triggerAssignedAdjusterErrors(claimant);
      }
    }
  }

  private void triggerCompletedRequiredClaimantValidation(ModelBean claimant) throws Exception {
    boolean notCompleted = false;

    //Validate Claimant Required Fields
    if (claimant.gets("ClaimantTypeCd").isEmpty() || claimant.gets("ClaimantSubTypeCd").isEmpty() || claimant.gets("IndexName").isEmpty()) {
      notCompleted = true;
    }

    //Validate ACH Required Fields, if Preferred Payment Method is ACH
    if (claimant.gets("PreferredPaymentMethodCd").equals("ACH")) {
      ModelBean paymentDestination = claimant.getBeanByAlias("PaymentDestination");
      if (paymentDestination.gets("ACHStandardEntryClassCd").isEmpty() || paymentDestination.gets("ACHBankAccountTypeCd").isEmpty() ||
          paymentDestination.gets("ACHName").isEmpty() || paymentDestination.gets("ACHBankAccountNumber").isEmpty() ||
          paymentDestination.gets("ACHRoutingNumber").isEmpty()) {
        notCompleted = true;
      }
    }

    //Validate Claimant Name Required Fields
    ModelBean claimantName = BeanRenderer.getBeanWithAlias(claimant, "ClaimantName");
    if (claimantName.gets("CommercialName").isEmpty()) {
      notCompleted = true;
    }

    ModelBean assignedAdjuster = claimant.getBean("AssignedProvider", "AssignedProviderTypeCd", "AssignedAdjuster");
    if (!Claim.isALossNotice(claim) && assignedAdjuster != null && assignedAdjuster.gets("ProviderRef").isEmpty()) {
      assignedAdjuster.setValue("ProviderRef", claim.gets("AdjusterProviderRef"));
    }

    if (notCompleted) {
      String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
          claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam);
      String claimantLink = ValidationRenderer.createActionLink(action);
      ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR", "Claimant information has not yet been completed, please fill in claimant information.", claimantLink);
    }
  }

  private void triggerBestWayToContactValidation(ModelBean claimant) throws Exception {
    ModelBean partyInfo = claimant.getBean("PartyInfo", "PartyTypeCd", "ClaimantParty");
    ModelBean personInfo = partyInfo.getBean("PersonInfo", "PersonTypeCd", "ClaimantPerson");
    ModelBean primaryPhoneInfo = partyInfo.getBean("PhoneInfo", "PhoneTypeCd", "ClaimantPhonePrimary");
    ModelBean secondaryPhoneInfo = partyInfo.getBean("PhoneInfo", "PhoneTypeCd", "ClaimantPhoneSecondary");
    ModelBean faxPhoneInfo = partyInfo.getBean("PhoneInfo", "PhoneTypeCd", "ClaimantFax");
    ModelBean emailInfo = partyInfo.getBean("EmailInfo", "EmailTypeCd", "ClaimantEmail");
    String bestWayToContact = personInfo.gets("BestWayToContact");

    if (bestWayToContact.isEmpty()) {
      String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
          claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=ClaimantPerson.BestWayToContact");
      String claimantLink = ValidationRenderer.createActionLink(action);
      ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR", "Best way to contact information is not entered on Claimant " + claimant.gets("ClaimantNumber"), claimantLink);
    } else {
      Boolean showMsg = false;

      if (bestWayToContact.equals("Home Phone")) {
        if (!(primaryPhoneInfo.gets("PhoneName").equals("Home") || secondaryPhoneInfo.gets("PhoneName").equals("Home"))) {
          showMsg = true;
        } else if ((primaryPhoneInfo.gets("PhoneName").equals("Home") && primaryPhoneInfo.gets("PhoneNumber").isEmpty())
            && (secondaryPhoneInfo.gets("PhoneName").equals("Home") && secondaryPhoneInfo.gets("PhoneNumber").isEmpty())) {
          showMsg = true;
        }

        if (showMsg) {
          String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
              claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=ClaimantPerson.BestWayToContact");
          String claimantLink = ValidationRenderer.createActionLink(action);
          ValidationRenderer.addValidationError(claim, "Validation", "ERROR", "Best way to contact is home phone but home phone number is not entered on Claimant " + claimant.gets("ClaimantNumber"), claimantLink);
        }
      }

      if (bestWayToContact.equals("Mobile Phone")) {
        if (!(primaryPhoneInfo.gets("PhoneName").equals("Mobile") || secondaryPhoneInfo.gets("PhoneName").equals("Mobile"))) {
          showMsg = true;
        } else if ((primaryPhoneInfo.gets("PhoneName").equals("Mobile") && primaryPhoneInfo.gets("PhoneNumber").isEmpty())
            && (secondaryPhoneInfo.gets("PhoneName").equals("Mobile") && secondaryPhoneInfo.gets("PhoneNumber").isEmpty())) {
          showMsg = true;
        }

        if (showMsg) {
          String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
              claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=ClaimantPerson.BestWayToContact");
          String claimantLink = ValidationRenderer.createActionLink(action);
          ValidationRenderer.addValidationError(claim, "Validation", "ERROR", "Best way to contact is mobile phone but mobile phone number is not entered on Claimant " + claimant.gets("ClaimantNumber"), claimantLink);
        }
      }

      if (bestWayToContact.equals("Business Phone")
          && !(primaryPhoneInfo.gets("PhoneName").equals("Business") || secondaryPhoneInfo.gets("PhoneName").equals("Business"))) {
        String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
            claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=ClaimantPerson.BestWayToContact");
        String claimantLink = ValidationRenderer.createActionLink(action);
        ValidationRenderer.addValidationError(claim, "Validation", "ERROR", "Best way to contact is business phone but business phone number is not entered on Claimant " + claimant.gets("ClaimantNumber"), claimantLink);
      }

      if (bestWayToContact.equals("Email") && emailInfo.gets("EmailAddr").isEmpty()) {
        String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
            claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=ClaimantPerson.BestWayToContact");
        String claimantLink = ValidationRenderer.createActionLink(action);
        ValidationRenderer.addValidationError(claim, "Validation", "ERROR", "Best way to contact is email but email is not entered on Claimant " + claimant.gets("ClaimantNumber"), claimantLink);
      }

      if (bestWayToContact.equals("Fax") && faxPhoneInfo.gets("PhoneNumber").isEmpty()) {
        String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
            claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=ClaimantPerson.BestWayToContact");
        String claimantLink = ValidationRenderer.createActionLink(action);
        ValidationRenderer.addValidationError(claim, "Validation", "ERROR", "Best way to contact is fax but fax number is not entered on Claimant " + claimant.gets("ClaimantNumber"), claimantLink);
      }
    }
  }

  //Validate Major Trauma Description
  private void triggerInjuryForMajorTraumaError(ModelBean claimant) throws Exception {
    String claimantName = claimant.getBean("NameInfo", "NameTypeCd", "ClaimantName").gets("CommercialName");

    if (!claimant.gets("MajorTraumaCd").equals("") && claimant.gets("InjuryDesc").equals("")) {
      String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
          claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=Claimant.InjuryDesc");
      String claimantLink = ValidationRenderer.createActionLink(action);
      ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
          String.format("Enter Description of Injury for Major Trauma on Claimant %s.", claimantName), claimantLink);
    }
  }

  private void triggerAssignedAdjusterErrors(ModelBean claimant) throws Exception {
    if (!StringRenderer.isTrue(claimant.gets("InjuryInvolvedInd"))) {
      //Injury Involved Indicator is Blank or No
      if (ClaimantTransactionRenderer.hasFeature(claimant, "CovF")) {
        String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
            claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=Litigation.SuitStatusCd");
        String claimantLink = ValidationRenderer.createActionLink(action);

        // If Liability Feature was Detected, Warn User that No Injury was Indicated
        ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "WARNING",
            "Liability Feature Detected - Injury Involved Not Indicated on Claimant #" + claimant.gets("ClaimantNumber"), claimantLink);
      }
    } else {
      if (claimant.valueEquals("InjuredPartyRelationshipCd", "")) {
        String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
            claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=Claimant.InjuredPartyRelationshipCd");
        String claimantLink = ValidationRenderer.createActionLink(action);
        ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
            String.format("Claimant Relationship to Insured Required on Claimant #%s", claimant.gets("ClaimantNumber")), claimantLink);
      }

      // Injury Involved Indicator is Yes
      if (ClaimantTransactionRenderer.hasORMorTPOCs(claimant)) {
        //Validate Medicare Beneficiary Information
        ModelBean injuredParty = claimant.getBean("InjuredParty");
        String medicareBeneficiaryCd = injuredParty.gets("MedicareBeneficiaryCd");

        if (medicareBeneficiaryCd.equals("")) {
          //Validate Medicare Beneficiary Selection
          String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
              claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=InjuredParty.MedicareBeneficiaryCd");
          String claimantLink = ValidationRenderer.createActionLink(action);
          ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
              String.format("Medicare validation - Injured Party Medicare Beneficiary Selection Required on Claimant #%s", claimant.gets("ClaimantNumber")), claimantLink);
        } else if (medicareBeneficiaryCd.equals("Yes")) {
          // Required Fields are Based on Medicare Section 111 Users Guide - Version 3.1 Dated July 2, 2010
          StringDate compareDtApril2010 = DateRenderer.getStringDate("20100401");
          StringDate compareDtJanuary2011 = DateRenderer.getStringDate("20110101");
          ModelBean injuredPartyName = BeanRenderer.getBeanWithAlias(injuredParty, "InjuredPartyName");
          ModelBean injuredPartyTax = BeanRenderer.getBeanWithAlias(injuredParty, "InjuredPartyTax");
          ModelBean injuredPartyPerson = BeanRenderer.getBeanWithAlias(injuredParty, "InjuredPartyPerson");
          String diagnosisCd1 = injuredParty.getBean("Diagnosis", "DiagnosisNumber", "1").gets("DiagnosisCd");
          ModelBean injuredPartyMailingAddr = BeanRenderer.getBeanWithAlias(injuredParty, "InjuredPartyMailingAddr");
          ModelBean injuredPartyPhone = BeanRenderer.getBeanWithAlias(injuredParty, "InjuredPartyPhonePrimary");

          // Validate Injured Party Information
          if (injuredPartyName.valueEquals("GivenName", "") || injuredPartyName.valueEquals("Surname", "")) {
            String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=InjuredPartyName.GivenName");
            String claimantLink = ValidationRenderer.createActionLink(action);
            ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                String.format("Medicare validation - First and Last Name Required on Medicare Injured Party [Claimant #%s]", claimant.gets("ClaimantNumber")), claimantLink);
          }

          if (injuredPartyPerson.valueEquals("BirthDt", "")) {
            String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=InjuredPartyPerson.BirthDt");
            String claimantLink = ValidationRenderer.createActionLink(action);
            ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                String.format("Medicare validation - Birthdate Required on Medicare Injured Party [Claimant #%s]", claimant.gets("ClaimantNumber")), claimantLink);
          }

          if (injuredPartyMailingAddr.valueEquals("Addr1", "") || injuredPartyMailingAddr.valueEquals("City", "") || injuredPartyMailingAddr.valueEquals("StateProvCd", "") || injuredPartyMailingAddr.valueEquals("PostalCode", "")) {
            String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=InjuredPartyMailingAddr.Addr1");
            String claimantLink = ValidationRenderer.createActionLink(action);
            ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                String.format("Medicare validation - Mailing Address information Required on Medicare Injured Party [Claimant #%s]", claimant.gets("ClaimantNumber")), claimantLink);
          }

          if (injuredPartyPhone.valueEquals("PhoneNumber", "")) {
            String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=InjuredPartyPhonePrimary.PhoneNumber");
            String claimantLink = ValidationRenderer.createActionLink(action);
            ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                String.format("Medicare validation - Phone Number Required on Medicare Injured Party [Claimant #%s]", claimant.gets("ClaimantNumber")), claimantLink);
          }

          if (injuredParty.valueEquals("HealthInsuranceClaimNumber", "") && injuredPartyTax.valueEquals("SSN", "")) {
            String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=InjuredPartyTax.SSN");
            String claimantLink = ValidationRenderer.createActionLink(action);
            ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                String.format("Medicare validation - Health Insurance Claim Number or SSN Required on Medicare Injured Party [Claimant #%s]", claimant.gets("ClaimantNumber")), claimantLink);
          }

          if (DateRenderer.compareTo(todayDt, compareDtJanuary2011) >= 0) {
            //On or After January 1, 2011
            if (injuredParty.valueEquals("InjuryCauseCd", "") || diagnosisCd1.equals("")) {
              String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                  claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=InjuredParty.InjuryCauseCd");
              String claimantLink = ValidationRenderer.createActionLink(action);
              ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                  String.format("Medicare validation - Injury Cause Code and Diagnosis Code 1 Required on Medicare Injured Party [Claimant #%s]", claimant.gets("ClaimantNumber")), claimantLink);
            }
          } else {
            //Through December 31, 2010
            if ((injuredParty.valueEquals("InjuryCauseCd", "") || diagnosisCd1.equals("")) && claimant.valueEquals("InjuryDesc", "")) {
              String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                  claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=InjuredParty.InjuryCauseCd");
              String claimantLink = ValidationRenderer.createActionLink(action);
              ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                  String.format("Medicare validation - Injury Cause Code and Diagnosis Code 1 Required on Medicare Injured Party, or Injury Description Required [Claimant #%s]", claimant.gets("ClaimantNumber")), claimantLink);
            }
          }

          if (injuredParty.valueEquals("StateOfVenue", "")) {
            String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=InjuredParty.StateOfVenue");
            String claimantLink = ValidationRenderer.createActionLink(action);
            ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                String.format("Medicare validation - State Of Venue Required on Medicare Injured Party [Claimant #%s]", claimant.gets("ClaimantNumber")), claimantLink);
          }

          //Validate Injured Party Representative Information
          if (injuredParty.valueEquals("RepresentativeTypeCd", "")) {
            String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=InjuredParty.RepresentativeTypeCd");
            String claimantLink = ValidationRenderer.createActionLink(action);
            ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                String.format("Medicare validation - Injured Party Representative Indicator Required on Medicare Injured Party [Claimant #%s]", claimant.gets("ClaimantNumber")), claimantLink);
          } else if (!injuredParty.valueEquals("RepresentativeTypeCd", "None")) {
            ModelBean injuredPartyRepresentativeName = BeanRenderer.getBeanWithAlias(injuredParty, "InjuredPartyRepresentativeName");
            ModelBean injuredPartyRepresentativeMailingAddr = BeanRenderer.getBeanWithAlias(injuredParty, "InjuredPartyRepresentativeMailingAddr");
            ModelBean injuredPartyRepresentativePhonePrimary = BeanRenderer.getBeanWithAlias(injuredParty, "InjuredPartyRepresentativePhonePrimary");
            if (injuredPartyRepresentativeName.valueEquals("CommercialName", "")) {
              if (injuredPartyRepresentativeName.valueEquals("GivenName", "") && injuredPartyRepresentativeName.valueEquals("Surname", "")) {
                String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                    claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=InjuredPartyRepresentativeName.CommercialName");
                String claimantLink = ValidationRenderer.createActionLink(action);
                ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                    String.format("Medicare validation - Either Firm Name or First and Last Name Required on Medicare Injured Party Representative [Claimant #%s]", claimant.gets("ClaimantNumber")), claimantLink);
              }
              if (!injuredPartyRepresentativeName.valueEquals("GivenName", "") && injuredPartyRepresentativeName.valueEquals("Surname", "")) {
                String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                    claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=InjuredPartyRepresentativeName.Surname");
                String claimantLink = ValidationRenderer.createActionLink(action);
                ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                    String.format("Medicare validation - Last Name Required on Medicare Injured Party Representative [Claimant #%s]", claimant.gets("ClaimantNumber")), claimantLink);
              }
              if (injuredPartyRepresentativeName.valueEquals("GivenName", "") && !injuredPartyRepresentativeName.valueEquals("Surname", "")) {
                String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                    claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=InjuredPartyRepresentativeName.GivenName");
                String claimantLink = ValidationRenderer.createActionLink(action);
                ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                    String.format("Medicare validation - First Name Required on Medicare Injured Party Representative [Claimant #%s]", claimant.gets("ClaimantNumber")), claimantLink);
              }
            }
            if (injuredPartyRepresentativeMailingAddr.valueEquals("Addr1", "") || injuredPartyRepresentativeMailingAddr.valueEquals("City", "") || injuredPartyRepresentativeMailingAddr.valueEquals("StateProvCd", "") || injuredPartyRepresentativeMailingAddr.valueEquals("PostalCode", "")) {
              String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                  claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=InjuredPartyRepresentativeMailingAddr.Addr1");
              String claimantLink = ValidationRenderer.createActionLink(action);
              ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                  String.format("Medicare validation - Complete Address Required on Medicare Injured Party Representative [Claimant #%s]", claimant.gets("ClaimantNumber")), claimantLink);
            }
            if (injuredPartyRepresentativePhonePrimary.valueEquals("PhoneNumber", "")) {
              String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                  claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=InjuredPartyRepresentativePhonePrimary.PhoneName");
              String claimantLink = ValidationRenderer.createActionLink(action);
              ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                  String.format("Medicare validation - Phone Number Required on Medicare Injured Party Representative [Claimant #%s]", claimant.gets("ClaimantNumber")), claimantLink);
            }
          }

          //Validate Claimant Information
          String injuredPartyRelationshipCd = claimant.gets("InjuredPartyRelationshipCd");
          if (!injuredPartyRelationshipCd.equals("") && !injuredPartyRelationshipCd.equals("Self")) {
            ModelBean claimantTax = BeanRenderer.getBeanWithAlias(claimant, "ClaimantTax");
            ModelBean claimantName = BeanRenderer.getBeanWithAlias(claimant, "ClaimantName");
            ModelBean claimantMailingAddr = BeanRenderer.getBeanWithAlias(claimant, "ClaimantMailingAddr");
            ModelBean claimantPhonePrimary = BeanRenderer.getBeanWithAlias(claimant, "ClaimantPhonePrimary");

            if (claimantTax.valueEquals("FEIN", "")) {
              String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                  claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=ClaimantTax.TaxIdTypeCd");
              String claimantLink = ValidationRenderer.createActionLink(action);
              ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                  String.format("Medicare validation - Tax ID Number Required on Claimant #%s for Medicare Beneficiary", claimant.gets("ClaimantNumber")), claimantLink);
            }

            if (DateRenderer.compareTo(todayDt, compareDtApril2010) >= 0) {
              //On or After April 1, 2010
              if (injuredPartyRelationshipCd.equals("Estate - Individual") || injuredPartyRelationshipCd.equals("Family Member - Individual") || injuredPartyRelationshipCd.equals("Other - Individual")) {
                if (claimantName.valueEquals("GivenName", "") || claimantName.valueEquals("Surname", "")) {
                  String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                      claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=ClaimantName.GivenName");
                  String claimantLink = ValidationRenderer.createActionLink(action);
                  ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR", "Medicare validation - First and Last Name Required on Claimant #%s for Medicare Beneficiary", claimantLink);
                }
              }
              if (injuredPartyRelationshipCd.equals("Estate - Entity") || injuredPartyRelationshipCd.equals("Family Member - Entity") || injuredPartyRelationshipCd.equals("Other - Entity")) {
                if (claimantName.valueEquals("CommercialName", "")) {
                  String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                      claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=ClaimantName.CommercialName");
                  String claimantLink = ValidationRenderer.createActionLink(action);
                  ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                      String.format("Medicare validation - Entity/Organization Name Required on Claimant #%s for Medicare Beneficiary", claimant.gets("ClaimantNumber")), claimantLink);
                }
              }
              if (claimantMailingAddr.valueEquals("Addr1", "") || claimantMailingAddr.valueEquals("City", "") || claimantMailingAddr.valueEquals("StateProvCd", "") || claimantMailingAddr.valueEquals("PostalCode", "")) {
                String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                    claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=ClaimantMailingAddr.Addr1,ClaimantMailingAddr.City,ClaimantMailingAddr.StateProvCd,ClaimantMailingAddr.PostalCode");
                String claimantLink = ValidationRenderer.createActionLink(action);
                ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                    String.format("Medicare validation - Complete Address Required on Claimant #%s for Medicare Beneficiary", claimant.gets("ClaimantNumber")), claimantLink);
              }
              if (claimantPhonePrimary.valueEquals("PhoneNumber", "")) {
                String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                    claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam + "&HighlightFields=ClaimantPhonePrimary.PhoneName");
                String claimantLink = ValidationRenderer.createActionLink(action);
                ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                    String.format("Medicare validation - Phone Number Required on Claimant #%s for Medicare Beneficiary", claimant.gets("ClaimantNumber")), claimantLink);
              }
            }
          }

          //Validate Litigation Associate
          ModelBean litigationAssociate = claimant.getBean("Litigation").getBean("LitigationAssociate", "StatusCd", "Active");
          if (litigationAssociate != null) {
            ModelBean litigationAssociateTax = BeanRenderer.getBeanWithAlias(litigationAssociate, "LitigationAssociateTax");
            ModelBean litigationAssociatePhonePrimary = BeanRenderer.getBeanWithAlias(litigationAssociate, "LitigationAssociatePhonePrimary");

            if (DateRenderer.compareTo(todayDt, compareDtJanuary2011) >= 0) {
              //On or After January 1, 2011
              if (litigationAssociate.valueEquals("AssociateTypeCd", "Plaintiff Counsel") || litigationAssociate.valueEquals("AssociateTypeCd", "Defense Counsel")) {
                ModelBean litigationNameInfo = BeanRenderer.getBeanWithAlias(litigationAssociate, "LitigationAssociateName");
                if (litigationNameInfo.valueEquals("CommercialName", "")) {
                  String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                      claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam);
                  String claimantLink = ValidationRenderer.createActionLink(action);
                  ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                      String.format("Medicare validation - Firm Name Required on Claim Associate #%s [Claimant #%s]",
                          litigationAssociate.gets("AssociateNumber"), claimant.gets("ClaimantNumber")), claimantLink);
                }
              }
            }

            if (litigationAssociateTax.valueEquals("FEIN", "")) {
              String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                  claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam);
              String claimantLink = ValidationRenderer.createActionLink(action);
              ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                  String.format("Medicare validation - Tax ID Number Required on Claim Associate #%s [Claimant #%s]",
                      litigationAssociate.gets("AssociateNumber"), claimant.gets("ClaimantNumber")), claimantLink);
            }

            if (litigationAssociatePhonePrimary.valueEquals("PhoneNumber", "")) {
              String action = String.format("javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')",
                  claim.getSystemId(), claim.getId(), claimant.getId(), claimantParam);
              String claimantLink = ValidationRenderer.createActionLink(action);
              ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR",
                  String.format("Medicare validation - Phone Number Required on Claim Associate #%s [Claimant #%s]",
                      litigationAssociate.gets("AssociateNumber"), claimant.gets("ClaimantNumber")), claimantLink);
            }
          }
        }
      }
    }
  }
}
