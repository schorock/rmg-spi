package com.ric.claims.common.product.model.template.ric.dwellingproperty.cw.v01_00_00.rule;

import com.iscs.claims.claim.render.ClaimRenderer;
import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.render.DateRenderer;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.utility.validation.render.ValidationRenderer;
import com.ric.common.business.rule.RICClaimsRuleProcessor;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

public class ClaimantTransactionValidation implements RuleProcessor {
	
	private ModelBean cmmParams;
	private ModelBean claimant;
	private ModelBean claimantTransaction;
	
	private ModelBean claim;
	private ModelBean oldClaim;
	private ModelBean response;
	private ModelBean request;
	private StringDate todayDt;
	private ModelBean errors;
	private ModelBean user;
	private JDBCData data;
	private Boolean isNewClaim;
	private String transactionCd;
	
	@Override
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user,
			JDBCData data) throws RuleException {
		
		try {
			claim = bean;
			todayDt = new StringDate(DateRenderer.getDate());
			request = additionalBeans[1];
			response = additionalBeans[2];
			errors = response.getBean("ResponseParams").getBean("Errors");
			this.user = user;
			this.data = data;
			isNewClaim = claim.gets("ClaimRef").equals("");
			cmmParams = response.getBean("ResponseParams").getBean("CMMParams");
			claimant = claim.getBeanById(cmmParams.gets("ParentIdRef"));
			claimantTransaction = claimant.getBeanById(cmmParams.gets("IdRef"));
			
			if( claimantTransaction == null )
				return errors;
			
			transactionCd = claimantTransaction.gets("TransactionCd");
			
			if( StringRenderer.greaterThan(claim.gets("TransactionNumber"), "1") ){
				oldClaim = ClaimRenderer.getClaim(data, claim.gets("ClaimRef"));
			}
			
			processRules();
			
		} catch(Exception e) {
			throw new RuleException(e);
		}
		
		return errors;
	}

	public void processRules() throws Exception {
		ValidationRenderer.clearValidationErrors(claim,"ClaimantTransactionValidation");
		
		if( transactionCd.equals("Adjust Reserve") ){
			triggerMakeReserveAdjustmentOnReadOnlyError();
		}
		
		if( transactionCd.equals("Payment") ){
			triggerMakePaymentOnReadOnlyError();
		}
	}
	
	/**
	 *  Prevent attempt to reserve/pay on Record-Only claims
	 * @throws Exception
	 */
	private void triggerMakeReserveAdjustmentOnReadOnlyError() throws Exception {
		if( claim.gets("ForRecordOnlyInd").equals("Yes") ){
			for( ModelBean featureAllocation : claimantTransaction.getBeans("FeatureAllocation") ){
				if( !featureAllocation.gets("ReserveAmt").isEmpty() && !StringRenderer.equal(featureAllocation.gets("ReserveAmt"), "0") ){
					ValidationRenderer.addValidationError(claim,"ClaimantTransactionValidation","ERROR", "Reserve changes cannot be made on Record-Only claims" );
					break;
				}
			}
		}
	}
	
	private void triggerMakePaymentOnReadOnlyError() throws Exception {
		if( claim.gets("ForRecordOnlyInd").equals("Yes") )
			ValidationRenderer.addValidationError(claim,"ClaimantTransactionValidation","ERROR", "Payments cannot be made on Record-Only claims" );
	}
	
}
