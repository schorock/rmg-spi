package com.ric.claims.common.product.model.template.ric.homeowners.cw.v01_00_00.rule;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;

import com.iscs.common.business.rule.RuleException;

/** Defaults the Notice with Loss Information 
 * 
 * @author moniquef
 */
public class NoticeBuilder extends com.iscs.claims.notice.NoticeBuilder {

	// Rule Processor Interface
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user, JDBCData data) 
	throws RuleException {	
		return super.process(ruleTemplate, bean, additionalBeans, user, data);
	} 	
}
