/*
 * TPCClaimantTransactionRenderer.java
 *
 */

package com.ric.claims.common.product.model.template.ric.dwellingproperty.render;

import java.util.ArrayList;

import net.inov.mda.MDAOption;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.math.Money;

import com.iscs.claims.claim.Claim;
import com.iscs.claims.common.Feature;
import com.iscs.claims.transaction.render.ClaimantTransactionRenderer;
import com.iscs.common.business.provider.Provider;
import com.iscs.common.tech.form.field.FormFieldRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.insurance.product.ProductSetup;

/** Rendering Tools for the ClaimantTransaction
 *
 * @author  allend
 */
public class DwellingClaimantTransactionRenderer extends ClaimantTransactionRenderer{
    
     
    /** Creates a new instance of ClaimantTransactionRenderer */
    public DwellingClaimantTransactionRenderer() {
    }
    
    /** Context Variable Getter
      * @return ClaimantTransactionRenderer
      */     
    public String getContextVariableName() {
         return "DwellingClaimantTransactionRenderer";
    }

    /** Get Interest Payee Select List
     * @param fieldId Field Identifier
     * @param defaultValue Select List Default Value
     * @param claim ModelBean Containing the Additional Interests
     * @param claimant ModelBean
     * @param meta Parameters
     * @return String HTML Select Tag
     * @throws Exception if an Unexpected Error Occurs
     */
    public static String interestPayeeFirstParty(ModelBean claim, ModelBean claimant)
    throws Exception {
        try {
            String returnString = "";
        	// Do Not Display Interests on Third Party Claimants
        	if( claimant.valueEquals("ClaimantTypeCd", "First Party") ) {
	        
        		// Get the Policy Additional Interests Linked to the Risk on the Claim
	            ModelBean[] policyAIArray = Claim.getPolicyAIs(claim);
	            BeanTools.sortBeansBy(policyAIArray, new String[] {"TypeCd"}, new String[] {"Ascending"});
	            for( ModelBean policyAI : policyAIArray ) {
	            	if (policyAI.gets("TypeCd").startsWith("Additional Insured") || policyAI.gets("TypeCd").startsWith("Owner-Occupant")){
	            		returnString += " and " + policyAI.getBean("PartyInfo").getBean("NameInfo").gets("IndexName");
	            	}
	            }
        	}
            return returnString;            
        }
        catch( Exception e ) {
            e.printStackTrace();
            Log.error(e);
            return "";
        }
    }
    
    
    /** Get Prior Used Payee Select List
     * @param fieldId Field Identifier
     * @param defaultValue Select List Default Value
     * @param data JDBCData
     * @param claim ModelBean Containing the Prior Claimant Transactions
     * @param meta Parameters
     * @return String HTML Select Tag
     * @throws Exception if an Unexpected Error Occurs
     */
    public static String vendorPayeeSelectField( String fieldId, String defaultValue, JDBCData data, ModelBean claim, ModelBean claimant, String meta, String productVersionIdRef, String umol )
    throws Exception {
        try {
        	ArrayList<MDAOption> array = new ArrayList<MDAOption>();
        	
        	ArrayList<String> priorProviderNumberArray = new ArrayList<String>();
        	
            ModelBean[] claimantTransactionArray = claim.findBeansByFieldValue("ClaimantTransaction","StatusCd","Active");
            
            String priorProviderNumber = "";
            for( ModelBean claimantTransaction : claimantTransactionArray ) {
            	
            	// Load Prior Provider Payees
            	if( claimantTransaction.valueEquals("PayToProviderInd","Yes") ) {
            		int providerRef = claimantTransaction.getValueInt("ProviderRef");
            		ModelBean provider = Provider.getProviderBySystemId(data, providerRef);
            		if( provider != null ) {
	            		String providerNumber = provider.gets("ProviderNumber");
	            		if( !providerNumber.equals(priorProviderNumber) && !providerNumber.equals("") ) {
			                if( !provider.getSystemId().equals("") ) { 
			                	ModelBean nameInfo = BeanTools.getBeanWithAlias(provider,"ProviderName");
			                	ModelBean taxInfo = BeanTools.getBeanWithAlias(provider,"ProviderTaxInfo");
			                	
			                	// Return the appropriate Tax ID
			                	String taxId = "";
			                	if( taxInfo.gets("TaxIdTypeCd").equals("FEIN") )
			                		taxId = taxInfo.gets("FEIN");
			                	else if( taxInfo.gets("TaxIdTypeCd").equals("SSN") )
			                		taxId = taxInfo.gets("SSN");
			                	
			                	MDAOption option = new MDAOption();
				                option.setValue("ProviderNumber::" + providerNumber + "::" + taxInfo.gets("Required1099Ind") + "::" + taxInfo.gets("TaxIdTypeCd") + "::" + taxId);
				                option.setLabel(nameInfo.gets("CommercialName"));
				                array.add(option);
				                
				                priorProviderNumberArray.add(providerNumber);
			                }
	            		}
		                priorProviderNumber = providerNumber;
            		}
            	}
            }
            
            // Load Assigned Providers
            String[] priorProviderNumbers = (String[]) priorProviderNumberArray.toArray(new String[priorProviderNumberArray.size()]);
            ModelBean[] assignedProvider = claimant.getBeans("AssignedProvider");
            
            ModelBean coderef = ProductSetup.getProductCoderef(productVersionIdRef, umol);
            ModelBean[] list = coderef.getBean("Options").getBeans("Option");
            String providerList = (String) list[0].getValue("name");
            
            for( int p = 1; p < list.length; p++ ){
            	providerList = providerList+ ","+ (String) list[p].getValue("name");
            }
            
           
            for( int i = 0; i < assignedProvider.length; i++ ) {
            	int providerRef = assignedProvider[i].getValueInt("ProviderRef");
            	if( providerRef > 0 ) {
	            	String assignedProviderNumber = Provider.getProviderBySystemId(data, providerRef).gets("ProviderNumber");
	            	String providerTypeCd = (String) assignedProvider[i].getValue("ProviderTypeCd");
	    			
	            	if( !assignedProviderNumber.equals("") && StringTools.in(providerTypeCd, providerList)){
	            		// Check if the Assigned Provider has already been added as a Prior Provider
	            		if( StringTools.searchStringArray(assignedProviderNumber, priorProviderNumbers) == -1 ) {
	            			ModelBean provider = Provider.getProvider(data, assignedProviderNumber);	                
			                if( !provider.getSystemId().equals("") ) { 
			                	ModelBean nameInfo = BeanTools.getBeanWithAlias(provider,"ProviderName");
			                	ModelBean taxInfo = BeanTools.getBeanWithAlias(provider,"ProviderTaxInfo");
		 
			                	// Return the appropriate Tax ID
			                	String taxId = "";
			                	if( taxInfo.gets("TaxIdTypeCd").equals("FEIN") )
			                		taxId = taxInfo.gets("FEIN");
			                	else if( taxInfo.gets("TaxIdTypeCd").equals("SSN") )
			                		taxId = taxInfo.gets("SSN");
			                	
			                	MDAOption option = new MDAOption();
				                option.setValue("ProviderNumber::" + assignedProviderNumber + "::" + taxInfo.gets("Required1099Ind") + "::" + taxInfo.gets("TaxIdTypeCd") + "::" + taxId);
				                option.setLabel(nameInfo.gets("CommercialName"));
				                array.add(option);
			                }
	            		}
	            	}
            	}
            }
            
            MDAOption[] options = (MDAOption[]) array.toArray(new MDAOption[array.size()]);
            return FormFieldRenderer.renderSelect(fieldId, defaultValue, options, "SelectList", meta);
        }
        catch( Exception e ) {
            e.printStackTrace();
            Log.error(e);
            return "";
        }
    }
    
    /** Get the limits of the feature code
     * 
     * @param claim ModelBean claim
     * @param featureName The feature code to look up 
     * @param showAggregate boolean indicator to show the aggregate limit also
     * @param showDescription boolean indicator to show the limit description
     * @param referenceId The reference id to look up the feature belonging to it 
     * @return The limits of the feature
     */
    public static String getFeatureLimits(ModelBean claim, String featureName, boolean showAggregate, boolean showDescription, String referenceId) {
    	try {
    		if (referenceId.equals("")) {
            	return getFeatureLimits(claim, featureName, showAggregate, showDescription);    			
    		}
    		ModelBean feature = parseFeatureName(featureName);
    		feature.setValue("PropertyDamagedIdRef", referenceId);
    		
			return getFeatureLimit(claim, feature, showAggregate, showDescription, true, false, true);    			
    		
    	} catch( Exception e ) {
            e.printStackTrace();
            Log.error("error", e);
    		return "";
    	}
    }
    
    
    /** Get the limit(s) of the feature 
     * 
     * @param claim ModelBean claim
     * @param feature The feature ModelBean 
     * @param showAggregate boolean indicator to show the aggregate limit also
     * @param dollarSignInd boolean indicator to show dollar sign in the limit amount(s)
     * @param centsInd boolean indicator to show cents in the limit amount(s)
     * @param commaSeparatorInd boolean indicator to show comma separators in the limit amount(s)
     * @return The limit(s) of the feature in String form
     */
    public static String getFeatureLimit(ModelBean claim, ModelBean feature, boolean showAggregate, boolean showDescription, boolean dollarSignInd, boolean centsInd, boolean commaSeparatorInd) 
    throws Exception {
    	String featureItem = feature.gets("ItemNumber", null);
    	String featureSubCd = feature.gets("FeatureSubCd", null);
    	
    	String limitDescription = "";
    	StringBuffer limit = new StringBuffer();
    	    	
    	ModelBean coverage = Feature.getClaimPolicyLimit(claim, feature);

    	if( coverage != null ) {
			// Determine which limit to use
			String limitAmt;
			if (featureItem != null) {
				ModelBean item = coverage.getBean("ClaimPolicySubLimit", "ItemNumber", featureItem);
				limitAmt = Feature.getCoverageLimit(claim, item);
				limitDescription = item.gets("LimitDescription");
			} else if (featureSubCd != null ) {
				return "";
			} else {
				limitAmt = Feature.getCoverageLimit(claim, coverage);	
				limitDescription = coverage.gets("LimitDescription");
			}
		
			// Format the limit and add it to the String result
			if (limitAmt.length() > 0) {
				if (StringTools.isNumeric(limitAmt))
					limit.append(StringTools.formatMoney(limitAmt, dollarSignInd, centsInd, commaSeparatorInd));
				else
					limit.append(limitAmt);
			}
					
			// Append the aggregate value if requested
			if (showAggregate) {
				if (coverage.getBeanName().equals("ClaimPolicyLimit")) {
					String limitAggregate = Feature.getCoverageAggregateLimit(claim, coverage);
					if (limitAggregate.length() > 0) {
						limit.append("/");
						if (StringTools.isNumeric(limitAggregate))
							limit.append(StringTools.formatMoney(limitAggregate, dollarSignInd, centsInd, commaSeparatorInd));
						else
							limit.append(limitAggregate);
					}
				}
			}
		}
		
		// Append the limit description if requested
		if (showDescription) {
			if (limitDescription.length() > 0)
				limit.append(" (" + limitDescription + ")");
		}
		return limit.toString();
    	
    }
   
    /** Get the limits of the feature code
     * 
     * @param claim ModelBean claim
     * @param featureName The feature code to look up 
     * @param showAggregate boolean indicator to show the aggregate limit also
     * @param showDescription boolean indicator to show the limit description
     * @return The limits of the feature
     */
    public static String getFeatureLimits(ModelBean claim, String featureName, boolean showAggregate, boolean showDescription) {
    	try {
    		ModelBean feature = parseFeatureName(featureName);
			return getFeatureLimit(claim, feature, showAggregate, showDescription, true, false, true);    			
    		
    	} catch( Exception e ) {
            e.printStackTrace();
            Log.error("error", e);
    		return "";
    	}
    }
    
    
	 /** Get the limits of the feature code
     * 
     * @param claim ModelBean claim
     * @param featureName The feature code to look up 
     * @param showDescription boolean indicator to show the limit description
     * @param referenceId The reference id to look up the feature belonging to it 
     * @return The limits of the feature
     */
    public static String getFeatureLimits(ModelBean claim, String featureName, boolean showDescription, String referenceId) {
    	if (referenceId.equals("")) {
        	return getFeatureLimits(claim, featureName, true, showDescription);
    	}
    	return getFeatureLimits(claim, featureName, true, showDescription, referenceId);
    }
    
    /** Get the deductible of the feature code
     * 
     * @param claim ModelBean claim
     * @param featureName The feature code to look up 
     * @return The deductible of the feature
     */
    public static String getFeatureDeductible(ModelBean claim, String featureName, boolean showDescription) {
    	try {
    		
    		ModelBean feature = ClaimantTransactionRenderer.parseFeatureName(featureName);
			return getFeatureDeductible(claim, feature, showDescription, true, false, true);    			
    		
    	} catch( Exception e ) {
            e.printStackTrace();
            Log.error("error", e);
    		return "";
    	}
    }

    /** Get the deductible of the feature code
     * 
     * @param claim ModelBean claim
     * @param featureName The feature code to look up 
     * @param referenceId The reference id that the feature belongs to
     * @return The deductible of the feature
     */
    public static String getFeatureDeductible(ModelBean claim, String featureName, boolean showDescription, String referenceId) {
    	try {    		
    		ModelBean feature = ClaimantTransactionRenderer.parseFeatureName(featureName);
    		feature.setValue("PropertyDamagedIdRef", referenceId);
			return getFeatureDeductible(claim, feature, showDescription, true, false, true);    			
    		
    	} catch( Exception e ) {
            e.printStackTrace();
            Log.error("error", e);
    		return "";
    	}
    }

    /** Get the deductible of the feature code
     * 
     * @param claim ModelBean claim
     * @param featureName The feature code to look up 
     * @param referenceId The reference id that the feature belongs to
     * @return The deductible of the feature
     */
    public static String getFeatureDeductible(ModelBean claim, ModelBean feature, boolean showDescription, String referenceId) {
    	try {
			return getFeatureDeductible(claim, feature, showDescription, true, false, true);    			
    		
    	} catch( Exception e ) {
            e.printStackTrace();
            Log.error("error", e);
    		return "";
    	}
    }
    
    /** Get the limit(s) of the feature 
     * 
     * @param claim ModelBean claim
     * @param feature The feature ModelBean 
     * @param dollarSignInd boolean indicator to show dollar sign in the limit amount(s)
     * @param centsInd boolean indicator to show cents in the limit amount(s)
     * @param commaSeparatorInd boolean indicator to show comma separators in the limit amount(s)
     * @return The limit(s) of the feature in String form
     */
    public static String getFeatureDeductible(ModelBean claim, ModelBean feature, boolean showDescription, boolean dollarSignInd, boolean centsInd, boolean commaSeparatorInd) 
    throws Exception {
    	String featureItem = feature.gets("ItemNumber", null);
    	String featureSubCd = feature.gets("FeatureSubCd", null);
    	
    	ModelBean coverage = Feature.getClaimPolicyLimit(claim, feature);
    	
		if( coverage == null )
			return "";
		
		// Determine which deductible to use
		String deductAmt;
		String deductDescription = "";
		if (featureItem != null) {
			ModelBean item = coverage.getBean("ClaimPolicySubLimit", "ItemNumber", featureItem);
			deductDescription = item.gets("DeductibleDescription");
			deductAmt = getCoverageDeductible(claim, item, deductDescription);			
		} else if (featureSubCd != null ) {
			ModelBean[] covItems = coverage.findBeansByFieldValue("ClaimPolicySubLimit", "Category", feature.gets("FeatureSubCd"));
			deductAmt = "0.00";
			if( covItems.length > 0 ) {
				deductDescription = covItems[0].gets("DeductibleDescription");
				deductAmt = getCoverageDeductible(claim, covItems[0],deductDescription);				
			}	
		} else {
			deductDescription = coverage.gets("DeductibleDescription");
			deductAmt = getCoverageDeductible(claim, coverage,deductDescription);	
			
		}
		
		StringBuffer deduct = new StringBuffer();
		
		// Format the deductible and add it to the String result
		if (deductAmt.length() > 0) 
			if(!deductAmt.contains("%"))
			  deduct.append(StringTools.formatMoney(deductAmt, dollarSignInd, centsInd, commaSeparatorInd));	
			else
				deduct.append(deductAmt);
				
		// Append the deductible description if requested
		if (showDescription) {
			if (deductDescription.length() > 0)
				deduct.append(" (" + deductDescription + ")");
		}
		return deduct.toString();
    	
    }
    /** Obtain the current coverage deductible.  The best deductible to use will be returned, based on the claim and
     * policy settings
     * 
     * @param claim The claim ModelBean containing the claim and policy information
     * @param coverage The coverage ModelBean in question 
     * @throws Exception when an error occurs
     */
    public static String getCoverageDeductible(ModelBean claim, ModelBean coverage, String deductDescription) 
    throws Exception {
    	String deductOver = coverage.gets("DeductibleOverridden");
    	if(!deductDescription.equalsIgnoreCase("Wind/Hail Ded")){
	    	if (deductOver.length() > 0) {
				return getCleanAmount(deductOver);
	    	} else {
	    		String deductAdjusted = coverage.gets("DeductibleAdjusted");
	    		if (deductAdjusted.length() > 0) {
	    			return getCleanAmount(deductAdjusted);
	    		} else {
	    			return getCleanAmount(coverage.gets("Deductible"));    			
	    		}
	    	}
      }else{
    	  if (deductOver.length() > 0) {
				return deductOver;
	    	} else {
	    		String deductAdjusted = coverage.gets("DeductibleAdjusted");
	    		if (deductAdjusted.length() > 0) {
	    			return deductAdjusted;
	    		} else {
	    			return coverage.gets("Deductible");    			
	    		}
	    	}
      }	  
    }
    
    /** Return a clean amount.  Zero amounts will not be shown at all
     * 
     * @param amount The amount in String form
     * @return The amount in clean form.
     */
    private static String getCleanAmount(String amount) {
    	amount = StringTools.stripNumberFormatChars(amount);
		if (StringTools.isNumeric(amount)) {
			// Make sure the limit is non-zero if numeric
			if (Float.parseFloat(amount) == 0) 
				return "";
		}
		return amount;    	
    }
    
    public static Money sumPaidAmt(ModelBean claim) {
		try {
			Money totalPaidAmt = new Money("0.00");
			ModelBean[] claimantTransactionArray = claim.getAllBeans("ClaimantTransaction");
			for( ModelBean claimantTransaction : claimantTransactionArray ) {
				if(!claimantTransaction.gets("PaymentTypeCd").equalsIgnoreCase("Adjustment"))
					totalPaidAmt = totalPaidAmt.add(new Money(claimantTransaction.gets("PaidAmt")));
			}
			return totalPaidAmt;
		} catch (Exception e) {
    		e.printStackTrace();
    		Log.error(e);
    		return new Money("0.00");
		}
	}
    
}
