package com.ric.claims.common.product.model.template.ric.homeowners;


import net.inov.tec.beans.ModelBean;

import net.inov.tec.data.JDBCData;

import com.iscs.claims.claim.Claimant;
import com.iscs.common.business.provider.Provider;
import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.tech.log.Log;

/** Sample Product Rule that Defaults Examiner and Adjuster on the Claim and its Claimants.
 * 
 * 
 * @author alavu
 */

public class AdjusterAssignment implements RuleProcessor {

    // Rule processor interface
    public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user, JDBCData data)
    throws RuleException {

		ModelBean errors = null;
		try {
		   
		    Log.debug("Processing AdjusterAssignment...");
		    ModelBean claim = bean;
		    errors = new ModelBean("Errors");
		    ModelBean provider = null;
		    
		    // Determine if the Loss Cause allows automatic assignment
		    String lossCauseCd = claim.gets("LossCauseCd");
		    if( !lossCauseCd.equals("Explosion") ) {
		    
			    String examinerId = claim.gets("ExaminerProviderRef");	  
	            if (examinerId.equals("")) {
	            	// Hard code Claim Examiner to 'jimexaminer'   
	            	String providerNumber = "jimexaminer";            	
		        	provider = Provider.getProvider(data, providerNumber, "Examiner", true);
	                if(provider!=null && provider.valueEquals("StatusCd","Active")){
	                	int systemId = Integer.parseInt(provider.getSystemId());
	                	claim.setValue("ExaminerProviderRef", Integer.toString(systemId));
	                }
	            }      
	        	
	          	ModelBean[] claimants = Claimant.getValidClaimants(claim);
	          	if( claimants.length > 0 ) {
	          		String providerNumber = "andyadjuster";            	
		        	provider = Provider.getProvider(data, providerNumber, "Adjuster", true);
	          	}
	         	for( ModelBean claimant: claimants ) {
	         		if( Claimant.getAdjusterId(claimant) == 0 ) {
	         			// Hard code the Claimant Adjuster to 'andyadjuster'       			
	                    if( provider!=null && provider.valueEquals("StatusCd","Active") ){
	                    	int adjusterId = Integer.parseInt(provider.getSystemId());
	                    	ModelBean adjuster = claimant.getBeanByAlias("AssignedAdjuster");
	                		adjuster.setValue("ProviderRef", Integer.toString(adjusterId));                		
	                		adjuster.setValue("ProviderTypeCd", provider.gets("ProviderTypeCd"));
	                    }	        			
	         		}
	           	}	
		    }
         
		} catch(Exception e){	
			throw new RuleException(e);
		}
		return errors;	
    }
}
   