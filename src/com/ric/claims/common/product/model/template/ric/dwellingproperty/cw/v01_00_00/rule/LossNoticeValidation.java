package com.ric.claims.common.product.model.template.ric.dwellingproperty.cw.v01_00_00.rule;

import com.iscs.claims.claim.Claim;
import com.iscs.claims.claim.render.ClaimRenderer;
import com.iscs.claims.notice.render.NoticeRenderer;
import com.iscs.claims.transaction.render.ClaimantTransactionRenderer;
import com.iscs.common.render.DateRenderer;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.render.VelocityTools;
import com.iscs.common.utility.validation.render.ValidationRenderer;
import com.iscs.uw.policy.Policy;
import com.iscs.uw.policy.render.PolicyRenderer;
import com.ric.common.business.rule.RICClaimsRuleProcessor;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.date.StringDate;

public class LossNoticeValidation extends RICClaimsRuleProcessor {
	
	private final String claimantParam = "TabOption=Claimants&ShowRequiredFieldsInd=Yes";
	StringDate asOfDt;
	StringDate inceptionDt;
	String inceptionTm;
    StringDate lossDt;
    StringDate cancelDt;
    StringDate expirationDt;
    ModelBean policy;
    StringDate reportedDt;
    String reportedBy;
    String reportedTo;
    ModelBean[] claimants;
    Boolean outOfBounds;
    ModelBean[] openClaimants;
    ModelBean[] closedClaimants;
    String lossCauseCd;
    
	public void processRules() throws Exception {
		if( Claim.isALossNotice(claim) ){
			asOfDt = claim.getBean("ClaimPolicyInfo").getDate("AsOfDt");
			inceptionDt= claim.getBean("ClaimPolicyInfo").getDate("InceptionDt"); 
			inceptionTm = claim.getBean("ClaimPolicyInfo").gets("InceptionTm");
			lossDt = claim.getDate("LossDt");
			policy = Policy.getPolicyBySystemId(data, Integer.parseInt( claim.getBean("ClaimPolicyInfo").gets("PolicyRef") ) ); 
			cancelDt = policy.getBean("BasicPolicy").getBean("TransactionHistory").getDate("TransactionEffectiveDt");
			expirationDt = claim.getBean("ClaimPolicyInfo").getDate("ExpirationDt");
			reportedBy = claim.gets("ReportedBy");
			reportedTo = claim.gets("ReportedTo");
			claimants = ClaimRenderer.getOpenClaimants(claim); 
			lossCauseCd = claim.gets("LossCauseCd");

			triggerDOIComplaintIndWarning();
			triggerSIUActiveWarning();
			triggerAssignmentOfBenefitsActiveWarning();
			triggerLossCauseByEarthquakeWarning();
			triggerLossCauseByFireOrSmokeWarning();
			triggerThirdPartyClaimantWindstormClaimError();
			triggerInjuryForMajorTraumaError();
			triggerClaimantsValidation();
			
			triggerLossNoticeIncomplete();
			triggerLossDtAfterToday();
			triggerLossDtBeforeInception();
			triggerLossDtAfterExpiration();
			triggerLossDtAfterCancellation();
			triggerReportedByError();
			
			triggerLossDtWithinLapsePeriod();
			triggerLossDtEqualsInception();
			triggerReportedDtAfterToday();
			triggerValidatePolicy();
			triggerValidateRisk();
			triggerValidateLossCause();
			triggerInjuryInvolvedIndWarning();
			triggerNoClaimants();
			triggerThirdPartyClaimant();
			triggerValidateWitnesses();
			triggerSumOfIndividualLiability();
		}
		triggerAssignedAdjusterRequired();
	}
	
	private void triggerLossNoticeIncomplete() throws Exception {
		if( claim.gets("DamagedInd").equalsIgnoreCase("Yes") ){
    		ValidationRenderer.addValidationError(claim,"IncompleteLossNotice","WARNING","Select Edit to verify claimant information.");

			//Always present a warning that Loss Notice is not submitted until Finalized and Submitted 
    		ValidationRenderer.addValidationError(claim,"IncompleteLossNotice","WARNING","This loss notice is incomplete until you click the Complete action followed by the Submit button.");
		}
		else {
			if( VelocityTools.beanArrayLength(claimants) > 0 )
				//Always present a warning that Loss Notice is not submitted until Finalized and Submitted
				ValidationRenderer.addValidationError(claim,"IncompleteLossNotice","WARNING","This loss notice is incomplete until you click the Complete action followed by the Submit button.");
			else
				ValidationRenderer.addValidationError(claim,"IncompleteLossNotice","ERROR","Select New Claimant under Actions to add claimant information.");
		}
	}
	
	private void triggerLossDtAfterToday() throws Exception {
		if( DateRenderer.compareTo(todayDt, lossDt) < 0 ) {
			ValidationRenderer.addValidationError(claim,"IncompleteLossNotice","ERROR","Loss Date can not be after Today's Date");
			outOfBounds = true;
		}
	}
	
	
	private void triggerLossDtBeforeInception() throws Exception {
		if( DateRenderer.compareTo(lossDt, inceptionDt) < 0 ){
			ValidationRenderer.addValidationError(claim, "IncompleteLossNotice","WARNING","Loss date cannot be before Policy Inception");
			outOfBounds = true;
		}
	}
	
	private void triggerLossDtAfterExpiration() throws Exception {
		if( DateRenderer.compareTo(lossDt, expirationDt) > 0 ){
			ValidationRenderer.addValidationError(claim, "IncompleteLossNotice","WARNING","Loss date cannot be after Policy Expiration");
			outOfBounds = true;
		}
	}
	
	private void triggerLossDtAfterCancellation() throws Exception {
		if( !cancelDt.equals("") ){
			int compareLossToCancelation = DateRenderer.compareTo(cancelDt, lossDt);
			if( compareLossToCancelation <= 0 )
			{
				ValidationRenderer.addValidationError(claim, "Validation","WARNING","Loss date cannot be after Policy Cancellation Date");
			}
		}
	}
	
	public void triggerReportedByError() throws Exception {
		if (reportedBy.equals(""))
			ValidationRenderer.addValidationError(claim, "Validation","ERROR","Reported By required");
	}
	
	public void triggerReportedToError() throws Exception {
		if (reportedTo.equals("")) 
			ValidationRenderer.addValidationError(claim, "Validation","ERROR","Reported To required");
	}
	
	// Determine if the loss date is within a lapse period 
	public void triggerLossDtWithinLapsePeriod() throws Exception {
			
		String policyNumber = claim.getBean("ClaimPolicyInfo").gets("PolicyNumber");
		String securityId = request.gets("SecurityId");
		String sessionId = request.gets("SessionId");
		String userId = request.gets("UserId");
		String conversationId = request.gets("ConversationId");		
		Boolean inLapsePeriod = NoticeRenderer.inLapsePeriod(data, policyNumber, lossDt, securityId, sessionId, userId, conversationId);
		if( inLapsePeriod )
			ValidationRenderer.addValidationError(claim,"IncompleteLossNotice","WARNING","Loss Date is within a lapse period");
		else if( !cancelDt.equals("") ){
			int compareLossToCancelation = DateRenderer.compareTo(cancelDt, lossDt);
			if( compareLossToCancelation <= 0 ){
				ValidationRenderer.addValidationError(claim, "IncompleteLossNotice","WARNING","Loss date cannot be after Policy Cancellation Date");
				outOfBounds = true;
			}
		}
		else if( outOfBounds = false){
			Boolean hasTransactionChanges = PolicyRenderer.hasTransactionChanges(data, claim.getBean("ClaimPolicyInfo").gets("PolicyNumber"), asOfDt, lossDt);
			if( hasTransactionChanges )
				ValidationRenderer.addValidationError(claim,"IncompleteLossNotice","WARNING","The new loss date falls within a different policy change, and therefore may affect coverages. Please check the Features and Reserves.");				
		}
	}
	
	// Warn the user that the loss date is the same as the inception date if inception time is not blank and not 12:01am 
	public void triggerLossDtEqualsInception() throws Exception {
		if( DateRenderer.equals(inceptionDt, lossDt) )
			if( !inceptionTm.equals("12:01am") && !inceptionTm.equals("") )					
				ValidationRenderer.addValidationError(claim,"ValidationPersist","WARNING","Loss date is the same as policy inception date. Loss time should be manually verified");
			
	}
	
	private void triggerReportedDtAfterToday() throws Exception {
		
		if( DateRenderer.compareTo(todayDt, lossDt) < 0) {
			ValidationRenderer.addValidationError(claim,"Validation","ERROR","Loss Date can not be after Today's Date");
			outOfBounds = true;
		}
		
		if( !claim.gets("ReportedDt").equals("") ) {
			reportedDt = claim.getDate("ReportedDt");
			
			if( DateRenderer.compareTo(todayDt, reportedDt) < 0) 
				ValidationRenderer.addValidationError(claim,"Validation","ERROR","Reported Date can not be after Today's Date");
			
			if( !lossDt.equals("") && DateRenderer.compareTo(lossDt, reportedDt) > 0 )
				ValidationRenderer.addValidationError(claim,"Validation","ERROR","Reported Date can not be before Loss Date");
		}	
	}

	// Validate Policy 
	private void triggerValidatePolicy() throws Exception {
		if( claim.getBean("ClaimPolicyInfo").gets("PolicyRef").equals("") )
	    	ValidationRenderer.addValidationError(claim,"Validation","ERROR","Policy Required");
	}
	
	// Validate Risk
	private void triggerValidateRisk() throws Exception {
		if( claim.gets("RiskIdRef").equals("") )
	    	ValidationRenderer.addValidationError(claim,"Validation","ERROR","Property Required");
	}
	
	// Validate Loss Cause 
	private void triggerValidateLossCause() throws Exception {
		if( claim.gets("LossCauseCd").equals("") ) 
			ValidationRenderer.addValidationError(claim,"Validation","ERROR","Loss Cause Required");
	}
	
	private void triggerAssignedAdjusterRequired() throws Exception {
		claimants = ClaimRenderer.getOpenClaimants(claim);
		for( ModelBean claimant : claimants ){
			if( claim.gets("TypeCd").equals("Transaction") ) {
				ModelBean assignedAdjuster = claimant.getBean("AssignedProvider","AssignedProviderTypeCd","AssignedAdjuster");
				
				if( assignedAdjuster.gets("ProviderRef").equals("") ){
					String claimantParamWithFields = claimantParam + "&HighlightFields=ClaimantPerson.AssignedAdjuster";
					String action = String.format( "javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')", claim.getSystemId(), claim.getId(), claimant.getId(), claimantParamWithFields );
					String claimantLink = ValidationRenderer.createActionLink(action);
					ValidationRenderer.addValidationErrorWithLink(claim,"Validation","ERROR","Assigned Adjuster Required on Claimant #" + claimant.gets("ClaimantNumber"), claimantLink);
				}
			}
		}
	}

	private void triggerInjuryInvolvedIndWarning() throws Exception {
		for( ModelBean claimant : claimants ){			
			if( !StringRenderer.isTrue(claimant.gets("InjuryInvolvedInd")) ){
				//Injury Involved Indicator is Blank or No
				if( ClaimantTransactionRenderer.hasFeature(claimant, "CovF") ){
					// If Liability Feature was Detected, Warn User that No Injury was Indicated 
					ValidationRenderer.addValidationError(claim,"Validation","WARNING","Liability Feature Detected - Injury Involved Not Indicated on Claimant #" + claimant.gets("ClaimantNumber"));
				}
			}				
		}
	}	
		
	private void triggerDOIComplaintIndWarning() throws Exception {
		if( claim.gets("DOIComplaintInd").equals("Yes") ){
			ValidationRenderer.addValidationError(claim, "ValidationPersist","WARNING","DOI Complaint Active");
		}
	}
	
	private void triggerSIUActiveWarning() throws Exception {
		if( claim.gets("InSIUInd").equals("Yes") ){
			ValidationRenderer.addValidationError(claim, "ValidationPersist", "WARNING", "Special Investigative Unit Active");
		}
	}
	
	private void triggerAssignmentOfBenefitsActiveWarning() throws Exception {
		if( claim.gets("AssignmentOfBenefits").equals("Yes") ) {
			ValidationRenderer.addValidationError(claim, "ValidationPersist", "WARNING", "Assignment of Benefits Active");
		}
	}
	
	private void triggerLossCauseByEarthquakeWarning() throws Exception {
		if( claim.gets("LossCauseCd").equals("Earthquake") ){
			ValidationRenderer.addValidationError(claim, "Validation", "WARNING", "Describe all of damages to the property (including if any holes in the roof or water leaking in)");
		}
	}
	
	//No claimants setup 
	private void triggerNoClaimants() throws Exception {
		if( claim.gets("TypeCd").equals("Transaction") ){
			openClaimants = ClaimRenderer.getOpenClaimants(claim);
			closedClaimants = claim.findBeansByFieldValue("Claimant","StatusCd","Closed");
			if( VelocityTools.beanArrayLength(openClaimants) == 0 && VelocityTools.beanArrayLength(closedClaimants) == 0 ){
				ValidationRenderer.addValidationError(claim,"IncompleteLossNotice","ERROR","No Claimants Have Been Setup");
			}
		}	
	}
	
	//A third party claimant existing on a Windstorm claim
	private void triggerThirdPartyClaimant() throws Exception {
		if( lossCauseCd.equals("Windstorm") ){
			claimants = ClaimRenderer.getOpenClaimants(claim, "Third Party");
			if( VelocityTools.beanArrayLength(claimants) > 0 )
				 ValidationRenderer.addValidationError(claim,"Validation","ERROR","Third Party Claimant Not Allowed on Windstorm Claim");
		}	
	}
	
	// Validate Witnesses 
	private void triggerValidateWitnesses() throws Exception {
		String action;
		String witnessLink;
		ModelBean[] witnesses = claim.findBeansByFieldValue("Witness","StatusCd","Active");
		for( ModelBean witness : witnesses ){
			//Set Param for Witness 
			String witnessParam = "TabOption=Witnesses&ShowRequiredFieldsInd=Yes";
			
			if( witness.gets("TypeCd").equals("") ){
				action = String.format( "javascript:cmmXFormSafe('CLWitnessSync','%s','%s','%s','%s')", claim.getSystemId(), claim.getId(), witness.getId(), witnessParam +"&HighlightFieldsWitness.TypeCd" );
				witnessLink = ValidationRenderer.createActionLink(action);
				ValidationRenderer.addValidationErrorWithLink(claim,"Validation","ERROR","Type Required on Witness #"+witness.gets("WitnessNumber"), witnessLink);
			}
			
			String witnessName = witness.getBean("NameInfo","NameTypeCd","WitnessName").gets("CommercialName");
			if( witnessName.equals("") ){
				action = String.format( "javascript:cmmXFormSafe('CLWitnessSync','%s','%s','%s','%s')", claim.getSystemId(), claim.getId(), witness.getId(), witnessParam +"&HighlightFields=WitnessName.CommercialName" );
				witnessLink = ValidationRenderer.createActionLink(action);
				ValidationRenderer.addValidationErrorWithLink(claim,"Validation","ERROR","Name Required on Witness #"+ witness.gets("WitnessNumber"), witnessLink);
			}
		}
	}
	
	// Check for Prior Adjust Reserve Transactions 
	private void triggerSumOfIndividualLiability() throws Exception {
		Boolean reserveAdjusted = false;	
		ModelBean[] priorTransactions = claim.findBeansByFieldValue("ClaimantTransaction", "TransactionCd", "Adjust Reserve");
		
		for( ModelBean priorTransaction : priorTransactions ){
			if( priorTransaction.valueEquals("StatusCd", "Active") || priorTransaction.valueEquals("StatusCd", "Open") )
				reserveAdjusted = true;
		}
		
		// Verify if any active reserves exists and then validate the liability percentage 
		if( reserveAdjusted ){
			if( !claim.gets("TypeCd").equals("LossNotice") ){
			
				String liabilityPct = "0";
				Boolean liabilityLessThan100 = false;
				Boolean liabilitygreaterThan100 = false;
				String showMsg;
				ModelBean[] subrogations = claim.getAllBeans("Subrogation");
				for ( ModelBean subrogation : subrogations ){ 
					if( !subrogation.valueEquals("StatusCd","Deleted") ){
						liabilityPct = StringRenderer.addFloat(liabilityPct, subrogation.gets("ResponsiblePartyLiabilityPct"));
						
					    if( StringRenderer.lessThanEqual(liabilityPct,"0") )
					    	liabilityLessThan100 = true;
						else if( StringRenderer.greaterThan(liabilityPct,"100") )
					    	liabilitygreaterThan100 = true;
					}
				}
				
				if( liabilityLessThan100 ){
					showMsg = "The sum of all the individual liability % cannot be 0";
					ValidationRenderer.addValidationError(claim,"Validation","ERROR",showMsg);
				}
				
				if( liabilitygreaterThan100 ){
					showMsg = "The sum of all the individual liability % cannot be greater than 100";
					ValidationRenderer.addValidationError(claim,"Validation","ERROR",showMsg);
				}			
			}
		}
	}
			
	private void triggerLossCauseByFireOrSmokeWarning() throws Exception {
		if( claim.gets("LossCauseCd").equals("Fire") || claim.gets("LossCauseCd").equals("Sudden And Accidental Damage From Smoke") ){
			ValidationRenderer.addValidationError(claim, "Validation", "WARNING", "Please update Detailed Description with how the fire started.");
		}
	}
	
	private void triggerThirdPartyClaimantWindstormClaimError() throws Exception {
		if( claim.gets("LossCauseCd").equals("Windstorm") && ClaimRenderer.getOpenClaimants(claim, "Third Party").length > 0 ){
			ValidationRenderer.addValidationError(claim, "Validation", "ERROR", "Third Party Claimant Not Allowed on Windstorm Claim");
		}
	}
	
	private void triggerInjuryForMajorTraumaError() throws Exception {

	}
	
	private void triggerClaimantsValidation() throws Exception {
		ModelBean[] claimants = claim.getBeans("Claimant");
		
		for( ModelBean claimant : claimants ){
			ModelBean partyInfo = claimant.getBean("PartyInfo", "PartyTypeCd", "ClaimantParty");
			ModelBean personInfo = partyInfo.getBean("PersonInfo", "PersonTypeCd", "ClaimantPerson");
			ModelBean primaryPhoneInfo = partyInfo.getBean("PhoneInfo", "PhoneTypeCd", "ClaimantPhonePrimary");
			ModelBean secondaryPhoneInfo = partyInfo.getBean("PhoneInfo", "PhoneTypeCd", "ClaimantPhoneSecondary");
			ModelBean faxPhoneInfo = partyInfo.getBean("PhoneInfo", "PhoneTypeCd", "ClaimantFax");
			ModelBean emailInfo = partyInfo.getBean("EmailInfo", "EmailTypeCd", "ClaimantEmail");
			String bestWayToContact = personInfo.gets("BestWayToContact");
			String claimantParamWithFields = claimantParam;
			
			if( bestWayToContact.isEmpty() ){
				claimantParamWithFields += "&HighlightFields=ClaimantPerson.BestWayToContact";
				String action = String.format( "javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')", claim.getSystemId(), claim.getId(), claimant.getId(), claimantParamWithFields );
				String claimantLink = ValidationRenderer.createActionLink(action);
				ValidationRenderer.addValidationErrorWithLink(claim, "Validation", "ERROR", "Best way to contact information is not entered on Claimant " + claimant.gets("ClaimantNumber"), claimantLink );
			} else {
				Boolean showMsg = false;
				
				if( bestWayToContact.equals("Home Phone") ){
					if( !( primaryPhoneInfo.gets("PhoneName").equals("Home") || secondaryPhoneInfo.gets("PhoneName").equals("Home") ) )
						showMsg = true;
					else if( ( primaryPhoneInfo.gets("PhoneName").equals("Home") && primaryPhoneInfo.gets("PhoneNumber").isEmpty() ) 
							&& ( secondaryPhoneInfo.gets("PhoneName").equals("Home") && secondaryPhoneInfo.gets("PhoneNumber").isEmpty() ) )
						showMsg = true;
						
					if(showMsg){
						String action = String.format( "javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')", claim.getSystemId(), claim.getId(), claimant.getId(), claimantParamWithFields );
						String claimantLink = ValidationRenderer.createActionLink(action);
						ValidationRenderer.addValidationError(claim, "Validation", "ERROR", "Best way to contact is home phone but home phone number is not entered on Claimant " + claimant.gets("ClaimantNumber"), claimantLink );
					}
				}
				
				if( bestWayToContact.equals("Mobile Phone") ){
					
					if( !( primaryPhoneInfo.gets("PhoneName").equals("Mobile") || secondaryPhoneInfo.gets("PhoneName").equals("Mobile") ) )
						showMsg = true;
					else if( ( primaryPhoneInfo.gets("PhoneName").equals("Mobile") && primaryPhoneInfo.gets("PhoneNumber").isEmpty() ) 
							&& ( secondaryPhoneInfo.gets("PhoneName").equals("Mobile") && secondaryPhoneInfo.gets("PhoneNumber").isEmpty() ) )
						showMsg = true;
					
					if( showMsg ){
						String action = String.format( "javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')", claim.getSystemId(), claim.getId(), claimant.getId(), claimantParamWithFields );
						String claimantLink = ValidationRenderer.createActionLink(action);
						ValidationRenderer.addValidationError(claim, "Validation", "ERROR", "Best way to contact is mobile phone but mobile phone number is not entered on Claimant " + claimant.gets("ClaimantNumber"), claimantLink );
					}
				}
				
				if(bestWayToContact.equals("Business Phone") 
						&& !( primaryPhoneInfo.gets("PhoneName").equals("Business") || secondaryPhoneInfo.gets("PhoneName").equals("Business") ) ){
					String action = String.format( "javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')", claim.getSystemId(), claim.getId(), claimant.getId(), claimantParamWithFields );
					String claimantLink = ValidationRenderer.createActionLink(action);
					ValidationRenderer.addValidationError(claim, "Validation", "ERROR", "Best way to contact is business phone but business phone number is not entered on Claimant " + claimant.gets("ClaimantNumber"), claimantLink );
				}
				
				if( bestWayToContact.equals("Email") && emailInfo.gets("EmailAddr").isEmpty() ){
					String action = String.format( "javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')", claim.getSystemId(), claim.getId(), claimant.getId(), claimantParamWithFields );
					String claimantLink = ValidationRenderer.createActionLink(action);
					ValidationRenderer.addValidationError(claim, "Validation", "ERROR", "Best way to contact is email but email is not entered on Claimant " + claimant.gets("ClaimantNumber"), claimantLink );
				}
				
				if( bestWayToContact.equals("Fax") && faxPhoneInfo.gets("PhoneNumber").isEmpty() ){
					String action = String.format( "javascript:cmmXFormSafe('CLClaimantSync','%s','%s','%s','%s')", claim.getSystemId(), claim.getId(), claimant.getId(), claimantParamWithFields );
					String claimantLink = ValidationRenderer.createActionLink(action);
					ValidationRenderer.addValidationError(claim, "Validation", "ERROR", "Best way to contact is fax but fax number is not entered on Claimant " + claimant.gets("ClaimantNumber"), claimantLink );
				}
			}
		}
	}
}
