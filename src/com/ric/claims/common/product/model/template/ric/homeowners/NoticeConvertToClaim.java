package com.ric.claims.common.product.model.template.ric.homeowners;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.IBIZXmlController;
import net.inov.biz.server.ServiceContext;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

import com.iscs.common.business.logging.Logger;
import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.NameValuePair;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.common.utility.webpath.WebPathHelper;
import com.iscs.workflow.Task;

/** Submit the Loss Notice to be converted to claim automatically.
 * In case of failure leave the Loss Notice as it is and in status In Process.
 *
 * @author  pradeeppa
 */
public class NoticeConvertToClaim implements RuleProcessor {

	/** Rule to create claim from loss notice.
    * @return the current response bean
    * @throws IBIZException when an error requiring user attention occurs
    * @throws ServiceHandlerException when a critical error occurs
    */
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user, JDBCData data)
	throws RuleException {
		Log.debug("Processing NoticeConvertToClaim...");
		// Save a copy in case we can't process the claim straight through
		ModelBean requestParams = null;
		ModelBean errors = null;		
		ServiceContext context = null;
		try{
			errors = new ModelBean("Errors");						
			ModelBean autoSubmitParam = ruleTemplate.getBean("Param", "Name", "AutoSubmitInd");
			if( autoSubmitParam != null && autoSubmitParam.gets("Value").equals("Yes")) {
				// Activate a subcontext
				ServiceContext.getServiceContext().activateSubContext();
				context = ServiceContext.getServiceContext();
				errors = new ModelBean("Errors");
				for(ModelBean additionalBean : additionalBeans){
					if(additionalBean.getBeanName().equals("RequestParams")){
						requestParams = additionalBean;
					}
				}
				//Saving the bean in case of a failure we still have the Loss Notice with data from ClaimEventSubmit
				BeanTools.saveBean(bean, data);
				data.commit();				
				requestParams.setValue("SessionId", context.getSessionId());									
				// attempt to turn the loss notice into a claim
				ModelBean serviceRq = buildRequest(data, requestParams, bean);
				// Process Request
				ModelBean response = IBIZXmlController.processRequest(serviceRq, true);
				
				ModelBean error = response.getBean("ResponseParams").getBean("Errors");
				if(!response.hasErrors()) {	                	        
					String taskQuickKey = bean.getBeanName().toUpperCase() + bean.getSystemId();

					String[] jdbckey = new String[] { "TaskQuickKey", "TemplateId" };
					String[] jdbcop = new String[] { "=", "=" };
					String[] jdbcval = new String[] { taskQuickKey.toUpperCase(), "LossNoticeTask2001" };

					JDBCData.QueryResult[] result = data.doQueryFromLookup("Task", jdbckey, jdbcop, jdbcval , 0);
					if (result.length > 0) {
						for( int taskCnt = 0; taskCnt < result.length; taskCnt++ ) {
							ModelBean task = new ModelBean("Task");
							String systemId = result[taskCnt].getSystemId();
							data.selectModelBean(task, Integer.parseInt(systemId));
							Task.updateTaskStatus(data, task, "Completed");				                  
						}
					}					
				}else{
					//log the failure
					ModelBean[] errorExplicit = error.findBeansByFieldValue("Error", "Severity", ErrorTools.SEVERITY_ERROR.getSeverity());
					NameValuePair[] pairs = new NameValuePair[1];
					NameValuePair errorMsg = new NameValuePair("ErrorMsg", errorExplicit[0].gets("Msg"));
					pairs[0] = errorMsg;
					StringDate todayDt = DateTools.getStringDate(requestParams);
					ModelBean claim = response.getBean("Claim");					
					Logger.message(data, "LossNoticeConversionToClaimFailure", user.gets("LoginId"), todayDt, new ModelBean[]{claim}, pairs);
				}
			}
		}catch(Exception e){			
			throw new RuleException(e);			
		}
		return errors;
	}

	
	/** Build the web request 
     * @param data JDBCData Connection Object
     * @param originalRequestParams Original Request Params from the Main Service Call
     * @param claim The Claim ModelBean  
     * @return Service Request ModelBean
     * @throws Exception If an Unexpected Error Occurs
     */
    private ModelBean buildRequest(JDBCData data, ModelBean originalRequestParams, ModelBean claim) 
    throws Exception {
    	try {
    		// Create Request ModelBean
    		String requestName = "CLNoticeSubmitRq";
    		ModelBean serviceRq = new ModelBean(requestName);

    		// Add Policy 
    		serviceRq.addValue(claim);

    		// Add RequestParams
    		ModelBean requestParams = new ModelBean("RequestParams"); 
    		requestParams.setValue("SessionId", originalRequestParams.gets("SessionId"));
    		requestParams.setValue("UserId", originalRequestParams.gets("UserId"));
    		requestParams.setValue("SecurityId", originalRequestParams.gets("SecurityId"));     
    		requestParams.setValue("ConversationId", originalRequestParams.gets("ConversationId"));	        
    		serviceRq.addValue(requestParams);       

    		// Add CMMParams
    		ModelBean cmmParams = new ModelBean("CMMParams");
    		cmmParams.setValue("Operation", "Update");
    		cmmParams.setValue("Container", "Claim");
    		cmmParams.setValue("SystemId", claim.getSystemId());
    		cmmParams.setValue("ModelName", "Claim"); 
    		requestParams.addValue(cmmParams);
    		
    		WebPathHelper.addParam(serviceRq, "StraightThroughClaimInd", "Yes");	 
    		WebPathHelper.addParam(serviceRq, "ErrorInd", "");	 
    		WebPathHelper.addParam(serviceRq, "SkipInd", "");	     		

    		return serviceRq;
    	}
    	catch( Exception e ) {
    		throw new Exception(e);
    	}
    }        	
}
