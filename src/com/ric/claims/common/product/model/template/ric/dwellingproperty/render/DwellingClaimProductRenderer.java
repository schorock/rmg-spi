/*
 * ClaimProductRenderer.java
 *
 */

package com.ric.claims.common.product.model.template.ric.dwellingproperty.render;

import net.inov.mda.MDAOption;
import net.inov.mda.MDATable;

import com.iscs.claims.common.product.render.ClaimProductRenderer;
import com.iscs.common.mda.Store;

/** Rendering Tool Used to Get Data from the Claims Product Setup
 *
 * @author scottf
 */
public class DwellingClaimProductRenderer extends ClaimProductRenderer {
    
    
    /** Creates a new instance of ClaimProductRenderer */
    public DwellingClaimProductRenderer() {
    }
    
    /** Context Variable Getter
      * @return ClaimProductRenderer
      */     
    public String getContextVariableName() {
         return "DwellingClaimProductRenderer";
    }

   /** Obtain a selection list of coderef defined name/values given a MDA umol
     * @param umol The MDA repository path to the coderef
     * @return list of MDA packages in NameValuePair form
     */
    public static String getCodeRefList(String umol) {
        try {
            MDATable packages = (MDATable) Store.getModelObject(umol);
            MDAOption[] options = packages.getOptions();
            StringBuffer reasons = new StringBuffer();
            for (int cnt=0; cnt<options.length; cnt++) {
            	reasons.append(options[cnt].getValue());
            	if (cnt < (options.length-1)) {
            		reasons.append(",");
            	}
            }
                         
            return reasons.toString();
        } catch (Exception e) { 
            return "";
        }
    }

}


