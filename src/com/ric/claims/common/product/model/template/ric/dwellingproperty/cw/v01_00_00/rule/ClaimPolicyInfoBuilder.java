package com.ric.claims.common.product.model.template.ric.dwellingproperty.cw.v01_00_00.rule;

import java.util.ArrayList;

import com.ibm.math.BigDecimal;
import com.iscs.claims.interfaces.PolicyInfoRequest;
import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.insurance.product.ProductMaster;
import com.iscs.insurance.product.ProductSetup;
import com.iscscl.claims.common.product.ClaimPolicyReinsuranceBuilder;
import com.iscscl.claims.common.product.personal.PolicyInfoBuilderBase;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.data.JDBCData;
import net.inov.tec.math.Money;
import net.inov.tec.web.webpath.FrontController;

/** Loads Policy Information Specific for the Claim
 * 
 * @author Debasish
 */
public class ClaimPolicyInfoBuilder extends PolicyInfoBuilderBase implements RuleProcessor, PolicyInfoRequest {

	private static String hurricaneHailCoverages  = "CovA, CUOASC, CovB, CovC, CCHICP, CovD, CCF, DUCT, LTC, OLLC, PSIO, DR, DRT, ER, FDSC, GB, GM, TI, TPSL";
	private static String noDeductibleValueNeeded  = "RFP, CovL, PIII, BAL, CovM, CovO, CovP, OPPL, WL, HLIC";
	private static String policyCoverageLimit  = "CovB, CovD, CovM, CovO, CovP, CCF, DUCT, EQ, LTC, PSIO, RALA, RAELA, WD";
	private static String LINE_LIABILITY = "Dwelling";
	private static ModelBean lines = null;
	
	// Rule processor interface
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user, JDBCData data) 
	throws RuleException {	
		ModelBean errors = null;
		ArrayList<ModelBean> errorList = new ArrayList<ModelBean>();
		try{
			// Load the coverage information for the claim risk 
			Log.debug("Processing DwellingClaimPolicyInfoBuilder...");
			
			// Build the Errors ModelBean
			errors = new ModelBean("Errors");
			
			// Build Service Request ModelBean
            ModelBean serviceRq = buildRequest(ruleTemplate, bean);
            
            // Turn Off Debug Messages
            Log.setLoggingLevelError();
            
            // Send Webpath Request
			FrontController controller = new FrontController();
	        ModelBean response = controller.sendRequest(serviceRq, data);
	        ModelBean dtoPolicy = response.getBean("DTOPolicy");
	        
			// Load the Policy Information
			Log.debug("DTOPolicy: " + dtoPolicy.readableDoc());
			lines = getClaimLines(dtoPolicy);
			
			// Save the old ClaimPolicyInfo
			ModelBean claimPolicyInfo = bean.getBean("ClaimPolicyInfo");

			storeClaimPolicyInfo(claimPolicyInfo);
			loadCoverageInfo(dtoPolicy, bean); 
			
			// Restore Claim Policy Limits
			restoreClaimPolicyLimits(claimPolicyInfo);
						
			// Load any reinsurance information if needed also
			ClaimPolicyReinsuranceBuilder.buildClaimReinsurance(bean, errorList);
			
	        for( ModelBean error : errorList ) {
	            errors.addValue(error);
	        }            

	        Log.debug("Claim: " + bean.readableDoc());
							
		}
		catch(Exception e){	
			throw new RuleException(e);
		} finally {
			// Set Log Level back after processing the FrontController request
			Log.setLoggingLevelDefault();
		}
		return errors;		
	}
	
	/** Load the coverage(s) information that will apply to the claim 
	 * @param dtoPolicy The DTO Policy data Bean obtained from the external system
	 * @param claim The current claim container
	 * @throws RuleException when an unexpected error occurs 
	 */
	public void loadCoverageInfo(ModelBean dtoPolicy, ModelBean claim)
	throws RuleException {
		try {
			ModelBean claimPolicyInfo = claim.getBean("ClaimPolicyInfo");
			String riskId = claim.gets("RiskIdRef");
			
			for (ModelBean claimRisk : claimPolicyInfo.getBean("PolicyRisks").getBeans("PolicyRisk")) { 
				ArrayList<ModelBean> lineCoverages = new ArrayList<ModelBean>();
				ArrayList<ModelBean> riskCoverages = new ArrayList<ModelBean>();
				
				loadRiskCoverageInfo(claim, dtoPolicy, claimRisk, lineCoverages, riskCoverages);

				ModelBean[] coverageActivities = findCoveragesWithActivity(claim, claimRisk);
				
				// Delete Existing ClaimPolicyLimit ModelBeans
				claimRisk.deleteBeans("ClaimPolicyLimit");

				// Add Line Coverages to ClaimPolicyInfo
				for (ModelBean lineCoverage : lineCoverages) {
					claimRisk.addValue(lineCoverage);
				}

				// Add Risk Coverages to ClaimPolicyInfo
				for (ModelBean riskCoverage : riskCoverages) {
					claimRisk.addValue(riskCoverage);
				}

				addCoveragesWithActivity(claim, claimRisk, coverageActivities);
				
				// Add the coverages to the claim if the risk is the primary risk
				if (claimRisk.getId().equals(riskId)) {
					coverageActivities = findCoveragesWithActivity(claim, claimPolicyInfo);

					// Delete Existing ClaimPolicyLimit ModelBeans
					claimPolicyInfo.deleteBeans("ClaimPolicyLimit");

					// Add Line Coverages to ClaimPolicyInfo
					for (ModelBean lineCoverage : lineCoverages) {
						claimPolicyInfo.addValue(lineCoverage);
					}

					// Add Risk Coverages to ClaimPolicyInfo
					for (ModelBean riskCoverage : riskCoverages) {
						claimPolicyInfo.addValue(riskCoverage);
					}

					addCoveragesWithActivity(claim, claimPolicyInfo, coverageActivities);
				}
			}
		} catch (Exception e) {
			throw new RuleException(e);
		}
	}
	
	/**
	 * Load the coverage information from the Policy based on the current risk
	 * 
	 * @param claim
	 *            The Claim ModelBean
	 * @param dtoPolicy
	 *            The DTOPolicy ModelBean
	 * @param claimRisk
	 *            The current Risk
	 * @param lineCoverages
	 *            The list of coverages to add that belongs to the line
	 * @param riskCoverages
	 *            The list of coverages to add that belongs to the risk
	 * @throws Exception
	 *             when an error occurs
	 */
	public void loadRiskCoverageInfo(ModelBean claim, ModelBean dtoPolicy, ModelBean claimRisk, ArrayList<ModelBean> lineCoverages, ArrayList<ModelBean> riskCoverages) throws Exception {
		String riskLine = "";

		// Get all of the risk coverages if the risk was selected
		String riskIdRef = claimRisk.gets("RiskIdRef");
		if (!riskIdRef.isEmpty()) {
			ModelBean policyRisk = dtoPolicy.getBeanById(riskIdRef);
			if (policyRisk == null)
				throw new Exception(String.format("Risk IdRef %s references a non-existent ModdelBean", riskIdRef));
			else {
				ModelBean building = dtoPolicy.findBeanByFieldValue("DTOBuilding","Status","Active");
				String windHailDed = building.gets("WindHailDedLimit");
				String allPerilDed = building.gets("AllPerilDed");
				loadCoverages(claim, policyRisk, riskCoverages, windHailDed, allPerilDed);

				// Get all of the risk's line level coverages
				ModelBean line = BeanTools.getParentBean(policyRisk, "DTOLine");
				if (line != null) {
					loadCoverages(claim, line, lineCoverages, windHailDed, allPerilDed);
					riskLine = line.gets("LineCd");
				}
			}
		}

		// Grab all of the Liability Lines
		ModelBean liability = dtoPolicy.getBean("DTOLine", "LineCd", LINE_LIABILITY);
		if (!riskLine.equals(liability.gets("LineCd"))) {
			loadCoverages(claim, liability, lineCoverages, "", "");
		}
	}

	/**
	 * Convenience method to add coverages from the current parent beean to the
	 * Arraylist passed in.
	 * 
	 * @param parent
	 *            The parent ModelBean containing coverages to add
	 * @param covList
	 *            The ArrayList to add any coverages found
	 * @throws Exception
	 *             when an error occurs
	 */
	private void loadCoverages(ModelBean claim, ModelBean parent, ArrayList<ModelBean> covList, String windHailDed, String allPerilDed) throws Exception {
		Log.debug(parent.getBeanName());
		ModelBean covA = parent.findBeanByFieldValue("DTOCoverage", "CoverageCd", "CovA");
		ModelBean covB = parent.findBeanByFieldValue("DTOCoverage", "CoverageCd", "CovB");
		ModelBean covC = parent.findBeanByFieldValue("DTOCoverage", "CoverageCd", "CovC");
		ModelBean covL = parent.findBeanByFieldValue("DTOCoverage", "CoverageCd", "CovL");
		ModelBean covPIII = parent.findBeanByFieldValue("DTOCoverage", "CoverageCd", "CovPIII");
		ModelBean covGAL = parent.findBeanByFieldValue("DTOCoverage", "CoverageCd", "GAL");
		String covALimit = "";
		String covBLimit = "";
		String covCLimit = "";
		String covLLimit = "";
		String covPIIILimit = "";
		String covGALLimit = "";
		String lossCauseCd = claim.gets("LossCauseCd");
		
		if (covA != null) {
			covALimit = covA.getBean("DTOLimit").gets("Value");
		}
		if (covB != null) {
			covBLimit = covB.getBean("DTOLimit").gets("Value");
		}
		if (covC != null) {
			covCLimit = covC.getBean("DTOLimit").gets("Value");
		}
		if (covL != null) {
			covLLimit = covL.getBean("DTOLimit").gets("Value");
		}
		if (covPIII != null) {
			covPIIILimit = covPIII.getBean("DTOLimit").gets("Value");
		}
		if (covGAL != null) {
			covGALLimit = covGAL.getBean("DTOLimit").gets("Value");
		}
		
		for (ModelBean coverage : parent.getBeans("DTOCoverage")) {
			if (coverage.gets("Status").equals("Active")) {
				String coverageCd = coverage.gets("CoverageCd"); 
				ModelBean limit = new ModelBean("ClaimPolicyLimit");
				limit.setValue("SourceCd","Policy");
				limit.setValue("OriginalCoverageCd", coverage.gets("CoverageCd"));
				limit.setValue("CoverageCd", coverage.gets("CoverageCd"));
				limit.setValue("Description", coverage.gets("Description"));
				
				// Obtain the coverage limit
				ModelBean[] limits = coverage.getBeans("DTOLimit");
				for (int cnt=0; cnt<limits.length; cnt++) {
					if( !limits[cnt].gets("Value").equals("") ) {				
						if (limit.gets("Limit").equals("")){
							limit.setValue("Limit", limits[cnt].gets("Value"));
						}
					}
				}
				
				//Grab the coverage limit and set it as the feature limit 
				if( StringTools.in(coverage.gets("CoverageCd"), policyCoverageLimit) ) {
					limit.setValue("Limit",limit.gets("Limit"));
				}

				//Set Limit and Deductibles for Special Features
				if( StringTools.in(coverage.gets("CoverageCd"), hurricaneHailCoverages) ) {
					if(lossCauseCd.equals("Windstorm") || lossCauseCd.equals("Hail")){
						limit.setValue("Deductible",windHailDed);
						limit.setValue("DeductibleDescription", "Wind Hail Deductible");
						limit.setValue("DeductibleAllocationCd", "WindHailDed");
					} else {
						limit.setValue("Deductible",allPerilDed);	
						limit.setValue("DeductibleDescription", "All Peril");
						limit.setValue("DeductibleAllocationCd", "AllPeril");
					}
					
					
					limit.setValue("DeductibleAllocationTypeCd", "Risk");
				}
				
				if(coverageCd.equals("CUOASC") ){
					limit.setValue("Limit",covALimit);
				} else if(coverageCd.equals("RFP") ){
					limit.setValue("Limit","500");
				} else if(coverageCd.equals("PIII") ){
					limit.setValue("Limit",covLLimit);
				} else if(coverageCd.equals("BAL") ){
					limit.setValue("Limit",covLLimit);
				} else if(coverageCd.equals("EQ") ){
					String deductibleTotal = StringRenderer.addInt(StringRenderer.addInt(covALimit, covBLimit), covCLimit);
					String deductiblePercentage = coverage.getBean("DTODeductible", "DeductibleCd", "Deductible1").gets("Value");
					deductiblePercentage = StringRenderer.multiply(StringRenderer.replaceAll(deductiblePercentage, "[^0-9.]+", ""), ".01", 2);
					String deductibleAmount = StringRenderer.multiply(deductibleTotal, deductiblePercentage, 2);
					
					if( StringRenderer.greaterThan(deductibleAmount, "250") )
						limit.setValue("Deductible", "" );
					else
						limit.setValue("Deductible", "250" );
				} else if(coverageCd.equals("EB") ){
					limit.setValue("Limit","100000");
					limit.setValue("Deductible","500");
				} else if(coverageCd.equals("OPPL") ){
					limit.setValue("Limit",covPIIILimit);
				} else if(coverageCd.equals("OLLC") ){
					limit.setValue("Limit",covALimit);
				} else if(coverageCd.equals("RAELA") ){
					limit.setValue("Deductible","250");
				} else if(coverageCd.equals("RALA") ){
					limit.setValue("Deductible","250");
				} else if(coverageCd.equals("SLE") ){
					limit.setValue("Limit","10000");
					limit.setValue("Deductible","500");
				} else if(coverageCd.equals("WD") ){
					limit.setValue("Deductible","250");
				} else if(coverageCd.equals("WL") ){
					limit.setValue("Limit",covLLimit);
				} else if(coverage.gets("CoverageCd").equals("HLIC")){
					Double limit1 = Double.parseDouble(coverage.findBeanByFieldValue("DTOLimit", "LimitCd", "Limit1").gets("Value"));
					Double limit2 = Double.parseDouble(coverage.findBeanByFieldValue("DTOLimit", "LimitCd", "Limit2").gets("Value"));
					limit.setValue("Limit",limit1 +limit2);
				}

				// Build any sub limits
				buildSubCoverage(coverage, limit, lossCauseCd, windHailDed, allPerilDed, covCLimit, covGALLimit);		
				// Build the limit and deductible information fields
				buildCoverageDetails(coverage, limit,lossCauseCd, windHailDed);	
				covList.add(limit);
	
				// Add the line code from the Line
				ModelBean line = BeanTools.getParentBean(coverage, "DTOLine");
				if (line != null)
					limit.setValue("LineCd", line.gets("LineCd"));
			}
		}
	}

	/** Build the limit items using Policy Coverage Item information
	 * 
	 * @param coverage The policy coverage ModelBean
	 * @param limit The claim limit ModelBean
	 * @throws Exception when an unexpected error occurs
	 */
	private static void buildSubCoverage(ModelBean coverage, ModelBean limit, String lossCauseCd, String windHailDed, String allPerilDed, String covCLimit, String covGALLimit)
	throws Exception {
		ModelBean container = null;
		ModelBean risk = null;
		
		if(coverage.getParentBean().getBeanName().equals("DTORisk") ){
			container = coverage.getParentBean().getParentBean().getParentBean(); 
			risk = coverage.getParentBean();
		}else{
			container = coverage.getParentBean().getParentBean();
		}
		
		ModelBean basicPolicy = container.getBean("DTOBasicPolicy");
		String subType = basicPolicy.gets("SubTypeCd");
		ModelBean[] items = coverage.getBeans("DTOCoverageItem");
		ModelBean claimCoverage = lines.findBeanByFieldValue("ClaimCoverage", "CoverageCd", coverage.gets("CoverageCd"));
		
		if(coverage.gets("CoverageCd").equals("CovA")){	
			ModelBean[] claimSubCoverages = claimCoverage.getBeans("ClaimSubCoverage");
			BigDecimal covCLim = new BigDecimal("0.00");
			for (ModelBean claimSubCoverage : claimSubCoverages) {
				String subCovLimit = "";
				String subCovDeductible = "";
				String coverageSubCd = claimSubCoverage.gets("CoverageSubCd"); 
				
				if(coverageSubCd.equals("DRT") || coverageSubCd.equals("FDSC") ) {
					subCovLimit = "500";
				} else if(coverageSubCd.equals("ER") ) {
					subCovLimit = "250";
				} else if(coverageSubCd.equals("GM") ) {
					if (subType.equals("FL1")) {
						subCovLimit = "500";
					} else {
						subCovLimit = "1000";
					}
				} else if(coverageSubCd.equals("TI") ) {
					if(!covCLimit.equals("")) {
						covCLim = new BigDecimal(covCLimit);
					}
					BigDecimal factor = new BigDecimal("0.10"); 
					subCovLimit = Double.toString((Math.round(((covCLim.multiply(factor)).doubleValue()))));
				} else if(coverageSubCd.equals("TPSL") ) {
					subCovLimit = limit.gets("Limit");
					subCovLimit = StringRenderer.multiplyMoney(subCovLimit, ".05").toString();	
				} else {
					subCovLimit = limit.gets("Limit"); 
				}
				
				if( !coverageSubCd.equals("DRT") && !coverageSubCd.equals("ER") && !coverageSubCd.equals("FDSC") && !coverageSubCd.equals("GM") ) {
					if(lossCauseCd.equals("Windstorm") || lossCauseCd.equals("Hail")){
						subCovDeductible = windHailDed;
					} else {
						subCovDeductible = allPerilDed;	
					}
				} else {
					subCovDeductible = "";
				}
				
				String limitDescription = claimSubCoverage.gets("Description");
				
				if ( !(subType.equals("FL1") && coverageSubCd.equals("GB")) && !(subType.equals("FL3") && coverageSubCd.equals("TI")) ) {
					addSubCoverageBean(coverage, limit, claimSubCoverage.gets("CoverageSubCd"), claimSubCoverage.gets("Description"), subCovLimit, subCovDeductible, limitDescription, claimSubCoverage.gets("CoverageSubCd"));	
				}
				
			}
		} else if(coverage.gets("CoverageCd").equals("CCHICP")) {	
			ModelBean[] claimSubCoverages = claimCoverage.getBeans("ClaimSubCoverage");
			ModelBean[] dtoCoverageItems = coverage.getBeans("DTOCoverageItem");
			boolean moneyIncluded = false; 
			boolean securitiesIncluded = false; 
			boolean businessIncluded = false;
			int totalMoneyAmt = 0;
			int totalSecuritiesAmt = 0;
			int totalBusinessAmt = 0;
			
			for( ModelBean coverageItem : dtoCoverageItems ) {
				if(coverageItem.getBean("DTOCoverageData").gets("Value").equalsIgnoreCase("Money") ) {
					int subCovMItemLimit = Integer.parseInt(coverageItem.findBeanByFieldValue("DTOLimit", "LimitCd", "ItemLimit1").gets("Value"));
					totalMoneyAmt = subCovMItemLimit + totalMoneyAmt;
				} else if(coverageItem.getBean("DTOCoverageData").gets("Value").equalsIgnoreCase("Securities") ) {
					int subCovSItemLimit = Integer.parseInt(coverageItem.findBeanByFieldValue("DTOLimit", "LimitCd", "ItemLimit1").gets("Value"));
					totalSecuritiesAmt = subCovSItemLimit + totalSecuritiesAmt;
				} else if (coverageItem.getBean("DTOCoverageData").gets("Value").equalsIgnoreCase("Business Property") ) {
					int subCovBItemLimit =  Integer.parseInt(coverageItem.findBeanByFieldValue("DTOLimit", "LimitCd", "ItemLimit1").gets("Value"));
					totalBusinessAmt = subCovBItemLimit + totalBusinessAmt;
				}
			}
			
			for( ModelBean dtoCoverageItem : dtoCoverageItems ) {
				for (ModelBean claimSubCoverage : claimSubCoverages) {
					String subCovDeductible = "";
					String limitDescription = claimSubCoverage.gets("Description");
					
					if(lossCauseCd.equals("Windstorm") || lossCauseCd.equals("Hail")){
						subCovDeductible = windHailDed;
					} else {
						subCovDeductible = allPerilDed;	
					}	

					if(dtoCoverageItem.getBean("DTOCoverageData").gets("Value").equalsIgnoreCase("Money") && limitDescription.toLowerCase().contains("money") && moneyIncluded==false ) {
						addSubCoverageBean(coverage, limit, claimSubCoverage.gets("CoverageSubCd"), claimSubCoverage.gets("Description"), Integer.toString(totalMoneyAmt), subCovDeductible, limitDescription, claimSubCoverage.gets("CoverageSubCd"));
						moneyIncluded = true;
						break;
					} else if(dtoCoverageItem.getBean("DTOCoverageData").gets("Value").equalsIgnoreCase("Securities") && limitDescription.toLowerCase().contains("securities") &&  securitiesIncluded==false ) {
						addSubCoverageBean(coverage, limit, claimSubCoverage.gets("CoverageSubCd"), claimSubCoverage.gets("Description"), Integer.toString(totalSecuritiesAmt), subCovDeductible, limitDescription, claimSubCoverage.gets("CoverageSubCd"));
						securitiesIncluded = true; 
						break;
					} else if(dtoCoverageItem.getBean("DTOCoverageData").gets("Value").equalsIgnoreCase("Business Property") && limitDescription.toLowerCase().contains("business") && businessIncluded==false ) {
						addSubCoverageBean(coverage, limit, claimSubCoverage.gets("CoverageSubCd"), claimSubCoverage.gets("Description"), Integer.toString(totalBusinessAmt), subCovDeductible, limitDescription, claimSubCoverage.gets("CoverageSubCd"));
						businessIncluded = true; 
						break;
					}					
				}	
			}
		
		} else if(coverage.gets("CoverageCd").equals("CovL")){	
			ModelBean[] claimSubCoverages = claimCoverage.getBeans("ClaimSubCoverage");
			for (ModelBean claimSubCoverage : claimSubCoverages) {
				String subCovLimit = limit.gets("Limit"); 
				String subCovDeductible = "";
				String coverageSubCd = claimSubCoverage.gets("CoverageSubCd"); 
					
				if(coverageSubCd.equals("GAL") ) {
					subCovLimit = covGALLimit;
				} 
				
				String limitDescription = claimSubCoverage.gets("Description");
				addSubCoverageBean(coverage, limit, claimSubCoverage.gets("CoverageSubCd"), claimSubCoverage.gets("Description"), subCovLimit, subCovDeductible, limitDescription, claimSubCoverage.gets("CoverageSubCd"));
			}

		} else if(coverage.gets("CoverageCd").equals("HLIC")){	
			ModelBean[] claimSubCoverages = claimCoverage.getBeans("ClaimSubCoverage");
			for (ModelBean claimSubCoverage : claimSubCoverages) {
				String subCovItemLimit = "";
				String subCovDeductible = "";
				String limitDescription = claimSubCoverage.gets("Description");
				String coverageSubCd = claimSubCoverage.gets("CoverageSubCd");
				
				if(coverageSubCd.equals("HLICFDSC") ) {
					subCovItemLimit  = coverage.findBeanByFieldValue("DTOLimit", "LimitCd", "Limit1").gets("Value");
				} else if(coverageSubCd.equals("HLICFDTI") ) {
					subCovItemLimit = coverage.findBeanByFieldValue("DTOLimit", "LimitCd", "Limit2").gets("Value");
					if(subType.equals("FL3")) {
						continue; 
					}
				}
				
				if(!subCovItemLimit.equals("") && !subCovItemLimit.equals("0") ) {
					addSubCoverageBean(coverage, limit, claimSubCoverage.gets("CoverageSubCd"), claimSubCoverage.gets("Description"), subCovItemLimit, subCovDeductible, limitDescription, claimSubCoverage.gets("CoverageSubCd"));
				}
			}
		}

		for (int cnt=0; cnt<items.length; cnt++) {
			if (!items[cnt].gets("Status").equals("Active")) continue;
			ModelBean item = new ModelBean("ClaimPolicySubLimit");
			item.setValue("CoverageSubCd", items[cnt].gets("CoverageItemCd"));
			item.setValue("Description", items[cnt].gets("ItemDesc"));
			
			// Get the Item Category or Code
			ModelBean code = items[cnt].getBean("DTOCoverageData", "CoverageDataCd", "ClassCd");
			if (code != null) {
				//item.setValue("Category", code.gets("Value").replaceAll(",","").replaceAll(" ",""));
				//item.setValue("CategoryDescription", code.gets("Value"));
			    //item.setValue("OriginalCoverageSubCd", code.gets("Value").replaceAll(",",""));
			}
			
			item.setValue("ItemNumber", items[cnt].gets("SequenceNumber"));
			
			// Get the limit
			ModelBean itemLimit = items[cnt].getBean("DTOLimit");
			item.setValue("Limit", itemLimit.gets("Value"));
			
			// Get the deductible
			ModelBean itemDeduct = items[cnt].getBean("DTODeductible");
			if (itemDeduct != null)
				item.setValue("Deductible", itemDeduct.gets("Value"));
			
			
			// Add the item to the limit
			limit.addValue(item);
			
			// Build the coverage item limit and deductible details
			//buildSubCoverageDetails(coverage, item);
		}

	}
	
	/** Create the unique description fields for the limits and deductibles for each coverage 
	 * 
	 * @param coverage The policy coverage ModelBean
	 * @param limit The claim policy limit ModelBean
	 * @throws Exception when an error occurs
	 */
	private static void buildCoverageDetails(ModelBean coverage, ModelBean limit,String lossCauseCd, String windHailDed) 
	throws Exception {
		
		// Pseudo switch structure for limits
		for (int cnt=0; cnt<1; cnt++) {
			
			String lim = limit.gets("Limit");
			if (StringTools.isNumeric(lim)) {
				Money limitAmt = new Money(lim);
				if (limitAmt.compareTo("0.00") > 0)
					limit.setValue("LimitDescription", "Policy Limit");
			}
			
		}
		
		// Pseudo switch structure for deductibles
		for (int cnt=0; cnt<1; cnt++) {
			if( !StringTools.in(coverage.gets("CoverageCd"), noDeductibleValueNeeded) ) {
				if(lossCauseCd.equals("Windstorm")|| lossCauseCd.equals("Hail")) {
				  if(windHailDed!=null && !windHailDed.equals("")){
					if(!windHailDed.equalsIgnoreCase("None")){					
						limit.setValue("DeductibleDescription", "Wind/Hail Ded");
					}
				  }
				}else {				
					limit.setValue("DeductibleDescription", "All Peril");
				} 
				if(coverage.gets("CoverageCd").equals("RAELA")) {
					limit.setValue("DeductibleDescription", "Per Occurance");
				}
			}			
		}
			
	}
	
	/** Create the unique description fields for the limits and deductibles for each coverage item 
	 * 
	 * @param coverage The policy coverage ModelBean
	 * @param limit The claim policy limit ModelBean
	 * @throws Exception when an error occurs
	 
	private static void buildSubCoverageDetails(ModelBean coverage, ModelBean limit) 
	throws Exception {
		String lim = limit.gets("Limit");		
		
		// Pseudo switch structure for description of coverage
		for (int cnt=0; cnt<1; cnt++) {
			String covCd = coverage.gets("CoverageCd");
			StringBuffer description = new StringBuffer();
			
			// Business Pursuits
			if (covCd.equals("BUP")) {
				ModelBean covItem = coverage.getBean("DTOCoverageItem", "SequenceNumber", limit.gets("ItemNumber"));
				ModelBean nameVal = covItem.findBeanByFieldValue("QuestionReply", "Name", "InsName");
				ModelBean occVal = covItem.findBeanByFieldValue("QuestionReply", "Name", "Occupation");
				ModelBean classVal = covItem.findBeanByFieldValue("QuestionReply", "Name", "ClassCode");
				
				description.append(nameVal.gets("Value"))
					.append("-")
					.append(occVal.gets("Value"))
					.append("-")
					.append(classVal.gets("Value"));
				limit.setValue("Description", description.toString());				
			}
		}
		
	}
	
	/** Obtain the Policy Line Code from the coverage code
	 * @param policyProductVersionIdRef The policy product version id reference name 
	 * @param coverageCd The coverage code
	 * @return the line code
	 * @throws Exception when an error occurs
	 */
	public String getPolicyLineCd(String policyProductVersionIdRef, String coverageCd) 
	throws Exception {
		ModelBean dtoLine = new ModelBean("DTOLine");
        // Get the Line Code based on the Coverage code from the product
        ProductSetup productSetup = ProductMaster.getProductMaster("UW").getProductVersion(policyProductVersionIdRef).getProductSetup();
        if (productSetup != null) {
            ModelBean productCov = productSetup.getBean().findBeanByFieldValue("ProductCoverage", "CoverageCd", coverageCd);
            if (productCov != null) {
                ModelBean productLine = BeanTools.getParentBean(productCov, "ProductLine");
                if (productLine != null) {
                  dtoLine.setValue("LineCd", productLine.gets("LineCd"));
                }
            }
        }                
        
        return dtoLine.gets("LineCd");
        	
	}
	/** Returns true if coverage is active
     * @param container ModelBean holding the container of the coverage.
     * @param coverageCd String containing the coverage code
     * @return true/false if the coverage is active
     */    
    public static boolean isActive( ModelBean container, String coverageCd ) {
        try {
            ModelBean[] coverages = getActiveCoverages(container, coverageCd);
            if( coverages.length == 0 )
                return false;
            else
                return true;
        }
        catch( Exception e ) {
            e.printStackTrace();
            Log.error(e);
            return false;
        }
    }
    
    /** Returns an array of Coverage ModelBeans for all coverages that are active
     * @return ModelBean[] array of active coverages
     * @param coverageCd String containing the coverage code
     * @param container ModelBean holding the container of the coverage, application/policy/quote.
     */    
    public static ModelBean[] getActiveCoverages( ModelBean container, String coverageCd ) {
        try {
            ArrayList<ModelBean> arr = new ArrayList<ModelBean>();
            
            ModelBean[] coverage = container.findBeansByFieldValue("DTOCoverage", "CoverageCd", coverageCd);   
            for( int i = 0; i < coverage.length; i++ ) {
                if( coverage[i].valueEquals("Status", "Active") )
                    arr.add(coverage[i]);
            }
            ModelBean[] coverages = (ModelBean[]) arr.toArray( new ModelBean[arr.size()] );
            
            return coverages;
        }
        catch( Exception e ) {
            e.printStackTrace();
            Log.error(e);
            return null;
        }
    }
    
	/** Add ClaimPolicySubLimit bean to limit Bean
	 * 
	 * @param coverage The policy coverage ModelBean
	 * @param limit The claim limit ModelBean
	 * @param CoverageSubCd
	 * @param Description
	 * @param ItemNumber
	 * @param Limit
	 * @param Deductible
	 * @param LimitDescription
	 * @throws Exception when an unexpected error occurs
	 */
    
    private static void addSubCoverageBean(ModelBean coverage, ModelBean limit, String coverageSubCd, String subCovDescription,String subLimit, String deductible, String limitDescription, String subFeatureCd)
	throws Exception {

				
		ModelBean item = new ModelBean("ClaimPolicySubLimit");
		item.setValue("CoverageSubCd", coverageSubCd );
		item.setValue("Description", subCovDescription );
		
		//Set Limit and Deductible
		item.setValue("Limit", subLimit);
		item.setValue("Deductible", deductible);
		
		//Set Limit Description
		item.setValue("LimitDescription", limitDescription);
		
		// Get the Item Category or Code
		if ( !subFeatureCd.equals("") ) {
			item.setValue("Category", subFeatureCd.replaceAll("[^a-zA-Z]", ""));
			item.setValue("CategoryDescription", subCovDescription);
			item.setValue("OriginalCoverageSubCd", subFeatureCd);
		}		
		
		//Add the item to the limit
		limit.addValue(item);
								
	}	
	
	private ModelBean getClaimLines(ModelBean dtoPolicy) throws ModelBeanException {
		ModelBean setupProduct = ProductSetup.getProductSetup(dtoPolicy.getBean("DTOBasicPolicy").gets("ProductVersionIdRef"));
		ModelBean setupClaim = ProductSetup.getProductSetup(setupProduct.gets("ClaimSetupName"));

		return setupClaim.getBean("ClaimLines");
	}

}
