package com.ric.claims.common.product.model.template.ric.homeowners.cw.v01_00_00.rule;

import java.util.ArrayList;

import net.inov.biz.server.ServiceContext;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.math.Money;
import net.inov.tec.web.webpath.FrontController;

import com.iscs.claims.common.Feature;
import com.iscs.claims.interfaces.PolicyInfoRequest;
import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.webpath.WebPathHelper;
import com.iscscl.claims.common.product.ClaimPolicyReinsuranceBuilder;
import com.iscscl.claims.common.product.personal.PolicyInfoBuilderBase;

/** Loads Policy Information Specific for the Claim
 * 
 * @author allend
 */
public class ClaimPolicyInfoBuilder extends PolicyInfoBuilderBase implements RuleProcessor, PolicyInfoRequest {

	
	
	// Rule processor interface
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user, JDBCData data) 
	throws RuleException {	
		ModelBean errors = null;
		ArrayList<ModelBean> errorList = new ArrayList<ModelBean>();
		try{
			// Load the coverage information for the claim risk 
			Log.debug("Processing ClaimPolicyInfoBuilder...");
			
			// Build the Errors ModelBean
			errors = new ModelBean("Errors");
			
			// Build Service Request ModelBean
            ModelBean serviceRq = buildRequest(ruleTemplate, bean);
            
            // Turn Off Debug Messages
            Log.setLoggingLevelError();
            
            // Send Webpath Request
			FrontController controller = new FrontController();
	        ModelBean response = controller.sendRequest(serviceRq, data);
	        ModelBean dtoPolicy = response.getBean("DTOPolicy");

			// Load the Policy Information
			Log.debug("DTOPolicy: " + dtoPolicy.readableDoc());

			// Save the old ClaimPolicyInfo
			ModelBean claimPolicyInfo = bean.getBean("ClaimPolicyInfo");
			storeClaimPolicyInfo(claimPolicyInfo);
			
			loadCoverageInfo(dtoPolicy, bean); 
			
			// Restore Claim Policy Limits
			restoreClaimPolicyLimits(claimPolicyInfo);
			
			// Load any reinsurance information if needed also
			ClaimPolicyReinsuranceBuilder.buildClaimReinsurance(bean, errorList);
			
	        for( ModelBean error : errorList ) {
	            errors.addValue(error);
	        }            

			Log.debug("Claim: " + bean.readableDoc());
							
		}
		catch(Exception e){	
			throw new RuleException(e);
		} finally {
			// Set Log Level back after processing the FrontController request
			Log.setLoggingLevelDefault();
		}
		return errors;		
	}
	
	/** Load the coverage(s) information that will apply to the claim 
	 * @param dtoPolicy The DTO Policy data Bean obtained from the external system
	 * @param claim The current claim container
	 * @throws RuleException when an unexpected error occurs 
	 */
	public void loadCoverageInfo(ModelBean dtoPolicy, ModelBean claim)
	throws RuleException {
		try {
			ArrayList<ModelBean> lineCoverages = new ArrayList<ModelBean>();
			ArrayList<ModelBean> riskCoverages = new ArrayList<ModelBean>();
						
			// Get All Active Coverages
			ModelBean[] coverages = dtoPolicy.findBeansByFieldValue("DTOCoverage","Status","Active");
			for (ModelBean coverage : coverages) {
				
				// Get the Parent to the Coverage (i.e. Line, Risk)
				ModelBean parent = coverage.getParentBean();
				
				// Load Line Level Coverages
				if( parent.getBeanName().equals("DTOLine") ) {
					ModelBean limit = buildCoverage(claim, coverage);
					lineCoverages.add(limit);		
				}
				
				// Load Risk Level Coverages
				if( parent.getBeanName().equals("DTORisk") ) {
					ModelBean risk = claim.getBeanById(claim.gets("RiskIdRef"));
					if( risk != null ) {
						String riskNumber = risk.gets("RiskNumber");
						ModelBean building = parent.getBean("DTOBuilding");
						if( building.gets("BldgNumber").equals(riskNumber) ) {
							ModelBean limit = buildCoverage(claim, coverage);
							riskCoverages.add(limit);
						}
					}
				}
			}
			
			ModelBean claimPolicyInfo = claim.getBean("ClaimPolicyInfo");
			
			ModelBean[] coverageActivities = findCoveragesWithActivity(claim, claimPolicyInfo);
			
			// Delete Existing ClaimPolicyLimit ModelBeans
			claimPolicyInfo.deleteBeans("ClaimPolicyLimit");
			
			// Add Line Coverages to ClaimPolicyInfo
			for (ModelBean lineCoverage : lineCoverages) {
				claimPolicyInfo.addValue(lineCoverage);
			}
			
			// Add Risk Coverages to ClaimPolicyInfo
			for (ModelBean riskCoverage : riskCoverages) { 
				claimPolicyInfo.addValue(riskCoverage);		
			}
			
			addCoveragesWithActivity(claim, claimPolicyInfo, coverageActivities);
			
		} catch (Exception e) {
			throw new RuleException(e);
		}
	}
	
	/** Build the claim limit using policy coverage information
	 * 
	 * @param coverage Policy coverage ModelBean
	 * @return Claim Limit ModelBean
	 * @throws Exception when an unexpected error occurs
	 */
	private static ModelBean buildCoverage(ModelBean claim, ModelBean coverage)
	throws Exception {
		ModelBean limit = new ModelBean("ClaimPolicyLimit");
		limit.setValue("SourceCd","Policy");
		limit.setValue("OriginalCoverageCd", coverage.gets("CoverageCd"));
		limit.setValue("CoverageCd", coverage.gets("CoverageCd"));
		limit.setValue("Description", coverage.gets("Description"));
		
		// Obtain the coverage limit
		ModelBean[] limits = coverage.getBeans("DTOLimit");
		for (int cnt=0; cnt<limits.length; cnt++) {
			if( !limits[cnt].gets("Value").equals("") ) {
				String typeCd = limits[cnt].gets("TypeCd");
				if (typeCd.matches(".*Aggregate.*")) {
					if (limit.gets("AggregateLimit").equals("")) { 
						limit.setValue("AggregateLimit", limits[cnt].gets("Value"));
						limit.setValue("AggregateLimitTypeCd", limits[cnt].gets("TypeCd"));
					}
				} else {
					if (limit.gets("Limit").equals(""))
						limit.setValue("Limit", limits[cnt].gets("Value"));
				}	
			}
		}		
		ModelBean deductible = coverage.getBean("DTODeductible");
		if (deductible != null) {
			limit.setValue("Deductible", deductible.gets("Value"));
		}
		
		// Build any sub limits
		buildSubCoverage(claim, coverage, limit);
		
		// Build the limit and deductible information fields
		buildCoverageDetails(coverage, limit);
		
		// Add the line code from the Line
		ModelBean line = BeanTools.getParentBean(coverage, "DTOLine");
		if (line != null)
			limit.setValue("LineCd", line.gets("LineCd"));
		
		return limit;
	}

	/** Build the limit items using Policy Coverage Item information
	 * 
	 * @param coverage The policy coverage ModelBean
	 * @param limit The claim limit ModelBean
	 * @throws Exception when an unexpected error occurs
	 */
	private static void buildSubCoverage(ModelBean claim, ModelBean coverage, ModelBean limit)
	throws Exception {
		ModelBean[] items = coverage.getBeans("DTOCoverageItem");
		
		for (int cnt=0; cnt<items.length; cnt++) {
			if (!items[cnt].gets("Status").equals("Active")) continue;
			ModelBean item = new ModelBean("ClaimPolicySubLimit");
			item.setValue("CoverageSubCd", items[cnt].gets("CoverageItemCd"));
			item.setValue("Description", items[cnt].gets("ItemDesc"));
			
			// Get the Item Category or Code
			ModelBean code = items[cnt].getBean("DTOCoverageData", "CoverageDataCd", "ClassCd");
			if (code != null) {
				item.setValue("Category", code.gets("Value"));
				item.setValue("CategoryDescription", code.gets("Value"));
				item.setValue("OriginalCoverageSubCd", code.gets("Value"));
			}
			
			item.setValue("ItemNumber", items[cnt].gets("SequenceNumber"));
			
			//Set the AnnualStatementLineCd
			String causeOfLoss = claim.gets("LossCauseCd");
	
			//For dwelling, any coverage that is for a property, if the damage is caused by anything except for fire or lightning,
			//set the AnnualStatementLineCd to 0200
			String productName = claim.getBean("ClaimPolicyInfo").gets("PolicyProductName");
			ModelBean coverageDefinition = Feature.getFeatureDefinition(claim, coverage.gets("CoverageCd"));
			String sectionCd = coverageDefinition.gets("SectionCd");
			if (productName.equals("Dwelling") 
					&& sectionCd.equals("Property") 
					&& (!causeOfLoss.equals("Fire") && !causeOfLoss.equals("Lightning"))){
				item.setValue("AnnualStatementLineCd", "0200");
			}
			
			
			// Get the limit
			ModelBean itemLimit = items[cnt].getBean("DTOLimit");
			item.setValue("Limit", itemLimit.gets("Value"));
			
			// Get the deductible
			ModelBean itemDeduct = items[cnt].getBean("DTODeductible");
			if (itemDeduct != null)
				item.setValue("Deductible", itemDeduct.gets("Value"));
			
			// Add the item to the limit
			limit.addValue(item);
			
			// Build the coverage item limit and deductible details
			buildSubCoverageDetails(coverage, item);
		}
	}
	
	/** Create the unique description fields for the limits and deductibles for each coverage 
	 * 
	 * @param coverage The policy coverage ModelBean
	 * @param limit The claim policy limit ModelBean
	 * @throws Exception when an error occurs
	 */
	private static void buildCoverageDetails(ModelBean coverage, ModelBean limit) 
	throws Exception {
		
		// Pseudo switch structure for limits
		for (int cnt=0; cnt<1; cnt++) {
			String covCd = coverage.gets("CoverageCd");
			
			// Increased Personal Property
			if (covCd.equals("ICC")) {
				limit.setValue("LimitDescription", "Additional Coverage Limit");
				break;
			}
			
			// Scheduled Personal Property
			if (covCd.equals("SPP")) {
				limit.setValue("LimitDescription", "Scheduled Limits");
				break;
			}

			String lim = limit.gets("Limit");
			if (StringTools.isNumeric(lim)) {
				Money limitAmt = new Money(lim);
				if (limitAmt.compareTo("0.00") > 0)
					limit.setValue("LimitDescription", "Policy Limit");
			}
			
		}
		
		// Pseudo switch structure for deductibles
		for (int cnt=0; cnt<1; cnt++) {
			String covCd = coverage.gets("CoverageCd");
			
			// Base Coverages
			if (covCd.equals("CovA") || covCd.equals("CovB") || covCd.equals("CovC") || 
				covCd.equals("CovD")) {
				
				//For dwelling, set the DeductibleAllocationTypeCd to Risk
				ModelBean parentBean = coverage.getParentBean();				
				if(parentBean.getBeanName().equals("Risk") && parentBean.gets("TypeCd").equals("Dwelling")){
					limit.setValue("DeductibleAllocationTypeCd", "Risk");
				}else if(parentBean.getBeanName().equals("Line") && parentBean.gets("LineCd").equals("Dwelling")){
					limit.setValue("DeductibleAllocationTypeCd", "Risk");
				}else{
					limit.setValue("DeductibleAllocationTypeCd", "Risk"); // For Homeowners product
				}
				
				limit.setValue("DeductibleDescription", "All Peril");
				limit.setValue("DeductibleAllocationCd", "AllPeril");
				
				break;
			}
						
			String deduct = limit.gets("Deductible");
			if (StringTools.isNumeric(deduct)) {
				Money deductAmt = new Money(deduct);
				if (deductAmt.compareTo("0.00") > 0){
					limit.setValue("DeductibleDescription", "Policy Deductible");
					limit.setValue("DeductibleAllocationCd", covCd);
					limit.setValue("DeductibleAllocationTypeCd", "Feature");
				}				
			}
			
			if (covCd.equals("SPP")) {
				limit.setValue("Deductible", "0");
				limit.setValue("DeductibleDescription", "Coverage Deductible");
				limit.setValue("DeductibleAllocationCd", covCd);
				limit.setValue("DeductibleAllocationTypeCd", "Feature");
			}
			
		}		
	}	
	
	/** Create the unique description fields for the limits and deductibles for each coverage item 
	 * 
	 * @param coverage The policy coverage ModelBean
	 * @param limit The claim policy limit ModelBean
	 * @throws Exception when an error occurs
	 */
	private static void buildSubCoverageDetails(ModelBean coverage, ModelBean limit) 
	throws Exception {
		String lim = limit.gets("Limit");
		if (StringTools.isNumeric(lim)) {
			Money limitAmt = new Money(lim);
			if (limitAmt.compareTo("0.00") > 0)
				limit.setValue("LimitDescription", "Scheduled Item Limit");
		}
		
		// Pseudo switch structure for description of coverage
		for (int cnt=0; cnt<1; cnt++) {
			String covCd = coverage.gets("CoverageCd");
			StringBuffer description = new StringBuffer();
			
			// Business Pursuits
			if (covCd.equals("BUP")) {
				ModelBean covItem = coverage.getBean("DTOCoverageItem", "SequenceNumber", limit.gets("ItemNumber"));
				ModelBean nameVal = covItem.findBeanByFieldValue("QuestionReply", "Name", "InsName");
				ModelBean occVal = covItem.findBeanByFieldValue("QuestionReply", "Name", "Occupation");
				ModelBean classVal = covItem.findBeanByFieldValue("QuestionReply", "Name", "ClassCode");
				
				description.append(nameVal.gets("Value"))
					.append("-")
					.append(occVal.gets("Value"))
					.append("-")
					.append(classVal.gets("Value"));
				limit.setValue("Description", description.toString());				
			} else if(covCd.equals("ARR")) {
				ModelBean covItem = coverage.getBean("DTOCoverageItem", "SequenceNumber", limit.gets("ItemNumber"));
				ModelBean addr = covItem.getBean("Addr");
				description.append(addr.gets("Addr1"))
						   .append(" ")
						   .append(addr.gets("City"))
						   .append(" ")
						   .append(addr.gets("StateProvCd"))
						   .append(" ")
						   .append(addr.gets("PostalCode"));
				limit.setValue("Description", description.toString());
			}
		}
		
	}
	
	/** Obtain the Policy Line Code from the coverage code
	 * @param policyProductVersionIdRef The policy product version id reference name 
	 * @param coverageCd The coverage code
	 * @return the line code
	 * @throws Exception when an error occurs
	 */
	public String getPolicyLineCd(String policyProductVersionIdRef, String coverageCd) 
	throws Exception {
		ModelBean serviceRq = WebPathHelper.buildServiceRequest("UWProductGetLine", ServiceContext.getServiceContext().getUserId());
		WebPathHelper.addParam(serviceRq, "ProductVersionIdRef", policyProductVersionIdRef);
		WebPathHelper.addParam(serviceRq, "CoverageCd", coverageCd);
		        
        // Set to Log errors
		Log.debug("Setting logging level to error");
		int oldLogLevel = Log.setLogLevel(Log.ERROR);

        // Call the Webpath Request Service
        ModelBean response = WebPathHelper.callService(serviceRq);
        
        // Turn Logging back to the previous set level
        Log.setLogLevel(oldLogLevel);
        Log.debug("Restore previous logging level");
                
        ModelBean dtoLine = response.getBean("DTOLine");
        if (dtoLine != null){
        	return dtoLine.gets("LineCd");
        }
        
        return null;	
	}
 
}
