package com.ric.claims.common.product.model.template.ric.dwellingproperty.cw.v01_00_00.rule;

import com.iscs.claims.claim.render.ClaimRenderer;
import com.iscs.claims.common.product.render.ClaimProductRenderer;
import com.iscs.claims.common.render.ReserveRenderer;
import com.iscs.claims.common.shared.render.ClaimAIRenderer;
import com.iscs.claims.notice.render.NoticeRenderer;
import com.iscs.claims.transaction.render.ClaimantTransactionRenderer;
import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.render.DateRenderer;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.utility.bean.render.BeanRenderer;
import com.iscs.common.utility.validation.render.ValidationRenderer;
import com.ric.uw.common.product.model.template.ric.dwellingproperty.render.DwellingProductRenderer;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

public class TransactionApproval implements RuleProcessor {
	
	private ModelBean claim;
	private ModelBean oldClaim;
	private ModelBean response;
	private StringDate todayDt;
	private ModelBean errors;
	private ModelBean user;
	private JDBCData data;
	private Boolean isNewClaim;
	
	@Override
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user,
			JDBCData data) throws RuleException {
		
		try {
			claim = bean;
			todayDt = new StringDate(DateRenderer.getDate());
			response = additionalBeans[2];
			errors = response.getBean("ResponseParams").getBean("Errors");
			this.user = user;
			this.data = data;
			isNewClaim = claim.gets("ClaimRef").equals("");
			
			if( !isNewClaim ){
				oldClaim = ClaimRenderer.getClaim(data, claim.gets("ClaimRef"));
			}

      processRules();
			
		} catch(Exception e) {
			throw new RuleException(e);
		}
		
		return errors;
	}

	public void processRules() throws Exception {
		ValidationRenderer.clearValidationErrors(claim,"Approval");
		//ValidationRenderer.clearValidationErrors(claim, "Validation");
		
		triggerExceededMaximumOrIncreasedApprovals();
		triggerPayToOverrideCheckApproval();
		triggerMaximumReservesThirdPartyClaimsApprovals();
		triggerMaximumPaymentsCheckApprovals();
		triggerPolicyLimitExceedsApproval();
		triggerMedicareChangesRequireRereportingWarning();
		triggerCatastropheChangeApproval();
		triggerAdditionalInterestChangeApproval();
		triggerLossDtExceedsPolicyEquityDtWarning();
	}

	private void triggerPayToOverrideCheckApproval() throws Exception {
		String allowPayToOverride = ClaimProductRenderer.getAuthorityAttributeValue(claim.gets("ProductVersionIdRef"), "AllowPayToOverride", user, todayDt);

		if( StringRenderer.isFalse(allowPayToOverride) ){
			ModelBean[] transactions = claim.findBeansByFieldValue("ClaimantTransaction", "ClaimTransactionHistoryRef", "");
			for( ModelBean transaction : transactions ){
				if( transaction.gets("PayToOverrideInd").equals("Yes") ){
					ValidationRenderer.addValidationError(claim,"Approval","APPROVAL", "Pay To Override Must Be Approved for all payments.");
					break;
				}
			}
		}
	}
	
	private void triggerMaximumReservesThirdPartyClaimsApprovals() throws Exception {
		String maximumAdjustmentReserveAmt = ClaimProductRenderer.getAuthorityAttributeValue(claim.gets("ProductVersionIdRef"), "MaximumAdjustmentReserveAmt", user, todayDt);
		String maximumIndemnityReserveAmt = ClaimProductRenderer.getAuthorityAttributeValue(claim.gets("ProductVersionIdRef"), "MaximumIndemnityReserveAmt", user, todayDt);
		
		//Check for Maximum Reserves for Third Party Claims
		Boolean thirdPartyClaims = DwellingProductRenderer.getThirdPartyClaimantDetails(data, claim, maximumAdjustmentReserveAmt, isNewClaim, oldClaim, maximumIndemnityReserveAmt);
		
		//Check for Maximum Reserves
		ModelBean[] claimants = claim.getBeans("Claimant");
		if (!thirdPartyClaims) {
		    for (ModelBean claimant: claimants) {
		        //If Adjustment Amount Exceeds Maximum
		        String reserveAdjustmentAmt = DwellingProductRenderer.getClaimantTotalExpense(data, claim, claimant).toString();
		        if (StringRenderer.greaterThan(reserveAdjustmentAmt.toString(), maximumAdjustmentReserveAmt)) {
		            String showAmt = StringRenderer.formatValue(maximumAdjustmentReserveAmt, 2, "0.00", true, true);
		            String showMsg = String.format( "Maximum Reserve Adjustment Amount For %s : %s exceeded limit of %s", 
		            		claimant.gets("ClaimantNumber"), claimant.gets("IndexName"), showAmt);
		            
		            if (isNewClaim) {
		                ValidationRenderer.addValidationError(claim, "Approval", "APPROVAL", showMsg);
		            } else {
		                //If Reserve Increased Since Last Transaction, Then Create Approval Error
		                ModelBean oldClaimant = oldClaim.getBeanById("Claimant", claimant.getId());
		                if (!oldClaimant.equals("")) {
		                    String oldReserveAdjustmentAmt = DwellingProductRenderer.getClaimantTotalExpense(data, oldClaim, oldClaimant).toString();
		                    if (StringRenderer.greaterThan(reserveAdjustmentAmt.toString(), oldReserveAdjustmentAmt.toString())) {
		                        String showOldReserve = StringRenderer.formatValue(oldReserveAdjustmentAmt, 2, "0.00", true, true);
		                        String showCurrReserve = StringRenderer.formatValue(reserveAdjustmentAmt, 2, "0.00", true, true);
		                        showMsg = String.format( "Reserve Adjustment Amount For %s : %s was increased from %s to %s", 
		                        		claimant.gets("ClaimantNumber"), claimant.gets("IndexName"), showOldReserve, showCurrReserve );
		                        ValidationRenderer.addValidationError(claim, "Approval", "APPROVAL", showMsg);
		                    }
		                }
		            }
		        }
		        
		        //If Indemnity Amount Exceeds Maximum
		        String reserveIndemnityAmt = DwellingProductRenderer.getClaimantTotalIndemnity(data, claim, claimant).toString();
		        if (StringRenderer.greaterThan(reserveIndemnityAmt.toString(), maximumIndemnityReserveAmt)) {
		            String showAmt = StringRenderer.formatValue(maximumIndemnityReserveAmt, 2, "0.00", true, true);
		            String showMsg = String.format( "Maximum Reserve Indemnity Amount For %s : %s exceeded limit of %s", 
		            		claimant.gets("ClaimantNumber"), claimant.gets("IndexName"), showAmt );
		            if (isNewClaim) {
		                ValidationRenderer.addValidationError(claim, "Approval", "APPROVAL", showMsg);
		            } else {
		                //If Reserve Increased Since Last Transaction, Then Create Approval Error
		                ModelBean oldClaimant = oldClaim.getBeanById("Claimant", claimant.getId());
		                
		                if (!oldClaimant.equals("")) {
		                    String oldReserveIndemnityAmt = DwellingProductRenderer.getClaimantTotalIndemnity(data, oldClaim, oldClaimant).toString();
		                    
		                    if (StringRenderer.greaterThan(reserveIndemnityAmt.toString(), oldReserveIndemnityAmt.toString())) {
		                        String showOldReserve = StringRenderer.formatValue(oldReserveIndemnityAmt, 2, "0.00", true, true);
		                        String showCurrReserve = StringRenderer.formatValue(reserveIndemnityAmt, 2, "0.00", true, true);
		                        showMsg = String.format( "Reserve Indemnity Amount For %s : %s was increased from %s to %s", 
		                        		claimant.gets("ClaimantNumber"), claimant.gets("IndexName"), showOldReserve, showCurrReserve );
		                        ValidationRenderer.addValidationError(claim, "Approval", "APPROVAL", showMsg);
		                    }
		                }
		            }
		        }
		    }
		}
		
	}

	private void triggerMaximumPaymentsCheckApprovals() throws Exception {
		String maximumAdjustmentPaymentAmt = ClaimProductRenderer.getAuthorityAttributeValue(claim.gets("ProductVersionIdRef"), "MaximumAdjustmentPaymentAmt", user, todayDt);
		String maximumIndemnityPaymentAmt = ClaimProductRenderer.getAuthorityAttributeValue(claim.gets("ProductVersionIdRef"), "MaximumIndemnityPaymentAmt", user, todayDt);
		ModelBean[] allClaimantTransactions = claim.getAllBeans("ClaimantTransaction");
		String totalAdjustment = "0.00";
		String totalIndemnity = "0.00";
		Boolean showAdjustmentMsg = false;
		Boolean showPaymentMsg = false;

		for( ModelBean claimantTransaction : allClaimantTransactions ){
			for( ModelBean reserveAllocation : claimantTransaction.getAllBeans("ReserveAllocation") ){
				String reserveType = ReserveRenderer.getReserveType(claim, reserveAllocation.gets("ReserveCd"));

				if( reserveType.equals("Adjustment") )
					totalAdjustment = StringRenderer.addMoney(totalAdjustment, reserveAllocation.gets("PaidAmt"));
				else if( reserveType.equals("Indemnity") )
					totalIndemnity = StringRenderer.addMoney(totalIndemnity, reserveAllocation.gets("PaidAmt"));
			}
		}

		allClaimantTransactions = claim.findBeansByFieldValue("ClaimantTransaction", "StatusCd", "Active");

		for( ModelBean claimantTransaction : allClaimantTransactions ){
			for( ModelBean reserveAllocation : claimantTransaction.getAllBeans("ReserveAllocation") ){
				String reserveType = ReserveRenderer.getReserveType(claim, reserveAllocation.gets("ReserveCd"));

				if( reserveType.equals("Adjustment") && StringRenderer.greaterThan(totalAdjustment, maximumAdjustmentPaymentAmt) && !showAdjustmentMsg){
					String amt = StringRenderer.formatValue(maximumAdjustmentPaymentAmt, 2, "0.00", true, true);
					String msg = String.format("Maximum Total Payment Adjustment Amount exceeded limit of %s", amt);
					ValidationRenderer.addValidationError(claim,"Approval","APPROVAL", msg);
					showAdjustmentMsg = true;
				} else if( reserveType.equals("Indemnity") && StringRenderer.greaterThan(totalIndemnity, maximumIndemnityPaymentAmt) && !showPaymentMsg ) {
					String amt = StringRenderer.formatValue(maximumIndemnityPaymentAmt, 2, "0.00", true, true);
					String msg = String.format("Maximum Total Payment Indemnity Amount exceeded limit of %s", amt);
					ValidationRenderer.addValidationError(claim,"Approval","APPROVAL", msg);
					showPaymentMsg = true;
				}		
			}
		}
	}

	private void triggerExceededMaximumOrIncreasedApprovals() throws Exception {
		ModelBean[] claimants = claim.getBeans("Claimant");

		for( ModelBean claimant : claimants ){
			triggerAdjustmentExceededMaximumOrIncreasedApproval(claimant);
			triggerIndemnityExceededMaximumOrIncreasedApproval(claimant);
		}
	}

	//Maximum Adjustment Reserves
	private void triggerAdjustmentExceededMaximumOrIncreasedApproval( ModelBean claimant ) throws Exception {
		String maximumAdjustmentReserveAmt = ClaimProductRenderer.getAuthorityAttributeValue(claim.gets("ProductVersionIdRef"), "MaximumAdjustmentReserveAmt", user, todayDt);
		String reserveAdjustmentAmt = DwellingProductRenderer.getClaimantTotalExpense(data, claim, claimant).toString();

		if( StringRenderer.greaterThan(reserveAdjustmentAmt, maximumAdjustmentReserveAmt) ){
			if( isNewClaim ){
				String amt = StringRenderer.formatMoney( maximumAdjustmentReserveAmt, true);
				String msg = String.format("Maximum Reserve Adjustment Amount for %s : %s exceded limit of %s.", 
						claimant.gets("ClaimantNumber"), claimant.gets("IndexName"), amt);
				ValidationRenderer.addValidationError(claim, "Approval", "APPROVAL", msg);
			} else {
				ModelBean oldClaimant = oldClaim.getBeanById("Claimant", claimant.getId());
				if( oldClaimant != null ){
					String oldReserveAdjustmentAmt = DwellingProductRenderer.getClaimantTotalExpense(data, oldClaim, oldClaimant).toString();
					if( StringRenderer.greaterThan(reserveAdjustmentAmt, oldReserveAdjustmentAmt) ){
						String oldReserveAmt = StringRenderer.formatMoney(oldReserveAdjustmentAmt, true);
						String currentReserveAmt = StringRenderer.formatMoney(reserveAdjustmentAmt, true);
						String msg = String.format("Reserve Adjustment Amount For %s : %s was increased from %s to %s", 
								claimant.gets("ClaimantNumber"), claimant.gets("IndexName"), oldReserveAmt, currentReserveAmt );
						ValidationRenderer.addValidationError(claim, "Approval", "APPROVAL", msg);
					}
				}
			}
		}
	}

	//Maximum Indemnity Reserves
	private void triggerIndemnityExceededMaximumOrIncreasedApproval( ModelBean claimant ) throws Exception {
		String maximumIndemnityReserveAmt = ClaimProductRenderer.getAuthorityAttributeValue(claim.gets("ProductVersionIdRef"), "MaximumIndemnityReserveAmt", user, todayDt);
		String reserveAdjustmentAmt = DwellingProductRenderer.getClaimantTotalExpense(data, claim, claimant).toString();

		if( StringRenderer.greaterThan(reserveAdjustmentAmt, maximumIndemnityReserveAmt) ){
			if( isNewClaim ){
				String amt = StringRenderer.formatMoney( maximumIndemnityReserveAmt, true);
				String msg = String.format("Maximum Reserve Indemnity Amount for %s : %s exceded limit of %s.", 
						claimant.gets("ClaimantNumber"), claimant.gets("IndexName"), amt);
				ValidationRenderer.addValidationError(claim, "Approval", "APPROVAL", msg);
			} else {
				ModelBean oldClaimant = oldClaim.getBeanById("Claimant", claimant.getId());
				if( oldClaimant != null ){
					String oldReserveAdjustmentAmt = DwellingProductRenderer.getClaimantTotalExpense(data, oldClaim, oldClaimant).toString();
					if( StringRenderer.greaterThan(reserveAdjustmentAmt, oldReserveAdjustmentAmt) ){
						String oldReserveAmt = StringRenderer.formatMoney(oldReserveAdjustmentAmt, true);
						String currentReserveAmt = StringRenderer.formatMoney(reserveAdjustmentAmt, true);
						String msg = String.format("Reserve Indemnity Amount For %s : %s was increased from %s to %s", 
								claimant.gets("ClaimantNumber"), claimant.gets("IndexName"), oldReserveAmt, currentReserveAmt );
						ValidationRenderer.addValidationError(claim, "Approval", "APPROVAL", msg);
					}
				}
			}
		}
	}

	private void triggerPolicyLimitExceedsApproval() throws Exception {
		String allowExceedPolicyLimits = ClaimProductRenderer.getAuthorityAttributeValue(claim.gets("ProductVersionIdRef"), "AllowExceedPolicyLimits", user, todayDt);

		if( StringRenderer.isFalse(allowExceedPolicyLimits) ){
			ModelBean[] exceedList = ClaimantTransactionRenderer.exceededCoverageLimits(data, claim);

			for( ModelBean exceedItem : exceedList ){
				String msg = String.format("Policy limit of %s was exceeded for feature %s", exceedItem.gets("ReserveAmt"), exceedItem.gets("FeatureCd"));
				ValidationRenderer.addValidationError(claim, "Approval", "APPROVAL", msg);
			}
		}
	}

	private void triggerCatastropheChangeApproval() throws Exception {
		String allowCatastropheChange = ClaimProductRenderer.getAuthorityAttributeValue(claim.gets("ProductVersionIdRef"), "AllowCatastropheChange", user, todayDt);

		if( StringRenderer.isFalse(allowCatastropheChange) && !isNewClaim && 
				!claim.gets("CatastropheRef").equals( oldClaim.gets("CatastropheRef") ) ){
			ValidationRenderer.addValidationError(claim, "Approval", "APPROVAL", "Catastrophe changes require approval");
		}
	}

	private void triggerAdditionalInterestChangeApproval() throws Exception {
		String allowAdditionalInsuredEdit = ClaimProductRenderer.getAuthorityAttributeValue(claim.gets("ProductVersionIdRef"), "AllowAdditionalInsuredEdit", user, todayDt);

		if( StringRenderer.isFalse(allowAdditionalInsuredEdit) ){
			ModelBean policyAIs = claim.getBean("ClaimPolicyInfo").getBean("PolicyAIs");

			for( ModelBean policyAI : policyAIs.getBeans("PolicyAI") ){
				if( policyAI.gets("SourceCd").equals("Claim") ){
					ModelBean oldPolicyAI = oldClaim.getBeanById("PolicyAI", policyAI.getId());

					if( oldPolicyAI == null && policyAI.gets("Status").equals("Active") ){
						String msg = String.format("Additional Interest # %s - %s was added which requires approval", 
								policyAI.gets("SequenceNumber"), policyAI.gets("Name"));
						ValidationRenderer.addValidationError(claim, "Approval", "APPROVAL", msg);
					} else if( oldPolicyAI.gets("Status").equals("Active") && policyAI.gets("Status").equals("Deleted") ){
						String msg = String.format("Additional Interest # %s - %s was deleted which requires approval", 
								policyAI.gets("SequenceNumber"), policyAI.gets("Name"));
						ValidationRenderer.addValidationError(claim, "Approval", "APPROVAL", msg);
					} else {
						ClaimAIRenderer claimAIRenderer = new ClaimAIRenderer();
						
						if( claimAIRenderer.hasValuesChanged(oldPolicyAI, policyAI ) ){
							String msg = String.format("Additional Interest # %s - %s was modified which requires approval", 
									policyAI.gets("SequenceNumber"), policyAI.gets("Name"));
							ValidationRenderer.addValidationError(claim, "Approval", "APPROVAL", msg);
						}
					}
				}

			}
		}
	}

	private void triggerLossDtExceedsPolicyEquityDtWarning() throws Exception {
		ValidationRenderer validationRenderer = new ValidationRenderer();
		Boolean lossDtExceedsWarningExists = validationRenderer.validationErrorExists(claim, null, null, null, "Loss Date exceeds the Policy Equity Date of.*");

		if( !lossDtExceedsWarningExists ){
			String policyEquityDt = NoticeRenderer.getPolicyEquityDtExceedingLossDt(claim);
			if( !policyEquityDt.equals("") ){
				String msg = "Loss Date exceeds the Policy Equity Date of " + policyEquityDt;
				ValidationRenderer.addValidationError(claim,"Validation","WARNING", msg);
			}
		}
	}


	private void triggerMedicareChangesRequireRereportingWarning() throws Exception {
		Boolean previouslyReportedInd = false;
		ModelBean[] claimants = claim.getBeans("Claimant");

		for( ModelBean claimant : claimants ){
			if(oldClaim != null) {
				ModelBean oldClaimant = oldClaim.getBeanById("Claimant", claimant.getId());
	
				if( oldClaimant != null ){
					ModelBean injuredParty = claimant.getBean("InjuredParty");
					ModelBean oldInjuredParty = oldClaimant.getBean("InjuredParty");
					ModelBean claimantParty = BeanRenderer.getBeanWithAlias(claimant, "ClaimantParty");
					ModelBean oldClaimantParty = BeanRenderer.getBeanWithAlias(oldClaimant,"ClaimantParty");
	
					if( !oldClaimant.gets("MedicareReportingFilename").equals("") ){
						previouslyReportedInd = true;
						String oldInjuryDesc = oldClaimant.gets("InjuryDesc");
	
						if( !claimant.gets("InjuryDesc").equals(oldInjuryDesc) ){
							if( oldInjuryDesc.equals("") )
								oldInjuryDesc = "blank";
	
							String warningMsg = String.format("Manual Reporting to Medicare required : Injury description has changed from %s to %s for Claimant #%s", 
									oldInjuryDesc, claimant.gets("InjuryDesc"), claimant.gets("ClaimantNumber"));
							ValidationRenderer.addValidationError(claim,"Validation","WARNING",warningMsg);	
						}
	
						if( injuredParty != null && oldInjuredParty != null ){
							String previousHICN = oldInjuredParty.gets("HealthInsuranceClaimNumber");
	
							if( !injuredParty.gets("HealthInsuranceClaimNumber").equals(previousHICN) ){
								if( previousHICN.equals("") )
									previousHICN = "blank";
	
								String warningMsg = 
										String.format( "Manual Reporting to Medicare required : Health Insurance Claim Number has changed from %s to %s for Claimant #%s", 
										previousHICN, injuredParty.gets("HealthInsuranceClaimNumber"), claimant.gets("ClaimantNumber"));
								ValidationRenderer.addValidationError(claim,"Validation","WARNING",warningMsg);					
							}
	
							// ORM Indicator/Termination Date have changed
							String previousORMInd = oldInjuredParty.gets("OngoingResponsibilityMedicalsInd");
							if( !injuredParty.gets("OngoingResponsibilityMedicalsInd").equals(previousORMInd) ){
								if(previousORMInd.equals(""))
									previousORMInd = "blank";
	
								String warningMsg = 
										String.format("Manual Reporting to Medicare required : Ongoing Medical Responsibility has changed from %s to %s for Claimant #%s", 
										previousORMInd, injuredParty.gets("OngoingResponsibilityMedicalsInd"), claimant.gets("ClaimantNumber"));
								ValidationRenderer.addValidationError(claim,"Validation","WARNING",warningMsg);		
							}
	
							String previousORMDt = oldInjuredParty.gets("OngoingResponsibilityMedicalsTerminationDt");
							if( !injuredParty.gets("OngoingResponsibilityMedicalsTerminationDt").equals(previousORMDt) ){
								if( previousORMDt.equals("") )
									previousORMDt = "blank";
	
								String warningMsg = 
										String.format("Manual Reporting to Medicare required : Ongoing Medical Responsibility Termination Date has changed from %s to %s for Claimant #%s", 
										previousORMDt, injuredParty.gets("OngoingResponsibilityMedicalsTerminationDt"), claimant.gets("ClaimantNumber"));
								ValidationRenderer.addValidationError(claim,"Validation","WARNING",warningMsg);
							}
	
							ModelBean[] tpocs = injuredParty.getBeans("TotalPaymentObligationToClaimant");   			
							for( ModelBean tpoc : tpocs){
								ModelBean previousTpoc = oldInjuredParty.getBeanById("TotalPaymentObligationToClaimant",tpoc.getId());
	
								//Check if TPOC Date/Amount have changed
								String previousAmt = previousTpoc.gets("TotalPaymentAmt");
								if( !tpoc.gets("TotalPaymentAmt").equals(previousAmt)){
									if( previousAmt.equals("") )
										previousAmt = "blank";
	
									String warningMsg = 
											String.format("Manual Reporting to Medicare required : TPOC Amount has changed from %s to %s for Claimant #%s", 
											previousAmt, tpoc.gets("TotalPaymentAmt"), claimant.gets("ClaimantNumber"));
									ValidationRenderer.addValidationError(claim,"Validation","WARNING",warningMsg);
								}
	
								String previousPaymentDt = previousTpoc.gets("PaymentDueDt");
								if( !tpoc.gets("PaymentDueDt").equals(previousPaymentDt)){
									if( previousPaymentDt.equals("") )
										previousPaymentDt = "blank";
	
									String warningMsg = String.format("Manual Reporting to Medicare required : TPOC Date has changed from %s to %s for Claimant #%s", 
											previousPaymentDt, tpoc.gets("PaymentDueDt"), claimant.gets("ClaimantNumber"));
									ValidationRenderer.addValidationError(claim,"Validation","WARNING",warningMsg);
								}
							}
	
	
							ModelBean[] icd9s = injuredParty.getBeans("Diagnosis");
							for( ModelBean icd9 : icd9s){
								ModelBean previousIcd9 = oldInjuredParty.getBeanById("Diagnosis",icd9.getId());
	
								//Check if diagnosis code has changed
								String previousCode = previousIcd9.gets("DiagnosisCd");
								if( !icd9.gets("DiagnosisCd").equals(previousCode)){
									if( previousCode.equals("") )
										previousCode = "blank";
	
									String warningMsg = String.format("Manual Reporting to Medicare required : Diagnosis code has changed from %s to %s for Claimant #%s", 
											previousCode, icd9.gets("DiagnosisCd"), claimant.gets("ClaimantNumber"));
									ValidationRenderer.addValidationError(claim,"Validation","WARNING",warningMsg);
								}
							}
						} // Injured Party and Old Injured Party not null
	
						if( StringRenderer.isTrue(claimant.gets("FatalityInd")) ){
							String previousRelationshipCd = oldClaimant.gets("InjuredPartyRelationshipCd");
	
							//Check if relationship code has changed
							if( !claimant.gets("InjuredPartyRelationshipCd").equals(previousRelationshipCd) ){
								if( previousRelationshipCd.equals("") )
									previousRelationshipCd = "blank";
	
								String warningMsg = String.format("Manual Reporting to Medicare required : Claimant Relationship has changed from %s to %s for Claimant #%s", previousRelationshipCd, claimant.gets("InjuredPartyRelationshipCd"), claimant.gets("ClaimantNumber"));
								ValidationRenderer.addValidationError(claim,"Validation","WARNING",warningMsg);
							}
	
	
							//Tax id change
							ModelBean partyTax = BeanRenderer.getBeanWithAlias(claimantParty, "ClaimantTax");
							ModelBean oldPartyTax = BeanRenderer.getBeanWithAlias(oldClaimantParty, "ClaimantTax");
							String previousSSN = oldPartyTax.gets("SSN");
							if( !partyTax.gets("SSN").equals(previousSSN) ){
								if( previousSSN.equals("") )
									previousSSN = "blank";
	
								String warningMsg = String.format("Manual Reporting to Medicare required : Claimant SSN has changed from %s to %s for Claimant #%s", 
										previousSSN, partyTax.gets("SSN"), claimant.gets("ClaimantNumber"));
								ValidationRenderer.addValidationError(claim,"Validation","WARNING",warningMsg);
							}
	
							String previousFEIN = oldPartyTax.gets("FEIN");
							if( !partyTax.gets("FEIN").equals(previousFEIN) ){
								if( previousFEIN.equals("") )
									previousFEIN = "blank";
	
								String warningMsg = String.format("Manual Reporting to Medicare required : Claimant taxIdType has changed from %s to %s for Claimant #%s", 
										previousFEIN, partyTax.gets("FEIN"), claimant.gets("ClaimantNumber"));
								ValidationRenderer.addValidationError(claim,"Validation","WARNING",warningMsg);
							}
	
							//Name change
							ModelBean partyName = BeanRenderer.getBeanWithAlias(claimantParty, "ClaimantName");
							ModelBean previousPartyName = BeanRenderer.getBeanWithAlias(oldClaimantParty, "ClaimantName");
							String previousSurname = previousPartyName.gets("Surname");
							if( !partyName.gets("Surname").equals(previousSurname) ){
								if( previousSurname.equals("") )
									previousSurname = "blank";
	
								String warningMsg = String.format("Manual Reporting to Medicare required : Claimant Last Name has changed from %s to %s for Claimant #%s", 
										previousSurname, partyName.gets("Surname"), claimant.gets("ClaimantNumber"));
								ValidationRenderer.addValidationError(claim,"Validation","WARNING",warningMsg);
							}
	
							String previousGivenName = previousPartyName.gets("GivenName");
							if( !partyName.gets("GivenName").equals(previousGivenName) ){
								if( previousGivenName.equals("") )
									previousGivenName = "blank";
	
								String warningMsg = String.format("Manual Reporting to Medicare required : Claimant First Name has changed from %s to %s for Claimant #%s", 
										previousGivenName, partyName.gets("GivenName"), claimant.gets("ClaimantNumber"));
								ValidationRenderer.addValidationError(claim,"Validation","WARNING",warningMsg);
							}
	
							String previousOtherGivenName = previousPartyName.gets("OtherGivenName");
							if( !partyName.gets("OtherGivenName").equals(previousOtherGivenName) ){
								if( previousOtherGivenName.equals("") )
									previousOtherGivenName = "blank";
	
								String warningMsg = String.format("Manual Reporting to Medicare required : Claimant Middle Name has changed from %s to %s for Claimant #%s", 
										previousOtherGivenName, partyName.gets("OtherGivenName"), claimant.gets("ClaimantNumber"));
								ValidationRenderer.addValidationError(claim,"Validation","WARNING",warningMsg);
							}
	
							String previousCommercialName = previousPartyName.gets("CommercialName");
							if( !partyName.gets("CommercialName").equals(previousCommercialName) ){
								if( previousCommercialName.equals("") )
									previousCommercialName = "blank";
		
								String warningMsg = String.format("Manual Reporting to Medicare required : Claimant Name has changed from %s to %s for Claimant #%s", 
										previousCommercialName, partyName.gets("CommercialName"), claimant.gets("ClaimantNumber"));
								ValidationRenderer.addValidationError(claim,"Validation","WARNING",warningMsg);
							}
	
							//Address change
							ModelBean partyAddr = BeanRenderer.getBeanWithAlias(claimantParty, "ClaimantMailingAddr");
							ModelBean previousPartyAddr = BeanRenderer.getBeanWithAlias(oldClaimantParty, "ClaimantMailingAddr");
		
							String previousAddr1 = previousPartyAddr.gets("Addr1");
							if( !partyAddr.gets("Addr1").equals(previousAddr1) ){
								if( previousAddr1.equals("") )
									previousAddr1 = "blank";
		
								String warningMsg = String.format("Manual Reporting to Medicare required : Claimant Address has changed from %s to %s for Claimant #%s", 
										previousAddr1, partyAddr.gets("Addr1"), claimant.gets("ClaimantNumber"));
								ValidationRenderer.addValidationError(claim,"Validation","WARNING",warningMsg);
							}
		
							String previousAddr2 = previousPartyAddr.gets("Addr2");
							if( !partyAddr.gets("Addr2").equals(previousAddr2) ){
								if( previousAddr2.equals("") )
									previousAddr2 = "blank";
		
								String warningMsg = String.format("Manual Reporting to Medicare required : Claimant Address has changed from %s to %s for Claimant #%s", 
										previousAddr2, partyAddr.gets("Addr2"), claimant.gets("ClaimantNumber"));
								ValidationRenderer.addValidationError(claim,"Validation","WARNING",warningMsg);
							}
		
							String previousCity = previousPartyAddr.gets("City");
							if( !partyAddr.gets("City").equals(previousCity)){
								if( previousCity.equals("") )
									previousCity = "blank";
		
								String warningMsg = String.format("Manual Reporting to Medicare required : Claimant City has changed from %s to %s for Claimant #%s", 
										previousCity, partyAddr.gets("City"), claimant.gets("ClaimantNumber"));
								ValidationRenderer.addValidationError(claim,"Validation","WARNING",warningMsg);
							}
		
							String previousState = previousPartyAddr.gets("StateProvCd");
							if( !partyAddr.gets("StateProvCd").equals(previousState) ){
								if( previousState.equals("") )
									previousState = "blank";
		
								String warningMsg = String.format("Manual Reporting to Medicare required : Claimant State has changed from %s to %s for Claimant #%s", 
										previousState, partyAddr.gets("StateProvCd"), claimant.gets("ClaimantNumber"));
								ValidationRenderer.addValidationError(claim,"Validation","WARNING",warningMsg);
							}
		
							String previousZip = previousPartyAddr.gets("PostalCode");
							if( !partyAddr.gets("PostalCode").equals(previousZip) ){
								if( previousZip.equals("") )
									previousZip = "blank";
		
								String warningMsg = String.format("Manual Reporting to Medicare required : Claimant Zip has changed from %s to %s for Claimant #%s", 
										previousZip, partyAddr.gets("PostalCode"), claimant.gets("ClaimantNumber"));
								ValidationRenderer.addValidationError(claim,"Validation","WARNING",warningMsg);
							}		
						} //Fatality Indicator is Yes
					} //Old claimant previously reported to medicare			
				} //Old claimant not null
			}
		} //Claimants loop
		
		if( previouslyReportedInd ){
			//if any of the claimants was reported to Medicare and the loss date has changed, re-report
			String previousLossDt = oldClaim.gets("LossDt");
			if( !claim.gets("LossDt").equals(previousLossDt) ){
				String warningMsg = String.format("Manual Reporting to Medicare required : Loss Date has changed from %s to %s" , previousLossDt, claim.gets("LossDt") );
				ValidationRenderer.addValidationError(claim,"Validation","WARNING",warningMsg);
			}
		}
	}
}
