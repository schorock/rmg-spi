package com.ric.claims.common.product.model.template.ric.dwellingproperty.handler;

import java.util.ArrayList;

import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.tech.log.Log;
import com.iscs.insurance.product.ProductSetup;
import com.iscscl.claims.common.product.ClaimPolicyReinsuranceBuilder;
import com.iscscl.claims.common.product.personal.PolicyInfoBuilderBase;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.webpath.FrontController;

/** Loads All Policy Information for the Claim
 * 
 * @author moniquef
 */
public class DwellingPolicyInfoBuilder extends PolicyInfoBuilderBase implements RuleProcessor {

	// Rule processor interface
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user, JDBCData data) 
	throws RuleException {	
		ModelBean errors = null;
		ArrayList<ModelBean> errorList = new ArrayList<ModelBean>();
		try{
			Log.debug("Processing PolicyInfoBuilder...");
			
			// Initialize the info builder
			initializeBuilder(ruleTemplate);
			
			// Build the Errors ModelBean
			errors = new ModelBean("Errors");
			
			// Check if the Loss Notice is Linked to a Policy
			String policyRef = bean.getBean("ClaimPolicyInfo").gets("PolicyRef");
			if( policyRef.equals("") ) {
				bean.setValue("VehicleInvolvedInd", "No");
				createEmptyRisk(bean);	
				createEmptyInsuredPartyInfo(bean);
				return errors;		
			}
			
			// Check if Product Line Code is Blank Due to Change in Product
			if( bean.valueEquals("ProductLineCd", "") ) 
				return errors;		
			
			// Get DTOPolicy
            ModelBean dtoPolicy = null;                                    
            ModelBean dtoLatestPolicy = null;
            
            if( bean.valueEquals("SourceCd","Innovation") ) {
            	// If Loss Notice Source Equals Innovation, Then Fetch DTOPolicy Using Webpath Request

            	// Build Service Request ModelBean
            	ModelBean serviceRq = buildRequest(ruleTemplate, bean);

            	// Turn Off Debug Messages
            	Log.setLoggingLevelError();

            	// Send Webpath Request
            	FrontController controller = new FrontController();
            	ModelBean response = controller.sendRequest(serviceRq, data);
            	dtoPolicy = response.getBean("DTOPolicy");	

            	// Send Webpath Request
            	controller = new FrontController();
            	//Build Service Request modelbean for latest term for the policy
            	StringDate endDt = new StringDate("20991231");
            	ModelBean serviceRqForLatestPolicy = buildRequest(ruleTemplate, bean, endDt);

            	// Send Webpath Request
            	ModelBean responseForLatestPolicy = controller.sendRequest(serviceRqForLatestPolicy, data);
            	dtoLatestPolicy = responseForLatestPolicy.getBean("DTOPolicy");

            	// Turn On Debug Messages
            	Log.setLoggingLevelDebug();
            } else {
            	// If Loss Notice Source is not Innovation (i.e. Web Portal), Get DTOPolicy From AdditionalBeans
            	for( int i = 0; i < additionalBeans.length; i++ ) {
            		if( additionalBeans[i].getBeanName().equals("DTOPolicy") )
            			if(i==1){
            				dtoLatestPolicy = additionalBeans[i];
            			}else{
            				dtoPolicy = additionalBeans[i];
            			}
            	}
            }
            
            //If dtoLatestPolicy is null then set it as with current dtoPolicy 
			if(dtoLatestPolicy == null){
				dtoLatestPolicy = dtoPolicy;
			}
           
			Log.debug("DTOPolicy: " + dtoPolicy.readableDoc());
            
            Log.debug("DTOLatestPolicy: " + dtoLatestPolicy.readableDoc());            
            
            //Set the correct ProductVersionIdRef and ProductLineCd for the claim
            updateCorrectClaimProduct(bean, dtoPolicy);
             
			// Save the old ClaimPolicyInfo
			ModelBean claimPolicyInfo = bean.getBean("ClaimPolicyInfo");
			storeClaimPolicyInfo(claimPolicyInfo);
			            
			// Load the Policy Information
			loadPolicyInfo(dtoPolicy, bean, dtoLatestPolicy, errorList);
			
			// Restore the ClaimPolicyInfo id's
			restoreClaimPolicyInfoIds(claimPolicyInfo);
			
			for( ModelBean error : errorList ) {
				errors.addValue(error);
			}			
			
			Log.debug("Claim: " + bean.readableDoc());
		}
		catch(Exception e){	
			throw new RuleException(e);
		} finally {
			// Set Log Level back after processing the FrontController request
			Log.setLoggingLevelDefault();
		}
		return errors;		
	}
    
    /** Load Policy Information from DTOPolicy into ClaimPolicyInfo
	 * @param dtoPolicy ModelBean - Snapshot as of Loss Date
	 * @param claim ModelBean
	 * @param dtoLatestPolicy ModelBean
	 * @param errors Errors of the service
	 * @throws Exception in an Unexpected Error Occurs
	 */
	public void loadPolicyInfo(ModelBean dtoPolicy, ModelBean claim, ModelBean dtoLatestPolicy, ArrayList<ModelBean> errorList) 
	throws Exception{
		try {
			
			// Load Policy Info 
    		ModelBean dtoBasicPolicy = dtoPolicy.getBean("DTOBasicPolicy");
			ModelBean claimPolicyInfo = claim.getBean("ClaimPolicyInfo");
			
			claimPolicyInfo.setValue("PolicyRef",dtoPolicy.getSystemId());
			claimPolicyInfo.setValue("PolicyNumber",dtoBasicPolicy.gets("PolicyNumber"));
			claimPolicyInfo.setValue("PolicyVersion",dtoBasicPolicy.gets("PolicyVersion"));
			claimPolicyInfo.setValue("InceptionDt",dtoBasicPolicy.gets("InceptionDt"));
			claimPolicyInfo.setValue("InceptionTm",dtoBasicPolicy.gets("InceptionTm"));
			claimPolicyInfo.setValue("ExpirationDt",dtoBasicPolicy.gets("LastTermExpirationDt"));
			claimPolicyInfo.setValue("CancelDt",dtoBasicPolicy.gets("CancelDt"));
			claimPolicyInfo.setValue("TermBeginDt",dtoBasicPolicy.gets("EffectiveDt"));
			claimPolicyInfo.setValue("TermEndDt",dtoBasicPolicy.gets("ExpirationDt"));
			claimPolicyInfo.setValue("ProviderRef",dtoBasicPolicy.gets("ProviderRef"));
			claimPolicyInfo.setValue("PolicyDescription",dtoBasicPolicy.gets("Description"));
			claimPolicyInfo.setValue("CarrierCd",dtoBasicPolicy.gets("CarrierCd"));
			claimPolicyInfo.setValue("CarrierName",dtoBasicPolicy.gets("CarrierName"));
			claimPolicyInfo.setValue("CarrierGroupCd",dtoBasicPolicy.gets("CarrierGroupCd"));
			claimPolicyInfo.setValue("ControllingStateCd",dtoBasicPolicy.gets("ControllingStateCd"));
			claimPolicyInfo.setValue("PolicyProductVersionIdRef",dtoBasicPolicy.gets("ProductVersionIdRef"));
			claimPolicyInfo.setValue("PolicyProductName",dtoBasicPolicy.gets("ProductName"));
			claimPolicyInfo.setValue("PolicyTypeCd",dtoBasicPolicy.gets("PolicyTypeCd"));
			claimPolicyInfo.setValue("PolicyGroupCd",dtoBasicPolicy.gets("PolicyGroupCd"));
			claimPolicyInfo.setValue("AsOfDt", claim.gets("LossDt"));
			claimPolicyInfo.setValue("ShellPolicyInd", dtoBasicPolicy.gets("ShellPolicyInd"));

			
			// Load Insured Party 
			loadInsured(dtoLatestPolicy, claimPolicyInfo);

			// Clear the Claim Policy Information records
			clearClaimPolicyInfo(claimPolicyInfo);
			
			// Load Risks
			loadRisks(dtoPolicy, claimPolicyInfo);
			
			// Create the empty risk if setup requires it
			createEmptyRisk(claim);
			
			// Load Forms
			loadForms(dtoPolicy, claimPolicyInfo);
			
			// Load Additional Interests
			loadAIs(dtoPolicy, claimPolicyInfo, dtoLatestPolicy);
						
			// Load the reinsurance information
			ClaimPolicyReinsuranceBuilder.loadPolicyReinsurance(dtoPolicy, claim, claimPolicyInfo, errorList);
			
			// Load the ACH Information
			loadPaymentInfo(dtoPolicy, claimPolicyInfo);
			
		}
		catch(Exception e){
			throw new Exception(e);
		}
	}
	
	/** Load Risk Information from DTOPolicy into ClaimPolicyInfo
	 * @param dtoPolicy ModelBean - Snapshot as of Loss Date
	 * @param claimPolicyInfo ModelBean
	 * @throws Exception in an Unexpected Error Occurs
	 */
	public static void loadRisks(ModelBean dtoPolicy, ModelBean claimPolicyInfo) 
	throws Exception{
		try{
			ModelBean policyRisks = new ModelBean("PolicyRisks");
			
			// Get All Active Risks From DTOPolicy
			ModelBean[] riskArray = dtoPolicy.findBeansByFieldValue("DTORisk","Status","Active");
			for( ModelBean risk : riskArray ) {
				
				// Create a new PolicyRisk bean
				ModelBean policyRisk = new ModelBean("PolicyRisk");
				
				policyRisk.setValue("TypeCd",risk.gets("TypeCd"));
				policyRisk.setValue("Description",risk.gets("Description"));
				policyRisk.setValue("RiskIdRef", risk.getId());
				
				// If there is a location then add the location number
				String locationRef = risk.gets("LocationRef");
				if( !locationRef.equals("") ) {
					ModelBean dtoLocation = dtoPolicy.getBeanById(locationRef);
					if( dtoLocation != null ) {
						policyRisk.setValue("LocationNumber", dtoLocation.gets("LocationNumber"));
					}
				}
				
				// If there is building information, then attach
				ModelBean building = risk.getBean("DTOBuilding");
				if( building != null ) {
					policyRisk.setValue("RiskNumber",building.gets("BldgNumber"));
					
					// If address information exists, then add				
					ModelBean addr = building.getBean("Addr", "AddrTypeCd", "RiskAddr");
					if (addr != null)
						policyRisk.addValue(addr.cloneBean());
				}
				
				// Add the line code
				ModelBean line = risk.getParentBean();
				policyRisk.setValue("LineCd", line.gets("LineCd"));
				policyRisk.setValue("VehicleInd", "No");				
				
				// Add the PolicyRisk bean to the PolicyRisks array
				policyRisks.addValue(policyRisk);
			}
			 
			claimPolicyInfo.setValue(policyRisks);
		}
		catch(Exception e){
			throw new Exception(e);
		}
	}
	
	/** Load Forms from DTOPolicy into ClaimPolicyInfo
	 * @param dtoPolicy ModelBean - Snapshot as of Loss Date
	 * @param claimPolicyInfo ModelBean
	 * @throws Exception in an Unexpected Error Occurs
	 */
	public static void loadForms(ModelBean dtoPolicy, ModelBean claimPolicyInfo) 
	throws Exception{
		try{
			ModelBean policyForms = new ModelBean("PolicyForms");
			
			// Get All Attached Forms From DTOPolicy
			ModelBean[] form = dtoPolicy.findBeansByFieldValue("DTOForm","Status","Attached");
			for( int i = 0; i < form.length; i++ ) {
				ModelBean policyForm = new ModelBean("PolicyForm");
				policyForm.setValue("FormNum", form[i].gets("FormNum"));
				policyForm.setValue("Name", form[i].gets("Name"));
				policyForm.setValue("FormEdition", form[i].gets("FormEdition"));
				
				policyForms.addValue(policyForm);
			}
			
			claimPolicyInfo.setValue(policyForms);                                                			
		}
		catch(Exception e){
			throw new Exception(e);
		}
	}
	
	/** Load Addtional Interests from DTOPolicy into ClaimPolicyInfo
	 * @param dtoPolicy ModelBean - Snapshot as of Loss Date
	 * @param claimPolicyInfo ModelBean
	 * @throws Exception in an Unexpected Error Occurs
	 */
	public static void loadAIs(ModelBean dtoPolicy, ModelBean claimPolicyInfo, ModelBean dtoLatestPolicy) 
	throws Exception{
		try{
			ModelBean policyAIs = new ModelBean("PolicyAIs");

			// Get All Active AIs From DTOPolicy
			ModelBean[] ai = dtoPolicy.findBeansByFieldValue("DTOAI","Status","Active");
			ModelBean[] aiInLatestPolicy = dtoLatestPolicy.findBeansByFieldValue("DTOAI","Status","Active");
			for( int i = 0; i < ai.length; i++ ) {
				// Create New PolicyAI
				ModelBean policyAI = new ModelBean("PolicyAI");

				if(!ai[i].gets("InterestTypeCd").contains("Mortgagee")){
					
					//This is a common method in policyInfoBuilderBase which loads AI from either policy as of loss dt or from latest policy
					policyAI = loadAIsFromPolicy(ai[i], claimPolicyInfo, policyAIs, dtoPolicy);
					policyAIs.addValue(policyAI);
				}
			}
				for(int j=0; j < aiInLatestPolicy.length; j++){
					// Create New PolicyAI
					ModelBean policyAI = new ModelBean("PolicyAI");
					if(aiInLatestPolicy[j].gets("InterestTypeCd").contains("Mortgagee")){

						//This is a common method in policyInfoBuilderBase which loads AI from either policy as of loss dt or from latest policy
						policyAI = loadAIsFromPolicy(aiInLatestPolicy[j], claimPolicyInfo, policyAIs, dtoLatestPolicy);
						policyAIs.addValue(policyAI);
					}
				}
			claimPolicyInfo.setValue(policyAIs);                                                			
		}
		catch(Exception e){
			throw new Exception(e);
		}
	}

	/** Create Empty Risk(s) for the Claim Policy Info if Claim Setup Requires One
	 * @param claim The Claim ModelBean
	 * @throws Exception if an Unexpected Error Occurs
	 */
	private static void createEmptyRisk(ModelBean claim)
	throws Exception {
		
		// Get Empty Policy Risk Options
        ModelBean coderef = ProductSetup.getProductCoderef(claim.gets("ProductVersionIdRef"), "CLClaim::claim::risk-other::all");
        ModelBean claimPolicyInfo = claim.getBean("ClaimPolicyInfo");
        ModelBean policyRisks = claimPolicyInfo.getBean("PolicyRisks");
        if( policyRisks == null ) {
        	
        	// Add PolicyRisks
        	policyRisks = new ModelBean("PolicyRisks");
        	claimPolicyInfo.setValue(policyRisks);
        }
        
        // Loop Through Empty Policy Risks 
        ModelBean[] emptyOptionArray = coderef.getAllBeans("option");        
		for( ModelBean emptyOption : emptyOptionArray ) {
			
			// Check if Risk Already Exists
			String optionValue = emptyOption.gets("value");
			ModelBean policyRisk = policyRisks.getBean("PolicyRisk", "TypeCd", optionValue);
			if( policyRisk == null ) {
				
				// Add PolicyRisk
				ModelBean risk = new ModelBean("PolicyRisk");
				risk.setValue("TypeCd", optionValue);
				risk.setValue("Description", emptyOption.gets("name"));
				risk.setValue("VehicleInd", "No");				
				risk.setValue("LineCd", "Dwelling");
				policyRisks.addValue(risk);
			}
		}
	}
	
	/** Create Empty Insured PartyInfo ModelBean
	 * @param claim The Claim ModelBean
	 * @throws Exception
	 */
	private static void createEmptyInsuredPartyInfo(ModelBean claim)
	throws Exception {
        ModelBean claimPolicyInfo = claim.getBean("ClaimPolicyInfo");
        
		ModelBean insuredParty = claimPolicyInfo.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "InsuredParty");
		if (insuredParty == null) {
	        ModelBean partyInfo = new ModelBean("PartyInfo");
	        partyInfo.setValue("PartyTypeCd","InsuredParty");
	        claimPolicyInfo.setValue(partyInfo);
	        
	        ModelBean nameInfo = new ModelBean("NameInfo");
	        nameInfo.setValue("NameTypeCd", "InsuredName");
	        partyInfo.setValue(nameInfo);
	        
	        ModelBean addr = new ModelBean("Addr");
	        addr.setValue("AddrTypeCd", "InsuredMailingAddr");
	        partyInfo.setValue(addr);
		}        
	}
}