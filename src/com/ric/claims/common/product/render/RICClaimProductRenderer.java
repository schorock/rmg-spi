package com.ric.claims.common.product.render;

import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.google.gson.Gson;
import com.iscs.claims.common.product.render.ClaimProductRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.insurance.product.ProductSetup;

import net.inov.tec.beans.ModelBean;

public class RICClaimProductRenderer extends ClaimProductRenderer {

	public RICClaimProductRenderer() {
		super();
	}

	public String getContextVariableName() {
		return getClass().getSimpleName();
	}
	
	/**
	 * Get the corresponding Linked LossCd
	 * @param productVersionIdRef
	 * @param lossCd
	 * @return
	 */
	public String getLinkedLossCdCodeRef( String productVersionIdRef, String lossCauseCd ){
		try{
			try{ 
				ModelBean lossCauseCds = ProductSetup.getProductCoderef( productVersionIdRef, "CLClaim::claim::loss-causes::all" );

				if( lossCauseCds != null ){
					for( ModelBean option : lossCauseCds.getBean("Options").getBeans("Option") ){
						if( option.gets("name").equals( lossCauseCd ) )
							return option.gets("value");
					}
				}

			} catch( NullPointerException nullPointerException ) {
				Log.debug("Loss Causes UMOL does not exist.");
			}

			return null;

		} catch (Exception e) {
			Log.debug("No coderef was found" );
			return null;
		}
	}
	
	
	/**
	 * Get Value from linked lossCd
	 * @param productVersionIdRef
	 * @param linkedDamages
	 * @return
	 */
	public List<String> getValueFromLinkedCdFromCodeRef( String productVersionIdRef, String linkedDamages ) {
		try{
			try{ 
				if(linkedDamages == null || linkedDamages.isEmpty() )
					return new ArrayList<>();
				
				ModelBean linkedDamageCds = ProductSetup.getProductCoderef( productVersionIdRef, "CLClaim::claim::loss-causes::" + linkedDamages );
				List<String> linkedDamageList = new ArrayList<>();

				if( linkedDamageCds != null ){
					for( ModelBean option : linkedDamageCds.getBean("Options").getBeans("Option") ){
						linkedDamageList.add(option.gets("value"));
					}
				}
				
				return linkedDamageList;
			} catch( NullPointerException nullPointerException ) {
				Log.debug("Loss Causes UMOL does not exist.");
			}

			return null;

		} catch (Exception e) {
			Log.debug("No coderef was found" );
			return null;
		}
	}
	
	/**
	 * Return a linked damages list from list.xml
	 * @param productVersionIdRef
	 * @param lossCauseCd
	 * @param additionalLossNoticeCd1
	 * @param additionalLossNoticeCd2
	 * @param additionalLossNoticeCd3
	 * @param additionalLossNoticeCd4
	 * @return JSON string of linked damages.
	 */
	public String getJSONLinkedDamagesList( String productVersionIdRef, String lossCauseCd, String additionalLossNoticeCd1, String additionalLossNoticeCd2, String additionalLossNoticeCd3, String additionalLossNoticeCd4 ) {
		Set<String> linkedDamages = new HashSet<>();
		
		linkedDamages.addAll( getValueFromLinkedCdFromCodeRef(productVersionIdRef, getLinkedLossCdCodeRef(productVersionIdRef, lossCauseCd)) );
		linkedDamages.addAll( getValueFromLinkedCdFromCodeRef(productVersionIdRef, getLinkedLossCdCodeRef(productVersionIdRef, additionalLossNoticeCd1)) );
		linkedDamages.addAll( getValueFromLinkedCdFromCodeRef(productVersionIdRef, getLinkedLossCdCodeRef(productVersionIdRef, additionalLossNoticeCd2)) );
		linkedDamages.addAll( getValueFromLinkedCdFromCodeRef(productVersionIdRef, getLinkedLossCdCodeRef(productVersionIdRef, additionalLossNoticeCd3)) );
		linkedDamages.addAll( getValueFromLinkedCdFromCodeRef(productVersionIdRef, getLinkedLossCdCodeRef(productVersionIdRef, additionalLossNoticeCd4)) );
		
		return new Gson().toJson(linkedDamages);
	}
	
	public boolean isSeverityLvl1GreaterThanSeverityLvl2( String severityLvl1, String severityLvl2 ){
		String[] severityLevels = {"","P5","P4","P3","C2","C1","PL","P2","P1","S1"};
		Integer severityLvl1Value = 0, severityLvl2Value = 0;
		
		for( int j = 0; j < severityLevels.length; j++ ){
			if( severityLvl1.equals( severityLevels[j] ) )
				severityLvl1Value = j;
			
			if( severityLvl2.equals( severityLevels[j] ) )
				severityLvl2Value = j;
		}
		
		return severityLvl1Value > severityLvl2Value;
	}
}
