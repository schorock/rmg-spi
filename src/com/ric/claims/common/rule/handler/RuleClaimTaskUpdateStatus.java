package com.ric.claims.common.rule.handler;

import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.bean.render.BeanRenderer;
import com.iscs.workflow.Task;
import com.iscs.workflow.render.WorkflowRenderer;

import net.inov.biz.server.ServiceContext;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;

public class RuleClaimTaskUpdateStatus implements RuleProcessor {
	

    /**
     * 
     */
    public RuleClaimTaskUpdateStatus() { }

	/**
	 * @return
	 */
	public String getContextVariableName() {
		return "RuleClaimTaskUpdateStatus";
	}
	

	@Override
	public ModelBean process(ModelBean ruleTemplate, ModelBean task, ModelBean[] additionalBeans, ModelBean user, JDBCData data) throws RuleException {
		try {
			ModelBean taskLink = task.getBean("TaskLink", "ModelName", "Claim");
			
			ModelBean rq = ServiceContext.getServiceContext().getRequest();
			
			if( ( rq.getBeanName().equals("WFTaskCompleteRq") || rq.getBeanName().equals("WFTaskDeleteRq") ) && taskLink != null ) {
				ModelBean cmmParams = rq.getBean("CMMParams", "ModelName", "Task");
				ModelBean claim = BeanRenderer.selectModelBean(data,"Claim", taskLink.gets("IdRef"));
				
				if( cmmParams != null && claim != null ) {
					ModelBean parentTask = Task.getTask(data, cmmParams.gets("SystemId"));
					
					if( parentTask != null && task.gets("TemplateId").contains(parentTask.gets("TemplateId")) ) {
						if( rq.getBeanName().equals("WFTaskCompleteRq") )
							WorkflowRenderer.completeTask(data, task);
						if( rq.getBeanName().equals("WFTaskDeleteRq") )
							WorkflowRenderer.deleteTask(data, task);
					}
				}
			}
	        
	        return new ModelBean("UWRuleRs");
		}
		catch (Exception e) {
			Log.error(e.getMessage(), e);
            throw new RuleException(e);
		}
	}
}

