package com.ric.claims.common.stat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import com.iscs.claims.claim.Claim;
import com.iscs.claims.claim.ClaimAsOf;
import com.iscs.claims.claim.Claimant;
import com.iscs.claims.common.Feature;
import com.iscs.claims.common.Reserve;
import com.iscs.claims.common.catastrophe.Catastrophe;
import com.iscs.claims.common.stat.ClaimStats;
import com.iscs.claims.transaction.PropertyDamaged;
import com.iscs.claims.transaction.Transaction;
import com.iscs.claims.transaction.render.ClaimantTransactionRenderer;
import com.iscs.common.business.provider.Provider;
import com.iscs.common.business.stat.StatBase;
import com.iscs.common.business.stat.StatException;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.tech.bean.data.SQLRelationHelper;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.codegenerator.CodeBase;
import com.iscs.insurance.product.ProductMaster;
import com.iscs.insurance.product.ProductVersion;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.math.Money;

public class RICClaimStats extends StatBase {

	private static int MAX_ROWS = 0;
	private String[] AMOUNT_FIELDS = {"ReserveChangeAmt", "PaidAmt", "ExpectedRecoveryChangeAmt", "PostedRecoveryAmt", "HistoricReserveAmt", "HistoricExpectedRecoveryAmt", "HistoricPaidAmt", "HistoricPostedRecoveryAmt"};	
	private static String PACKAGE = "Claims";
	private static int COMMIT_NO = 1;
	private ModelBean curBean = null;  // The current Claim 
	private ModelBean prevBean = null;  // The previous Claim transaction
	private static String KEY_DELIMETER = "|";
	public static String STATUS_DELIMETER = "-";
	private static String[] REVERSE_FIELDS = {"LossDt", "LossCauseCd", "CatastropheRef", "SubLossCauseCd"}; // Base core fields that can do a complete reverse, (Not part of of the StatAttributes) 
	
	// A copy of the last ClaimAsOf used. This allows getPreviousClaim() to use the ClaimAsOf created by regenerateStatItem() without passing it through the StatBase superclass
	private ClaimAsOf cachedClaimAsOf = null;

	/** Initialize the RICClaimStats
	 * 
	 * @throws StatException when an error occurs
	 */
	public RICClaimStats() throws StatException {
	}

	/** Reset the current state of PolicyStats
	 * 
	 * @throws Exception when an error occurs
	 */
	protected void resetState() { 
	}
	
	/** Obtain the list of stat models in the setup to be processed for this claim
	 * 
	 * @param claim The Claim ModelBean
	 * @return The ModelBean array list of stat models
	 * @throws StatException when an error occurs
	 */
	public ModelBean[] getProductStats(ModelBean claim)
	throws StatException {
		try {
			ModelBean product = Claim.getClaimSetup(claim);
			ModelBean prodStats = product.getBean("ProductStats");
			ArrayList<ModelBean> list = new ArrayList<ModelBean>();
			if (prodStats != null) {
				ModelBean[] stats = prodStats.getBeans("ProductStat");
				for (int i=0; i<stats.length; i++) {
					if (stats[i].gets("TypeCd").equals("") || stats[i].gets("TypeCd").equals("Stat")) 
						list.add(stats[i]);				
				}				
			}
			return (ModelBean[]) list.toArray(new ModelBean[list.size()]); 
			
		} catch (Exception e) {
			throw new StatException("Unable to obtain the Product Stat definitions", e);
		}
	}

	/** Find the current stat beans that will be affected by the current stat record.  
	 * 
	 * @param data The Data repository connection
	 * @param productStat The Product Setup for statistics
	 * @param stat The current Stat record
	 * @return An array of ModelBeans containing the stat records affected by the current record
	 * @throws StatException when an error occurs
	 * 
	 */
	public ModelBean[] findStatBeans(JDBCData data, ModelBean productStat, ModelBean stat, ModelBean container) 
	throws StatException {
		try {

			delegate = SQLRelationHelper.getDelegate(data);
			SQLRelationHelper sqlHelp = new SQLRelationHelper(data, delegate);
			
			ModelBean[] results = sqlHelp.selectModelBean(stat.getBeanName(), new String[] {"CombinedKey", "StatusCd"}, new String[] {"=", "=" }, new String[] {stat.gets("CombinedKey"), "Active"}, MAX_ROWS);
	
			ArrayList<ModelBean> found = new ArrayList<ModelBean>();
			
			if (results.length == 0)
				setFirstRecord(true);
			else
				setFirstRecord(false);
			
			for (int i=0; i<results.length; i++) {
				ModelBean result = results[i];
				String sysId = result.getSystemId();
				
				// Ignore the new stat bean just added
				if (!stat.getSystemId().equals("") && sysId.equals(stat.getSystemId()))
					continue;
				
				boolean diff = false;
				// Did the amount values change?
				String[] amounts = getAmountFields();				
				for (int j=0; j<amounts.length; j++) {
					if (StringTools.notEqual(stat.gets(amounts[j]), result.gets(amounts[j])) 
							&& StringTools.notEqual(stat.gets(amounts[j], "0.00"), "0.00")) { 
							diff = true;
							break;
					}					
				}
				
				// Now determine the Product defined statistical significant fields comparisons
				if (!diff && !compareStatKeys(productStat, stat, result)) 
					diff = true;
				
				if (diff) {
					found.add(result);
				}
			}
			
			// Record differences on similar items
			String dbTableName = SQLRelationHelper.getDelegate(data).tableName(stat.getBeanName());
			String sql = "SELECT * FROM " + dbTableName + " WHERE SUMMARYKEY = '" + stat.gets("SummaryKey") + "' AND CLAIMANTTRANSACTIONNUMBER < " + stat.gets("ClaimantTransactionNumber") + " ORDER BY CLAIMANTTRANSACTIONNUMBER DESC";
			ModelBean[] lastTrans = sqlHelp.doQuery(stat.getBeanName(), sql, 1);
			stat.setValue("ClaimStatusChgInd", false);
			stat.setValue("FeatureStatusChgInd", false);
			stat.setValue("ReserveStatusChgInd", false);
			stat.setValue("ClaimantStatusChgInd", false);
			if (lastTrans.length > 0) {
				String lastClaimStatusCd = lastTrans[0].gets("ClaimStatusCd");
				if (!stat.gets("ClaimStatusCd").equals(lastClaimStatusCd))
					stat.setValue("ClaimStatusChgInd", true);					
				String lastFeatureStatusCd = lastTrans[0].gets("FeatureStatusCd");
				String[] lastFeatureCodes = lastFeatureStatusCd.split(STATUS_DELIMETER);
				String[] featureCodes = stat.gets("FeatureStatusCd").split(STATUS_DELIMETER);
				if (!featureCodes[0].equals(lastFeatureCodes[0]))
					stat.setValue("FeatureStatusChgInd", true);					
				String lastReserveStatusCd = lastTrans[0].gets("ReserveStatusCd");
				if (!stat.gets("ReserveStatusCd").equals(lastReserveStatusCd))
					stat.setValue("ReserveStatusChgInd", true);					
				String lastClaimantStatusCd = lastTrans[0].gets("ClaimantStatusCd");
				if (!stat.gets("ClaimantStatusCd").equals(lastClaimantStatusCd))
					stat.setValue("ClaimantStatusChgInd", true);					
			} else {
				// Set for the first record
				stat.setValue("ClaimStatusChgInd", true);
				stat.setValue("FeatureStatusChgInd", true);
				stat.setValue("ReserveStatusChgInd", true);
				stat.setValue("ClaimantStatusChgInd", true);
			}
									
			return (ModelBean[]) found.toArray(new ModelBean[found.size()]);
		} catch (Exception e) {
			throw new StatException(e);
		}		
	}

	/** Obtain the CodeBase Generator for parsing value fields
	 * 
	 * @param claim The Claim ModelBean 
	 * @exception StatException when an error occurs
	 */
	protected CodeBase getFormatGenerator(ModelBean claim)
	throws StatException {
		try {
			ModelBean product = Claim.getClaimSetup(claim);
	        String clazz = product.gets("ClaimNumberGenerator");
	        return (CodeBase) Class.forName(clazz).newInstance();
		} catch (Exception e) {
			throw new StatException("Unable to retrieve the CodeBase Generator", e);
		}
	}

	/** Create the array of stat beans that belong to the current claim transaction.
	 * These include all coverage and coverage items.
	 * 
	 * @param setup The Product setup for stats
	 * @param claim The current claim ModelBean
	 * @throws StatException when an error occurs
	 */
	public ModelBean[] createStatBeans(ModelBean setup, ModelBean claim, JDBCData data)
	throws StatException {
		try {
			
			ArrayList<ModelBean> statList = new ArrayList<ModelBean>();
			HashMap<String, String> deniedList = new HashMap<String, String>();
			ModelBean[] historicStatList = new ModelBean[0];
			ModelBean claimCopy = null;
			
			 // Manufacture all stat records pertaining to history when switching from manual to live mode					
			if( switchToLiveClaimInd(claim) ) {
				if( prevBean !=null){
					historicStatList = buildHistoricStatList(data, setup, claim);														
				}
			}			    
			
			// For Each ClaimantTransaction
			ModelBean[] claimantTransaction = claim.getAllBeans("ClaimantTransaction");
			for( int i = 0; i < claimantTransaction.length; i++ ) {
				
				// Check if New Transaction
				if( prevBean == null || prevBean.getBeanById(claimantTransaction[i].getId()) == null ) {

					// Pre-setup for Deny detection
					boolean hasDenyReverseDeny = false;
					ModelBean[] claimantTrans = claimantTransaction[i].getParentBean().getBeans("ClaimantTransaction");
					for (ModelBean claimantTran : claimantTrans) {
						if( prevBean == null || prevBean.findBeanById("ClaimantTransaction", claimantTran.getId()) == null ) {
							ModelBean featureAllocation = claimantTran.getBean("FeatureAllocation");
							if (featureAllocation != null && (featureAllocation.gets("StatusCd").equals(Feature.ST_DENIED) || featureAllocation.gets("StatusCd").equals(Feature.ST_REVERSE_DENIED))) {
								hasDenyReverseDeny = true;								
							}
						}
					}					
					ModelBean featureAlloc = claimantTransaction[i].getBean("FeatureAllocation");
					ModelBean feature = Feature.find(claimantTransaction[i].getParentBean(), featureAlloc);
					ModelBean claimant = claimantTransaction[i].getParentBean();					
					
					// For Each ReserveAllocation Create a Stat Record
					ModelBean[] reserveAllocation = claimantTransaction[i].getAllBeans("ReserveAllocation");
					for( int j = 0; j < reserveAllocation.length; j++ ) {
						ModelBean stat = null;
						if( historicStatList.length == 0 || hasProcessedHistoryStatRecord(data, setup,claim,reserveAllocation[j], statList) ){
							stat = createStatBean(data, setup, claim, reserveAllocation[j], new ModelBean[0]);
						} else {
							stat = createStatBean(data, setup, claim, reserveAllocation[j], historicStatList);
						}
						statList.add(stat);
						if (hasDenyReverseDeny) {
							String hashKey = createSummaryHashKey(claimant, feature, reserveAllocation[j].gets("ReserveCd"));
							deniedList.put(hashKey, hashKey);
						}
					}
					
					// Create stat records for each current Reserve under a Deny Feature.  If the stat record
					// has been created from a current ReserveAllocation, do not create one.
					if (reserveAllocation.length == 0) {
						if (hasDenyReverseDeny) {
							ModelBean[] featureAllocations = claimantTransaction[i].getBeans("FeatureAllocation");
							if (featureAllocations != null) {
								for (ModelBean featureAllocation : featureAllocations) {
									ModelBean featureBean = Feature.find(claimantTransaction[i].getParentBean(), featureAllocation);
									ModelBean[] reserves = featureBean.getBeans("Reserve");
									Money zeroAmt = new Money("0.00");
									if (claimCopy == null)
										claimCopy = claim.cloneBean();
									ModelBean featureAllocCopy = claimCopy.findBeanById("FeatureAllocation", featureAllocation.getId());
									for (ModelBean reserve : reserves) {
										String hashKey = createSummaryHashKey(claimant, featureBean, reserve.gets("ReserveCd"));
										if (deniedList.get(hashKey) != null) 
											continue; // If record written already, ignore writing new one.
										ModelBean newReserve = Reserve.createAllocation(reserve.gets("ReserveCd"), zeroAmt, zeroAmt, zeroAmt, zeroAmt, reserve.gets("StatusCd"));
										featureAllocCopy.addValue(newReserve);
										ModelBean stat = createStatBean(data, setup, claim, newReserve, historicStatList);
										stat.setValue("StatusCd", "Active"); // Force to Active because no amount values have changed.
										statList.add(stat);								
									}								
								}
							}
						}
					}
				}
			}
			
			// Add any historic stats that are not reflected in the current transaction(s)
			ModelBean[] list = (ModelBean[]) statList.toArray(new ModelBean[statList.size()]);
			for( ModelBean historicStat:historicStatList){
				boolean found = false;
				for( ModelBean stat:list) {
					if( stat.gets("SummaryKey").equals(historicStat.gets("SummaryKey")) ){
						found = true;
						break;
					}					
				}
				if( !found){
					historicStat.setValue("ClaimantTransactionCd","");
					historicStat.setValue("ClaimantTransactionNumber","0");
					historicStat.setValue("ClaimantTransactionIdRef","");
					historicStat.setValue("CombinedKey", historicStat.gets("SummaryKey"));
					if (isAmountDelta(historicStat) ) {
						historicStat.setValue("StatusCd", "Active");
					}
					list = BeanTools.addBeanToBeans(list, historicStat);
				}
			}						
			return list;			
		} catch (Exception e) {
			Log.error(e);
			throw new StatException(e);
		}
	}

	/** Create each individual stat record.  Fills in all of the unique fields related to Claim stats.
	 * 
	 * @param data Data connection
	 * @param setup The Product Stat setup ModelBean
	 * @param claim The claim ModelBean
	 * @param reserveAllocation The reserveAllocation to base this stat record on
	 * @param historicStatList ModelBean List of stats for historic transactions
	 * @return The new Stat ModelBean record 
	 * @throws Exception when an error occurs
	 */
	protected ModelBean createStatBean(JDBCData data, ModelBean setup, ModelBean claim, ModelBean reserveAllocation, ModelBean[] historicStatList) 
	throws Exception {
		try {
			ModelBean stat = new ModelBean(setup.gets("BeanName"));			
			
			// Set the main key fields
			setKeyFields(data, setup, claim, reserveAllocation, stat);						
				        
			// Update the amount fields
			setAmountDelta(stat, reserveAllocation);			
			
			// Clear the historic amount fields before setting them
			stat.setValue("HistoricReserveAmt", "0.00");
			stat.setValue("HistoricExpectedRecoveryAmt", "0.00");
			stat.setValue("HistoricPaidAmt", "0.00");
			stat.setValue("HistoricPostedRecoveryAmt", "0.00");
			
			for( ModelBean bean:historicStatList){
				if( bean.gets("SummaryKey").equals(stat.gets("SummaryKey")) ){
					String historicReserveAmt = bean.gets("HistoricReserveAmt");
					String historicExpectedRecoveryAmt = bean.gets("HistoricExpectedRecoveryAmt");
					String historicPaidAmt = bean.gets("HistoricPaidAmt");
					String historicPostedRecoveryAmt = bean.gets("HistoricPostedRecoveryAmt");
					stat.setValue("HistoricReserveAmt", StringTools.addMoney(historicReserveAmt, stat.gets("HistoricReserveAmt")));    	
					stat.setValue("HistoricExpectedRecoveryAmt", StringTools.addMoney(historicExpectedRecoveryAmt, stat.gets("HistoricExpectedRecoveryAmt")));    	
					stat.setValue("HistoricPaidAmt", StringTools.addMoney(historicPaidAmt, stat.gets("HistoricPaidAmt")));    	
					stat.setValue("HistoricPostedRecoveryAmt", StringTools.addMoney(historicPostedRecoveryAmt, stat.gets("HistoricPostedRecoveryAmt")));    	
					break;
				}
			}
			
        	// Determine if the amounts are non-zero indicating a change
        	if (isAmountDelta(stat) || reserveAllocation.gets("StatusCd").equals("Closed"))
        		stat.setValue("StatusCd", "Active");
        	else 
        		stat.setValue("StatusCd", "InActive");
        	
			// Update the policy information fields
			ModelBean claimPolicyInfo = claim.getBean("ClaimPolicyInfo");
			stat.setValue("PolicyRef", claimPolicyInfo.gets("PolicyRef"));
			stat.setValue("PolicyNumber", claimPolicyInfo.gets("PolicyNumber"));
			stat.setValue("PolicyVersion", StringTools.leftPad(claimPolicyInfo.gets("PolicyVersion"), "0", 3));
			stat.setValue("CarrierCd", claimPolicyInfo.gets("CarrierCd"));
			stat.setValue("CarrierGroupCd", claimPolicyInfo.gets("CarrierGroupCd"));
			stat.setValue("StateCd", claimPolicyInfo.gets("ControllingStateCd"));
						
			// Build the RiskCd 
	        ModelBean featureAllocation = BeanTools.getParentBean(reserveAllocation, "FeatureAllocation");
	        setRiskInformation(claim, featureAllocation, stat);
			
			StringDate effDt = claimPolicyInfo.getDate("TermBeginDt");
			stat.setValue("EffectiveDt", effDt);
			stat.setValue("ExpirationDt", claimPolicyInfo.getValue("TermEndDt"));
	        stat.setValue("PolicyYear", Integer.toString(effDt.getCalendar().get(Calendar.YEAR)));
	        if( stat.hasBeanField("CoverageTriggerCd") )
	        	stat.setValue("CoverageTriggerCd", claimPolicyInfo.gets("CoverageTriggerCd"));
	        
	        // Get Feature Allocation
	    	String featureItemNumber = featureAllocation.gets("ItemNumber", null);
	    	String featureSubCd = featureAllocation.gets("FeatureSubCd", null);
	    		    	
	    	ModelBean claimPolicyLimit = Feature.getClaimPolicyLimit(claim, featureAllocation);
	        if( claimPolicyLimit != null ) {
	        	stat.setValue("CoverageCd", claimPolicyLimit.gets("OriginalCoverageCd"));
	        	stat.setValue("RateAreaName",claimPolicyLimit.gets("RateAreaName"));
	        	stat.setValue("StatData",claimPolicyLimit.gets("StatData"));
	        	stat.setValue("LineCd", claimPolicyLimit.gets("LineCd"));  // If the feature can be mapped, then set the LineCd here
	        
		        ModelBean claimPolicySubLimit = claimPolicyLimit.getBean("ClaimPolicySubLimit", "Category", featureSubCd);
		        if( claimPolicySubLimit != null ) {
		        	stat.setValue("CoverageItemCd", claimPolicySubLimit.gets("OriginalCoverageSubCd"));
		        	stat.setValue("RateAreaName",claimPolicySubLimit.gets("RateAreaName"));
		        	stat.setValue("StatData",claimPolicySubLimit.gets("StatData"));
		        }	
		        
		        // Set Limits and Deductibles
		        String policyLimit = "";
				String limit = "";
				String limitDescription = "";
				String policyDeductible = "";
				String deductible = "";
				String deductibleDescription = "";
				if( featureItemNumber != null ) {
					claimPolicySubLimit = claimPolicyLimit.getBean("ClaimPolicySubLimit", "ItemNumber", featureItemNumber);
					policyLimit = claimPolicySubLimit.gets("Limit"); 
					limit = Feature.getCoverageLimit(claim, claimPolicySubLimit);
					limitDescription = claimPolicySubLimit.gets("LimitDescription");
					policyDeductible = claimPolicySubLimit.gets("Deductible");
					deductible = Feature.getCoverageDeductible(claim, claimPolicySubLimit);
					deductibleDescription = claimPolicySubLimit.gets("DeductibleDescription");
				} else if( featureSubCd != null ) {
					ModelBean[] claimPolicySubLimits = claimPolicyLimit.findBeansByFieldValue("ClaimPolicySubLimit", "Category", featureSubCd);
					limit = "0";
					deductible = "";
					boolean numericPolicyLimitValueSet = false;
					boolean numericLimitValueSet = false;
					for ( int i = 0; i < claimPolicySubLimits.length; i++ ) {
						String policySubLimit = claimPolicySubLimits[i].gets("Limit");
						if( StringTools.isCurrency(policySubLimit, true) ){
							if( !StringTools.isCurrency(policyLimit, true) ){
								policyLimit = "0";
							}							
							policyLimit = StringTools.addMoney(policyLimit, policySubLimit);
							numericPolicyLimitValueSet = true;
						} else if ( !numericPolicyLimitValueSet ){
							policyLimit = policySubLimit;
							
						}
						String subLimit = Feature.getCoverageLimit(claim, claimPolicySubLimits[i]);
						if ( StringTools.isCurrency(subLimit, true) ){
							if ( !StringTools.isCurrency(limit, true) ){
								limit = "0";
							}							
							limit = StringTools.addMoney(limit, subLimit);	
							numericLimitValueSet = true;
						} else if ( !numericLimitValueSet ){
							limit = subLimit;
						}
						
						//RMG-1219 Only set deductible to an amount when subfeature deductible has a value >= 0
						String subDeductible = Feature.getCoverageDeductible(claim, claimPolicySubLimits[i]);
						if( StringRenderer.greaterThanEqual(subDeductible, "0") )
							deductible = Feature.getCoverageDeductible(claim, claimPolicyLimit);
					}
					
					policyLimit = StringTools.stripNumberFormatChars(policyLimit);
					limit = StringTools.stripNumberFormatChars(limit);
					limitDescription = claimPolicyLimit.gets("LimitDescription");
					policyDeductible = claimPolicyLimit.gets("Deductible");
					deductible = StringTools.stripNumberFormatChars(deductible);
					deductibleDescription = claimPolicyLimit.gets("DeductibleDescription");
				} else {
					policyLimit = claimPolicyLimit.gets("Limit"); 
					limit = Feature.getCoverageLimit(claim, claimPolicyLimit);	
					limitDescription = claimPolicyLimit.gets("LimitDescription");
					policyDeductible = claimPolicyLimit.gets("Deductible");
					deductible = Feature.getCoverageDeductible(claim, claimPolicyLimit);	
					deductibleDescription = claimPolicyLimit.gets("DeductibleDescription");
				}
				stat.setValue("PolicyLimit", policyLimit);
		        stat.setValue("Limit", limit);
		        stat.setValue("LimitDescription", limitDescription);
		        
		        stat.setValue("PolicyDeductible", policyDeductible);
				stat.setValue("Deductible", deductible);
				stat.setValue("DeductibleDescription", deductibleDescription);
				
				stat.setValue("AggregateLimit", Feature.getCoverageAggregateLimit(claim, claimPolicyLimit));
				stat.setValue("AggregateLimitDescription", claimPolicyLimit.gets("AggregateLimitDescription"));
	        }
	     
	        // Update claim information dates
			String transNo = claim.gets("TransactionNumber");
			ModelBean trans = claim.findBeanByFieldValue("ClaimTransactionInfo", "TransactionNumber", transNo);			
			stat.setValue("BookDt", trans.getValue("BookDt"));
			StringDate transDt = trans.getDate("TransactionDt");
			stat.setValue("StartDt", transDt);
			stat.setValue("EndDt", transDt);			
	        stat.setValue("LossYear", Integer.toString(claim.getDate("LossDt").getCalendar().get(Calendar.YEAR)));
	        stat.setValue("LossDt", claim.getValue("LossDt"));
	        stat.setValue("ReportDt", claim.getValue("ReportedDt"));
	        if( claimPolicyInfo.valueEquals("CoverageTriggerCd", "Claims Made") ) {
	        	if( stat.hasBeanField("AwarenessDt") )
	        		stat.setValue("AwarenessDt", claim.getValue("AwarenessDt"));
	        }	
				        
	        // TODO:  InsuranceTypeCd is hardcoded as Direct, until Reinsurance is added which can change this to Ceded
	        stat.setValue("InsuranceTypeCd", "Direct");
	        
	        stat.setValue("CatastropheRef", claim.gets("CatastropheRef"));
	        stat.setValue("CatastropheNumber", Catastrophe.getCatastropheNumber(data, claim.gets("CatastropheRef")));
	         
	        //Fill the statistically significant fields in the setup
			fillStatBean(claim, setup, stat, reserveAllocation, data);					
	        
			return stat;
		} catch (Exception e) {
			Log.error(e);
			e.printStackTrace();
			throw e;
		}
	}
	
	/** Statistically significant fields that do not alter the original written premium needs to have 
	 * the correct premium from the previous change.  This updates the current stat record with the 
	 * values of the old record pro-rated to the current dates
	 *  
	 * @param previous The previous stat record
	 * @param current The current stat record
	 * @return true/false if the amounts where updated
	 * @throws Exception when an error occurs
	 */
	public boolean updateAmounts(ModelBean previous, ModelBean current) 
	throws Exception {
		try {
			// Determine if previous had non-zero premium amounts and now current has zero.
			// Means the user defined significant fields triggered the affected records
			// So transfer the premium to the current

	        boolean update = false;
	        String[] amounts = getAmountFields();
	        
	        for (int i=0; i<amounts.length; i++) {
	        	update = updateAmount(previous, current, amounts[i]);
	        	if (update) 
	        		break;
	        }
	        
	        return update;
	        
		} catch (Exception e) {
			throw e;
		}
	}
	
	/** Statistically significant fields that do not alter the original written premium needs to have 
	 * the correct premium from the previous change.  This updates the current stat record with the 
	 * value of the old record 
	 *  
	 * @param previous The previous stat record
	 * @param current The current stat record
	 * @param fieldName The value field name in the previous and current to update
	 * @return true/false if the amounts where updated
	 * @throws Exception when an error occurs
	 */
	public boolean updateAmount(ModelBean previous, ModelBean current, String fieldName)
	throws Exception {
		Money zero = new Money("0.00");
		Money prevValue = new Money(previous.gets(fieldName));
        Money curValue = new Money(current.gets(fieldName));

        boolean update = false;
        
        if (!prevValue.equals(zero) && curValue.equals(zero)) {
        	current.setValue(fieldName, prevValue.toString());
        	update = true;
        }
        return update;
	}
	
	/** Set the key fields in the stat record.  These key fields are what defines the stat record for 
	 * this system.
	 * 
	 * @param data Data connection
	 * @param setup The setup stat ModelBean
	 * @param claim The claim ModelBean
	 * @param reserveAllocation The reserve allocation in question to set up the key fields for
	 * @param stat The current stat record
	 * @throws Exception when an error occurs
	 */
	private void setKeyFields(JDBCData data, ModelBean setup, ModelBean claim, ModelBean reserveAllocation, ModelBean stat)
	throws Exception {
		
		// Set ReserveAllocation Fields
		stat.setValue("ReserveCd", reserveAllocation.gets("ReserveCd"));
		String reserveType = Reserve.getReserveType(claim, reserveAllocation.gets("ReserveCd"));
		stat.setValue("ReserveTypeCd", reserveType);
		
		// Set FeatureAllocation Fields
		ModelBean featureAllocation = BeanTools.getParentBean(reserveAllocation, "FeatureAllocation");
		stat.setValue("FeatureCd", featureAllocation.gets("FeatureCd"));
		ModelBean featureDefinition = Feature.getFeatureDefinition(claim, featureAllocation);
		stat.setValue("FeatureTypeCd", featureDefinition.gets("SectionCd"));
		
		String subFeatureCd = featureAllocation.gets("FeatureSubCd", null);
		String itemNumber = featureAllocation.gets("ItemNumber", null);
		if (subFeatureCd != null)
			stat.setValue("FeatureSubCd", subFeatureCd);
		if (itemNumber != null)
			stat.setValue("ItemNumber", itemNumber);
		
		//Set AnnualStatementLineCd;
		String annualStatementLineCd = "";
		ModelBean claimPolicySubLimit = Feature.getClaimPolicySubLimit(claim, featureAllocation, false);
		if (claimPolicySubLimit != null) {
			annualStatementLineCd = claimPolicySubLimit.gets("AnnualStatementLineCd");
		}
		if (annualStatementLineCd.equals("")) {
			ModelBean subFeatureDefinition = featureDefinition.findBeanByFieldValue("ClaimSubCoverage", "CoverageSubCd", subFeatureCd);
			if (subFeatureDefinition != null) {
				annualStatementLineCd = subFeatureDefinition.gets("AnnualStatementLineCd");
			}
		}
		if (annualStatementLineCd.equals("")) {
			annualStatementLineCd = featureDefinition.gets("AnnualStatementLineCd");
		}
		
		stat.setValue("AnnualStatementLineCd", annualStatementLineCd);		
		// Set ClaimantTransaction Fields
		ModelBean claimantTransaction = BeanTools.getParentBean(featureAllocation, "ClaimantTransaction");
		stat.setValue("ClaimantTransactionCd", claimantTransaction.gets("TransactionCd"));
		stat.setValue("ClaimantTransactionNumber", claimantTransaction.gets("TransactionNumber"));
		stat.setValue("ClaimantTransactionIdRef", claimantTransaction.getId());
		stat.setValue("PayToName", claimantTransaction.gets("PayToName"));
		stat.setValue("PaymentAccountCd", claimantTransaction.gets("PaymentAccountCd"));
		// Set ClaimantTransaction Service Period Start and End Date Fields
		stat.setValue("ServicePeriodStartDt", claimantTransaction.getValue("ServicePeriodStartDt"));
		stat.setValue("ServicePeriodEndDt", claimantTransaction.getValue("ServicePeriodEndDt"));
		stat.setValue("ReasonCd", claimantTransaction.gets("ReasonCd"));
		
		// Set Claimant Fields
		ModelBean claimant = BeanTools.getParentBean(claimantTransaction, "Claimant");
		String claimantCd = StringTools.leftPad(claimant.getValueInt("ClaimantNumber"), "0", 3);		
		stat.setValue("ClaimantCd", claimantCd);
		String adjusterProviderRef = claimant.getBean("AssignedProvider", "AssignedProviderTypeCd", "AssignedAdjuster").gets("ProviderRef");
		stat.setValue("AdjusterProviderRef", adjusterProviderRef);
		if( !adjusterProviderRef.equals("") )
			stat.setValue("AdjusterProviderCd", Provider.getProviderNumber(data, Integer.parseInt(adjusterProviderRef)));
		stat.setValue("ClaimantStatusCd", claimant.gets("StatusCd"));
		
		// Override with Claimant Linking if needed
		ModelBean[] claimantLinks = claim.findBeansByFieldValue("Claimant", "ClaimantLinkIdRef", claimant.getId());
		if (claimant.gets("ClaimantLinkIdRef").length() > 0 || claimantLinks.length > 0) {
            Claimant claimantObj = new Claimant();
            ModelBean origClaimant = claimant;
            if (claimant.gets("ClaimantLinkIdRef").length() > 0) {
    			origClaimant = claim.findBeanById("Claimant", claimant.gets("ClaimantLinkIdRef"));
    			claimantCd = StringTools.leftPad(origClaimant.getValueInt("ClaimantNumber"), "0", 3);		
    			stat.setValue("ClaimantCd", claimantCd);
            }
			stat.setValue("ClaimantStatusCd", claimantObj.getClaimantLinkedStatus(claim, origClaimant));
			stat.setValue("ClaimantLinkIdRef", claimant.getId());
		}
		
		// Set Claim Fields
		stat.setValue("ClaimRef", claim.getSystemId());
		stat.setValue("ClaimNumber", claim.gets("ClaimNumber"));
		stat.setValue("ProductVersionIdRef", claim.gets("ProductVersionIdRef"));
        ProductVersion productVersion = ProductMaster.getProductVersion("Claims", claim.gets("ProductVersionIdRef"));
		stat.setValue("ProductName", productVersion.getBean().getParentBean().gets("Name"));
		ModelBean claimTransactionInfo = claim.getBean("ClaimTransactionInfo", "TransactionNumber", claim.gets("TransactionNumber"));
		stat.setValue("TransactionCd", claimTransactionInfo.gets("TransactionCd"));
		stat.setValue("TransactionNumber", claimTransactionInfo.gets("TransactionNumber"));
		stat.setValue("BranchCd", claim.gets("BranchCd"));
		stat.setValue("LossCauseCd", claim.gets("LossCauseCd"));
		stat.setValue("SubLossCauseCd", claim.gets("SubLossCauseCd"));
		String examinerProviderRef = claim.gets("ExaminerProviderRef");
		stat.setValue("ExaminerProviderRef", examinerProviderRef);
		if( !examinerProviderRef.equals("") )
			stat.setValue("ExaminerProviderCd", Provider.getProviderNumber(data, Integer.parseInt(examinerProviderRef)));
		stat.setValue("CustomerRef", claim.gets("CustomerRef"));
		
		
		// Set Policy Fields
		ModelBean claimPolicyInfo = claim.getBean("ClaimPolicyInfo");
		stat.setValue("PolicyProductVersionIdRef", claimPolicyInfo.gets("PolicyProductVersionIdRef"));
		stat.setValue("PolicyProductName", claimPolicyInfo.gets("PolicyProductName"));
		stat.setValue("PolicyTypeCd", claimPolicyInfo.gets("PolicyTypeCd"));
		stat.setValue("PolicyGroupCd", claimPolicyInfo.gets("PolicyGroupCd"));
						
		// Set the Status fields
		ModelBean feature = Feature.find(claimant, featureAllocation);
		ModelBean reserve = feature.getBean("Reserve", "ReserveCd", reserveAllocation.gets("ReserveCd"));
		stat.setValue("ReserveStatusCd", reserve.gets("StatusCd"));
		StringBuilder featureStatusCd = new StringBuilder(feature.gets("StatusCd"));
		if (StringTools.isTrue(feature.gets("DenyInd")))
			featureStatusCd.append(STATUS_DELIMETER).append(Feature.ST_DENIED);
		stat.setValue("FeatureStatusCd", featureStatusCd.toString());
		stat.setValue("ClaimStatusCd", claim.gets("StatusCd"));
		
		// Set the Property Damaged fields for multiple risk support
		String propertyDamagedNumber = "";
		String referenceId = feature.gets("PropertyDamagedIdRef");
		String notToRepairInd = "No";
		if (referenceId.length() > 0) {
			PropertyDamaged damaged = new PropertyDamaged();
			String damagedNo = damaged.getPropertyDamagedNumberByRef(claim, referenceId);
			propertyDamagedNumber = StringTools.leftPad(damagedNo, "0", 3);			
			stat.setValue("PropertyDamagedIdRef", referenceId);
			stat.setValue("PropertyDamagedNumber", propertyDamagedNumber);
			notToRepairInd = damaged.getNotToRepairInd(claim, referenceId);			
		} else {
			// Get the main risk			
			ModelBean[] props = claimant.getBeans("PropertyDamaged");
			for( ModelBean prop:props){
				if( prop.gets("PolicyRiskIdRef").equals(claim.gets("RiskIdRef"))) {
					notToRepairInd = prop.gets("NotToRepairInd");
					break;
				}
			}
		   			
		}
		
		stat.setValue("NotToRepairInd", notToRepairInd);
		
		// Set the Combined Key and Summary Key
		StringBuffer summaryKey = new StringBuffer();
        StringBuffer combinedKey = new StringBuffer();        
        summaryKey.append(stat.gets("ClaimNumber"))
        .append(KEY_DELIMETER)
        .append(stat.gets("ClaimantCd"));
        if (propertyDamagedNumber.length() > 0) {
        	summaryKey.append(KEY_DELIMETER).append(propertyDamagedNumber);
        }
        summaryKey.append(KEY_DELIMETER)
        .append(stat.gets("FeatureCd"))
        .append(KEY_DELIMETER)
        .append(stat.gets("FeatureSubCd"))
        .append(KEY_DELIMETER)
        .append(stat.gets("ItemNumber"))
        .append(KEY_DELIMETER)
        .append(stat.gets("ReserveCd"));
        
        combinedKey.append(summaryKey)
        .append(KEY_DELIMETER)
        .append(stat.gets("ClaimantTransactionNumber")); 
        stat.setValue("CombinedKey", combinedKey);
        stat.setValue("SummaryKey", summaryKey);
        		
	}
		
	/** The bridging record is not needed for claims, as there are no date ranges to pro-rate the amounts
	 * 
	 * @param previous The previous stat record
	 * @param current The current stat record
	 * @return The difference stat record
	 * @throws Exception when an error occurs
	 */
	public ModelBean createDiff(ModelBean previous, ModelBean current, ModelBean container) 
	throws Exception {
		try {
			// For Claims, there are no dates, and therefore a bridging record is not needed
			return null;
		} catch (Exception e) {
			throw e;
		}
	}

	/** Determine if the current claim has an action of Stop; the transaction that closes or ends the
	 * current claim.  
	 * 
	 * @param claim The claim ModelBean
	 * @return true/false if the claim transaction is of a Stop type
	 * @throws Exception when an error occurs
	 */
	protected boolean isStopRecord(ModelBean claim)
	throws Exception {
		try {
			// There are no stop transactions for claims
			return false;
			
		} catch (Exception e) {
			Log.error("Error", e);
			throw e;
		}
	}

	/** Determine if the current claim has an action of Restart; the transaction that closes or ends the
	 * current claim. 
	 * 
	 * @param claim The claim ModelBean
	 * @return true/false if the claim transaction is of a Restart type
	 * @throws Exception when an error occurs
	 */
	protected boolean isRestartRecord(ModelBean claim)
	throws Exception {
		try {
			// There are no stop transactions for claims
			return false;
			
		} catch (Exception e) {
			Log.error("Error", e);
			throw e;
		}
	}

	/** Get the list of field names that are amount fields. Used for processing the same calculations
	 * over those amount fields
	 * 
	 * @return The String array of amount field names
	 */
	public String[] getAmountFields() {
		return AMOUNT_FIELDS;
	}
	
	/** Obtain the list of table names that are associated with the current package.
	 * 
	 * @return The list of stat table names in String form
	 * @throws Exception when an error occurs
	 */
	public String[] getStatTables() 
	throws Exception {
		Hashtable<String, String> hash = new Hashtable<String, String>();
		
		ProductVersion[] versions = ProductMaster.getProductMaster(PACKAGE).getProductVersions(null, null, null, null, null);
		
		for (int cnt=0; cnt<versions.length; cnt++) {
			ModelBean product = versions[cnt].getProductSetup().getBean();
			ModelBean productStats = product.getBean("ProductStats");
			if (productStats != null) {
				ModelBean[] stats = productStats.getBeans("ProductStat");
				for (int j=0; j<stats.length; j++) {
					if (stats[j].gets("TypeCd").equals("") || stats[j].gets("TypeCd").equals("Stat")) {
						String beanName = stats[j].gets("BeanName");
						if (!hash.containsKey(beanName))
							hash.put(beanName, beanName);						
					}
				}
			}
		}
		
		// Break out the results from the hash into the string array
		Enumeration<String> list = hash.keys();
		String[] tableNames = new String[hash.size()];
		int cnt = 0;
		while (list.hasMoreElements()) {
			tableNames[cnt++] = (String) list.nextElement();
		}
		return tableNames;
	}

	/** Find the Product versions that uses the passed in statistic table name and falls within the 
	 * dates
	 * 
	 * @param tableName The statistic table name
	 * @param startDt The start date to determine if the product version is valid in
	 * @param endDt The end date to determine if the product version is valid in
	 * @return The list of Product version id's that fall within the parameters provided
	 * @throws Exception when an error occurs
	 */
	public String[] getStatProductVersions(String tableName, StringDate startDt, StringDate endDt)
	throws Exception {
		Hashtable<String, String> hash = new Hashtable<String, String>();
		
		ProductVersion[] versions = ProductMaster.getProductMaster(PACKAGE).getProductVersions(null, null, null, null, null);
		
		for (int cnt=0; cnt<versions.length; cnt++) {
			ModelBean product = versions[cnt].getProductSetup().getBean();
			ModelBean[] stats = product.getBean("ProductStats").getBeans("ProductStat");
			for (int j=0; j<stats.length; j++) {
				String beanName = stats[j].gets("BeanName");
				if (tableName.equals(beanName)) {
					// Now determine if the date range of the product are valid within the dates passed in
					StringDate newStartDt = versions[cnt].getBean().getDate("StartDt");
					StringDate newEndDt = versions[cnt].getBean().getDate("StopDt");
					if (dateRangeIntersects(newStartDt, newEndDt, startDt, endDt)) {
						String versionId = versions[cnt].getBean().gets("id");
						if (!hash.containsKey(versionId))
							hash.put(versionId, versionId);
						break;
					}
				}
			}
		}
		
		// Break out the results from the hash into the string array
		Enumeration<String> list = hash.keys();
		String[] versionNames = new String[hash.size()];
		int cnt = 0;
		while (list.hasMoreElements()) {
			versionNames[cnt++] = (String) list.nextElement();
		}
		return versionNames;
		
	}

	/** Determine if two date ranges intersect each other.  If so, they are inter-related
	 * 
	 * @param rangeOneStart The first date range start date
	 * @param rangeOneEnd The first date range end date 
	 * @param rangeTwoStart The second date range start date
	 * @param rangeTwoEnd The second date range end date
	 * @return true if they intersect, else false
	 * @throws Exception when an error occurs
	 */
	protected boolean dateRangeIntersects(StringDate rangeOneStart, StringDate rangeOneEnd, StringDate rangeTwoStart, StringDate rangeTwoEnd)
	throws Exception {
		if ( (rangeOneStart.compareTo(rangeTwoStart) >= 0 && rangeOneStart.compareTo(rangeTwoEnd) <= 0) ||
				 (rangeOneEnd.compareTo(rangeTwoStart) >= 0 && rangeOneEnd.compareTo(rangeTwoEnd) <= 0) || 
				 (rangeOneStart.compareTo(rangeTwoStart) <= 0 && rangeOneEnd.compareTo(rangeTwoEnd) >= 0) ) {
			return true;
		}
		return false;		
	}
	
	
	/** Regenerate a specific statistics table within the given date range.  The general steps to achieve
	 * this should be the following:
	 * 1) Obtain the specific containers that will generate stat records within this table
	 * 2) For each specific container, delete the stat records involved.
	 * 2) Obtain an exact view of the container at the date given and process the statistics again or
	 *    re-process the container at the date given.
	 * 
	 * @param data JDBCData repository data connection
	 * @param tableName The statistics table name to regenerate
	 * @param startDt The StringDate containing the start date of the regeneration
	 * @param endDt The StringDate containing the end date of the regeneration
	 * @param systemId The system id to begin the search for in the table
	 * @param todayDt The current processing date
	 * @throws StatException when an error occurs
	 */
	public void regenerateStats(JDBCData data, String tableName, StringDate startDt, StringDate endDt, int systemId, StringDate todayDt) 
	throws StatException {
		try {
			
			if (systemId > 0) {
				ModelBean container = new ModelBean("Claim");
				data.selectModelBean(container, systemId);
				if (container.gets("TypeCd").equals("Claim"))
					regenerateStatItem(data, systemId, startDt, endDt, tableName, todayDt);				
			} else {
				String[] versions = getStatProductVersions(tableName, startDt, endDt);
				// Create a hash table of all the version id's that can create the stat table
				Hashtable<String, String> versionIds = new Hashtable<String, String>();
				StringBuffer prodIds = new StringBuffer();
				for (int i=0; i<versions.length; i++) {
					String idKeyed = ModelSpecification.indexString(versions[i]);
					if (!versionIds.containsKey(idKeyed)) {
						versionIds.put(idKeyed, versions[i]);
						if (prodIds.length() > 0)
							prodIds.append(",");
						prodIds.append(idKeyed);					
					}
				}

	        	// Find the ModelBean database table name
	        	ModelBean clm = new ModelBean("Claim");
	        	String clmTable = clm.getTableName();
	        	
	        	JDBCData.QueryResult[] results =  data.doQueryFromLookup(clmTable, new String[] {"TypeCd", "ProductVersionIdRef"}, new String[] {"=", "IN"}, new String[] {"Claim", prodIds.toString()}, 0);
	        	for(int i=0; i<results.length; i++){
	        		int sysId = Integer.parseInt(results[i].getSystemId());
	    			regenerateStatItem(data, sysId, startDt, endDt, tableName, todayDt);
	    			// Commit the data every set number of policies
	    			if (i % COMMIT_NO == 0) {
	    				data.commit();
	    			}        				
	        	}				
			}
			        	
		} catch (Exception e) {
			throw new StatException("Unable to regenerate stats.", e);
		}		
	}
	
	/** Regenerate a specific statistics table within the given date range.  The general steps to achieve
	 * this should be the following:
	 * 1) Obtain the specific containers that will generate stat records within this table
	 * 2) For each specific container, delete the stat records involved.
	 * 2) Obtain an exact view of the container at the date given and process the statistics again or
	 *    re-process the container at the date given.
	 * 
	 * @param data JDBCData repository data connection
	 * @param tableName The statistics table name to regenerate
	 * @param startDt The StringDate containing the start date of the regeneration
	 * @param endDt The StringDate containing the end date of the regeneration
	 * @param containerNo The table unique name/number that is displayed to the user.  Used if the process
	 * 		  was halted midway, or for single table entry processing
	 * @param todayDt The current processing date
	 * @throws StatException when an error occurs
	 */
	public void regenerateStats(JDBCData data, String tableName, StringDate startDt, StringDate endDt, String containerNo, StringDate todayDt) 
	throws StatException {
		try {
			if (containerNo.length() > 0) {
				JDBCData.QueryResult[] results = data.doQueryFromLookup("Claim", new String[] {"TypeCd", "ClaimNumber"}, new String[] {"=","="}, new String[] {"Claim", containerNo}, 1);
				if (results.length > 0)
					regenerateStatItem(data, Integer.parseInt(results[0].getSystemId()), startDt, endDt, tableName, todayDt);
				else {
					throw new StatException("Unable to regenerate stats for table: " + tableName + " and number: " + containerNo);
				}
			} else {
				regenerateStats(data, tableName, startDt, endDt, 0, todayDt);
			}			        	
		} catch (Exception e) {
			Log.error(e);
			throw new StatException("Unable to regenerate stats.", e);
		}
	}
		
	/** Regenerate the policies that have the product version and have transactions which fall within
	 * the dates required by the statistic regeneration parameters
	 * 
	 * @param data JDBCData repository data connection
	 * @param systemId The system id of the policy which may be affected for regeneration
	 * @param startDt The start date of the regeneration
	 * @param endDt The end date of the regeneration
	 * @param statTable The statistic table name to use
	 * @param todayDt The current processing date
	 * @throws Exception when an error occurs
	 */
	public void regenerateStatItem(JDBCData data, int systemId, StringDate startDt, StringDate endDt, String statTable, StringDate todayDt)
	throws Exception {
		ModelBean claim = new ModelBean("Claim");
		data.selectModelBean(claim, systemId);
		
		// Determine the transactions that should be reprocessed based on the date range
		StringDate reportDt = claim.getDate("ReportedDt");
		if (reportDt.compareTo(startDt) >= 0 && reportDt.compareTo(endDt) <= 0) {
			
			// Determine the ending transaction number by obtaining the current transaction number
			int endNumber = claim.getValueInt("TransactionNumber");
			
			// Remove all of the stat records for the current selected claim
			String dbTableName = SQLRelationHelper.getDelegate(data).tableName(statTable);
			String deleteSql = "DELETE FROM " + dbTableName + " WHERE CLAIMNUMBER = '" + claim.gets("ClaimNumber") + "'";
			data.deleteStatement(deleteSql);			
			
			// Since we will likely get more than one transaction, use the more efficient ClaimAsOf
			ClaimAsOf asOf = new ClaimAsOf(claim);
			
			// Stash it away for getPreviousClaim() to use
			cachedClaimAsOf = asOf;
			
			for (int i=1; i<=endNumber; i++) {
				ModelBean history = claim.getBean("ClaimTransactionHistory");	
				if( history !=null){
				    ModelBean transaction = history.findBeanByFieldValue("ClaimTransactionInfo", "TransactionNumber", Integer.toString(i));
					if( transaction.gets("StatProcessedInd").equals("Yes") ){ 
		            	continue;
					}
				}
	            ModelBean claimAsOf = asOf.getClaimAsOfTransaction(i);				
				processStats(data, claimAsOf, todayDt, statTable);
			}			
		}				
	}	
	
	/** Process an individual stat model defined in the setup.  The stat model consists of rules 
	 * and setup information allowing the creation of stat record(s) for each defined important
	 * set of fields in the current container.  The method is overridden here to handle the case of
	 * reversing the entire previous set of stat records due to a significant change which will 
	 * affect all the current stat records.
	 * @param data The database repository connection
	 * @param setup The setup model containing the stat model
	 * @param claim The claim ModelBean
	 *  @param todayDt The current operational date which will be used to set the current date
	 * @throws StatException when an error occurs
	 */
	public void processStat(JDBCData data, ModelBean setup, ModelBean claim, StringDate todayDt)
	throws StatException {
		try {
			// Determine the one rule which will initiate a complete reverse
			curBean = claim;
			prevBean = getPreviousClaim(claim);
			// If the rules determine that the Claim should be affected by a complete reversal and re-calculation
			if (prevBean != null) {
				if (isCompleteReverse(prevBean, curBean)) {
					ModelBean[] reserveAllocations = claim.getAllBeans("ReserveAllocation");
					if (reserveAllocations.length > 0) {
						clearCodeBase();					
						ModelBean curStat = createStatBean(data, setup, curBean, reserveAllocations[0], new ModelBean[0]);
						processCompleteReversal(data, setup, claim, curStat, todayDt);
					}
				}				
			}			
			// Pre-process the Claim to add potential ReserveAllocations for status updates
			ModelBean updatedClaim = processStatusUpdates(data, prevBean, curBean);
			if (updatedClaim != null) {
				curBean = updatedClaim;
				super.processStat(data, setup, updatedClaim, todayDt);
			} else {
				super.processStat(data, setup, claim, todayDt);				
			}
		} catch (Exception e) {
			Log.error(e);
			e.printStackTrace();
			throw new StatException("Unable to process the stat record for this package", e);
		}
	}
	
	/** Obtain the previous Claim Bean transaction
	 * @param claim The current Claim 
	 * @return The Claim as of the previous transaction
	 * @throws Exception when an error occurs
	 */
	protected ModelBean getPreviousClaim(ModelBean claim) 
	throws Exception {
		int transactionNumber = Integer.parseInt(claim.gets("TransactionNumber"));
		if( transactionNumber > 1 )
			transactionNumber--;
		else
			return null;
		
		if (cachedClaimAsOf != null && cachedClaimAsOf.getClaim().getSystemId().equals(claim.getSystemId())) {
			// Use the ClaimAsOf stashed away by regenerateStatItem() if at all possible
			return cachedClaimAsOf.getClaimAsOfTransaction(transactionNumber);
		} else {
			ModelBean prevClaim = claim.cloneBean();
			Claim.getClaimAsOfTransaction(prevClaim, transactionNumber);
			return prevClaim;
		}
	}
	
	/** There are no balancing fields in Claim
	 * 
	 * @return The String array of amount field names
	 */
	public String[] getBalanceAmountFields() { 
		return new String[] {}; 
	}
	
	/** There are no balancing fields in Claim so always write records
	 * @param affected The list of stat affected records
	 * @param stat The stat ModelBean
	 * @return true/false indicator if the record should be written
	 * @throws Exception when an error occurs
	 */
	public boolean isValidBalances(ModelBean[] affected, ModelBean stat) 
	throws Exception {
		return true;
	}	
	
	/** From the previous claim transaction and the current transaction, determine the
	 * differences and set it in the stat record.
	 * @param stat The Stat ModelBean
	 * @param reserveAllocation The ReserveAllocation ModelBean
	 * @throws Exception when an error occurs
	 */ 
	private void setAmountDelta(ModelBean stat, ModelBean reserveAllocation)
	throws Exception {
		ModelBean oldReserveAllocation = null;
		if (prevBean != null) 
			oldReserveAllocation = prevBean.getBeanById(reserveAllocation.getId());
		if (oldReserveAllocation == null) {
			stat.setValue("ReserveChangeAmt", reserveAllocation.gets("ReserveAmt", "0.00"));
			stat.setValue("PaidAmt", reserveAllocation.gets("PaidAmt", "0.00"));
			stat.setValue("ExpectedRecoveryChangeAmt", reserveAllocation.gets("ExpectedRecoveryAmt", "0.00"));
			stat.setValue("PostedRecoveryAmt", reserveAllocation.gets("RecoveryAmt", "0.00"));											
		} else {
			stat.setValue("ReserveChangeAmt", StringTools.subtractMoney(reserveAllocation.gets("ReserveAmt", "0.00"), oldReserveAllocation.gets("ReserveAmt", "0.00")));
			stat.setValue("PaidAmt", StringTools.subtractMoney(reserveAllocation.gets("PaidAmt", "0.00"), oldReserveAllocation.gets("PaidAmt", "0.00")));
			stat.setValue("ExpectedRecoveryChangeAmt", StringTools.subtractMoney(reserveAllocation.gets("ExpectedRecoveryAmt", "0.00"), oldReserveAllocation.gets("ExpectedRecoveryAmt", "0.00")));
			stat.setValue("PostedRecoveryAmt", StringTools.subtractMoney(reserveAllocation.gets("RecoveryAmt", "0.00"), oldReserveAllocation.gets("RecoveryAmt", "0.00")));
		}
	}
	
	/** Determines if there were any change in the amounts 
	 * @param stat The stat ModelBean
	 * @return true/false if there were changes
	 * @throws Exception when an error occurs
	 */
	private boolean isAmountDelta(ModelBean stat)
	throws Exception {
		boolean isDelta = false;
		String zeroAmt = "0.00";
		for (String amountField : getAmountFields()) {
			if (!StringTools.equal(stat.gets(amountField), zeroAmt)) {
				isDelta = true;
				break;
			}
		}
		return isDelta;
	}
	
	/** Process the complete reversal when a KeyInd field is found to have changed
	 * @param data The data connection
	 * @param setup The Stat Setup ModelBean
	 * @param container The Account container
	 * @param stat The Stat ModelBean
	 * @param todayDt The current processing date
	 * @throws Exception when an error occurs
	 */ 
	private void processCompleteReversal(JDBCData data, ModelBean setup, ModelBean container, ModelBean stat, StringDate todayDt)
	throws Exception {
		delegate = SQLRelationHelper.getDelegate(data);	
		SQLRelationHelper sqlHelp = new SQLRelationHelper(data, delegate);
		
		// Determine the starting point of the reversal
		String claimNo = container.gets("ClaimNumber");
		String dbTableName = SQLRelationHelper.getDelegate(data).tableName("ClaimStats");
		String query = "SELECT * FROM " + dbTableName + " WHERE ReversalStopInd = 'Yes' AND ClaimNumber = '" + claimNo + "' ORDER BY SystemId DESC";
		String table = setup.gets("BeanName");
		ModelBean[] stopRecs = sqlHelp.doQuery(table, query, MAX_ROWS);
		String startRec = null;
		if (stopRecs.length > 0)
			startRec = stopRecs[0].getSystemId();
		else
			startRec = "0";
		
		// Start processing the reversal
		ModelBean[] results = sqlHelp.selectModelBean(table, new String[] {"SystemId", "ClaimNumber"}, new String[] {">=", "="}, new String[] {startRec, claimNo}, MAX_ROWS);
		
		// Reverse the resulting entries
		for (int i=0; i<results.length; i++) {			
			ModelBean reverse = reverseAmounts(results[i]);
			
			updateTransitionalFields(setup, stat, reverse);
			
			// Replace the transaction specific fields
			reverse.setValue("TransactionCd", stat.gets("TransactionCd"));
			reverse.setValue("TransactionNumber", stat.gets("TransactionNumber"));
			reverse.setValue("BookDt", stat.getValue("BookDt"));
			
			sqlHelp.saveBean(reverse);
		}
		
		for (int i=0; i<results.length; i++) {
			// Replace the resulting entries with the new KeyInd field values
			ModelBean key = results[i];
			ModelBean[] fields = setup.findBeansByFieldValue("StatAttribute", "KeyInd", "Yes");
			for (int cnt=0; cnt<fields.length; cnt++) {
				String value = stat.gets(fields[cnt].gets("Name"));
				key.setValue(fields[cnt].gets("Name"), value);
			}
			// Replace the core fields that initiated the reversal
			for (String reverseField : REVERSE_FIELDS) {
				key.setValue(reverseField, stat.getValue(reverseField));
				if( reverseField.equals("CatastropheRef")){
					//update the catastrophe number
				     key.setValue("CatastropheNumber", stat.gets("CatastropheNumber"));			         
				}
				if( reverseField.equals("LossDt")){					
					//update the Loss Year
				     key.setValue("LossYear", Integer.toString(stat.getDate("LossDt").getCalendar().get(Calendar.YEAR)));	
				   //update the Reported Date
				     key.setValue("ReportDt", stat.getValue("ReportDt"));						
				}
				if( reverseField.equals("LossCauseCd")){					
					//update the SubLossCauseCd
				     key.setValue("SubLossCauseCd", stat.gets("SubLossCauseCd"));				  					
				}
			}
			key.setValue("SystemId", "");  // Reset the system id so it will insert a new record
			if (i == 0) 
				key.setValue("ReversalStopInd", "Yes");
			
			updateTransitionalFields(setup, stat, key);
			
			// Replace the transaction specific fields
			key.setValue("TransactionCd", stat.gets("TransactionCd"));
			key.setValue("TransactionNumber", stat.gets("TransactionNumber"));
			key.setValue("BookDt", stat.getValue("BookDt"));
			
			sqlHelp.saveBean(key);
		}
	}
	
	/** Determine if a complete reversal and capture of stats is needed.  This is only done on data that
	 * affects each stat record.  If so, then a complete reversal of all the current stat records is needed.  
	 * Then a rebuild of the current stat records with the new data.
	 * @param previousClaim The previous Claim
	 * @param currentClaim The current Claim
	 * @return true/false indicator if the Claim needs a reversal
	 * @throws Exception when an error occurs
	 */
	protected boolean isCompleteReverse(ModelBean previousClaim, ModelBean currentClaim) 
	throws Exception {
		boolean doReverse = false;
		for (String reverseField : REVERSE_FIELDS) {
			String prevValue = previousClaim.getValueString(reverseField);
			String curValue = currentClaim.getValueString(reverseField);
			if (!prevValue.equals(curValue)) {
				doReverse = true;
				break;
			}
		}
		return doReverse;			
	}
	
	/** Create a hash key that mimics the Summary Key for a Stat record.  It should be unique enough 
	 * to differentiate between Feature/Reserve within a Claimant.
	 * @param claimant The Claimant ModelBean
	 * @param feature The Feature/FeatureAllocation ModelBean
	 * @return The Summary Key to hash
	 * @throws Exception when an error occurs
	 */
	protected String createSummaryHashKey(ModelBean claimant, ModelBean feature, String reserveCd) 
	throws Exception {
		String claimantCd = StringTools.leftPad(claimant.getValueInt("ClaimantNumber"), "0", 3);
		StringBuilder hashKey = new StringBuilder();
		hashKey.append(claimantCd)
			   .append(KEY_DELIMETER)
		       .append(feature.gets("FeatureCd"));
		hashKey.append(KEY_DELIMETER);
		if (feature.gets("FeatureSubCd") != null)
			hashKey.append(feature.gets("FeatureSubCd"));
		hashKey.append(KEY_DELIMETER);
		if (feature.gets("ItemNumber") != null)
			hashKey.append(feature.gets("ItemNumber"));
		hashKey.append(KEY_DELIMETER);
		if (reserveCd != null)
			hashKey.append(reserveCd);
		
		return hashKey.toString();
	}
	
	/** Track and create Claim Stats records if there are any changes to the Status of heirarchial
	 * parents of the stat records (Feature, Claimant, Claim)
	 * @param previousClaim The previous version of the Claim
	 * @param currentClaim The current version of the Claim
	 * @return
	 * @throws Exception
	 */
	protected ModelBean processStatusUpdates(JDBCData data, ModelBean previousClaim, ModelBean currentClaim) 
	throws Exception {
		// Return right away if there is no need to track changes
		if (previousClaim == null) {
			return null; 
		}
		Money zeroAmt = new Money("0.00");
		boolean hasClaimChange = false;
		ModelBean modifyClaim = null;
		if (!previousClaim.gets("StatusCd").equals(currentClaim.gets("StatusCd"))) {
			hasClaimChange = true;
		}
		String transactionNo = currentClaim.gets("TransactionNumber");
        ModelBean history = currentClaim.getBean("ClaimTransactionHistory");
        ModelBean currentTransaction = history.getBean("ClaimTransactionInfo", "TransactionNumber", transactionNo);
		
		String reserveTransCd = Transaction.getProductTransactionCdValue(currentClaim, Transaction.TX_ADJUST_RESERVE);
		String recoveryTransCd = Transaction.getProductTransactionCdValue(currentClaim, Transaction.TX_ADJUST_RECOVERY);
		
		ModelBean claimants[] = currentClaim.getBeans("Claimant");
		int transactionCnt = 0;
		for (ModelBean claimant : claimants) {
			ModelBean reserveTransaction = null;
			ModelBean recoveryTransaction = null;
			boolean hasClaimantChange = false;
			ModelBean previousClaimant = previousClaim.getBeanById("Claimant", claimant.getId());
			if (previousClaimant != null && !previousClaimant.gets("StatusCd").equals(claimant.gets("StatusCd"))) {
				hasClaimantChange = true;
			}
			
			ModelBean[] featureList = claimant.getBeans("Feature");
			for (ModelBean feature : featureList) {
				boolean hasFeatureChange = false;
				ModelBean previousFeature = previousClaim.findBeanById("Feature", feature.getId());
				if (previousFeature != null && !previousFeature.gets("StatusCd").equals(feature.gets("StatusCd"))) {
					hasFeatureChange = true;
				}
				
				// If there has been any status changes, then determine if the reserves need to be updated in stats
				if (hasClaimChange || hasClaimantChange || hasFeatureChange) {
					ModelBean[] reserveList = feature.getBeans("Reserve");
					for (ModelBean reserve : reserveList) {
						boolean foundReserveAllocation = false;
						String reserveCd = reserve.gets("ReserveCd");						
						ModelBean[] newClaimantTransactions = claimant.findBeansByFieldValue("ClaimantTransaction", "ClaimTransactionHistoryRef", currentTransaction.getId());
						for (ModelBean newClaimantTransaction : newClaimantTransactions) {
							ModelBean featureAlloc = Feature.findAllocation(newClaimantTransaction, feature);
							if (featureAlloc != null) {
								ModelBean reserveAlloc = featureAlloc.getBean("ReserveAllocation", "ReserveCd", reserveCd);
								if (reserveAlloc != null) {
									foundReserveAllocation = true;
								}
							}
						}
						if (!foundReserveAllocation) {
							// The reserve has no transactions, therefore create one to have stats written out for it
							ModelBean modifyClaimantTransaction = null;
							if (ClaimantTransactionRenderer.isReserveType(currentClaim, reserveCd)) {
								if (reserveTransaction == null) {
									reserveTransaction = new ModelBean("ClaimantTransaction");
									transactionCnt++;
									setupClaimantTransaction(data, reserveTransaction, reserveTransCd, currentTransaction, transactionCnt);
								}
								modifyClaimantTransaction = reserveTransaction;
							} else {
								if (recoveryTransaction == null) {
									recoveryTransaction = new ModelBean("ClaimantTransaction");
									transactionCnt++;
									setupClaimantTransaction(data, recoveryTransaction, recoveryTransCd, currentTransaction, transactionCnt);
								}								
								modifyClaimantTransaction = recoveryTransaction;
							}
							ModelBean modifyFeatureAllocation = Feature.setAllocation(modifyClaimantTransaction, feature);
							ModelBean modifyReserveAllocation = Reserve.createAllocation(reserveCd, zeroAmt, zeroAmt, zeroAmt, zeroAmt, feature.gets("StatusCd"));
							modifyFeatureAllocation.addValue(modifyReserveAllocation);
						}						
					}
				}
				if (reserveTransaction != null || recoveryTransaction != null) {
					// Add the new Transactions
					if (modifyClaim == null) {
						modifyClaim = currentClaim.cloneBean();
					}
					ModelBean modifyClaimant = modifyClaim.findBeanById("Claimant", claimant.getId());
					if (reserveTransaction != null) {
						modifyClaimant.addValue(reserveTransaction);						
					}
					if (recoveryTransaction != null) {
						modifyClaimant.addValue(recoveryTransaction);						
					}					
				}
			}			
		}
		return modifyClaim;
	}
	
    /** Fill a claimant transaction ModelBean with the basic detail information.  Some of the data
     * will be passed in from the calling logic, specifically the user, transaction date and time.
     * @param data JDBCData Connection Object
     * @param claimantTransaction The Claimant Transaction used to fill the details with
     * @param transactionCd The transaction code for this action
     * @param userId The userId to tie this transaction to
     * @param transactionDt The date of this transaction
     * @param transactionTm The time of this transaction
     * @return The claimant transaction ModelBean filled with basic setup information
     * @throws Exception when an error occurs
     */
    protected void setupClaimantTransaction(JDBCData data, ModelBean claimantTransaction, String transactionCd, ModelBean currentTransaction, int transactionCnt)
    throws Exception {
    	
		claimantTransaction.setValue("TransactionCd", transactionCd );
        
		// Set Status to Active
		claimantTransaction.setValue("StatusCd", "Active" );
		
        // Set up the User, Date, Time for the transaction
        claimantTransaction.setValue("TransactionDt", currentTransaction.getDate("TransactionDt"));
        claimantTransaction.setValue("TransactionTm", currentTransaction.gets("TransactionTm"));
        claimantTransaction.setValue("TransactionUser", "System");

        // Set the Transaction Number and Sequence Number
        claimantTransaction.setValue("TransactionNumber", currentTransaction.gets("TransactionNumber"));
        
        // Set the Book Date
        claimantTransaction.setValue("BookDt", currentTransaction.getDate("BookDt"));
    }
	
    /** Set the Risk Information fields for the current stat record
     * @param claim The Claim ModelBean
     * @param featureAllocation The current Feature Allocation
     * @param stat The current Stat ModelBean
     * @throws Exception when an error occurs
     */
    public void setRiskInformation(ModelBean claim, ModelBean featureAllocation, ModelBean stat)
    throws Exception {
        String propertyDamagedIdRef = featureAllocation.gets("PropertyDamagedIdRef");
        ModelBean policyRisk = null;
        if (propertyDamagedIdRef.length() > 0) {
	        PropertyDamaged damaged = new PropertyDamaged();
	        policyRisk = damaged.getPolicyRiskByPropertyDamagedRef(claim, propertyDamagedIdRef);	        	
        } else {
			String riskId = claim.gets("RiskIdRef");
			policyRisk = claim.getBeanById(riskId);	        	
        }
		stat.setValue("LineCd", policyRisk.gets("LineCd"));
		String riskCd = "";						
		String locationNumber = policyRisk.gets("LocationNumber");
		String riskNumber = policyRisk.gets("RiskNumber");
		
		if( !locationNumber.equals("") ) {
			riskCd = StringTools.leftPad(locationNumber, "0", 3);
		}
		if( !riskNumber.equals("") ) {
			riskCd = riskCd + StringTools.leftPad(riskNumber, "0", 3);
		}
		else {
			riskCd = policyRisk.gets("TypeCd");						
		}
		stat.setValue("RiskCd", riskCd);
    	
    }
    
    /**Check to see if claim was switch from Manual to live
     * 
     * @param claim ModelBean The claim bean
     * @return ttrue if claim was swtich from Manual to live
     * @throws Exception when an error occurs0
     */
    public boolean switchToLiveClaimInd(ModelBean claim) throws Exception {
    	boolean ind = false;
    	if( prevBean !=null){
	    	if( StringTools.isTrue(prevBean.gets("HistoryModeInd")) && StringTools.isFalse(claim.gets("HistoryModeInd")) ){
	    		ind = true;
	    	}  	    
    	}
    	return ind;
    }
    
    /** Update stat outstanding amounts when claim is switched from manual to live mode       
     * @param stat
     * @param reserveAllocation
     * @throws Exception
     */
    public void updateChangeAmts(ModelBean stat, ModelBean reserveAllocation) throws Exception {
    	String reserveChangeAmt = "0.00";
    	String expectedRecoveryChangeAmt = "0.00";
    	String paidAmt = "0.00";
    	String postedRecoveryAmt = "0.00";
    	if (prevBean != null) {    		   		
    		ModelBean feature = reserveAllocation.getParentBean();
    		ModelBean claimant = feature.getParentBean().getParentBean();
    		ModelBean oldClaimant = prevBean.getBeanById("Claimant", claimant.getId());
    		if( oldClaimant !=null){
    			ModelBean[] transactions = oldClaimant.getBeans("ClaimantTransaction");
    			for( ModelBean transaction:transactions){
    				ModelBean[] oldFeatures = transaction.findBeansByFieldValue("FeatureAllocation", "FeatureCd", feature.gets("FeatureCd"));
    				for( ModelBean oldFeature:oldFeatures){	    				
	    					ModelBean oldReserveAllocation = oldFeature.findBeanByFieldValue("ReserveAllocation", "ReserveCd", stat.gets("ReserveCd"));
	    					if( oldReserveAllocation!=null){
	    						/*boolean updateInd = false;
	    						if( stat.gets("FeatureSubCd").isEmpty() && stat.gets("ItemNumber").isEmpty() ){
	    							updateInd = true;
	    						} else if( !stat.gets("FeatureSubCd").isEmpty() & stat.gets("FeatureSubCd").equals(oldFeature.gets("FeatureSubCd")) ){
	    							if( stat.gets("ItemNumber").isEmpty() && oldFeature.gets("ItemNumber").isEmpty() ){
	    								updateInd = true;
	    							} else if ( stat.gets("ItemNumber").equals(oldFeature.gets("ItemNumber")) ){
	    								updateInd = true;
	    							}
	    						} else if( !stat.gets("ItemNumber").isEmpty() && stat.gets("ItemNumber").equals(oldFeature.gets("ItemNumber")) ){
	    							if( stat.gets("FeatureSubCd").isEmpty() && oldFeature.gets("FeatureSubCd").isEmpty() ){
	    								updateInd = true;
	    							} else if ( stat.gets("FeatureSubCd").equals(oldFeature.gets("FeatureSubCd")) ){
	    								updateInd = true;
	    							}
	    						}*/
	    						if( stat.gets("FeatureSubCd", "").equals(oldFeature.gets("FeatureSubCd", "")) && stat.gets("ItemNumber", "").equals(oldFeature.gets("ItemNumber","")) ) { 
	    						//if( updateInd){
		    						reserveChangeAmt = StringTools.addMoney(oldReserveAllocation.gets("ReserveAmt", "0.00"), reserveChangeAmt);
		    						expectedRecoveryChangeAmt = StringTools.addMoney(oldReserveAllocation.gets("ExpectedRecoveryAmt", "0.00"), expectedRecoveryChangeAmt);
		    						paidAmt = StringTools.addMoney(oldReserveAllocation.gets("PaidAmt", "0.00"), paidAmt);
		    						postedRecoveryAmt = StringTools.addMoney(oldReserveAllocation.gets("RecoveryAmt", "0.00"), postedRecoveryAmt);
	    						}
	    					}
	    				
    				}
    			}
    		}
    		
    		if( !reserveChangeAmt.equals("$0.00")){
    			stat.setValue("HistoricReserveAmt", StringTools.addMoney(reserveChangeAmt, stat.gets("HistoricReserveAmt")));    			
    		}
    		if( !expectedRecoveryChangeAmt.equals("$0.00")){
    			stat.setValue("HistoricExpectedRecoveryAmt", StringTools.addMoney(expectedRecoveryChangeAmt, stat.gets("HistoricExpectedRecoveryAmt")));    			
    		}
    		if( !paidAmt.equals("$0.00")){
    			stat.setValue("HistoricPaidAmt", StringTools.addMoney(paidAmt, stat.gets("HistoricPaidAmt")));    			
    		}
    		if( !postedRecoveryAmt.equals("$0.00")){
    			stat.setValue("HistoricPostedRecoveryAmt", StringTools.addMoney(postedRecoveryAmt, stat.gets("HistoricPostedRecoveryAmt")));    			
    		}
    	}
    }
    
    /** Build a list of ClaimStats for historical transactions
     * 
     * @param data JDBCData The data connection
     * @param setup ModelBean The set up bean
     * @param claim ModelBean The claim bean
     * @return list of ClaimStats for historical transactions
     * @throws Exception when an error occurs
     */
    public ModelBean[] buildHistoricStatList(JDBCData data, ModelBean setup, ModelBean claim) throws Exception {
    	HashMap<String, ModelBean> historicStatlist = new HashMap<String,ModelBean>();    
    	ModelBean[] claimants = prevBean.getBeans("Claimant");   	    	
		for( ModelBean claimant:claimants ) {
			ModelBean[] allocations = claimant.getAllBeans("ReserveAllocation");			
			for( ModelBean allocation:allocations) {				
				ModelBean stat = createStatBean(data, setup, claim, allocation, new ModelBean[0]);
				String summaryKey =  stat.gets("SummaryKey");
				if( !historicStatlist.containsKey(summaryKey) ){					
					// update stat record to clear out all amounts
					stat.setValue("ReserveChangeAmt", "0.00");
					stat.setValue("ExpectedRecoveryChangeAmt", "0.00");
					stat.setValue("PaidAmt", "0.00");
					stat.setValue("PostedRecoveryAmt", "0.00");						
					historicStatlist.put(summaryKey,stat);			
				}
			}
		}
		
		ArrayList<ModelBean> list = new ArrayList<ModelBean>();
		Iterator<String> iter = historicStatlist.keySet().iterator();
		while (iter.hasNext()) {
			String key = (String) iter.next();
			ModelBean stat = historicStatlist.get(key);
			ModelBean[] allocations = prevBean.getAllBeans("ReserveAllocation");
			for( ModelBean allocation:allocations){
				ModelBean bean = new ModelBean(setup.gets("BeanName"));	
				setKeyFields(data, setup, claim, allocation, bean);				
				if( stat.gets("SummaryKey").equals(bean.gets("SummaryKey"))){
					updateChangeAmts(stat, allocation);
					list.add(stat);
					break;
				}				
			}
		}				
    	
    	return list.toArray(new ModelBean[list.size()]);
    }
    
    /** Check to see if a history stat record matching the summary key has already been processed to avoid duplicates
     * 
     * @param data JDBCData The data connection
     * @param setup ModelBean The set up bean
     * @param claim ModelBean The claim bean
     * @param reserveAllocation ModelBean The ReserveAllocation bean
     * @param list List of stat beans already processed
     * @return true if a similar stat bean has already been procesed, false if none found
     * @throws Exception when an error occurs
     */
    public boolean hasProcessedHistoryStatRecord(JDBCData data, ModelBean setup,ModelBean claim, ModelBean reserveAllocation,ArrayList<ModelBean> list) throws Exception {
    	boolean processedInd = false;
    	ModelBean[] statList = list.toArray(new ModelBean[list.size()]);
    	ModelBean stat = new ModelBean(setup.gets("BeanName"));			
		// Call the common method to set the main key fields so we can compare
		setKeyFields(data, setup, claim, reserveAllocation, stat);	
    	for( ModelBean bean:statList){
    		if( bean.gets("SummaryKey").equals(stat.gets("SummaryKey")) ){
    			processedInd = true;
    			break;
    		}
    		
    	}
    	
    	return processedInd;
    }
}