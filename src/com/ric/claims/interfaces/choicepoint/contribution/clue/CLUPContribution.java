package com.ric.claims.interfaces.choicepoint.contribution.clue;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.math.Money;
import com.iscs.claims.claim.Claim;
import com.iscs.claims.common.Reserve;
import com.ric.claims.interfaces.choicepoint.contribution.clue.CLUPContributionHandler;
import com.iscs.claims.interfaces.choicepoint.contribution.clue.entity.CLUPClaimContrib;
import com.iscs.claims.interfaces.choicepoint.contribution.clue.entity.CLUPClaimInfo;
import com.iscs.claims.transaction.PropertyDamaged;
import com.iscs.common.business.company.Company;
import com.iscs.common.render.VelocityTools;
import com.iscs.uw.policy.Policy;

public class CLUPContribution extends com.iscs.claims.interfaces.choicepoint.contribution.clue.CLUPContribution implements CLUPContributionHandler {
	
	public CLUPContribution() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public CLUPClaimContrib createPropertyContribution(ModelBean claim, StringDate todayDt, String propertyDamagedIdRef) throws Exception {
		CLUPClaimContrib clup = new CLUPClaimContrib();				
		clup.setClaim(createClaimRecord(claim,propertyDamagedIdRef));  	
		return clup;
	}
	
	protected CLUPClaimInfo createClaimRecord(ModelBean claim, String propertyDamagedIdRef) throws Exception {
		CLUPClaimInfo claimInfo = new CLUPClaimInfo();
		ModelBean policyInfo = claim.getBean("ClaimPolicyInfo");
		ModelBean carrier = Company.getCompany().getBean().getBeanById("Carrier", policyInfo.gets("CarrierCd"));
		claimInfo.setAMBestNumber(carrier.gets("AMBestNumber"));				
		
		fillInsuredInfo(claim, claimInfo);
		fillPropertyInfo(claim, claimInfo, propertyDamagedIdRef);
		fillClaimantInfo(claim, claimInfo);
		fillClaimAmount(claim, claimInfo,propertyDamagedIdRef);
		
		// Set disposition
		ModelBean[] subro = claim.getBeans("Subrogation");
		if (subro.length > 0) {
			claimInfo.setDispositionCd("S");			
		} else {
			if (Claim.isOpen(claim.gets("StatusCd"))) {
				claimInfo.setDispositionCd("O");
			} else {
				claimInfo.setDispositionCd("C");				
			}
		}
		
		// Innovation uses the 'Aggregate Amount' reporting type.  It reports the current total rather than each individual transaction amount
		claimInfo.setReportingStatus("A");
		
		return claimInfo;
	}

	protected void fillInsuredInfo(ModelBean claim, CLUPClaimInfo claimInfo) throws Exception {
		JDBCData data = VelocityTools.getConnection();
		ModelBean claimPolicyInfo = claim.getBean("ClaimPolicyInfo");
		String policyRef = claimPolicyInfo.gets("PolicyRef");
		ModelBean policy = Policy.getPolicyBySystemId(data,Integer.parseInt(policyRef));
		ModelBean insuredBean = policy.getBean("Insured");
		String entityType = insuredBean.gets("EntityTypeCd");
		
		ModelBean[] insureds = claim.getBean("ClaimPolicyInfo").findBeansByFieldValue("PartyInfo", "PartyTypeCd", "InsuredParty");
		for (int i=0; i<insureds.length; i++) {
			if (i > 1) 
				break;
			ModelBean partyInfo = insureds[i];
			ModelBean nameInfo = partyInfo.getBean("NameInfo");
			if (nameInfo != null) {
				if (i == 0) {
					claimInfo.setInsuredPrefixCd1(nameInfo.gets("PrefixCd"));
					claimInfo.setInsuredSurname1(nameInfo.gets("Surname"));
					claimInfo.setInsuredGivenName1(nameInfo.gets("GivenName"));
					claimInfo.setInsuredOtherGivenName1(nameInfo.gets("OtherGivenName"));
					claimInfo.setInsuredSuffixCd1(nameInfo.gets("SuffixCd"));
				} else {
					claimInfo.setInsuredPrefixCd2(nameInfo.gets("PrefixCd"));
					claimInfo.setInsuredSurname2(nameInfo.gets("Surname"));
					claimInfo.setInsuredGivenName2(nameInfo.gets("GivenName"));
					claimInfo.setInsuredOtherGivenName2(nameInfo.gets("OtherGivenName"));
					claimInfo.setInsuredSuffixCd2(nameInfo.gets("SuffixCd"));
				}
			}
			ModelBean taxInfo = partyInfo.getBean("TaxInfo");
			if (taxInfo != null) {
				if (i == 0) {
					claimInfo.setInsuredSSN1(taxInfo.gets("SSN"));
				} else {
					claimInfo.setInsuredSSN2(taxInfo.gets("SSN"));
				}
			}
			ModelBean personInfo = partyInfo.getBean("PersonInfo");
			if (personInfo != null) {
				if (i == 0) {
					claimInfo.setInsuredBirthDt1(personInfo.getDate("BirthDt"));
					claimInfo.setInsuredGenderCd1(personInfo.gets("GenderCd"));					
				} else {
					claimInfo.setInsuredBirthDt2(personInfo.getDate("BirthDt"));
					claimInfo.setInsuredGenderCd2(personInfo.gets("GenderCd"));					
				}
			}
		}
		
		if(entityType.equalsIgnoreCase("Joint"))
		{
			ModelBean insuredPartyJoint = insuredBean.findBeanByFieldValue("PartyInfo", "PartyTypeCd", "InsuredPartyJoint");
			ModelBean insuredPartyBean = claim.getBean("ClaimPolicyInfo").findBeanByFieldValue("PartyInfo", "PartyTypeCd", "InsuredParty");
			ModelBean nameInfo = insuredPartyJoint.getBean("NameInfo");
			if (nameInfo != null) {
				claimInfo.setInsuredPrefixCd2(nameInfo.gets("PrefixCd"));
				claimInfo.setInsuredSurname2(nameInfo.gets("Surname"));
				claimInfo.setInsuredGivenName2(nameInfo.gets("GivenName"));
				claimInfo.setInsuredOtherGivenName2(nameInfo.gets("OtherGivenName"));
				claimInfo.setInsuredSuffixCd2(nameInfo.gets("SuffixCd"));
			}
			
			ModelBean taxInfo = insuredPartyBean.findBeanByFieldValue("TaxInfo", "TaxTypeCd", "InsuredTaxInfo");
			ModelBean jointTaxInfo = insuredPartyJoint.getBean("TaxInfo");
			if(taxInfo != null){
				claimInfo.setInsuredSSN1(taxInfo.gets("SSN"));
			}
			if (jointTaxInfo != null) {
				claimInfo.setInsuredSSN2(jointTaxInfo.gets("SSN"));
			}
			ModelBean personInfo = insuredPartyJoint.getBean("PersonInfo");
			if (personInfo != null) {
				claimInfo.setInsuredBirthDt2(personInfo.getDate("BirthDt"));
				claimInfo.setInsuredGenderCd2(personInfo.gets("GenderCd"));	
			}
		}
	}
	
	   private void fillClaimAmount(ModelBean claim, CLUPClaimInfo claimInfo, String propertyDamagedIdRef) throws Exception {
	    	Money zeroAmt = new Money("0.00");
	    	Money claimAmt = zeroAmt;
	    	boolean isPrimaryRisk = false;
			if (!propertyDamagedIdRef.equals("")) {
				// Determine if this is a primary risk on a multi-risk claim.  Because the allocations for a primary risk on a muli-risk will have blank references.
				PropertyDamaged damaged = new PropertyDamaged();
	    		ModelBean policyRisk = damaged.getPolicyRiskByPropertyDamagedRef(claim, propertyDamagedIdRef);
		    	String riskId = claim.gets("RiskIdRef");
		    	ModelBean primaryRisk = claim.getBeanById(riskId);
				if (policyRisk.getId().equals(primaryRisk.getId())) {
					isPrimaryRisk = true;
				}
			}
	    	ModelBean[] transactions = claim.getAllBeans("ClaimantTransaction");    	
			for( ModelBean transaction:transactions){
				ModelBean[] featureAllocations = transaction.findBeansByFieldValue("FeatureAllocation", "PropertyDamagedIdRef", propertyDamagedIdRef);
				claimAmt = calculateClaimAmount(claim, featureAllocations, claimAmt);
				// Include also the blank references for primary risk, this is in case the primary risk has been changed several times with details
				if (isPrimaryRisk) {
					featureAllocations = transaction.findBeansByFieldValue("FeatureAllocation", "PropertyDamagedIdRef", "");
					claimAmt = calculateClaimAmount(claim, featureAllocations, claimAmt);			
				}
			} 
	    	// LexisNexis accepts only unsigned zoned decimals i.e no negative numbers ,
			// because of that we are sending as claim amount as zero amount in case of negative number
			if(claimAmt.compareTo(zeroAmt)<0) {
				claimAmt = zeroAmt;
	    	}
	    	claimInfo.setClaimAmt(claimAmt.toString());
	    }

	    private Money calculateClaimAmount(ModelBean claim, ModelBean[] featureAllocations, Money claimAmt) 
	    throws Exception {
	    	Money zeroAmt = new Money("0.00");
			for( ModelBean featureAllocation:featureAllocations){    				
				ModelBean[] reserveAllocations = featureAllocation.getBeans("ReserveAllocation");
				for( ModelBean reserveAllocation:reserveAllocations){
					String reserveTypeCd = Reserve.getReserveType(claim, reserveAllocation.gets("ReserveCd"));
					if (reserveTypeCd.equals("Indemnity")) {
		    			Money paidAmt = new Money(reserveAllocation.gets("PaidAmt"));
		    			if (!paidAmt.equals(zeroAmt)) {
		    				claimAmt = claimAmt.add(paidAmt);
		    			}
		    		} else if( reserveTypeCd.equals("Salvage") || reserveTypeCd.equals("Subrogation") || reserveTypeCd.equals("Recovery") )  {
		    			Money receivedAmt = new Money(reserveAllocation.gets("RecoveryAmt"));
		    			if (!receivedAmt.equals(zeroAmt)) {
		    				claimAmt = claimAmt.subtract(receivedAmt);
		    			}    			
		    		}
				}    					    				
			}
			return claimAmt;
	    }
	
}
