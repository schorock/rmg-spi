package com.ric.claims.interfaces.choicepoint.contribution.clue.handler;

import java.util.HashMap;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;
import net.inov.tec.data.JDBCLookup;
import net.inov.tec.data.JDBCLookup.LookupKey;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

import com.ric.claims.interfaces.choicepoint.contribution.ContributionJobHelper;
import com.iscs.claims.interfaces.choicepoint.contribution.clue.CLUEBuilder;
import com.iscs.claims.interfaces.choicepoint.contribution.clue.CLUPGenerator;
import com.iscs.claims.interfaces.choicepoint.contribution.clue.entity.CLUPClaimContrib;
import com.iscs.common.tech.bean.pojomapper.BeanPojoMapper;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.error.ErrorTools;
import com.ric.interfaces.choicepoint.contribution.ContributionFileGenerator;
import com.iscs.interfaces.utility.record.Record;

/** Create the initial ChoicePoint Current Carrier Contribution.  The initial contribution
 * consists of all Active and in-force policies. 
 * The contribution consists of the current state or transaction the policy is at.  There 
 * is no need to regenerate the policy as of its inception date.
 * 
 * @author allend
 *
 */
public class CLUPInitialContribution extends InnovationIBIZHandler {
	
	public CLUPInitialContribution() throws Exception {
	}
	
    /** Process the Initial Contribution
     * 1) Obtain the job parameters, and determine if regen-ability is needed.  
     * 2) If so, delete the previous initial contribution records
     * 3) Process all Active and Inforce policies
     * 4) Convert these into the File format required
     * 5) If electronic transmission available, call the transmission
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process()
    throws ServiceHandlerException {
        JDBCData data = this.getConnection();
        try {            
            Log.debug("Processing CLUPInitialContribution...");
            
            ModelBean rs = this.getHandlerData().getResponse();
            ModelBean rq = this.getHandlerData().getRequest();
            AdditionalParams ap = new AdditionalParams(rq);
            StringDate todayDt = DateTools.getStringDate(rs.getBean("ResponseParams"));
            String todayTm = DateTools.getTime(rs.getBean("ResponseParams"));            
            
         // Ensure the Errors bean exists in the response
        	ModelBean responseParams = getResponse().getBean("ResponseParams");
        	ModelBean errorsBean = responseParams.getBean("Errors");
        	if(errorsBean == null){
        		errorsBean = new ModelBean("Errors");
        		responseParams.addValue(errorsBean);
        	}
        	
            // Obtain the parameters needed for the batch job
            boolean regenInd = StringTools.isTrue(ap.gets("RegenerateInd"));
            boolean transmitInd = StringTools.isTrue(ap.gets("TransmitElectronicallyInd"));
            String productId = ap.gets("CLProductId");            
            
            if (regenInd) {
            	deletePrevious(data, productId);
            } else {
            	// Validate that there are no other initial contribution data
        		if (productId.equals("All")) {
                	JDBCData.QueryResult[] results = data.doQueryFromLookup("CPCLUPContrib", new String[] {"TypeCd"}, new String[] {"IN"}, new String[] {"INITIAL", "PERIOD"}, 1);
                	if (results.length > 0) {
                		throw new IBIZException("The Initial Contribution has been processed already");
                	}            		
        		} else {
                	CLUEBuilder builder = new CLUEBuilder();        	
            		String[] ids = builder.contributionProductVersionsEnabled(productId, true);
            		StringBuilder list = new StringBuilder();
            		for (String id : ids) {
            			if (list.length() > 0)
            				list.append(",");
            			list.append(ModelSpecification.indexString(id));			
            		}
        			
                	JDBCData.QueryResult[] results = data.doQueryFromLookup("CPCLUPContrib", new String[] {"TypeCd", "ProductVersionIdRef"}, new String[] {"IN","IN"}, new String[] {"INITIAL", "PERIOD", list.toString()}, 1);
                	if (results.length > 0) {
                		throw new IBIZException("The Initial Contribution has been processed already for this product " + productId);
                	}            		
        		}
        	}
            
            processClaim(data, todayDt, todayTm, productId, transmitInd, regenInd);
                     
            data.commit();
            
            return rs;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
        finally {
        	try { data.rollback(); } catch(Exception e) { Log.error(e); }
        }
    }
    
    /** Delete the previous initial contribution for regeneration
     * @param data The data connection
     * @param productId The product id to delete, All will remove all Initial contribution records
     * @throws Exception when an error occurs
     */
    protected void deletePrevious(JDBCData data, String productId) 
    throws Exception {
    	JDBCData.QueryResult[] results = null;    	
    	if (productId.equals("All")) {
        	results = data.doQueryFromLookup("CPCLUPContrib", new String[] {"TypeCd"}, new String[] {"="}, new String[] {"Initial"}, 0);
    	} else {
        	CLUEBuilder builder = new CLUEBuilder();        	
    		String[] ids = builder.contributionProductVersionsEnabled(productId, true);
    		StringBuilder list = new StringBuilder();
    		for (String id : ids) {
    			if (list.length() > 0)
    				list.append(",");
    			list.append(ModelSpecification.indexString(id));			
    		}
    		JDBCLookup lookup = new JDBCLookup("CPCLUPContribLookup");
    		LookupKey lookupKey = lookup.addLookupKey("TypeCd", "INITIAL", JDBCLookup.LOOKUP_EQUALS);
    		lookupKey = lookup.addDelimitedListLookupKey("ProductVersionIdRef", list.toString(), ",");
    		lookupKey.setLookupType(JDBCLookup.LOOKUP_IN_LIST);
    		results = lookup.doLookup(data, 0);    		
    	}
    	
    	for (JDBCData.QueryResult result : results) {
    		ModelBean clueBean = new ModelBean("CPCLUPContrib");
    		data.selectModelBean(clueBean, result.getSystemId());
    		data.deleteModelBean(clueBean);
    	}    	
    }    
    /** Go through the system and find all Claim activity up to seven years
     * for the initial contribution.  The activity is the current view of the Claim.
     * No prior transactions are required to be reported for each Claim, just the current
     * transaction or state the Claim is in.
     * @param data The data connection
     * @param reportDt The report date
     * @param reportTm The report time
     * @param productId Product ID
     * @param transmitElectronicallyInd Indicator if this is to be transmitted electronically
     * @param regeneratedInd Indicator specifying if this is a regeneration of data
     * @throws Exception when an error occurs
     */
    private void processClaim(JDBCData data, StringDate reportDt, String reportTm, String productId, boolean transmitElectronicallyInd, boolean regeneratedInd)
    throws Exception {
    	ContributionFileGenerator fileGen = new ContributionFileGenerator();
		try {
	    	CLUEBuilder builder = new CLUEBuilder();
    		java.util.HashMap<String, Object> contextObjs = new HashMap();
    		contextObjs.put("ReportPeriodBeginDt", reportDt);
    		contextObjs.put("ReportPeriodEndDt", reportDt);
	    	
			if (!fileGen.setFile("CPContribution", "CLUPInitial", contextObjs)) {
				String msg = fileGen.getErrorMsg();
				fileGen = null;
				throw new Exception(msg);
			}
			CLUPGenerator generator = new CLUPGenerator();
			Record sacRec = generator.createSACHeader(reportDt, reportDt);
			fileGen.writeRecord(sacRec);
			
			String[] ids = builder.contributionProductVersionsEnabled(productId, false);
			StringBuilder list = new StringBuilder();
			for (String id : ids) {
				if (list.length() > 0)
					list.append(",");
				list.append(ModelSpecification.indexString(id));			
			}

			StringDate oldestLossDt = StringDate.advanceYear(reportDt, -7);
	    	
			JDBCLookup lookup = new JDBCLookup("ClaimLookup");
			LookupKey lookupKey = lookup.addLookupKey("TypeCd", "CLAIM", JDBCLookup.LOOKUP_EQUALS);			
			lookupKey = lookup.addDelimitedListLookupKey("ProductVersionIdRef", list.toString(), ",");
			lookupKey.setLookupType(JDBCLookup.LOOKUP_IN_LIST);
			lookupKey = lookup.addLookupKey("LossDt", oldestLossDt.toString(), JDBCLookup.LOOKUP_GREATER_THAN_OR_EQUALS);
			lookup.setSortType(JDBCLookup.SORT_ASCENDING);
    		JDBCData.QueryResult[] results = lookup.doLookup(data, 0);
			
	    	int totalRecords = 0;
	    	for (JDBCData.QueryResult result : results) {
	    		ModelBean claim = new ModelBean("Claim");
	    		data.selectModelBean(claim, result.getSystemId());
	    		
	    		//Ignore history claims
				if( claim.gets("HistoryModeInd", "").equalsIgnoreCase("Yes") ){
					continue;
				}
	    		
	    		// Since this will be reported right away, validate the report dates and validate the claim
	    		if (builder.isValidContribution(claim, reportDt) && builder.contributionRequired(claim)) {
	    			CLUPClaimContrib[] clueProperties = builder.processProperty(claim, reportDt);    
    				for( CLUPClaimContrib clueProperty : clueProperties) {
	    				clueProperty.setTypeCd("Initial");
                        ModelBean clueBean = BeanPojoMapper.toModelBean(clueProperty);
                        clueBean.setValue("ReportPeriodBeginDt", reportDt);
                        clueBean.setValue("ReportPeriodEndDt", reportDt);
                        clueBean.setValue("AddDt", reportDt);
                        clueBean.setValue("AddTm", reportTm);
                        clueBean.setValue("ProcessDt", reportDt);
                        clueBean.setValue("ProcessTm", reportTm);                        
                        clueBean.setValue("StatusCd", builder.STATUS_PROCESSED);
                        if (regeneratedInd)
                        	clueBean.setValue("RegeneratedInd", "Yes");
                        data.saveModelBean(clueBean);

                		// Log the Contribution data
                        Log.debug("CLUP Contribution Record : " + clueBean.readableDoc());
                                                
                        Record[] records = generator.createClaimSet(clueProperty);
                    	for (Record rec : records) {
                            fileGen.writeRecord(rec);                    		
                    	}
                    	totalRecords++;
    				}
	    		}
	    	}
	    	
        	if (totalRecords > 0) {
    	    	Record satRec = generator.createSATTrailer(totalRecords);
    	    	fileGen.writeRecord(satRec);
    	    	ContributionJobHelper.writeAplus(fileGen, reportDt, reportDt, "APlusInitial");
    	    	
    	    	// Process the file electronically if needed
    	    	if (transmitElectronicallyInd) {
    	    		
            	} else {
                    // Return info messages for batch reporting
                	super.addErrorMsg("ProcessingInfo", "The initial contribution file with a Report Date of " + reportDt.toString() + " was saved in the file " + fileGen.getFilename() , ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_INFO);
                	super.addErrorMsg("ProcessingInfo", "There were a total of " + totalRecords + " processed for this period." , ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_INFO);
    	    	}			        		
        	} else {
        		fileGen.cleanFile();
            	super.addErrorMsg("ProcessingInfo", "The initial contribution file with a Report Date of " + reportDt.toString() + " was not created because there were no records to process.", ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);        		
        	}
    	} catch (Exception e) {
    		if (fileGen != null)
    			fileGen.cleanFile();
    		throw e;
    	}
    	
    }
    
}
