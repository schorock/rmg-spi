package com.ric.claims.interfaces.choicepoint.contribution.clue.handler;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import com.iscs.claims.claim.Claim;
import com.ric.claims.interfaces.choicepoint.contribution.clue.CLUEBuilder;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.interfaces.choicepoint.contribution.ChoicePointContributionException;


public class CLUEProcess extends InnovationIBIZHandler {

    /** Creates a new instance of CLUEProcess
     * @throws Exception never
     */
    public CLUEProcess() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a Greeting
            Log.debug("Processing CLUEProcess...");
            ModelBean rs = this.getHandlerData().getResponse();
            JDBCData data = this.getHandlerData().getConnection();
            ModelBean responseParams = rs.getBean("ResponseParams");
            StringDate todayDt = DateTools.getStringDate(responseParams);
            String todayTm = DateTools.getTime(rs.getBean("ResponseParams"));
            
            ModelBean claim = rs.getBean("Claim");
            
            if( Claim.isClaimInHistoryMode(claim) ){
        		return rs;
        	}
            
            // Create the CLUE Contribution record if needed for this claim
            CLUEBuilder builder = new CLUEBuilder();
            builder.process(data, claim, todayDt, todayTm);                
            
            // Return the Response ModelBean
            return rs;
        }
        catch( ChoicePointContributionException ce ) {
        	try {
        		addErrorMsg("General", ce.getMessage(), ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
        	} catch (Exception e) {
                e.printStackTrace();
                throw new ServiceHandlerException(e);        		
        	}        	
            throw new IBIZException(ce);
        }
        catch( Exception e ) {
            e.printStackTrace();
            throw new ServiceHandlerException(e);
        }
    }	
}
