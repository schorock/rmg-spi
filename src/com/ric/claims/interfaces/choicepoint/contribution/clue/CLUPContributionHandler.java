package com.ric.claims.interfaces.choicepoint.contribution.clue;

import net.inov.tec.beans.ModelBean;

import net.inov.tec.date.StringDate;

import com.iscs.claims.interfaces.choicepoint.contribution.clue.entity.CLUPClaimContrib;

public interface CLUPContributionHandler extends com.iscs.claims.interfaces.choicepoint.contribution.clue.CLUPContributionHandler{

	public CLUPClaimContrib createPropertyContribution(ModelBean claim, StringDate todayDt, String propertyDamagedIdRef) throws Exception;	
	
}
