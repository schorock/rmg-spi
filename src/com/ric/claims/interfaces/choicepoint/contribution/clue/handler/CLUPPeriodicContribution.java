package com.ric.claims.interfaces.choicepoint.contribution.clue.handler;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;
import net.inov.tec.data.JDBCLookup;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

import com.ric.claims.interfaces.choicepoint.contribution.ContributionJobHelper;

import java.util.HashMap;

import com.iscs.claims.interfaces.choicepoint.contribution.clue.CLUEBuilder;
import com.iscs.claims.interfaces.choicepoint.contribution.clue.CLUPGenerator;
import com.iscs.claims.interfaces.choicepoint.contribution.clue.entity.CLUPClaimContrib;
import com.iscs.common.render.DynamicString;
import com.iscs.common.tech.bean.pojomapper.BeanPojoMapper;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.InnovationUtils;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.error.ErrorTools;
import com.ric.interfaces.choicepoint.contribution.ContributionFileGenerator;
import com.iscs.interfaces.utility.record.Record;

/** Create the periodic ChoicePoint Current Carrier Contribution.  The periodic contribution
 * consists of all Active and in-force policies that have had changes since the last 
 * periodic report date. 
 * 
 * @author allend
 *
 */
public class CLUPPeriodicContribution extends InnovationIBIZHandler {
	
	public CLUPPeriodicContribution() throws Exception {
	}
	
    /** Process the Periodic Contribution.  There are many permutations available here.
     * There are 6 parameters that will allow normal periodic contributions, regeneration of
     * previous data, and finally a re-run of a previous contribution file without altering
     * any data.
     * 
     * 1) The simplest option is to run without any parameters.  This will look to see if there
     * are any outstanding Contributions to be reported.  If so, those records will be retrieved
     * and processed into a contribution file for ChoicePoint processing.  The records are updated
     * to show which report period it belongs to. The Begin and End Dates are updated to show this.  
     * The Begin Date is the previous Report Period End Date + 1, and the End Date is the current
     * date.  This should be the default option most of the time, and will be option for an automatic
     * cron job or other OS scheduled job run for contributions.
     * 
     * 2) The second option is to re-run a previous contribution file.  If the Begin Date and End 
     * Date are filled out, then all Contributions that were processed for that period will be
     * picked up and re-processed into a Contribution file.  This option will be needed if the 
     * contribution file needs to be re-transmitted to ChoicePoint again because of 
     * communication problems.
     * 
     * 3) The third option is a regeneration of the Contribution data from current Claim data.
     * Use this option if one or more Policies were rejected, or if there were data fixes required.
     * To initiate this option, select the indicator to Regenerate Contributions, and possibly the
     * where clause options of Keys, Operators and Values.  If the where clause options are left blank,
     * then the previous Report Period is selected and all of the Policies within that period are 
     * regenerated.  Please note that the regenerated contributions are not put into the same contribution
     * file as before.  A new contribution file is created with the new Report Period.  The where clause
     * options should only be used if someone is familiar with SQL.  It allows searching of previous
     * contribution data to reprocess by selecting on the following key fields, ClaimNumber, ClaimType, ProcessDt, 
     * ReportPeriodBeginDt, and ReportPeriodEndDt.  
     * It is a breakdown of the where clause into the respective components, the field to search on, the 
     * operator to use for the field, and the value(s) of that field.  
     * 
     * Therefore a simple case would be
     * WHERE ClaimNumber = "ABC00100" would be translated into the 3 fields as:
     * KEYS : ClaimNumber
     * OPERATORS : =
     * VALUES : ABC00100
     * 
     * or if multiple fields are needed such as 
     * WHERE ReportPeriodBeginDt >= "20090101" and ReportPeriodEndDt <= "20090201" would be:
     * KEYS : ReportPeriodBeginDt,ReportPeriodEndDt
     * OPERATORS : >=,<=
     * VALUES : 20090101,20090201
     * 
     * With multiple fields, use the slash character as the delimiter
     * Most SQL operators are supported, such as IN, NOT IN, BETWEEN, etc as with the case of:
     * WHERE ClaimNumber IN ("ABC00100", "XYZ00200") would be:
     * KEYS : ClaimNumber
     * OPERATORS: IN
     * VALUES : ABC00100,XYZ00200
     * 
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process()
    throws ServiceHandlerException {
        JDBCData data = this.getConnection();
        try {            
            Log.debug("Processing CLUPPeriodicContribution...");
            
            ModelBean rs = this.getHandlerData().getResponse();
            ModelBean rq = this.getHandlerData().getRequest();
            AdditionalParams ap = new AdditionalParams(rq);
            
        	// Ensure the Errors bean exists in the response
        	ModelBean responseParams = getResponse().getBean("ResponseParams");
        	ModelBean errorsBean = responseParams.getBean("Errors");
        	if(errorsBean == null){
        		errorsBean = new ModelBean("Errors");
        		responseParams.addValue(errorsBean);
        	}
            
            boolean transmitInd = StringTools.isTrue(ap.gets("TransmitElectronicallyInd"));
        	
            ContributionJobHelper contribHelper = new ContributionJobHelper(rq, rs, false);

        	if (contribHelper.isRegenInd()) {
        		processClaims(data, contribHelper);
            }            
            
            if (!contribHelper.isRegenInd() && contribHelper.getBeginDt() != null && contribHelper.getEndDt() != null) {
            	boolean completed = reprocessFile(data, contribHelper.getBeginDt(), contribHelper.getEndDt(), transmitInd, contribHelper.getFileGen());
                if (!completed) {
                	this.addErrorMsg("General", "There were no matching Contributions for this reporting period beginning " + contribHelper.getReportBeginDt().toStringDisplay() + " and ending " + contribHelper.getReportEndDt().toStringDisplay() + ". The file was not created.", ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
                }
            } else {            	
                boolean completed = processContribution(data, contribHelper, transmitInd);            	
                if (!completed) {
                	this.addErrorMsg("General", "There were no matching Contributions for this reporting period beginning " + contribHelper.getReportBeginDt().toStringDisplay() + " and ending " + contribHelper.getReportEndDt().toStringDisplay() + ". The file was not created.", ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
                }
            }
            
            if (contribHelper.isTestInd()) {
            	data.rollback();
            } else {
            	data.commit();
            }                    	
            
            return rs;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
        finally {
        	try { data.rollback(); } catch(Exception e) { Log.error(e); }
        }
    }
    
    /** Go through the system and find all matching Contributions
     * to create the Report file
     * @param data The data connection
     * @param contribHelper The contribution job helper
     * @param transmitElectronicallyInd Indicator if this is to be transmitted electronically
     * @return true/false if periodic processing succeeded
     * @throws Exception when an error occurs
     */
    private boolean processContribution(JDBCData data, ContributionJobHelper contribHelper, boolean transmitElectronicallyInd)
    throws Exception {
    	// Do not process the file unless it is a full file regen or a testfile is not indicated
    	if ((contribHelper.isRegenInd() && (contribHelper.getBeginDt() == null || contribHelper.getEndDt() == null)) || contribHelper.isTestInd())
    		return true;

    	ContributionFileGenerator fileGen = contribHelper.getFileGen();    	
    	try {
        	CLUEBuilder builder = new CLUEBuilder();
    		CLUPGenerator generator = new CLUPGenerator();
    		StringDate beginDt = contribHelper.getReportBeginDt();
    		StringDate endDt = contribHelper.getReportEndDt();
    		Record sacRec = generator.createSACHeader(beginDt, endDt);
        	Log.debug("Sac Rec = " + sacRec.toString());
    		fileGen.writeRecord(sacRec);
    		
            String statusCd = ModelSpecification.indexString(builder.STATUS_NEW);
        	JDBCData.QueryResult[] results = null;
        	if (contribHelper.isRebuildFile()) {
        		JDBCLookup lookup = new JDBCLookup("CPCLUPContribLookup");
    			lookup.addLookupKey("TypeCd", "PERIOD", JDBCLookup.LOOKUP_EQUALS);
    			lookup.addLookupKey("AddDt", contribHelper.getTodayDt().toString(), JDBCLookup.LOOKUP_EQUALS);
    			lookup.addLookupKey("AddTm", contribHelper.getTodayTm(), JDBCLookup.LOOKUP_EQUALS);
    			lookup.addLookupKey("StatusCd", statusCd, JDBCLookup.LOOKUP_EQUALS);
    			lookup.setSortType(JDBCLookup.SORT_ASCENDING);
        		results = lookup.doLookup(data, 0);
        	}
        	else {
        		JDBCLookup lookup = new JDBCLookup("CPCLUPContribLookup");
    			lookup.addLookupKey("TypeCd", "PERIOD", JDBCLookup.LOOKUP_EQUALS);
    			lookup.addLookupKey("BookDt", beginDt.toString(), JDBCLookup.LOOKUP_GREATER_THAN_OR_EQUALS);
    			lookup.addLookupKey("BookDt", endDt.toString(), JDBCLookup.LOOKUP_LESS_THAN_OR_EQUALS);
    			lookup.addLookupKey("StatusCd", statusCd, JDBCLookup.LOOKUP_EQUALS);
    			lookup.setSortType(JDBCLookup.SORT_ASCENDING);
        		results = lookup.doLookup(data, 0);
        	}
    		
        	int totalRecords = 0;
        	for (JDBCData.QueryResult result : results) {        		
        		ModelBean contribution = new ModelBean("CPCLUPContrib");
        		data.selectModelBean(contribution, result.getSystemId());
        		ModelBean claim = new ModelBean("Claim");
        		data.selectModelBean(claim, contribution.gets("ClaimRef"));
        		
        		// Since this will be reported right away, validate the report dates and validate the Claim
        		if (builder.isValidContribution(contribution, endDt) && builder.contributionRequired(claim)) {
        			// Update the period dates
            		contribution.setValue("ReportPeriodBeginDt", beginDt);
            		contribution.setValue("ReportPeriodEndDt", endDt);
            		contribution.setValue("ProcessDt", contribHelper.getTodayDt());
            		contribution.setValue("ProcessTm", contribHelper.getTodayTm());
            		contribution.setValue("StatusCd", builder.STATUS_PROCESSED);
            		data.saveModelBean(contribution);

            		CLUPClaimContrib claimInfo = new CLUPClaimContrib();
        			BeanPojoMapper.populatePojo(claimInfo, contribution);
            		
                    Record[] records = generator.createClaimSet(claimInfo);
                	for (Record rec : records) {
                        fileGen.writeRecord(rec);                    		
                	}
                	totalRecords++;
        		}
        	}
        	
        	if (totalRecords > 0) {
            	Record satRec = generator.createSATTrailer(totalRecords);
            	fileGen.writeRecord(satRec);
            	java.util.HashMap<String, Object> contextObjs = new HashMap();
        		contextObjs.put("ReportPeriodBeginDt", beginDt);
        		contextObjs.put("ReportPeriodEndDt", endDt);
            	String fileName = InnovationUtils.getEnvSettingsLabel("APlusContribution","APlusPeriodic", "filename", "");
        		ModelBean party = new ModelBean("PartyInfo");
        		String  parsedFilename = DynamicString.render(new ModelBean[] {party}, contextObjs, fileName);
            	fileGen.copy(parsedFilename);
            	// Process the file electronically if needed
            	if (transmitElectronicallyInd) {
            	
            	} else {
                    // Return info messages for batch reporting
                	super.addErrorMsg("ProcessingInfo", "The periodic contribution file for the Report Period between " + beginDt.toString() + " to " + endDt.toString() + " was saved in the file " + fileGen.getFilename() , ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_INFO);
                	super.addErrorMsg("ProcessingInfo", "There were a total of " + totalRecords + " processed for this period." , ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_INFO);
            	}
            	return true;
        	} else {
        		fileGen.cleanFile();
            	super.addErrorMsg("ProcessingInfo", "The periodic contribution file for the Report Period between " + beginDt.toString() + " to " + endDt.toString() + " was not created because there were no records to process.", ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
        		return false;
        	}
        	
    	} catch (Exception e) {
    		fileGen.cleanFile();
    		throw e;
    	}
    }

    /** Go through the system and find all matching Contributions
     * to re-create the Report file
     * @param data The data connection
     * @param beginDt The report period begin date
     * @param endDt The report period end date
     * @param transmitElectronicallyInd Indicator if this is to be transmitted electronically
     * @return true/false if periodic processing succeeded
     * @throws Exception when an error occurs
     */
    private boolean reprocessFile(JDBCData data, StringDate beginDt, StringDate endDt, boolean transmitElectronicallyInd, ContributionFileGenerator fileGen)
    throws Exception {
    	try {
    		CLUPGenerator generator = new CLUPGenerator();
    		Record sacRec = generator.createSACHeader(beginDt, endDt);
        	Log.debug("Sac Rec = " + sacRec.toString());
    		fileGen.writeRecord(sacRec);
    		
        	JDBCLookup lookup = new JDBCLookup("CPCLUPContribLookup");
			lookup.addLookupKey("TypeCd", "PERIOD", JDBCLookup.LOOKUP_EQUALS);
			lookup.addLookupKey("ReportPeriodBeginDt", beginDt.toString(), JDBCLookup.LOOKUP_EQUALS);
			lookup.addLookupKey("ReportPeriodEndDt", endDt.toString(), JDBCLookup.LOOKUP_EQUALS);
			lookup.setSortType(JDBCLookup.SORT_ASCENDING);
			JDBCData.QueryResult[] results = lookup.doLookup(data, 0);
    		
        	int totalRecords = 0;
        	for (JDBCData.QueryResult result : results) {        		
        		ModelBean contribution = new ModelBean("CPCLUPContrib");
        		data.selectModelBean(contribution, result.getSystemId());

        		CLUPClaimContrib claimInfo = new CLUPClaimContrib();
    			BeanPojoMapper.populatePojo(claimInfo, contribution);
        		
                Record[] records = generator.createClaimSet(claimInfo);
            	for (Record rec : records) {
                    fileGen.writeRecord(rec);                    		
            	}
            	totalRecords++;        		
        	}
        	
        	if (totalRecords > 0) {
            	Record satRec = generator.createSATTrailer(totalRecords);
            	fileGen.writeRecord(satRec);
            	// Process the file electronically if needed
            	if (transmitElectronicallyInd) {
            	
            	} else {
                    // Return info messages for batch reporting
                	super.addErrorMsg("ProcessingInfo", "The periodic contribution file for the Report Period between " + beginDt.toString() + " to " + endDt.toString() + " was saved in the file " + fileGen.getFilename() , ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_INFO);
            	}
            	return true;
        	} else {
        		fileGen.cleanFile();
        		return false;
        	}
        	
    	} catch (Exception e) {
    		fileGen.cleanFile();
    		throw e;
    	}
    }
    
    /** Process the list of claims to regenerate contribution data from
     * @param data The data connection
     * @param contribHelper The Contribution Job Helper that validates and stores parameter information
     * @throws Exception when an error occurs
     */
    private void processClaims(JDBCData data, ContributionJobHelper contribHelper) throws Exception {
		JDBCData.QueryResult[] contribList = data.doQueryFromLookup("CPCLUPContrib", contribHelper.getKeyList(), contribHelper.getOpList(), contribHelper.getValueList(), 0);            	

    	for( JDBCData.QueryResult contribution : contribList ) {
        	ModelBean contrib = new ModelBean("CPCLUPContrib");
    		data.selectModelBean(contrib, contribution.getSystemId());
    		ModelBean claim = new ModelBean("Claim");
    		data.selectModelBean(claim, contrib.gets("ClaimRef"));
    		
    		CLUEBuilder builder = new CLUEBuilder();
            if( builder.contributionRequired(claim) ) {
            	CLUPClaimContrib[] claimInfos = builder.processProperty(claim, contribHelper.getTodayDt());            	
        		for(CLUPClaimContrib claimInfo : claimInfos) {
    				// Save the contribution record to the database for future reporting
    				ModelBean claimInfoBean = BeanPojoMapper.toModelBean(claimInfo);
    				claimInfoBean.setValue("StatusCd", builder.STATUS_NEW);
    				claimInfoBean.setValue("RegeneratedInd", "Yes");
    				claimInfoBean.setValue("AddDt", contribHelper.getTodayDt().toString());
    				claimInfoBean.setValue("AddTm", contribHelper.getTodayTm());
    				data.saveModelBean(claimInfoBean);
        		}
            }
    	}    			
    }
}
