package com.ric.claims.interfaces.choicepoint.contribution.clue;

import java.util.ArrayList;

import net.inov.mda.MDAOption;
import net.inov.mda.MDATable;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.date.StringDate;
import net.inov.tec.math.Money;
import net.inov.tec.obj.ClassHelper;
import com.iscs.claims.common.Reserve;
import com.ric.claims.interfaces.choicepoint.contribution.clue.CLUPContributionHandler;
import com.iscs.claims.interfaces.choicepoint.contribution.clue.entity.CLUPClaimContrib;
import com.iscs.claims.interfaces.choicepoint.contribution.clue.entity.CLUPClaimInfo;
import com.iscs.claims.transaction.PropertyDamaged;
import com.iscs.common.mda.Store;

public class CLUEBuilder extends com.iscs.claims.interfaces.choicepoint.contribution.clue.CLUEBuilder{

	
	public CLUPClaimContrib[] processProperty(ModelBean claim, StringDate todayDt)
	throws Exception {
		// Obtain the Contribution Class which will map Policy data into Contribution data
        String repositoryName = "::clue-codes::PropertyContribution::all";
        MDATable table = (MDATable) Store.getModelObject(repositoryName);
        MDAOption option = table.getOption("handler");
        String handler = option.getLabel();
        Object obj = ClassHelper.loadObject(handler);
        CLUPContributionHandler clupContributionHandler = (CLUPContributionHandler) obj;
        
        // Create Array List
        ArrayList<CLUPClaimContrib> array = new ArrayList<CLUPClaimContrib>();
        
        ModelBean[] propertyDamaged = claim.getAllBeans(("PropertyDamaged")); 
        String propertyDamagedIdRef = "";
        boolean singleRiskInd = hasSingleRisk(claim);
        boolean singleRiskReportedInd = false;
        for (ModelBean property : propertyDamaged) {
        	if (singleRiskReportedInd) {
        		break;
        	}
        	if(!singleRiskInd){
        		propertyDamagedIdRef = property.getId();
        		
    			PropertyDamaged damaged = new PropertyDamaged();
        		ModelBean risk = damaged.getPolicyRiskByPropertyDamagedRef(claim, propertyDamagedIdRef);
        		if (risk == null) {
        			continue; // Ignore Property Damaged that do not map to a defined PolicyRisk
        		}        		
        	} else {
        		singleRiskReportedInd = true;
        	}
			CLUEBuilder builder = new CLUEBuilder();
			if( !builder.isAutoCLUE(claim, propertyDamagedIdRef)){
				        
		        CLUPClaimContrib clupClaimContrib = clupContributionHandler.createPropertyContribution(claim, todayDt, propertyDamagedIdRef);
		        		        
				// Set the key fields and other non-contribution fields needed for System processing
		        clupClaimContrib.setClaimRef(claim.getSystemId());
		        clupClaimContrib.setTransactionNumber(claim.getValueInt("TransactionNumber"));
		        clupClaimContrib.setProductVersionIdRef(claim.gets("ProductVersionIdRef"));
		        clupClaimContrib.setStatusCd(STATUS_NEW);
		        
		        // Add to Array
		        array.add(clupClaimContrib);
		        
		        boolean hasLAE = false;
		    	
	    		ModelBean[] transactions = claim.getAllBeans("ClaimantTransaction");
	    		for( ModelBean transaction:transactions){
	    			ModelBean[] featureAllocations = transaction.findBeansByFieldValue("FeatureAllocation", "PropertyDamagedIdRef", propertyDamagedIdRef);
	    			for( ModelBean featureAllocation:featureAllocations){    				
						ModelBean[] reserveAllocations = featureAllocation.getBeans("ReserveAllocation");
						for( ModelBean reserveAllocation:reserveAllocations){
							String reserveTypeCd = Reserve.getReserveType(claim, reserveAllocation.gets("ReserveCd"));
							if (reserveTypeCd.equals("Adjustment") && !reserveAllocation.gets("PaidAmt").equals("") ){
								hasLAE = true;
			        		    break;
							}
						}
	    			}
	    		}		    			    			        		    		        
		    	
		        if(hasLAE) {
		        	CLUPClaimContrib clupClaimContribLAE = (CLUPClaimContrib) clupClaimContrib.clone();
		        	CLUPClaimInfo claimInfo = (CLUPClaimInfo) clupClaimContribLAE.getClaim().clone();
		        	claimInfo.setLossCauseCd("LAE");
		        	fillClaimAmount(claim, claimInfo);
		        	clupClaimContribLAE.setClaim(claimInfo);
		        	
		        	// Add to Array
		            array.add(clupClaimContribLAE);
		        }
			}
        }
        
		return array.toArray( new CLUPClaimContrib[array.size()] );
	}
	private void fillClaimAmount(ModelBean claim, CLUPClaimInfo claimInfo) throws Exception {
    	Money zeroAmt = new Money("0.00");
    	Money claimAmt = zeroAmt;    	
    	ModelBean[] reserves = claim.getAllBeans("Reserve");
    	for (ModelBean reserve : reserves) {
    		String reserveTypeCd = Reserve.getReserveType(claim, reserve.gets("ReserveCd"));
    		if (reserveTypeCd.equals("Adjustment")) {
    			Money paidAmt = new Money(reserve.gets("PaidAmt"));
    			if (!paidAmt.equals(zeroAmt)) {
    				claimAmt = claimAmt.add(paidAmt);
    			}
    		}
    	}
    	claimInfo.setClaimAmt(claimAmt.toString());
    }
	
}
