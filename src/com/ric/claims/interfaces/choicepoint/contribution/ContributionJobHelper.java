package com.ric.claims.interfaces.choicepoint.contribution;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceContext;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

import com.iscs.common.render.DynamicString;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.InnovationUtils;
import com.iscs.common.utility.StringTools;
import com.ric.interfaces.choicepoint.contribution.ContributionFileGenerator;

public class ContributionJobHelper {
	
	private boolean regenInd = false;
	private String[] keyList = null;
	private String[] opList = null;
	private String[] valueList = null;
	private StringDate beginDt = null;
	private StringDate endDt = null;
	private StringDate reportBeginDt = null;
	private StringDate reportEndDt = null;
	private boolean testInd = false;
	private StringDate todayDt = null;
	private String todayTm = null;
	private ContributionFileGenerator fileGen = null;

	public ContributionJobHelper (ModelBean request, ModelBean response, boolean isCLUEAuto) throws Exception {
        AdditionalParams ap = new AdditionalParams(request);
        todayDt = DateTools.getStringDate(response.getBean("ResponseParams"));
        todayTm = DateTools.getTime(response.getBean("ResponseParams"));   
        regenInd = StringTools.isTrue(ap.gets("RegenerateInd"));
        String keyStr = ap.gets("KeyList");
        String opStr = ap.gets("OperatorList");
        String valueStr = ap.gets("ValueList");
        String beginDate = ap.gets("BeginDt");
        String endDate = ap.gets("EndDt");
        testInd = StringTools.isTrue(ap.gets("TestInd"));
        String testFilename = ap.gets("TestFilename");      

        // If test file is to be created, validate the filename/path and make sure it does not exist 
        if (testInd) {
        	if (testFilename.equals("")) 
    			throw new IBIZException("The Test Filename must be filled out if the Test Indicator is set to Yes.");
        }
        
        if (beginDate.length() > 0) {
        	beginDt = new StringDate(beginDate);
        }
        if (endDate.length() > 0) {
        	endDt = new StringDate(endDate);
        }

		StringDate previousEndDt = getPreviousEndDt(ServiceContext.getServiceContext().getData(), isCLUEAuto);
        
        if (regenInd) {
        	// If the question fields are left blank, except the regen indicator, then the previous periodic contribution is regenerated
        	//     The Policies from the this contribution are used to regenerate the next periodic contribution 
        	// If the where fields are filled out, then parse them and obtain the list of Policies to regenerate from
        	
        	if (keyStr.equals("") && opStr.equals("") && valueStr.equals("")) {
        		if (previousEndDt == null) 
        			throw new IBIZException("There are no previous Contributions to regenerate from.");
        		reportBeginDt = StringDate.advanceDate(previousEndDt, 1);
        		
        		if (beginDt != null || endDt != null) {
        			if (beginDt == null || endDt == null) {
            			throw new IBIZException("If Begin Date or End Date is filled out with the Regenerate Indicator, both date fields must be filled out");            				
        			}
        			keyList = new String[] {"TypeCd", "ReportPeriodBeginDt", "ReportPeriodEndDt", "RegeneratedInd"};
        			opList = new String[] {"=", "=", "=", "<>"};
        			valueList = new String[] {"PERIOD", beginDt.toString(), endDt.toString(), "YES"};
        		}
        		
        		keyList = new String[] {"TypeCd", "ReportPeriodEndDt", "RegeneratedInd"};
        		opList = new String[] {"=", "=", "<>"};
        		valueList = new String[] {"PERIOD", StringDate.advanceDate(previousEndDt, 1).toString(), "YES"};
            	
        	} else {
        		List keySet = StringTools.split(keyStr, ",", "\"");
        		List opSet = StringTools.split(opStr, ",", "\"");
        		List valueSet = StringTools.split(valueStr, ",", "\"");
        		
        		if (keySet.size() != opSet.size() || keySet.size() != valueSet.size()) {
        			throw new IBIZException("The Key List, Operator List, and Values List must have the same number of parameters delimited by the character |");
        		}
        		
        		if (beginDt != null || endDt != null) {
        			throw new IBIZException("If the Key List, Operator List, and Value List are filled out, then the Begin Date and End Date cannot be filled out.");
        		}
        		
        		keyList = (String[]) keySet.toArray(new String[keySet.size()]);
        		opList = (String[]) opSet.toArray(new String[opSet.size()]);
        		valueList = (String[]) valueSet.toArray(new String[valueSet.size()]);            		
        	}
        }

        // Determine the Report Period Begin and End Dates
        if (endDt == null) 
        	reportEndDt = todayDt;
        else
        	reportEndDt = endDt;
        
        if (beginDt == null) {            	
    		if (previousEndDt == null) 
    			throw new IBIZException("There are no previous Contributions to base the Begin Date from.  Please enter a Begin Date for the beginning of the first reported contribution.");
    		reportBeginDt = StringDate.advanceDate(previousEndDt, 1);
        } else {
        	reportBeginDt = beginDt;
        }
        
        if (reportBeginDt.compareTo(todayDt) > 0) {
        	throw new IBIZException("The Contribution for the current period has been processed already.");
        }
        
        if (reportBeginDt.compareTo(reportEndDt) > 0) {
        	throw new IBIZException("The Report Period Begin Date of " + reportBeginDt.toStringDisplay() + " is greater than the Report Period End Date of " + reportEndDt.toStringDisplay());
        }
     
        // Validate the file and make sure it does not exist
    	fileGen = new ContributionFileGenerator();
		java.util.HashMap<String, Object> contextObjs = new HashMap();
		contextObjs.put("ReportPeriodBeginDt", reportBeginDt);
		contextObjs.put("ReportPeriodEndDt", reportEndDt);
    	
    	boolean validFile = false;
    	if (testFilename != null && testFilename.length() > 0) {
			validFile = fileGen.setFile(testFilename, contextObjs);
		} else {
			if (isCLUEAuto)
				validFile = fileGen.setFile("CPContribution", "CLUEPeriodic", contextObjs);
			else 
				validFile = fileGen.setFile("CPContribution", "CLUPPeriodic", contextObjs);				
    	}
		if (!validFile) {
			throw new IBIZException(fileGen.getErrorMsg());
		}
	}

	public boolean isRegenInd() {
		return regenInd;
	}

	public String[] getKeyList() {
		return keyList;
	}

	public String[] getOpList() {
		return opList;
	}

	public String[] getValueList() {
		return valueList;
	}

	public StringDate getBeginDt() {
		return beginDt;
	}

	public StringDate getEndDt() {
		return endDt;
	}

	public StringDate getReportBeginDt() {
		return reportBeginDt;
	}

	public StringDate getReportEndDt() {
		return reportEndDt;
	}

	public boolean isTestInd() {
		return testInd;
	}

	public StringDate getTodayDt() {
		return todayDt;
	}

	public String getTodayTm() {
		return todayTm;
	}

	public ContributionFileGenerator getFileGen() {
		return fileGen;
	}

	public boolean isRebuildFile() {
		if (beginDt != null & endDt != null)
			return true;
		else
			return false;
	}
	

	
    /** Obtain the previous Report Period End Date.  This is used to set the current Reporting Period Begin Date
     * @param data The data connection
     * @param isCLUEAuto indicator if CLUE auto
     * @return The previous report end date
     * @throws Exception when an error occurs
     */
    private StringDate getPreviousEndDt(JDBCData data, boolean isCLUEAuto) throws Exception {
    	String lookupTableName = null;
    	if (isCLUEAuto) 
        	lookupTableName = data.getLookupTableName(new ModelBean("CPCLUEContrib"));
    	else
        	lookupTableName = data.getLookupTableName(new ModelBean("CPCLUPContrib"));
    		
    	String sql = "SELECT MAX(LookupValue) ReportPeriodEndDt FROM " + lookupTableName + " WHERE LookupKey = 'ReportPeriodEndDt'";
    	ResultSet rSet = data.doSQL(sql);
    	String endDate = null;
    	if (rSet.next()) {
    		endDate = rSet.getString("ReportPeriodEndDt");
    		if (endDate == null || endDate.length() == 0)
    			return null;
    	}
    	return new StringDate(endDate);
    }
    
    /** This is used to copy the generated record into new aplus file
     * @param contributionFileGenerator Generated file
     * @param beginDt Beginning date of transaction
     * @param endDt End date of transaction
     * @param key KeyValue from env-settings
     * @throws Exception when an error occurs
     */
	public static void writeAplus(ContributionFileGenerator contributionFileGenerator, StringDate beginDt, StringDate endDt, String key){
		try {
	    	java.util.HashMap<String, Object> contextObjs = new HashMap();
			contextObjs.put("ReportPeriodBeginDt",beginDt);
			contextObjs.put("ReportPeriodEndDt", endDt);
	    	String fileName = InnovationUtils.getEnvSettingsLabel("APlusContribution", key, "filename", "");
			ModelBean party = new ModelBean("PartyInfo");
			String  parsedFilename = DynamicString.render(new ModelBean[] {party}, contextObjs, fileName);
			if( fileName.length() > 0 )
				contributionFileGenerator.copy(parsedFilename);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
   
}
