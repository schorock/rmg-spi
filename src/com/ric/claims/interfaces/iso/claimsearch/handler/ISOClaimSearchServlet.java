package com.ric.claims.interfaces.iso.claimsearch.handler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang.StringUtils;
import org.jdom.Element;

import com.iscs.claims.claim.Claim;
import com.iscs.claims.interfaces.iso.claimsearch.handler.ISOClaimSearchServletContextInitializer;
import com.iscs.common.business.datareport.DataReport;
import com.iscs.common.business.note.Note;
import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDAUrl;
import com.iscs.common.tech.log.Log;
import com.iscs.common.tech.web.servlet.ServletServiceContextInitializer;
import com.iscs.common.tech.web.servlet.WebPathServletHelper;
import com.iscs.common.utility.DateTools;
import com.iscs.workflow.Task;
import com.iscs.workflow.render.WorkflowRenderer;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceContext;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.data.bean.Transaction;
import net.inov.tec.date.StringDate;
import net.inov.tec.prefs.PrefsDirectory;
import net.inov.tec.security.InnovSecurityException;
import net.inov.tec.web.servlet.WebPathServletBase;
import net.inov.tec.xml.XmlDoc;

/** Handles the ISO Response data from ISO  
 *
 * @author Ayyanar Inbamohan
 */
public class ISOClaimSearchServlet extends WebPathServletBase {

	protected static final long serialVersionUID = 1L;
	
	
	/** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException for Servlet errors
     * @throws IOException for IO errors
     */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException {
    	
		try {
		   // Initialize the service context
    	ServletServiceContextInitializer ctxInitializer = new ISOClaimSearchServletContextInitializer();
        ctxInitializer.initialize(new Object[] {request, response});
        Log.start(ServiceContext.getServiceContext().getLogfile());
        
        // Log a message
        Log.debug("===== ENTER WEB TIER =====");            
        Log.debug("...processing iso claim search response begins");            
        		
		// Write the Response
		response.setContentType("text/html"); 
		response.setHeader("Cache-Control", "no-cache");
		
		String responseXML = "";
		
		// Get the Response XML data
		responseXML = request.getParameter("txtMatch");
		
		// Process the Asynchronous Response
		ModelBean claim = processISOResponseXML(responseXML);
		
		createClaimSearchNote(claim , responseXML);
            
		Log.info("txtMatch - (responseXML) = " + responseXML);
		Log.debug("...processing iso claim search response ends");
		
		//generate task
		if(claim != null) {
			generateISOClaimSearchTask(claim,"ISOClaimSearchResponseNotify");
		}
		
        }
        catch(InnovSecurityException e){
        	// Handle the security exception
            WebPathServletHelper.handleSecurityException(response, e);
        }
        catch( Exception e ) {
        	Log.error(e);
        	//Don't throw any exception after error logging so that ISO won't keep retrying again and again 
            //throw new ServletException(e.getMessage(),e);
        }
        catch( Error e ) {
        	// Catch all Errors too and rethrow. Allows us to catch OutOfMemoryError and other serious errors just to (try to) log them
        	Log.error(e);
        	//Don't throw any exception after error logging so that ISO won't keep retrying again and again 
        	//throw e;
        }
	    finally {
        	Log.debug("===== EXIT WEB TIER =====");
            
        	Log.finalizeLogs();
        	
        	try{
            	// Close the transaction
	        	ServiceContext.getServiceContext().closeTransaction();
	        	// Clear the service context
	        	ServiceContext.getServiceContext().clear();
	        }
        	catch(Exception e){
        		e.printStackTrace();
        	}
        }	    
	   
	}	

    /** Processes and Store the ISO Response XML 
     * @return the current response bean
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean processISOResponseXML(String responseXML) throws IBIZException, ServiceHandlerException {
        try {
            // Log a Greeting
            Log.debug("Processing ISOClaimSearchResponseHandler.processISOResponseXML...");
            
                       
            // find and get the claim based on the RQUID
            XmlDoc responseDoc = new XmlDoc(responseXML);
            String rqUID = responseDoc.getRootElement().getChildText("RqUID");
            String[] reqSplitData = rqUID.split("--");
            String claimSystemId = reqSplitData[0];
            String claimDataReportRef = reqSplitData[2];
            
            //Log response details
            Log.debug("Processing ClaimSystemId:"+reqSplitData[0]+" ClaimNo.:"+reqSplitData[1]+" DataReportRefId:"+reqSplitData[2]);            
            
            Transaction transaction = ServiceContext.getServiceContext().getTransaction();
            ModelBean dataReport = transaction.fetchBean("DataReport", claimDataReportRef);	
            ModelBean claim = transaction.fetchBean("Claim", claimSystemId);
			if( dataReport == null ) {
				// Data Report request not Found in the System
				Log.error("Unable to find Data Report bean for "+reqSplitData[0]+" ClaimNo.:"+reqSplitData[1]+" DataReportRefId:"+reqSplitData[2]);			
				return null;
			}else if (claim == null){
				
				// Claim not Found in the System
				Log.error("Unable to find Claim bean for "+reqSplitData[0]+" ClaimNo.:"+reqSplitData[1]+" DataReportRefId:"+reqSplitData[2]);			
				return null;
				
			}	
			
        	dataReport.setValue("StatusCd","Received");
        	dataReport.setValue("ReceivedDt",DateTools.getStringDate());
        	dataReport.setValue("ReceivedTm",DateTools.getTime());
        	dataReport.setValue("ResultCd","Success");
        	

        	try {
	            String responseXMLFileName = rqUID + ".xml";
	            String responseHTMLFileName = rqUID + ".html";
	            
	            // Create an Attachment Which Contains the ISO Claim Search Response in XML Format
	    		DataReport.createAttachment(dataReport, "xmlreport", ".txt", responseDoc.toPrettyString());
	    		
	            // Create the XML file from response XML.
	            writeXMLToFile(responseXML, responseXMLFileName);
	            
	            
	            // Generate the HTML file from XML file
	            generateHTML(dataReport,responseXMLFileName,responseHTMLFileName);
	            
	            
        	} catch(Exception e) {
        		dataReport.setValue("StatusCd","Error");
        		dataReport.setValue("ResultCd","Error generating HTML file");
        	}
            
	    	
	    	// handle error scenario
	    	String statusCd = responseDoc.getRootElement().getChild("MsgStatus").getChildText("MsgStatusCd");
	    	String statusDesc = responseDoc.getRootElement().getChild("MsgStatus").getChildText("MsgStatusDesc");
	    	
	    	if( statusCd.equals("Rejected") ) {
	    		dataReport.setValue("StatusCd","Reject");	    		
	    		dataReport.setValue("ResultCd",statusDesc);
	    	}
	    	
	    	//save the data report with updated status
            transaction.saveBean(dataReport);
            
            // Commit the data
            transaction.commit();
            
            return claim;
        }
        catch( IBIZException e ) {
            throw new IBIZException();
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
    
    /**
     * Get the Task id from the Product
     * @param claim
     * @param action
     * @return
     */
    protected String generateISOClaimSearchTask(ModelBean claim, String action) throws Exception {
    	String taskId= "";
    	 // Get Product Setup
        ModelBean productSetup = Claim.getClaimSetup(claim);

        // Get the Task template
        ModelBean productTasks = productSetup.getBean("ClaimTasks");
        ModelBean productTask = productTasks.getBean("ClaimTask","Action",action);
        
        if( productTask != null ) {
        	taskId = productTask.gets("TaskTemplateIdRef");
        }
        Transaction transaction = ServiceContext.getServiceContext().getTransaction();
        
        // Send Task to the Examiner to Notify that the ISO Claim Search response has been received...
        JDBCData data =  (JDBCData) transaction.getConnection().getConnectionObject();
        StringDate runDt = DateTools.getStringDate();
        ModelBean task = WorkflowRenderer.createTask(data, taskId, claim, runDt);
        Task.insertTask(data, task);
        
        // Commit the data
        transaction.commit();
        return taskId;
    }
    
    /**
     * Write the Response String XML into the XML file
     * @param responseXML
     * @param xmlFileName
     */
    protected void writeXMLToFile(String responseXML, String responseXMLFileName) {
    	try {
    		BufferedWriter out = new BufferedWriter(new FileWriter(PrefsDirectory.getLocation() + "upload/" + responseXMLFileName));
    		out.write(responseXML);
    		out.close();
    	}
    	catch (IOException e) { 
    		Log.error("Error writing ISO response into XML file " + responseXMLFileName);
    	}
    }
    
    /** 
     * TO get the Transformed HTML file's Storage Id
     * Stores the HTML file into the Storage Server
     * @param responseXML
     */
    public void generateHTML(ModelBean dataReport,String sourceXMLName, String targetHTMLName) throws Exception {
    	
    	//Generate html and xml file names
    	String outputFolderPath = PrefsDirectory.getLocation() + "upload/" ;
    	String htmlFileName = outputFolderPath + targetHTMLName;
    	String xmlFileName = outputFolderPath + sourceXMLName;
    	
    	//Locate xsl file and its systemId for transformation    	 	
    	String umol = "CLInterfaceISOClaimSearch::XMLtoHTMLtransform::null::CS_Xml_Output.xsl";
    	MDAUrl mdaUrl = (MDAUrl) Store.getModelObject(umol);
    	URL url = mdaUrl.getURL();
    	InputStream aInputStream = url.openStream();
    	String systemId = url.toExternalForm();   	
    	
    	//transform response xml to html format
    	TransformerFactory tFactory = TransformerFactory.newInstance();   
        Transformer transformer = tFactory.newTransformer(new javax.xml.transform.stream.StreamSource(aInputStream,systemId));
        FileOutputStream fos = new FileOutputStream(htmlFileName);
        StreamSource streamSource = new StreamSource(xmlFileName);
        transformer.transform(streamSource, new javax.xml.transform.stream.StreamResult(fos));
        
        //close the output stream
        fos.close();
        
        //Get html and xml files
        File htmlFile =  new File(htmlFileName);
        File xmlFile = new File(xmlFileName);
        
        // Create an Attachment Which Contains the ISO Claim Search Response in HTML Format
		DataReport.createAttachment(dataReport, "htmlreport", ".html", htmlFile.toURI().toURL());
        		
		//Delete both XML and HTML files after processing		 
		htmlFile.delete();
        xmlFile.delete();
        
        Log.info("HTML file created successfully..." + htmlFileName);  
          	
    }
    
    /**
     * 
     * @param claim
     * @param responseXML
     * @throws Exception
     */
    public void createClaimSearchNote(ModelBean claim, String responseXML) throws Exception {
		Log.debug("Starting to create note ....." );
		try{
			Transaction transaction = ServiceContext.getServiceContext().getTransaction();
			XmlDoc responseDoc = new XmlDoc();	
		    responseDoc.loadDoc(responseXML);
			List<Element> fileNumbers = responseDoc.selectElementArray("//ClaimsOccurrence/ItemIdInfo/AgencyId");
			
			if(fileNumbers != null && !fileNumbers.isEmpty()){
				String[] fileNos = new String[fileNumbers.size()];
				for (int i =0; i < fileNumbers.size(); i++){
					fileNos[i] = fileNumbers.get(i).getValue();				
				}
				
				String fileNumberCSV = StringUtils.join(fileNos, ", ");
				Log.debug("	created note with ISO File Numbers				===>             " +  fileNumberCSV);
				if(StringUtils.isNotBlank(fileNumberCSV)){
					ModelBean newNote = Note.create("ClaimNote0018", claim, null, null, "");
					newNote.setValue("StickyInd", "Yes");
					newNote.setValue("Description",  "ISO Claim file numbers :  "+ fileNumberCSV);
					newNote.setParentBean(claim);
					transaction.saveBean(newNote);
					transaction.saveBean(claim);
					transaction.commit();
				}
				
			}		

		}catch(Exception  e){
			Log.error("Note creation failed : "+ e.getMessage());
		}
			
	}
	
}