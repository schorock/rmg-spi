package com.ric.claims.interfaces.xact;

import com.innovextechnology.service.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.error.ErrorTools;

import net.inov.biz.server.ServiceContext;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;
import net.inov.tec.data.JDBCLookup;

/**
 * Manages the xact listener processor bean. 
 * @author ssoohoo
 *
 */
public class XactListenerProcessor {
	
	private ModelBean record; 
	public static final String ST_PROCESSED = "Processed";
	public static final String ST_UNPROCESSED = "Unprocessed";
	public static final String XMATCH_FILENAME = "ZIP Xact File Name";
	public static final String XMATCH_FILENAME_HASH = "ZIP Xact File Name against DB";
	public static final String XMATCH_ORIGINAL_TX_ID = "XML Original Transaction Id";
	public static final String XMATCH_XML_TX_ID = "XML Transaction Id";
	public static final String XMATCH_XML_COVLOSS_CLAIM_NUMBER = "XML Coverage Loss Node";
	public static final String XMATCH_XML_PROJINFO_CLAIM_NUMBER = "XML Project Info Node";
	public static final String XMATCH_XML_TYPELOSS_CLAIM_NUMBER = "XML Type of Loss Node";
	public static final String BEAN_NAME = "XactListenerProcessor";
	public static final String LOOKUP_TABLE_NAME = "XactListenerProcessorLookup";
	public static final String NO_ID = "None";
	
	public XactListenerProcessor(String fileName, String logFileName) throws ModelBeanException, Exception {
		if(fileName == null || fileName.length() == 0)
			throw new Exception("XactListenerProcessor cannot create a record for a zip file of no name");
		
		record = new ModelBean(BEAN_NAME);
		record.nextUid();
		record.setValue("ZipFileName",fileName);
		record.setValue("StatusCd","Active");
		setLogFile(logFileName);
		
		ModelBean errors = new ModelBean("Errors");
		errors.setValue("TypeCd","Client");
		errors.nextUid();
		record.addValue(errors);
	}
		
	public void setLogFile(String logFile) throws ModelBeanException {
		record.setValue("LogFile", logFile);
	}
	
	public void setProcessingDetails(String status) {
		try {
			record.setValue("ProcessedDt",DateTools.getDate());
			record.setValue("ProcessedTm",DateTools.getTime());
			record.setValue("ProcessingStatusCd",status);			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setFileSuccessfullyMoved(boolean value) {
		try {
			record.setValue("FileTransferStatusCd", value ? "Yes" : "No");		
		} catch (Exception e) {
			e.printStackTrace();
			Log.debug("Problem with setting file moved indicator");
		}
	}
	
	public void addXactMatchCriteria(String typeCd, String value, ModelBean claim) throws ModelBeanException, Exception {
		addXactMatchCriteria(typeCd, value, (claim == null) ? null : claim.getSystemId());
	}

	
	public void addXactMatchCriteria(String typeCd, String value, String claimRef) throws ModelBeanException, Exception {
		
		//if no value was found for this type, then don't record anything because nothing was there
		if(value == null || value.length() == 0) {
			addLogMessage("NoMatch", typeCd + " value not found in zip file");
			return; 
		}
		
		ModelBean x = new ModelBean("XactMatchCriteria");
		x.nextUid();
		x.setValue("TypeCd",typeCd);
		x.setValue("Value", value);
		if(claimRef == null || claimRef.length() == 0) {
			x.setValue("MatchInd","No");
		} else {
			x.setValue("MatchInd","Yes");
			record.setValue("ClaimRef", claimRef);
		}
		record.addValue(x);
	}
	
	public void addXactAttachmentData(String fileName) throws Exception {
		addXactAttachmentData(fileName, "","","");
	}
	
	public void addXactAttachmentData(String fileName, String claimSystemId, String storageFileId, String attachmentReferenceId) throws Exception {
		ModelBean link = new ModelBean("LinkReference");
		link.nextUid();
		link.setValue("SystemIdRef",claimSystemId);
		link.setValue("ModelName","Claim");
		link.setValue("Description","Attachment " + fileName + " " + storageFileId);
		link.setValue("IdRef",attachmentReferenceId);
		link.setValue("Status","Active");
		record.addValue(link);
	}

	public void addXactNoteData(String claimSystemId, String noteReferenceId) throws Exception {
		ModelBean link = new ModelBean("LinkReference");
		link.nextUid();
		link.setValue("SystemIdRef",claimSystemId);
		link.setValue("ModelName","Claim");
		link.setValue("Description","Note");
		link.setValue("IdRef",noteReferenceId);
		link.setValue("Status","Active");
		record.addValue(link);
	}

	public boolean saveBean(JDBCData data) {
		try {
			record.setValue("ReplacedByRef",NO_ID);
			BeanTools.saveBean(record, data);
			//if file was processed before, then set all those records to deleted
			deleteExistingDuplicateRecords(record.gets("ZipFileName"), record.getSystemId());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * A file may be processed multiple times due to error. When file is processed, find the 
	 * older record and set the replaced by system id field and mark the bean as deleted,
	 * as the results don't apply. This should only occur if the file was not processed before
	 * and is being submitted for processing another time. 
	 * @param fileName
	 * @param systemId
	 * @throws Exception
	 */
	private void deleteExistingDuplicateRecords(String fileName, String systemId) throws Exception {
		if(fileName == null || fileName.length() == 0)
			return; 
		JDBCData data = ServiceContext.getServiceContext().getData();
		JDBCLookup lookup = new JDBCLookup(LOOKUP_TABLE_NAME);
		lookup.addLookupKey("ZipFileName", ModelSpecification.indexString(fileName), JDBCLookup.LOOKUP_EQUALS);
		lookup.addLookupKey("StatusCd", "ACTIVE", JDBCLookup.LOOKUP_EQUALS);
		lookup.addLookupKey("ReplacedByRef", ModelSpecification.indexString(NO_ID), JDBCLookup.LOOKUP_EQUALS);
		
		JDBCData.QueryResult[] results = lookup.doLookup(data, 1);
		if(results != null && results.length > 0) {
			for(JDBCData.QueryResult thisRecord : results) {
				String thisSysId = thisRecord.getSystemId();
				if(!thisSysId.equals(systemId)) {
					ModelBean oldRecord = new ModelBean(BEAN_NAME);
					data.selectModelBean(oldRecord, thisSysId);
					oldRecord.setValue("ReplacedByRef",systemId);
					oldRecord.setValue("StatusCd","Deleted");
					data.saveModelBean(oldRecord);
				}
			}
		}
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		try {
			return record.toPrettyString();			
		} catch (Exception e) {
			e.printStackTrace();
			return ""; 
		}
	}
	
	/**
	 * Find the claim system id based on previous files which were processed through the system. 
	 * Since keys are stored only if claim match is found, then there should never be the case that the claim ref is blank value. 
	 * @param data
	 * @param transactionId
	 * @return
	 */
	public static String getClaimRefFromXactListenerProcessorRecords(JDBCData data, String transactionId) {
		if(transactionId == null || transactionId.equals(""))
			return null; 
		
		try {
			JDBCLookup lookup = new JDBCLookup(LOOKUP_TABLE_NAME);
			lookup.addLookupKey("XactTransactionId", ModelSpecification.indexString(transactionId), JDBCLookup.LOOKUP_EQUALS);
			JDBCData.QueryResult[] results = lookup.doLookup(data, 1);
			if(results == null || results.length == 0)
				return null;
			ModelBean xactRecord = new ModelBean(BEAN_NAME);
			data.selectModelBean(xactRecord, results[0].getSystemId());
			return xactRecord.gets("ClaimRef");
		} catch (Exception e) {
			e.printStackTrace();
			return null; 
		}
	}
	
	public void addLogMessage(String name, String msg) throws ModelBeanException, Exception {
		ModelBean error = new ModelBean("Error");
		error.setValue("Name",name);
		error.setValue("Msg",msg);
		error.setValue("Severity", ErrorTools.SEVERITY_INFO);
		record.getBean("Errors").addValue(error);
	}
}
	
