package com.ric.claims.interfaces.xact;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import com.iscs.common.tech.log.Log;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.KeyGenerator;
import net.inov.tec.data.LookupKey;

public class GKXactListenerProcessor implements KeyGenerator {
    

	public LookupKey[] generateKeys(ModelBean bean) throws Exception {
        Log.debug("Generate Keys...");
                
        //log all matched xact transaction ids if a claim match was found
        if(!bean.gets("ClaimRef").equals("")) {
            ModelBean[] xactMatchCriterias = bean.getAllBeans("XactMatchCriteria");
            List<ModelBean> list = Arrays.asList(xactMatchCriterias);
            List<LookupKey> keyArray = list
            		.stream()
            		.filter(matchCriteria -> { try { return (matchCriteria.gets("TypeCd").contains("Transaction") || matchCriteria.gets("TypeCd").contains("File Name") ); } catch (Exception e1) { return false; } })
            		.map(matchCriteria -> { try { return new LookupKey("XactTransactionId", ModelSpecification.indexString(matchCriteria.gets("Value"))); } catch (Exception e2) { return null; }} )
            		.collect(Collectors.toList());
            
            // Return key/value pairs to the called
    		LookupKey[] keys = keyArray.toArray( new LookupKey[keyArray.size()] );
            return keys;        	
        }
        return new LookupKey[0];
	}

	public String getModelName() {
		return "XactListenerProcessor";
	}
}
