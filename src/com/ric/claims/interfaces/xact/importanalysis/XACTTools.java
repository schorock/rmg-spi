package com.ric.claims.interfaces.xact.importanalysis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import com.iscs.common.business.provider.render.ProviderRenderer;
import com.iscs.common.business.datareport.DataReport;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;

public class XACTTools {
	
	public static final String[] XACT_ADJUSTER_FIELDS = {"AssignedOther1","AssignedOther2","AssignedOther3","AssignedOther4"};

	
	
	/**
	 * Should a report be generated for this particular report.
	 * Made with concept of adding a checkbox later. 
	 * @param propDamaged
	 * @return
	 */
	public static boolean sendReportForPropertyDamaged(ModelBean propDamaged) {
		try {
			return (StringTools.isTrue(propDamaged.gets("XactInd"))); 			
		} catch (ModelBeanException e1) {
			return false; 
		}
	}
	
	public static List<ModelBean> getProvidersForXactAssignment(ModelBean claimant) {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		ArrayList<ModelBean> providers = new ArrayList<ModelBean>(); 
		
		//default is the assigned examiner 
		try {
			ModelBean claim = claimant.getParentBean();
			addProviderToMap(claim.gets("AdjusterProviderRef"), map, providers);					
		} catch (Exception e) {
			Log.debug("skipping provider in field = AdjusterProviderRef");
		}
		
		//add data reports for all other adjuster fields 
		for (String adjusterFieldName: XACT_ADJUSTER_FIELDS) {
			try {
				String providerRef = claimant.getBean("AssignedProvider","AssignedProviderTypeCd",adjusterFieldName).gets("ProviderRef");
				addProviderToMap(providerRef, map, providers);
			} catch (Exception e) {
				Log.debug("skipping provider in field = " + adjusterFieldName);
			}
		}
		return providers; 
	}
	
	private static void addProviderToMap(String systemId, Map<String, Boolean> map, ArrayList<ModelBean> providers) throws ModelBeanException, Exception {
		ModelBean provider = new ModelBean("Provider");
		if(!systemId.equals("")) {
			provider = ProviderRenderer.getProviderBySystemId(systemId);
			if(provider != null && !provider.getSystemId().equals("")) {
				if(!map.containsKey(provider.getSystemId())) {
					map.put(provider.getSystemId(), true);
					providers.add(provider);
				}
			}
		}				
	}

}
