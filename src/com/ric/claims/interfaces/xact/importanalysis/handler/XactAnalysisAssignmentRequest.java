package com.ric.claims.interfaces.xact.importanalysis.handler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import net.inov.biz.server.ServiceContext;
import net.inov.tec.beans.Helper_Beans;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.file.FileIO;
import net.inov.tec.xml.XmlDoc;

import org.jdom.Element;
import org.jdom.xpath.XPath;

import com.ric.insurance.product.RICProductRuleTransformer;

import com.innovextechnology.service.Log;
import com.iscs.common.business.datareport.DataReport;
import com.iscs.common.business.provider.Provider;
import com.iscs.common.render.DateRenderer;
import com.iscs.common.render.DynamicString;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.tech.storage.StorageFile;
import com.iscs.common.tech.storage.StorageServerBase;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.InnovationUtils;
import com.iscs.common.utility.bean.render.BeanRenderer;
import com.iscs.common.utility.io.FileTools;
import com.iscs.uw.policy.PM;
import com.iscs.workflow.Task;

/** XactAnalysisImport 
 * 
 * @author monicaa
 *
 */
public class XactAnalysisAssignmentRequest {
	protected static final String TO_IBIZ_RULE_TYPE = "TransformIBIZRsToXactRq";
    File file = null;
    ZipOutputStream zos = null;

	/** Do Not Allow Public Initialization
	 */
	private XactAnalysisAssignmentRequest() {	
	}

	/** Gets the Singleton Instance of ISOClaimSearch
	 * @return Instance
	 */
	public static XactAnalysisAssignmentRequest getInstance() {
		return Holder.instance;
	}
	
	/** Thread-Safe Holder Class Singleton Design Pattern Does Not Require Synchronization
	 * and Avoids Using the Synchronized Double-Check Method Which is Broken
	 */
	private static class Holder {
		// XactAnalysisAssignmentRequest Instance
		public static final XactAnalysisAssignmentRequest instance = new XactAnalysisAssignmentRequest();
	}
	
	/** Send Claim details to XACT
	 * @param ModelBean claim, ModelBean exportCriteria, ModelBean dataReport,  ModelBean claimSetup 
	 * @throws Exception if an Error Occurred Connecting to XACT
	 */
	public boolean doXactAnalysisAssignmentRequest(ModelBean claim, ModelBean claimant, ModelBean claimSetup, ModelBean provider) throws Exception {
		XmlDoc xactResponse = new XmlDoc();
		try {
			JDBCData data = ServiceContext.getServiceContext().getData();
			String claimNumber = claim.gets("ClaimNumber");
			XmlDoc transformed = new RICProductRuleTransformer().claimRuleTransform(claimSetup, TO_IBIZ_RULE_TYPE, claim ,claimant, provider);
			String transformedXML = transformed.toPrettyString();
			Log.debug("XACT Request xml => " + transformed.toPrettyString());
			Log.debug("XACT Request Ends...");
			String directoryPath = DynamicString.render(new ModelBean("Company"), InnovationUtils.getEnvironmentVariable("Xact", "OutboundDirectoryPath", ""));
	   		String dailyFilename = InnovationUtils.getEnvironmentVariable("Xact", "OutboundDailyFilename", "");
	   		String zipFileName = "RockinghamGroup_" + DateRenderer.getStringDate() + DateRenderer.getTime("HHmmss") + ".zip";
			xmlFileGeneration(claimNumber, transformedXML, directoryPath, dailyFilename,zipFileName);
			pdfExtraction(data, claim, claimNumber, directoryPath, zipFileName);
			claimant.setValue("isXactSuccess", "Yes");
			// Return the Ack status
			return true;
		} catch( Exception e ) {
            Log.error("XACT request might have failed or unable to parse XACT response",e);
            return false;
        }
	}
	
	public void xmlFileGeneration(String claimNumber, String transformedXML, String directoryPath, String dailyFilename, String zipFileName) throws Exception {
   		FileTools.createPath( directoryPath ); 
   		String fullPathName = directoryPath + dailyFilename;
   		File saveFile = new java.io.File(fullPathName);
   		StringBuilder sb = new StringBuilder();
   		sb.append(transformedXML);
   		Charset charset = Charset.forName("UTF-8"); 
		CharsetEncoder encoder = charset.newEncoder(); 
		FileOutputStream fos = new FileOutputStream(saveFile,false);
		FileChannel channel = fos.getChannel();
		try {
			channel.write(encoder.encode(CharBuffer.wrap(sb.toString().toCharArray())));
		} 
		finally {
			if (channel!=null)
				channel.close();
			if (fos!=null)
				fos.close();
		}
		zipFileGeneration(claimNumber, directoryPath, dailyFilename,zipFileName);
		File file = new File(fullPathName);
		file.delete();
	}
	
	public void zipFileGeneration( String claimNumber, String directoryPath, String dailyFilename, String zipFileName) throws Exception {
		String fullPathName = directoryPath+dailyFilename;
        boolean check = new File(directoryPath+zipFileName).exists();
        if( !check ){
            file = new java.io.File(directoryPath+zipFileName);
            zos = new ZipOutputStream(new FileOutputStream(file));
        }
        ZipEntry zipEntry = new ZipEntry(dailyFilename);
        zos.putNextEntry(zipEntry);
        byte[] zos1 =  Files.readAllBytes(Paths.get(fullPathName));
        zos.write(zos1, 0, zos1.length);
        if( check ){
			zos.closeEntry();
			zos.close();
			File oldFile = new File(directoryPath+zipFileName);
	        File newFile = new File(directoryPath+zipFileName+".xml");
	        oldFile.renameTo(newFile);
        }
		
	}
	
	public void pdfExtraction(JDBCData data, ModelBean claim, String claimNumber, String directoryPath, String zipFileName) throws Exception {	
		
		try {
	        StringDate lossdt = claim.getDate("LossDt");
	        String policyRef = claim.getBean("ClaimPolicyInfo").gets("PolicyRef");
			ModelBean policyBean = getPolicyBean(policyRef,data);
			ModelBean basicPolicy = policyBean.getBean("BasicPolicy");
			String policyNumber = basicPolicy.gets("PolicyNumber");
			String transactionCode = basicPolicy.gets("TransactionCd");
			String storageId="";
			
			if(StringRenderer.in(transactionCode, "New Business,Rewrite-New")){	
				ModelBean  outputbean = policyBean.findBeanByFieldValue("Output", "OutputTemplateIdRef", "New Business Declaration");						
				ModelBean opbean = outputbean.findBeanByFieldValue("OutputItem", "ItemName", "Insured New Business Package");
				storageId= opbean.gets("FormURL");
			} else if(StringRenderer.in(transactionCode, "Renewal,Renewal Changes")){	
				ModelBean  outputbean = policyBean.findBeanByFieldValue("Output", "OutputTemplateIdRef", "Renewal Declaration");						
				ModelBean opbean = outputbean.findBeanByFieldValue("OutputItem", "ItemName", "Insured Renewal Declaration");
				storageId= opbean.gets("FormURL");
			} else if(StringRenderer.in(transactionCode, "Endorsement,Change Date")){	
				ModelBean tempPolicYbean = getTransactionHistryBean(basicPolicy, lossdt, policyBean);
				ModelBean tempBasicPolicyBean =  tempPolicYbean.getBean("BasicPolicy");
				String transactionNum = tempBasicPolicyBean.gets("TransactionNumber");
				ModelBean outputbean = tempPolicYbean.findBeanByFieldValue("Output", "TransactionNumber",transactionNum );
				ModelBean opbean = outputbean.findBeanByFieldValue("OutputItem", "ItemName", "Insured Endorsement Package");
				storageId= opbean.gets("FormURL");	
			}
			StorageServerBase storageServerBase = new StorageServerBase();                                                            
            StorageFile storageFile = storageServerBase.retrieveFile(storageId);
            String filename = storageFile.getFilename();
            String filepath = storageFile.getFilePath();                                                            
            filepath = "file:"+ filepath;
            URL url = new URL(filepath);
            String fullPathName = directoryPath + filename;
            String newPathName = directoryPath +  policyNumber + ".pdf";
            File outfile = new File(fullPathName);
            FileTools.copyFile(url, outfile);
            File oldFile = new File(fullPathName);
            File newFile = new File(newPathName);
            oldFile.renameTo(newFile);
            zipFileGeneration(claimNumber, directoryPath, policyNumber +".pdf" ,zipFileName);
            File file = new File(newPathName);
            file.delete();

        }
        catch( Exception e ) {
	            e.printStackTrace();
        }
	}

	public ModelBean getPolicyBean(String policyRef , JDBCData data )
	{		
		ModelBean policy = null;
		try {
			policy = new ModelBean("Policy");
			data.selectModelBean(policy, policyRef);
		}  catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    
		
		return policy;
	}

	public ModelBean getTransactionHistryBean(ModelBean basicPolicybean,StringDate lossDate,ModelBean policyBean){	
		ModelBean tempPolicyBean = null;
		try {
			tempPolicyBean = policyBean.cloneBean();
			ModelBean[] transactionBeans = basicPolicybean.getBeans("TransactionHistory");			
			transactionBeans=  BeanRenderer.sortBeansBy(transactionBeans, "TransactionEffectiveDt", "Descending");
			 
			for(ModelBean tranHistBean:transactionBeans ){
			if(tranHistBean.gets("TransactionCd").equalsIgnoreCase("Endorsement")){
				if(lossDate.compareTo(tranHistBean.getDate("TransactionEffectiveDt"))>0 || lossDate.compareTo(tranHistBean.getDate("transactionEffectiveDt"))!=0)	{			
				int transactionNumber = tranHistBean.getValueInt("TransactionNumber");
				PM.getPolicyAsOfTransaction(tempPolicyBean, transactionNumber);
				break;
					}
				}
			}	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tempPolicyBean;
		
	}
}