/*
 * XactRenderer.java
 *
 */

package com.ric.claims.interfaces.xact.importanalysis.render;


import com.iscs.common.render.Renderer;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.tech.log.Log;

import net.inov.tec.beans.ModelBean;
/** Rendering Tools for the Xact
 *
 * @author  allend
 */
public class XactRenderer implements Renderer{

    public static final String ZERO = "0";


    public XactRenderer() {
    }
    
    public String getContextVariableName() {
         return "XactRenderer";
    }
	
    public static String formatDeductible(String value) {
    	return formatDeductible(value, ZERO);
    }
    
    public static String formatDeductible(String value, String defaultVal) {
    	if(value == null || value.equals("") || !StringRenderer.isNumeric(value))
    		return defaultVal;
    	return value; 
    }
    
    public static String getDeductibleForXact(ModelBean claim, String coverageCd, String defaultValue) {
    	try {
	    	ModelBean claimPolicyLimit = claim.getBean("ClaimPolicyInfo").getBean("ClaimPolicyLimit","CoverageCd",coverageCd);
	    	if(claimPolicyLimit != null) {
	    		String ded = claimPolicyLimit.gets("Deductible");
	    		return formatDeductible(ded, defaultValue);
	    	}
    	} catch (Exception e) {
    		Log.debug("Unable to find deductible for " + coverageCd);
    	}
    	return defaultValue; 
    }
    

	
}
