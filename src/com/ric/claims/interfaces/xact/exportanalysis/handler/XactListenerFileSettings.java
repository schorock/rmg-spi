package com.ric.claims.interfaces.xact.exportanalysis.handler;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.iscs.common.render.DynamicString;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.InnovationUtils;

import net.inov.tec.beans.ModelBean;

public class XactListenerFileSettings {
	
	private File inboundDir = null;
	private File archiveDir = null;
	private File unprocessedDir = null;
	
	/**
	 * @return the inboundDir
	 */
	public File getInboundDirectory() throws Exception {
		if (this.inboundDir != null) {
			return this.inboundDir;
		}
		
		String inboundDirStr = DynamicString.render(new ModelBean("Company"), InnovationUtils.getEnvironmentVariable("Xact","inboundXactDailyFeedDirectoryPath", ""));
		this.inboundDir = new File(inboundDirStr);
		
		if (!this.inboundDir.exists()) {
			Log.error("Xact inbound directory not found - " + inboundDirStr);
			throw new Exception("Xact inbound directory not found - " + inboundDirStr);
		}
		
		return this.inboundDir;
	}
	
	public String getInboundDirectoryPath() throws Exception {
		return getInboundDirectory().getPath();
	}

	/**This archive directory is mainly for unzipping the files and let the files to be processed
	 * @return the archiveDir
	 */
	public File getArchiveDirectory() throws Exception {
		
		SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = sdfSource.format(new Date());
		String archiveDirStr = DynamicString.render(new ModelBean("Company"),InnovationUtils.getEnvironmentVariable("Xact","backup_inboundXactDailyFeedDirectoryPath", ""));
		archiveDirStr += dateStr;
		this.archiveDir = new File(archiveDirStr);
		
		try {
			if (!this.archiveDir.exists()) {
				this.archiveDir.mkdirs();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			Log.error("Unable to locate and/or create archive directory - " + archiveDirStr, ex);
			throw new Exception("Unable to locate and/or create archive directory - " + archiveDirStr, ex);
		}
		
		return this.archiveDir;
	}
	
	public String getArchiveDirectoryPath() throws Exception {
		return getArchiveDirectory().getPath();
	}

	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public File getUnprocessedDirectory() throws Exception {
		
		String unprocessedDirStr = DynamicString.render(new ModelBean("Company"),InnovationUtils.getEnvironmentVariable("Xact","unprocessedXactDailyFeedDirectoryPath", ""));
		this.unprocessedDir = new File(unprocessedDirStr);
		
		try {
			if (!this.unprocessedDir.exists()) {
				this.unprocessedDir.mkdirs();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			Log.error("Unable to locate and/or create unprocessed directory - " + unprocessedDirStr, ex);
			throw new Exception("Unable to locate and/or create unprocessed directory - " + unprocessedDirStr, ex);
		}
		
		return this.unprocessedDir;
	}
	
	public String getUnprocessedDirectoryPath() throws Exception {
		return getUnprocessedDirectory().getPath();
	}
	
	/**This archive directory is mainly for unzipping the files and let the files to be processed
	 * @return the archiveDir
	 */
	public File getprocessedDirectory() throws Exception {	

		String processedDirStr = DynamicString.render(new ModelBean("Company"),InnovationUtils.getEnvironmentVariable("Xact","processedXactDailyFeedDirectoryPath", ""));		
		this.archiveDir = new File(processedDirStr);
		
		try {
			if (!this.archiveDir.exists()) {
				this.archiveDir.mkdirs();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			Log.error("Unable to locate and/or create archive directory - " + processedDirStr, ex);
			throw new Exception("Unable to locate and/or create archive directory - " + processedDirStr, ex);
		}
		
		return this.archiveDir;
	}
	
	public String getprocessedDirectoryPath() throws Exception {
		return getprocessedDirectory().getPath();
	}
	
	


}