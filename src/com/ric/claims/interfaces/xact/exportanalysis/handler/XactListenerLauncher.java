package com.ric.claims.interfaces.xact.exportanalysis.handler;

import java.util.Map;


import com.iscs.common.tech.initialization.InitializationTask;
import com.iscs.common.tech.log.Log;

/** Xact listener launcher
 * 
 * @author Varadharajan
 *
 */
public class XactListenerLauncher implements InitializationTask{

	
	public void execute(Map<String, String> options) throws Exception {
	
		Log.debug("Launching Xact Analysis listener");
		
		// Start a listener
		XactListenerStart.getInstance().startListener();
		
	}
	
	
}
