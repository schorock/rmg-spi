package com.ric.claims.interfaces.xact.exportanalysis.handler;

/** Xact Listener Settings Object
 * 
 * @author Varadharajan
 *
 */
public class XactListenerSettings {
	
	private String pollTime;
	private String connectWaitTime;

	/** Poll Time Getter
	 * @return the pollTime
	 */
	public String getPollTime() {
		return pollTime;
	}
	
	/** Poll Time Setter
	 * @param pollTime the pollTime to set
	 */
	public void setPollTime(String pollTime) {
		this.pollTime = pollTime;
	}
	
	/** Connect Wait Time Getter
	 * @return the connectWaitTime
	 */
	public String getConnectWaitTime() {
		return connectWaitTime;
	}
	
	/** Connect Wait Time Setter
	 * @param connectWaitTime the connectWaitTime to set
	 */
	public void setConnectWaitTime(String connectWaitTime) {
		this.connectWaitTime = connectWaitTime;
	}
}
