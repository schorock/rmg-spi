package com.ric.claims.interfaces.xact.exportanalysis.handler;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import com.iscs.claims.claim.Claim;
import com.iscs.claims.claim.render.ClaimRenderer;
import com.iscs.common.business.attachment.Attachment;
import com.iscs.common.business.note.Note;
import com.iscs.common.business.provider.render.ProviderRenderer;
import com.iscs.common.render.DynamicString;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.InnovationUtils;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.ThreadContextInitializer;
import com.iscs.workflow.Task;
import com.iscs.workflow.TaskException;
import com.ric.insurance.interfaces.model.render.RICInterfaceRenderer;

import net.inov.biz.server.ServiceContext;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;
import net.inov.tec.data.JDBCLookup;
import net.inov.tec.data.JDBCLookup.LookupKey;

/**
 * Xact Listener Worker
 * 
 * @author Varadharajan
 *
 */
public class XactListenerWorker implements Runnable {

	private static int FILE_CNT_PER_POLL_DEFAULT = 5;
	private ModelBean userInfo = null;
	private String logStart= null;
	private String transactionName = null;
	private String serverName = null;
	private boolean forcedLogOut = false;
	private int transactionNumber = 0;
	private int fileListNumber = 0;
	private File archiveMovDir = null;
	private XactListenerSettings xactListenerSettings = null;
	private XactListenerFileSettings xactListenerFileSettings = null; 
	private ModelBean claimAttachmentTemplate = null;
	private ModelBean claimClaimNote3030Template = null;
	public void run() {
		try {
			serverName = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			serverName = "unknown";
		}
				
		main: for (;;) {
			try {
				// Initialize the service context
				String transactionNameNoDot = serverName + ".xactworker."+ getLogStart();
				transactionName = transactionNameNoDot + ".";
				new ThreadContextInitializer().initializeContext(userInfo,transactionNameNoDot, true, true);

				// Set the thread name
				Thread.currentThread().setName("Xact Listener Worker "+ Thread.currentThread().getId()+ " ("+ ServiceContext.getServiceContext().getTransactionName() + ")");

				Log.debug("Starting xact listener thread");
				
				String pollTime = xactListenerSettings.getPollTime();
				
				// Logging a new file at the start
				Log.start(transactionName + getNextTransactionNumber() + ".txt");
				
				for (;;) {
					boolean reconnectInd = true;
					
					if (getForcedLogOut()) {
						break main;
					}
					
					if (reconnectInd) {
						Log.start(transactionName + getNextTransactionNumber() + ".txt");
						reconnectInd = processXactFiles();
						
						//force export of thread if even one file is being processed
						if(fileListNumber > 0)
							Log.export(); 
						Log.debug("xact listener thread sleeping");
						Thread.sleep(Integer.parseInt(pollTime));
					}
				}
			} catch (Throwable e) {
				// Catch all Throwables, not just Exceptions. Allows us to catch OutOfMemoryError and other serious errors
				Log.error(e);
				e.printStackTrace();
				try {
					String connectWaitTime = xactListenerSettings.getConnectWaitTime();
					Log.debug("Sleep for " + connectWaitTime + " before reinitializing worker thread...");
					Log.export();
					Thread.sleep(Integer.parseInt(connectWaitTime));
				} catch (InterruptedException ie) {
					Log.error(ie);
				}
			} finally {
				// Finalize the context
				Log.debug("Worker thread reinitializing");
				new ThreadContextInitializer().finalizeContext();
			}
		}
	}
	

		
	/**
	 * 
	 * @return
	 */
	private String getFileCountPerPoll() {
		try {
			String fileCountPerPoll = DynamicString.render(new ModelBean("Company"),InnovationUtils.getEnvironmentVariable("Xact","fileCountPerPoll", ""));
			if (StringTools.isBlank(fileCountPerPoll) || StringTools.isBlank(fileCountPerPoll.trim())) {
				return "All";
			} else if (!StringTools.isNumeric(fileCountPerPoll.trim())) {
				return FILE_CNT_PER_POLL_DEFAULT+"";
			}
			return fileCountPerPoll.trim();
		} catch (Exception ex) {
			ex.printStackTrace();
			Log.error("Unable to get the fileCountPerPoll value from the env settings file. Defaulting the count to " + FILE_CNT_PER_POLL_DEFAULT, ex);
			return FILE_CNT_PER_POLL_DEFAULT+"";
		}
	}
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	private File[] getXactFiles() throws Exception {
		File[] fileList = null;
		
		//Ignoring the files under the backup directory
		fileList = getXactListenerFileSettings().getInboundDirectory().listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return (name.indexOf(".ZIP") >= 0 || name.indexOf(".zip") >= 0);
			}
		});
		
		if (fileList == null) {
			Log.error("Not able to retrieve Xact file(s) from = " + getXactListenerFileSettings().getInboundDirectory().getPath());
			return fileList;
		}

		if (fileList.length <= 0) {
			Log.info("No Xact ZIP file(s) found to process under = " + getXactListenerFileSettings().getInboundDirectory().getPath());
			return fileList;
		}
		
		String fileCountPerPoll = getFileCountPerPoll();
		
		if ("All".equals(fileCountPerPoll)) {
			return fileList;
		} else {
			int srcFileListCnt = fileList.length;
			int destFileListCnt = Integer.parseInt(fileCountPerPoll);
			
			if (destFileListCnt < 0) {
				destFileListCnt = FILE_CNT_PER_POLL_DEFAULT;
			}
			
			if (srcFileListCnt <= destFileListCnt) {
				destFileListCnt = srcFileListCnt;
			}
			
			File[] destFileList = new File[destFileListCnt];
			System.arraycopy(fileList, 0, destFileList, 0, destFileList.length);
			fileList = null;
			return destFileList;
		}
	}
		
	private SimpleDateFormat getSimpleDateFormat(String format, TimeZone timeZone) {
		SimpleDateFormat sdfSource = new SimpleDateFormat(format);
		if(timeZone != null) {
			sdfSource.setTimeZone(timeZone);
		}
		return sdfSource;
	}
		
	private void addAttachment(ModelBean claim, File zipEntryFile, SimpleDateFormat sdfSource) throws Exception {
		String mda = "CLClaim::attachment::repository:setup";			
		// ModelBean attachment = Attachment.create("", claim, null, "", null /*memoText*/);	
		ModelBean attachment = Attachment.create("CLClaim", "attachment-template", Attachment.getAttachmentTemplate("XClaimAttachment3025").getId(), claim, sdfSource.format(new Date()), DateTools.getTime("HH:mm:ss z"), userInfo.gets("LoginId"), "");
		attachment.nextUid();
		attachment.setValue("Description", zipEntryFile.getName());
		attachment.setValue("OriginalFilename", zipEntryFile.getName());		
		String savedFile = Attachment.saveFile(attachment, zipEntryFile, mda);
		attachment.setValue("Filename", savedFile);		
		Log.debug("Finished adding attachment Bean to Claim");
	}
	
	
	/**
	 * @return the claimClaimNote3011Template
	 */
	private ModelBean getClaimNote3030Template() throws Exception {
		if (this.claimClaimNote3030Template != null) {
			return this.claimClaimNote3030Template;
		}
		this.claimClaimNote3030Template = Note.getNoteTemplate("ClaimNote3030");
		return this.claimClaimNote3030Template;
	}
	
	/**
	 * @return the claimAttachmentTemplate
	 */
	private ModelBean getClaimAttachmentTemplate() throws Exception {
		if (this.claimAttachmentTemplate != null) {
			return this.claimAttachmentTemplate;
		}
		
		this.claimAttachmentTemplate = Attachment.getAttachmentTemplate("XClaimAttachment3025");
		return this.claimAttachmentTemplate;
	}
		
	/**
	 * 
	 * @param zipFile
	 * @param JDBCData
	 * @param errorTask
	 * @throws Exception
	 */
	private void moveFileToUnprocessedFolder(File zipFile,JDBCData data,String errorTask) throws Exception {		
		//Moving the file to unprocessed folder.
		String unprocessedFilePath = getXactListenerFileSettings().getUnprocessedDirectoryPath() + File.separator + zipFile.getName();
		File unprocessedZipFile = new File(unprocessedFilePath);
		unprocessedZipFile.delete(); 
		boolean isMoved = zipFile.renameTo(unprocessedZipFile);
		if (!isMoved) {			
			String msg = "Unable to move " + zipFile.getName() + " from " + getXactListenerFileSettings().getInboundDirectory().getPath() + " to " + getXactListenerFileSettings().getUnprocessedDirectoryPath();
			Log.error(msg);	
			triggerTask(data,errorTask,zipFile.getName(),null,msg,null);			
		}else{
			triggerTask(data,errorTask,zipFile.getName(),null,"ZipFile moved to unprocessed folder since there are no matching claims",null);
		}
	}		
	/** Retrieve XACT zip files from app server folder	
	 * @return true if system should try to reconnect when an exception occurs, false if it should not
	 * @throws Exception when an error occurs
	 */
	private boolean processXactFiles() throws Exception {
		JDBCData data =null;
		String errorTask= "XactAnalysisErrorTask";
		String successTask = "XactAnalysisReviewTask";
		String filename ="";
		
		try {			
			 data = ServiceContext.getServiceContext().getData();
			File[] fileList = getXactFiles();
			if (fileList == null || fileList.length <= 0) {
				fileListNumber = 0;
				return false;
			}
			fileListNumber = fileList.length;			
			Log.info("Processing Xact inbound file(s) present under = " + getXactListenerFileSettings().getInboundDirectory().getPath());			
			Map<String, ModelBean> claimMap = new HashMap<String, ModelBean>();
			//Iterating across zip files.
			for (File zipXactFile : fileList) {	
				try {
					StringTokenizer st = new StringTokenizer(zipXactFile.getName(), ".");
					String claimNumber = st.nextToken();	
					filename = claimNumber;
					ModelBean claim = null;
					
					String clNum=getClaimNumberforLossNotice(claimNumber,data);
					
					if (clNum!= null && !clNum.equals("")) {						
						claim = Claim.getClaimByClaimNumber(data, clNum, false);
			            // Call Test Method 
						}					
					if(claim == null) {
						Log.error("Moving " + zipXactFile.getName() + " to unprocessed directory.");
						moveFileToUnprocessedFolder(zipXactFile,data,errorTask);
					} else {
						//Maintaining a map between Xact transaction id and SPI claim to avoid the claim lookup for the same Xact transaction id across zip files.
						claimMap.put(claim.gets("ClaimNumber"), claim);
						//Processsing the zip file.
						processZipFile(claim, zipXactFile,data,errorTask,successTask,filename);
						//Saving the claim bean
						data.saveModelBean(claim);
					}					
				} catch (Exception a) {					
					Log.error(a.getMessage());
					triggerTask(data,errorTask,filename,null,a.getMessage(),null);
				} finally {
				
					
					if (data != null) {
						data.commit();
					}
				}
			}			
			return true;
		} catch (InterruptedException ie) {
			Log.error("Server going down...");
			triggerTask(data,errorTask,filename,null,ie.getMessage(),null);
			setForcedLogOut(true); //do not try to reconnect
			return false;
		} catch (Exception e) {
			Log.error(e.getMessage());
			triggerTask(data,errorTask,filename,null,e.getMessage(),null);
			e.printStackTrace();
			return true; // try to reconnect
		}
	}
	
	/**
	 * 
	 * @param claim
	 * @param zipFile
	 * @throws Exception
	 */
	private void processZipFile(ModelBean claim, File zipFile,JDBCData data,String failureTask,String successtask,String filename)  throws Exception {
		//Archiving the file before processing.
		String archiveFilePath = getXactListenerFileSettings().getArchiveDirectoryPath() + File.separator + zipFile.getName();
		File archivedZipFile = new File(archiveFilePath);		
		archivedZipFile.delete();		
		boolean isArchived = zipFile.renameTo(archivedZipFile);				
		if (!isArchived) {			
			String msg = "Unable to archive " + zipFile.getName() + " from " + getXactListenerFileSettings().getInboundDirectoryPath() + " to " + getXactListenerFileSettings().getArchiveDirectoryPath();
			Log.error(msg);
			triggerTask(data,failureTask,filename,null,msg,null);		
		}
		
		ZipInputStream zipFileInStream = null;
		try {
			zipFileInStream = new ZipInputStream(new BufferedInputStream(new FileInputStream(archivedZipFile)));
			byte[] buffer = new byte[1024];
			ZipEntry zipEntry = null;
			
			while ((zipEntry = zipFileInStream.getNextEntry()) != null) {		
				String fileName = zipEntry.getName();											
				File zipEntryFile = new File(getXactListenerFileSettings().getArchiveDirectoryPath() + File.separator + fileName);
				FileOutputStream zipEntryOutStream = new FileOutputStream(zipEntryFile);				
				try {
					int len;
					while ((len = zipFileInStream.read(buffer)) > 0) {
						zipEntryOutStream.write(buffer, 0, len);
					}
					
					if (!zipEntryFile.exists()) {
						Log.error("Unable to unzip " + fileName + " from " + archiveFilePath);
						triggerTask(data,failureTask,filename,null,"Unable to unzip " + fileName + " from " + archiveFilePath,null);
						continue;
					}
					
					SimpleDateFormat sdfSource = getSimpleDateFormat("MM/dd/yyyy", null);					
					addAttachment(claim, zipEntryFile,  sdfSource);		
					
								
					zipFileInStream.closeEntry();
				} finally {
					if (zipEntryOutStream != null) {
						zipEntryOutStream.flush();
						zipEntryOutStream.close();
					}
					zipEntryFile.delete();
				}
			}
			triggerTask(data,successtask,filename,null,"ZipFile was processed successfully",claim);
			try {
			// Moving the file to attachment folder (archiving purpose)
				String importDirStr = getXactListenerFileSettings().getprocessedDirectoryPath();
				RICInterfaceRenderer.moveFile(archivedZipFile, importDirStr);
			} catch (Exception e1) {				
				Log.error(" Unable to move the file - " + e1);
				triggerTask(data,failureTask,filename,null,e1.getMessage(),null);
				e1.printStackTrace();
				
			}
		} finally {
			if (zipFileInStream != null) {
				zipFileInStream.close();
			}
			// After moving the file to attachment folder (archiving purpose) delete the file in the xactanalysis directory
			archivedZipFile.delete();
		}
	}
	

	private String getClaimNumberforLossNotice(String searchText, JDBCData data) throws Exception {
        String searchString = ModelSpecification.indexString(searchText);
        JDBCLookup lookup = new JDBCLookup("ClaimLookup");
        LookupKey lookUpKey = lookup.addLookupKey("ClaimNumber", searchString, JDBCLookup.LOOKUP_EQUALS);
        lookUpKey.attachOrKey("ClaimNumberLookup", searchString, JDBCLookup.LOOKUP_EQUALS);          
        JDBCData.QueryResult[] results = lookup.doLookup(data, 0);
        if (results != null && results.length>0) {                  
              lookup.addLookupKey("StatusCd");
              lookup.addLookupKey("TypeCd");
              results = lookup.doLookup(data, 0);             
              String statusCd = results[0].getLookupValue("StatusCd");
              String typeCd = results[0].getLookupValue("TypeCd");
              if (typeCd.equals("LOSSNOTICE")) {
                    if (statusCd.equals("COMPLETED")) {
                          lookup.addLookupKey("ClaimRef");                            
                          results = lookup.doLookup(data, 0);
                          String claimRef = results[0].getLookupValue("ClaimRef");
                          ModelBean claim=ClaimRenderer.getClaim(data, claimRef);
                          searchText=claim.gets("ClaimNumber");
                          return searchText;
                    } else {
                          return searchText;
                    }
              } else {
                    return searchText;
              }
        } else {
              return searchText;
        }
  }


	public XactListenerFileSettings getXactListenerFileSettings() {
		if(xactListenerFileSettings ==  null)
			xactListenerFileSettings = new XactListenerFileSettings();
		return xactListenerFileSettings;
	}
	
	/**
	 * @return the userInfo
	 */
	public ModelBean getUserInfo() {
		return userInfo;
	}

	/**
	 * @param userInfo the userInfo to set
	 */
	public void setUserInfo(ModelBean userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * @return the forcedLogOut indicator
	 */
	public boolean getForcedLogOut() {
		return forcedLogOut;
	}

	/**
	 * @param forcedLogOut the forcedLogOut flag to set
	 */
	public void setForcedLogOut(boolean forcedLogOut) {
		this.forcedLogOut = forcedLogOut;
	}

	/**
	 * @return the logStart 
	 */
	public String getLogStart() {
		return logStart;
	}

	/**
	 * @param logStart the logStart to set
	 */
	public void setLogStart(String logStart) {
		this.logStart = logStart;
	}	

	/** Get the next transaction number. The counter is simply incremented once each call.
	 * This method is synchronized to prevent simultaneous calls from different threads from
	 * returning the same number.
	 * @return Integer
	 */
	protected synchronized int getNextTransactionNumber() {
		return (++transactionNumber);
	}

	/** Email Listener Settings Getter
	 * @return the emailListenerSettings
	 */
	public XactListenerSettings getXactListenerSettings() {
		return xactListenerSettings;
	}

	/** Email Listener Settings Setter
	 * @param emailListenerSettings the emailListenerSettings to set
	 */
	public void setXactListenerSettings (XactListenerSettings xactListenerSettings) {
		this.xactListenerSettings = xactListenerSettings;
	}
	
	private void triggerTask(JDBCData data, String taskId, String fileName, ModelBean attachment, String description,ModelBean claim) throws TaskException, Exception {
		String correctedTemplateTaskId;
		
		if( Claim.isALossNotice(claim) )
			correctedTemplateTaskId = "LN-" + taskId;
		else if( Claim.isATransaction(claim) )
			correctedTemplateTaskId = "TX-" + taskId;
		else
			correctedTemplateTaskId = taskId;
		
		ModelBean task = Task.createTask(data, correctedTemplateTaskId, claim, "admin", DateTools.getStringDate(), attachment);
		
		if(taskId.equalsIgnoreCase("XactAnalysisReviewTask")){
			ModelBean provider = ProviderRenderer.getProviderBySystemId(claim.gets("AdjusterProviderRef"));			
			task.setValue("CurrentOwner", provider.gets("ProviderNumber")); 
            task.setValue("OriginalOwner", provider.gets("ProviderNumber"));
		}
		
        Task.insertTask(data, task);
	}
}