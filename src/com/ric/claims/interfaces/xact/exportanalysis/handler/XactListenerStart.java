package com.ric.claims.interfaces.xact.exportanalysis.handler;

import java.util.ArrayList;
import java.util.List;

public class XactListenerStart {			
	/** XactListenerStart
	 *  
	 * 
	 * @author Varadharajan
	 *
	 */
	private static XactListenerStart instance;

	private List<XactListener> listeners = new ArrayList<XactListener>();


	public XactListenerStart() throws Exception {}

	/** Gets the xact listener instance
	 * 
	 * @return XactListener
	 * @throws Exception
	 */
	public static XactListenerStart getInstance() throws Exception{

		if(instance == null){
			instance = new XactListenerStart();
		}

		return instance;
	}

	/** Start an xact listener
	 * 
	 * @return
	 * @throws Exception
	 */
	public XactListener startListener() throws Exception{

		XactListener listener = XactListenerFactory.getInstance().createListener();
		listeners.add(listener);		
		Thread t = new Thread(listener);
		
		t.start();

		return listener;

	}
	
	/** Get the active listeners that were created by this class
	 * 
	 * @return Active listeners
	 */
	public XactListener[] getActiveListeners(){
		
		List<XactListener> activeListeners = new ArrayList<XactListener>();
		
		for(XactListener listener : listeners){			
			if(listener.isListening()){
				activeListeners.add(listener);
			}
		}
		
		return activeListeners.toArray(new XactListener[activeListeners.size()]);
		
	}
	

	/** Closes the active listeners that were created by this class
	 * 
	 */
	public void closeListeners(){
		
		for(XactListener listener : getActiveListeners()){			
			listener.stopListening();
			
		}
		
	}





}



