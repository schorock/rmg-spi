package com.ric.claims.interfaces.xact.exportanalysis.handler;

import net.inov.tec.security.SecurityManager;
import net.inov.tec.xml.XmlDoc;

import org.jdom.Element;

import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDAXmlDoc;
import com.iscs.common.utility.StringTools;


/** XactListenerFactory - Creates common xact setting objects
 * 
 * @author Varadharajan
 *
 */
public class XactListenerFactory {

	private static final String XACT_LISTENER_SETTINGS = "xact-analysis-listener-settings::*::*";
	
	// Instance
	private static XactListenerFactory instance;

	// Do not allow public initialization
	private XactListenerFactory(){	
	}

	/** Gets the xact factory instance
	 * 
	 * @return XactListenerFactory
	 * @throws Exception
	 */
	public static XactListenerFactory getInstance() throws Exception{
		if(instance == null){
			instance = new XactListenerFactory();				
		}

		return instance;
	}			

	/** Create a listener
	 * 
	 * @return Listener
	 * @throws Exception 
	 */
	public XactListener createListener() throws Exception{

		// Get Xact Listener Settings
		MDAXmlDoc object = (MDAXmlDoc) Store.getModelObject(XACT_LISTENER_SETTINGS);
		XmlDoc setting = object.getDocument();
		Element el = setting.selectSingleElement("//xactListenerSettings");
		if( el == null )
			throw new Exception("Could not find " + XACT_LISTENER_SETTINGS);				
		
		XactListenerSettings xactListenerSettings = createListenerSettings(el);
		
		// Initialize XactListener
		XactListener xactListener = new XactListener();
		xactListener.setXactListenerSettings(xactListenerSettings);
		xactListener.setUserInfo(SecurityManager.getSecurityManager().getUser(el.getAttributeValue("InnovationUsername")));
		return xactListener;
	}
	
	/** Create the EmailListenerSettings
	 * @param el The Email Listener Settings element
	 * @return the EmailListenerSettings
	 * @throws Exception when an exception occurs
	 */
	public static XactListenerSettings createListenerSettings(Element el) throws Exception {		
		
		// Create an EmailListenerSettings Object
		XactListenerSettings xactListenerSettings = new XactListenerSettings();
		xactListenerSettings.setPollTime(el.getAttributeValue("PollTime"));
		xactListenerSettings.setConnectWaitTime(el.getAttributeValue("ConnectWaitTime"));
		
		return xactListenerSettings;
	}
}