package com.ric.claims.interfaces.xact.exportanalysis.handler;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.inov.tec.beans.ModelBean;

import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.ThreadContextInitializer;

/** Xact Listener
 * 
 * @author Varadharajan
 *
 */
public class XactListener implements Runnable{

	private XactListenerSettings xactListenerSettings;
	private String templates;
	private ModelBean userInfo;	
	private String logStart;
	
	private static int transactionNumber = 0;
	
	private Thread worker = null;

	/** Listen for xact files
	 * @throws Exception 
	 *  
	 */
	private void Listen() throws Exception{

		// Initialize the bits of the context we need to
		Log.debug("Starting xact listener...");
		XactListenerWorker thread = new XactListenerWorker();			
		thread.setXactListenerSettings(xactListenerSettings);
		thread.setUserInfo(userInfo);	
		thread.setLogStart(logStart);
		
		// Create the thread and spawn the process to run asynchronously
		Thread t = new Thread(thread);
		worker = t;
		
		t.start();

		// Flush the log
		if( Log.isLogThreads() ) {
			Log.export();
		}
	}

	/** Stops the listener
	 * 
	 * @throws Exception
	 */
	public void stopListening(){
		try{		   
           worker.interrupt();
		   worker = null;		
		}
		catch(Exception e){
			Log.error(e);
		}

	}

	/** Listening getter
	 * 
	 * @return if listening
	 */
	public boolean isListening(){

		return (worker != null);

	}

	/** Called by the java threading system when this thread is spawned.  Do not call directly
	 * 
	 */
	public void run() {
		try{
			new ThreadContextInitializer().initializeContext(userInfo, getTransactionName(), true, false);

			Listen();	

		}
		catch(Throwable e){
			// Catch all Throwables, not just Exceptions. Allows us to catch OutOfMemoryError and other serious errors
			Log.error(e);
		}
		finally{

			// Finalize the context
			new ThreadContextInitializer().finalizeContext();


		}
	}

	/**
	 * @return the templates
	 */
	public String getTemplates() {
		return templates;
	}

	/**
	 * @param templates the templates to set
	 */
	public void setTemplates(String templates) {
		this.templates = templates;
	}

	/**
	 * @return the userInfo
	 */
	public ModelBean getUserInfo() {
		return userInfo;
	}

	/**
	 * @param userInfo the userInfo to set
	 */
	public void setUserInfo(ModelBean userInfo) {
		this.userInfo = userInfo;
	}	

	/** Get the transaction name
	 * @return 
	 * 
	 */
	private String getTransactionName() {
		String serverName = null;
		try {
			serverName = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			serverName = "unknown";
		}
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd.HH-mm-ss");
		String stringDate = fmt.format(new Date());
		logStart = stringDate + "." + getNextTransactionNumber();
		String transactionName = serverName + ".xact-listener." + logStart;
		return transactionName;
	}

	/** Get the next transaction number. The counter is simply incremented once each call.
	 * This method is synchronized to prevent simultaneous calls from different threads from
	 * returning the same number.
	 * @return Integer
	 */
	protected synchronized int getNextTransactionNumber() {
		return (++transactionNumber);
	}

	/** XactListenerSettings Getter
	 * @return the xactListenerSettings
	 */
	public XactListenerSettings getXactListenerSettings() {
		return xactListenerSettings;
	}

	/** XactListenerSettings Setter
	 * @param xactListenerSettings the xactListenerSettings to set
	 */
	public void setXactListenerSettings(XactListenerSettings xactListenerSettings) {
		this.xactListenerSettings = xactListenerSettings;
	}
}