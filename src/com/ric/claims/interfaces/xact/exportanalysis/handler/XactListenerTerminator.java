package com.ric.claims.interfaces.xact.exportanalysis.handler;

import java.util.Map;

import com.iscs.common.tech.initialization.TerminationTask;
import com.iscs.common.tech.log.Log;

/** Terminates the xact listener
 * 
 * @author Varadharajan
 *
 */
public class XactListenerTerminator implements TerminationTask {

	public void execute(Map<String, String> options) throws Exception {
		
		Log.debug("Terminating the xact analysis listener");
		
		XactListenerStart.getInstance().closeListeners();
		 
	}

}
