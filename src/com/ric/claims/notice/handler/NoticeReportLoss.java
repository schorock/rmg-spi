/*
 * NoticeReportLoss.java
 *
 */

package com.ric.claims.notice.handler;

import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.insurance.producer.Producer;
import com.iscs.uw.policy.Policy;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

/** Report Loss Notice
 *
 * @author allend
 */
public class NoticeReportLoss extends InnovationIBIZHandler {
    
    /** Creates a new instance of NoticeReportLoss
     * @throws Exception never
     */
    public NoticeReportLoss() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a Greeting
            Log.debug("Processing NoticeReportLoss...");
            ModelBean rs = this.getHandlerData().getResponse();
            JDBCData data = this.getHandlerData().getConnection();
            AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest() );
            ModelBean responseParams = rs.getBean("ResponseParams");
            StringDate todayDt = DateTools.getStringDate(responseParams);

            // Get the Claim ModelBean
            ModelBean claim = rs.getBean("Claim");
  
            // Default Claim Information
            claim.setValue("TypeCd", "LossNotice");
            claim.setValue("StatusCd", "New");
            claim.setValue("CustomerRef", ap.gets("CustomerSystemId"));
            claim.setValue("ProductVersionIdRef", ap.gets("ClaimSetupName"));
            claim.setValue("ReportedDt", todayDt);
            claim.setValue("ReportedTm", DateTools.getTime(responseParams));
            claim.setValue("AwarenessDt", todayDt);
            claim.setValue("VehicleInvolvedInd", ap.gets("VehicleInvolvedInd"));
            
            // Default Claim Policy Information
            ModelBean policyInfo = new ModelBean("ClaimPolicyInfo");
            if (ap.gets("ProviderSysId") != null){
            	policyInfo.setValue("ProviderRef", ap.gets("ProviderSysId"));
            } else {
            	ModelBean provider = Producer.getProvider(data,ap.gets("ProviderNumber"));
            	policyInfo.setValue("ProviderRef", provider.getSystemId());	
            }
         
            policyInfo.setValue("PolicyNumber", ap.gets("PolicyNumber"));
            policyInfo.setValue("PolicyRef", ap.gets("PolicyRef"));
            policyInfo.setValue("InceptionDt", ap.gets("InceptionDt"));
            policyInfo.setValue("ExpirationDt", ap.gets("ExpirationDt"));
            policyInfo.setValue("CancelDt", ap.gets("CancelDt"));
            
            if( !ap.gets("PolicyRef").equals("") ){
            	ModelBean policy = Policy.getPolicyBySystemId(data, Integer.parseInt(ap.gets("PolicyRef")));
	            policyInfo.setValue("SubTypeCd", policy.getBean("BasicPolicy").gets("SubTypeCd"));
	            policyInfo.setValue("LegacyPolicyNumber", policy.getBean("BasicPolicy").gets("LegacyPolicyNumber"));
            
            }
            
            ModelBean partyInfo = new ModelBean("PartyInfo");
            ModelBean nameInfo = new ModelBean("NameInfo");
            nameInfo.setValue("NameTypeCd", "InsuredName");
            nameInfo.setValue("CommercialName", ap.gets("InsuredName"));
            partyInfo.setValue(nameInfo);
            partyInfo.setValue("PartyTypeCd","InsuredParty");
            policyInfo.setValue(partyInfo);
            claim.setValue(policyInfo);
            
            // Return the Response ModelBean
            return rs;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
}

