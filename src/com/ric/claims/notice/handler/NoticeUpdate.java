/*
 * NoticeUpdate.java
 *
 */

package com.ric.claims.notice.handler;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.web.cmm.AdditionalParams;

import com.iscs.claims.claim.Claim;
import com.iscs.claims.notice.Notice;
import com.iscs.common.business.provider.Provider;
import com.iscs.common.business.provider.render.ProviderRenderer;
import com.iscs.common.tech.biz.IBIZ;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.insurance.customer.Customer;
import com.iscs.workflow.Task;

/** Update Loss Notice - Check for Change in Loss Date
 *
 * @author moniquef
 */
public class NoticeUpdate extends InnovationIBIZHandler {

    /** Creates a new instance of NoticeUpdate
     * @throws Exception never
     */
    public NoticeUpdate() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a Greeting
            Log.debug("Processing NoticeUpdate...");
            ModelBean rs = this.getHandlerData().getResponse();
            AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest() );
            JDBCData data = this.getHandlerData().getConnection();
            
            // Get the Claim ModelBean
            ModelBean claim = rs.getBean("Claim");
            ModelBean claimPolicyInfo = claim.getBean("ClaimPolicyInfo");
            String policyRef = claimPolicyInfo.gets("PolicyRef");
            
            String examinerProviderType = ProviderRenderer.getProviderTypeCodes("COProvider::provider::search::examiner-type");
            if( examinerProviderType.equals("")){
            	examinerProviderType = "Examiner";
            }

            // Update the Examiner Provider Reference
            String examinerProviderNumber = ap.gets("ExaminerProviderNumber");
            if( !examinerProviderNumber.equals("")){            	
            	 ModelBean examiner = Provider.getProvider(data, examinerProviderNumber, examinerProviderType);
                 claim.setValue("ExaminerProviderRef",examiner.getSystemId());
            }
            
            String adjusterProviderType = ProviderRenderer.getProviderTypeCodes("COProvider::provider::search::assigned-adjuster-type");
            if( adjusterProviderType.equals("")){
            	adjusterProviderType = "Adjuster";
            }
            
            // Update the Adjuster Provider Reference
            String adjusterProviderRef = ap.gets("AdjusterProviderNumber");
            if( !adjusterProviderRef.equals("")){            	
            	 ModelBean adjuster = Provider.getProvider(data, adjusterProviderRef, adjusterProviderType);
                 
                 if( adjuster == null ) {
            		 claim.setValue("AdjusterProviderRef", "");
            	 } else {
	                 claim.setValue("AdjusterProviderRef",adjuster.getSystemId());
	                 
	                 // Preset Claimaint.AssignedAdjuster
	                 ModelBean[] claimants = claim.getBeans("Claimant");
	                 for( ModelBean claimant : claimants ) {
	                 	ModelBean assignedAdjuster = claimant.getBean("AssignedProvider", "AssignedProviderTypeCd", "AssignedAdjuster");
	                 	assignedAdjuster.setValue("ProviderRef", adjuster.getSystemId());
	                 }
            	 }
            }
            
            // SIU update
            String siuInd = claim.gets("InSIUInd");
            if (siuInd.equalsIgnoreCase("Yes")) {
            	String siuProviderNumber = ap.gets("SIUProviderNumber");
            	if (!siuProviderNumber.isEmpty()) {
            		ModelBean siuProvider = Provider.getSIUProvider(data, siuProviderNumber);
            		claim.setValue("SIUAdjusterRef", siuProvider.getSystemId());
            	}
            }
            
            // On a Unverified Loss Do Not Rebuild ClaimPolicyInfo
            if( !policyRef.equals("") ) {
            
	            // Get Old ClaimMini ModelBean
		        ModelBean oldClaimMini = data.selectMiniBean("ClaimMini", claim.getSystemId());
		        ModelBean oldClaimPolicyInfo = oldClaimMini.getBean("ClaimPolicyInfo");
		        
	            boolean lossDtChanged = Notice.hasLossDateChanged(data, oldClaimMini, claim);
	            boolean lossCauseChanged = Notice.hasLossCauseChanged(data, oldClaimMini, claim);
	            boolean reportedDtChanged = Notice.hasReportedDateChanged(data, oldClaimMini, claim);
	            boolean coverageTriggerCdChanged = Notice.hasCoverageTriggerCodeChanged(data, oldClaimMini, claim);
	            boolean policyAssigned = Notice.hasPolicyBeenAssigned(data, oldClaimMini, claim);
	            boolean productLineChanged = Notice.hasProductLineChanged(data, oldClaimMini, claim);
	            boolean propertyChangedState = Notice.hasPropertyStateChanged(claim);
	            
	        	// Call PolicyInfoBuilder to Reload ClaimPolicyInfo
	            ModelBean claimSetup = Claim.getClaimSetup(claim);
	            ModelBean ibiz = claimSetup.getBean("ibiz");
	            StringBuffer errorText = new StringBuffer();
	            
	            if( !oldClaimPolicyInfo.valueEquals("CoverageTriggerCd", "") && coverageTriggerCdChanged ) {
	            	// Rebuild ClaimPolicyInfo
	                IBIZ.processSequence("CLNoticeLossDateChange", ibiz, getHandlerData());
	                errorText.append("Coverage Trigger Changed - Reselect Risk");
	            } else if( !claimPolicyInfo.valueEquals("CoverageTriggerCd", "Claims Made") && lossDtChanged ) {
	            	// Rebuild ClaimPolicyInfo
	                IBIZ.processSequence("CLNoticeLossDateChange", ibiz, getHandlerData());
	                errorText.append("Loss Date Changed - Reselect Risk");
	            } else if( claimPolicyInfo.valueEquals("CoverageTriggerCd", "Claims Made") && reportedDtChanged ) {
	            	// Rebuild ClaimPolicyInfo
	                IBIZ.processSequence("CLNoticeLossDateChange", ibiz, getHandlerData());
	                errorText.append("Reported Date Changed - Reselect Risk");                
	            } else if( policyAssigned ) {
	            	// Product Changed - Reselect Line
	            	if( claim.valueEquals("ProductLineCd", "") ) {
	            		addErrorMsg("General", "Product Changed - Reselect Loss Type/Line", ErrorTools.GENERIC_BUSINESS_ERROR, "Warn");
	                    
	            		// Clear the RiskIdRef and DriverIdRef
	                    claim.setValue("RiskIdRef","");
	                    claim.setValue("DriverIdRef","");
	            	} else {
	                	// Rebuild ClaimPolicyInfo
	            		IBIZ.processSequence("CLNoticePolicyAssign", ibiz, getHandlerData());
	            		errorText.append("Policy Assigned - Reselect Risk");
	                }	            	
	            	// Set the VIP Security Level
	                if( !claim.gets("CustomerRef").equals("") ) {
	                	ModelBean customer = Customer.getCustomerBySystemId(data, Integer.parseInt(claim.gets("CustomerRef")));
	                	claim.setValue("VIPLevel",customer.gets("VIPLevel"));
	                }
	            } else if( productLineChanged ) {
	            	// Rebuild ClaimPolicyInfo
	            	IBIZ.processSequence("CLNoticePolicyAssign", ibiz, getHandlerData());
	            	errorText.append("Product Changed - Reselect Risk");
	            } else if( propertyChangedState ) {
	            	// Rebuild ClaimPolicyInfo if state has changed
	            	IBIZ.processSequence("CLNoticeLossDateChange", ibiz, getHandlerData());	
	            	errorText.append("Controlling State Changed - Reselect Loss Cause");	            	
	            }
	            
	            if( errorText.length() > 0 ) {
	            	
	            	// Check for Missing Driver
	            	ModelBean policyDrivers = claimPolicyInfo.getBean("PolicyDrivers");
	                if( policyDrivers != null )
	                	errorText.append("/Driver");
	                 
	                // Check for a First Party Claimant
	            	ModelBean claimant = BeanTools.findBeanByFieldValueAndStatus(claim, "Claimant", "ClaimantTypeCd", "First Party", "Open");
		            if( claimant != null ) 
		            	errorText.append(" and Review First Party Claimant");
		            
	            	addErrorMsg("General", errorText.toString(), ErrorTools.GENERIC_BUSINESS_ERROR, "Warn");
	            	
            		// Clear the Loss Cause on Controlling State Change
	            	if( propertyChangedState ) {
	            		
	            		claim.setValue("LossCauseCd","");
	                    claim.setValue("SubLossCauseCd","");
	            	}
	            	else {	            		
	            		claim.setValue("RiskIdRef","");
	                	claim.setValue("DriverIdRef","");
	            	}
	            }
	            
	            if( lossDtChanged || lossCauseChanged || policyAssigned || productLineChanged || propertyChangedState) {
	            	
		            // Updated Task Attributes on Change
	                Task.updateTaskAttributes(data, claim);
	            }
	            
	            
            }

            // Return the Response ModelBean
            return rs;

        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
}