/*
 * NoticePolicyValidate.java
 *
 */

package com.ric.claims.notice.handler;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

import com.iscs.claims.notice.Notice;
import com.iscs.common.admin.user.ContainerSecurity;
import com.iscs.common.admin.user.ContainerSecurityResult;
import com.iscs.common.render.VelocityTools;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.error.ErrorTools;

/** Validate Policy Being Assigned to the Loss Notice
 *
 * @author moniquef
 */
public class NoticePolicyValidate extends InnovationIBIZHandler {
	
    /** Creates a new instance of NoticePolicyValidate
     * @throws Exception never
     */
    public NoticePolicyValidate() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a Greeting
            Log.debug("Processing NoticePolicyValidate...");
            ModelBean rs = this.getHandlerData().getResponse();
            ModelBean rq = this.getHandlerData().getRequest();
            ModelBean requestParams = rq.getBean("RequestParams");
            JDBCData data = this.getHandlerData().getConnection();	          
            AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest() );    
            ModelBean responseParams = rs.getBean("ResponseParams");
            StringDate todayDt = DateTools.getStringDate(responseParams);
            ModelBean userInfo = responseParams.getBean("UserInfo"); 
            
            String vehicleInvolvedInd = ap.gets("VehicleInvolvedInd");
            
            // Get the Claim ModelBean
            ModelBean claim = rs.getBean("Claim");
            ModelBean claimPolicyInfo = claim.getBean("ClaimPolicyInfo");
            String policyNumber = ModelSpecification.indexString(claimPolicyInfo.gets("PolicyNumber"));
            
            if( !policyNumber.equals("") ) {
				ModelBean dtoPolicy = Notice.getPolicyLatestTerm(data, policyNumber, VelocityTools.getSecurityId(rq), requestParams.gets("SessionId"), requestParams.gets("UserId"), requestParams.gets("ConversationId"));
				if( dtoPolicy != null ) {	
					
					// Check Container Security
					ContainerSecurity security = new ContainerSecurity();
                	ContainerSecurityResult securityResult = security.getContainerSecurity(data, dtoPolicy, userInfo, todayDt);
                	if (!securityResult.canDisplay()) {
	                	addErrorMsg("General", "You do not have security for this policy", ErrorTools.GENERIC_BUSINESS_ERROR);
	                	throw new IBIZException();
                	} else {
                		// Report any errors
                		if (securityResult.getErrorList().size() > 0) {
                			for (ModelBean error : securityResult.getErrorList()) {
                                addErrorMsg(error.gets("Name"), error.gets("Msg"), error.gets("Type"));
        	                	throw new IBIZException();
                			}
                		}
                	}
					
					claim.setValue("CustomerRef", dtoPolicy.gets("CustomerRef"));
					claimPolicyInfo.setValue("PolicyNumber", dtoPolicy.getBean("DTOBasicPolicy").gets("PolicyNumber"));
					claimPolicyInfo.setValue("CarrierGroupCd", dtoPolicy.getBean("DTOBasicPolicy").gets("CarrierGroupCd"));
					claimPolicyInfo.setValue("ControllingStateCd", dtoPolicy.getBean("DTOBasicPolicy").gets("ControllingStateCd"));
					claimPolicyInfo.setValue("ExpirationDt", dtoPolicy.getBean("DTOBasicPolicy").gets("ExpirationDt"));
					claimPolicyInfo.setValue("InceptionDt", dtoPolicy.getBean("DTOBasicPolicy").gets("InceptionDt"));
					claimPolicyInfo.setValue("SubTypeCd", dtoPolicy.getBean("DTOBasicPolicy").gets("SubTypeCd"));
					claimPolicyInfo.setValue("LegacyPolicyNumber", dtoPolicy.getBean("DTOBasicPolicy").gets("LegacyPolicyNumber"));
					claim.setValue("ProductVersionIdRef", dtoPolicy.getBean("DTOBasicPolicy").gets("ClaimSetupName"));
					claim.setValue("ProductLineCd", "");
					
					if( vehicleInvolvedInd.isEmpty() ) {
						ModelBean[] dtoVehicleArray = dtoPolicy.getAllBeans("DTOVehicle");
			    		for( ModelBean dtoVehicle : dtoVehicleArray ) {
			    			if( !dtoVehicle.valueEquals("VehNumber", "") ) {
			    				claim.setValue("VehicleInvolvedInd", "Yes");
			    				break;
			    			}
			    		}
					}
				} 
            }
            
            // Return the Response ModelBean
            return rs;
        }
        catch( IBIZException ibe ) {
        	throw ibe;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
}