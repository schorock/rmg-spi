/*
 * ClaimantTransactionRenderer.java
 *
 */

package com.ric.claims.transaction.render;

import org.jdom.Attribute;
import org.jdom.JDOMException;
import org.jdom.xpath.XPath;

import com.iscs.claims.claim.Claim;
import com.iscs.claims.common.Feature;
import com.iscs.claims.common.Reserve;
import com.iscs.claims.transaction.render.ClaimantTransactionRenderer;
import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDAUrl;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.table.excel.ExcelReader;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.math.Money;
import net.inov.tec.xml.XmlDoc;

public class RICClaimantTransactionRenderer extends ClaimantTransactionRenderer {
	private static final String STRING_EMPTY = "";
	private static final String QUERY_RESERVE_AMOUNT = "//Table[@Name='average-reserve']/Row[@FeatureCd='%s' and @LossCauseCd='%s']/@ReserveAmt";
	private static final String subCoveragesList  = "CUOASC, DR, DRT, ER, TPSL, FDSC, GM, GB, RFP, TI, BAL, OOPL, WLL, PIII";

	public RICClaimantTransactionRenderer() {
		super();
	}

	public String getContextVariableName() {
		return getClass().getSimpleName();
	}

	public static String defaultAverageReserves(ModelBean claim, ModelBean transaction) throws Exception {
		String lossCauseCd = claim.gets("LossCauseCd");

		// Get ClaimSetup
		ModelBean claimSetup = Claim.getClaimSetup(claim);
		// Get Average Reserve Excel Workbook
		MDAUrl mdaUrl = (MDAUrl) Store.getModelObject(claimSetup.gets("AverageReserveSource"));
		XmlDoc workbook = new XmlDoc(mdaUrl.getURL());

		// Convert Workbook to Element Format
		XmlDoc doc = ExcelReader.convertToElementFormat(workbook);

		String amount;

		// Walk the list of policy limits and load defaults from the workbook
		for (ModelBean claimPolicyLimit : claim.getBean("ClaimPolicyInfo").getBeans("ClaimPolicyLimit")) {
			String featureCd = claimPolicyLimit.gets("CoverageCd");

			// Fetch Value From Workbook
			amount = findReserveAmount(doc, featureCd, lossCauseCd);

			// look for reserveAmt in average-reserves.xml worksheet
			if (amount == null)
				amount = findReserveAmount(doc, featureCd, "*");

			if (amount == null)
				amount = findReserveAmount(doc, "*", lossCauseCd);

			if (amount == null)
				amount = findReserveAmount(doc, "*", "*");

			// Add Feature and Reserve Allocation
			if (amount != null ) {
				Money reserveAmt = new Money(amount);
				ModelBean feature = new ModelBean("FeatureAllocation");
				feature.setValue("FeatureCd", featureCd);
				// ModelBean coverageDefinition =
				// Feature.getFeatureDefinition(claim, featureCd);
				// String sectionCd = coverageDefinition.gets("SectionCd");

				if (amount != "") {
					ModelBean reserveAlloc = Reserve.createAllocation("Indemnity", null, reserveAmt, null, null, "Open");
					ModelBean featureAlloc = Feature.setAllocation(transaction, feature);
					Feature.addReserve(featureAlloc, reserveAlloc, true);
				}

			}

			for (ModelBean claimPolicySubLimit : claimPolicyLimit.getBeans("ClaimPolicySubLimit")) {
				String featureSubCd = claimPolicySubLimit.gets("Category");

				amount = findReserveAmount(doc, featureCd, lossCauseCd);

				if (amount == null)
					amount = findReserveAmount(doc, featureCd, "*");


				if (amount != null) {
					if( !StringTools.in(featureSubCd, subCoveragesList)) {					
						Money reserveAmt = new Money(amount);
						ModelBean feature = new ModelBean("FeatureAllocation");
						feature.setValue("FeatureCd", featureCd);
						feature.setValue("FeatureSubCd", featureSubCd);
						feature.setValue("ItemNumber", claimPolicySubLimit.gets("ItemNumber"));
						// ModelBean coverageDefinition =
						// Feature.getFeatureDefinition(claim, featureCd);
						// String sectionCd = coverageDefinition.gets("SectionCd");
	
						ModelBean reserveAlloc = Reserve.createAllocation("Indemnity", null, reserveAmt, null, null, "Open");
						ModelBean featureAlloc = Feature.setAllocation(transaction, feature);
						Feature.addReserve(featureAlloc, reserveAlloc, true);
	
						featureAlloc = Feature.setAllocation(transaction, feature);
					}
				}
			}
		}

		return STRING_EMPTY;
	}

	private static String findReserveAmount(XmlDoc document, String featureCd, String lossCauseCd) {
		String amount = null;
		String query = String.format(QUERY_RESERVE_AMOUNT, featureCd, lossCauseCd);

		try {
			Attribute a = (Attribute) XPath.selectSingleNode(document, query);

			if (a != null)
				amount = a.getValue();
			else
				amount = "";
		} catch (JDOMException e) {
		}

		return amount;
	}
	
}
