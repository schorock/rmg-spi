/*
 * TransactionCatastropheSet.java
 *
 */

package com.ric.claims.transaction.handler;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import java.util.ArrayList;
import com.ric.claims.interfaces.xact.importanalysis.XACTTools;
import com.ric.claims.interfaces.xact.importanalysis.handler.XactAnalysisAssignmentRequest;
import com.iscs.common.business.datareport.DataReportController;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.insurance.product.ProductMaster;

/** Update the ClaimCatastrophe beans on a Claim with data posted by the claim-catastrophe tile
 * or the claim-information tile.
 *
 * @author  monicaa
 */
public class XACTRequestController extends InnovationIBIZHandler {
    
    /** Creates a new instance of TransactionCatastropheSet
     * @throws Exception never
     */
    public XACTRequestController() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
        	
            Log.debug("Processing RIC XACTRequestController...");
            ModelBean rs = this.getHandlerData().getResponse();
                        
            //for each claimant, get all property damaged
            //if property damaged is active, then auto generate a report
            ModelBean claim = rs.getBean("Claim");
            for (ModelBean claimant: claim.getAllBeans("Claimant")) {
            	if(!claimant.gets("StatusCd").equals("Deleted") && !claimant.gets("isXactSuccess").equals("Yes") && StringRenderer.in(claim.gets("SeverityLevel"), "P1,P2")) {
            		for (ModelBean propDamaged : claimant.getAllBeans("PropertyDamaged")) {
            			if(propDamaged.gets("StatusCd").equals("Active") || XACTTools.sendReportForPropertyDamaged(propDamaged) ) {
            				Log.debug("Creating data report for claimant " + claimant.gets("IndexName") + " Property Damaged: " + propDamaged.gets("PropertyDamagedNumber")); 
            				ModelBean addr = propDamaged.getBeanByAlias("PropertyDamagedLocation"); 
            				createXactXML(claim, claimant, addr.getId()); 
            			}
            		}
            	}
            }
            
            
            // Return the response bean
            return null;
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
    
    private void createXactXML(ModelBean claim, ModelBean claimant, String lossAddrRef) throws ModelBeanException, Exception { 
    	ArrayList<ModelBean> list = new ArrayList<ModelBean>(); 
    	for(ModelBean provider: XACTTools.getProvidersForXactAssignment(claimant)) {
    		claimant.setValue("InvolvedParty", claimant.getId());
    		claimant.setValue("Location", lossAddrRef);
    		String productVersionId = claim.gets("ProductVersionIdRef");
	       	ModelBean claimSetup = ProductMaster.getProductMaster("Claims").getProductSetupBean(productVersionId);
	       	XactAnalysisAssignmentRequest.getInstance().doXactAnalysisAssignmentRequest(claim, claimant, claimSetup, provider);
    	}
    }

    
}

