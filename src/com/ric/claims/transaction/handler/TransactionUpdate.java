/*
 * TransactionUpdate.java
 *
 */

package com.ric.claims.transaction.handler;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

import com.iscs.claims.claim.Claim;
import com.iscs.claims.transaction.Transaction;
import com.iscs.common.business.provider.Provider;
import com.iscs.common.business.provider.render.ProviderRenderer;
import com.iscs.common.tech.biz.IBIZ;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;

/** Update Transaction - Check for Change in Risk
 *
 * @author moniquef
 */
public class TransactionUpdate extends InnovationIBIZHandler {
    // Create an object for logging messages
    
    
    /** Creates a new instance of TransactionUpdate
     * @throws Exception never
     */
    public TransactionUpdate() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a Greeting
            Log.debug("Processing TransactionUpdate...");
            ModelBean rs = this.getHandlerData().getResponse();
            AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest() );
            JDBCData data = this.getHandlerData().getConnection();
                        
            // Get the Claim ModelBean
            ModelBean claim = rs.getBean("Claim");
            
            String examinerProviderType = ProviderRenderer.getProviderTypeCodes("COProvider::provider::search::examiner-type");
            if( examinerProviderType.equals("")){
            	examinerProviderType = "Examiner";
            }

            // Update the Examiner Provider Reference
            String examinerProviderNumber = ap.gets("ExaminerProviderNumber");
            if( !examinerProviderNumber.equals("")){            	
            	 ModelBean examiner = Provider.getProvider(data, examinerProviderNumber, examinerProviderType);
                 claim.setValue("ExaminerProviderRef",examiner.getSystemId());
            }   

            String adjusterProviderType = ProviderRenderer.getProviderTypeCodes("COProvider::provider::search::assigned-adjuster-type");
            if( adjusterProviderType.equals("")){
            	adjusterProviderType = "Adjuster";
            }
            
            // Update the Adjuster Provider Reference
            String adjusterProviderRef = ap.gets("AdjusterProviderNumber");
            if( !adjusterProviderRef.equals("")){            	
            	 ModelBean adjuster = Provider.getProvider(data, adjusterProviderRef, adjusterProviderType);
            	 
            	 if( adjuster == null ) {
            		 claim.setValue("AdjusterProviderRef", "");
            	 } else {
	                 claim.setValue("AdjusterProviderRef",adjuster.getSystemId());
	                 
	                 // Preset Claimaint.AssignedAdjuster
	                 ModelBean[] claimants = claim.getBeans("Claimant");
	                 for( ModelBean claimant : claimants ) {
	                 	ModelBean assignedAdjuster = claimant.getBean("AssignedProvider", "AssignedProviderTypeCd", "AssignedAdjuster");
	                 	assignedAdjuster.setValue("ProviderRef", adjuster.getSystemId());
	                 }
            	 }
            }
            
            // SIU update
            String siuInd = claim.gets("InSIUInd");
            if (siuInd.equalsIgnoreCase("Yes")) {
            	String siuProviderNumber = ap.gets("SIUProviderNumber");
            	if (!siuProviderNumber.isEmpty()) {
            		ModelBean siuProvider = Provider.getSIUProvider(data, siuProviderNumber);
            		claim.setValue("SIUAdjusterRef", siuProvider.getSystemId());
            	}
            }
            
            // If Risk has Changed
            if( Transaction.hasRiskChanged(data, claim) ) {
            	
            	// Call ClaimPolicyInfoBuilder to Reload ClaimPolicyInfo Limits for New Risk
                ModelBean claimSetup = Claim.getClaimSetup(claim);
                ModelBean ibiz = claimSetup.getBean("ibiz");
                IBIZ.processSequence("CLTransactionRiskChange", ibiz, getHandlerData());
                
                // Re-populate the list of claims where this risk was not repaired
                String policyRiskIdRef = claim.gets("RiskIdRef");
                ModelBean policyRisk = claim.getBeanById(policyRiskIdRef);
                if( policyRisk!=null) {
	                String claimList = Claim.getClaimListWithUnrepairedRisk(data, claim, policyRisk.gets("RiskIdRef"));	        		
        			ModelBean firstPartyClaimant = claim.findBeanByFieldValue("Claimant", "ClaimantTypeCd", "First Party");
        			if( firstPartyClaimant!=null){
        				ModelBean[] propertyDamagedArray = firstPartyClaimant.getBeans("PropertyDamaged");
        				for( ModelBean propertyDamaged:propertyDamagedArray){
        					if( propertyDamaged.gets("PolicyRiskIdRef").equals(policyRiskIdRef) && (!propertyDamaged.gets("StatusCd").equals("Deleted") ) ){
        						propertyDamaged.setValue("ClaimsWithUnrepairedRisk", claimList);		        				
        					}	        					
        				}	        				
        			}        		
                }

            }
            
            // Save off Historical Transaction Date
            String historicalTransactionDt = ap.gets("HistoricalTransactionDt");
            if( !historicalTransactionDt.equals("") ) {
            	ModelBean claimTransactionInfo = claim.getBean("ClaimTransactionInfo");
            	String formattedHistoricalTransactionDt = StringDate.formatEntryDate(historicalTransactionDt, true);
            	claimTransactionInfo.setValue("HistoricalTransactionDt",formattedHistoricalTransactionDt);
            }
            
            // Return the Response ModelBean
            return rs;

        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
}

