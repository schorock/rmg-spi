package com.ric.workflow;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;

/** Inbox Item Object
 * 
 * @author moniquef
 *
 */
public class InboxItem extends com.iscs.workflow.InboxItem {
	
	public InboxItem(ModelBean task) {
		super(task);
	}

	/** Sort Inbox Item Array
	 * @param inboxItemArray Inbox Item Array to Sort
	 * @param orderBy Order By Array
	 * @param sortOrder Sort Order Array (i.e. Ascending, Descending)
	 * @return Sorted Inbox Item Array
	 */
	public static InboxItem[] sortList(InboxItem[] inboxItemArray, String[] orderByArray, String[] sortOrderArray) {
		
		final String[] orderBy = orderByArray;
		final String[] sortOrder = sortOrderArray;
		
		ArrayList<InboxItem> inboxItemList = new ArrayList<InboxItem>();
		for( InboxItem inboxItem : inboxItemArray ) 
			inboxItemList.add(inboxItem);
		
		Collections.sort(inboxItemList, new Comparator<Object>() {
			
			public int compare(Object obj1, Object obj2) throws ClassCastException {
				try {
					InboxItem firstInboxItem  = (InboxItem) obj1;
					InboxItem secondInboxItem  = (InboxItem) obj2;
					ModelBean firstBean  = firstInboxItem.getTask();
					ModelBean secondBean  = secondInboxItem.getTask();
					
					int comparisonValue = 0;
					for( int i = 0; i < orderBy.length; i++) { 
						if( orderBy[i].equals("AddDt") ) {
							if( sortOrder[i].equals("Ascending") )
								comparisonValue = firstBean.getDate("AddDt").compareTo(secondBean.getDate("AddDt"));
							else
								comparisonValue = secondBean.getDate("AddDt").compareTo(firstBean.getDate("AddDt"));
						} else if( orderBy[i].equals("AddedByUserId") ) {
							if( sortOrder[i].equals("Ascending") )
								comparisonValue = firstBean.gets("AddedByUserId").compareToIgnoreCase(secondBean.gets("AddedByUserId"));
							else
								comparisonValue = secondBean.gets("AddedByUserId").compareToIgnoreCase(firstBean.gets("AddedByUserId"));
						} else if( orderBy[i].equals("Attachment") ) {
							if( sortOrder.equals("Ascending") )
								comparisonValue = String.valueOf(firstInboxItem.hasAttachment()).compareTo(String.valueOf(secondInboxItem.hasAttachment()));
							else
								comparisonValue = String.valueOf(secondInboxItem.hasAttachment()).compareTo(String.valueOf(firstInboxItem.hasAttachment()));
						} else if( orderBy[i].equals("Description") ) {
							if( sortOrder[i].equals("Ascending") )
								comparisonValue = firstBean.gets("Description").compareToIgnoreCase(secondBean.gets("Description"));
							else
								comparisonValue = secondBean.gets("Description").compareToIgnoreCase(firstBean.gets("Description"));
						} else if( orderBy[i].equals("Escalated") ) { 
							if( sortOrder[i].equals("Ascending") ) 
								comparisonValue = String.valueOf(firstInboxItem.isEscalated()).compareTo(String.valueOf(secondInboxItem.isEscalated()));
							else
								comparisonValue = String.valueOf(secondInboxItem.isEscalated()).compareTo(String.valueOf(firstInboxItem.isEscalated()));
						} else if( orderBy[i].equals("CompleteDt") ) {
							if( sortOrder[i].equals("Ascending") )
								comparisonValue = firstBean.getDate("CompleteDt").compareTo(secondBean.getDate("CompleteDt"));
							else
								comparisonValue = secondBean.getDate("CompleteDt").compareTo(firstBean.getDate("CompleteDt"));
						} else if( orderBy[i].equals("CompletedByUserId") ) {
							if( sortOrder[i].equals("Ascending") )
								comparisonValue = firstBean.gets("CompletedByUserId").compareToIgnoreCase(secondBean.gets("CompletedByUserId"));
							else
								comparisonValue = secondBean.gets("CompletedByUserId").compareToIgnoreCase(firstBean.gets("CompletedByUserId"));
						} else if( orderBy[i].equals("CriticalDt") ) {
							if( sortOrder[i].equals("Ascending") )
								comparisonValue = firstBean.getDate("CriticalDt").compareTo(secondBean.getDate("CriticalDt"));
							else
								comparisonValue = secondBean.getDate("CriticalDt").compareTo(firstBean.getDate("CriticalDt"));
						} else if( orderBy[i].equals("Forwarded") ) { 
							if( sortOrder[i].equals("Ascending") ) 
								comparisonValue = String.valueOf(firstInboxItem.isTransferred()).compareTo(String.valueOf(secondInboxItem.isTransferred()));
							else
								comparisonValue = String.valueOf(secondInboxItem.isTransferred()).compareTo(String.valueOf(firstInboxItem.isTransferred()));
						} else if( orderBy[i].equals("Name") ) {
							if( sortOrder[i].equals("Ascending") )
								comparisonValue = firstBean.gets("Name").compareToIgnoreCase(secondBean.gets("Name"));
							else
								comparisonValue = secondBean.gets("Name").compareToIgnoreCase(firstBean.gets("Name"));
						} else if( orderBy[i].equals("OriginalOwner") ) {
							if( sortOrder[i].equals("Ascending") )
								comparisonValue = firstBean.gets("OriginalOwner").compareToIgnoreCase(secondBean.gets("OriginalOwner"));
							else
								comparisonValue = secondBean.gets("OriginalOwner").compareToIgnoreCase(firstBean.gets("OriginalOwner"));
						} else if( orderBy[i].equals("SeverityLevel")){
							String[] severityLevels = {"","P5","P4","P3","C2","C1","PL","P2","P1","S1"};
							Integer bean1SeverityLevel = 0, bean2SeverityLevel = 0;
							
							for( int j = 0; j < severityLevels.length; j++ ){
								if( firstBean.gets("SeverityLevel").equals( severityLevels[j] ) )
									bean1SeverityLevel = j;
								
								if( secondBean.gets("SeverityLevel").equals( severityLevels[j] ) )
									bean2SeverityLevel = j;
							}
							
							if( sortOrder[i].equals("Ascending") )
								comparisonValue = bean1SeverityLevel.compareTo(bean2SeverityLevel);
							else 
								comparisonValue = bean2SeverityLevel.compareTo(bean1SeverityLevel);
						} else if( orderBy[i].equals("OutOfOffice") ) { 
							if( sortOrder[i].equals("Ascending") ) 
								comparisonValue = String.valueOf(firstInboxItem.isForwarded()).compareTo(String.valueOf(secondInboxItem.isForwarded()));
							else
								comparisonValue = String.valueOf(secondInboxItem.isForwarded()).compareTo(String.valueOf(firstInboxItem.isForwarded()));
						} else if( orderBy[i].equals("Priority") ) {
							if( sortOrder[i].equals("Ascending") )
								comparisonValue = firstBean.getValueInt("Priority") - secondBean.getValueInt("Priority");
							else
								comparisonValue = secondBean.getValueInt("Priority") - firstBean.getValueInt("Priority");
						} else if( orderBy[i].equals("CurrentOwner") ) {
							if( sortOrder[i].equals("Ascending") )
								comparisonValue = firstBean.gets("CurrentOwner").compareToIgnoreCase(secondBean.gets("CurrentOwner"));
							else
								comparisonValue = secondBean.gets("CurrentOwner").compareToIgnoreCase(firstBean.gets("CurrentOwner"));
						} else if( orderBy[i].equals("Status") ) {
							if( sortOrder[i].equals("Ascending") )
								return firstBean.gets("Status").compareTo(secondBean.gets("Status"));
							else
								return secondBean.gets("Status").compareTo(firstBean.gets("Status"));
						} else if( orderBy[i].equals("WorkDt") ) {
							if( sortOrder[i].equals("Ascending") )
								comparisonValue = firstBean.getDate("WorkDt").compareTo(secondBean.getDate("WorkDt"));
							else
								comparisonValue = secondBean.getDate("WorkDt").compareTo(firstBean.getDate("WorkDt"));
						} else if( orderBy[i].equals("ReminderDt") ) {
							if( sortOrder[i].equals("Ascending") )
								comparisonValue = firstBean.getDate("ReminderDt").compareTo(secondBean.getDate("ReminderDt"));
							else
								comparisonValue = secondBean.getDate("ReminderDt").compareTo(firstBean.getDate("ReminderDt"));
						} else if( orderBy[i].equals("RepeatingInd") ) { 
							if( sortOrder[i].equals("Ascending") ) 
								comparisonValue = firstBean.gets("RepeatingInd").compareTo(secondBean.gets("RepeatingInd"));
							else
								comparisonValue = secondBean.gets("RepeatingInd").compareTo(firstBean.gets("RepeatingInd"));
						} else if( orderBy[i].equals("ReportTo") ) { 
							if( sortOrder[i].equals("Ascending") ) 
								comparisonValue = firstBean.gets("ReportTo").compareTo(secondBean.gets("ReportTo"));
							else
								comparisonValue = secondBean.gets("ReportTo").compareTo(firstBean.gets("ReportTo"));
						} else if( orderBy[i].equals("Viewed") ) { 
							if( sortOrder[i].equals("Ascending") ) 
								comparisonValue = String.valueOf(firstInboxItem.isRead()).compareTo(String.valueOf(secondInboxItem.isRead()));
							else
								comparisonValue = String.valueOf(secondInboxItem.isRead()).compareTo(String.valueOf(firstInboxItem.isRead()));
						} else {
							if( sortOrder[i].equals("Ascending") ) 
								comparisonValue = firstBean.gets(orderBy[i]).compareTo(secondBean.gets(orderBy[i]));
							else
								comparisonValue = secondBean.gets(orderBy[i]).compareTo(firstBean.gets(orderBy[i]));
						}
						
						if( comparisonValue != 0 ) 
                            break;
					}
					return comparisonValue;
				}
				catch (ModelBeanException mbe) {
					throw new ClassCastException("Error occurred trying to compare fields for sorting." + mbe.toString() );
				}
			}
		});
		
		return inboxItemList.toArray( new InboxItem[inboxItemList.size()]);
	}
}