package com.ric.workflow;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.inov.biz.server.ServiceContext;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCLookup;
import net.inov.tec.data.bean.FieldLookup;
import net.inov.tec.data.bean.FieldLookupKey;
import net.inov.tec.data.bean.SearchResult;
import net.inov.tec.date.StringDate;
import net.inov.tec.security.SecurityManager;
import net.inov.tec.security.userdata.UserData;
import net.inov.tec.xml.XmlDoc;

import org.jdom.Element;

import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDAXmlDoc;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.StringTools;
import com.iscs.workflow.InboxSearch.IndexedTaskField;
import com.iscs.workflow.InboxSearch.TaskLookupKey;
import com.iscs.workflow.Task;

/** Inbox search
 * 
 * @author kenb
 *
 */
public class InboxSearch {

    protected static ModelBean[] cachedForwardingUsers = null;
    protected static long cacheExpireTimestamp = 0;
	
	/** Get list of tasks for the Inbox
	 * @param user Current User ModelBean
	 * @param viewingUserId LoginId of the User viewing the Inbox
	 * @param asOfDate Date for determining active User Task Groups
	 * @param filterUnauthorizedTasks Indicates that unauthorized tasks should be filtered from result set
	 * @param keys Array of Lookup Keys for the query
	 * @param showHiddenTasks Indicates that the showHiddenTask InboxItem attribute should be set if applicable
	 * @param useManagerHierarchy Indicates that the inManagerHierarchy attribute should be set if applicable
	 * @return 
	 * 
     */    
    public InboxSearchResult performSearch(ModelBean user, String viewingUserId, StringDate asOfDate, boolean filterUnauthorizedTasks, List<TaskLookupKey> keys, boolean showHiddenTasks, boolean useManagerHierarchy, ModelBean inboxView) {
    	try {  
    		
    		// This flag will make sure the InboxSearch results are filtered not expanded by the AddByUserId key
    		boolean filterByAddUser = false;
    		if( inboxView != null && !inboxView.gets("FilterEnteredBy").equals("") )
    			filterByAddUser = true;
    		
    		FieldLookup lookup = new FieldLookup("Task");
    		
    		// Loop through all the keys to add them to the lookup
    		boolean directUserCodeSearch = false;
    		for(TaskLookupKey key : keys){
    			lookup.addLookupKey(key);
    			
    			// If the Task search field is "AddedByUserId", "OriginalOwner", or "CurrentOwner"
    			// then this is a direct user code search, so we will not be fetching any additional
    			// user codes to search on (This will expand the Inbox Results past the current user's
    			// Inbox).
    			// NOTE: Exclude the AddedByUserId if it's part of an Inbox Filter (We don't want to 
    			// expand the Inbox Results but filter it)
    			if(
    				   (key.getTaskField() == IndexedTaskField.AddedByUserId && !filterByAddUser) ||
    					key.getTaskField() == IndexedTaskField.OriginalOwner ||
    					key.getTaskField() == IndexedTaskField.CurrentOwner ||
    					key.getTaskField() == IndexedTaskField.OpenCurrentOwner){
    			
    				directUserCodeSearch = true;
    				
    			}
    		}
    		
    		// If a specific user code search for any of the user code fields has not been ordered,
    		// then restrict the user codes to the user's id, task group codes, delegation user codes, etc.
    		if(!directUserCodeSearch){
    			Set<String> ownerCodes =  getAllTaskOwnerCodes( user, asOfDate );
    			TaskLookupKey currentOwner = new TaskLookupKey(IndexedTaskField.CurrentOwner, ownerCodes.toArray());
    			lookup.addLookupKey(currentOwner);
    		}
    		
    		// Exclude any hidden tasks unless the user has asked for them
    		if( !showHiddenTasks ) {
    			String[] hiddenTemplateIds = getHiddenTaskTemplateIds();
    			if (hiddenTemplateIds.length > 0) {
	    			FieldLookupKey lookupKey = lookup.addListLookupKey("TemplateId", hiddenTemplateIds);
	    			lookupKey.setLookupType(JDBCLookup.LOOKUP_NOT_IN_LIST);
    			}
    		}
    		
    		lookup.getLookupConfig().setPreFetchBeans(true);
    		lookup.getLookupConfig().setMiniBeanMode(InboxSettings.getSettings().useTaskMiniBeans());
    		
    		// Get Tasks
    		ServiceContext context = ServiceContext.getServiceContext();
    		SearchResult results = context.getTransaction().performLookup(lookup);
    		ModelBean[] taskArray = results.getBeans();
    		
    		List<InboxItem> inboxItemList = new ArrayList<InboxItem>();
    		for( ModelBean task : taskArray ) 
    			inboxItemList.add(new InboxItem(task));
    		
    		// Create InboxSearchResult Objects
    		InboxSearchResult inboxSearchResult = new InboxSearchResult();
    		inboxSearchResult.getInboxItemList().addAll(inboxItemList);
    		inboxSearchResult.setUser(user);
    		inboxSearchResult.setAsOfDate(asOfDate);
    		
    		// Filter out unauthorized tasks
    		if( filterUnauthorizedTasks ) 
    			inboxSearchResult.filterUnauthorizedTasks();
    		
    		// Set Inbox Item Flags
    		inboxSearchResult.setInboxItemFlags(user, viewingUserId, useManagerHierarchy, asOfDate);
    		
    		// If the current 'Inbox View' is filtering by 'Source'
    		if( inboxView != null && !inboxView.gets("FilterBySource").equals("") ) {
    			inboxSearchResult.filterBySource(inboxView.gets("FilterBySource"));
    		}
    		
    		// If the current 'Inbox View' is filtering by 'Description'
    		if( inboxView != null && !inboxView.gets("FilterDescription").equals("") ) {
    			inboxSearchResult.filterByDescription(inboxView.gets("FilterDescription"));
    		}
    		
    		// Filter by Task Attributes
    		if( inboxView != null ) {
	            ModelBean[] inboxViewFilters = inboxView.getBeans("InboxViewFilter");
	            for( ModelBean inboxViewFilter : inboxViewFilters ) {
	            	
	            	if( !inboxViewFilter.gets("FilterValue").equals("") ) {
	            		
	            		inboxSearchResult.filterByAttribute(inboxViewFilter.gets("FilterName"),inboxViewFilter.gets("FilterValue"));
	            	}
	            }
    		}
    		
    		return inboxSearchResult;
        }
        catch( Exception e ) {
            e.printStackTrace();
            Log.error(e);
            return null;
        }
    }

	/** Get a list of task templates that are normally hidden.
	 * @throws Exception 
	 */
	public String[] getHiddenTaskTemplateIds() throws Exception {
		
		List<String> ids = new ArrayList<String>();
		
		ModelBean[] templates = Task.getAllTaskTemplates();
		for( ModelBean template : templates ) {
			if( template.gets("ShowOnInboxInd").equalsIgnoreCase("No") ) 
				ids.add(template.getId());
		}
		
		return ids.toArray(new String[0]);
	}
	
	/** Get list of tasks for the Inbox
	 * @param user 
	 * @param asOfDate
	 * @param filterUnauthorizedTasks  
	 * @return 
	 * @throws Exception 
	 * 
     */    
    public InboxSearchResult getOpenTasks(ModelBean user, StringDate asOfDate, boolean filterUnauthorizedTasks) throws Exception {
	

		TaskLookupKey openWorkDt = new TaskLookupKey(IndexedTaskField.OpenWorkDt, asOfDate, FieldLookup.LOOKUP_LESS_THAN_OR_EQUALS);
		
		List<TaskLookupKey> keys = new ArrayList<TaskLookupKey>();
		keys.add(openWorkDt);
		
		return performSearch(user, user.gets("LoginId"), asOfDate, filterUnauthorizedTasks, keys, true, false, null);

    }

    /** Get a List of Task Current Owners that Should be Visible to the Current User 
     * @param userInfo Current UserInfo ModelBean
     * @param todayDt Today's Date
     * @return List of Task Current Owners
     * @throws Exception if an Unexpected Error Occurs
     */
    public Set<String> getAllTaskOwnerCodes(ModelBean userInfo, StringDate todayDt) throws Exception{
    	    	
    	// Initialize Owner and Group Lists
    	Set<String> ownerList = new HashSet<String>();
    	
    	// Add Current UserId to the Owner List 
    	String currentUserId = userInfo.gets("LoginId");    	 
    	ownerList.add(ModelSpecification.indexString(currentUserId));
    	
        // Add Current User Task Groups to the Owner List
        ModelBean[] currentUserTaskGroupArray = userInfo.getAllBeans("UserTaskGroup");
        for( ModelBean currentUserTaskGroup : currentUserTaskGroupArray ) {
        	if( DateTools.between(todayDt, currentUserTaskGroup.getDate("StartDt"), currentUserTaskGroup.getDate("EndDt")) ) {
        		String taskGroupCd = currentUserTaskGroup.gets("TaskGroupCd");
        		String taskGroupCdIndexed = ModelSpecification.indexString(taskGroupCd);
        		if( !ownerList.contains(taskGroupCdIndexed) )
        				ownerList.add(taskGroupCdIndexed);
            }
        }
                
        // Get All Out of Office Users 
        ModelBean[] outOfOfficeUserArray = getAllForwardingUsers(todayDt);
        
        filterOutOfOfficeUsers(outOfOfficeUserArray, userInfo, ownerList, todayDt, true);
                
    	return ownerList;
    }
    
    /** Get a list of users Forwarding (via Out of Office) to anyone.
     * Note: To improve performance, this method will cache the results in the ServiceContext object cache
     * for the life of the 1 request. This then implies that any changes to out-of-office forwarding that happen
     * (from other threads) during the 1 request will not be seen. This seems a very reasonable compromise.
	 * @param todayDt
	 * @return List of forwarding UserInfo beans
	 * @throws Exception
	 */
	public ModelBean[] getAllForwardingUsers(StringDate todayDt) throws Exception {
		
		long currentTimestamp = System.currentTimeMillis();
		if (cacheExpireTimestamp == 0 || cachedForwardingUsers == null || currentTimestamp >= cacheExpireTimestamp) {
			// Cached forwarding users has expired or was never set. Refresh
    		Log.debug("Refreshing global cache of out-of-office UserInfos");
			cachedForwardingUsers = SecurityManager.getSecurityManager().searchUsers(
	        		new String[] {"TransferStartDt", "TransferEndDt"}, 
	        		new int[] {UserData.LOOKUP_LESS_THAN_OR_EQUALS, UserData.LOOKUP_GREATER_THAN_OR_EQUALS},
	        	    new String[] {todayDt.toString(), todayDt.toString()}, 0);
			cacheExpireTimestamp = System.currentTimeMillis() + (InboxSettings.getSettings().getOutOfOfficeUserCacheExpirationSeconds() * 1000);
		} else {
    		Log.debug("Retrieved out-of-office UserInfos from global cache");
		}
		
		return cachedForwardingUsers;
		
	}
    
    /** Get a list of users Forwarding (via Out of Office) to the current user 
     * @param userInfo Current UserInfo ModelBean
     * @param todayDt Today's Date
     * @return List of forwarding Users
     * @throws Exception if an Unexpected Error Occurs
     */
    public Set<String> getAllForwardingUsers(ModelBean userInfo, StringDate todayDt) throws Exception{
    	
    	// Initialize Owner and Group Lists
    	Set<String> ownerList = new HashSet<String>();
    	
    	ModelBean[] outOfOfficeUserArray = getAllForwardingUsers(todayDt);
        
        filterOutOfOfficeUsers(outOfOfficeUserArray, userInfo, ownerList, todayDt, true);
        
        return ownerList;
    }
    
    /** <p>Invalidate the cache of users Forwarding (via Out of Office) to anyone.</p>
     * 
     * <p><strong>IMPORTANT</strong>: This method only invalidate's the local app server's cache. Other app servers may have also cached the users and those
     * caches are NOT invalidated by this method.</p>
     */
    public void invalidateForwardingUsersCache() {
    	cachedForwardingUsers = null;
    	cacheExpireTimestamp = 0;
    }
    
    /* Recursively filter an array of Out-Of-Office users based on the user passed in
     * The method performs the following tests:
     * 1. Any Out of Office user delegating to the current user
     * 2. Any Out of Office user delegating to a group the current user is a member of
     * 3. Any Out of Office user's group delegating to the current user
     * 4. Any Out of Office user's group delegating to a group the current user is a member of
     * 
     * @param outOfOfficeUserArray Array of users that currently have out of office forwarding 
     * @param userInfo User who may be the recipient of out of office forwarding
     * @param ownerList List of task owner names
     * @param groupList List of task group names
     * @param todayDt Current date
    */
    public static void filterOutOfOfficeUsers(ModelBean[] outOfOfficeUserArray, ModelBean userInfo, Set<String> ownerList, StringDate todayDt, boolean firstIteration) 
    throws Exception {
    	    	
    	// Get the current userId
    	String currentUserId = userInfo.gets("LoginId");
    	    
    	Set<String> groupList = new HashSet<String>();
    	
    	// Add Current User Task Groups to the Group List
        ModelBean[] currentUserTaskGroupArray = userInfo.getAllBeans("UserTaskGroup");
        for( ModelBean currentUserTaskGroup : currentUserTaskGroupArray ) {
        	if( DateTools.between(todayDt, currentUserTaskGroup.getDate("StartDt"), currentUserTaskGroup.getDate("EndDt")) ) {
        		String taskGroupCd = currentUserTaskGroup.gets("TaskGroupCd");
        		String taskGroupCdIndexed = ModelSpecification.indexString(taskGroupCd);

                if( !groupList.contains(taskGroupCdIndexed) ) 
                    groupList.add(taskGroupCdIndexed);
            }
        }
            	
    	// Loop through Out of Office user array looking for the current user
        for( ModelBean outOfOfficeUser : outOfOfficeUserArray ) {
        	String outOfOfficeUserId = outOfOfficeUser.gets("LoginId");
        	String outOfOfficeUserIdIndexed = ModelSpecification.indexString(outOfOfficeUserId);
        	ModelBean outOfOfficeUserTaskControl = outOfOfficeUser.getBean("UserTaskControl");
        	
        	// If Out of Office User Tasks Have Been Delegated to the Current User (and not delegated to itself)
        	String delegateUserId = outOfOfficeUserTaskControl.gets("TransferToUser");
            if( delegateUserId.equalsIgnoreCase(currentUserId) && !delegateUserId.equals(outOfOfficeUserId) ) {
            	
                // Add Out of Office UserId to Owner List
            	if( !ownerList.contains(outOfOfficeUserIdIndexed) ) {
                    ownerList.add(outOfOfficeUserIdIndexed);    
            	
                    // Re-run the out of office filtering for this user
                    filterOutOfOfficeUsers( outOfOfficeUserArray, outOfOfficeUser, ownerList, todayDt, false);
            	}
            }
            
            // If Out of Office User Tasks Have Been Delegated to a Group Which the Current User is a Member
            String delegateGroupCd = outOfOfficeUserTaskControl.gets("TransferTo");
            if( firstIteration && groupList.contains(ModelSpecification.indexString(delegateGroupCd)) ) {
            	
            	// Add Out of Office UserId to Owner List
            	if( !ownerList.contains(outOfOfficeUserIdIndexed) ) { 
                    ownerList.add(outOfOfficeUserIdIndexed);
            	
                    // Re-run the out of office filtering for this user
                    filterOutOfOfficeUsers( outOfOfficeUserArray, outOfOfficeUser, ownerList, todayDt, false);
            	}
            }
            
            // Loop Through the Out of Office User Task Groups
            ModelBean[] outOfOfficeUserTaskGroupArray = outOfOfficeUserTaskControl.getBeans("UserTaskGroup");
            for( ModelBean outOfOfficeUserTaskGroup : outOfOfficeUserTaskGroupArray ) {
            	
            	// Check if Out of Office Task Group Transfer is Indicated
            	if( StringTools.isTrue(outOfOfficeUserTaskGroup.gets("TransferInd")) ) {
            		
	            	String outOfOfficeUserTaskGroupCd = outOfOfficeUserTaskGroup.gets("TaskGroupCd");
	            	String outOfOfficeUserTaskGroupCdIndexed = ModelSpecification.indexString(outOfOfficeUserTaskGroupCd);
	            	String transferToCd = outOfOfficeUserTaskGroup.gets("TransferToCd");
	            	String transferTo = outOfOfficeUserTaskGroup.gets("TransferTo");
	            	
	            	// If Out of Office User Group Tasks Have Been Delegated to the Current User
	            	if( transferToCd.equalsIgnoreCase("User") ) {
	            		if( transferTo.equalsIgnoreCase(currentUserId) ) {
	            			
	            			// Add Out of Office User Task Group to Owner List
	            			if( !ownerList.contains(outOfOfficeUserTaskGroupCdIndexed) )  {
	                            ownerList.add(outOfOfficeUserTaskGroupCdIndexed);          
	                            
	                            filterOutOfOfficeGroups(outOfOfficeUserArray, outOfOfficeUserTaskGroup, ownerList, todayDt, false);
	            			}
	            		}
	            	}
	            	
	            	// If Out of Office User Group Tasks Have Been Delegated to a Group Which the Current User is a Member
	            	if( firstIteration && transferToCd.equalsIgnoreCase("Group") ) {
	            		if( groupList.contains(ModelSpecification.indexString(transferTo)) ) {
	            			
	            			// Add Out of Office User Task Group to Owner List
	            			if( !ownerList.contains(outOfOfficeUserTaskGroupCdIndexed) ) {
	                            ownerList.add(outOfOfficeUserTaskGroupCdIndexed);    
	                            
	                            filterOutOfOfficeGroups(outOfOfficeUserArray, outOfOfficeUserTaskGroup, ownerList, todayDt, false);
	            			}
	            		}
	            	}
            	}
            }
        }
    }
    
    /* Recursively filter an array of Out-Of-Office users based on the group passed in
     * * The method performs the following tests:
     * 1. Any Out of Office user delegating to the current group
     * 3. Any Out of Office user's group delegating to the current group
     * 
     * @param outOfOfficeUserArray Array of users that currently have out of office forwarding 
     * @param taskGroup Task Group who may be the recipient of out of office forwarding
     * @param ownerList List of task owner names
     * @param groupList List of task group names
     * @param todayDt Current date
    */
    private static void filterOutOfOfficeGroups(ModelBean[] outOfOfficeUserArray, ModelBean taskGroup, Set<String> ownerList, StringDate todayDt, boolean firstIteration) 
    throws Exception {

    	String groupCd = taskGroup.gets("TaskGroupCd");
    	            	
    	// Loop through Out of Office user array looking for the current group
        for( ModelBean outOfOfficeUser : outOfOfficeUserArray ) {
        	String outOfOfficeUserId = outOfOfficeUser.gets("LoginId");
        	String outOfOfficeUserIdIndexed = ModelSpecification.indexString(outOfOfficeUserId);
        	ModelBean outOfOfficeUserTaskControl = outOfOfficeUser.getBean("UserTaskControl");
        	            
            // If Out of Office User Tasks Have Been Delegated to the current Group
            String delegateGroupCd = outOfOfficeUserTaskControl.gets("TransferTo");
            if( delegateGroupCd.equals(groupCd) ) {
            	
            	// Add Out of Office UserId to Owner List
            	if( !ownerList.contains(outOfOfficeUserIdIndexed) ) { 
                    ownerList.add(outOfOfficeUserIdIndexed);
            	
                    // Re-run the out of office filtering for this user
                    filterOutOfOfficeUsers( outOfOfficeUserArray, outOfOfficeUser, ownerList, todayDt, false);
            	}
            }
            
            // Loop Through the Out of Office User Task Groups
            ModelBean[] outOfOfficeUserTaskGroupArray = outOfOfficeUserTaskControl.getBeans("UserTaskGroup");
            for( ModelBean outOfOfficeUserTaskGroup : outOfOfficeUserTaskGroupArray ) {
            	
            	// Check if Out of Office Task Group Transfer is Indicated
            	if( StringTools.isTrue(outOfOfficeUserTaskGroup.gets("TransferInd")) ) {
            		
	            	String outOfOfficeUserTaskGroupCd = outOfOfficeUserTaskGroup.gets("TaskGroupCd");
	            	String outOfOfficeUserTaskGroupCdIndexed = ModelSpecification.indexString(outOfOfficeUserTaskGroupCd);
	            	String transferToCd = outOfOfficeUserTaskGroup.gets("TransferToCd");
	            	String transferTo = outOfOfficeUserTaskGroup.gets("TransferTo");
	            	   		            	
	            	// If Out of Office User Group Tasks Have Been Delegated to the current group
	            	if( transferToCd.equalsIgnoreCase("Group") ) {
	            		if( transferTo.equals(groupCd) ) {
	            			
	            			// Add Out of Office User Task Group to Owner List
	            			if( !ownerList.contains(outOfOfficeUserTaskGroupCdIndexed) ) {
	                            ownerList.add(outOfOfficeUserTaskGroupCdIndexed);    
	                            
	                            filterOutOfOfficeGroups(outOfOfficeUserArray, outOfOfficeUserTaskGroup, ownerList, todayDt, false);
	            			}
	            		}
	            	}
            	}
            }
        }
    }
	
    
    /** Inbox settings class
     * 
     * @author myronm
     *
     */
    public static class InboxSettings {
    	
    	public static final String INBOX_SETTINGS_UMOL = "inbox-settings::*::*";
    		
    	protected boolean useTaskMiniBeans = true;
    	protected long outOfOfficeUserCacheExpirationSeconds = 0;
    	
    	private InboxSettings() {
    		try {
				MDAXmlDoc object = (MDAXmlDoc) Store.getModelObject(INBOX_SETTINGS_UMOL);
				XmlDoc setting = object.getDocument();
				
				Element el = setting.selectSingleElement("//InboxSettings");
				if( el == null )
					throw new Exception("Cannot understand " + INBOX_SETTINGS_UMOL);
				
				useTaskMiniBeans = StringTools.isTrue(el.getAttributeValue("UseTaskMini", "Yes"));
				outOfOfficeUserCacheExpirationSeconds = Long.parseLong(el.getAttributeValue("OutOfOfficeUserCacheExpirationSeconds", "0"));
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
    	}
    	
    	public static InboxSettings getSettings() {
    		return InboxSettingsHolder.INSTANCE;
    	}
    	
    	private static class InboxSettingsHolder {
    		public static final InboxSettings INSTANCE = new InboxSettings();
    	}

		/**
		 * @return the useTaskMiniBeans
		 */
		public boolean useTaskMiniBeans() {
			return useTaskMiniBeans;
		}

		/**
		 * @return the outOfOfficeUserCacheExpirationSeconds
		 */
		public long getOutOfOfficeUserCacheExpirationSeconds() {
			return outOfOfficeUserCacheExpirationSeconds;
		}
    	
    }
    
}
