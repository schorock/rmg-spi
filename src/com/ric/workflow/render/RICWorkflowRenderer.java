package com.ric.workflow.render;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.iscs.common.admin.calendar.Calendar;
import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDATemplate;
import com.iscs.common.render.DateRenderer;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.workflow.InboxSearch.IndexedTaskField;
import com.iscs.workflow.InboxSearch.TaskLookupKey;
import com.iscs.workflow.render.WorkflowRenderer;
import com.ric.workflow.InboxItem;
import com.ric.workflow.InboxSearch;
import com.ric.workflow.InboxSearchResult;

import net.inov.mda.MDAOption;
import net.inov.mda.MDATable;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.data.bean.FieldLookup;
import net.inov.tec.date.StringDate;

public class RICWorkflowRenderer extends com.iscs.workflow.render.WorkflowRenderer {

	/** Get Current Open Inbox List
	 * @param data JDBCData Connection Object
	 * @param userToDisplay UserInfo of user to display (not necessarily logged in user)
	 * @param userInfo UserInfo ModelBean (logged in user)
	 * @param todayDt Today's Date
	 * @param showHiddenTasks Include/Exclude Hidden Tasks
	 * @param rs Response ModelBean
	 * @param keys Task Lookup Keys
	 * @return Inbox Item Object Array
	 */
	public static InboxItem[] getCurrentOpenInboxList(JDBCData data, ModelBean userToDisplay, ModelBean userInfo, StringDate todayDt, boolean showHiddenTasks, ModelBean rs, List<TaskLookupKey> keys) {
		try {  
			// Initialize InboxSearch
			InboxSearch inboxSearch = new InboxSearch();

			// Include any up coming weekends/holidays
			ModelBean template = null;
			MDATemplate templateMDA = (MDATemplate) Store.getModelObject("calendar-template", "CalendarTemplate", "Inbox");
			if( templateMDA != null ){	
				template = templateMDA.getBean();	
			} else {
				throw new Exception("Calendar template for Inbox not found");    			
			}
			StringDate nextBusinessDt = Calendar.getNextBusinessDate(data, todayDt, template);
			StringDate queryDt = StringDate.advanceDate(nextBusinessDt, -1);

			// Build Lookup Keys
			if( keys == null ) {
				keys = new ArrayList<TaskLookupKey>();
			}
			Set<String> ownerCodes = inboxSearch.getAllTaskOwnerCodes(userToDisplay, queryDt);
			TaskLookupKey key = new TaskLookupKey(IndexedTaskField.OpenCurrentOwner, ownerCodes.toArray());
			keys.add(key);
			key = new TaskLookupKey(IndexedTaskField.OpenWorkDt, queryDt.toString(), FieldLookup.LOOKUP_LESS_THAN_OR_EQUALS);
			keys.add(key);

			// Build any ordering
			String orderBy = "";
			String sortOrder = "Ascending";
			if (rs != null) {
				ModelBean responseParams = rs.getBean("ResponseParams");
				ModelBean sortOverrideIndParam = responseParams.getBean("AdditionalParams").getBean("Param","Name","SortOverrideInd");
				if( sortOverrideIndParam != null && sortOverrideIndParam.valueEquals("Value", "Yes") ) {
					ModelBean orderByParam = responseParams.getBean("AdditionalParams").getBean("Param","Name","OrderBy");
					if( orderByParam != null ) {
						orderBy = orderByParam.gets("Value");
					}
				}
				ModelBean sortOrderParam = responseParams.getBean("AdditionalParams").getBean("Param","Name","SortOrder");
				if( sortOrderParam != null ) {
					sortOrder = sortOrderParam.gets("Value");
				}
			}

			// Perform Search
			InboxSearchResult inboxSearchResult = inboxSearch.performSearch(userToDisplay, userInfo.gets("LoginId"), todayDt, true, keys, showHiddenTasks, false, null);
			InboxItem[] inboxItemArray = inboxSearchResult.getInboxItemList().toArray(new InboxItem[inboxSearchResult.getInboxItemList().size()]);
			Log.info("Result Inbox Item Count: " + inboxItemArray.length);

			// Sort List
			if( !orderBy.equals("") ) {
				inboxItemArray = InboxItem.sortList(inboxItemArray, new String[] {orderBy}, new String[] {sortOrder});
			} else {
				inboxItemArray = InboxItem.sortList(inboxItemArray, new String[] {"WorkDt", "Priority"}, new String[] {"Ascending", "Ascending"});
			}

			return inboxItemArray;
		}
		catch( Exception e ) {
			e.printStackTrace();
			Log.error(e);
			return null;
		}
	}

	/** Get Current Open Inbox List. Assumes the inbox being viewed is for the user viewing it.
	 * If one user is viewing the inbox of another (trusted inbox viewer / buddy user feature),
	 * use {@link WorkflowRenderer#getCurrentOpenInboxList(JDBCData, ModelBean, ModelBean, StringDate)}.
	 * @param data JDBCData Connection Object
	 * @param userInfo UserInfo ModelBean
	 * @param todayDt Today's Date
	 * @return Inbox Item Object Array
	 */
	public static InboxItem[] getCurrentOpenInboxList(JDBCData data, ModelBean userInfo, StringDate todayDt) {
		return getCurrentOpenInboxList(data, userInfo, userInfo, todayDt, null);
	}

	/** Get Current Open Inbox List. Assumes the inbox being viewed is for the user viewing it.
	 * If one user is viewing the inbox of another (trusted inbox viewer / buddy user feature),
	 * use {@link WorkflowRenderer#getCurrentOpenInboxList(JDBCData, ModelBean, ModelBean, StringDate, ModelBean)}.
	 * @param data JDBCData Connection Object
	 * @param userInfo UserInfo ModelBean
	 * @param todayDt Today's Date
	 * @param rs Response ModelBean
	 * @return Inbox Item Object Array
	 */
	public static InboxItem[] getCurrentOpenInboxList(JDBCData data, ModelBean userInfo, StringDate todayDt, ModelBean rs) {
		return getCurrentOpenInboxList(data, userInfo, userInfo, todayDt, rs);
	}

	/** Get Current Open Inbox List
	 * @param data JDBCData Connection Object
	 * @param userToDisplay UserInfo of user to display (not necessarily logged in user)
	 * @param userInfo UserInfo ModelBean (logged in user)
	 * @param todayDt Today's Date
	 * @return Inbox Item Object Array
	 */
	public static InboxItem[] getCurrentOpenInboxList(JDBCData data, ModelBean userToDisplay, ModelBean userInfo, StringDate todayDt) {
		return getCurrentOpenInboxList(data, userToDisplay, userInfo, todayDt, null);
	}

	/** Get Current Open Inbox List
	 * @param data JDBCData Connection Object
	 * @param userToDisplay UserInfo of user to display (not necessarily logged in user)
	 * @param userInfo UserInfo ModelBean (logged in user)
	 * @param todayDt Today's Date
	 * @param rs Response ModelBean
	 * @return Inbox Item Object Array
	 */	
	public static InboxItem[] getCurrentOpenInboxList(JDBCData data, ModelBean userToDisplay, ModelBean userInfo, StringDate todayDt, ModelBean rs) {
		return getCurrentOpenInboxList(data, userToDisplay, userInfo, todayDt, false, rs);
	}
	
	/** Get Current Open Inbox List
	 * @param data JDBCData Connection Object
	 * @param userToDisplay UserInfo of user to display (not necessarily logged in user)
	 * @param userInfo UserInfo ModelBean (logged in user)
	 * @param todayDt Today's Date
	 * @param showHiddenTasks Include/Exclude Hidden Tasks
	 * @param rs Response ModelBean
	 * @return Inbox Item Object Array
	 */
	public static InboxItem[] getCurrentOpenInboxList(JDBCData data, ModelBean userToDisplay, ModelBean userInfo, StringDate todayDt, boolean showHiddenTasks, ModelBean rs) {
		return getCurrentOpenInboxList(data, userToDisplay, userInfo, todayDt, showHiddenTasks, rs, null);
	}
	
    /** Get the list of columns to display on the Inbox
     * 
     * @param list List of columns from an 'Inbox View'
     * @param userInfo UserInfo Model Bean
     * @param
     * @return The InboxView column list or the default column list
     * @throws Exception
     */
    public static String[] getColumnList(String list, ModelBean userInfo, StringDate todayDt) throws Exception {

    	// Initialize Variables
    	ArrayList<String> array = new ArrayList<String>();

    	if (list.equals("")) {
        	// Get the default column list from the coderef
	        MDATable table = (MDATable) Store.getModelObject("WF::task::inboxcolumns::default");
	        MDAOption[] options = table.getOptions();
	    	
	        for( MDAOption opt : options ) {
	        	if ("SeverityLevel".equals(opt.getValue())) {
	        		if (hasClaimUserRole(userInfo, todayDt)) {
	        			array.add(opt.getValue());
	        		} else {
	        			continue;
	        		}
	        	} else {
	        		array.add(opt.getValue());
	        	}
	        }

    	} else {
    		// Build the column list from the Inbox View
    		String[] split = list.split(",");
    		for( String str : split ) {
    			array.add(str);
    		}
    	}
    	
    	// Return the column list
        return (String[]) array.toArray(new String[array.size()]);
    }
    
    /**
     * Returns the Policy Number tied to the Task (if any)
     * @param data JDBCData
     * @param task Task Model Bean
     * @return Policy Number
     */
    public static String getPolicyNumberForInboxView(JDBCData data, ModelBean task) {
    	
    	if (task == null) {
    		return "";
    	}
    	
    	try {
    		ModelBean taskLink = task.getBean("TaskLink");
    		if (taskLink != null) {
    			if ("Claim".equals(taskLink.gets("ModelName"))) {
    				ModelBean claim = new ModelBean("Claim");
    				data.selectModelBean(claim, taskLink.gets("IdRef"));
    				
    				if (claim.getBean("ClaimPolicyInfo") != null) {
    					return claim.getBean("ClaimPolicyInfo").gets("PolicyNumber");
    				}
    			} else if (StringRenderer.in(taskLink.gets("ModelName"), "Application,Policy")) {
    				String policyNumber = taskLink.gets("SearchValue");
    				// Remove policy term for consistency
    				if (!"".equals(policyNumber)) {
    					String[] policyNumberParts = policyNumber.split("-");
    					return policyNumberParts[0];
    				}

    				return taskLink.gets("SearchValue");
    			}
    		}
    		
    	} catch (Exception e) {
    		Log.error("Unable to retrieve policy number from task.");
    		e.printStackTrace();
    	}
    	
    	return "";
    }
    
    /**
     * Returns TRUE if the user has an active claim role
     * @param userInfo UserInfo ModelBean
     * @param todayDt Current Date
     * @return boolean flag
     */
    public static boolean hasClaimUserRole(ModelBean userInfo, StringDate todayDt) {
    	
    	if (userInfo == null || todayDt == null) {
    		return false;
    	}
    	
    	// Claim Role Array
    	String[] claimRoles = new String[] {
    			"CAT", "ClaimsAssistant", "ClaimsManager", "ExternalAdjuster", "PropertyInternalAdjuster" ,"CasualtyInternalAdjuster",
    			"SIU", "TelephoneAdjuster", "Adjuster", "RegionalManager", "ClaimManager", "Everything"
    	};
    	
    	try {
    		ModelBean[] userRoles = userInfo.getAllBeans("UserRole");
    		if (userRoles != null && userRoles.length != 0) {
    			for (ModelBean userRole : userRoles) {
    				for (String claimRole : claimRoles) {
    					// If the user has an active claim role, return TRUE.
    					if (userRole.gets("AuthorityRoleIdRef").equals(claimRole)
    							&& DateRenderer.greaterThanEqual(todayDt, userRole.getDate("StartDt"))
    									&& DateRenderer.lessThanEqual(todayDt, userRole.getDate("EndDt"))) {
    						return true;
    					}
    				}
    			}
    		}
    		
    	} catch (Exception e) {
    		Log.error("Unable to determine claim user role.");
    		e.printStackTrace();
    	}
    	
    	return false;
    }
    
    /**
     * Returns TRUE if the agent is the owner of the task.
     * Returns TRUE for non-agent users.
     * 
     * @param task Task ModelBean
     * @param userInfo UserInfo ModelBean
     * @param todayDt Current Date
     * @return boolean flag
     * @throws Exception upon error
     */
    public static boolean isAgentTaskOwner(ModelBean task, ModelBean userInfo, StringDate todayDt)
    throws Exception {
    	
    	if (task == null || userInfo == null || todayDt == null) {
    		return false;
    	}
    	
    	if (hasAuthorityRole(userInfo, "PolicyAgent", todayDt)) {
    		return task.gets("CurrentOwner").equals(userInfo.gets("LoginId"));
    	} else {
    		// Return TRUE if user is not an agent since the validation is only for users with agent roles.
    		return true;
    	}
    }
    
	/**
	 * Returns TRUE if the user has active authority
	 * 
	 * @param userInfo UserInfo ModelBean
	 * @param authorityRole Authority Role to verify
	 * @param todayDt Current Date
	 * @return boolean flag
	 */
    public static boolean hasAuthorityRole(ModelBean userInfo, String authorityRole, StringDate todayDt) {
    	try {
    		ModelBean[] userRoles = userInfo.getAllBeans("UserRole");
    		if (userRoles != null && userRoles.length != 0) {
    			for (ModelBean userRole : userRoles) {
    				if (userRole.gets("AuthorityRoleIdRef").equals(authorityRole)
    						&& DateRenderer.greaterThanEqual(todayDt, userRole.getDate("StartDt"))
    						&& DateRenderer.lessThanEqual(todayDt, userRole.getDate("EndDt"))) {
    					return true;
    				}
    			}
    		}
    		
    	} catch (Exception e) {
    		Log.error("Unable to verify authority role.");
    		e.printStackTrace();
    	}
    	
    	return false;
    }
    	
}
