package com.ric.workflow.processor;

import java.sql.SQLException;

import com.iscs.common.render.DateRenderer;
import com.iscs.common.tech.log.Log;
import com.iscs.common.tech.service.ModelBeanProcessorBaseSPI;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.StringTools;
import com.iscs.workflow.Task;
import com.iscs.workflow.render.WorkflowRenderer;

import net.inov.biz.server.IBIZXmlController;
import net.inov.tec.batch.MBPException;
import net.inov.tec.batch.MBPLookupSpec;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

/**
 * @author nvodapally
 */
public class UWReminderTaskProcessor extends ModelBeanProcessorBaseSPI {

  public UWReminderTaskProcessor() throws Exception {
    super();
    this.spec = ModelSpecification.getSharedModel();
  }

  /** Bean processor main routine.  Each bean in the data repository table will be presented to this processor.  The criteria
   * for task escalation will be handled and then a resultant success or fail indicator will be passed back.
   * @param application Each application bean in the data repository that is passed for evaluation
   * @param data JDBC data repository connection
   * @return boolean of whether this bean has been processed or not
   * @throws MBPException when an error has occurred.
   */
  public boolean processBean(ModelBean policy, JDBCData data) throws MBPException {
	  try {
		  // Log a Greeting
		  Log.debug("Processing UWReminderTaskProcessor...");
		  String owner = policy.gets("UpdateUser");
		  boolean uwTaskCreated = false;

		  ModelBean basicPolicy = policy.getBean("BasicPolicy");
		  String manualRenewalInd = basicPolicy.gets("ManualRenewalInd");

		  if(StringTools.isTrue(manualRenewalInd)) {
			  StringDate expirationDate = basicPolicy.getDate("ExpirationDt");
			  StringDate runDt = getRunDate();
			  int diffDays = (int) DateRenderer.diffDays(expirationDate, runDt);
			  if(diffDays >= 36 && diffDays <= 70) {
				  ModelBean reminderTask2018 = Task.getTask(data, "PolicyTask2018", policy, "Open");
				  if(reminderTask2018 == null) {
					  ModelBean task = Task.createTask(data, "PolicyTask2018", new ModelBean[] {policy}, null, null,  getRunDate(), null, owner);
					  Task.insertTask(data, task);
				  }
				  uwTaskCreated = true;
			  }
			  if(diffDays <= 35) {
				  ModelBean reminderTask2020 = Task.getTask(data, "PolicyTask2020", policy, "Open");
				  if(reminderTask2020 == null) {
					  ModelBean task = Task.createTask(data, "PolicyTask2020", new ModelBean[] {policy}, null, null,  getRunDate(), null, owner);
					  Task.insertTask(data, task);
				  }
				  uwTaskCreated = true;
			  }
			  if(uwTaskCreated) {
				  //Cancel Automated Renewal Task (if open)
				  ModelBean automatedTask = Task.getTask(data, "PolicyTask2004", policy, "Open");
				  if(automatedTask != null)
					  Task.updateOpenTaskStatus(data, "PolicyTask2004", policy, "Cancelled");

				  //Close Automated Renewal Error Task (if open)
				  ModelBean automatedErrorTask = Task.getTask(data, "PolicyTask2017", policy, "Open");
				  if(automatedErrorTask != null)
					  Task.updateOpenTaskStatus(data, "PolicyTask2017", policy, "Cancelled");
			  }
		  }	else { //if ManualRenewalInd is not Yes
			  //Create Automated Renewal Task (if not open)
			  ModelBean automatedTask = Task.getTask(data, "PolicyTask2004", policy, "Open");
			  if(automatedTask == null) {
				  ModelBean task = WorkflowRenderer.createTaskOverrideWorkDays(data, "PolicyTask2004",policy,"-45", getRunDate());
				  Task.insertTask(data, task);
			  }
			  //Create Automated Renewal Error Task (if not open)
			  ModelBean automatedErrorTask = Task.getTask(data, "PolicyTask2017", policy, "Open");
			  if(automatedErrorTask == null) {
				  ModelBean task = WorkflowRenderer.createTaskOverrideWorkDays(data, "PolicyTask2017",policy,"-44", getRunDate());
				  Task.insertTask(data, task);
			  }

			  //Cancel Manual Renewal Task (if open)
			  ModelBean existingTask = Task.getTask(data, "PolicyTask2018", policy, "Open");
			  if( existingTask != null )
				  Task.updateOpenTaskStatus(data, "PolicyTask2018", policy, "Cancelled");
			  
			  //Cancel Manual Renewal Task (if open)
			  existingTask = Task.getTask(data, "PolicyTask2020", policy, "Open");
			  if( existingTask != null )
				  Task.updateOpenTaskStatus(data, "PolicyTask2020", policy, "Cancelled");
		  }
		  return uwTaskCreated;
	  } catch (Exception e) {
		  return false;
	  }
  }

  /** Data repository selection criteria.
   *  To look up any active Policy having ExpirationDt later than the runDt + 70 days (which is configured in the job.xml)
   * @return MBPLookupSpec The lookup specification object containing the fields to be used for data selection
   * @throws MBPException when an error occurs
   */
  public MBPLookupSpec getLookupSpec() throws MBPException {
    try {
      StringDate runDt = getRunDate();
      StringDate startDt = DateRenderer.advanceDate(runDt, "70");
      return new MBPLookupSpec(
          new String[] { "Status", "ExpirationDt", "ExpirationDt" },
          new String[] { "=", ">=", "<=" },
          new String[] {"Active", runDt.toString(), startDt.toString() });
    } catch (Exception e) {
      throw new MBPException(e);
    }
  }

  /** Find out the run date of the current batch process
   * @return StringDate The date of the current batch run is set to.
   * @throws Exception when an error occurs
   */
  public StringDate getRunDate() throws Exception {
    // Find if the run date has been passed in
    if (this.getParam("RunDt", "").equals("")) {
      return DateTools.getStringDate();
    } else {
      return new StringDate(this.getParam("RunDt"));
    }
  }

  protected ModelBean callService(ModelBean rq)
      throws Exception {

    // Call the service
    return IBIZXmlController.processRequest(rq, true);
  }

  /** Set up the processor before the batch job is executed.
   * <BR><BR>
   * Instance variables runDt, myName, and templateId in base TaskReminderProcess are private, so must get my own.
   *
   * @throws Exception when an error occurs
   */
  public void initialCall() throws MBPException {
    try {
      // Set the error reporting to use the new JobProgress class instead of immediately
      // reporting to the Processor table.
      this.setReportErrorsImmediately(false);
    } catch (Exception e) {
      // If the job has been entered into the process table, go ahead and mark it as an error
      try {
        jobActionProgress.addExceptionError(e.getMessage());
        jobActionProgress.updateJobActionStatus("Error");
      } catch (Exception ec) {
        throw new MBPException(ec);
      }

      throw new MBPException(e);
    }
  }

  /** Update the process control manager: this process is now complete.
   * @param beanData The data repository connection
   * @throws MBPException when an error occurs
   */
  public void finalCall (JDBCData beanData) throws MBPException {
    try {
      //  Make an entry in the processor table to show that we have finished.
      //  This is for information purposes only.
      jobActionProgress.updateJobActionStatus("Complete");
    } catch (SQLException sqle) {
      // Do not throw an exception if its because the result of the search is empty
      if (sqle.getErrorCode() != 0)
        throw new MBPException(sqle);
    } catch (Exception e) {
      throw new MBPException(e);
    }
  }
}
