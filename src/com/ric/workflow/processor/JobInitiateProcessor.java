package com.ric.workflow.processor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.IBIZServer;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.batch.BatchJob;
import net.inov.tec.batch.BatchJobManager;
import net.inov.tec.batch.BatchJobTable;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.obj.ClassHelper;
import net.inov.tec.web.cmm.AdditionalParams;

import com.iscs.common.admin.operation.Operation;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.tech.service.ModelBeanProcessorBaseSPI;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.bean.BeanTools;

public class JobInitiateProcessor extends InnovationIBIZHandler {
	
	public JobInitiateProcessor() {
		// TODO Auto-generated constructor stub
	    // Overridden specifically for Monthly Job 1
	}
	
public ModelBean process() throws ServiceHandlerException {
		
		// Log a Greeting
        Log.debug("Processing Job Initiate Processor...");
        try {
	        ModelBean rs = this.getHandlerData().getResponse();    
	        ModelBean rq = this.getHandlerData().getRequest();
	        AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest() );	 
	        String jobTemplateId = ap.gets("Action");
	        int paramCount = Integer.parseInt(ap.gets("ParamCount"));
	        ModelBean questionReplies = new ModelBean("QuestionReplies");
	        ModelBean[] questionReply = new ModelBean[paramCount];
	        
	        String runDt = ap.gets("RunDt");
	        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		    Date date = null;
		    try {
				date = formatter.parse(runDt);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    if(date !=null) {
			    Calendar calendar = Calendar.getInstance();
			    calendar.setTime(date);
			    
			    
			    int lastDate = calendar.getActualMaximum(Calendar.DATE);
			    int currentDate = calendar.get(Calendar.DAY_OF_MONTH);
			    int runDtDay = calendar.get(Calendar.DAY_OF_MONTH);
			    
			    // Commissions should only be run on the 20th of the month
			    if(jobTemplateId.equals("JobCommission") && (runDtDay != 20)){
			    	return rs;
			    } 
			    
			   // JobMonthEnd should only be run on the last date of the month.
			    if(jobTemplateId.equals("JobMonthEnd1") && (lastDate != currentDate)){
			    	return rs;
			    }
		    }
	        
	        for(int i=0; i < paramCount; i++){
	        	questionReply[i] = new ModelBean("QuestionReply");
		    	questionReply[i].setValue("Name", ap.gets("Param"+i));
		    	questionReply[i].setValue("Value", ap.gets(questionReply[i].gets("Name")));
		    	questionReplies.addValue(questionReply[i]);		        
	        }
	        
	        JDBCData data = this.getHandlerData().getConnection();
	        initiateJob(data, jobTemplateId, questionReplies, rs, rq);	    	
        } catch(Exception ex){
        	ex.printStackTrace();
        }        
        return null;
    }
	
	/** This method initiates a job in a new thread 
	 * @param data JDBCData 
	 * @param jobTemplateId String to create the job from this template id
	 * @param questionReplies ModelBean for question reply beans in the job 
	 * @param rs Modelbean response params
	 * @param rq Modelbean request params
	 * @throws Exception
	 */
	public void initiateJob(JDBCData data, String jobTemplateId,ModelBean questionReplies, ModelBean rs, ModelBean rq) throws Exception {
		ModelBean job = new ModelBean("Job");
        job.setValue("StartDt",DateTools.getStringDate());
    	job.setValue("StartTm",DateTools.getTime());
    	job.setValue("JobTemplateIdRef",jobTemplateId);    	
    	
    	job.setValue("Status","Started");
    	
    	ModelBean jobTemplate = Operation.getJobTemplate(jobTemplateId);
    	ModelBean[] jobActionTemplate  = jobTemplate.getAllBeans("JobActionTemplate");            	
    	ModelBean jobActions = new ModelBean("JobActions");
    	
    	if( jobActionTemplate.length > 0 ) {            		
    		job.addValue(jobActions);
    	}
    	
    	for( int i=0;i<jobActionTemplate.length; i++ ) {
    		ModelBean jobAction = new ModelBean("JobAction");
    		// Set the id to the Job Action's id so that it is predictable.  This makes it easier for a web service call to set the Skip Indicator.
    		jobAction.setValue("id",jobActionTemplate[i].getId());

    		jobAction.setValue("JobActionTemplateIdRef",jobActionTemplate[i].getId());
    		jobAction.setValue("Status","Pending");
    		if(jobAction.gets("SkipInd").equals("")){
    			jobAction.setValue("SkipInd","No");		
    		}
    		jobActions.addValue(jobAction);
    	}
    	   	    	
    	job.addValue(questionReplies);
    	
    	
    	if( job.gets("SystemId").length() == 0){
    		// save the job bean to generate a system ID-- needed for jobs started
    		// from the command line processor
        	BeanTools.saveBean(job, data);
        	data.commit();
    	}

    	// Set up the batch job manager and call the appropriate Task Processor
        BatchJobManager manager = new BatchJobManager();
        
        // Get custom class loader
        ClassHelper helper = new ClassHelper();
                        		
        // Create a ModelBeanProcessorBaseSPI
        String procName = "com.iscs.common.admin.operation.processor.GenericProcessor";
        	                	
        ModelBeanProcessorBaseSPI mbp = (ModelBeanProcessorBaseSPI) helper.createThis(procName, null);
    
        try {
            mbp.setUserID(rs.getBean("ResponseParams").gets("UserId"));
            
            // Set up the job parameters
            mbp.addParam("JobRef", job.gets("SystemId"));
            
            // Set up the parameters needed for webpath security
            // Use the interal-only SessionId rather than the starting users's SessionId.
            // This isolates the batch job's internal webpath calls from any changes in
            // UserInfo.UserSession beans that may occur as the user logs in and out independent
            // of the batch job. Instead, they use the UserId directly.
            ModelBean resParams = rs.getBean("ResponseParams");
            mbp.addParam("RqUID", rq.getBean("RequestParams").gets("RqUID"));
            mbp.addParam("SecurityId", rq.getBean("RequestParams").gets("SecurityId"));
            mbp.addParam("SessionId", IBIZServer.SESSIONID_SECURITY_USE_USERID_DIRECTLY);
            mbp.addParam("ConversationId", IBIZServer.SESSIONID_SECURITY_USE_USERID_DIRECTLY);
            mbp.addParam("UserId", resParams.gets("UserId"));
            mbp.addParam("UserSessionId", resParams.gets("SessionId"));
            mbp.addParam("TransactionResponseDt", resParams.getDate("TransactionResponseDt").toString());
            mbp.addParam("TransactionResponseTm", resParams.gets("TransactionResponseTm"));
            mbp.addParam("RunUser", resParams.gets("UserId"));
            for(ModelBean questionReply:questionReplies.getAllBeans("QuestionReply")){
            	mbp.addParam(questionReply.gets("Name"), questionReply.gets("Value"));
            }
            
        	// Call the ModelBeanProcessorBaseSPI initializations                
            mbp.initialCall();
        }
        catch (Exception e){
            throw new IBIZException("Cannot load the Task batch processor", e);
        }
        
        // Create a BatchJobTable object
        BatchJob batchJob = new BatchJobTable("Job", mbp);

    	// Update the job record and commit it
        job.setValue("LogFilename", batchJob.getLogFileName());
    	BeanTools.saveBean(job, data);
    	data.commit();
        
    	// Set the title of the batch job (shows up as part of thread name)
    	batchJob.setTitle("BatchJob - " + jobTemplate.getId());

        // Add job to manager
        manager.add(batchJob);
        
        // Update the flag to tell the user the batch job is currently running
        ModelBean addl = rs.getBean("ResponseParams").getBean("AdditionalParams");
        setParam(addl, "CycleProcessing", "Yes", true);
	}
	
	/** Set an additional parameter in the response. Utility routine
     * @param parent ModelBean containing the parent of the parameter
     * @param name String holding the name of the parameter to set or add
     * @param value String containing the value of the paramter
     * @param useOld boolean determining whether to use an existing parameter or add a new one
     * @throws Exception when an error occurs
     */
    private void setParam(ModelBean parent, String name, String value, boolean useOld) 
    throws Exception {
        ModelBean param = null;
        if (useOld)
            param = parent.getBean("Param", "Name", name);
        if (param == null) {
            param = new ModelBean("Param");
            param.setValue("Name", name);
            parent.addValue(param);
        }
        param.setValue("Value", value);
    }

}