/*
 * TaskSystemProcessor.java
 *
 */

package com.ric.workflow.processor;

import java.sql.SQLException;

import net.inov.tec.batch.MBPException;
import net.inov.tec.batch.MBPLookupSpec;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

import com.iscs.common.admin.user.User;
import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDATemplate;
import com.iscs.common.tech.log.Log;
import com.iscs.common.tech.service.ModelBeanProcessorBaseSPI;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.NameValuePair;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.workflow.RuleResult;
import com.iscs.workflow.Task;

/** Process all system tasks that are Open and fall under the current processing run date
 *
 * @author  allend
 */
public class TaskSystemProcessor extends ModelBeanProcessorBaseSPI {
    
    protected static String ERROR_DELIM = "||";
    
    /** Creates a new instance of TaskSystemProcessor 
     * @throws Exception */
    public TaskSystemProcessor() 
    throws Exception {
        super ();
        
        this.spec = ModelSpecification.getSharedModel();
    }
    
    /** Bean processor main routine.  Each bean in the data repository table will be presented to this processor.  The criteria 
     * for task escalation will be handled and then a resultant sucess or fail indicator will be passed back.
     * @param taskBean Each task bean in the data repository that is passed for evaluation
     * @param data JDBC data repository connection
     * @return boolean of whether this bean has been processed or not
     * @throws MBPException when an error has occurred.
     */
    public boolean processBean(ModelBean taskBean, JDBCData data) 
    throws MBPException {
        try {
            Log.debug ("Enter processBean() for "+taskBean.getBeanName()+" bean. SystemId = " + taskBean.getSystemId());
                       
            // Re-check selection criteria in case another thread/server has updated the task since
            // This should be kept in synch with getLookupSpec()
            StringDate workDt = taskBean.getDate("WorkDt");
            StringDate runDt = getRunDate();
            if (workDt.compareTo(runDt) > 0) {
            	Log.debug("Skipping bean (workDt " + workDt + " > " + runDt + ")");
                return false;
            }
            if (!taskBean.gets("CurrentOwner").equalsIgnoreCase("System")) {
            	Log.debug("Skipping bean (CurrentOwner " + taskBean.gets("CurrentOwner") + " != System)");
                return false;
            }
            if (!taskBean.gets("Status").equalsIgnoreCase("Open")) {
            	Log.debug("Skipping bean (Status " + taskBean.gets("Status") + " != Open)");
            	return false;
            }
            
        	String policyNumber = this.getParam("PolicyNumber", "");
        	if( !policyNumber.equals("")) {
        		ModelBean taskLink = taskBean.getBean("TaskLink");
        		if( taskLink !=null) {
        			String searchValue = ModelSpecification.indexString(taskLink.gets("SearchValue"));
        			policyNumber = ModelSpecification.indexString(policyNumber);
	        		if( !searchValue.equalsIgnoreCase(policyNumber)) {
		                Log.debug("Skipping bean (PolicyNumber " + taskBean.gets("SearchValue") + " != " + policyNumber);
		                return false;
	        		}
        		}
        	}
            
        	String claimNumber = this.getParam("ClaimNumber", "");
        	if( !claimNumber.equals("")) {
        		ModelBean taskLink = taskBean.getBean("TaskLink");
        		if( taskLink !=null) {
        			String searchValue = ModelSpecification.indexString(taskLink.gets("SearchValue"));
        			claimNumber = ModelSpecification.indexString(claimNumber);
	        		if( !searchValue.equalsIgnoreCase(claimNumber)) {
		                Log.debug("Skipping bean (ClaimNumber " + taskBean.gets("SearchValue") + " != " + claimNumber);
		                return false;
	        		}
        		}
        	}
            
            // If the operator specified multiple, parallel running jobs (N of M), use the Task SystemId to determine if we
            // should process this task
            String strParallelN = this.getParam("ParallelN", "1");
            if (strParallelN.trim().equals(""))
            	strParallelN = "1";
            String strParallelM = this.getParam("ParallelM", "1");
            if (strParallelM.trim().equals(""))
            	strParallelM = "1";
			int parallelN = Integer.parseInt(strParallelN);
			int parallelM = Integer.parseInt(strParallelM);
            int systemId = Integer.parseInt(taskBean.getSystemId());
            if ( ((systemId % parallelM) + 1) != parallelN ) {
            	Log.debug("Skipping bean (not " + parallelN + " of " + parallelM + ")");
            	return false;
            }
            
            ModelBean[] webRules = taskBean.findBeansByFieldValue("TaskRule", "Processor", "webpath");

            String userId = this.getParam("RunUser");
            ModelBean user = User.loadUser(data, userId);

            // Add the security parameters to be able to run the webpath services
            for (int cnt=0; cnt<webRules.length; cnt++) {
                setParam(webRules[cnt], "RqUID", this.getParam("RqUID"), true);
                setParam(webRules[cnt], "SecurityId", this.getParam("SecurityId"), true);
                setParam(webRules[cnt], "SessionId", this.getParam("SessionId"), true);
                setParam(webRules[cnt], "ConversationId", getParam("ConversationId"), true);
                setParam(webRules[cnt], "UserId", this.getParam("UserId"), true);
                setParam(webRules[cnt], "TransactionResponseDt", runDt.toString(), true);
                setParam(webRules[cnt], "TransactionResponseTm", this.getParam("TransactionResponseTm"), true);                
            }

            ModelBean[] velocityRules = taskBean.findBeansByFieldValue("TaskRule", "Processor", "velocity");
            for (int cnt=0; cnt<velocityRules.length; cnt++) {
            	setParam(velocityRules[cnt], "SecurityId", this.getParam("SecurityId"), true);
                setParam(velocityRules[cnt], "SessionId", this.getParam("SessionId"), true);
                setParam(velocityRules[cnt], "ConversationId", getParam("ConversationId"), true);
                setParam(velocityRules[cnt], "UserId", this.getParam("UserId"), true);
            	setParam(velocityRules[cnt], "TransactionResponseDt", runDt.toString(), true);
                setParam(velocityRules[cnt], "TransactionResponseTm", this.getParam("TransactionResponseTm"), true);
            }
            
            //  Trigger Repeating Task before processing this task
            if( StringTools.isTrue(taskBean.gets("RepeatingInd")) ) {
            	ModelBean taskTemplate = Task.getTaskTemplate(taskBean.gets("TemplateId"));
            	Task.generateTaskForTask(data, taskBean, taskTemplate.getId(), userId, DateTools.getStringDate(runDt.toString()) );
            }

            Log.debug("---> Gonna process this system task " + taskBean.readableDoc());
            
            // Call the rule processor to execute the services tied to this task
            RuleResult result = Task.processRules(taskBean, user, "System", data, null);
            ModelBean response = result.getResult();
            Log.debug("---> response--- = " + response.readableDoc());
            
            // No errors were returned
            if( response.getBean("Errors") == null )
            	return false;
            
            // Look to see if the response returned any errors besides just warnings
            ModelBean[] infos = response.getBean("Errors").findBeansByFieldValue("Error", "Severity", ErrorTools.SEVERITY_INFO.getSeverity());
            ModelBean[] warnings = response.getBean("Errors").findBeansByFieldValue("Error", "Severity", ErrorTools.SEVERITY_WARN.getSeverity());            
            ModelBean[] errors = response.getBean("Errors").getBeans("Error");
            
            jobActionProgress.updateJobActionStatus("Running", false);
            for (int cnt=0; cnt<errors.length; cnt++) {
                String msg = getErrorMsg(taskBean, errors[cnt].gets("Msg"), data);
                jobActionProgress.addExceptionError(msg, errors[cnt].gets("Severity"));                
            }
            if (errors.length > (warnings.length + infos.length)) {
            	            	
            	// Create a new task to UW            	
                ModelBean taskLink = taskBean.getBean("TaskLink");
        		if( taskLink !=null && taskLink.gets("ModelName").equals("Policy" )) {
        			
        			ModelBean policy = new ModelBean("Policy");
        			data.selectModelBean(policy, taskLink.gets("IdRef"));
        		            			
        			// Verify template exists
        			MDATemplate taskTemplateMDA = (MDATemplate) Store.getModelObject("task-template", "TaskTemplate", "PolicyTask0008");
        		    if( taskTemplateMDA != null ) {
        		    
        		    	// Verify task is not already added to the policy
        		    	ModelBean existingTask = Task.getTask(data, "PolicyTask0008", policy, "Open");
        		    	if( existingTask == null ) {
        		    		ModelBean newTask = Task.createTask(data, "PolicyTask0008", new ModelBean[] {policy}, "System", new StringDate(), null, "");
        		    		newTask.setValue("Note","The " + taskBean.gets("Description") + " task for this policy failed during the Task System Cycle job with the following error: " + errors[0].gets("Msg") + ".");
                    
        		    		BeanTools.saveBean(newTask, data);
        		    	}
        		    }
        			
        		}
        		
                return false;
            }
                        
            // Update the task results
            Task.updateTaskStatus(taskBean, "Completed");
                        
            return true;
        } catch (Exception e) {
        	e.printStackTrace();
    		Log.error(e);
            try {
                String msg = getErrorMsg(taskBean, e.getMessage(), data);
                // Report the current error
                jobActionProgress.addExceptionError(msg);
                jobActionProgress.updateJobActionStatus("Running");
                
            } catch (Exception ec) {
                throw new MBPException(ec);
            }
            return false;
        }
    }
    
    /** Data repository selection criteria.  Only pass through tasks that have status of Open, are owned by System,
     * and have a WorkDt equal to or past the job run date.
     * @return MBPLookupSpec The lookup specification object containing the fields to be used for data selection
     * @throws MBPException when an error occurs
     */
    public MBPLookupSpec getLookupSpec () 
    throws MBPException {
      try {
    	// This criteria should be kept in synch with the recheck in processBean()
    	String policyNumber = this.getParam("PolicyNumber", "");
    	String claimNumber = this.getParam("ClaimNumber", "");
    	if( !policyNumber.equals("")) {    		
    		// this is used to run the task cycle for a specified policy
    		return new MBPLookupSpec(new String[] {"CycleKey", "WorkDt", "SearchValue"}, new String[] {"=", "<=", "="}, new String[] {"SystemOpen", getRunDate().toString(), ModelSpecification.indexString(policyNumber)});
    	} else if (!claimNumber.isEmpty()) {
    		// this is used to run the task cycle for a specified claim
    		return new MBPLookupSpec(new String[] {"CycleKey", "WorkDt", "SearchValue"}, new String[] {"=", "<=", "="}, new String[] {"SystemOpen", getRunDate().toString(), ModelSpecification.indexString(claimNumber)});
    	} else { 
    		return new MBPLookupSpec(new String[] {"CycleKey", "WorkDt"}, new String[] {"=", "<="}, new String[] {"SystemOpen", getRunDate().toString()});
    	}
      } catch (Exception e) {
		e.printStackTrace();
		Log.error(e);
		throw new MBPException(e);
      }
    }

    /** Find out the run date of the current batch process
     * @return StringDate The date of the current batch run is set to.
     * @throws Exception when an error occurs
     */
    public StringDate getRunDate() 
    throws Exception {
        // Find if the run date has been passed in
        if (this.getParam("RunDt", "").equals("")) {
            return DateTools.getStringDate();
        } else {
            return new StringDate(this.getParam("RunDt"));
        }
    }
    
    /** Set up the processor before the batch job is executed.
     * @throws Exception when an error occurs
     */
    public void initialCall() 
    throws MBPException {
        try {
            // Set the error reporting to use the new JobProgress class instead of immediately 
            // reporting to the Processor table.
            this.setReportErrorsImmediately(false);
        } catch (Exception e) {
            // If the job has been entered into the process table, go ahead and mark it as an error
            try {
            	jobActionProgress.addExceptionError(e.getMessage());
                jobActionProgress.updateJobActionStatus("Error");
            } catch (Exception ec) {
                throw new MBPException(ec);
            }
            throw new MBPException(e);
        }
    }

    
    /** Update the process control manager that this task is now complete.
     * @param beanData The data repository connection
     * @throws MBPException when an error occurs
     */
    public void finalCall (JDBCData beanData) 
    throws MBPException {
        try {
            //  Make an entry in the processor table to show that we have finished. 
            //  This is for information purposes only.
            jobActionProgress.updateJobActionStatus("Complete");
        } catch (SQLException sqle) {
            // Do not throw an exception if its because the result of the search is empty
            if (sqle.getErrorCode() != 0) 
                throw new MBPException(sqle);
        } catch (Exception e) {
            throw new MBPException(e);
        }
    }
    
    /** Set an additional parameter in the response. Utility routine
     * @param parent ModelBean containing the parent of the parameter
     * @param name String holding the name of the parameter to set or add
     * @param value String containing the value of the paramter
     * @param useOld boolean determining whether to use an existing parameter or add a new one
     * @throws Exception when an error occurs
     */
    protected void setParam(ModelBean parent, String name, String value, boolean useOld) 
    throws Exception {
        ModelBean param = null;
        if (useOld)
            param = parent.getBean("Param", "Name", name);
        if (param == null) {
            param = new ModelBean("Param");
            param.setValue("Name", name);
            parent.addValue(param);
        }
        param.setValue("Value", value);
    }    

    /** Build Error Message
     * @param taskBean Task ModelBean
     * @param msg Service Error Message
     * @param data JDBCData Connection
     * @return Error Message
     */
    protected String getErrorMsg(ModelBean taskBean, String msg, JDBCData data) {
        try {
            StringBuilder msgBuilder = new StringBuilder();
            
            // Task information
            msgBuilder = msgBuilder.append("Task[")
                           .append(taskBean.gets("SystemId"))
                           .append("]: ")
                           .append(taskBean.gets("TemplateId"))
                           .append(" - ")
                           .append(taskBean.gets("Name"));
            
            // Container Link
            ModelBean link = Task.getTaskLink(taskBean);
            if (link != null) {
            	msgBuilder = msgBuilder.append(ERROR_DELIM)
                               .append("Link ")
                               .append(link.gets("ModelName"))
                               .append("[")
                               .append(link.gets("IdRef"))
                               .append("]: ")        
                			   .append(link.gets("SearchValue"));    
            }
            
            return msgBuilder.append(ERROR_DELIM).append(msg).append(ERROR_DELIM).toString();
            
        } catch (Exception e) {
            return "Unknown error";
        }
    }
    
	/** Obtain the processing key that will be used to determine resource conflicts during
	 * multi-threading processing of ModelBean processor.
	 * The key is a concatenation of the TaskLink ModelName with the system id of the model
	 * @param bean The bean to use if needed to create the processing key
	 * @return The processing key if any to help with resource conflicts
	 */
    public String getProcessingKey(ModelBean bean) 
    throws Exception {
    	StringBuilder key = new StringBuilder();
    	ModelBean defaultTask = bean.findBeanByFieldValue("TaskLink", "DefaultInd", "Yes");
    	if (defaultTask != null) {
    		key.append(defaultTask.gets("ModelName"))
    		   .append(defaultTask.gets("IdRef"));    		
    	} else {
    		ModelBean link = bean.getBean("TaskLink");
    		key.append(link.gets("ModelName"))
 		       .append(link.gets("IdRef"));    		    		
    	}
    	return key.toString();
    }
    
	/** Obtain any additional processing resources that will be used to determine resource conflicts during
	 * multi-threading processing of ModelBean processor
	 * @param data The data connection
	 * @param bean The bean containing information to fill out the processing key 
	 * @return The list of resource keys that should be checked for concurrency issues
	 */
    public NameValuePair[] getProcessingResources(JDBCData data, ModelBean bean)
    throws Exception {
    	return Task.getTaskResourceKeys(data, bean);
    }
}
