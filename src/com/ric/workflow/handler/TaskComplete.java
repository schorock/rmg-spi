package com.ric.workflow.handler;

import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.workflow.Task;
import com.iscs.workflow.TaskHandler;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.security.SecurityManager;
import net.inov.tec.web.cmm.AdditionalParams;

/** Business service to complete the Task
 *
 * @author  beno
 */
public class TaskComplete extends InnovationIBIZHandler {
    
    /** Creates a new instance of TaskComplete
     * @throws Exception never
     */
    public TaskComplete() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a Greeting
            Log.debug("Processing TaskComplete...");
            ModelBean rs = this.getHandlerData().getResponse();
            JDBCData data = this.getHandlerData().getConnection();
            AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest() );
            ModelBean responseParams = rs.getBean("ResponseParams");
            StringDate todayDt = DateTools.getStringDate(responseParams);
            String todayTm = DateTools.getTime(responseParams);
            String userId = responseParams.gets("UserId");
            ModelBean user = responseParams.getBean("UserInfo");
            ModelBean xfdf = responseParams.getBean("XFDF");
    		ModelBean[] paramArray = xfdf.getBeans("Param");
            
            // Get AdditionalParams
            String logKey = ap.gets("LogKey");
            String comments = ap.gets("CompleteComments");
            String noteToFileInd = ap.gets("NoteToFileInd");
            
            boolean validateComments = true;           
            String completeCommentsRequired = ap.gets("CompleteCommentsRequired");
           	if(!StringTools.isTrue(completeCommentsRequired)){
           		validateComments = false;
           	}       
           
            // Validate Required Fields
            if( validateComments && comments.equals("") ) {
            	 addErrorMsg("CompleteComments", "Completion Comments Required", ErrorTools.MISSING_FIELD_ERROR);  
            }               
            
            if( hasErrors() )
                throw new IBIZException();

            // Attempt to Get Task
            ModelBean task = rs.getBean("Task");
            if( task == null ) {
            
            	// Loop Through Tasks
	            for( ModelBean param : paramArray ) {
	    			String name = param.gets("Name");
	                if( name.startsWith("ChangeTaskRef_") ) {
	                	String value = param.gets("Value");
	                	task = new ModelBean("Task");
	                    data.selectModelBean(task, value);
	            
			            // Validate against authority checks
			            boolean hasAuthority = Task.hasAuthority(user, data, task, "Complete", todayDt);
			            if( !hasAuthority ) 
			            	addErrorMsg("General", "You do not have authority to complete this task", ErrorTools.AUTHORITY_ERROR);
			            
			            if( hasErrors() )
			                throw new IBIZException();
			            
			            // Complete Task
			            complete(data, task, comments, userId, todayDt, todayTm, logKey, true); 
			            
			            // Update Task
			            BeanTools.saveBean(task, data);
	                }
	            }    
            } else {
            	
            	// Validate against authority checks
	            boolean hasAuthority = Task.hasAuthority(user, data, task, "Complete", todayDt);
	            if( !hasAuthority ) 
	            	addErrorMsg("General", "You do not have authority to complete this task", ErrorTools.AUTHORITY_ERROR);
	            
	            if( hasErrors() )
	                throw new IBIZException();
	            
	            // Complete Task
	            complete(data, task, comments, userId, todayDt, todayTm, logKey, false); 
	            
	            // Update Task
	            BeanTools.saveBean(task, data);
            }    
            
            // Add a Note with the Task Notes and Completion Comments  
            if( noteToFileInd.equalsIgnoreCase("Yes") ) {
	            String workflowComments = ap.gets("CompleteComments");
	            String handler = Task.getHandlerName(task);		
	    		TaskHandler oHandler = Task.getTaskHandler(handler);		
	    		if (oHandler != null) {
	    			oHandler.createCompleteNote(data, task, responseParams, workflowComments);
	    		}     
            }
            
            // Return the Response ModelBean
            return rs;
        }
        catch( IBIZException e ) {
            throw new IBIZException();
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
    
    /** Complete Task
     * @param data JDBCData Connection Object
     * @param task Task ModelBean 
     * @param comments Completion Comments
     * @param userId User ID
     * @param todayDt Today's Date
     * @param todayTm Today's Time
     * @param logKey Logging Key
     * @param massChangeInd Indicates this action is for a Mass Task Change
     * @throws Exception if an Unexpected Error Occurs
     */
    public static void complete(JDBCData data, ModelBean task, String comments, String userId, StringDate todayDt, String todayTm, String logKey, boolean massChangeInd) 
    throws Exception {
 		try {
 			// Verify Change of Ownership
        	if( !task.valueEquals("CurrentOwner", userId) ) {
	            // Change Ownership to Current User
	            Task.changeOwnership(task, userId, userId, "User", "Complete", "System Comment - Task is completed", todayDt, todayTm);
        	}
        	
        	// Process Task Rules
        	processTaskRules(data, task, userId);
        	
        	// Complete Task
            Task.updateTaskStatus(task, "Completed", comments, logKey);
        	
            // Mass Change - Prevent Reminder and Escalation
            if( massChangeInd ) {
            	task.setValue("ReminderInd", "");
            	task.setValue("CriticalDtInd", "");
            }
          
            // Get Task Template 
            ModelBean taskTemplate = Task.getTaskTemplate(task.gets("TemplateId"));
            
            // Trigger New Task Off Completion of Current Task
            Task.generateTaskForTask(data, task, taskTemplate.gets("TaskTemplateIdRef"), userId, todayDt, true);
            
            // Trigger Repeating Task
            if( StringTools.isTrue(task.gets("RepeatingInd")) ) {
            	Task.generateTaskForTask(data, task, taskTemplate.getId(), userId, todayDt);
            }
            	
            // Complete linked tasks (Reminders, etc)
            String quickKey = "TASK" + task.getSystemId();
            ModelBean[] linkedTaskArray = Task.fetchTasks(quickKey, data);
            
            for( ModelBean linkedTask : linkedTaskArray ) {
            	Task.updateTaskStatus(data, linkedTask, "Completed", task.gets("Comments"), logKey);

            	if( !massChangeInd )
            		Task.reportCompletion(data, linkedTask, todayDt);
            }

            if( !massChangeInd )
            	Task.reportCompletion(data, task, todayDt);
            
 		} catch (Exception e) {
 			e.printStackTrace();
 			Log.error(e.getMessage(), e);
 			throw e;
 		}
 	}
    
    private static void processTaskRules(JDBCData data, ModelBean task, String userId) throws Exception {
    	try {
    		ModelBean taskLink = task.getBean("TaskLink");
    		
    		if( taskLink == null )
    			return;
    		
    		ModelBean user = SecurityManager.getSecurityManager().getUser(data, userId);
			ModelBean[] activeTasks = Task.fetchTasks(taskLink.gets("ModelName"), taskLink.gets("IdRef"), "Open", data);
			
			for( ModelBean activeTask : activeTasks ) {
				if( !activeTask.getSystemId().equals(task.getSystemId()) )
					Task.processRules(activeTask, user, "Service", data, null);
			}
		} catch (Exception e) {
			e.printStackTrace();
 			Log.error(e.getMessage(), e);
 			throw e;
		} 
    }
}