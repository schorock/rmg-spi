package com.ric.workflow.handler;

import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.workflow.Task;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.security.SecurityManager;
import net.inov.tec.web.cmm.AdditionalParams;

/** Business service to delete the task
 *
 * @author  beno
 */
public class TaskDelete extends InnovationIBIZHandler {
    
    /** Creates a new instance of TaskDelete
     * @throws Exception never
     */
    public TaskDelete() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a Greeting
            Log.debug("Processing TaskDelete...");
            ModelBean rs = this.getHandlerData().getResponse();
            JDBCData data = this.getHandlerData().getConnection();
            AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest() );
            ModelBean responseParams = rs.getBean("ResponseParams");
            StringDate todayDt = DateTools.getStringDate(responseParams);
            String todayTm = DateTools.getTime(responseParams);
            ModelBean user = responseParams.getBean("UserInfo");
            String userId = responseParams.gets("UserId");
            ModelBean xfdf = responseParams.getBean("XFDF");
    		ModelBean[] paramArray = xfdf.getBeans("Param");
            
            // Get AdditionalParams
            String logKey = ap.gets("LogKey");
            String comments = ap.gets("DeleteComments");
            
            // Validate Required Fields
            if( comments.equals("") ) 
                addErrorMsg("DeleteComments", "Deletion Comments Required", ErrorTools.MISSING_FIELD_ERROR);
            
            if( hasErrors() )
                throw new IBIZException();

            // Attempt to Get Task
            ModelBean task = rs.getBean("Task");
            if( task == null ) {
            
	            // Loop Through Tasks
	            for( ModelBean param : paramArray ) {
	    			String name = param.gets("Name");
	                if( name.startsWith("ChangeTaskRef_") ) {
	                	String value = param.gets("Value");
	                	task = new ModelBean("Task");
	                    data.selectModelBean(task, value);
	            	
			            // Validate against authority checks
			            boolean hasAuth = Task.hasAuthority(user, data, task, "Change", todayDt);
			            if( !hasAuth )
			            	addErrorMsg("General", "You do not have authority to complete this task", ErrorTools.AUTHORITY_ERROR);
			            
			            if( hasErrors() )
			                throw new IBIZException();
			            
			            // Delete Task
			            delete(data, task, comments, userId, todayDt, todayTm, logKey); 
			            
			            // Update Task
			            BeanTools.saveBean(task, data);
	                }    
	            }
            } else {
            	
            	// Validate against authority checks
	            boolean hasAuth = Task.hasAuthority(user, data, task, "Change", todayDt);
	            if( !hasAuth )
	            	addErrorMsg("General", "You do not have authority to complete this task", ErrorTools.AUTHORITY_ERROR);
	            
	            if( hasErrors() )
	                throw new IBIZException();
	            
	            // Delete Task
	            delete(data, task, comments, userId, todayDt, todayTm, logKey); 
	            
	            // Update Task
	            BeanTools.saveBean(task, data);
            }    
            
            // Return the Response ModelBean
            return rs;
        }
        catch( IBIZException e ) {
            throw new IBIZException();
        }
        catch( Exception e ) {
            throw new ServiceHandlerException(e);
        }
    }
 
    /** Delete Task
     * @param data JDBCData Connection Object
     * @param task Task ModelBean 
     * @param comments Deletion Comments
     * @param userId User ID
     * @param todayDt Today's Date
     * @param todayTm Today's Time
     * @param logKey Logging Key
     * @throws Exception if an Unexpected Error Occurs
     */
    public static void delete(JDBCData data, ModelBean task, String comments, String userId, StringDate todayDt, String todayTm, String logKey) 
    throws Exception {
 		try {
 			// Verify Change of Ownership
        	if( !task.valueEquals("CurrentOwner", userId) ) {
        		
	            // Change Ownership to Current User
	            Task.changeOwnership(task, userId, userId, "User", "Delete", "System Comment - Task is deleted", todayDt, todayTm);
        	}
        	
        	// Process Task Rules
        	processTaskRules(data, task, userId);
            
            // Delete Task
            Task.updateTaskStatus(task, "Deleted", comments, logKey);

 		} catch (Exception e) {
 			e.printStackTrace();
 			Log.error(e.getMessage(), e);
 			throw e;
 		}
 	}
    
    private static void processTaskRules(JDBCData data, ModelBean task, String userId) throws Exception {
    	try {
    		ModelBean taskLink = task.getBean("TaskLink");
    		
    		if( taskLink == null )
    			return;
    		
    		ModelBean user = SecurityManager.getSecurityManager().getUser(data, userId);
			ModelBean[] activeTasks = Task.fetchTasks(taskLink.gets("ModelName"), taskLink.gets("IdRef"), "Open", data);
			
			for( ModelBean activeTask : activeTasks ) {
				if( !activeTask.getSystemId().equals(task.getSystemId()) )
					Task.processRules(activeTask, user, "Service", data, null);
			}
		} catch (Exception e) {
			e.printStackTrace();
 			Log.error(e.getMessage(), e);
 			throw e;
		} 
    }
}