package com.ric.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import net.inov.biz.server.ServiceContext;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

import com.iscs.common.admin.user.User;
import com.iscs.common.business.attachment.Attachment;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.workflow.Task;
import com.iscs.workflow.TaskHandler;

/** InboxSearchResult class contains the Tasks from an inbox search
 * 
 * @author kenb
 *
 */
public class InboxSearchResult {

	private ModelBean user;
	private StringDate asOfDate;
	private List<InboxItem> inboxItemList = new ArrayList<InboxItem>();
    
	/**
	 * @return the user
	 */
	public ModelBean getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(ModelBean user) {
		this.user = user;
	}

	/**
	 * @return the asOfDate
	 */
	public StringDate getAsOfDate() {
		return asOfDate;
	}

	/**
	 * @param asOfDate the asOfDate to set
	 */
	public void setAsOfDate(StringDate asOfDate) {
		this.asOfDate = asOfDate;
	}
	
	/** Inbox Item List Getter
	 * @return the inbox item list
	 */
	public List<InboxItem> getInboxItemList() {
		return inboxItemList;
	}

	/** Filter Out Unauthorized Tasks
	 * 
	 */
	public void filterUnauthorizedTasks() {
        
		List<InboxItem> filteredList = new ArrayList<InboxItem>();
		
		JDBCData data = ServiceContext.getServiceContext().getData();
		for( InboxItem inboxItem : inboxItemList ) {
			
			// Get Task
			ModelBean task = inboxItem.getTask();
			
			// Validate Task Against Authority Rules
			if( Task.hasAuthority(user, data, task, "View", asOfDate) ) 
				filteredList.add(inboxItem);
		}
		
		inboxItemList.clear();
		inboxItemList.addAll(filteredList);
	}

	/** Filter Out Tasks By Template
	 * 
	 */
	@Deprecated
	public void filterByTemplate() throws Exception {
         
		List<InboxItem> filteredList = new ArrayList<InboxItem>();
		
		for( InboxItem inboxItem : inboxItemList ) {
			
			// Get Task
			ModelBean task = inboxItem.getTask();
			
			// Filter Task if ShowOnInboxInd=No	
			ModelBean taskTemplate = Task.getTaskTemplate(task.gets("TemplateId"));
			if( !taskTemplate.gets("ShowOnInboxInd").equalsIgnoreCase("No") ) 
				filteredList.add(inboxItem);
		}
		
		inboxItemList.clear();
		inboxItemList.addAll(filteredList);
	}
	
	/** Fetch provider refs corresponding to all the unique targets of the TaskLinks in the Tasks
	 * using optimized calls that fetch them all of each model from the db in combined statements
	 * and put them into the session. These will then be picked up by the deeper calls to
	 * TaskHandlers in Task.hasProducerSecurity().
	 */
	private void prefetchTaskLinkTargetProducers() throws Exception {

		// A map, keyed by task handler class name (e.g. ...PolicyTask), of maps of unique systemIds keyed by model name (e.g. Policy)
		// i.e.
		//    TaskHandler class name
		//     |         Model name
		//     |          |      List of uniqueSystemIds
		//     |          |       |
		//     v          v       v
		Map<String,Map<String,List<String>>> targetHandlerSets = new HashMap<String,Map<String,List<String>>>();
		
		// Scan the inboxItemList to build the targetSets
		for( InboxItem inboxItem : inboxItemList ) {
			
			ModelBean task = inboxItem.getTask();
			if (task != null) {
				
				String taskHandlerClassName = Task.getHandlerName(task);
				if (taskHandlerClassName != null && !taskHandlerClassName.equals("")) {
					Map<String, List<String>> targetHandlerSet = targetHandlerSets.get(taskHandlerClassName);
					if (targetHandlerSet == null) {
						targetHandlerSet = new HashMap<String, List<String>>();
						targetHandlerSets.put(taskHandlerClassName,	targetHandlerSet);
					}
					ModelBean[] links = task.getBeans("TaskLink");
					for (ModelBean link : links) {

						String model = link.gets("ModelName");
						String systemId = link.gets("IdRef");
						if (!model.equals("") && !systemId.equals("")) {
							List<String> targetSet = targetHandlerSet.get(model);
							if (targetSet == null) {
								targetSet = new ArrayList<String>();
								targetHandlerSet.put(model, targetSet);
							}

							if (!targetSet.contains(systemId)) {
								targetSet.add(systemId);
							}
						}

					}
				}
        	}
			
		}

		ServiceContext context = ServiceContext.getServiceContext();
		JDBCData data = context.getData();
		
		// Call each TaskHandler to have it fetch provider refs for each set of models
		// Save the results in the context session
		for (Entry<String,Map<String,List<String>>> handlerSetEntry : targetHandlerSets.entrySet()) {
			String handlerClassName = handlerSetEntry.getKey();
			TaskHandler handler = Task.getTaskHandler(handlerClassName);
			if (handler != null) {
				
				Map<String,List<String>> handlerSet = handlerSetEntry.getValue();
				
				for (Entry<String,List<String>> targetSetEntry : handlerSet.entrySet()) {
					String model = targetSetEntry.getKey();
					String[] systemIds = targetSetEntry.getValue().toArray(new String[0]);
					if (systemIds.length > 0) {
						
						// Get the producer provider refs for all the beans of model
						Map<String,String[]> containerProviderRefs = handler.getContainerProducerProviderRefs(data, model, systemIds);
						
						// Give the handler a chance to cache away the results for later use
						handler.cacheProducerProviderRefs(model, containerProviderRefs);
						
					}
				}
			}
		}

	}
	
	/** Set Inbox Item Flags
	 * @param user Current User ModelBean
	 * @param viewingUserId LoginId of the user viewing the Inbox
	 * @param useManagerHierarchy indicates that the manager hierarchy should be built and each InboxItem flagged 
	 * @param todayDt The current date
	 * @throws Exception if an Unexpected Error Occurs
	 */
	public void setInboxItemFlags(ModelBean user, String viewingUserId, boolean useManagerHierarchy, StringDate todayDt) throws Exception {
		JDBCData data = ServiceContext.getServiceContext().getData();
		String userId = user.gets("LoginId");
		
		Set<String> managerHierarchyList = new HashSet<String>();
		if( useManagerHierarchy ) {
			
			// Add the current user to the list
			managerHierarchyList.add(userId);
						
			// Query anyone who is managed by the current user
			managerHierarchyList = User.queryManagerHierarchy(managerHierarchyList, userId);
			
			// Add the current user's groups to the list
			ModelBean[] currentUserTaskGroupArray = user.getAllBeans("UserTaskGroup");
	        for( ModelBean currentUserTaskGroup : currentUserTaskGroupArray ) {
	        	if( DateTools.between(asOfDate, currentUserTaskGroup.getDate("StartDt"), currentUserTaskGroup.getDate("EndDt")) ) {
	        		String taskGroupCd = currentUserTaskGroup.gets("TaskGroupCd");
	                if( !managerHierarchyList.contains(taskGroupCd) ) 
	                	managerHierarchyList.add(taskGroupCd);
	            }
	        }
		}
		
		if (User.isProducer(user)) {
			prefetchTaskLinkTargetProducers();
		}
		
		Attachment attachObj = new Attachment(); 
		for( InboxItem inboxItem : inboxItemList ) {
			
			// Get Task
			ModelBean task = inboxItem.getTask();
			
			// Initialize Task Variables
			String status = task.gets("Status");
			String templateId = task.gets("TemplateId");
			String currentOwner = task.gets("CurrentOwner");
			String currentOwnerCd = task.gets("CurrentOwnerCd");
			boolean isCompleteRequired = Task.isCompleteRequired(templateId);
			
			// Set Is Read Flag
			if( BeanTools.findBeanByFieldValueAndStatus(task, "TaskRead", "ReadBy", viewingUserId, "Active") != null )
				inboxItem.setRead(true);
			
			// Set Is Escalated Flag
			ModelBean taskHistory = Task.getLastHistory(task, "Escalation", user, asOfDate);
			if( taskHistory != null ) {
				inboxItem.setEscalated(true);
				inboxItem.setEscalatedFrom(taskHistory.gets("AssignedBy"));
			}	
			
			// Set Is Transferred Flag
			taskHistory = Task.getLastHistory(task, "Transfer", user, asOfDate);
			if( taskHistory != null && !taskHistory.gets("AssignedBy").equals(user.gets("LoginId")) ) {
				inboxItem.setTransferred(true);
				inboxItem.setTransferredFrom(taskHistory.gets("AssignedBy"));
			}	
			
			// Set Is Forwarded Flag
			if( currentOwnerCd.equals("User") ) {
				if( !currentOwner.equalsIgnoreCase(userId) ) {
					inboxItem.setForwarded(true);
					inboxItem.setForwardedFrom(currentOwner);
				}
			} else if( currentOwnerCd.equals("Group") ) {
				if( !User.hasTaskGroup(user, currentOwner) ) {
					inboxItem.setForwarded(true);
					inboxItem.setForwardedFrom(currentOwner);
				}
			}
			
			// If Status is Not Completed or Deleted
			if( !StringTools.in(status, "Completed,Deleted") ) {
				
				// Set Can Be Worked Flag 
				if( Task.canTaskBeWorked(data, task, user, user, asOfDate) )
					inboxItem.setCanBeWorked(true);
				
				// Set Can Complete Flag
				if( isCompleteRequired && !StringTools.isTrue(task.gets("WorkRequiredInd")) )
					inboxItem.setCanComplete(true);
				
				// Set Can Transfer Flag
				if( Task.canTaskBeTransferred(task) ) 
					inboxItem.setCanTransfer(true);
				
				// Set Can Suspend Flag
				if( Task.canTaskBeSuspended(task) )
					inboxItem.setCanSuspend(true);
				
				// Set Can Delete Flag
				if( Task.isOrphan(task) )
					inboxItem.setCanDelete(true);
				
				// Set Can Change Flag
				inboxItem.setCanChange(true);
				
				// Set Has Attachment Flag
				ModelBean attachment = task.getBean("Attachment");
	        	if( attachment != null && !attachment.valueEquals("Filename", "") ) {
	        		inboxItem.setHasAttachment(true);
	        		// Set Can Annotate Flag
	        		boolean canAnnotate = false;
	        		if (!attachment.gets("TemplateId").equals("")) {
	        			if (attachObj.hasAnnotateAuthority(attachment.gets("TemplateId"), user, todayDt) && Attachment.isAnnotatable(attachment)) {
			        		canAnnotate = true;
	        			}
	        		}
        			inboxItem.setCanAnnotate(canAnnotate);
	        	}	        		        	
			}	
			
			// Set flag based on supervisor chain
			if( useManagerHierarchy && !managerHierarchyList.contains(currentOwner) ) 
				inboxItem.setIsInManagerHierarchy(false);
			else
				inboxItem.setIsInManagerHierarchy(true);
		}
	}
	
	/** Filter the Inbox result set by the Inbox View Source filter
	 * @param filterBySource Array of 'Source' filters 
	 * @throws Exception if an Unexpected Error Occurs
	 */
	public void filterBySource(String filterBySource) throws Exception {

		List<InboxItem> filteredList = new ArrayList<InboxItem>();
								
		for( InboxItem inboxItem : inboxItemList ) {
			
			if( StringTools.in("Mine", filterBySource) && !inboxItem.isEscalated() && !inboxItem.isTransferred() && !inboxItem.isForwarded() ) {
				filteredList.add(inboxItem);
			}
			else if( StringTools.in("Escalated", filterBySource) && inboxItem.isEscalated() ) {
				filteredList.add(inboxItem);
			}
			else if( StringTools.in("Forwarded", filterBySource) && inboxItem.isTransferred() ) {
				filteredList.add(inboxItem);
			}
			else if( StringTools.in("Out of Office Forwarded", filterBySource) && inboxItem.isForwarded() ) {
				filteredList.add(inboxItem);
			}
		}	
		
		inboxItemList.clear();
		inboxItemList.addAll(filteredList);
	}

	/** Filter the Inbox result by the Inbox View Description filter (case insensitive)
	 * @param filterByDescription Description filter
	 * @throws Exception if an Unexpected Error Occurs
	 */
	public void filterByDescription(String filterByDescription) throws Exception {

		List<InboxItem> filteredList = new ArrayList<InboxItem>();
		
		for( InboxItem inboxItem : inboxItemList ) {
			String descriptionUpperCase = inboxItem.getTask().gets("Description").toUpperCase();
			String filterByDescriptionUppercase = StringTools.createRegexFromGlob(filterByDescription).toUpperCase();
			if( descriptionUpperCase.matches(filterByDescriptionUppercase) ) {
				filteredList.add(inboxItem);
			}
		}	
		
		inboxItemList.clear();
		inboxItemList.addAll(filteredList);
	}
	
	/** Filter the Inbox result by the Inbox View Task Attribute filter (case insensitive)
	 * @param filterName Name of field to filter on
	 * @param filterValue Filter value
	 * @throws Exception if an Unexpected Error Occurs
	 */
	public void filterByAttribute(String filterName, String filterValue) throws Exception {

		List<InboxItem> filteredList = new ArrayList<InboxItem>();

		String filterValueUppercase = StringTools.createRegexFromGlob(filterValue).toUpperCase();
		
		String[] constraints = filterValue.split(",");
				
		for( InboxItem inboxItem : inboxItemList ) {

			String fieldValueUpperCase = inboxItem.getTask().gets(filterName).toUpperCase();
			
			if( constraints.length == 1 ) {
				
				// Match the single value against each task
				if( fieldValueUpperCase.matches(filterValueUppercase) ) {
					filteredList.add(inboxItem);
				}
			}
			else {
				
				// Look of any of the constraint values in each task
				for( String constraint : constraints ) {
					if( fieldValueUpperCase.equals(constraint.toUpperCase()) ) {
						filteredList.add(inboxItem);
						break;
					}
				}
			}
		}	
		
		inboxItemList.clear();
		inboxItemList.addAll(filteredList);
	}
}