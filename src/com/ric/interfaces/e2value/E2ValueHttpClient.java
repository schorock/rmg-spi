package com.ric.interfaces.e2value;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;

import org.apache.axis.encoding.Base64;

import com.innovextechnology.service.Log;



public class E2ValueHttpClient{

	private String serverUrl;
	private Map<String, String> formVariables = new Hashtable<String, String>();
	private StringBuffer lastResult;
	private boolean isBasicAuthorization = false;
	private String userName;
	private String password;
	
	/**
	 * 
	 */
	public E2ValueHttpClient(){
		
	}
	
	/**
	 * 
	 * @param url
	 */
	public E2ValueHttpClient(String url){
		serverUrl = url;
	}
	
	/**
	 * @return the url
	 */
	public String getUrl() {
		return serverUrl;
	}
	
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.serverUrl = url;
	}
	
	/** Add a form variable
	 * 
	 * @param name
	 * @param value
	 */
	public void addFormVariable(String name, String value){
		
		if(name != null && value != null){
			formVariables.put(name, value);	
		}
	}
	
	/**
	 * @return the formVariables
	 */
	public Map<String, String> getFormVariables() {
		return formVariables;
	}

	/**
	 * @return the lastResult
	 */
	public StringBuffer getLastResult() {
		return lastResult;
	}
	
	/**
	 * @return the isBasicAuthorization
	 */
	public boolean isBasicAuthorization() {
		return isBasicAuthorization;
	}

	/**
	 * @param isBasicAuthorization the isBasicAuthorization to set
	 */
	public void setBasicAuthorization(boolean isBasicAuthorization) {
		this.isBasicAuthorization = isBasicAuthorization;
	}
	
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/** Do the post
	 * 
	 * @return Result
	 * @throws IOException 
	 */
	public String doPost(String request) throws IOException{
		HttpURLConnection conn = null;
		OutputStreamWriter wr = null;
		BufferedReader rd = null;
		try{
			lastResult = new StringBuffer();
			String data = request;
			String token = "";

	        // Send data
	        URL url = new URL(serverUrl);
	        
	        int contentLength = data.length();
	        String lengthVal = String.valueOf(contentLength) ;
	        conn = (HttpURLConnection) url.openConnection();
	        conn.setRequestProperty("Content-Length", lengthVal);
	        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	        String uuid = UUID.randomUUID().toString();
	        conn.setRequestProperty("X-NWD-MessageID", uuid);
	        prepareConnection(conn);
	        
	        conn.setDoOutput(true);
	        conn.setRequestMethod("POST");
	        
	        wr = new OutputStreamWriter(conn.getOutputStream());
	        wr.write(data);
	        wr.flush();
	       
	        if( conn != null && conn.getHeaderField("Location") != null)
	        	token = conn.getHeaderField("Location").substring(conn.getHeaderField("Location").lastIndexOf("/") + 1);
	        
	        // Get the response
	        if (conn.getResponseCode() >= 400) {
	        	rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
	        } else {
	        	rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	        }
	        String line;
	        while ((line = rd.readLine()) != null) {
	            lastResult.append(line + "\n");
	        }
	        Log.debug("Process E2Value Response: " + lastResult);
	        
	        return lastResult.toString();			
		}
		finally{
			if(rd != null){
				try{ rd.close(); } catch(Exception e) { };
			}
			if(wr != null){
				try{ wr.close(); } catch(Exception e) { };
			}
			if(conn != null){
				try{ conn.disconnect(); } catch(Exception e) { };
			}
		}
        
	}
	

	/**
	 * @param conn
	 */
	private void prepareConnection(HttpURLConnection conn) {
		// Impersonate Mozilla
        conn.setRequestProperty("User-agent", "Mozilla/4.0");
        if (isBasicAuthorization) {
        	String authString = userName + ":" + password;
			String authStringEnc = Base64.encode(authString.getBytes());
			conn.setRequestProperty("Authorization", "Basic " + authStringEnc);		
        }
	}
	
}
