package com.ric.interfaces.e2value.processor;

import java.util.HashMap;
import java.util.Map;

import net.inov.tec.beans.ModelBean;

import com.iscs.common.business.rule.processor.velocity.RuleProcessorVelocity;
import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDATemplate;


/**
 * E2Value Rule Processor
 * @author tchelladura
 *
 */
public class E2ValueRuleProcessor {
	
	private static String E2VALUE_TEMPLATE_MDA = "e2value::e2value-rules-templates::RuleTemplate::";
	private static String REQUEST_EXT = "-request";
	private String service;
	private String ruleTemplateMDA;
	private ModelBean contextBean;
	private RuleProcessorVelocity rpv;
	
	/**
	 * Default Constructor
	 */
	public E2ValueRuleProcessor(){
		rpv = new RuleProcessorVelocity();
	}
	
	
	/**
	 * @param ruleTemplateMDA
	 * @param bean
	 */
	public E2ValueRuleProcessor(String ruleTemplateMDA,ModelBean bean){
		rpv = new RuleProcessorVelocity();
		this.setRuleTemplateMDA(ruleTemplateMDA );
		this.setContextBean(bean);
	}
	
	/**
	 * @return the service
	 */
	public String getService() {
		return service;
	}

	/**
	 * @param serviceRequest the serviceRequest to set
	 */
	public void setService(String service) {
		this.service = service;
	}

	/**
	 * @return the ruleTemplateMDA
	 */
	public String getRuleTemplateMDA() {
		return ruleTemplateMDA;
	}

	/**
	 * @param ruleTemplateMDA the ruleTemplateMDA to set
	 */
	public void setRuleTemplateMDA(String ruleTemplateMDA) {
		this.ruleTemplateMDA = ruleTemplateMDA;
	}

	/**
	 * @return the contextBean
	 */
	public ModelBean getContextBean() {
		return contextBean;
	}

	/**
	 * @param contextBean the contextBean to set
	 */
	public void setContextBean(ModelBean contextBean) {
		this.contextBean = contextBean;
	}

	/**
	 * @return the rpv
	 */
	public RuleProcessorVelocity getRpv() {
		return rpv;
	}

	/**
	 * @param rpv the rpv to set
	 */
	public void setRpv(RuleProcessorVelocity rpv) {
		this.rpv = rpv;
	}

	/**
	 * @return
	 * @throws Exception
	 */
	public String process()throws Exception{
        MDATemplate mdaTemplate = (MDATemplate) Store.getModelObject(ruleTemplateMDA);
        ModelBean ruleTemplate = mdaTemplate.getBean();
        Map<String, Object> context = new HashMap<String, Object>();
        context.put("bean", contextBean);
		String result = rpv.process(ruleTemplate, context);
		return result;
	}

	/**
	 * @param service
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	public String processRequest(String service, ModelBean bean)throws Exception{
        MDATemplate mdaTemplate = (MDATemplate) Store.getModelObject(E2VALUE_TEMPLATE_MDA + service + REQUEST_EXT);
        ModelBean ruleTemplate = mdaTemplate.getBean();
        Map<String, Object> context = new HashMap<String, Object>();
        context.put("bean", bean);
		String result = rpv.process(ruleTemplate, context);
		return result;
	}

}
