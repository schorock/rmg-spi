package com.ric.interfaces.e2value.datareport;

import java.util.ArrayList;
import java.util.List;

import com.iscs.common.business.datareport.DataReport;
import com.iscs.common.business.datareport.DataReportController;
import com.iscs.common.business.datareport.delegate.DataReportDelegate;
import com.iscs.common.business.datareport.delegate.DataReportDelegateBase;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.render.VelocityTools;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.table.Cell;
import com.iscs.common.utility.table.Row;
import com.ric.insurance.product.render.RICProductRenderer;
import com.ric.interfaces.e2value.E2ValueServiceClient;

import net.inov.biz.server.ServiceContext;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.data.JDBCData;
import net.inov.tec.xml.XmlDoc;
import org.jdom.Element;
import org.jdom.xpath.XPath;


public class E2ValueDataReportDelegate extends DataReportDelegateBase implements DataReportDelegate{
        
	
	public void doReport(ModelBean dataReport, ModelBean dataReportRequest) throws Exception {
		
		prepareDataReport(dataReport, dataReportRequest);
		
		submitRequest(dataReport, dataReportRequest);
		
		DataReport.timeStampReceived(dataReport);
		
		ServiceContext.getServiceContext().getTransaction().saveBean(dataReport);
		ServiceContext.getServiceContext().getTransaction().commit();
		Log.debug(dataReport.readableDoc()); 
		
		if(!dataReport.gets("StatusCd").equals("Error")){
			DataReportController.getInstance().reportReceived(dataReport);
		}
	}

	/** Prepare the data report
	 * 
	 * @param dataReport
	 * @param dataReportRequest
	 * @throws Exception
	 */
	public void prepareDataReport(ModelBean dataReport, ModelBean dataReportRequest) throws Exception{
		 
		// Get the search criteria
		ModelBean searchCriteria = dataReportRequest.getBean("E2ValueReportRequest");
		
		// Copy the search criteria into the data report
		dataReport.setValue(searchCriteria.cloneBean());
		
	}
	
	/** Submit the request
	 * 
	 * @param dataReport
	 * @throws ModelBeanException
	 * @throws Exception
	 */
	public boolean submitRequest(ModelBean dataReport, ModelBean dataReportRequest) throws ModelBeanException, Exception {
		
 		ModelBean policy = null;
		String roofCd = "";
		String constructionCd = "";
		String architecturalStyle = "";
		String constructionQuality = "";
		String primaryExterior = "";
		ModelBean risk = null;
		ModelBean building = null;
		ModelBean dataReportReq = null;
		String fieldName = "";
		String noofstories = "";
		JDBCData data = ServiceContext.getServiceContext().getData();
		ModelBean searchCriteria = dataReportRequest.getBean("E2ValueReportRequest");			
		String e2ValueReportType = searchCriteria.gets("E2ValueReportType");
		ModelBean response = ServiceContext.getServiceContext().getResponse();
		policy = response.getBean("Application");
		ModelBean cmmParams = response.getBean("ResponseParams").getBean("CMMParams");
		String idRef = cmmParams.gets("IdRef");
		if( idRef.contains("DataReportRequest")){
			dataReportReq = policy.getBeanById(idRef);
			risk = policy.getBeanById(dataReportReq.gets("SourceIdRef"));
			building = risk.getBean("Building");
		}else if(idRef.contains("Risk")){
			risk = policy.getBeanById(idRef);
			building = risk.getBean("Building");
		}
			if( risk.gets("TypeCd").equalsIgnoreCase("Dwelling")){
				fieldName = "DwellingStyle";
			}else if(risk.gets("TypeCd").equalsIgnoreCase("Homeowners")){
				fieldName = "DwellingStyleCd";
			}
			if( idRef.contains("DataReportRequest") && StringRenderer.in(building.gets(fieldName), "Condominium/Co-operative,Condo/Co-operative") ){
				dataReportRequest.setValue("StatusCd", "Error");
				if( risk.gets("TypeCd").equalsIgnoreCase("Dwelling")){
					dataReport.setValue("ResultCd", "E2Value cannot be ordered if Dwelling style is Condominium/Co-operative");
				}else if(risk.gets("TypeCd").equalsIgnoreCase("Homeowners")){
					dataReport.setValue("ResultCd", "E2Value cannot be ordered if Dwelling style is Condo/Co-operative");
				}
			} else{
				String e2ValueRsDoc = null;
				E2ValueServiceClient e2ValueClient = new E2ValueServiceClient();
				XmlDoc e2ValueRqDoc = e2ValueClient.createServiceRequest( e2ValueReportType, policy);
				e2ValueRsDoc = e2ValueClient.processE2ValueRequest(e2ValueRqDoc, building, idRef);
				XmlDoc e2valueRsDocXML = new XmlDoc(e2ValueRsDoc);
				DataReport.createAttachment(dataReport, "e2valuereport", ".txt", e2valueRsDocXML.toPrettyString());
				XmlDoc responseXML = new XmlDoc(e2valueRsDocXML.toPrettyString());
				String status = responseXML.getRootElement().getAttributeValue("status");
		    	Log.info(e2ValueRsDoc);
		    	
		    	if( e2ValueRsDoc !=null && !status.equalsIgnoreCase("failure") && e2ValueReportType.equalsIgnoreCase("get-valuation-manuf")){
		    		building.setValue("ReplacementCost",e2valueRsDocXML.xgetValue("//replacement_cost"));
		    		searchCriteria.setValue("ReplacementCost", e2valueRsDocXML.xgetValue("//replacement_cost"));
		    	}else if( e2ValueRsDoc !=null && !status.equalsIgnoreCase("failure")){
		    		roofCd = e2valueRsDocXML.xgetValue("//BasicStructure/PrimaryRoofCovering/Value");
			        constructionCd = e2valueRsDocXML.xgetValue("//BasicStructure/TypeOfConstruction/Value");
			        architecturalStyle = e2valueRsDocXML.xgetValue("//BasicStructure/ArchitecturalStyle/Value");
			        constructionQuality = e2valueRsDocXML.xgetValue("//BasicStructure/ConstructionQuality/Value");
			        primaryExterior = e2valueRsDocXML.xgetValue("//BasicStructure/PrimaryExterior/Value");
			        noofstories = e2valueRsDocXML.xgetValue("//AdditionalInfo/NumberOfStories/Value");
					building.setValue("ReplacementCost",e2valueRsDocXML.xgetValue("//ReplacementCost/Cost"));
					building.setValue("YearBuilt",e2valueRsDocXML.xgetValue("//SquareFootage/YearBuilt/Value"));
					String primaryExt = RICProductRenderer.getOptionLabel("e2value::e2value::mapping::dw_primary_exterior_from_e2Value", primaryExterior,"");
					if(building.gets(fieldName).equalsIgnoreCase("Mobile Home")){
						building.setValue("ArchitecturalStyle", RICProductRenderer.getOptionLabel("e2value::e2value::mapping::dw_architecural_style_from_e2Value_MH", architecturalStyle,""));
					} else{
						building.setValue("ArchitecturalStyle", RICProductRenderer.getOptionLabel("e2value::e2value::mapping::dw_architecural_style_from_e2Value", architecturalStyle,""));
					}
					building.setValue("ConstructionQuality", RICProductRenderer.getOptionLabel("e2value::e2value::mapping::dw_construction_quality_from_e2Value", constructionQuality,""));
					if( primaryExt !=null && !primaryExt.equals(""))
						building.setValue("PrimaryExterior", primaryExt);
					else
						building.setValue("PrimaryExterior", "Other");
					building.setValue("SqFt", e2valueRsDocXML.xgetValue("//SquareFootage/LivingAreaSqFt/Value"));
					building.setValue("NumStories", RICProductRenderer.getOptionLabel("e2value::e2value::mapping::noofstories", noofstories,""));
					String propId = e2valueRsDocXML.xgetValue("//ReplacementCost/PropertyID");
					VelocityTools vt = new VelocityTools();
					if( vt.isNotNull(propId) && !propId.equals("")){
						building.setValue("E2ValuePropertyId", e2valueRsDocXML.xgetValue("//ReplacementCost/PropertyID"));
					}
			    	if( risk.gets("TypeCd").equalsIgnoreCase("Dwelling")){
			    		building.setValue("ConstructionCd", RICProductRenderer.getOptionLabel("e2value::e2value::mapping::dw_construction_type_from_e2Value", constructionCd,""));
				        building.setValue("RoofCd", RICProductRenderer.getOptionLabel("e2value::e2value::mapping::dw_primary_roof_covering_from_e2Value", roofCd,""));
			    	} else if(risk.gets("TypeCd").equalsIgnoreCase("Homeowners") ){
						building.setValue("RoofCd", RICProductRenderer.getOptionValue("e2value::e2value::mapping::ho_roof_type", roofCd,""));
						building.setValue("ConstructionCd", RICProductRenderer.getOptionValue("e2value::e2value::mapping::ho_construction_type", constructionCd,""));
			    	}
			    	searchCriteria.setValue("ReplacementCost", e2valueRsDocXML.xgetValue("//ReplacementCost/Cost"));
			    	List<Element> reports = XPath.selectNodes(e2valueRsDocXML, "//OtherAreas/Area/Name");
			    	int count = 0;
			    	for(int cntNodes = 0;cntNodes < reports.size(); cntNodes++){
			    		Element returnData =(Element)reports.get(cntNodes);
			    		count = cntNodes+1;
			    		building.setValue("AdditionalArea"+count, RICProductRenderer.getOptionLabel("e2value::e2value::mapping::dw_additional_area_from_e2Value", returnData.getChildText("Value"),""));
			    	}
			    	List<Element> reportsqts = XPath.selectNodes(e2valueRsDocXML, "//OtherAreas/Area/SquareFootage");
			    	int countSq = 0;
			    	for(int cntSqNodes = 0;cntSqNodes < reportsqts.size(); cntSqNodes++){
			    		Element returnSqData =(Element)reportsqts.get(cntSqNodes);
			    		countSq = cntSqNodes +1;
			    		if(!building.gets("AdditionalArea"+countSq).equalsIgnoreCase("None")){
			    			building.setValue("AdditionalAreaSqFt"+countSq, returnSqData.getChildText("Value"));
			    		}
			    	}
		    	} else{
			    	building.setValue("ReplacementCost","");
			    	//building.setValue("E2ValuePropertyId", "");
		    	}
		    	building.setValue("E2Valuestatus", status);
		    	dataReportRequest.setValue("AppliedDt", DateTools.getStringDate());
		        dataReportRequest.setValue("AppliedTm", DateTools.getTime("HH:mm:ss z"));  
		        dataReportRequest.setValue("StatusCd", "Applied");
		        if( e2ValueRsDoc !=null && !status.equalsIgnoreCase("failure")){
		        	dataReport.setValue("ResultCd", "Hit");
		        } else{
		        	dataReport.setValue("ResultCd", "No Hit");
		        }
				BeanTools.saveBean(dataReport, data);
			}
			return true;
	}
	
	/** Get the E2Value DataReport Error Message
     * @param dataReport DataReport ModelBean
     * @return Error Message
     * @throws Exception if an Unexpected Error Occurs
     */
	public String getDataReportErrorMessage(ModelBean dataReport)
	throws Exception {
		// Get Error Report ModelBean
		ModelBean errorReport = dataReport.getBean("E2ValueErrorReport");
		if( errorReport == null )
			return "";
		
		return errorReport.gets("Message");
	}
	
	/** Get the E2Value DataReport Error Code
     * @param dataReport DataReport ModelBean
     * @return Error Code
     * @throws Exception if an Unexpected Error Occurs
     */
    public String getDataReportErrorCode(ModelBean dataReport) 
    throws Exception {
    	// Get Error Report ModelBean
		ModelBean errorReport = dataReport.getBean("E2ValueErrorReport");
		if( errorReport == null )
			return "";
		else
			return errorReport.gets("TransactionStatus");

    }
}
