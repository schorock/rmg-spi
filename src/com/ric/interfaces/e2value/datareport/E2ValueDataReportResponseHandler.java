package com.ric.interfaces.e2value.datareport;

import com.iscs.common.business.rule.RuleException;
import com.iscs.common.business.rule.RuleProcessor;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.bean.BeanTools;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;

/** E2Value data report response handler
 * 
 * @author tchelladura
 *
 */
public class E2ValueDataReportResponseHandler implements RuleProcessor {

        
	public ModelBean process(ModelBean ruleTemplate, ModelBean bean, ModelBean[] additionalBeans, ModelBean user, JDBCData data) throws RuleException {
		try {
			 
			ModelBean dataReport = BeanTools.searchArray("DataReport", additionalBeans);
			ModelBean application = bean;
			BeanTools.saveBean(dataReport, data);
            BeanTools.saveBean(application, data);
			return application;
			
			
		}
		catch (Exception e) {
			Log.error(e.getMessage(), e);
            throw new RuleException(e);
		}
	}  
}
