package com.ric.interfaces.e2value;

import net.inov.tec.beans.ModelBean;

import net.inov.tec.xml.XmlDoc;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URLEncoder;

import org.jdom.Element;

import java.util.concurrent.TimeUnit;
import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDAXmlDoc;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.tech.log.Log;
import com.ric.interfaces.e2value.processor.E2ValueRuleProcessor;

/**
 * @author tchelladura
 *
 */
public class E2ValueServiceClient {

	protected String _userId = "";
	protected String _password = "";
	protected String _serviceURL = "";
	protected String _service_2_call = "";
	protected String _service_3_call_mobile_home = "";
	protected static String _ReadTimeOut = "60000";
	protected static String _ConnTimeOut = "10000";

	public E2ValueServiceClient() {
		setCredentials();
	}

	protected void setCredentials() {
		try {

			// Get OTP environment variables
			_userId = getSetting("E2ValueUserId", "value");
			_password = getSetting("E2ValuePassword", "value");
			_serviceURL = getSetting("E2ValueServiceURL", "value");
			_service_2_call = getSetting("E2ValueServiceURL2", "value");
			_service_3_call_mobile_home = getSetting("E2ValueServiceURL3", "value");

		} catch (Exception ex) {
			Log.error("Unable to get E2 Value file settings", ex);
			ex.printStackTrace();
		}
	}
	
	public void setConnectionTimeOut(){
		try {
			MDAXmlDoc mdaDoc = (MDAXmlDoc)Store.getModelObject("e2value-settings::null::null");
			if (mdaDoc == null) {
				return;
			}
			XmlDoc doc = mdaDoc.getDocument();
			_ReadTimeOut =  doc.getRootElement().getChild("ReadTimeOut").getAttributeValue("value");
			_ConnTimeOut =  doc.getRootElement().getChild("ConnectionTimeOut").getAttributeValue("value");
		} catch (Exception ex) {
			Log.error("Unable to read ISO Fire Line configuration file.", ex);
			ex.printStackTrace();
		}
	}

	/**
	 * @param serviceRequest
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	public XmlDoc createServiceRequest(String serviceRequest, ModelBean bean) throws Exception {
		E2ValueRuleProcessor rp = new E2ValueRuleProcessor();
		return new XmlDoc(rp.processRequest(serviceRequest, bean));
	}

	protected String processE2ValueCall(String request, ModelBean building, String idRef) throws Exception {
		Log.debug("Process E2Value Request: " + request);
		
		String fieldName = "";
		ModelBean risk = building.getParentBean();
		String e2valuePropertyId = building.gets("E2ValuePropertyId");
		if( risk.gets("TypeCd").equalsIgnoreCase("Dwelling")){
			fieldName = "DwellingStyle";
		}else if(risk.gets("TypeCd").equalsIgnoreCase("Homeowners")){
			fieldName = "DwellingStyleCd";
		}
		
		String endpoint="";
		
		if (!StringRenderer.isTrue(building.gets("E2ValueReportInd"))){
			if (building.gets(fieldName).equalsIgnoreCase("Mobile Home")){
				endpoint=_service_3_call_mobile_home;
			}else if(!e2valuePropertyId.equals("") && !StringRenderer.in(building.gets(fieldName), "Mobile Home,Condominium/Co-operative,Condo/Co-operative") ){
				endpoint=_service_2_call;
			}else{
				endpoint=_serviceURL;
			}
		}else{
			endpoint=_serviceURL;
		}
		MediaType mediaType =MediaType.parse("application/x-www-form-urlencoded");;
		OkHttpClient client = new OkHttpClient.Builder()
		        .connectTimeout(Integer.parseInt(_ConnTimeOut), TimeUnit.MILLISECONDS)
		        .readTimeout(Integer.parseInt(_ReadTimeOut), TimeUnit.MILLISECONDS)		    
		        .build();
		RequestBody body = RequestBody.create(mediaType, "xml="+URLEncoder.encode(request, "UTF-8"));
		Request okhttpsrequest = new Request.Builder()
				  .url(endpoint)
				  .post(body)
				  .addHeader("content-type", "application/x-www-form-urlencoded")
				  .addHeader("cache-control", "no-cache")
				  .build();
		

				
		Response response = client.newCall(okhttpsrequest).execute();
		
		System.out.println(response.code());
		System.out.println(response.body());
		String e2ValueResponse="";
		 if (response != null && response.body() != null) {
			BufferedReader in = new BufferedReader(new InputStreamReader(response.body().byteStream()));
			String inputLine = "";
			StringBuffer content = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				content.append(inputLine);
			}
			e2ValueResponse = content.toString();
			System.out.println(content.toString());
		 }
		return e2ValueResponse;
	}

	public String processE2ValueRequest(XmlDoc e2ValueRqDoc, ModelBean building, String idRef) throws Exception {
		String request = "";
		// Make call
		request = e2ValueRqDoc.toString(null, false, true);
		request = request.replace("E2ValueUserId", _userId);
		request = request.replace("E2ValuePassword", _password);
		return processE2ValueCall(request, building, idRef);
	}

	public static String getSetting(String elementName, String attribute) throws Exception {

		String val = "";
		MDAXmlDoc mdaDoc = (MDAXmlDoc) Store.getModelObject("e2value-settings::E2ValueSettings::null");
		Element codeRefElement = mdaDoc.getDocument().getRootElement().getChild(elementName);
		if (!attribute.isEmpty()) {
			val = codeRefElement.getAttributeValue(attribute);
		} else {
			val = codeRefElement.getValue();
		}
		return val;
	}

}
