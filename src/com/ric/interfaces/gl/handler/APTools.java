package com.ric.interfaces.gl.handler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import com.iscs.claims.transaction.render.ClaimantTransactionRenderer;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.StringTools;
import com.ric.insurance.interfaces.gl.FileWriter;
import com.ric.interfaces.gl.GLCarrierCd;
import com.ric.interfaces.gl.GLCompanyCd;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.date.StringDate;

public class APTools {
	public enum APAccountType {CLAIMS, COMMISSIONS, REFUNDS};

	public static List<String> splitByLength(String toSplit, int maxLength) {
		List<String> strings = new ArrayList<String>();
		
		int i = 0;		

		while (i < toSplit.length()) {
			strings.add(toSplit.substring(i, Math.min(i + maxLength, toSplit.length())));
			i += maxLength;
		}
		
		if(i==60){
			strings.add("");
		}
		
		return strings;
	}
	
	public static String toCompanyCode(final String carrierCd) {
		String result = "";

		switch (GLCarrierCd.Index.get(carrierCd))
		{
		case GLCarrierCd.Index.NORTH_CAROLINA_INSURANCE_UNDERWRITING_ASSOCIATION:
			result = GLCompanyCd.NORTH_CAROLINA_INSURANCE_UNDERWRITING_ASSOCIATION;
			break;
		case GLCarrierCd.Index.NORTH_CAROLINA_JOINT_UNDERWRITING_ASSOCIATION:
			result = GLCompanyCd.NORTH_CAROLINA_JOINT_UNDERWRITING_ASSOCIATION;
			break;
		}

		return result;
	}
	
	public static void populateApBeanReferenceIds(ModelBean apBean) throws Exception {
		for(ModelBean subBean : apBean.getBeans()) {
			subBean.setValue("SourceCd", apBean.gets("SourceCd"));
			subBean.setValue("ReferenceId", apBean.gets("ReferenceId"));
		}
	}
	
	public static void populateUnlimitedDates(ModelBean unlimited, StringDate runDt) throws ModelBeanException {
		String runDay = runDt.toStringDisplay("dd");
		String runMonth = runDt.toStringDisplay("MM");
		String runYear = runDt.toStringDisplay("yyyy");
		
		unlimited.setValue("PayDay", runDay);
		unlimited.setValue("PayMonth", runMonth);
		unlimited.setValue("PayYear", runYear);

		unlimited.setValue("TransactionDay", runDay);
		unlimited.setValue("TransactionMonth", runMonth);
		unlimited.setValue("TransactionYear", runYear);
	}
	
	/**
	 * Populates all the header dates which use the run date
	 * @param header Header to be populated
	 * @param runDt current run date
	 */
	public static void populateHeaderDates(ModelBean header, StringDate runDt) throws ModelBeanException {
		String runDay = runDt.toStringDisplay("dd");
		String runMonth = runDt.toStringDisplay("MM");
		String runYear = runDt.toStringDisplay("yyyy");
		
		header.setValue("PayDay", runDay);
		header.setValue("PayMonth", runMonth);
		header.setValue("PayYear", runYear);

		header.setValue("TransactionDay", runDay);
		header.setValue("TransactionMonth", runMonth);
		header.setValue("TransactionYear", runYear);

		header.setValue("AccrualMonth", runMonth);
		header.setValue("AccrualDay", runDay);
		header.setValue("AccrualYear", runYear);

		header.setValue("InvoiceMonth", runMonth);
		header.setValue("InvoiceDay", runDay);
		header.setValue("InvoiceYear", runYear);
	}
	
	/**
	 * Populate the check wire indicator
	 * @param header the Header bean
	 * @param paymentMethod method of payment
	 */
	public static void populateHeaderCheckWireInd(ModelBean header, String paymentMethod) throws ModelBeanException {
		header.setValue("CheckEFTWireInd", "C");
		if("ACH".equals(paymentMethod)) {
			header.setValue("CheckEFTWireInd", "E");
		} else if(paymentMethod.contains("Check")) {
			header.setValue("CheckEFTWireInd", "C");
		}
	}
	
	public static void populateHeaderClaimCheckDescription(ModelBean header, String claimName, String insuredName, String producerNumber) throws ModelBeanException {
		header.setValue("CheckDescription1", claimName);
		header.setValue("CheckDescription2", StringTools.trim(insuredName, 60));
		header.setValue("CheckDescription3", "");
		header.setValue("CheckDescription4", "");
	}
	
	public static void populateHeaderRefundCheckDescription(ModelBean header, String refundReason) throws ModelBeanException {
		header.setValue("CheckDescription1", refundReason);
	}
	
	public static void populateHeaderCommissionCheckDescription(ModelBean header, StringDate runDt, String producerNumber) throws Exception {
		StringDate end = StringDate.advanceDate(DateTools.advanceMonthEnd(runDt), -1);
		
		StringDate beginning = new StringDate(((java.util.Calendar) runDt.getCalendar().clone()));
		beginning.getCalendar().set(Calendar.DAY_OF_MONTH, 1);

		header.setValue("CheckDescription1", String.format("%s - %s", beginning.toString(), end.toString()));
		
		header.setValue("CheckDescription3", "ZZ" + producerNumber);
	}
	
	public static void populateHeaderEFTFields(ModelBean header, String accountNumber, String accountType, String routingNumber) throws ModelBeanException {
		header.setValue("EFTPayeeAccountNumber", accountNumber);
		
		if("Checking".equalsIgnoreCase(accountType)) {
			header.setValue("EFTPayeeAccountType", "C");
		} else if("Savings".equalsIgnoreCase(accountType)) {
			header.setValue("EFTPayeeAccountType", "S");
		} else {
			throw new IllegalStateException(String.format("Unknown account type [%s], expecting Savings or Checkings", accountType));
		}

		header.setValue("EFTPayeeRoutingTransit", routingNumber);
	}
	
	public static void populateHeaderIRSBox(ModelBean header) throws ModelBeanException {
		header.setValue("IRSBoxCd", "1070");
		header.setValue("IRSForm", "1099MISC");
	}
	
	public static void populateHeaderTaxInfo(ModelBean header, ModelBean taxInfo) throws ModelBeanException {
		String taxIdType = taxInfo.gets("TaxIdTypeCd");
		if("SSN".equals(taxIdType)) {
			header.setValue("TaxIdNumberType", "P");
			header.setValue("TaxIdNumber", taxInfo.gets("SSN").replace("-", ""));
		} else if("FEIN".equals(taxIdType)) {
			header.setValue("TaxIdNumberType", "C");
			header.setValue("TaxIdNumber", taxInfo.gets("FEIN").replace("-", ""));
		} else if(taxIdType.isEmpty()) {
			header.setValue("TaxIdNumber", "111111111");
		} else {
			throw new IllegalStateException(String.format("Unknown tax type of [%s], expected SSN or FEIN", taxIdType));
		}
		
	}
	
	public static void writeAPBean(ModelBean bean, FileWriter target) throws Exception {
		ModelBean header = bean.getBean("APSubLedgerHeader");
		target.write(header, "PayableRecord::Header");
		
		for(ModelBean unlimited : bean.getAllBeans("APSubLedgerUnlimited")) {
			target.write(unlimited, "PayableRecord::Unlimited");
		}
		
		ModelBean detail = bean.getBean("APSubLedgerDetail");
		target.write(detail, "PayableRecord::Detail");
	}

	public static void writeVoidStopBean(ModelBean bean, FileWriter target) throws Exception {
//		ModelBean stopVoid = bean.getBean("APSubLedgerStopVoid");
		target.write(bean, "PayableRecord::StopVoid");
	}
	public static void writeReferenceBean(ModelBean bean, FileWriter target) throws Exception {
//		ModelBean stopVoid = bean.getBean("APSubLedgerStopVoid");
		target.write(bean, "ReferenceRecord::Standard");
	}
	public static void populateDescription(ModelBean apBean, String description) throws ModelBeanException {
		ModelBean header = apBean.getBean("APSubLedgerHeader");
		header.setValue("HeaderTransactionDescription", description);
		
		ModelBean detail = apBean.getBean("APSubLedgerDetail");
		detail.setValue("DescriptionDetail", description);
		detail.setValue("ReferenceId", description);
		
		ModelBean[] unlimiteds = apBean.getBeans("APSubLedgerUnlimited");
		if(unlimiteds != null){
			for(ModelBean unlimited:unlimiteds){
				unlimited.setValue("ReferenceId", description);
			}
		}
	}
	
	
	public static List<String> createIndemnityStubUnlimited(ModelBean claim, ModelBean transaction) throws Exception {
		ModelBean[] allocations = transaction.getBeans("FeatureAllocation");
		
		List<String> lines = new ArrayList<String>();
		for(ModelBean allocation: allocations) {
			lines.add(StringTools.trim(claim.gets("ClaimNumber"),60));
			lines.add(StringTools.trim(transaction.gets("InvoiceNumber"),60));
			lines.add(StringTools.trim(claim.gets("LossDt"),60));
			lines.add(StringTools.trim(transaction.gets("Memo"),60));
			lines.add(StringTools.trim(allocation.gets("FeatureCd")+""+allocation.gets("FeatureSubCd"), 60));
			lines.add(allocation.gets("PaidAmt"));
		}
		
		return lines;
	}
}
