package com.ric.interfaces.gl.handler;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;

import com.iscs.ar.HPAccount;
import com.iscs.common.business.rule.processor.velocity.VelocityTransform;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.io.FileTools;
import com.iscs.insurance.interfaces.gl.Settings;
import com.iscs.uw.common.statementaccount.StatementAccount;
import com.ric.insurance.interfaces.gl.FileWriter;
import com.ric.insurance.interfaces.gl.FileWriterFactory;
import com.ric.interfaces.gl.Account;
import com.ric.interfaces.gl.AccountBuilder;
import com.ric.interfaces.gl.Book;
import com.ric.interfaces.gl.BookException;
import com.ric.interfaces.gl.Books;
import com.ric.interfaces.gl.Distribution;
import com.ric.interfaces.gl.GLAnnualStatement;
import com.ric.interfaces.gl.GLCarrierCd;
import com.ric.interfaces.gl.GLCompanyCd;
import com.ric.interfaces.gl.GLProductName;
import com.ric.interfaces.gl.Perl;
import com.ric.interfaces.gl.types.BigDecimals;
import com.ric.interfaces.gl.types.Strings;

import net.inov.biz.server.IBIZException;
import net.inov.mda.MDA;
import net.inov.mda.MDAException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;

/**
 * @author peter.hose
 *
 */
public abstract class GLGeneralLedgerProcessor extends GLHandler {
	
	public static class ReceiptTypeCd {
		public static final String UNKNOWN = "UNKNOWN";
		public static final String ACH = "ACH";
		public static final String CC = "Credit Card";
		public static final String PORTALCC = "Portal Credit Card";
		public static final String LOCKBOX = "Lockbox";
		public static final String LOCKBOX2 = "Lock Box";
		public static final String OTHER = "Other";
		public static final String BILLPAY = "Bill Pay";
		
		public static class Index {
			public static final int UNKNOWN = 0;
			public static final int ACH = 1;
			public static final int CC = 2;
			public static final int LOCKBOX = 3;
			public static final int OTHER = 4;
			public static final int BILLPAY = 5;
		
			public static int get(String text) {
				Integer result = index.get(text);

				return (result == null) ? UNKNOWN : result.intValue();
			}

			private static final Map<String, Integer> index = new HashMap<String, Integer>();

			static {
				index.put(ReceiptTypeCd.ACH, ACH);
				index.put(ReceiptTypeCd.CC, CC);
				index.put(ReceiptTypeCd.PORTALCC, CC);
				index.put(ReceiptTypeCd.LOCKBOX, LOCKBOX);
				index.put(ReceiptTypeCd.LOCKBOX2, LOCKBOX);
				index.put(ReceiptTypeCd.OTHER, OTHER);
				index.put(ReceiptTypeCd.BILLPAY, BILLPAY);
				
			}
		}
	}

	/**
	 * Beach/Coastal Indicator<br>
	 * This will need to be updated when the beach coastal indicator is implemented
	 * 
	 * @author peter.hose
	 *
	 */
	protected static class BC {
		protected static final String BEACH = "Beach";
		protected static final String COASTAL = "Coastal";
	}
	
	protected static class LOB {
		protected static final String UNKNOWN = "";
		protected static final String COMMERCIAL_WINDSTORM_BEACH = "COMBW";
		protected static final String COMMERCIAL_WINDSTORM_COASTAL = "COMCW";
		protected static final String COMMERCIAL_FIRE = "COMFIRE";
		protected static final String COMMERCIAL_EXTENDED_COVERAGE = "COMEC";
		protected static final String HOMEOWNERS_BEACH = "HOBEACH";
		protected static final String HOMEOWNERS_COASTAL = "HOCOASTAL";
		protected static final String HOMEOWNERS_WINDSTORM_BEACH = "HOBW";
		protected static final String HOMEOWNERS_WINDSTORM_COASTAL = "HOCW";
		protected static final String DWELLING_WINDSTORM_COASTAL = "RESCW";
		protected static final String DWELLING_WINDSTORM_BEACH = "RESBW";
		protected static final String DWELLING_EXTENDED_COVERAGE = "RESEC";
		protected static final String DWELLING_FIRE = "RESFIRE";
		protected static final String RESIDENTIAL_CRIME = "RESCRIME";
		protected static final String COMMERCIAL_CRIME = "COMCRIME";
		protected static final String FARM_BEACH = "TBD";
		protected static final String FARM_COASTAL = "TBD";
		protected static final String FARM_WINDSTORM_BEACH = "TBD";
		protected static final String FARM_WINDSTORM_COASTAL = "TBD";

		protected static String get(final String key) {
			return map.containsKey(key) ? map.get(key) : UNKNOWN;
		}
		
		protected static String get(final String productName, final String annualStatementLine) {
			return get(Perl.join(Perl.SEPARATOR, productName, annualStatementLine));
		}

		protected static String get(final String productName, final String annualStatementLine, final String beachInd) {
			return get(Perl.join(Perl.SEPARATOR, productName, annualStatementLine, beachInd));
		}
		
		protected static Map<String, String> map = new HashMap<String, String>();

		static {
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.COMMERCIAL_WIND, GLAnnualStatement.Line.ALLIED_LINES, BC.BEACH), COMMERCIAL_WINDSTORM_BEACH);		
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.COMMERCIAL_WIND, GLAnnualStatement.Line.ALLIED_LINES, BC.COASTAL), COMMERCIAL_WINDSTORM_COASTAL);		
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.COMMERCIAL_FIRE, GLAnnualStatement.Line.FIRE, ""), COMMERCIAL_FIRE);	
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.COMMERCIAL_FIRE, GLAnnualStatement.Line.FIRE, BC.BEACH), COMMERCIAL_FIRE);
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.COMMERCIAL_FIRE, GLAnnualStatement.Line.FIRE, BC.COASTAL), COMMERCIAL_FIRE);
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.COMMERCIAL_FIRE, GLAnnualStatement.Line.ALLIED_LINES, ""), COMMERCIAL_EXTENDED_COVERAGE);	
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.COMMERCIAL_FIRE, GLAnnualStatement.Line.ALLIED_LINES, BC.BEACH), COMMERCIAL_EXTENDED_COVERAGE);
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.COMMERCIAL_FIRE, GLAnnualStatement.Line.ALLIED_LINES, BC.COASTAL), COMMERCIAL_EXTENDED_COVERAGE);
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.DWELLING, GLAnnualStatement.Line.FIRE, BC.BEACH), DWELLING_FIRE);	
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.DWELLING, GLAnnualStatement.Line.FIRE, BC.COASTAL), DWELLING_FIRE);
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.DWELLING, GLAnnualStatement.Line.FIRE, ""), DWELLING_FIRE);
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.DWELLING, GLAnnualStatement.Line.ALLIED_LINES, BC.BEACH), DWELLING_EXTENDED_COVERAGE);
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.DWELLING, GLAnnualStatement.Line.ALLIED_LINES, BC.COASTAL), DWELLING_EXTENDED_COVERAGE);
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.DWELLING, GLAnnualStatement.Line.ALLIED_LINES, ""), DWELLING_EXTENDED_COVERAGE);
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.DWELLING_WIND, GLAnnualStatement.Line.ALLIED_LINES, BC.BEACH), DWELLING_WINDSTORM_BEACH);		
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.DWELLING_WIND, GLAnnualStatement.Line.ALLIED_LINES, BC.COASTAL), DWELLING_WINDSTORM_COASTAL);		
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.HOMEOWNERS, "", BC.BEACH), HOMEOWNERS_BEACH);
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.HOMEOWNERS, "", BC.COASTAL), HOMEOWNERS_COASTAL);
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.HOMEOWNERS_WIND, "", BC.BEACH), HOMEOWNERS_WINDSTORM_BEACH);
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.HOMEOWNERS_WIND, "", BC.COASTAL), HOMEOWNERS_WINDSTORM_COASTAL);
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.CRIME, GLAnnualStatement.Line.CRIME, BC.BEACH), RESIDENTIAL_CRIME);
			map.put(Perl.join(Perl.SEPARATOR, GLProductName.CRIME, GLAnnualStatement.Line.CRIME, BC.COASTAL), RESIDENTIAL_CRIME);
		}

	}

	protected static class SourceCode {
		protected static final String COMM = "INCOM";
		protected static final String CLAIMS = "INCLM";
		protected static final String REFUND = "INRPM";
	}
	protected static class ReferenceID {
		protected static final String REF_REFUND = "RPISSUED";
		protected static final String REF_COMM = "COMISSUED";
		protected static final String REF_CLAIMS = "CLMISSUED";
	}

	protected static class BankAccount {
		@Deprecated
		protected static final String ACCOUNT_BILL = "AccountBill";
		protected static final String ACCOUNT_BILL_NCIUA = "NCIUARefunds";
		protected static final String ACCOUNT_BILL_NCJUA = "NCJUARefunds";
		protected static final String CLAIMS = "Claims";
		@Deprecated
		
		protected static class CarrierCd {
			
			protected static String get(final String account) {
				String result = "";
				
				if (account.contains(GLCarrierCd.NORTH_CAROLINA_INSURANCE_UNDERWRITING_ASSOCIATION))
					result = GLCarrierCd.NORTH_CAROLINA_INSURANCE_UNDERWRITING_ASSOCIATION;
				else if (account.contains(GLCarrierCd.NORTH_CAROLINA_JOINT_UNDERWRITING_ASSOCIATION))
					result = GLCarrierCd.NORTH_CAROLINA_JOINT_UNDERWRITING_ASSOCIATION;
				
				return result;
			}
		}

	    /** Obtain the BankAccount ModelBean record from the account code
	     * @param data The data connection
	     * @param accountCd The account code
	     * @return The ModelBean containing the Bank Account information 
	     * @throws Exception when an error occurs
	     */
	    public ModelBean getAccount(JDBCData data, String accountCd)
	    throws Exception {
	    	return BeanTools.selectModelBeanFromLookup(data, "BankAccount", "AccountCd", accountCd);
	    }
	}

	protected static class FeeCd {
		//protected static final String NJGUF = "NJGUFFee";
		protected static final String CRC = "CRCTax";
	}

	public GLGeneralLedgerProcessor() {
		super();
	}

	@Override
	protected void init() throws Exception {
		super.init();

		if (Strings.Traits.isNullOrEmpty(runDt)) {
			throw new IBIZException("The run date cannot be null or blank.");
		}

		final StringDate temp = new StringDate(runDt);
		final StringDate start = new StringDate(temp.toDate());
		final StringDate end = new StringDate(temp.toDate());

		start.getCalendar().set(Calendar.DAY_OF_MONTH, start.getCalendar().getActualMinimum(Calendar.DAY_OF_MONTH));
		end.getCalendar().set(Calendar.DAY_OF_MONTH, end.getCalendar().getActualMaximum(Calendar.DAY_OF_MONTH));

		startDt = start.toString();
		endDt = end.toString();

		statementAccount = new StatementAccount();

		final ModelBean rs = getResponse();
		final ModelBean rsp = rs.getBean("ResponseParams");

		rsp.setValue(new ModelBean("Errors"));
	}

	@Override
	protected void open() throws Exception {
		super.open();

		final File directory = new File(settings.getSetting("baseDirectory"));

		if (!directory.exists()) {
			directory.mkdirs();
		}
	}

	@Override
	protected void actions() throws Exception {
		// Direct Premium and UW Fees
		processDirectWrittenPremium();
		processUnderwritingFeesAndTaxes();
		processDirectUnearnedPremium();
		// AR and Suspended Cash
		processBillingFeesCharged();
		processCRCInterestDue();
		processAccountsReceivableReceipts();
		processSuspenseReceipts();
		processSuspenseReceiptVoidedDueToNSFStopPay();
		processAccountsReceivableRefundEntered();
		processSuspenseRefundsEntered();
		processRefundAndSuspenseChecksPrinted(); // AP
		processVoidOfRefundOrSuspensePaymentsIssued(); // AP
		processSuspenseRefundVoided();
		processSuspendedCashAppliedToAccountsReceivableRemoveFromSuspense();
		processSuspendedCashAppliedToAccountsReceivableApplyToAccountsReceivable();
		processTransferBetweenAccountsReceivableAccounts();
		processTransferFromAccountsReceivableToSuspenseApplyToSuspense();
		processTransferFromAccountsReceivableToSuspenseRemoveFromAccountsReceivable();
		processAccountsReceivableAdjustments();
		processMiscellaneousReallocations();
		processAdvanceCashLiability();
		processDirectNonAdmittedReceivables();
		processUnclearedPaymentsMovedToUnclaimed();
		// Direct Commissions
		processDirectProducerCommissionExpense();
		processMonthlyCommissionPayments();
		processProducerCommmissionAdjustments();
		processDirectUnearnedCommission();
		// Direct Losses and ALAE
		processDirectLossAndALAEPaymentsEntered();
		processClaimPaymentsIssued();
		processVoidOfClaimPaymentsIssued();
		processDirectLossAndALAEMiscellaneousPaidAdjustments();
		processSalvageSubrogationOrDeductibleRecoveriesReceived();
		processSalvageSubrogationOrDeductibleRefunds();
		// Direct Losses and ALAE
		processDirectUnpaidLossesAndALAE();
	}

	/**
	 *
	 * <p><b>Tab:</b> Direct Premium and UW Fees</p>
	 * <p><b>Format:</b> General Ledger Journal Entry File</p>
	 * <pre>
	 * </pre>
	 *
	 * @throws IBIZException
	 */
	protected void processDirectWrittenPremium() throws IBIZException {
		final String process = "Direct Written Premium";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p><b>Tab:</b> Direct Premium and UW Fees</p>
	 * <p><b>Format:</b> General Ledger Journal Entry File</p>
	 *
	 * @throws IBIZException
	 */
	protected void processUnderwritingFeesAndTaxes() throws IBIZException {
		final String process = "UW Fees and Taxes";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>Direct Premium and UW Fees
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processDirectUnearnedPremium() throws IBIZException {
		final String process = "Direct Unearned Premium";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>AR and Suspended Cash
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processBillingFeesCharged() throws IBIZException {
		final String process = "Billing Fees Charged";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>AR and Suspended Cash
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processCRCInterestDue() throws IBIZException {
		final String process = "CRC Interest Due";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 * <p>
	 * Format: General Ledger Journal Entry File
	 * </p>
	 * <p>
	 * <b>Tab:&nbsp;</b>AR and Suspended Cash
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processAccountsReceivableReceipts() throws IBIZException {
		final String process = "AR Receipts";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>AR and Suspended Cash
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processSuspenseReceipts() throws IBIZException {
		final String process = "Suspense Receipts";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>AR and Suspended Cash
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processSuspenseReceiptVoidedDueToNSFStopPay() throws IBIZException {
		final String process = "Suspense - Receipt Voided due to NSF/Stop Pay";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>AR and Suspended Cash
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processAccountsReceivableRefundEntered() throws IBIZException {
		final String process = "AR Refund Entered";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>AR and Suspended Cash
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processSuspenseRefundsEntered() throws IBIZException {
		final String process = "Suspense Refunds Entered";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p><b>Tab:&nbsp;</b>AR and Suspended Cash</p>
	 * <p><b>Format:&nbsp;</b>Accounts Payable Invoice File</p>
	 * <p><b>Frequency:&nbsp;</b>Daily</p>
	 *
	 * @throws IBIZException
	 */
	protected void processRefundAndSuspenseChecksPrinted() throws IBIZException {
		final String process = "Refund and Suspense Checks Printed";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p><b>Tab:&nbsp;</b>AR and Suspended Cash</p>
	 * <p><b>Format:&nbsp;</b>Accounts Payable Check Stop/Void File</p>
	 * <p><b>Frequency:&nbsp;</b>Daily</p>
	 *
	 * @throws IBIZException
	 */
	protected void processVoidOfRefundOrSuspensePaymentsIssued() throws IBIZException {
		final String process = "Void of Refund or Suspense Payments Issued";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>AR and Suspended Cash
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processSuspenseRefundVoided() throws IBIZException {
		final String process = "Suspense Refund Voided";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>AR and Suspended Cash
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processSuspendedCashAppliedToAccountsReceivableRemoveFromSuspense() throws IBIZException {
		final String process = "Suspended Cash Applied to Accounts Receivable - Remove from Suspense";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>AR and Suspended Cash
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processSuspendedCashAppliedToAccountsReceivableApplyToAccountsReceivable() throws IBIZException {
		final String process = "Suspended Cash Applied to Accounts Receivable - Apply To Accounts Receivable";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>AR and Suspended Cash
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processTransferBetweenAccountsReceivableAccounts() throws IBIZException {
		final String process = "Transfer Between Accounts Receivable Accounts";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>AR and Suspended Cash
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processTransferFromAccountsReceivableToSuspenseApplyToSuspense() throws IBIZException {
		final String process = "Transfer From Accounts Receivable to Suspense - Apply to Suspense";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>AR and Suspended Cash
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processTransferFromAccountsReceivableToSuspenseRemoveFromAccountsReceivable() throws IBIZException {
		final String process = "Transfer From Accounts Receivable to Suspense - Remove from Accounts Receivable";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>AR and Suspended Cash
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processAccountsReceivableAdjustments() throws IBIZException {
		final String process = "Accounts Receivable Adjustments";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>AR and Suspended Cash
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processMiscellaneousReallocations() throws IBIZException {
		final String process = "Miscellaneous Reallocations";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>AR and Suspended Cash
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processAdvanceCashLiability() throws IBIZException {
		final String process = "Advance Cash Liability";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>AR and Suspended Cash
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processDirectNonAdmittedReceivables() throws IBIZException {
		final String process = "Direct Non-Admitted Receivables";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>AR and Suspended Cash
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processUnclearedPaymentsMovedToUnclaimed() throws IBIZException {
		final String process = "Uncleared Payments Moved to Unclaimed";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>Direct Commissions
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processDirectProducerCommissionExpense() throws IBIZException {
		final String process = "Direct Producer Commission Expense";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p><b>Tab:&nbsp;</b>Direct Commissions</p>
	 * <p><b>Format:&nbsp;</b>Accounts Payable Invoice File</p>
	 * <p><b>Frequency:&nbsp;</b>Monthly</p>
	 *
	 * <p>{@code SELECT AllocationAmt AS Amount, TransactionDt FROM PayableStats WHERE (PaymentAccountCd IN ('Commissions') AND TransactionTypeCd IN ('Print', 'Open', 'Unstop Payment', 'Unvoid Payment')) }</p>
	 *
	 * @throws IBIZException
	 */
	protected void processMonthlyCommissionPayments() throws IBIZException {
		final String process = "Monthly Commission Payments";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>Direct Commissions
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processDirectUnearnedCommission() throws IBIZException {
		final String process = "Direct Unearned Commission";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>Direct Losses and ALAE
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processDirectLossAndALAEPaymentsEntered() throws IBIZException {
		final String process = "Direct Loss and ALAE Payments Entered";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p><b>Tab:&nbsp;</b>Direct Losses and ALAE</p>
	 * <p><b>Format:&nbsp;</b>Accounts Payable Invoice File</p>
	 * <p><b>Frequency:&nbsp;</b>On Demand</p>
	 *
	 * @throws IBIZException
	 */
	protected void processClaimPaymentsIssued() throws IBIZException {
		final String process = "Claim Payments Issued";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p><b>Tab:</b> Direct Losses and ALAE</p>
	 * <p><b>Format:</b> Accounts Payable Invoice File</p>
	 *
	 * @throws IBIZException
	 */
	protected void processVoidOfClaimPaymentsIssued() throws IBIZException {
		final String process = "Void of Claim Payments Issued";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>Direct Losses and ALAE
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processDirectLossAndALAEMiscellaneousPaidAdjustments() throws IBIZException {
		final String process = "Direct Loss and ALAE Miscellaneous Paid Adjustments";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>Direct Losses and ALAE
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processSalvageSubrogationOrDeductibleRecoveriesReceived() throws IBIZException {
		final String process = "Salvage, Subrogation, or Deductible Recoveries Received";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>Direct Losses and ALAE
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processSalvageSubrogationOrDeductibleRefunds() throws IBIZException {
		final String process = "Salvage, Subrogation, or Deductible Refunds";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>Direct Losses and ALAE
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processDirectUnpaidLossesAndALAE() throws IBIZException {
		final String process = "Direct Unpaid Losses and ALAE";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	/**
	 *
	 * <p>
	 * <b>Tab:&nbsp;</b>Direct Commissions
	 * </p>
	 *
	 * @throws IBIZException
	 */
	protected void processProducerCommmissionAdjustments() throws IBIZException {
		final String process = "Producer Commission Adjustments";
		final String refid = getReference();

		writeReferenceProcess(refid, process);
		writeReferenceStatus(refid, Status.NOT_IMPLEMENTED);
		writeReferenceComplete(refid);
	}

	protected String getReference() {
		return String.format("%08d", reference++);
	}

	protected String writeReferenceComplete(final String reference) {
		try {
			if (fwReference != null) {
				fwReference.write("\n");
			}
		}
		catch (final Exception e) {
			swallow(e);
		}

		return reference;
	}
	
	
	protected String writeReference(final String reference, final String description) {
		return writeReference(this.fwReference, reference, description);
		
	}

	public static String writeReference(FileWriter fwGeneralLedgerReference, final String reference, final String description) {
		try {
				ModelBean bean = new ModelBean("GLReferenceEntry");
				bean.setValue("Reference", reference);
				bean.setValue("Description", description);
				
//				fwGeneralLedgerReference.write(bean, "ReferenceRecord::Standard");
		}
		catch (final Exception e) {
			e.printStackTrace();
		}

		return reference;
	}
	
	protected String writeAPReference(final String reference, final String description) {
		try {
			if (fwReference != null) {
				final Map<String, Object> map = new HashMap<String, Object>();
				
				map.put("Reference", reference);
				map.put("Description", description);
				
//				fwReference.write(map, "ReferenceRecord::Standard");
			}
		}
		catch (final Exception e) {
			swallow(e);
		}

		return reference;
	}

	/**
	 * Write a BookException to the reference file. Only the message needs to be
	 * written, the stack trace is overkill</br>
	 *
	 * @param reference
	 * @param exception
	 * @return
	 */
	protected String writeReference(final String reference, final BookException exception) {
		return writeReference(reference, exception.toString());
	}

	protected String writeReference(final String reference, final Exception exception) {
		try {
			writeReference(reference, exception.toString());

			for (final StackTraceElement element : exception.getStackTrace()) {
				writeReference(reference, element.toString());
			}
		}
		catch (final Exception e) {
			swallow(e);
		}

		return reference;
	}

	protected String writeReferenceProcess(final String reference, final String process) {
		return writeReference(reference, process);
	}

	protected String writeReferenceStatus(final String reference, final String status) {
		return writeReference(reference, String.format("Status: %s", status));
	}

	protected String writeReferenceSql(final String reference, final String sql) {
		return writeReference(reference, sql);
	}

	protected String writeReferenceData(final String reference, final String data) {
		return writeReference(reference, data);
	}

	public static String writeReferenceRecordReadCount(FileWriter fwReference,final String reference, final int count) {
		return writeReference(fwReference, reference, String.format("Records Read Count: %d", count));
	}

	public static String writeReferenceRecordMappedCount(FileWriter fwReference,final String reference, final int count) {
		return writeReference(fwReference, reference, String.format("Records Mapped Count: %d", count));
	}

	public static String writeReferenceRecordErrorCount(FileWriter fwReference, final String reference, final int count) {
		return writeReference(fwReference, reference, String.format("Records Error Count: %d", count));
	}

	public static String writeReferenceRecordSkippedCount(FileWriter fwReference, final String reference, final int count) {
		return writeReference(fwReference, reference, String.format("Records Skipped Count: %d", count));
	}
	
	protected String writeReferenceRecordCounts(final String reference, final int rcount, final int ecount, final int scount) {
		return writeReferenceRecordCounts(this.fwReference, reference, rcount, ecount, scount);
	}

	public static String writeReferenceRecordCounts(FileWriter fwReference, final String reference, final int rcount, final int ecount, final int scount) {
		writeReferenceRecordReadCount(fwReference, reference, rcount);
		writeReferenceRecordMappedCount(fwReference, reference, rcount - ecount - scount);
		writeReferenceRecordErrorCount(fwReference, reference, ecount);
		writeReferenceRecordSkippedCount(fwReference, reference, scount);

		return reference;
	}

	protected String writeReference(final String reference, final Properties properties) {
		final Enumeration<?> e = properties.propertyNames();

		while (e.hasMoreElements()) {
			final String key = (String) e.nextElement();
			writeReference(reference, String.format("%s:%s", key, properties.getProperty(key)));
		}

		return reference;
	}

	protected String writeReferenceBooks(final String reference, final Books books) {
		if (!books.isEmpty()) {
			writeReference(reference, "Books");
			writeReference(reference, String.format("Key Fields: %s", books.getKeydef().join()));
			writeReference(reference, books.getProperties());

			for (final Entry<String, Book> entry : books.entrySet()) {
				writeReference(reference, String.format("Book: %s", entry.getKey()));
				writeReferenceBook(reference, entry.getValue());
			}
		}

		return reference;
	}

	protected String writeReferenceBook(final String reference, final Book book) {
		final int COLUMN_SIZE_ACCOUNT = 24;
		final int COLUMN_SIZE_DEBIT = 12;
		final int COLUMN_SIZE_CREDIT = 12;
		final String FORMAT_HEADER = String.format("%%%ds\t%%%ds\t%%%ds", COLUMN_SIZE_ACCOUNT, COLUMN_SIZE_DEBIT, COLUMN_SIZE_CREDIT);
		final String FORMAT_LINE = String.format("%%%ds\t%%%d.2f\t%%%d.2f", COLUMN_SIZE_ACCOUNT, COLUMN_SIZE_DEBIT, COLUMN_SIZE_CREDIT);

		writeReference(reference, book.getProperties());

		final List<Account> accounts = AccountBuilder.build(book);

		if (!accounts.isEmpty()) {
			final String HEADER_ACCOUNT = Strings.rpad("Account", COLUMN_SIZE_ACCOUNT);
			final String HEADER_DEBIT = String.format("(%s)    Debit", (BigDecimals.Traits.isNegative(debit(BigDecimal.ONE))) ? "-" : "+");
			final String HEADER_CREDIT = String.format("(%s)   Credit", (BigDecimals.Traits.isNegative(credit(BigDecimal.ONE))) ? "-" : "+");

			final String SEPARATOR_ACCOUNT = Strings.repeat("-", COLUMN_SIZE_ACCOUNT);
			final String SEPARATOR_DEBIT = Strings.repeat("-", COLUMN_SIZE_DEBIT);
			final String SEPARATOR_CREDIT = Strings.repeat("-", COLUMN_SIZE_CREDIT);

			final String HEADER = String.format(FORMAT_HEADER, HEADER_ACCOUNT, HEADER_DEBIT, HEADER_CREDIT);
			final String DIVIDER = String.format(FORMAT_HEADER, SEPARATOR_ACCOUNT, SEPARATOR_DEBIT, SEPARATOR_CREDIT);

			BigDecimal debit = BigDecimal.ZERO.setScale(2);
			BigDecimal credit = BigDecimal.ZERO.setScale(2);

			writeReference(reference, HEADER);
			writeReference(reference, DIVIDER);
			for (final Account account : accounts) {
				writeReference(reference, String.format(FORMAT_LINE, account.key, account.debit, account.credit));
				debit = debit.add(account.debit);
				credit = credit.add(account.credit);
			}
			writeReference(reference, DIVIDER);
			writeReference(reference, String.format(FORMAT_LINE, Strings.rpad("Total:", COLUMN_SIZE_ACCOUNT), debit, credit));
		}

		return reference;
	}

	protected void reportRecordCounts(final String process, final int rcount, final int ecount, final int scount) {
		// Only report errors at this time.
		if (ecount > 0) {
			final String message = String.format("Failed to process %d records", ecount);

			addErrorMsg("Process Error", process + " - " + message, "Data", "Error");
		}
	}

	protected static BigDecimal debit(final BigDecimal value) {
		return value.plus();
	}

	protected static BigDecimal credit(final BigDecimal value) {
		return value.negate();
	}

	protected void mapped(final String debit, final String credit, final String reference) throws IBIZException {
		if (debit == null)
			throw new IBIZException(String.format("Failed to map %s to debit account", reference));

		if (credit == null)
			throw new IBIZException(String.format("Failed to map %s to credit account", reference));
	}

	protected void write(final Books books) throws MDAException, Exception {
		final Properties pbooks = books.getProperties();

		for (final Entry<String, Book> entry : books.entrySet()) {
			final String key = entry.getKey();
			final Book book = entry.getValue();
			final Properties pbook = book.getProperties();
			final String companyCode = toCompanyCode(pbook.getProperty("CarrierCd"));

			final Distribution debits = book.debits();
			final Distribution credits = book.credits();

			for (final Entry<String, BigDecimal> _entry : debits.entrySet()) {
				final String account = _entry.getKey();
				final BigDecimal value = _entry.getValue();
				final ModelBean bean = new ModelBean("GLJournalEntry");

				bean.setValue("AccountNumber", account);
				bean.setValue("AccountingBasis", pbooks.get("AccountingBasis"));
				bean.setValue("CalendarAccountingMonth", runMonth);
				bean.setValue("CalendarAccountingYear", runYear);
				bean.setValue("CompanyCode", companyCode);
				bean.setValue("ConvertedAmount", debit(value));
				bean.setValue("ReferenceId", pbooks.get("ReferenceId"));
				bean.setValue("SourceCode", pbooks.get("SourceCode"));
				bean.setValue("TransactionDay", runDay);
				bean.setValue("TransactionMonth", runMonth);
				bean.setValue("TransactionYear", runYear);
				bean.setValue("BusinessUnitCode1", pbook.containsKey("BusinessUnitCode1") ? pbook.getProperty("BusinessUnitCode1") : "");
				bean.setValue("BusinessUnitCode2", pbook.containsKey("BusinessUnitCode2") ? pbook.getProperty("BusinessUnitCode2") : "");
				bean.setValue("BusinessUnitCode3", pbook.containsKey("BusinessUnitCode3") ? pbook.getProperty("BusinessUnitCode3") : "");
				bean.setValue("BusinessUnitCode4", pbook.containsKey("BusinessUnitCode4") ? pbook.getProperty("BusinessUnitCode4") : "");
				bean.setValue("DescriptionDetail", pbooks.getProperty("DescriptionDetail"));
				bean.setValue("HeaderDescription", pbooks.getProperty("HeaderDescription"));

				fwGeneralLedger.write(bean, "JournalRecord::Standard");
			}

			for (final Entry<String, BigDecimal> _entry : credits.entrySet()) {
				final String account = _entry.getKey();
				final BigDecimal value = _entry.getValue();
				final ModelBean bean = new ModelBean("GLJournalEntry");

				bean.setValue("AccountNumber", account);
				bean.setValue("AccountingBasis", pbooks.get("AccountingBasis"));
				bean.setValue("CalendarAccountingMonth", runMonth);
				bean.setValue("CalendarAccountingYear", runYear);
				bean.setValue("CompanyCode", companyCode);
				bean.setValue("ConvertedAmount", credit(value));
				bean.setValue("ReferenceId", pbooks.get("ReferenceId"));
				bean.setValue("SourceCode", pbooks.get("SourceCode"));
				bean.setValue("TransactionDay", runDay);
				bean.setValue("TransactionMonth", runMonth);
				bean.setValue("TransactionYear", runYear);
				bean.setValue("BusinessUnitCode1", pbook.containsKey("BusinessUnitCode1") ? pbook.getProperty("BusinessUnitCode1") : "");
				bean.setValue("BusinessUnitCode2", pbook.containsKey("BusinessUnitCode2") ? pbook.getProperty("BusinessUnitCode2") : "");
				bean.setValue("BusinessUnitCode3", pbook.containsKey("BusinessUnitCode3") ? pbook.getProperty("BusinessUnitCode3") : "");
				bean.setValue("BusinessUnitCode4", pbook.containsKey("BusinessUnitCode4") ? pbook.getProperty("BusinessUnitCode4") : "");
				bean.setValue("DescriptionDetail", pbooks.getProperty("DescriptionDetail"));
				bean.setValue("HeaderDescription", pbooks.getProperty("HeaderDescription"));

				fwGeneralLedger.write(bean, "JournalRecord::Standard");
			}
		}
	}

	protected static String toCompanyCode(final String carrierCd) {
		String result = "";

		switch (GLCarrierCd.Index.get(carrierCd))
		{
		case GLCarrierCd.Index.NORTH_CAROLINA_INSURANCE_UNDERWRITING_ASSOCIATION:
			result = GLCompanyCd.NORTH_CAROLINA_INSURANCE_UNDERWRITING_ASSOCIATION;
			break;
		case GLCarrierCd.Index.NORTH_CAROLINA_JOINT_UNDERWRITING_ASSOCIATION:
			result = GLCompanyCd.NORTH_CAROLINA_JOINT_UNDERWRITING_ASSOCIATION;
			break;
		}

		return result;
	}

	protected static String toCalendarAccountingPeriod(final StringDate date) {
		date.getCalendar().set(Calendar.DAY_OF_MONTH, date.getCalendar().getActualMaximum(Calendar.DAY_OF_MONTH));

		return date.toString();
	}

	protected static String toReferenceDate(final String runDt) {
		final SimpleDateFormat fmt = new SimpleDateFormat("MM-dd-yyyy");
		return fmt.format(new StringDate(runDt).toDate());
	}

	protected static String toReportPeriod(final String runDt, final int months) {
		StringDate date = new StringDate(runDt);

		if (months != 0)
			date = StringDate.advanceMonth(date, months);

		return date.toString().substring(0, 6);
	}

	protected ModelBean findPayPlan(final String payplanCd) throws IBIZException {
		ModelBean bean = payplans.get(payplanCd);

		try {
			if (bean == null) {
				bean = (ModelBean) MDA.getModelObject("AR", "payplan", "All", payplanCd.replaceAll(" ", "-"));
				payplans.put(payplanCd, bean);
			}
		}
		catch (final MDAException e) {
			throw new IBIZException(String.format("Failed to load payplan: %s", payplanCd), e);
		}

		return bean;
	}

	protected String findBillMethod(final ModelBean policy, final String statementAccountNumber, final String payplanCd) throws IBIZException {
		String method = null;

		if (statementAccountNumber.isEmpty())
			method = findBillMethod(payplanCd);
		else
			try {
				method = findBillMethod(statementAccount.mapStatementAccount(policy, payplanCd));
			}
			catch (final Exception e) {
				throw new IBIZException(String.format("Failed to load payplan: %s, %s", statementAccountNumber, payplanCd), e);
			}

		return method;
	}

	protected String findBillMethod(final String payplanCd) throws IBIZException {
		String method = null;

		try {
			method = findPayPlan(payplanCd).gets("BillMethod");

			if (method == null || method.isEmpty())
				throw new IBIZException("Failed to determine the Bill Method from the payplan using payplan code: " + payplanCd);
		}
		catch (final ModelBeanException e) {
			throw new IBIZException(String.format("Failed to determine bill method for payplan: %s", payplanCd), e);
		}

		return method;
	}

	private String _getCarrierCd(final String key) {
		String result = "";

		if (!(key == null || key.isEmpty())) {
			try {
				final ModelBean account = HPAccount.getAccountByAccountNumber(key);
				final ModelBean policy = account.getBean("ARPolicy");

				result = policy.gets("CarrierCd");
			}
			catch (final Exception e) {
				log.error(String.format("Failed to get carrier code for key: %s", key), e);
			}
		}

		return result;
	}

	protected ModelBean findBankAccount(final String paymentAccountCd) {
		ModelBean result = null;

		try {
			result = new com.iscs.payables.account.BankAccount().getAccount(getConnection(), paymentAccountCd);
		}
		catch (final Exception e) {
			_swallow(e);
		}

		return result;
	}
	
	/** Create the general ledger export file requested. Do not allow any current files to be overridden
	 * @param settingsId The settings file section id
	 * @param fileName The generated filename to use
	 * @param append Indicator to pass on to determine whether to overwrite or append to file. The validation overrides this as we are calling a common method
	 * @return A FileWriter object to the generated file from the settings file
	 * @throws Exception when an error occurs
	 */
	public static FileWriter createFile(String settingsId, String fileName, boolean append, boolean allowOverwriteInd) throws Exception, IBIZException {
		if (FileTools.exists(fileName)) {
//			if (!allowOverwriteInd) {
//				throw new IBIZException("General Ledger File " + fileName + " currently exists and cannot be overwritten.");
//			}
		} else {
			// Create the directory path if it doesn't exist
			File file = new File(fileName);
			File parentDirectory = file.getParentFile();
			FileTools.createPath(parentDirectory.toString());
		}

		return FileWriterFactory.create(settingsId, fileName, append);	
	}

	protected String findSourceCarrierCd(final String sourceCd) {
		return _getCarrierCd(sourceCd);
	}

	protected String findAccountCarrierCd(final String accountNumber) {
		return _getCarrierCd(accountNumber);
	}

	protected String formatReferenceId(final String referenceid) {
		return referenceid.substring(0, Math.min(10, referenceid.length())) + referenceDt; // .replaceAll("-",
																							// "");
	}

	protected ResultSet execute(final String sql) throws SQLException {
		final JDBCData data = getConnection();

		return data.doSQL(sql);
	}

	protected void _swallow(final Exception e) {
		log.warn("swallow", e);
	}
	
	/** Safely set the scale of a BigDecimal 
	 * @param value The value to scale
	 * @param scale The amount to scale
	 * @return The value passed in with scaling applied or null
	 */
	protected static BigDecimal scaled(BigDecimal value, int scale) {
		return (value != null) ? value.setScale(scale) : value;
	}
	
	protected static final int ARITHMETIC_SCALE = 2;

	protected String startDt = null;
	protected String endDt = null;
	protected String referenceDt = null;

	protected StatementAccount statementAccount = null;

	protected FileWriter fwReference = null;
	protected FileWriter fwGeneralLedger = null;

	private Integer reference = new Integer(0);

	private final Map<String, ModelBean> payplans = new HashMap<String, ModelBean>();

	private final Logger log = LogManager.getLogger(GLGeneralLedgerProcessor.class);
	
}
//APFilewriter is a contaner class for all of the filewriters associated with the AP 
class APFileWriter {
	
		public static final int TRANS_TYPE_CLAIMS = 0;
		public static final int TRANS_TYPE_REFUND = 1;
		public static final int TRANS_TYPE_COMMISSION = 3;
		
		public static final int PAYMENT_TYPE_BATCHCHECK = 0;
		public static final int PAYMENT_TYPE_ACH = 1;
		public static final int PAYMENT_TYPE_MANUAL_CHECK_CREDIT_CARD_DEBIT_CARD = 2;
		
		public static final String REFUND_REFERENCE_ID = "RPISSUED";
		public static final String COMMISSION_REFERENCE_ID = "COMISSUED";
		public static final String CLAIMS_REFERENCE_ID = "CLMISSUED";
		

		
		FileWriter fwAPSubLedger = null;
		FileWriter fwAccountsPayableStopPayment = null;
		FileWriter fwAccountsPayableVoidPayment = null;
		FileWriter fwAPUnVoidUnStopPayment = null;
		FileWriter fwGeneralLedgerReference = null;
		
		JDBCData data;
		Settings settings;
		String transType;
		String paymentType;
		String carrierCd;
		String runDt;
		boolean appendInd; 
		boolean reprocessInd;
		
		
		public APFileWriter(JDBCData data, Settings settings,int transType,int paymentType,String carrierCd, String runDt, boolean appendInd, boolean reprocessInd) throws IBIZException, Exception{
			this(data, settings, transType, paymentType,carrierCd, runDt, appendInd, reprocessInd, false, false);
		}
		
		public APFileWriter(JDBCData data, Settings settings, String transactionCd,String runDt) throws IBIZException, Exception{
			this.settings = settings;
			this.data = data;
			this.runDt = runDt;
			if(transactionCd.equalsIgnoreCase("Stop Payment")){
				 createStopFile();
			}else{
				createVoidFile();
			}
		}
		
		public APFileWriter(JDBCData data, Settings settings, String runDt) throws IBIZException, Exception{
			this.settings = settings;
			this.data = data;
			this.runDt = runDt;
			createUnStopFile(); 
		}
		//AP files generated use the transaction type, payment type, carrier and timestamp in their filename
		public APFileWriter(JDBCData data, Settings settings,int transType,int paymentType,String carrierCd, String runDt, boolean appendInd, boolean reprocessInd, boolean omitReferenceFile, boolean omitVoidStopUnvoidUnstop) throws IBIZException, Exception{
			this.appendInd=appendInd;
			this.reprocessInd = reprocessInd;
			this.settings = settings;
			this.data = data;
			this.carrierCd = carrierCd;
			this.runDt = runDt;

			switch(transType){
				case TRANS_TYPE_CLAIMS :  this.transType = settings.getSetting("claimPrefix");break;
				case TRANS_TYPE_REFUND :  this.transType = settings.getSetting("refundPrefix");break;
				case TRANS_TYPE_COMMISSION :  this.transType = settings.getSetting("commissionPrefix");break;
				default : throw new Exception("General Ledger setting not found for Transaction Type");
			
			}
			switch(paymentType){
				case PAYMENT_TYPE_BATCHCHECK :  this.paymentType = settings.getSetting("batchCheckPayment");break;
				case PAYMENT_TYPE_ACH :  this.paymentType = settings.getSetting("achPayment");break;
				case PAYMENT_TYPE_MANUAL_CHECK_CREDIT_CARD_DEBIT_CARD :  this.paymentType = settings.getSetting("manualCheckCardPayment");break;
				default : throw new Exception("General Ledger setting not found for Payment Type");
		
			}

			//create the reference file if it's used
			if(!omitReferenceFile){
//				createReferenceFile();
			}
			
			//if the filterEmptyAPFiles is set to anything but yes, create the AP  files now
			//when the filterEmptyAPFiles is set to Yes, the file will be created only if data is written out to them
			if(!settings.getSetting("filterEmptyAPFiles").equalsIgnoreCase("Yes") ){
				 createAPFile();
				 if(!omitVoidStopUnvoidUnstop){
//					 createVoidFile();
//					 createStopFile();
//				 	createUnStopFile(); 
				 }
			}			
		}
		
		private void createAPFile() throws Exception{ fwAPSubLedger = getAPSettings(data,settings,"dailyApFilePath","SunGard::APSubLedger");}
		/*private void createReferenceFile() throws Exception{ fwGeneralLedgerReference = getAPSettings(data,settings,"dailyApReferenceFilePath","SunGard::GLReferenceEntry");}*/
//		private void createVoidStopFile() throws Exception{ fwAccountsPayableVoidStopPayment = getAPSettings(data,settings,"dailyApVoidStopPaymentFilePath","SunGard::APSubLedgerStopVoid");}
		private void createVoidFile() throws Exception{ fwAccountsPayableVoidPayment = getAPSettings(data,settings,"dailyExternalVoidPaymentFilePath","SunGard::APSubLedgerStopVoid");}
		private void createStopFile() throws Exception{ fwAccountsPayableStopPayment = getAPSettings(data,settings,"dailyExternalStopPaymentFilePath","SunGard::APSubLedgerStopVoid");}
		private void createUnStopFile() throws Exception{ fwAPUnVoidUnStopPayment = getAPSettings(data,settings,"dailyApUnVoidUnStopFilePath","SunGard::APSubLedgerStopVoid");}
		
		public void writeAPFile(ModelBean bean) throws Exception { 
			if(fwAPSubLedger == null){
				createAPFile();
			}
			APTools.writeAPBean(bean, fwAPSubLedger);
		}
		public void writeReferenceFile(ModelBean bean) throws Exception { 
			//if the file has not been created, create it
			if(fwGeneralLedgerReference == null){
//				createReferenceFile();
			}
			APTools.writeReferenceBean(bean, fwGeneralLedgerReference);
		}
		public void writeVoidFile(ModelBean bean) throws Exception { 
			//if the file has not been created, create it
			if(fwAccountsPayableVoidPayment == null){
				createVoidFile();
			}
			APTools.writeVoidStopBean(bean, fwAccountsPayableVoidPayment);
		}
		
		public void writeVoidFile(final Map<String, Object> map) throws Exception { 
			//if the file has not been created, create it
			if(fwAccountsPayableVoidPayment == null){
				createVoidFile();
			}
			fwAccountsPayableVoidPayment.write(map, "PayableRecord::StopVoid");
		}
		public void writeStopFile(ModelBean bean) throws Exception { 
			//if the file has not been created, create it
			if(fwAccountsPayableStopPayment == null){
				createVoidFile();
			}
			APTools.writeVoidStopBean(bean, fwAccountsPayableStopPayment);
		}
		
		public void writeStopFile(final Map<String, Object> map) throws Exception { 
			//if the file has not been created, create it
			if(fwAccountsPayableStopPayment == null){
				createVoidFile();
			}
			fwAccountsPayableStopPayment.write(map, "PayableRecord::StopVoid");
		}
		public void writeReferences(String reference,String description) throws Exception { 
			//if the file has not been created, create it
			if(fwGeneralLedgerReference == null){
//				createReferenceFile();
			}
			
			try {
//				createReferenceFile();
					ModelBean bean = new ModelBean("GLReferenceEntry");
					bean.setValue("Reference", reference);
					bean.setValue("Description", description);
//					fwGeneralLedgerReference.write(bean, "ReferenceRecord::Standard");
			}
			catch (final Exception e) {
				e.printStackTrace();
			}

		}

		public void writeUnStopFile(ModelBean bean) throws Exception { 
			//if the file has not been created, create it
			if(fwAPUnVoidUnStopPayment == null){
				createUnStopFile();
			}
			fwAPUnVoidUnStopPayment.write(bean, "PayableRecord::StopVoid");
		}

		public void close(){
			if (fwAPSubLedger != null) { try { fwAPSubLedger.close(); } catch (Exception e) { Log.error(e); } }
			if (fwAccountsPayableStopPayment != null) { try { fwAccountsPayableStopPayment.close(); } catch (Exception e) { Log.error(e); } }
			if (fwAccountsPayableVoidPayment != null) { try { fwAccountsPayableVoidPayment.close(); } catch (Exception e) { Log.error(e); } }
			if (fwAPUnVoidUnStopPayment != null) { try { fwAPUnVoidUnStopPayment.close(); } catch (Exception e) { Log.error(e); } }
			if (fwGeneralLedgerReference != null) { try { fwGeneralLedgerReference.close(); } catch (Exception e) { Log.error(e); } }
		}		
		
		private FileWriter getAPSettings(JDBCData data, Settings settings, String apSettingsName, String settingsId) throws Exception{
			String dir = settings.getSetting(apSettingsName);
			
			VelocityContext velocityContext = new VelocityContext();
			velocityContext.put("TransType", transType);
			velocityContext.put("PaymentType", paymentType);
			velocityContext.put("CarrierCd", carrierCd);
			velocityContext.put("RunDt", runDt);
			// Initialize the Singleton VelocityTransform(er)
	        VelocityTransform.initialize();
			// Create a new transformer object
	        VelocityTransform vt = new VelocityTransform();
	        // After calling the transform method, the template has been merged with context
	        dir = vt.transform( velocityContext, dir );
			return GLGeneralLedgerProcessor.createFile(settingsId, dir, appendInd, reprocessInd);
		}
		
		public String writeReferenceRecordCount(String reference, int count) {
			return GLGeneralLedgerProcessor.writeReference(fwGeneralLedgerReference,reference,String.format("Record Count: %d", count));
		}
		public String writeReferenceRecordCounts(final String reference, final int rcount, final int ecount, final int scount) {return GLGeneralLedgerProcessor.writeReferenceRecordCounts(fwGeneralLedgerReference,reference, rcount, ecount, scount);}

	 	
}
