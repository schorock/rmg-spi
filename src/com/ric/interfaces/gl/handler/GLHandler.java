package com.ric.interfaces.gl.handler;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;

import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.insurance.interfaces.gl.Settings;
import com.ric.interfaces.gl.Book;
import com.ric.interfaces.gl.Books;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

/**
 * Root of all General Ledger jobs
 * 
 * @author peter.hose
 *
 */
public class GLHandler extends InnovationIBIZHandler {

	protected static class Status {
		public static final String NOT_USED = "Not Used";
		public static final String NOT_IMPLEMENTED = "Not Implemented";
		public static final String DEVELOPMENT = "In Development";
		public static final String TESTING = "In Testing";
		public static final String COMPLETE = "Complete";
	}

	public GLHandler() {
		super();
	}

	@Override
	public ModelBean process() throws IBIZException, ServiceHandlerException {
		greeting();

		try {
			init();

			try {
				open();

				actions();
			}
			finally {
				close();
			}

			last();

			return null;
		}
		
		catch (IBIZException e) {
			throw e;
		}
		catch (Exception e) {
			throw new ServiceHandlerException(e);
		}
	}

	/**
	 * Announce to the log that this job is running
	 */
	protected void greeting() {
		try {
			final ModelBean rq = getRequest();
			final ModelBean rs = getResponse();

			Log.debug(String.format("Processing %s ...", getClass().getName()));
			//
			// Silly debugging statements
			//
			if (Log.isDebugEnabled()) {
				Log.debug(rq.toPrettyString());
				Log.debug(rs.toPrettyString());
			}
		}
		catch (Exception e) {
			swallow(e); // Fail quietly
		}
	}

	/**
	 * Initialize all common data members
	 * 
	 * @throws Exception
	 */
	protected void init() throws Exception {
		final ModelBean rq = getRequest();
		final AdditionalParams ap = new AdditionalParams(rq);
		//
		// Obtain the common parameters needed for the batch job
		//
		if ((runDt = ap.gets("RunDt")) != null) {
			runDay = runDt.substring(6, 8);
			runMonth = runDt.substring(4, 6);
			runYear = runDt.substring(0, 4);
		}
	}

	/**
	 * Open any files and / or external connections
	 * 
	 * @throws Exception
	 */
	protected void open() throws Exception {
	}

	/**
	 * Process individual actions, and transactions
	 * 
	 * @throws Exception
	 */
	protected void actions() throws Exception {
	}

	/**
	 * Close any open files and / or external connections 
	 * 
	 * @throws Exception
	 */
	protected void close() throws Exception {
	}

	/**
	 * Perform any final actions or cleanup before giving up control
	 * 
	 * @throws Exception
	 */
	protected void last() throws Exception {
	}


	/**
	 * Remove zero-sum accounts
	 * 
	 * @param books
	 */
	protected void offsets(Books books) {
		books.offsets();
	}

	/**
	 * Remove zero-sum accounts
	 * 
	 * @param book
	 */
	protected void offsets(Book book) {
		book.offsets();
	}
	
	/**
	 * Reduce accounts with off setting debit and credits
	 * 
	 * @param books
	 */
	protected void reduce(Books books) {
		books.reduce();
	}
	
	/**
	 * Reduce accounts with off setting debit and credits
	 * 
	 * @param book
	 */
	protected void reduce(Book book) {
		book.reduce();
	}
	
	/**
	 * Verify that all books balance
	 * 
	 * @param books
	 * @throws IBIZException
	 */
	protected void balanced(Books books) throws IBIZException {
		final List<String> keys = books.balanced();

		if (!keys.isEmpty()) {
			for (String key : keys)
				addErrorMsg(String.format("Book: %s", key), "does not balance");

			throw new IBIZException("Books are out of balance");
		}
	}

	protected String join(ResultSet rs) {
		final int BASE = 1;
		final StringBuilder sb = new StringBuilder();
		
		try {
			final ResultSetMetaData meta = rs.getMetaData();
			
			for (int index = BASE, count = meta.getColumnCount() + BASE; index < count; ++index) {
				final String value = rs.getString(index);
				
				if (index > BASE)
					sb.append("::");
				
				sb.append(meta.getColumnName(index));
				sb.append(":");
				sb.append((value == null) ? null : value);
			}
		}
		catch (SQLException e) {
			swallow(e); // This is a debugging method.  So handle this quietly
		}		
		
		return sb.toString();
	}
	
	protected StringDate getRunDate() {
		return new StringDate(runDt);
	}

	/**
	 * Generic handler for exceptions the are to be swallowed. The may be empty
	 * or used for debugging
	 * 
	 * @param e an instance of an Exception or one of its derived classes
	 */
	protected void swallow(Exception e) {
		e.printStackTrace();
	}

	protected final Settings settings = new Settings();
	/**
	 * Run Date in YYYYMMDD format
	 */
	protected String runDt = null;
	protected String runDay = null;
	protected String runMonth = null;
	protected String runYear = null;
}