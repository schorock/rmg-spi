/**
 * 
 */
package com.ric.interfaces.gl.handler;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.jms.IllegalStateException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.iscs.ar.render.AccountRenderer;
import com.iscs.claims.claim.Claim;
import com.iscs.common.business.provider.Provider;
import com.iscs.common.render.DateRenderer;
import com.iscs.common.render.DynamicString;
import com.iscs.common.render.StringRenderer;
import com.iscs.common.shared.SystemBookDt;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.InnovationUtils;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.io.FileTools;
import com.iscs.insurance.interfaces.gl.Settings;
import com.iscs.payables.account.AccountPayable;
import com.ric.insurance.interfaces.gl.FileWriter;
import com.ric.insurance.interfaces.gl.output.handler.GLOutputHandler;
import com.ric.interfaces.gl.SQL;
import com.ric.interfaces.gl.handler.APTools.APAccountType;
import com.ric.interfaces.gl.types.BigDecimals;
import com.ric.interfaces.gl.types.Strings;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.Helper_Beans;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;
import net.inov.tec.data.JDBCLookup;
import net.inov.tec.data.JDBCLookup.LookupKey;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;

/**
 * @author mkandasamy
 * 
 */
public class APGeneralLedgerProcessor extends GLGeneralLedgerProcessor {
	//private FileWriter fwGeneralLedgerReference = null;
	//private FileWriter fwAccountsPayableVoidPayment = null;
	//private FileWriter fwAccountsPayableStopPayment = null;
	//private FileWriter fwAPUnVoidUnStopPayment = null;
	
	private String runDt = null;
	private Integer reference = new Integer(0);
	StringDate bookDt = null;
	String acctCheckDates = "";
	JDBCData calendarData = null;
	JDBCData data = null;
	StringDate reprocessDt = null;
	String reprocessTm = null;
	Boolean appendInd;
	Boolean reprocessInd;
	ModelBean rq;
	
	private FileWriter fwGeneralLedgerReference = null;	
	APFileWriter dailyAPFileWriter = null;
	APFileWriter dailyAPStopFileWriter = null;
	APFileWriter dailyAPVoidFileWriter = null;
	APFileWriter dailyRccAPFileWriter = null;
	APFileWriter dailyRicAPFileWriter = null;
	/** Constructor
	 * 
	 */
	public APGeneralLedgerProcessor() {
		super();
	}

	@Override
	protected void init() throws Exception {
		super.init();

		settings.getMap().put("RunDt", runDt);
		referenceDt = toReferenceDate(runDt);
	}

	
	/**Getter
	 * 
	 * @return StringDate runDate
	 */
	protected StringDate getRunDate() {
		return new StringDate(runDt);
	}

	/** Getter
	 * 
	 * @return String reference
	 */
	protected String getReference() {
		return String.format("%08d", reference++);
	}

	/**Processes a generic service request.	  
	 * @param data
	 * @param rq
	 * @param rsprocessDirectWrittenPremium
	 * @return the current response bean
	 * @throws IBIZException
	 * @throws ServiceHandlerException
	 */
	public ModelBean process(JDBCData data, ModelBean rq, ModelBean rs) throws IBIZException, ServiceHandlerException {
		return process(data, rq, rs, false, false);
	}
	
	/**Processes a generic service request.	  
	 * @param data
	 * @param rq
	 * @param rs
	 * @return the current response bean
	 * @throws IBIZException
	 * @throws ServiceHandlerException
	 */
	public ModelBean process(JDBCData data, ModelBean rq, ModelBean rs, boolean appendInd, boolean reprocessInd) throws IBIZException, ServiceHandlerException {
		Log.debug(String.format("Processing %s ...", getClass().getName()));

		try {
			
			this.rq = rq;
			AdditionalParams ap = new AdditionalParams(rq);
			
			this.data = data;
			this.appendInd = appendInd;
			this.reprocessInd = reprocessInd;
			Settings settings = new Settings();	
			
			// Check if the batch check requests are being printed via automatic Job
            boolean autoProcess = false;
            ModelBean autoProcessParam = ap.findBeanByFieldValue("Param","Name", "AutoBatchRelease");
            if(autoProcessParam!=null){
            	autoProcess = StringTools.isTrue(autoProcessParam.gets("Value"));
            } 

            if( autoProcess ) {
            	ModelBean runDtParam = ap.findBeanByFieldValue("Param","Name","RunDt");
            	bookDt = new StringDate(runDtParam.gets("Value"));
            }
            else {
                bookDt = SystemBookDt.getBookDt(data);
            }
            
            String dateTimeStamp = bookDt.toString();
           
        	ModelBean dateTimeStampParam = ap.findBeanByFieldValue("Param","Name","DateTimeStamp");
        	if (dateTimeStampParam != null) {
        		dateTimeStamp = dateTimeStampParam.gets("Value");
        	}
        	this.runDt = dateTimeStamp;
			settings.getMap().put("RunDt", dateTimeStamp);
			this.referenceDt = bookDt.toStringDisplay("MMddyy");
			
			String bankAccountCd = ap.gets("AccountCd");

			if (reprocessInd) {
				String fileDateValue = null	;
	        	ModelBean reprocessDateParam = ap.findBeanByFieldValue("Param","Name","FileDate");
	        	if (reprocessDateParam != null) {
	        		fileDateValue = reprocessDateParam.gets("Value");
	        	}
				if (fileDateValue == null || fileDateValue.isEmpty()) {
					throw new IBIZException("General Ledger File processing requires a valid date to reprocess and rebuild the file(s)");
				}
				reprocessDt = new StringDate(fileDateValue);

	        	ModelBean reprocessTimeParam = ap.findBeanByFieldValue("Param","Name","FileTime");
	        	if (reprocessTimeParam != null) {
	        		reprocessTm = reprocessTimeParam.gets("Value");
	        	}
				if (reprocessTm.isEmpty()) {
					throw new IBIZException("General Ledger File processing requires a valid time to reprocess and rebuild the file(s)");
				}
				
			}
			
			try {
				
//				fwGeneralLedgerReference = createFile("SunGard::GLReferenceEntry", settings.getSetting("dailyApReferenceFilePath"), appendInd, reprocessInd);

				// Extract the commissions
				if(bankAccountCd.startsWith("BOA") || bankAccountCd.endsWith("Commissions") || bankAccountCd.endsWith("Commission")) {
					processCommissionsIssued(bankAccountCd);
				}
				
				// Extract the claims payments
				if(bankAccountCd.endsWith("Claims")) {
					// Create file writers for output
					//
					processLossPaymentsIssued(bankAccountCd);
				}

				// Extract the refunds
				if(bankAccountCd.endsWith("AccountBill") || bankAccountCd.endsWith("Refunds")) {
					// Create file writers for output
					processRefundAndSuspensePayments(bankAccountCd);
				}

				processVoided(bankAccountCd);
				processUnVoidUnStop(bankAccountCd);
				
			} catch (IBIZException ibe) {
				throw ibe;
			} catch (Exception e) {
				throw e;				  
			}
			finally {
				// Close the file writers. We are done
				
				if(dailyAPFileWriter != null){
					try { dailyAPFileWriter.close(); } catch (Exception e) { Log.error(e); } }
				if(dailyRccAPFileWriter != null){
					try { dailyRccAPFileWriter.close(); } catch (Exception e) { Log.error(e); } }
				if(dailyRicAPFileWriter != null){
					try { dailyRicAPFileWriter.close(); } catch (Exception e) { Log.error(e); } }
				if(dailyAPStopFileWriter != null){
					try { dailyAPStopFileWriter.close(); } catch (Exception e) { Log.error(e); } }
				if(dailyAPVoidFileWriter != null){
					try { dailyAPVoidFileWriter.close(); } catch (Exception e) { Log.error(e); } }
			}

			return rs;
		} catch (IBIZException ibe) {
			throw ibe;
		}
		catch (Exception e) {
			throw new ServiceHandlerException(e);
		}
	}
	
	/** Process external Commission payments issued
	 * @param bankAccountCd
	 * @throws Exception
	 */
	protected void processCommissionsIssued(String bankAccountCd) throws Exception {
		final String description = "External Payments Issued (Payments Issued out of Innovation, Detail Sent to Accounts Payable Module of General Ledger - Includes 1099 information)";
		final String refid = getReference();
		int rcount = 0;
		
		Log.debug(description);		
		
		try {
			// Allowed transaction codes
			String aCodes = ModelSpecification.indexString(AccountPayable.TRANS_PAYMENT);			
			// Create file writers for output
			
			
			// Query the External Payment table
			JDBCData.QueryResult[] results = queryData(bankAccountCd, aCodes);
            for( JDBCData.QueryResult result : results ) {            
				ModelBean externalPayment = new ModelBean("ExternalPayment");
				data.selectModelBean(externalPayment, result.getSystemId());				

				ModelBean apBean = convertExternalPaymentToApBean(externalPayment, APAccountType.COMMISSIONS, "250500");
				APTools.populateDescription(apBean, APFileWriter.COMMISSION_REFERENCE_ID+referenceDt);
				APTools.populateApBeanReferenceIds(apBean);
				++rcount;
				ModelBean payableDetail = Helper_Beans.findFirstBean(externalPayment, "PayableDetail", true);
				if(payableDetail.gets("CarrierCd").equalsIgnoreCase("RCC")){
					 dailyRccAPFileWriter = new APFileWriter(data, settings,APFileWriter.TRANS_TYPE_COMMISSION,getPaymentType(), "RCC", this.bookDt.toStringDisplay("MM-dd-yyyy").toString().concat("_"+DateRenderer.getTime("HH-mm-ss")),appendInd,reprocessInd);
					 dailyRccAPFileWriter.writeAPFile(apBean);
				}else{
					 dailyRicAPFileWriter = new APFileWriter(data, settings,APFileWriter.TRANS_TYPE_COMMISSION,getPaymentType(), "RIC", this.bookDt.toStringDisplay("MM-dd-yyyy").toString().concat("_"+DateRenderer.getTime("HH-mm-ss")),appendInd,reprocessInd);
					 dailyRicAPFileWriter.writeAPFile(apBean);
				}
				++rcount;
            }
            if(results.length > 0)
            {
            	extractCommissionRecord();
            }
		}
		catch (Exception e) {
			throw new Exception("Exception in " + description, e);
		}
	}

	
	private ModelBean convertExternalPaymentToApDetail(ModelBean externalPayment, String account,APAccountType accountType) throws Exception {
		ModelBean payableDetail = Helper_Beans.findFirstBean(externalPayment, "PayableDetail", true);
		ModelBean payableBatchRequest = externalPayment.getBean("PayableBatchRequest");

		String runDay = bookDt.toStringDisplay("dd");
		String runMonth = bookDt.toStringDisplay("MM");
		String runYear = bookDt.toStringDisplay("yyyy");

		ModelBean detail = new ModelBean("APSubLedgerDetail");
		detail.setValue("TagValue", "A");
		detail.setValue("TagLength", "1");
		detail.setValue("TagStartPosition", "1");
		detail.setValue("CalendarAccountingMonth", runMonth);
		detail.setValue("CalendarAccountingYear", runYear);
		String companyCd="2RCC";
		if(payableDetail.gets("CarrierCd").equalsIgnoreCase("RIC")){
			companyCd = "1RIC";
		}
		detail.setValue("CompanyCd", companyCd);
		detail.setValue("DebitCreditSignInd", "+");
		if(payableBatchRequest.gets("ItemAmt").contains("-")){
		detail.setValue("DebitCreditSignInd", "-");
		}
		detail.setValue("ConvertedAmount", StringUtils.leftPad(payableBatchRequest.gets("ItemAmt").replace(".", ""), 14, '0'));
		if(accountType== APAccountType.REFUNDS){
			detail.setValue("BusinessUnitCode1","UNDW");
		}else if(accountType== APAccountType.CLAIMS){
			detail.setValue("BusinessUnitCode1","CLMS");
		}else {
			detail.setValue("BusinessUnitCode1","MKTG");
		}
		detail.setValue("TransactionDay", runDay);
		detail.setValue("TransactionMonth", runMonth);
		detail.setValue("TransactionYear", runYear);
		
		detail.setValue("AccountNumber", account);
		return detail;
	}
	
	private void populateClaimSpecificFields(ModelBean externalPayment, ModelBean accountPayableBean) throws Exception {
		ModelBean payableDetail = Helper_Beans.findFirstBean(externalPayment, "PayableDetail", true);
		ModelBean payableBatchRequest = externalPayment.getBean("PayableBatchRequest");

		String claimNumber = payableDetail.gets("ClaimNumber");
		final ModelBean claim = Claim.getClaimByClaimNumber(this.data, claimNumber, false);
		final ModelBean claimPolicyInfo = claim.getBean("ClaimPolicyInfo");
		final ModelBean claimPartyInfo = claimPolicyInfo.getBeanByAlias("InsuredParty");
		final ModelBean claimNameInfo = claimPartyInfo.getBeanByAlias("InsuredName");

		String claimantTransactionId = payableDetail.gets("ClaimantTransactionIdRef");
		ModelBean transaction = claim.getBeanById(claimantTransactionId);
		ModelBean header = accountPayableBean.getBean("APSubLedgerHeader");
		
		//Override the ReportSort field with the ClaimTransaction user
		String updateTimestamp = payableBatchRequest.gets("UpdateTimestamp");
		int startDay = updateTimestamp.indexOf("/");
		int endDay = updateTimestamp.indexOf("/", startDay+1);
		String updateDay = updateTimestamp.substring(startDay+1, endDay);
		
		String referenceId = externalPayment.gets("SystemID");
		accountPayableBean.setValue("ReferenceId", referenceId);
		
		String providerCd = "";
		String providerRef = payableBatchRequest.gets("ProviderRef");
		if( providerRef.isEmpty()) {
			providerRef = transaction.gets("ProviderRef");
		}
		
		if(!providerRef.equals("")) {
			ModelBean provider = Provider.getProviderBySystemId(data, Integer.parseInt(providerRef));	
			providerCd = provider.gets("ProviderNumber");
			
		}
		
		if(transaction.gets("PayToProviderInd").equalsIgnoreCase("Yes")&& transaction.gets("PayToClaimantInd").isEmpty()){
			header.setValue("PayeeCd", providerCd);
		}
		List<String> stubLines = new ArrayList<String>();
		String paymentType = transaction.gets("PaymentTypeCd");
		accountPayableBean.setValue("SourceCd", SourceCode.CLAIMS);
		
		stubLines = APTools.createIndemnityStubUnlimited(claim, transaction);
		List<ModelBean> unlimitedBeans = convertExternalPaymentToApUnlimited(externalPayment, this.bookDt, stubLines);
		for(ModelBean unlimited : unlimitedBeans) {
			accountPayableBean.addValue(unlimited);
		}

		String insuredName = payableBatchRequest.gets("PayToName");

		String providerCode = "";
		String providerTypeCd = "";
		
		if(!providerRef.isEmpty()) {
			ModelBean providerBean = Provider.getProviderBySystemId(data, Integer.parseInt(providerRef));
			providerCode = providerBean.gets("ProviderNumber"); 
			providerTypeCd = providerBean.gets("ProviderTypeCd");
		}

		APTools.populateHeaderClaimCheckDescription(header, claimNumber, insuredName, providerCode);
		
		// IRS Box
		
		if(transaction != null) {
				ModelBean taxParty = payableBatchRequest.getBeanByAlias("MailingParty");
				ModelBean taxInfo = taxParty.getBean("TaxInfo");
			
				if(StringTools.isTrue(taxInfo.gets("Required1099Ind")) && transaction.gets("ProviderRef") != null) {
					if(paymentType.equalsIgnoreCase("Indemnity")){
						if(providerTypeCd.equalsIgnoreCase("Medical")){
							header.setValue("IRSBoxCd", "1060");
						}else if(providerTypeCd.equalsIgnoreCase("Legal") || providerTypeCd.equalsIgnoreCase("Outside Legal")){
							header.setValue("IRSBoxCd", "1140");
						}else{
							header.setValue("IRSBoxCd", "1070");
						}
					}else {
						header.setValue("IRSBoxCd", "1070");
					}
					header.setValue("IRSForm", "1099MISC");
				}
				
			
		}
	}

	private void populateCommissionSpecificFields(ModelBean externalPayment, ModelBean accountPayableBean, StringDate runDt) throws Exception {
		ModelBean header = accountPayableBean.getBean("APSubLedgerHeader");
		APTools.populateHeaderIRSBox(header);
		
		ModelBean payableBatchRequest = externalPayment.getBean("PayableBatchRequest");
		String providerRef = payableBatchRequest.gets("ProviderRef");
		String providerCd = "";
		
		if(!providerRef.equals("")) {
			ModelBean provider = Provider.getProviderBySystemId(data, Integer.parseInt(providerRef));	
			providerCd = provider.gets("ProviderNumber");
		}
 
		APTools.populateHeaderCommissionCheckDescription(header, runDt, providerCd);
			
		header.setValue("PayeeCd", providerCd );
		

		String referenceId = externalPayment.gets("SystemID");
		accountPayableBean.setValue("ReferenceId",referenceId);
		accountPayableBean.setValue("SourceCd", SourceCode.COMM);
	}

	private void populateRefundSpecificFields(ModelBean externalPayment, ModelBean accountPayableBean) throws Exception {
		ModelBean payableDetail = Helper_Beans.findFirstBean(externalPayment, "PayableDetail", true);
		ModelBean payableBatchRequest = externalPayment.getBean("PayableBatchRequest");
		ModelBean payableBatchRequestAllocation = payableBatchRequest.getBean("PayableBatchRequestAllocation");
		
		ModelBean header = accountPayableBean.getBean("APSubLedgerHeader");
		String arTransId = payableDetail.gets("AccountTransIdRef");
		
		header.setValue("StubNotes1", payableBatchRequest.gets("PayToName"));
		header.setValue("StubNotes2", payableDetail.gets("AccountNumber"));
		String memo = StringTools.trim(payableBatchRequestAllocation.gets("Memo").replaceAll("\\r?\\n", " "), 60);
		//use the ReasonDesc on ARTrans if it exists
		try {
			//Get the AccountId(eg: 111) from the SourceRef(eg: ACCOUNT::111::ARTrans-xxxxx-xxxxxx)
			int accountId = getSystemIdFromSourceRef(payableBatchRequestAllocation.gets("SourceRef"));
			ModelBean account = AccountRenderer.getAccount(accountId);
			ModelBean arTrans = account.getBeanById(arTransId);
			String reasonDesc = arTrans.gets("ReasonDesc");
			if(!reasonDesc.equals("")) {
				memo = reasonDesc;
			}
		} catch (Exception e) {
			Log.debug(e);
		}
			
		
		header.setValue("StubNotes3", memo);
		header.setValue("StubNotes4", payableDetail.gets("AccountNumber"));
		
		String referenceId = externalPayment.gets("SystemID");
		accountPayableBean.setValue("ReferenceId", referenceId);
		accountPayableBean.setValue("SourceCd", SourceCode.REFUND);
	}
	
	private ModelBean convertExternalPaymentToApBean(ModelBean externalPayment, APAccountType accountType, String account) throws Exception {
		ModelBean accountsPayableBean = new ModelBean("AccountsPayableEntry");

		ModelBean header = convertExternalPaymentToApHeader(externalPayment, accountType,this.bookDt);
		accountsPayableBean.addValue("APSubLedgerHeader", header);
		
		ModelBean detail = convertExternalPaymentToApDetail(externalPayment, account, accountType);
		accountsPayableBean.addValue("APSubLedgerDetail", detail);

		if(accountType == APAccountType.CLAIMS) {
			populateClaimSpecificFields(externalPayment, accountsPayableBean);
		} else if(accountType == APAccountType.COMMISSIONS) {
			populateCommissionSpecificFields(externalPayment, accountsPayableBean, this.bookDt);
		} else if(accountType == APAccountType.REFUNDS) {
			populateRefundSpecificFields(externalPayment, accountsPayableBean);
		}
		
		return accountsPayableBean;
	}
	
	private ModelBean convertExternalPaymentToApHeader(ModelBean externalPayment, APAccountType accountType,StringDate runDt) throws Exception {
		final ModelBean bean = new ModelBean("APSubLedgerHeader");
		GLOutputHandler glOutputHandler= new GLOutputHandler();
		ModelBean payableDetail = Helper_Beans.findFirstBean(externalPayment, "PayableDetail", true);
		ModelBean payableBatchRequest = externalPayment.getBean("PayableBatchRequest");
		ModelBean partyInfo = payableBatchRequest.getBean("PartyInfo");		
		ModelBean addrBean = partyInfo.getBeanByAlias("PayToMailingAddr");
		if(addrBean == null) {
			addrBean = partyInfo.getBeanByAlias("MailingAddr");
		}
		String CompanyCd="2RCC";
		if(payableDetail.gets("CarrierCd").equalsIgnoreCase("RIC")){
			CompanyCd = "1RIC";
		}
		APTools.populateHeaderDates(bean, this.bookDt);
		bean.setValue("RecordType", "1");
		bean.setValue("InvoiceAmt", String.format("%15s",payableBatchRequest.gets("ItemAmt").replace(".", "")));
		
		APTools.populateHeaderCheckWireInd(bean, payableBatchRequest.gets("PaymentMethodCd"));
		
		boolean isClaims = accountType == APAccountType.CLAIMS;

		if("ACH".equals(payableBatchRequest.gets("PaymentMethodCd"))) {
			
			ModelBean electronicPaymentDestination = payableBatchRequest.getBean("ElectronicPaymentDestination");
			APTools.populateHeaderEFTFields(
				bean,
				electronicPaymentDestination.gets("ACHBankAccountNumber"),
				electronicPaymentDestination.gets("ACHBankAccountTypeCd"),
				electronicPaymentDestination.gets("ACHRoutingNumber"));
			bean.setValue("AdvicePrint", "Y");
		}

		bean.setValue("CompanyCd", CompanyCd);
		bean.setValue("EntryOperator", "IN");
		bean.setValue("RemittanceAddress1", addrBean.gets("Addr1"));				
		bean.setValue("RemittanceAddress2", addrBean.gets("Addr2"));				
		bean.setValue("RemittanceCity", addrBean.gets("City"));
		bean.setValue("RemittanceState", addrBean.gets("StateProvCd"));
		bean.setValue("RemittanceZip", addrBean.gets("PostalCode"));
		bean.setValue("PayeeName1", payableBatchRequest.gets("PayToName"));
		String updateTimestamp = payableBatchRequest.gets("UpdateTimestamp");
		String timeStamp = updateTimestamp.toString().substring(updateTimestamp.indexOf(" ")+1,updateTimestamp.indexOf("."));
		timeStamp = timeStamp.replace(":", "-");
		if(accountType == APAccountType.REFUNDS){
			if("ACH".equals(payableBatchRequest.gets("PaymentMethodCd"))) {
				bean.setValue("HeaderDescription", "AP-"+payableDetail.gets("CarrierCd")+"Refund-"+payableBatchRequest.gets("PaymentMethodCd")+"-"+runDt.toStringDisplay("MM-dd-yyyy")+"_"+DateRenderer.getTime("HH-mm-ss"));
			}else{
				bean.setValue("HeaderDescription", "AP-"+payableDetail.gets("CarrierCd")+"Refund-"+payableBatchRequest.gets("PaymentMethodCd").substring(0,payableBatchRequest.gets("PaymentMethodCd").indexOf(" -"))+"-"+runDt.toStringDisplay("MM-dd-yyyy")+"_"+DateRenderer.getTime("HH-mm-ss"));
			}
		}else if(accountType == APAccountType.CLAIMS){
			if("ACH".equals(payableBatchRequest.gets("PaymentMethodCd"))) {
				bean.setValue("HeaderDescription", "AP-"+payableDetail.gets("CarrierCd")+"Claims-"+payableBatchRequest.gets("PaymentMethodCd")+"-"+runDt.toStringDisplay("MM-dd-yyyy")+"_"+DateRenderer.getTime("HH-mm-ss"));
			}else{
				bean.setValue("HeaderDescription", "AP-"+payableDetail.gets("CarrierCd")+"Claims-"+payableBatchRequest.gets("PaymentMethodCd").substring(0,payableBatchRequest.gets("PaymentMethodCd").indexOf(" -"))+"-"+runDt.toStringDisplay("MM-dd-yyyy")+"_"+DateRenderer.getTime("HH-mm-ss"));
			}
		}else{
			if("ACH".equals(payableBatchRequest.gets("PaymentMethodCd"))) {
				bean.setValue("HeaderDescription", "AP-"+payableDetail.gets("CarrierCd")+"Commissions-"+payableBatchRequest.gets("PaymentMethodCd")+"-"+runDt.toStringDisplay("MM-dd-yyyy")+"_"+DateRenderer.getTime("HH-mm-ss"));
			}else{
				bean.setValue("HeaderDescription", "AP-"+payableDetail.gets("CarrierCd")+"Commissions-"+payableBatchRequest.gets("PaymentMethodCd").substring(0,payableBatchRequest.gets("PaymentMethodCd").indexOf(" -"))+"-"+runDt.toStringDisplay("MM-dd-yyyy")+"_"+DateRenderer.getTime("HH-mm-ss"));
			}
		}
		
		//added for neg commissions
		BigDecimal amount =new BigDecimal(payableBatchRequest.gets("ItemAmt"));
		if (BigDecimals.Traits.isNegative(amount) && accountType == APAccountType.COMMISSIONS)
			bean.setValue("CreditMemo", "Y");
		
		
		
//		bean.setValue("PoNumber", isClaims ? payableDetail.gets("PolicyNumber") : "");
		
		ModelBean taxInfo = partyInfo.getBean("TaxInfo");
		APTools.populateHeaderTaxInfo(bean, taxInfo);

		return bean;
	}	
	
	/**
	 * Converts the ExternalPayment bean into a multiple APSubLedgerUnlimited beans, each of these
	 * contains a single line up to 60 characters with description
	 */
	private List<ModelBean> convertExternalPaymentToApUnlimited(ModelBean externalPayment, StringDate runDt, List<String> lines) throws Exception {
		ModelBean payableDetail = Helper_Beans.findFirstBean(externalPayment, "PayableDetail", true);
		ModelBean payableBatchRequest = externalPayment.getBean("PayableBatchRequest");
		
		List<ModelBean> beans = new ArrayList<ModelBean>();
		for(String desc : lines) {
			ModelBean unlimited = new ModelBean("APSubLedgerUnlimited");
			APTools.populateUnlimitedDates(unlimited, runDt);
			String CompanyCd="2RCC";
			if(payableDetail.gets("CarrierCd").equalsIgnoreCase("RIC")){
				CompanyCd = "1RIC";
			}
			unlimited.setValue("RecordType", "2");
			unlimited.setValue("InvoiceAmt", String.format("%15s",payableBatchRequest.gets("ItemAmt").replace(".", "")));
			unlimited.setValue("CompanyCd", CompanyCd);
			
			unlimited.setValue("Description", desc);
			beans.add(unlimited);
		}

		return beans;
	}

	/**
	 *
	 * <p><b>Tab:&nbsp;</b>AR and Suspended Cash</p>
	 * <p><b>Format:&nbsp;</b>Accounts Payable Invoice File</p>
	 * <p><b>Frequency:&nbsp;</b>Daily</p>
	 * @throws Exception 
	 */
		final String process = "Refund and Suspense Payments";
		
		protected void processRefundAndSuspensePayments(String bankAccountCd) throws Exception {
			final String refid = getReference();
	        int rcount = 0;
			Log.debug(process);
			Boolean rccAPFileWriter = false;
			Boolean ricAPFileWriter = false;
			
			try {
				// Allowed transaction codes
				String aCodes = ModelSpecification.indexString(AccountPayable.TRANS_PAYMENT);
				 
				
				// Create file writers for output
				
				
				List <ModelBean> apBeans= new ArrayList<ModelBean>();
				// Query the External Payment table
	            JDBCData.QueryResult[] result = queryData(bankAccountCd, aCodes);
	            for(JDBCData.QueryResult resut : result) {
					ModelBean externalPayment = new ModelBean("ExternalPayment");
					data.selectModelBean(externalPayment, resut.getSystemId());
					ModelBean payableDetail = Helper_Beans.findFirstBean(externalPayment, "PayableDetail", true);
					if(payableDetail.gets("CarrierCd").equalsIgnoreCase("RCC")&& rccAPFileWriter == false){
						dailyRccAPFileWriter = new APFileWriter(data, settings,APFileWriter.TRANS_TYPE_REFUND,getPaymentType(), "RCC", this.bookDt.toStringDisplay("MM-dd-yyyy").toString().concat("_"+DateRenderer.getTime("HH-mm-ss")),appendInd,reprocessInd);
						rccAPFileWriter = true;
					}else if (rccAPFileWriter == false){
						dailyRicAPFileWriter = new APFileWriter(data, settings,APFileWriter.TRANS_TYPE_REFUND,getPaymentType(), "RIC", this.bookDt.toStringDisplay("MM-dd-yyyy").toString().concat("_"+DateRenderer.getTime("HH-mm-ss")),appendInd,reprocessInd);
						ricAPFileWriter = true;
					}
	            }
	            
				for(JDBCData.QueryResult resut : result) {
					ModelBean externalPayment = new ModelBean("ExternalPayment");
					data.selectModelBean(externalPayment, resut.getSystemId());				

					ModelBean apBean = convertExternalPaymentToApBean(externalPayment, APAccountType.REFUNDS, "25200");

					APTools.populateApBeanReferenceIds(apBean);
					APTools.populateDescription(apBean, APFileWriter.REFUND_REFERENCE_ID+referenceDt);
					
					apBeans.add(apBean);
					ModelBean payableDetail = Helper_Beans.findFirstBean(externalPayment, "PayableDetail", true);
					if(payableDetail.gets("CarrierCd").equalsIgnoreCase("RCC")){
						dailyRccAPFileWriter.writeAPFile(apBean);
					}else{
						dailyRicAPFileWriter.writeAPFile(apBean);
					}
					++rcount;
	            }
			}
			
			catch (Exception e) {

				throw new Exception("Exception in " + process, e);
			}
		}

	protected void processLossPaymentsIssued(String bankAccountCd) throws Exception{
		final String process = "Loss Payment Issued ";
		final String refid = getReference();
		Boolean rccAPFileWriter = false;
		Boolean ricAPFileWriter = false;
		log.trace(process);

        int rcount = 0;

		Log.debug(process);
		
	
		try {
			// Allowed transaction codes
			String aCodes = ModelSpecification.indexString(AccountPayable.TRANS_PAYMENT);
			
			
			
			// Query the External Payment table
            JDBCData.QueryResult[] results = queryData(bankAccountCd, aCodes);
            for(JDBCData.QueryResult result : results) {
				ModelBean externalPayment = new ModelBean("ExternalPayment");
				data.selectModelBean(externalPayment, result.getSystemId());
				ModelBean payableDetail = Helper_Beans.findFirstBean(externalPayment, "PayableDetail", true);
				if(payableDetail.gets("CarrierCd").equalsIgnoreCase("RCC") && rccAPFileWriter == false){
					dailyRccAPFileWriter = new APFileWriter(data, settings,APFileWriter.TRANS_TYPE_CLAIMS,getPaymentType(), "RCC", this.bookDt.toStringDisplay("MM-dd-yyyy").toString().concat("_"+DateRenderer.getTime("HH-mm-ss")),appendInd,reprocessInd);
					rccAPFileWriter = true;
				}else if (ricAPFileWriter == false){
					dailyRicAPFileWriter = new APFileWriter(data, settings,APFileWriter.TRANS_TYPE_CLAIMS,getPaymentType(), "RIC", this.bookDt.toStringDisplay("MM-dd-yyyy").toString().concat("_"+DateRenderer.getTime("HH-mm-ss")),appendInd,reprocessInd);
					ricAPFileWriter= true;
				}
            }
			for(JDBCData.QueryResult result : results) {
				ModelBean externalPayment = new ModelBean("ExternalPayment");
				data.selectModelBean(externalPayment, result.getSystemId());				

				ModelBean apBean = convertExternalPaymentToApBean(externalPayment, APAccountType.CLAIMS, "25200");
				APTools.populateApBeanReferenceIds(apBean);
				
				ModelBean payableBatchRequest = externalPayment.getBean("PayableBatchRequest");
				ModelBean payableBatchRequestAllocation = payableBatchRequest.getBean("PayableBatchRequestAllocation");
				String classificationCd = payableBatchRequestAllocation.gets("ClassificationCd");
				if(StringTools.in(classificationCd, "Indemnity,Salvage,Subrogation")) {
					APTools.populateDescription(apBean, APFileWriter.CLAIMS_REFERENCE_ID+referenceDt);
				} else {
					APTools.populateDescription(apBean, APFileWriter.CLAIMS_REFERENCE_ID+referenceDt);
				}
				
				ModelBean payableDetail = Helper_Beans.findFirstBean(externalPayment, "PayableDetail", true);
				if(payableDetail.gets("CarrierCd").equalsIgnoreCase("RCC")){
					dailyRccAPFileWriter.writeAPFile(apBean);
				}else{
					dailyRicAPFileWriter.writeAPFile(apBean);
				}

				++rcount;
			}
		}
		
		catch (Exception e) {

			throw new Exception("Exception in " + process, e);
		}
	}
	
	private static String findBankAccount(JDBCData data, String paymentAccountCd) throws Exception {
		ModelBean result = new com.iscs.payables.account.BankAccount().getAccount(data, paymentAccountCd);
		return result.gets("AccountNumber");
	}
	
	private ModelBean convertExternalPaymentToApVoid(ModelBean externalPayment) throws Exception {
		ModelBean payableDetail = Helper_Beans.findFirstBean(externalPayment, "PayableDetail", true);
		ModelBean payableBatchRequest = externalPayment.getBean("PayableBatchRequest");
		final String paymentAccountCd = payableBatchRequest.gets("PaymentAccountCd");
		final String sourceRef = payableBatchRequest.getBean("PayableBatchRequestAllocation").gets("SourceRef");
		String itemNumber = null;
		
		// @formatter:off
		final String sql = SQL.statement(
			SQL.select("ItemNumber"),
			SQL.from("PayableStats"),
			SQL.where(
				
					SQL.equals("SourceRef", SQL.squote(sourceRef))
				)
				);
		// @formatter:on


		final ResultSet rs = execute(sql);
		if (rs != null) {
			while (rs.next()) {
				itemNumber = Strings.unnulled(rs.getString("ItemNumber"));
			}
		}
		
		String companyCd = null;
		
		// Some external payments do not have the payment detail bean so we derive the company code from payment account
		if(payableDetail != null) {
			companyCd = "2RCC";
			if(payableDetail.gets("CarrierCd") == "" || payableDetail.gets("CarrierCd")== null){
				companyCd = "1RIC";
			} else if(payableDetail.gets("CarrierCd").equalsIgnoreCase("RIC")){
				companyCd = "1RIC";
			} else {
				throw new IllegalStateException(String.format("No mapping to company code for account code [%s]", paymentAccountCd));
			}
			
		} 
		
		if(itemNumber==null)
		 itemNumber = payableBatchRequest.gets("ItemNumber");
		
		final String amount = payableBatchRequest.gets("ItemAmt");

		final ModelBean bean = new ModelBean("APSubLedgerStopVoid");
		bean.setValue("Company", companyCd);
		bean.setValue("CheckNumber",  StringUtils.leftPad(itemNumber, 11, '0'));
		bean.setValue("CheckAmt", amount.replace(".", ""));
		bean.setValue("VoidOrStopDt", bookDt.toString());
		bean.setValue("SystemId", externalPayment.gets("SystemId"));

		final String accountNumber = findBankAccount(this.data, paymentAccountCd);
		bean.setValue("BankAccount", accountNumber);
		
		return bean;
	}
	
	
	private void processVoided(String bankAccountCd) throws Exception{
		final String process = "Loss Payment Issued ";
		final String refid = getReference();
		String carrierCd = null;
		log.trace(process);
		Boolean stopFileCreated = false;
		Boolean voidFileCreated = false;
		
        int rcount = 0;

		Log.debug(process);
		

		try {
			// Allowed transaction codes
			String aCodes = String.format("%s,%s", 
				ModelSpecification.indexString(AccountPayable.TRANS_VOID), 
				ModelSpecification.indexString(AccountPayable.TRANS_STOP));	
			
			// Query the External Payment table
            JDBCData.QueryResult[] results = queryData(bankAccountCd, aCodes);
            for(JDBCData.QueryResult result : results) {
				ModelBean externalPayment = new ModelBean("ExternalPayment");
				data.selectModelBean(externalPayment, result.getSystemId());
				if(externalPayment.gets("TransactionCd").equalsIgnoreCase("Stop Payment") && stopFileCreated == false){
					dailyAPStopFileWriter = new APFileWriter(data, settings,"Stop Payment",this.bookDt.toStringDisplay("MM-dd-yyyy").toString().concat("_"+DateRenderer.getTime("HH-mm-ss")));
					stopFileCreated = true;
				}else if( externalPayment.gets("TransactionCd").equalsIgnoreCase("Void Payment") && voidFileCreated == false){
					dailyAPVoidFileWriter = new APFileWriter(data, settings,"Void Payment",this.bookDt.toStringDisplay("MM-dd-yyyy").toString().concat("_"+DateRenderer.getTime("HH-mm-ss")));
					voidFileCreated = true;
				}
            }
			for(JDBCData.QueryResult result : results) {
				ModelBean externalPayment = new ModelBean("ExternalPayment");
				data.selectModelBean(externalPayment, result.getSystemId());
				ModelBean payableDetail = Helper_Beans.findFirstBean(externalPayment, "PayableDetail", true);
				if(payableDetail != null) {
					carrierCd = "2RCC";
					if(payableDetail.gets("CarrierCd").equalsIgnoreCase("RIC")){
						carrierCd = "1RIC";
					} 
				}
				ModelBean voidBean = convertExternalPaymentToApVoid(externalPayment);
				if(externalPayment.gets("TransactionCd").equalsIgnoreCase("Stop Payment")){
					dailyAPStopFileWriter.writeStopFile(voidBean);
				}else{
					dailyAPVoidFileWriter.writeVoidFile(voidBean);
				}
				++rcount;
			}
			if(rcount >0 ){
			}
		} catch (Exception e) {
			throw new Exception("Exception in " + process, e);
		}
	}
	
	private void processUnVoidUnStop(String bankAccountCd) throws Exception{
		final String process = "UnVoid and UnStop Payments ";
		final String refid = getReference();
		String carrierCd = null;
		log.trace(process);
        int rcount = 0;

		Log.debug(process);
		try {
			// Allowed transaction codes
			String aCodes = String.format("%s,%s", 
				ModelSpecification.indexString(AccountPayable.TRANS_UNVOID), 
				ModelSpecification.indexString(AccountPayable.TRANS_UNSTOP));
			dailyAPFileWriter = new APFileWriter(data, settings,this.bookDt.toStringDisplay("MM-dd-yyyy").toString().concat("_"+DateRenderer.getTime("HH-mm-ss")));
			// Query the External Payment table
            JDBCData.QueryResult[] results = queryData(bankAccountCd, aCodes);
			for(JDBCData.QueryResult result : results) {
				ModelBean externalPayment = new ModelBean("ExternalPayment");
				data.selectModelBean(externalPayment, result.getSystemId());
				ModelBean payableDetail = Helper_Beans.findFirstBean(externalPayment, "PayableDetail", true);
				if(payableDetail != null) {
					carrierCd = "2RCC";
					if(payableDetail.gets("CarrierCd").equalsIgnoreCase("RIC")){
						carrierCd = "1RIC";
					} 
				}
				ModelBean voidBean = convertExternalPaymentToApVoid(externalPayment);
				dailyAPFileWriter.writeUnStopFile(voidBean);
				++rcount;
			}
			if(rcount >0 ){
			}
			
		} catch (Exception e) {
			throw new Exception("Exception in " + process, e);
		}
	}

	
	/*private String writeReferenceRecordCount(String reference, int count) {
		return writeReference(reference, String.format("Record Count: %d", count));
	}*/
	
	protected JDBCData.QueryResult[] queryData(String bankAccountCd, String aCodes)
	throws Exception {
	
		// Query the ExternalPayment table
        JDBCLookup lookup = new JDBCLookup("ExternalPaymentLookup");
        lookup.addLookupKey("PaymentAccountCd", bankAccountCd, JDBCLookup.LOOKUP_EQUALS);
        // Add the Date and Time if this is to reprocess and rebuild files
        if (reprocessDt != null && reprocessTm != null) {
            lookup.addLookupKey("ExternalPaymentStatusCd", AccountPayable.STATUS_PROCESSED, JDBCLookup.LOOKUP_EQUALS);
            lookup.addLookupKey("CreateDt", reprocessDt.toString(), JDBCLookup.LOOKUP_EQUALS);
            lookup.addLookupKey("CreateTm", ModelSpecification.indexString(reprocessTm), JDBCLookup.LOOKUP_EQUALS);
        } else {
            lookup.addLookupKey("ExternalPaymentStatusCd", AccountPayable.STATUS_OPEN, JDBCLookup.LOOKUP_EQUALS);
        }
        LookupKey lookupKey = lookup.addDelimitedListLookupKey("TransactionCd", aCodes, ",");
        lookupKey.setLookupType(JDBCLookup.LOOKUP_IN_LIST);
        
        return lookup.doLookup(data, 0);
	}
	
	protected void populateBeanVoidStop(ModelBean payableBatchRequest, ModelBean bean, String bankAccountCd) 
	throws Exception {
		
		final ModelBean bankAccount = new BankAccount().getAccount(data, bankAccountCd);
		final String accountNumber = (bankAccount != null) ? bankAccount.gets("AccountNumber") : "";
		final BigDecimal amount = scaled(new BigDecimal(payableBatchRequest.gets("ItemAmt")), ARITHMETIC_SCALE);
		
		bean.setValue("CheckAmt", amount.toString().replaceAll("\\.", ""));
		bean.setValue("VoidOrStopDt", bookDt);
		bean.setValue("BankAccount", accountNumber);
		
	}
	
	protected ResultSet execute(final String sql) throws SQLException {
		return this.data.doSQL(sql);
	}
	
	private final Logger log = LogManager.getLogger(GLGeneralLedgerProcessor.class);
	
	
	private int getPaymentType() throws Exception{
		
		String paymentType;
		
		ModelBean xfdf = rq.getBean("RequestParams").getBean("XFDF");
		ModelBean param = xfdf.findBeanByFieldValue("Param", "Name", "PaymentType");
		
		if(param != null){
			paymentType =  param.gets("Value");
		} else{
			ModelBean additionalParams = rq.getBean("RequestParams").getBean("AdditionalParams");
			param = additionalParams.findBeanByFieldValue("Param", "Name", "PaymentType");
			if(param != null){
				paymentType =  param.gets("Value");
			} else throw new Exception("No PaymentType param indicated for AP filename.");
		}

		if(paymentType.equals("Check")){
			return APFileWriter.PAYMENT_TYPE_BATCHCHECK;
		} else if(paymentType.equals("ACH")){
			return APFileWriter.PAYMENT_TYPE_ACH;
		} else {
			throw new Exception("No PaymentType: " + paymentType + " is not valid." );
		}
	}
	
	/**
	 *
	 * @param sourceRef
	 * @param value
	 * @return
	 */
	private static int getAcctFromSourceRef(final String sourceRef, final String value) {
		int result = 0;

		if (!(_isNullOrEmpty(sourceRef) || _isNullOrEmpty(value))) {
			final StringTokenizer stringTokenizer = new StringTokenizer(sourceRef, value);

			if (stringTokenizer.hasMoreTokens())
				stringTokenizer.nextToken();

			if (stringTokenizer.hasMoreTokens())
				result = Integer.parseInt(stringTokenizer.nextToken());
		}

		return result;
	}
	
	/**
	 * Get the systemId from sourceRef
	 * @param sourceRef
	 * @return
	 */
	public static int getSystemIdFromSourceRef(String sourceRef ){
		return getAcctFromSourceRef(sourceRef, "::");
	}
	private static boolean _isNullOrEmpty(final String text) {
		return (text == null || text.isEmpty());
	}
	
	protected void extractCommissionRecord() throws Exception {
		try {
			String directoryPath = DynamicString.render(new ModelBean("Company"),InnovationUtils.getEnvironmentVariable("CommissionReport", "dailyApCommissionFilePath", ""));
	   		String fullPathName = "";
	   		StringDate runStringDt = DateRenderer.getStringDate();
	   		String runDt = DateRenderer.getDate((runStringDt),"yyyy-MM-dd");
			String runTime = DateRenderer.getTime("HH-mm-ss");
			String bkDate = this.bookDt.toStringDisplay("MM-dd-yyyy").toString();
			fullPathName = directoryPath + '_' + bkDate+'_'+runTime+ ".csv";
			File saveFile = new java.io.File(fullPathName);
			StringBuilder sqlsb = new StringBuilder();
	        sqlsb.append("SELECT CarrierCd, PaytoCd, SUM(CommissionAmt) FROM CommissionDetail where PaymentRequestedDt = '"+runDt+"' group by CarrierCd,PaytoCd");
	        ResultSet rs = this.data.doSQL(sqlsb.toString());
			
			StringBuilder sb = new StringBuilder();
			sb.append("CarrierCd");sb.append(","); 
			sb.append("PayToCd");sb.append(","); 
			sb.append("CommissionAmt"); 
			sb.append(System.getProperty("line.separator")); 
			if (rs != null) {
				while (rs.next()) {
					sb.append(convertToCSV(rs.getString("CarrierCd")));sb.append(",");
					sb.append(convertToCSV(rs.getString("payToCd")));sb.append(",");
					sb.append(convertToCSV(StringRenderer.addMoney(rs.getString("SUM(CommissionAmt)"),"")));
					sb.append(System.getProperty("line.separator")); 
				}
			}
			Charset charset = Charset.forName("UTF-8"); 
    		CharsetEncoder encoder = charset.newEncoder(); 
    		FileOutputStream fos = new FileOutputStream(saveFile,true);
    		FileChannel channel = fos.getChannel();
    		FileLock lock = null;
    		int count = 0;
    		try {
    			while ((lock = performFileLock(channel)) == null && count<60)  {
    				Log.debug("File locked. Retrying in 100ms ");
    				Thread.sleep(100);
    				count++;
    			}    
    				channel.write(encoder.encode(CharBuffer.wrap(sb.toString().toCharArray())));
    		} 
    		catch(Exception ex){
    			Log.debug("File locked. Retrying in 100ms " +ex );
    			throw(ex);
    		} 
    		finally {
    			if (lock !=null)
    				lock.release();
    			if (channel!=null)
    				channel.close();
    			if (fos!=null)
    				fos.close();
    		}
	}catch( IBIZException e ) {
        throw e;
    } catch( Exception e ) {
		throw new ServiceHandlerException(e);
	}
			
	}

	 private String convertToCSV(String field) {
	    	StringBuilder buff = new StringBuilder();
	    	if(field !=null){
		    	if( field.indexOf(",") > 0){
					buff.append('"');
					buff.append(field);
					buff.append('"');
				}else{
					buff.append(field);
				}
	    	}
	    	return buff.toString();
	    }
	 
	    private FileLock performFileLock(FileChannel channel) {
	    	FileLock lock = null;
	    	try{
	    		lock = channel.tryLock();
	    	} catch(Exception ex){
	    		Log.error("Failed to lock DTOExport file...wait" +ex );
	    		return null;
	    	}
	    	return lock;
	    }  
	
}
