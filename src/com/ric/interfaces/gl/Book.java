package com.ric.interfaces.gl;


import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
/**
 * Abstraction of a book of accounts. The book contains two distributions The
 * first distribution contains the debit accounts and values. The second
 * distribution contains the credit accounts and values
 * 
 * @author peter.hose
 *
 */
public class Book implements Map<String, Distribution> {

	public Book() {
		put(DISTRIBUTION_DEBITS, new Distribution());
		put(DISTRIBUTION_CREDITS, new Distribution());
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public Book add(Book rhs) {
		for (String key : rhs.keySet()) {
			add(key, rhs.get(key));
		}

		return this;
	}

	public Distribution debits() {
		return get(DISTRIBUTION_DEBITS);
	}

	public Distribution credits() {
		return get(DISTRIBUTION_CREDITS);
	}

	public void clear() {
		get(DISTRIBUTION_DEBITS).clear();
		get(DISTRIBUTION_CREDITS).clear();
	}

	public Book add(String key, Distribution value) {
		if (containsKey(key))
			put(key, get(key).add(value));
		else
			put(key, value);

		return this;
	}

	private String _dcheck(String account) throws Exception {
		if (account == null || account.isEmpty())
			throw new BookException("Debit account is null or empty");

		return account;
	}

	private BigDecimal _dvcheck(BigDecimal value) throws BookException {
		if (value == null)
			throw new BookException("Debit value is null");

		return value;
	}

	private String _ccheck(String account) throws BookException {
		if (account == null || account.isEmpty())
			throw new BookException("Credit account is null or empty");
		return account;
	}

	private BigDecimal _cvcheck(BigDecimal value) throws BookException {
		if (value == null)
			throw new BookException("Credit value is null");
		
		return value;
	}

	public void debit(String account, BigDecimal value) throws BookException,Exception {
		get(DISTRIBUTION_DEBITS).distribute(_dcheck(account), _dvcheck(value));
	}

	public void credit(String account, BigDecimal value) throws BookException {
		get(DISTRIBUTION_CREDITS).distribute(_ccheck(account), _cvcheck(value));
	}

	public void debit(String account, BigDecimal value, boolean reverse) throws Exception {
		if (!reverse)
			debit(account, value);
		else
			credit(account, value);
	}

	public void credit(String account, BigDecimal value, boolean reverse) throws Exception {
		if (!reverse)
			credit(account, value);
		else
			debit(account, value);
	}

	/**
	 * Make an entry into the book. An entering the amount will distribute it to
	 * the appropriate accounts in the debit and credit distributions
	 * 
	 * @param debit the account that will be debited
	 * @param credit the account that will be credited
	 * @param value the amount of the entry. This value should be stored as an
	 *            absolute value.
	 * @throws BookException
	 */
	public void enter(String debit, String credit, BigDecimal value) throws Exception {
		get(DISTRIBUTION_DEBITS).distribute(_dcheck(debit), _dvcheck(value));
		get(DISTRIBUTION_CREDITS).distribute(_ccheck(credit), _cvcheck(value));
	}

	/**
	 * Make an entry into the book. This method is used when debiting and
	 * crediting the amount to different accounts.
	 * 
	 * @param debit the account that will be debited
	 * @param credit the account that will be credited
	 * @param value the amount of the entry. This value should be stored as an
	 * absolute value.
	 * @param reverse a flag that indicates that the debit and credit accounts should
	 * be reversed.  This is done when the amount sign of the original amount is the
	 * reverse of the expected value
	 * @throws BookException
	 */
	public void enter(String debit, String credit, BigDecimal value, boolean reverse) throws Exception {
		if (!reverse)
			enter(debit, credit, value);
		else
			enter(credit, debit, value);
	}

	/**
	 * Make an entry into the book. This method is used when debiting and
	 * crediting the amount to the same account.
	 * 
	 * @param debit_credit the account that will be both debited and credited
	 * @param value the amount of the entry. This value should be stored as an
	 *            absolute value
	 * @throws BookException
	 */
	public void enter(String debit_credit, BigDecimal value) throws Exception {
		enter(debit_credit, debit_credit, value);
	}

	/**
	 * Remove zero-sum values for matching keys
	 */
	public void offsets() {
		final Distribution debits = debits();
		final Distribution credits = credits();
		final Set<String> keys = new HashSet<String>();

		keys.addAll(debits.keySet());
		keys.addAll(credits.keySet());

		for (String _key : keys) {
			if (debits.containsKey(_key) && credits.containsKey(_key)) {
				if (debits.get(_key).compareTo(credits.get(_key)) == 0) {
					debits.remove(_key);
					credits.remove(_key);
				}
			}
		}
	}

	public void reduce() {
		final Distribution debits = debits();
		final Distribution credits = credits();
		final Set<String> keys = new HashSet<String>();

		keys.addAll(debits.keySet());
		keys.addAll(credits.keySet());

		for (String _key : keys) {
			if (debits.containsKey(_key) && credits.containsKey(_key)) {
				final BigDecimal debit = debits.get(_key);
				final BigDecimal credit = credits.get(_key);

				switch (debit.compareTo(credit))
				{
				case -1:
					debits.remove(_key);
					credits.put(_key, credit.subtract(debit));
					break;

				case 0:
					debits.remove(_key);
					credits.remove(_key);
					break;

				case +1:
					debits.put(_key, debit.subtract(credit));
					credits.remove(_key);
					break;
				}
			}
		}
	}

	public boolean balanced() {
		return (debits().sum().compareTo(credits().sum()) == 0);
	}

	private Set<String> _keyset() {
		final Set<String> keys = new HashSet<String>();

		keys.addAll(debits().keySet());
		keys.addAll(credits().keySet());

		return keys;
	}

	final public static String DISTRIBUTION_DEBITS = "Debits";
	final public static String DISTRIBUTION_CREDITS = "Credits";
	final public static String DISTRIBUTION_DEBIT = "Debit";
	final public static String DISTRIBUTION_CREDIT = "Credit";
	/**
	 * 
	 */
	private static final long serialVersionUID = 8033066622074466679L;

	protected Properties properties = new Properties();

	@Override
	public int size() {	
		return map.size();
	}

	@Override
	public boolean isEmpty() {	
		return map.isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return map.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return map.containsValue(value);
	}

	@Override
	public Distribution get(Object key) {	
		return map.get(key);
	}

	@Override
	public Distribution put(String key, Distribution value) {	
		return map.put(key, value);
	}

	@Override
	public Distribution remove(Object key) {	
		return map.remove(key);
	}

	@Override
	public void putAll(Map<? extends String, ? extends Distribution> m) {
		map.putAll(m);
	}

	@Override
	public Set<String> keySet() {		
		return map.keySet();
	}

	@Override
	public Collection<Distribution> values() {		
		return map.values();
	}

	@Override
	public Set<java.util.Map.Entry<String, Distribution>> entrySet() {
		return map.entrySet();
	}
	
	protected Map<String, Distribution> map = new HashMap<String, Distribution>();	
}
