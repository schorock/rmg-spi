package com.ric.interfaces.gl;

import java.util.HashMap;
import java.util.Map;

public class GLCompanyCd {

	public static final String NORTH_CAROLINA_INSURANCE_UNDERWRITING_ASSOCIATION = "CPIP";
	public static final String NORTH_CAROLINA_JOINT_UNDERWRITING_ASSOCIATION = "FAIR";

	public static class Index {
		public static final int UNKNOWN = 0;
		/**
		 * North Carolina Insurance Underwriting Association
		 */
		public static final int NORTH_CAROLINA_INSURANCE_UNDERWRITING_ASSOCIATION = 1;
		/**
		 * North Carolina Joint Underwriting Association
		 */
		public static final int NORTH_CAROLINA_JOINT_UNDERWRITING_ASSOCIATION  = 2;

		public static int get(final String text) {
			final Integer result = index.get(text);

			return (result == null) ? UNKNOWN : result.intValue();
		}

		private static final Map<String, Integer> index = new HashMap<String, Integer>();

		static {
			index.put(GLCompanyCd.NORTH_CAROLINA_INSURANCE_UNDERWRITING_ASSOCIATION, NORTH_CAROLINA_INSURANCE_UNDERWRITING_ASSOCIATION);
			index.put(GLCompanyCd.NORTH_CAROLINA_JOINT_UNDERWRITING_ASSOCIATION, NORTH_CAROLINA_JOINT_UNDERWRITING_ASSOCIATION);
		}
	}
}
