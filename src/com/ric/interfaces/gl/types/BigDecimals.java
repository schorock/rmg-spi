package com.ric.interfaces.gl.types;


import java.math.BigDecimal;
import java.math.BigInteger;

public final class BigDecimals {

	public static class Traits {
		public static final int ARITHMETIC_SCALE = 3;
		public static final int DISPLAY_SCALE = 2;

		public static final int ROUNDING_MODE = BigDecimal.ROUND_HALF_EVEN;

		public static final BigDecimal SCALED_ZERO = scaled(BigDecimal.ZERO, ARITHMETIC_SCALE);

		public static boolean isNegative(final BigDecimal value) {
			return (value.compareTo(SCALED_ZERO) < 0);
		}

		public boolean isPositive(final BigDecimal value) {
			return (value.compareTo(SCALED_ZERO) >= 0);
		}

		public static boolean isZero(final BigDecimal value) {
			return value.equals(SCALED_ZERO);
		}

		public boolean isNullOrZero(final BigDecimal value) {
			return (value == null || isZero(value));
		}
	}

	/**
	 * Safely set the scale of a BigDecimal
	 *
	 * @param value The value to scale
	 * @param scale The amount to scale
	 * @return The value passed in with scaling applied or null
	 */
	public static BigDecimal scaled(final BigDecimal value, final int scale) {
		return (value != null) ? value.setScale(scale) : value;
	}

	public static BigDecimal scaled(final BigDecimal value) {
		return scaled(value, Traits.ARITHMETIC_SCALE);
	}

	public static BigDecimal zeroed(final BigDecimal value, final int scale) {
		return (value != null) ? scaled(value, scale) : new BigDecimal(BigInteger.ZERO, scale);
	}

	public static BigDecimal zeroed(final BigDecimal value) {
		return zeroed(value, Traits.ARITHMETIC_SCALE);
	}
}
