package com.ric.interfaces.gl;


import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Implements the concept of a collection of named buckets for accumulators.
 * These buckets represent accounts within a book of accounts.
 *
 * @author peter.hose
 *
 */
public class Distribution implements Map<String, BigDecimal> {

	public BigDecimal getAmount(final String account) {
		BigDecimal result = BigDecimal.ZERO;

		if (containsKey(account))
			result = get(account);

		return result;
	}

	/**
	 * Add all the values of a distribution into this distribution.
	 *
	 * @param rhs A distribution containing the values to add
	 * @return this distribution
	 */
	public Distribution add(final Distribution rhs) {
		for (final String key : rhs.keySet()) {
			add(key, rhs.get(key));
		}

		return this;
	}

	/**
	 * Add a value to an account. If the account does not exist, it is added to
	 * the distribution.
	 *
	 * @param account
	 * @param value
	 */
	public Distribution add(final String key, final BigDecimal value) {
		if (containsKey(key))
			put(key, get(key).add(value));
		else
			put(key, value);

		return this;
	}

	/**
	 * A synonym for add, this method is more expressive as to the action of
	 * distributing values to accounts
	 *
	 * @param account
	 * @param value
	 * @return
	 */
	public Distribution distribute(final String account, final BigDecimal value) {
		return add(account, value);
	}

	/**
	 * Sum the values in the distribution
	 *
	 * @return the sum
	 */
	public BigDecimal sum() {
		BigDecimal sum = BigDecimal.ZERO;

		for (final BigDecimal value : values())
			sum = sum.add(value);

		return sum;
	}

	/**
	 * PERL like join method
	 *
	 * @param separator
	 * @param strings
	 * @return
	 */
	@Deprecated
	public static String join(final String separator, final String... strings) {
		final StringBuffer result = new StringBuffer();

		for (final String string : strings) {
			if (result.length() > 0)
				result.append(separator);

			result.append(string);
		}

		return result.toString();
	}

	@Deprecated
	public static final String SEPARATOR_DEFAULT = ":";

	protected Map<String, BigDecimal> map = new HashMap<String, BigDecimal>();

	@Override
	public int size() {
		return map.size();
	}

	@Override
	public boolean isEmpty() {
		return map.isEmpty();
	}

	@Override
	public boolean containsKey(final Object key) {
		return map.containsKey(key);
	}

	@Override
	public boolean containsValue(final Object value) {
		return map.containsValue(value);
	}

	@Override
	public BigDecimal get(final Object key) {
		return map.get(key);
	}

	@Override
	public BigDecimal put(final String key, final BigDecimal value) {
		return map.put(key, value);
	}

	@Override
	public BigDecimal remove(final Object key) {
		return map.remove(key);
	}

	@Override
	public void putAll(final Map<? extends String, ? extends BigDecimal> m) {
		map.putAll(m);
	}

	@Override
	public void clear() {
		map.clear();
	}

	@Override
	public Set<String> keySet() {
		return map.keySet();
	}

	@Override
	public Collection<BigDecimal> values() {
		return map.values();
	}

	@Override
	public Set<java.util.Map.Entry<String, BigDecimal>> entrySet() {
		return map.entrySet();
	}
}
