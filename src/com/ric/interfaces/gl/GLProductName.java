package com.ric.interfaces.gl;

import java.util.HashMap;
import java.util.Map;

/**
 * Constants and Indexing for Product Names.  The contents of this class
 * must match product names defined in the product masters.
 *
 * @author peter.hose
 *
 */
public class GLProductName {
	public static final String COMMERCIAL_WIND = "Commercial Wind";
	public static final String COMMERCIAL_FIRE = "Commercial Fire";
	public static final String CRIME = "Crime";
	public static final String DWELLING = "Dwelling";
	public static final String DWELLING_WIND = "Dwelling Wind";
	public static final String HOMEOWNERS = "Homeowners";
	public static final String HOMEOWNERS_WIND = "Homeowners Wind";

	static class Index {
		static final int UNKNOWN = 0;
		static final int COMMERCIAL_WIND = 1;
		static final int COMMERCIAL_FIRE = 2;
		static final int CRIME = 3;
		static final int DWELLING = 4;
		static final int DWELLING_WIND = 5;
		static final int HOMEOWNERS = 6;
		static final int HOMEOWNERS_WIND = 7;

		public static int get(final String text) {
			final Integer result = index.get(text);

			return (result == null) ? UNKNOWN : result.intValue();
		}

		private static final Map<String, Integer> index = new HashMap<String, Integer>();

		static {
			index.put(GLProductName.COMMERCIAL_WIND, COMMERCIAL_WIND);
			index.put(GLProductName.COMMERCIAL_FIRE, COMMERCIAL_FIRE);
			index.put(GLProductName.CRIME, CRIME);
			index.put(GLProductName.DWELLING, DWELLING);
			index.put(GLProductName.DWELLING_WIND, DWELLING_WIND);
			index.put(GLProductName.HOMEOWNERS, HOMEOWNERS);
			index.put(GLProductName.HOMEOWNERS_WIND, HOMEOWNERS_WIND);
		}
	}
}
