package com.ric.interfaces.gl;

import java.util.ArrayList;
import java.util.List;

/**
 * A series of function used for generating SQL strings wrapped in a class to give them a common name space
 *
 * @author peter.hose
 *
 */
public final class SQL {

	public static enum Variant {
		MSSQL,
		MYSQL
	}

	public static Variant variant = Variant.MSSQL;

	public static final int BUFFER_INITIAL_CAPACITY = 512;


	/** Wrap a string with matching quoting characters
	 * <p>
	 * For example, <code>quote("Bob", '?')</code> returns "?Bob?"
	 *
	 * @param s The string to quote
	 * @param q The quoting character
	 * @return The quoted string
	 *
	 */
	public static String quote(final String s, final char q) {
		return quote(s, q, q);
	}

	/** Wrap a string with non-matching quoting characters
	 * <p>
	 * For example, <code>quote("Bob", '?', '!')</code> returns "?Bob!"
	 *
	 * @param s The string to quote
	 * @param lhs The opening quoting character
	 * @param rhs The closing quoting character
	 * @return The quoted string
	 *
	 * @see #quote(String, char)
	 */
	public static String quote(final String s, final char lhs, final char rhs) {
		final StringBuilder sb = new StringBuilder(s.length() + 2);

		sb.append(lhs);
		sb.append(s);
		sb.append(rhs);

		return sb.toString();
	}

	/** Convenience function for single quoting a string, calls quote(s, '\'')
	 *
	 * @param s The string to quote
	 * @return The quoted string
	 */
	public static String squote(final String s) {
		return quote(s, '\'');
	}

	public static String[] squote(final String[] a) {
		final List<String> result = new ArrayList<String>();

		for (final String v : a) {
			result.add(squote(v));
		}

		return result.toArray(new String[result.size()]);
	}

	public static String[] squote(final List<String> a) {
		return squote(a.toArray(new String[a.size()]));
	}

	/** Convenience function for double quoting a string, calls quote(s, '"')
	 *
	 * @param s The string to quote
	 * @return The quoted string
	 */
	public static String dquote(final String s) {
		return quote(s, '"');
	}

	/** Convenience function wrapping a string in parenthesis, calls  quote(s, '(', ')')
	 *
	 * @param s The string to quote
	 * @return The quoted string
	 */
	public static String pquote(final String s) {
		return quote(s, '(', ')');
	}

	/**
	 * @param lhs expression
	 * @param op operator
	 * @param rhs expression
	 * @return <i>expression</i> <i>op</i> <i>expression</i>
	 */
	public static String expression(final String lhs, final String op, final String rhs) {
		return Perl.join(_sep(op), lhs, rhs);
	}

	/**
	 * @param op operator
	 * @param rhs list of expressions
	 * @return <i>expression</i> { op <i>expression</i> }
	 */
	public static String expressions(final String op, final String ... rhs) {
		return Perl.join(_sep(op), rhs);
	}

	@Deprecated
	public static String expressions(final ArrayList<String> list, final String op) {
		return Perl.join(_sep(op), list.toArray(new String[list.size()]));
	}

	/**
	 * @param rhs list of columns
	 * @return <i>column</i> { , <i>column</i> }
	 */
	public static String select_list(final String ... rhs) {
		return _csv(rhs);
	}

	/**
	 * @param rhs list of columns
	 * @return <b>SELECT</b> <i>column</i> { , <i>column</i> }
	 */
	public static String select(final String ... rhs) {
		return _ssv(Keyword.SELECT, select_list(rhs));
	}

	/**
	 * @param rhs list of tables
	 * @return <b>FROM</b> <i>table</i> { , <i>table</i> }
	 */
	public static String from(final String ... rhs) {
		return _ssv(Keyword.FROM, _csv(rhs));
	}

	public static String where(final String rhs) {
		return _ssv(Keyword.WHERE, rhs);
	}

	public static String group_by(final String ... rhs) {
		return _ssv("GROUP BY", _csv(rhs));
	}

	public static String union_all(final String ... rhs) {
		return expressions("UNION ALL", rhs);
	}

	public static String statement(final String ... rhs) {
		return _ssv(rhs);
	}

	/**
	 * @param rhs list of expressions
	 * @return "(" <i>expression</i> { <b>AND</b> <i>expression</i> } ")"
	 */
	public static String and(final String ... rhs) {
		return pquote(expressions(Operator.AND, rhs));
	}

	/**
	 * @param rhs list of expressions
	 * @return "(" <i>expression</i> { <b>OR</b> <i>expression</i> } ")"
	 */
	public static String or(final String ... rhs) {
		return pquote(expressions(Operator.OR, rhs));
	}

	/**
	 * @param lhs expression
	 * @param rhs expression
	 * @return <i>lhs</i> <b>=</b> <i>rhs</i>
	 */
	public static String equals(final String lhs, final String rhs) {
		return expression(lhs, Operator.EQUALS, rhs);
	}

	/**
	 * @param lhs expression
	 * @param rhs expression
	 * @return <i>lhs</i> <b><></b> <i>rhs</i>
	 */
	public static String notequals(final String lhs, final String rhs) {
		return expression(lhs, "<>", rhs);
	}

	/**
	 * @param lhs column
	 * @param rhs list of expressions
	 * @return <i>column</i> <b>IN</b> ( <i>expression</i> { , <i>expression</i> } )
	 */
	public static String in(final String lhs, final String ... rhs) {
		return expression(lhs, Operator.IN, pquote(_csv(rhs)));
	}

	/**
	 * @param lhs column
	 * @param rhs list of expressions
	 * @return <i>column</i> <b>NOT IN</b> ( <i>expression</i> { , <i>expression</i> } )
	 */
	public static String notin(final String lhs, final String ... rhs) {
		return expression(lhs, "NOT IN", pquote(_csv(rhs)));
	}

	public static String like(final String lhs, final String rhs) {
		return expression(lhs, Operator.LIKE, rhs);
	}

	public static String notlike(final String lhs, final String rhs) {
		return expression(lhs, "NOT LIKE", rhs);
	}

	public static String isnull(final String lhs) {
		return lhs + " IS NULL ";
	}

	public static String isnotnull(final String lhs) {
		return lhs + " IS NOT NULL ";
	}

	public static String between(final String lhs, final String lower, final String upper) {
		return and(expression(lhs, Operator.GE, lower), expression(lhs, Operator.LE,  upper));
	}

	public static String range(final String lhs, final String lower, final String upper) {
		return and(expression(lhs, Operator.GE, lower), expression(lhs, Operator.LE,  upper));
	}

	private static class Keyword {
		private static final String FROM = "FROM";
		private static final String WHERE = "WHERE";
		private static final String SELECT = "SELECT";
	}

	private static class Operator {
		private static final String AND = "AND";
		private static final String OR = "OR";
		private static final String NOT = "NOT";
		private static final String EQUALS = "=";
		private static final String IN = "IN";
		private static final String LIKE = "LIKE";
		private static final String GE = ">=";
		private static final String LE = "<=";
	}

	private static final String SP = " ";

	/**
	 * Create a value suitable for use as a separator in a join
	 *
	 * @param string text containing an operator or keyword
	 * @return a string quoted with a spaces
	 */
	private static String _sep(final String string) {
		return quote(string, ' ');
	}

	private static String _csv(final String ... rhs) {
		return Perl.join (", ", rhs);
	}

	private static String _ssv(final String ... rhs) {
		return Perl.join(SP, rhs);
	}
}
