package com.ric.interfaces.gl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class Books implements Map<String, Book> {

	public Books() {
		super();
	}

	public Books(final KeyDef keydef) {
		this.keydef = keydef;
	}

	public KeyDef getKeydef() {
		return keydef;
	}

	public void setKeydef(final KeyDef keydef) {
		this.keydef = keydef;
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(final Properties properties) {
		this.properties = properties;
	}

	@Override
	public int size() {
		return map.size();
	}

	@Override
	public boolean isEmpty() {
		return map.isEmpty();
	}

	@Override
	public boolean containsKey(final Object key) {
		return map.containsKey(key);
	}

	@Override
	public boolean containsValue(final Object value) {
		return map.containsValue(value);
	}

	@Override
	public Book get(final Object key) {
		return map.get(key);
	}

	@Override
	public Book put(final String key, final Book value) {
		return map.put(key, value);
	}

	@Override
	public Book remove(final Object key) {
		return map.remove(key);
	}

	@Override
	public void putAll(final Map<? extends String, ? extends Book> m) {
		map.putAll(m);
	}

	@Override
	public void clear() {
		map.clear();
	}

	@Override
	public Set<String> keySet() {
		return map.keySet();
	}

	@Override
	public Collection<Book> values() {
		return map.values();
	}

	@Override
	public Set<java.util.Map.Entry<String, Book>> entrySet() {
		return map.entrySet();
	}

	public Books add(final Books rhs) {
		for (final String key : rhs.keySet()) {
			add(key, rhs.get(key));
		}

		return this;
	}

	public Books add(final String key, final Book value) {
		if (containsKey(key))
			put(key, get(key).add(value));
		else
			put(key, value);

		return this;
	}

	public void offsets() {
		for (final Book book : values())
			book.offsets();
	}

	public void reduce() {
		for (final Book book : values())
			book.reduce();
	}

	public List<String> balanced() {
		final List<String> keys = new ArrayList<String>();

		for (final java.util.Map.Entry<String, Book> entry : entrySet()) {
			final String key = entry.getKey();
			final Book book = entry.getValue();

			if (!book.balanced())
				keys.add(key);
		}

		return keys;
	}

	/**
	 * Get an instance of a Book using a specific key. If the book does not
	 * exist, it is created, and the key is parsed according the <b>KeyDef</b>
	 * object associated with the Books object.<br>
	 * <br>
	 * This method will always return and object<br>
	 *
	 * @param key is a unique identifier for the Book
	 * @return an instance of a Book, either existing or newly created
	 */
	public Book getBook(final String key) {
		Book book = null;

		if (containsKey(key))
			book = get(key);
		else {
			book = newBook();

			final Properties pbook = book.getProperties();

			for (final java.util.Map.Entry<String, String> entry : Perl.map(keydef.join(), key).entrySet())
				pbook.setProperty(entry.getKey(), entry.getValue());

			put(key, book);
		}

		return book;
	}

	public Book getBook(final Key key) {
		return getBook(key.join());
	}
	
	@Deprecated
	public Book getAlways(final String key) {
		return getBook(key);
	}

	public Book newBook() {
		return new Book();
	}

	protected Properties properties = new Properties();
	protected KeyDef keydef = new KeyDef();
	protected Map<String, Book> map = new HashMap<String, Book>();
}
