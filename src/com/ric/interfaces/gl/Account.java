package com.ric.interfaces.gl;

import java.math.BigDecimal;

public class Account {

	public static final BigDecimal ZERO = BigDecimal.ZERO.setScale(2);

	public Account() {
		super();
	}

	public Account(final String key, final BigDecimal debit, final BigDecimal credit) {
		super();
		this.key = key;
		this.debit = (debit == null) ? ZERO : debit;
		this.credit = (credit == null) ? ZERO : credit;
	}

	public String key = "";
	public BigDecimal debit = ZERO;
	public BigDecimal credit = ZERO;
}