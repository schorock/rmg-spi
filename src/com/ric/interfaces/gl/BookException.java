package com.ric.interfaces.gl;


public class BookException extends Exception {

	public BookException() {
	}

	public BookException(final String message) {
		super(message);
	}

	public BookException(final Throwable cause) {
		super(cause);
	}

	public BookException(final String message, final Throwable cause) {
		super(message, cause);
	}

	/**
	 *
	 */
	private static final long serialVersionUID = -846741018949810115L;

}