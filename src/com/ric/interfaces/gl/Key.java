package com.ric.interfaces.gl;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Key implements List<String> {

	public Key() {
		super();
	}

	public Key(final List<String> list) {
		super();
		this.list = list;
	}

	public Key(final String ... list) {
		for (final String element : list)
			this.list.add(element);
	}
	
	@Override
	public int size() {
		return list.size();
	}

	@Override
	public boolean isEmpty() {
		return list.isEmpty();
	}

	@Override
	public boolean contains(final Object o) {
		return list.contains(o);
	}

	@Override
	public Iterator<String> iterator() {
		return list.iterator();
	}

	@Override
	public Object[] toArray() {
		return list.toArray();
	}

	@Override
	public <T> T[] toArray(final T[] a) {
		return list.toArray(a);
	}

	@Override
	public boolean add(final String e) {
		return list.add(e);
	}

	@Override
	public boolean remove(final Object o) {
		return list.remove(o);
	}

	@Override
	public boolean containsAll(final Collection<?> c) {
		return list.containsAll(c);
	}

	@Override
	public boolean addAll(final Collection<? extends String> c) {
		return list.addAll(c);
	}

	@Override
	public boolean addAll(final int index, final Collection<? extends String> c) {
		return list.addAll(index, c);
	}

	@Override
	public boolean removeAll(final Collection<?> c) {
		return list.removeAll(c);
	}

	@Override
	public boolean retainAll(final Collection<?> c) {
		return list.retainAll(c);
	}

	@Override
	public void clear() {
		list.clear();
	}

	@Override
	public String get(final int index) {
		return list.get(index);
	}

	@Override
	public String set(final int index, final String element) {
		return list.set(index, element);
	}

	@Override
	public void add(final int index, final String element) {
		list.add(index, element);
	}

	@Override
	public String remove(final int index) {
		return list.remove(index);
	}

	@Override
	public int indexOf(final Object o) {
		return list.indexOf(o);
	}

	@Override
	public int lastIndexOf(final Object o) {
		return list.lastIndexOf(o);
	}

	@Override
	public ListIterator<String> listIterator() {
		return list.listIterator();
	}

	@Override
	public ListIterator<String> listIterator(final int index) {
		return list.listIterator(index);
	}

	@Override
	public List<String> subList(final int fromIndex, final int toIndex) {
		return list.subList(fromIndex, toIndex);
	}

	@Override
	public int hashCode() {
		return list.hashCode();
	}

	public String join(final String separator) {
		return Perl.join(separator, list);
	}

	public String join(final char separator) {
		return Perl.join(separator, list);
	}

	public String join() {
		return join(SEPARATOR);
	}

	public void split(final String separator, final String value) {
		for (final String string : value.split(separator)) {
			list.add(string);
		}
	}

	public void split(final String value) {
		split(SEPARATOR, value);
	}

	public static final String SEPARATOR = "::";

	protected List<String> list = new ArrayList<String>();
}
