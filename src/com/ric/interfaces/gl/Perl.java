package com.ric.interfaces.gl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of Perl-like functionality
 * 
 * @author peter.hose
 *
 */
public class Perl {

	public static Map<String, String> map(final String fields, final String values) {
		final Map<String, String> map = new HashMap<String, String>();
		final String[] field = split(Perl.SEPARATOR, fields);
		final String[] value = split(Perl.SEPARATOR, values);

		if (field.length != value.length)
			throw new IllegalArgumentException("Arguments are inconsistant length");

		for (int index = 0, count = field.length; index < count; ++index)
			map.put(field[index], value[index]);

		return map;
	}

	/**
	 * Join an array of strings together<br>
	 * 
	 * @param separator the separator characters
	 * @param strings an array of strings to join
	 * @return the joined string
	 */
	public static String join(final String separator, final String... strings) {
		final StringBuffer result = new StringBuffer();

		for (final String string : strings) {
			if (result.length() > 0)
				result.append(separator);

			result.append(string);
		}

		return result.toString();
	}

	/**
	 * Join a list of strings together
	 * 
	 * @param separator the separator characters
	 * @param strings a list of strings to join
	 * @return the joined string
	 */
	public static String join(final String separator, final List<String> strings) {
		final StringBuffer result = new StringBuffer();

		for (final String string : strings) {
			if (result.length() > 0)
				result.append(separator);

			result.append(string);
		}

		return result.toString();
	}

	public static String join(final char separator, final String ... strings) {
		final StringBuilder sb = new StringBuilder();

		for (final String string : strings) {
			if (sb.length() > 0)
				sb.append(separator);

			sb.append(string);
		}

		return sb.toString();
	}
		
	public static String join(final char separator, final List<String> strings) {
		final StringBuffer result = new StringBuffer();

		for (final String string : strings) {
			if (result.length() > 0)
				result.append(separator);

			result.append(string);
		}

		return result.toString();
	}

	public static String[] split(final String separator, final String value) {
		return value.split(separator);
	}

	public static String[] split(final char separator, final String value) {
		return value.split(Character.toString(separator));
	}

	public static final String SEPARATOR = "::";
	// This are some common separators
	public static final String SCOPE = "::";
	public static final String COLON = ":";
	public static final String COMMA = ",";
	public static final String ARROW = "->";
	public static final String FARROW = "=>";
	public static final String NEWLINE = "\n";
	
	public static String PS = COLON;
	public static String FS = SCOPE;
	public static String RS = NEWLINE;

}
