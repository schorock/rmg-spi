package com.ric.interfaces.gl;

import java.util.HashMap;
import java.util.Map;

public class GLAccount {
	public static final String UNKNOWN = "999999";

	public static final class Type {
		public static final String Asset = "Asset";
		public static final String Equity = "Equity";
		public static final String Expense = "Expense";
		public static final String GainLoss = "Gain/Loss";
		public static final String Income = "Income";
		public static final String Liability = "Liability";
		
		private static String asset(final String value) {
			return join(Type.Asset, value);
		}

		private static String equity(final String value) {
			return join(Type.Equity, value);
		}

		private static String expense(final String value) {
			return join(Type.Expense, value);
		}

		private static String gainloss(final String value) {
			return join(Type.GainLoss, value);
		}

		private static String income(final String value) {
			return join(Type.Income, value);
		}

		private static String liability(final String value) {
			return join(Type.Liability, value);
		}
		
		private static String join(final String prefix, final String value) {
			return Perl.join(Perl.SEPARATOR, prefix, value);
		}
	}
	
	public static final class Asset {
		public static final String RECEIVABLES_FOR_INVESTMENTS = Type.asset("Receivables for Investments");
		public static final String INVESTMENT_INCOME_DUE_AND_ACCRUED = Type.asset("Investment Income Due and Accrued");
		public static final String A_R_PREMIUM = Type.asset("A/R - Premium");
		public static final String REINSURANCE_RECOVERABLE = Type.asset("Reinsurance Recoverable");
		public static final String CASH_FSA_BB_T = Type.asset("Cash - FSA (BB&T)");
		public static final String UNREALIZED_GAIN_LOSS_SHORT_TERM_DA = Type.asset("Unrealized Gain/Loss - Short Term (DA)");
		public static final String ACCUM_DEP_AUTO = Type.asset("Accum Dep - Auto");
		public static final String FIXED_ASSETS_EDP_NONADMITTED = Type.asset("Fixed Assets - EDP (Nonadmitted)");
		public static final String A_R_LATE_FEES = Type.asset("A/R - Late Fees");
		public static final String CASH_CLAIMS_ACCOUNT_BB_T = Type.asset("Cash - Claims Account (BB&T)");
		public static final String CASH_BOA_OPERATING_ACCOUNT = Type.asset("Cash - (BOA) Operating Account");
		public static final String PREPAID_POSTAGE = Type.asset("Prepaid Postage");
		public static final String A_R_SUBROGATION = Type.asset("A/R - Subrogation");
		public static final String INVESTMENTS_MM_D2_2 = Type.asset("Investments - MM (D2-2)");
		public static final String INVESTMENTS_PREFERRED_STOCK_D2_1 = Type.asset("Investments - Preferred Stock (D2-1)");
		public static final String INVESTMENTS_SHORT_TERM_DA = Type.asset("Investments - Short Term (DA)");
		public static final String REINSURANCE_RECOVERED = Type.asset("Reinsurance Recovered");
		public static final String UNREALIZED_GAIN_LOSS_CASH_EQUIVALENT_E = Type.asset("Unrealized Gain/Loss - Cash Equivalent (E)");
		public static final String A_R_INTERCOMPANY = Type.asset("A/R - Intercompany");
		public static final String ACCUM_DEP_EDP_EQUIP = Type.asset("Accum Dep - EDP Equip");
		public static final String CASH_OPERATING_ACCOUNT_BB_T = Type.asset("Cash - Operating Account (BB&T)");
		public static final String FIXED_ASSETS_EDP_EQUIP = Type.asset("Fixed Assets - EDP Equip");
		public static final String A_R_ASSESSMENTS = Type.asset("A/R - Assessments");
		public static final String ACCUM_DEP_LEASHOLD = Type.asset("Accum Dep - Leashold");
		public static final String INVESTMENTS_CASH_EQUIVALENT_E = Type.asset("Investments - Cash Equivalent (E)");
		public static final String LEASHOLD_IMPROVEMENTS = Type.asset("Leashold Improvements");
		public static final String INVESTMENTS_FIXED_INCOME_D1 = Type.asset("Investments - Fixed Income (D1)");
		public static final String ACCUM_DEP_EDP_NONADMITTED = Type.asset("Accum Dep - EDP (Nonadmitted)");
		public static final String CASH_BOA_PAYROLL_ACCOUNT = Type.asset("Cash - (BOA) Payroll Account");
		public static final String PREPAID_ASSETS = Type.asset("Prepaid Assets");
		public static final String CASH_PAYROLL_ACCOUNT_BB_T = Type.asset("Cash - Payroll Account (BB&T)");
		public static final String A_R_COMMISSION = Type.asset("A/R - Commission");
		public static final String FIXED_ASSETS_FURN_EQUIP = Type.asset("Fixed Assets - Furn & Equip");
		public static final String PREPAID_GROUP_INSURANCE = Type.asset("Prepaid Group Insurance");
		public static final String PREPAID_RENT = Type.asset("Prepaid Rent");
		public static final String CASH_BOA_MM = Type.asset("Cash - (BOA) MM");
		public static final String UNREALIZED_GAIN_LOSS_EQUITY_D2 = Type.asset("Unrealized Gain/Loss - Equity (D2)");
		public static final String PETTY_CASH = Type.asset("Petty Cash");
		public static final String INVESTMENTS_EQUITY_D2_2 = Type.asset("Investments - Equity (D2-2)");
		public static final String CASH_BOA_FSA = Type.asset("Cash - (BOA) FSA");
		public static final String CASH_DEBIT_CARD = Type.asset("Cash - Debit Card");
		public static final String FIXED_ASSETS_AUTO = Type.asset("Fixed Assets - Auto");
		public static final String ACCUM_DEP_FURN_EQUIP = Type.asset("Accum Dep - Furn & Equip");
		public static final String UNREALIZED_GAIN_LOSS_FIXED_INCOME_D1 = Type.asset("Unrealized Gain/Loss - Fixed Income (D1)");
		public static final String CASH_BOA_CLAIMS_ACCOUNT = Type.asset("Cash - (BOA) Claims Account");
		public static final String CASH_MM_BB_T = Type.asset("Cash - MM (BB&T)");

	}

	public static final class Equity {
		public static final String CHANGE_IN_UNREALIZED_GAIN_LOSS = Type.equity("Change in Unrealized Gain/(Loss)");
		public static final String INSOLVENT_MEMBER_COMPANY = Type.equity("Insolvent Member Company");
		public static final String CHANGE_IN_PENSION_LIABILITY = Type.equity("Change in Pension Liability");
		public static final String ASSESSMENT_TO_MEMBER_COMPANIES = Type.equity("Assessment to Member Companies");
		public static final String CHANGE_IN_NON_ADMITTED = Type.equity("Change in Non-Admitted");
		public static final String CHANGE_IN_PROVISION_FOR_REINSURANCE = Type.equity("Change in Provision for Reinsurance");
		public static final String CLOSED_MEMBER_INSURERS_EQUITY = Type.equity("Closed Member Insurers Equity");
		public static final String DISTRIBUTIONS_TO_MEMBER_COMPANIES = Type.equity("Distributions to Member Companies");
	}

	public static final class Expense {
		public static final String COMMISSION_EXPENSE = Type.expense("Commission Expense");
		public static final String FICA_TAX = Type.expense("FICA Tax");
		public static final String EDUCATION_SEMINARS_MEALS_ENTERTAINMENT = Type.expense("Education & Seminars - Meals/Entertainment");
		public static final String PREPAID_INTEREST_CASH_EQUIVALENT_E = Type.expense("Prepaid Interest - Cash Equivalent (E)");
		public static final String BOARD_OF_DIRECTOR_TRAVEL = Type.expense("Board of Director Travel");
		public static final String FEDERAL_UNEMPLOYMENT_TAX = Type.expense("Federal Unemployment Tax");
		public static final String BOARDS_BUREAUS_ASSOCIATIONS = Type.expense("Boards Bureaus & Associations");
		public static final String INVESTMENT_SOFTWARE_EXPENSE = Type.expense("Investment Software Expense");
		public static final String BANK_SERVICE_FEES_INTEREST = Type.expense("Bank Service Fees/Interest");
		public static final String AMORTIZATION_FIXED_INCOME_D1 = Type.expense("Amortization - Fixed Income (D1)");
		public static final String WORKERS_COMPENSATION = Type.expense("Workers Compensation");
		public static final String REINSURANCE_EXPENSE_GENERAL = Type.expense("Reinsurance Expense - General");
		public static final String RETIREMENT_401K = Type.expense("Retirement - 401k");
		public static final String BOOKS_SUBSCRIPTIONS = Type.expense("Books & Subscriptions");
		public static final String EDUCATION_SEMINARS = Type.expense("Education & Seminars");
		public static final String PAYROLL_PROCESSING_EXPENSE = Type.expense("Payroll Processing Expense");
		public static final String PRINTING_STATIONARY = Type.expense("Printing - Stationary");
		public static final String OFFICE_SUPPLIES = Type.expense("Office Supplies");
		public static final String CHANGE_IN_LAE_RESERVE = Type.expense("Change in LAE Reserve");
		public static final String PRINTING_GENERAL = Type.expense("Printing - General");
		public static final String PREMIUM_TAXES = Type.expense("Premium Taxes");
		public static final String UTILITIES = Type.expense("Utilities");
		public static final String RECRUITING_FEES = Type.expense("Recruiting Fees");
		public static final String MEALS_ENTERTAINMENT = Type.expense("Meals & Entertainment");
		public static final String REINSURANCE_EXPENSE_LEGAL = Type.expense("Reinsurance Expense - Legal");
		public static final String FURNITURE_EQUIPMENT = Type.expense("Furniture & Equipment");
		public static final String DEPRECIATION_EXPENSE = Type.expense("Depreciation Expense");
		public static final String PREPAID_INTEREST_FIXED_INCOME_D1 = Type.expense("Prepaid Interest - Fixed Income (D1)");
		public static final String TEMPORARY = Type.expense("Temporary");
		public static final String FACILITIES_MAINTENANCE = Type.expense("Facilities Maintenance");
		public static final String LAE = Type.expense("LAE");
		public static final String RETIREMENT_EXPENSE_FEES = Type.expense("Retirement - Expense Fees");
		public static final String ARSON_REWARD_FUND = Type.expense("Arson Reward Fund");
		public static final String INSURANCE_COMPANY_LEVEL = Type.expense("Insurance - Company Level");
		public static final String PAID_LOSSES = Type.expense("Paid Losses");
		public static final String COMPUTER_HARDWARE = Type.expense("Computer Hardware");
		public static final String LICENSE_FEES = Type.expense("License & Fees");
		public static final String BAD_DEBT_EXPENSE = Type.expense("Bad Debt Expense");
		public static final String PROPERTY_TAXES = Type.expense("Property Taxes");
		public static final String STATE_UMEMPLOYMENT_TAX = Type.expense("State Umemployment Tax");
		public static final String CHANGE_IN_LOSS_RESERVE = Type.expense("Change in Loss Reserve");
		public static final String CREDIT_CARD_FEES = Type.expense("Credit Card Fees");
		public static final String INTEREST_REFUND = Type.expense("Interest Refund");
		public static final String LEGAL_FEES = Type.expense("Legal Fees");
		public static final String CAT_EXPENSE_CLAIMS = Type.expense("CAT Expense - Claims");
		public static final String EMPLOYEE_BENEFITS_OTHER = Type.expense("Employee Benefits - Other");
		public static final String MANAGER_ADVISORY_COUNCIL = Type.expense("Manager Advisory Council");
		public static final String INSURANCE_PROPERTY_LIABILITY = Type.expense("Insurance - Property & Liability");
		public static final String AUTOMOBILE_MAINTENANCE = Type.expense("Automobile Maintenance");
		public static final String AMORTIZATION_CASH_EQUIVALENT_E = Type.expense("Amortization - Cash Equivalent (E)");
		public static final String INFORMATION_TECHNOLOGY = Type.expense("Information Technology");
		public static final String GENERAL_EXPENSE_ALLOCATION_CLAIMS = Type.expense("General Expense Allocation - Claims");
		public static final String LAE_RECOVERY = Type.expense("LAE Recovery");
		public static final String RENT_EQUIPMENT_OTHER = Type.expense("Rent - Equipment/Other");
		public static final String STORM_READINESS_SEMINARS = Type.expense("Storm Readiness Seminars");
		public static final String COBRA_EXPENSES = Type.expense("Cobra Expenses");
		public static final String FLEX_SPENDING_ACCOUNT = Type.expense("Flex Spending Account");
		public static final String RENT_OFFICE_BUILDING = Type.expense("Rent - Office Building");
		public static final String PRINTING_DATA_PROCESS = Type.expense("Printing - Data Process");
		public static final String PROFESSIONAL_SERVICES_OTHER = Type.expense("Professional Services - Other");
		public static final String POSTAGE = Type.expense("Postage");
		public static final String TELEPHONE = Type.expense("Telephone");
		public static final String MEMBERSHIPS_ASSOCIATIONS = Type.expense("Memberships & Associations");
		public static final String SURVEYS_INSPECTIONS = Type.expense("Surveys & Inspections");
		public static final String SUBROGATION = Type.expense("Subrogation");
		public static final String GENERAL_EXPENSE_ALLOCATION_UNDERWRITING = Type.expense("General Expense Allocation - Underwriting");
		public static final String RETIREMENT_PENSION = Type.expense("Retirement - Pension");
		public static final String PREPAID_INTEREST_SHORT_TERM_DA = Type.expense("Prepaid Interest - Short Term (DA)");
		public static final String INVESTMENT_MANAGEMENT_FEES = Type.expense("Investment Management Fees");
		public static final String HR_INITIATIVES = Type.expense("HR Initiatives");
		public static final String SALARIES = Type.expense("Salaries");
		public static final String AUDIT_TAXES = Type.expense("Audit & Taxes");
		public static final String INSOLVENT_COMPANY_RETURNED_EQUITY = Type.expense("Insolvent Company - Returned Equity");
		public static final String GENERAL_EXPENSE_ALLOCATION_INVESTMENTS = Type.expense("General Expense Allocation - Investments");
		public static final String LEASHOLD_IMPROVEMENTS = Type.expense("Leashold Improvements");
		public static final String AUTOMOBILE = Type.expense("Automobile");
		public static final String INVESTMENT_TRUE_UP = Type.expense("Investment - True-up");
		public static final String MISCELLANEOUS_INCOME_EXPENSE = Type.expense("Miscellaneous Income / Expense");
		public static final String ACTUARIAL_FEES = Type.expense("Actuarial Fees");
		public static final String AMORTIZATION_SHORT_TERM_DA = Type.expense("Amortization - Short Term (DA)");
		public static final String TRAVEL = Type.expense("Travel");
		public static final String COMMISSION_WRITTEN_OFF = Type.expense("Commission - Written Off");
		public static final String BOARD_MEETING_EXPENSES = Type.expense("Board Meeting Expenses");
	}

	public static final class GainLoss {
		public static final String UNASSIGNED_SURPLUS_RETAINED_EARNINGS = Type.gainloss("Unassigned Surplus/Retained Earnings");
	}

	public static final class Income {
		public static final String PREMIUM_WRITTEN_OFF = Type.income("Premium - Written Off");
		public static final String GAIN_LOSS_EQUITY_D2 = Type.income("Gain/Loss - Equity (D2)");
		public static final String INTEREST_DIVIDENDS_RECEIVED_EQUITY_D2 = Type.income("Interest/Dividends Received - Equity (D2)");
		public static final String WRITTEN_PREMIUM = Type.income("Written Premium");
		public static final String INCOME_INFORMATION_REQUEST = Type.income("Income Information Request");
		public static final String CHANGE_IN_UNEARNED_PREMIUM = Type.income("Change in Unearned Premium");
		public static final String GAIN_LOSS_CASH_EQUIVALENT_E = Type.income("Gain/Loss - Cash Equivalent (E)");
		public static final String PREMIUM_MISAPPLIED = Type.income("Premium - Misapplied");
		public static final String INTEREST_DIVIDENDS_RECEIVED_FIXED_INCOME_D1 = Type.income("Interest/Dividends Received - Fixed Income (D1)");
		public static final String ACCRUED_INTEREST_DIVIDENDS_SHORT_TERM_DA = Type.income("Accrued Interest/Dividends - Short Term (DA)");
		public static final String CEDED_REINSURANCE_PREMIUM_BEACH = Type.income("Ceded Reinsurance Premium - Beach");
		public static final String ACCRUED_INTEREST_DIVIDENDS_CASH_EQUIVALENT_E = Type.income("Accrued Interest/Dividends - Cash Equivalent (E)");
		public static final String GAIN_LOSS_SHORT_TERM_DA = Type.income("Gain/Loss - Short Term (DA)");
		public static final String INSUFFICIENT_FUND_FEE = Type.income("Insufficient Fund Fee");
		public static final String CEDED_REINSURANCE_PREMIUM_FAIR = Type.income("Ceded Reinsurance Premium - FAIR");
		public static final String ACCRUED_INTEREST_DIVIDENDS_FIXED_INCOME_D1 = Type.income("Accrued Interest/Dividends - Fixed Income (D1)");
		public static final String INTEREST_DIVIDENDS_RECEIVED_SHORT_TERM_DA = Type.income("Interest/Dividends Received - Short Term (DA)");
		public static final String GAIN_LOSS_FIXED_INCOME_D1 = Type.income("Gain/Loss - Fixed Income (D1)");
		public static final String FIXED_ASSET_GAIN_LOSS = Type.income("Fixed Asset - Gain/Loss");
		public static final String CEDED_REINSURANCE_PREMIUM_COASTAL = Type.income("Ceded Reinsurance Premium - Coastal");
		public static final String MISCELLANEOUS_INCOME_SERVICE_CHARGES = Type.income("Miscellaneous Income - Service Charges");
		public static final String INTEREST_DIVIDENDS_RECEIVED_CASH_EQUIVALENT_E = Type.income("Interest/Dividends Received - Cash Equivalent (E)");
		public static final String PREMIUM_SYSTEM_WRITE_OFF = Type.income("Premium - System Write Off");
		public static final String ACCRUED_INTEREST_DIVIDENDS_EQUITY_D2 = Type.income("Accrued Interest/Dividends - Equity (D2)");
		public static final String CATASTROPHE_RECOVERY_CHARGE = Type.income("Catastrophe Recovery Charge (CRC) income");
	}

	public static final class Liability {
		public static final String UNCLAIMED_CHECKS_CLAIMS = Type.liability("Unclaimed Checks - Claims");
		public static final String UNCLAIMED_CHECKS_MEMBER_COMPANY = Type.liability("Unclaimed Checks - Member Company");
		public static final String ACCRUED_PENSION = Type.liability("Accrued - Pension");
		public static final String CLAIMS_PAYABLE_CLEARING = Type.liability("Claims Payable - Clearing");
		public static final String IBNR_RESERVE = Type.liability("IBNR Reserve");
		public static final String UNCLAIMED_CHECKS_REFUND = Type.liability("Unclaimed Checks - Refund");
		public static final String ACCRUED_POST_RETIREMENT = Type.liability("Accrued - Post Retirement");
		public static final String CASE_LAE_RESERVE = Type.liability("Case LAE Reserve");
		public static final String DISTRIBUTION_PAYABLE = Type.liability("Distribution Payable");
		public static final String A_P_INTERCOMPANY = Type.liability("A/P - Intercompany");
		public static final String EMPLOYEE_SAVING_LOAN_PAYABLE = Type.liability("Employee Saving Loan Payable");
		public static final String FEDERAL_TAX_PAYABLE = Type.liability("Federal Tax Payable");
		public static final String PREPAID_PREMIUM = Type.liability("Prepaid Premium");
		public static final String FICA_TAX_PAYABLE_EMPLOYEE = Type.liability("FICA Tax Payable - Employee");
		public static final String ACCRUED_PREMIUM_TAX = Type.liability("Accrued - Premium Tax");
		public static final String COMMISSION_PAYABLE = Type.liability("Commission Payable");
		public static final String ISO_REFUND = Type.liability("ISO Refund");
		public static final String UNEARNED_PREMIUM = Type.liability("Unearned Premium");
		public static final String ACCRUED_RENT = Type.liability("Accrued - Rent");
		public static final String PAYABLE_FOR_INVESTMENTS = Type.liability("Payable for Investments");
		public static final String A_P_GENERAL = Type.liability("A/P - General");
		public static final String HEALTH_CARE_PAYABLE = Type.liability("Health Care Payable");
		public static final String IBNR_LAE_RESERVE = Type.liability("IBNR LAE Reserve");
		public static final String STATE_TAX_PAYABLE = Type.liability("State Tax Payable");
		public static final String DEPENDENT_CARE_PAYABLE = Type.liability("Dependent Care Payable");
		public static final String GARNISHMENT_PAYABLE = Type.liability("Garnishment Payable");
		public static final String UNCLAIMED_CHECKS_COMMISSION = Type.liability("Unclaimed Checks - Commission");
		public static final String PREMIUM_SUSPENSE = Type.liability("Premium Suspense");
		public static final String CLAIMS_PAYABLE_O_S = Type.liability("Claims Payable - O/S");
		public static final String CASE_LOSS_RESERVE = Type.liability("Case Loss Reserve");
		public static final String POLICYHOLDER_REFUND_NON_ISO = Type.liability("Policyholder Refund - Non ISO");
		public static final String PREMIUM_CLEARING = Type.liability("Premium Clearing");
		public static final String CLAIMS_CLEARING = Type.liability("Claims Clearing");
	}

	private static String account(final String carrierCd, final String type, final String name) {
		String account = null;

		try {
			account = map.get(carrierCd).get(type).get(name);
		}
		catch (final Exception e) {
			// Fail quietly
		}

		return account;
	}

	public static String account(final String carrierCd, final String name) {
		final String[] values = Perl.split(Perl.SEPARATOR, name);

		return account(carrierCd, values[0], name);
	}

	protected static final Map<String, Map<String, Map<String, String>>> map = new HashMap<String, Map<String, Map<String, String>>>();

	static {

		Map<String, Map<String, String>> carrier = null;
		Map<String, String> type = null;
		// @formatter:off

		carrier = new HashMap<String, Map<String, String>>();

			type = new HashMap<String, String>();
			type.put(Asset.ACCUM_DEP_AUTO, "140150");
			type.put(Asset.ACCUM_DEP_EDP_EQUIP, "140350");
			type.put(Asset.ACCUM_DEP_EDP_NONADMITTED, "140450");
			type.put(Asset.ACCUM_DEP_FURN_EQUIP, "140050");
			type.put(Asset.ACCUM_DEP_LEASHOLD, "140250");
			type.put(Asset.A_R_ASSESSMENTS, "130100");
			type.put(Asset.A_R_COMMISSION, "130500");
			type.put(Asset.A_R_INTERCOMPANY, "130400");
			type.put(Asset.A_R_LATE_FEES, "130300");
			type.put(Asset.A_R_PREMIUM, "130000");
			type.put(Asset.A_R_SUBROGATION, "130200");
			type.put(Asset.CASH_BOA_CLAIMS_ACCOUNT, "101100");
			type.put(Asset.CASH_BOA_FSA, "101300");
			type.put(Asset.CASH_BOA_MM, "101400");
			type.put(Asset.CASH_BOA_OPERATING_ACCOUNT, "101000");
			type.put(Asset.CASH_BOA_PAYROLL_ACCOUNT, "101200");
			type.put(Asset.CASH_CLAIMS_ACCOUNT_BB_T, "100100");
			type.put(Asset.CASH_DEBIT_CARD, "100300");
			type.put(Asset.CASH_FSA_BB_T, "100400");
			type.put(Asset.CASH_MM_BB_T, "100500");
			type.put(Asset.CASH_OPERATING_ACCOUNT_BB_T, "100000");
			type.put(Asset.CASH_PAYROLL_ACCOUNT_BB_T, "100200");
			type.put(Asset.FIXED_ASSETS_AUTO, "140100");
			type.put(Asset.FIXED_ASSETS_EDP_EQUIP, "140300");
			type.put(Asset.FIXED_ASSETS_EDP_NONADMITTED, "140400");
			type.put(Asset.FIXED_ASSETS_FURN_EQUIP, "140000");
			type.put(Asset.INVESTMENTS_CASH_EQUIVALENT_E, "110400");
			type.put(Asset.INVESTMENTS_EQUITY_D2_2, "110200");
			type.put(Asset.INVESTMENTS_FIXED_INCOME_D1, "110000");
			type.put(Asset.INVESTMENTS_MM_D2_2, "110500");
			type.put(Asset.INVESTMENTS_PREFERRED_STOCK_D2_1, "110100");
			type.put(Asset.INVESTMENTS_SHORT_TERM_DA, "110300");
			type.put(Asset.INVESTMENT_INCOME_DUE_AND_ACCRUED, "120700");
			type.put(Asset.LEASHOLD_IMPROVEMENTS, "140200");
			type.put(Asset.PETTY_CASH, "100900");
			type.put(Asset.PREPAID_ASSETS, "150300");
			type.put(Asset.PREPAID_GROUP_INSURANCE, "150200");
			type.put(Asset.PREPAID_POSTAGE, "150100");
			type.put(Asset.PREPAID_RENT, "150000");
			type.put(Asset.RECEIVABLES_FOR_INVESTMENTS, "121000");
			type.put(Asset.REINSURANCE_RECOVERABLE, "132000");
			type.put(Asset.REINSURANCE_RECOVERED, "132500");
			type.put(Asset.UNREALIZED_GAIN_LOSS_CASH_EQUIVALENT_E, "120300");
			type.put(Asset.UNREALIZED_GAIN_LOSS_EQUITY_D2, "120100");
			type.put(Asset.UNREALIZED_GAIN_LOSS_FIXED_INCOME_D1, "120000");
			type.put(Asset.UNREALIZED_GAIN_LOSS_SHORT_TERM_DA, "120200");

			carrier.put(Type.Asset, type);

			type = new HashMap<String, String>();
			type.put(Equity.ASSESSMENT_TO_MEMBER_COMPANIES, "310250");
			type.put(Equity.CHANGE_IN_NON_ADMITTED, "310100");
			type.put(Equity.CHANGE_IN_PENSION_LIABILITY, "310400");
			type.put(Equity.CHANGE_IN_PROVISION_FOR_REINSURANCE, "310300");
			type.put(Equity.CHANGE_IN_UNREALIZED_GAIN_LOSS, "310000");
			type.put(Equity.CLOSED_MEMBER_INSURERS_EQUITY, "310500");
			type.put(Equity.DISTRIBUTIONS_TO_MEMBER_COMPANIES, "310200");
			type.put(Equity.INSOLVENT_MEMBER_COMPANY, "310600");

			carrier.put(Type.Equity, type);

			type = new HashMap<String, String>();
			type.put(Expense.ACTUARIAL_FEES, "775100");
			type.put(Expense.AMORTIZATION_CASH_EQUIVALENT_E, "810200");
			type.put(Expense.AMORTIZATION_FIXED_INCOME_D1, "810000");
			type.put(Expense.AMORTIZATION_SHORT_TERM_DA, "810100");
			type.put(Expense.ARSON_REWARD_FUND, "786500");
			type.put(Expense.AUDIT_TAXES, "775000");
			type.put(Expense.AUTOMOBILE, "761000");
			type.put(Expense.AUTOMOBILE_MAINTENANCE, "761500");
			type.put(Expense.BAD_DEBT_EXPENSE, "918000");
			type.put(Expense.BANK_SERVICE_FEES_INTEREST, "860100");
			type.put(Expense.BOARDS_BUREAUS_ASSOCIATIONS, "710000");
			type.put(Expense.BOARD_MEETING_EXPENSES, "750000");
			type.put(Expense.BOARD_OF_DIRECTOR_TRAVEL, "750100");
			type.put(Expense.BOOKS_SUBSCRIPTIONS, "732000");
			type.put(Expense.CAT_EXPENSE_CLAIMS, "785200");
			type.put(Expense.CHANGE_IN_LAE_RESERVE, "560000");
			type.put(Expense.CHANGE_IN_LOSS_RESERVE, "530000");
			type.put(Expense.COBRA_EXPENSES, "739999");
			type.put(Expense.COMMISSION_EXPENSE, "610000");
			type.put(Expense.COMMISSION_WRITTEN_OFF, "620000");
			type.put(Expense.COMPUTER_HARDWARE, "760700");
			type.put(Expense.CREDIT_CARD_FEES, "782000");
			type.put(Expense.DEPRECIATION_EXPENSE, "762000");
			type.put(Expense.EDUCATION_SEMINARS, "731000");
			type.put(Expense.EDUCATION_SEMINARS_MEALS_ENTERTAINMENT, "731050");
			type.put(Expense.EMPLOYEE_BENEFITS_OTHER, "730400");
			type.put(Expense.FACILITIES_MAINTENANCE, "760400");
			type.put(Expense.FEDERAL_UNEMPLOYMENT_TAX, "721100");
			type.put(Expense.FICA_TAX, "721000");
			type.put(Expense.FLEX_SPENDING_ACCOUNT, "730700");
			type.put(Expense.FURNITURE_EQUIPMENT, "760600");
			type.put(Expense.GENERAL_EXPENSE_ALLOCATION_CLAIMS, "599999");
			type.put(Expense.GENERAL_EXPENSE_ALLOCATION_INVESTMENTS, "899999");
			type.put(Expense.GENERAL_EXPENSE_ALLOCATION_UNDERWRITING, "799999");
			type.put(Expense.HR_INITIATIVES, "733000");
			type.put(Expense.INFORMATION_TECHNOLOGY, "765000");
			type.put(Expense.INSOLVENT_COMPANY_RETURNED_EQUITY, "925000");
			type.put(Expense.INSURANCE_COMPANY_LEVEL, "730600");
			type.put(Expense.INSURANCE_PROPERTY_LIABILITY, "740000");
			type.put(Expense.INTEREST_REFUND, "911000");
			type.put(Expense.INVESTMENT_MANAGEMENT_FEES, "860000");
			type.put(Expense.INVESTMENT_SOFTWARE_EXPENSE, "860400");
			type.put(Expense.INVESTMENT_TRUE_UP, "860500");
			type.put(Expense.LAE, "540000");
			type.put(Expense.LAE_RECOVERY, "550000");
			type.put(Expense.LEASHOLD_IMPROVEMENTS, "760500");
			type.put(Expense.LEGAL_FEES, "775200");
			type.put(Expense.LICENSE_FEES, "780000");
			type.put(Expense.MANAGER_ADVISORY_COUNCIL, "750200");
			type.put(Expense.MEALS_ENTERTAINMENT, "755100");
			type.put(Expense.MEMBERSHIPS_ASSOCIATIONS, "734000");
			type.put(Expense.MISCELLANEOUS_INCOME_EXPENSE, "910000");
			type.put(Expense.OFFICE_SUPPLIES, "771000");
			type.put(Expense.PAID_LOSSES, "510000");
			type.put(Expense.PAYROLL_PROCESSING_EXPENSE, "730500");
			type.put(Expense.POSTAGE, "773000");
			type.put(Expense.PREMIUM_TAXES, "781000");
			type.put(Expense.PREPAID_INTEREST_CASH_EQUIVALENT_E, "820200");
			type.put(Expense.PREPAID_INTEREST_FIXED_INCOME_D1, "820000");
			type.put(Expense.PREPAID_INTEREST_SHORT_TERM_DA, "820100");
			type.put(Expense.PRINTING_DATA_PROCESS, "770100");
			type.put(Expense.PRINTING_GENERAL, "770000");
			type.put(Expense.PRINTING_STATIONARY, "770200");
			type.put(Expense.PROFESSIONAL_SERVICES_OTHER, "775300");
			type.put(Expense.PROPERTY_TAXES, "760100");
			type.put(Expense.RECRUITING_FEES, "730300");
			type.put(Expense.REINSURANCE_EXPENSE_GENERAL, "785000");
			type.put(Expense.REINSURANCE_EXPENSE_LEGAL, "785100");
			type.put(Expense.RENT_EQUIPMENT_OTHER, "760300");
			type.put(Expense.RENT_OFFICE_BUILDING, "760200");
			type.put(Expense.RETIREMENT_401K, "730000");
			type.put(Expense.RETIREMENT_EXPENSE_FEES, "730200");
			type.put(Expense.RETIREMENT_PENSION, "730100");
			type.put(Expense.SALARIES, "720000");
			type.put(Expense.STATE_UMEMPLOYMENT_TAX, "721200");
			type.put(Expense.STORM_READINESS_SEMINARS, "786000");
			type.put(Expense.SUBROGATION, "520000");
			type.put(Expense.SURVEYS_INSPECTIONS, "711000");
			type.put(Expense.TELEPHONE, "772000");
			type.put(Expense.TEMPORARY, "720100");
			type.put(Expense.TRAVEL, "755000");
			type.put(Expense.UTILITIES, "760000");
			type.put(Expense.WORKERS_COMPENSATION, "740100");

			carrier.put(Type.Expense, type);

			type = new HashMap<String, String>();
			type.put(GainLoss.UNASSIGNED_SURPLUS_RETAINED_EARNINGS, "399999");

			carrier.put(Type.GainLoss, type);

			type = new HashMap<String, String>();
			type.put(Income.ACCRUED_INTEREST_DIVIDENDS_CASH_EQUIVALENT_E, "830300");
			type.put(Income.ACCRUED_INTEREST_DIVIDENDS_EQUITY_D2, "830100");
			type.put(Income.ACCRUED_INTEREST_DIVIDENDS_FIXED_INCOME_D1, "830000");
			type.put(Income.ACCRUED_INTEREST_DIVIDENDS_SHORT_TERM_DA, "830200");
			type.put(Income.CEDED_REINSURANCE_PREMIUM_BEACH, "420000");
			type.put(Income.CEDED_REINSURANCE_PREMIUM_COASTAL, "421000");
			type.put(Income.CEDED_REINSURANCE_PREMIUM_FAIR, "423000");
			type.put(Income.CHANGE_IN_UNEARNED_PREMIUM, "430000");
			type.put(Income.FIXED_ASSET_GAIN_LOSS, "917000");
			type.put(Income.GAIN_LOSS_CASH_EQUIVALENT_E, "850300");
			type.put(Income.GAIN_LOSS_EQUITY_D2, "850100");
			type.put(Income.GAIN_LOSS_FIXED_INCOME_D1, "850000");
			type.put(Income.GAIN_LOSS_SHORT_TERM_DA, "850200");
			type.put(Income.INCOME_INFORMATION_REQUEST, "919000");
			type.put(Income.INSUFFICIENT_FUND_FEE, "916000");
			type.put(Income.INTEREST_DIVIDENDS_RECEIVED_CASH_EQUIVALENT_E, "840300");
			type.put(Income.INTEREST_DIVIDENDS_RECEIVED_EQUITY_D2, "840100");
			type.put(Income.INTEREST_DIVIDENDS_RECEIVED_FIXED_INCOME_D1, "840000");
			type.put(Income.INTEREST_DIVIDENDS_RECEIVED_SHORT_TERM_DA, "840200");
			type.put(Income.MISCELLANEOUS_INCOME_SERVICE_CHARGES, "912000");
			type.put(Income.PREMIUM_MISAPPLIED, "914000");
			type.put(Income.PREMIUM_SYSTEM_WRITE_OFF, "915000");
			type.put(Income.PREMIUM_WRITTEN_OFF, "913000");
			type.put(Income.WRITTEN_PREMIUM, "410000");
			type.put(Income.CATASTROPHE_RECOVERY_CHARGE, "489999");

			carrier.put(Type.Income, type);

			type = new HashMap<String, String>();
			type.put(Liability.ACCRUED_PENSION, "260100");
			type.put(Liability.ACCRUED_POST_RETIREMENT, "260200");
			type.put(Liability.ACCRUED_PREMIUM_TAX, "260000");
			type.put(Liability.ACCRUED_RENT, "260300");
			type.put(Liability.A_P_GENERAL, "250000");
			type.put(Liability.A_P_INTERCOMPANY, "250100");
			type.put(Liability.CASE_LAE_RESERVE, "220000");
			type.put(Liability.CASE_LOSS_RESERVE, "210000");
			type.put(Liability.CLAIMS_PAYABLE_CLEARING, "252000");
			type.put(Liability.CLAIMS_PAYABLE_O_S, "253000");
			type.put(Liability.COMMISSION_PAYABLE, "250500");
			type.put(Liability.DEPENDENT_CARE_PAYABLE, "270500");
			type.put(Liability.DISTRIBUTION_PAYABLE, "250600");
			type.put(Liability.EMPLOYEE_SAVING_LOAN_PAYABLE, "270400");
			type.put(Liability.FEDERAL_TAX_PAYABLE, "270100");
			type.put(Liability.FICA_TAX_PAYABLE_EMPLOYEE, "270000");
			type.put(Liability.GARNISHMENT_PAYABLE, "270300");
			type.put(Liability.HEALTH_CARE_PAYABLE, "270600");
			type.put(Liability.IBNR_LAE_RESERVE, "225000");
			type.put(Liability.IBNR_RESERVE, "215000");
			type.put(Liability.ISO_REFUND, "250300");
			type.put(Liability.PAYABLE_FOR_INVESTMENTS, "251000");
			type.put(Liability.POLICYHOLDER_REFUND_NON_ISO, "250400");
			type.put(Liability.PREMIUM_SUSPENSE, "250200");
			type.put(Liability.PREPAID_PREMIUM, "240000");
			type.put(Liability.STATE_TAX_PAYABLE, "270200");
			type.put(Liability.UNCLAIMED_CHECKS_CLAIMS, "261100");
			type.put(Liability.UNCLAIMED_CHECKS_COMMISSION, "261200");
			type.put(Liability.UNCLAIMED_CHECKS_MEMBER_COMPANY, "261300");
			type.put(Liability.UNCLAIMED_CHECKS_REFUND, "261000");
			type.put(Liability.UNEARNED_PREMIUM, "230000");
			type.put(Liability.PREMIUM_CLEARING, "259000");
			type.put(Liability.CLAIMS_CLEARING, "259100");

			carrier.put(Type.Liability, type);

		map.put(GLCarrierCd.NORTH_CAROLINA_INSURANCE_UNDERWRITING_ASSOCIATION, carrier);

		carrier = new HashMap<String, Map<String, String>>();

			type = new HashMap<String, String>();
			type.put(Asset.ACCUM_DEP_AUTO, "140150");
			type.put(Asset.ACCUM_DEP_EDP_EQUIP, "140350");
			type.put(Asset.ACCUM_DEP_EDP_NONADMITTED, "140450");
			type.put(Asset.ACCUM_DEP_FURN_EQUIP, "140050");
			type.put(Asset.ACCUM_DEP_LEASHOLD, "140250");
			type.put(Asset.A_R_ASSESSMENTS, "130100");
			type.put(Asset.A_R_COMMISSION, "130500");
			type.put(Asset.A_R_INTERCOMPANY, "130400");
			type.put(Asset.A_R_LATE_FEES, "130300");
			type.put(Asset.A_R_PREMIUM, "130000");
			type.put(Asset.A_R_SUBROGATION, "130200");
			type.put(Asset.CASH_BOA_CLAIMS_ACCOUNT, "101100");
			type.put(Asset.CASH_BOA_FSA, "101300");
			type.put(Asset.CASH_BOA_MM, "101400");
			type.put(Asset.CASH_BOA_OPERATING_ACCOUNT, "101000");
			type.put(Asset.CASH_BOA_PAYROLL_ACCOUNT, "101200");
			type.put(Asset.CASH_CLAIMS_ACCOUNT_BB_T, "100100");
			type.put(Asset.CASH_DEBIT_CARD, "100300");
			type.put(Asset.CASH_FSA_BB_T, "100400");
			type.put(Asset.CASH_MM_BB_T, "100500");
			type.put(Asset.CASH_OPERATING_ACCOUNT_BB_T, "100000");
			type.put(Asset.CASH_PAYROLL_ACCOUNT_BB_T, "100200");
			type.put(Asset.FIXED_ASSETS_AUTO, "140100");
			type.put(Asset.FIXED_ASSETS_EDP_EQUIP, "140300");
			type.put(Asset.FIXED_ASSETS_EDP_NONADMITTED, "140400");
			type.put(Asset.FIXED_ASSETS_FURN_EQUIP, "140000");
			type.put(Asset.INVESTMENTS_CASH_EQUIVALENT_E, "110400");
			type.put(Asset.INVESTMENTS_EQUITY_D2_2, "110200");
			type.put(Asset.INVESTMENTS_FIXED_INCOME_D1, "110000");
			type.put(Asset.INVESTMENTS_MM_D2_2, "110500");
			type.put(Asset.INVESTMENTS_PREFERRED_STOCK_D2_1, "110100");
			type.put(Asset.INVESTMENTS_SHORT_TERM_DA, "110300");
			type.put(Asset.INVESTMENT_INCOME_DUE_AND_ACCRUED, "120700");
			type.put(Asset.LEASHOLD_IMPROVEMENTS, "140200");
			type.put(Asset.PETTY_CASH, "100900");
			type.put(Asset.PREPAID_ASSETS, "150300");
			type.put(Asset.PREPAID_GROUP_INSURANCE, "150200");
			type.put(Asset.PREPAID_POSTAGE, "150100");
			type.put(Asset.PREPAID_RENT, "150000");
			type.put(Asset.RECEIVABLES_FOR_INVESTMENTS, "121000");
			type.put(Asset.REINSURANCE_RECOVERABLE, "132000");
			type.put(Asset.REINSURANCE_RECOVERED, "132500");
			type.put(Asset.UNREALIZED_GAIN_LOSS_CASH_EQUIVALENT_E, "120300");
			type.put(Asset.UNREALIZED_GAIN_LOSS_EQUITY_D2, "120100");
			type.put(Asset.UNREALIZED_GAIN_LOSS_FIXED_INCOME_D1, "120000");
			type.put(Asset.UNREALIZED_GAIN_LOSS_SHORT_TERM_DA, "120200");

			carrier.put(Type.Asset, type);

			type = new HashMap<String, String>();
			type.put(Equity.ASSESSMENT_TO_MEMBER_COMPANIES, "310250");
			type.put(Equity.CHANGE_IN_NON_ADMITTED, "310100");
			type.put(Equity.CHANGE_IN_PENSION_LIABILITY, "310400");
			type.put(Equity.CHANGE_IN_PROVISION_FOR_REINSURANCE, "310300");
			type.put(Equity.CHANGE_IN_UNREALIZED_GAIN_LOSS, "310000");
			type.put(Equity.CLOSED_MEMBER_INSURERS_EQUITY, "310500");
			type.put(Equity.DISTRIBUTIONS_TO_MEMBER_COMPANIES, "310200");
			type.put(Equity.INSOLVENT_MEMBER_COMPANY, "310600");

			carrier.put(Type.Equity, type);

			type = new HashMap<String, String>();
			type.put(Expense.ACTUARIAL_FEES, "775100");
			type.put(Expense.AMORTIZATION_CASH_EQUIVALENT_E, "810200");
			type.put(Expense.AMORTIZATION_FIXED_INCOME_D1, "810000");
			type.put(Expense.AMORTIZATION_SHORT_TERM_DA, "810100");
			type.put(Expense.ARSON_REWARD_FUND, "786500");
			type.put(Expense.AUDIT_TAXES, "775000");
			type.put(Expense.AUTOMOBILE, "761000");
			type.put(Expense.AUTOMOBILE_MAINTENANCE, "761500");
			type.put(Expense.BAD_DEBT_EXPENSE, "918000");
			type.put(Expense.BANK_SERVICE_FEES_INTEREST, "860100");
			type.put(Expense.BOARDS_BUREAUS_ASSOCIATIONS, "710000");
			type.put(Expense.BOARD_MEETING_EXPENSES, "750000");
			type.put(Expense.BOARD_OF_DIRECTOR_TRAVEL, "750100");
			type.put(Expense.BOOKS_SUBSCRIPTIONS, "732000");
			type.put(Expense.CAT_EXPENSE_CLAIMS, "785200");
			type.put(Expense.CHANGE_IN_LAE_RESERVE, "560000");
			type.put(Expense.CHANGE_IN_LOSS_RESERVE, "530000");
			type.put(Expense.COBRA_EXPENSES, "739999");
			type.put(Expense.COMMISSION_EXPENSE, "610000");
			type.put(Expense.COMMISSION_WRITTEN_OFF, "620000");
			type.put(Expense.COMPUTER_HARDWARE, "760700");
			type.put(Expense.CREDIT_CARD_FEES, "782000");
			type.put(Expense.DEPRECIATION_EXPENSE, "762000");
			type.put(Expense.EDUCATION_SEMINARS, "731000");
			type.put(Expense.EDUCATION_SEMINARS_MEALS_ENTERTAINMENT, "731050");
			type.put(Expense.EMPLOYEE_BENEFITS_OTHER, "730400");
			type.put(Expense.FACILITIES_MAINTENANCE, "760400");
			type.put(Expense.FEDERAL_UNEMPLOYMENT_TAX, "721100");
			type.put(Expense.FICA_TAX, "721000");
			type.put(Expense.FLEX_SPENDING_ACCOUNT, "730700");
			type.put(Expense.FURNITURE_EQUIPMENT, "760600");
			type.put(Expense.GENERAL_EXPENSE_ALLOCATION_CLAIMS, "599999");
			type.put(Expense.GENERAL_EXPENSE_ALLOCATION_INVESTMENTS, "899999");
			type.put(Expense.GENERAL_EXPENSE_ALLOCATION_UNDERWRITING, "799999");
			type.put(Expense.HR_INITIATIVES, "733000");
			type.put(Expense.INFORMATION_TECHNOLOGY, "765000");
			type.put(Expense.INSOLVENT_COMPANY_RETURNED_EQUITY, "925000");
			type.put(Expense.INSURANCE_COMPANY_LEVEL, "730600");
			type.put(Expense.INSURANCE_PROPERTY_LIABILITY, "740000");
			type.put(Expense.INTEREST_REFUND, "911000");
			type.put(Expense.INVESTMENT_MANAGEMENT_FEES, "860000");
			type.put(Expense.INVESTMENT_SOFTWARE_EXPENSE, "860400");
			type.put(Expense.INVESTMENT_TRUE_UP, "860500");
			type.put(Expense.LAE, "540000");
			type.put(Expense.LAE_RECOVERY, "550000");
			type.put(Expense.LEASHOLD_IMPROVEMENTS, "760500");
			type.put(Expense.LEGAL_FEES, "775200");
			type.put(Expense.LICENSE_FEES, "780000");
			type.put(Expense.MANAGER_ADVISORY_COUNCIL, "750200");
			type.put(Expense.MEALS_ENTERTAINMENT, "755100");
			type.put(Expense.MEMBERSHIPS_ASSOCIATIONS, "734000");
			type.put(Expense.MISCELLANEOUS_INCOME_EXPENSE, "910000");
			type.put(Expense.OFFICE_SUPPLIES, "771000");
			type.put(Expense.PAID_LOSSES, "510000");
			type.put(Expense.PAYROLL_PROCESSING_EXPENSE, "730500");
			type.put(Expense.POSTAGE, "773000");
			type.put(Expense.PREMIUM_TAXES, "781000");
			type.put(Expense.PREPAID_INTEREST_CASH_EQUIVALENT_E, "820200");
			type.put(Expense.PREPAID_INTEREST_FIXED_INCOME_D1, "820000");
			type.put(Expense.PREPAID_INTEREST_SHORT_TERM_DA, "820100");
			type.put(Expense.PRINTING_DATA_PROCESS, "770100");
			type.put(Expense.PRINTING_GENERAL, "770000");
			type.put(Expense.PRINTING_STATIONARY, "770200");
			type.put(Expense.PROFESSIONAL_SERVICES_OTHER, "775300");
			type.put(Expense.PROPERTY_TAXES, "760100");
			type.put(Expense.RECRUITING_FEES, "730300");
			type.put(Expense.REINSURANCE_EXPENSE_GENERAL, "785000");
			type.put(Expense.REINSURANCE_EXPENSE_LEGAL, "785100");
			type.put(Expense.RENT_EQUIPMENT_OTHER, "760300");
			type.put(Expense.RENT_OFFICE_BUILDING, "760200");
			type.put(Expense.RETIREMENT_401K, "730000");
			type.put(Expense.RETIREMENT_EXPENSE_FEES, "730200");
			type.put(Expense.RETIREMENT_PENSION, "730100");
			type.put(Expense.SALARIES, "720000");
			type.put(Expense.STATE_UMEMPLOYMENT_TAX, "721200");
			type.put(Expense.STORM_READINESS_SEMINARS, "786000");
			type.put(Expense.SUBROGATION, "520000");
			type.put(Expense.SURVEYS_INSPECTIONS, "711000");
			type.put(Expense.TELEPHONE, "772000");
			type.put(Expense.TEMPORARY, "720100");
			type.put(Expense.TRAVEL, "755000");
			type.put(Expense.UTILITIES, "760000");
			type.put(Expense.WORKERS_COMPENSATION, "740100");

			carrier.put(Type.Expense, type);

			type = new HashMap<String, String>();
			type.put(GainLoss.UNASSIGNED_SURPLUS_RETAINED_EARNINGS, "399999");

			carrier.put(Type.GainLoss, type);

			type = new HashMap<String, String>();
			type.put(Income.ACCRUED_INTEREST_DIVIDENDS_CASH_EQUIVALENT_E, "830300");
			type.put(Income.ACCRUED_INTEREST_DIVIDENDS_EQUITY_D2, "830100");
			type.put(Income.ACCRUED_INTEREST_DIVIDENDS_FIXED_INCOME_D1, "830000");
			type.put(Income.ACCRUED_INTEREST_DIVIDENDS_SHORT_TERM_DA, "830200");
			type.put(Income.CEDED_REINSURANCE_PREMIUM_BEACH, "420000");
			type.put(Income.CEDED_REINSURANCE_PREMIUM_COASTAL, "421000");
			type.put(Income.CEDED_REINSURANCE_PREMIUM_FAIR, "423000");
			type.put(Income.CHANGE_IN_UNEARNED_PREMIUM, "430000");
			type.put(Income.FIXED_ASSET_GAIN_LOSS, "917000");
			type.put(Income.GAIN_LOSS_CASH_EQUIVALENT_E, "850300");
			type.put(Income.GAIN_LOSS_EQUITY_D2, "850100");
			type.put(Income.GAIN_LOSS_FIXED_INCOME_D1, "850000");
			type.put(Income.GAIN_LOSS_SHORT_TERM_DA, "850200");
			type.put(Income.INCOME_INFORMATION_REQUEST, "919000");
			type.put(Income.INSUFFICIENT_FUND_FEE, "916000");
			type.put(Income.INTEREST_DIVIDENDS_RECEIVED_CASH_EQUIVALENT_E, "840300");
			type.put(Income.INTEREST_DIVIDENDS_RECEIVED_EQUITY_D2, "840100");
			type.put(Income.INTEREST_DIVIDENDS_RECEIVED_FIXED_INCOME_D1, "840000");
			type.put(Income.INTEREST_DIVIDENDS_RECEIVED_SHORT_TERM_DA, "840200");
			type.put(Income.MISCELLANEOUS_INCOME_SERVICE_CHARGES, "912000");
			type.put(Income.PREMIUM_MISAPPLIED, "914000");
			type.put(Income.PREMIUM_SYSTEM_WRITE_OFF, "915000");
			type.put(Income.PREMIUM_WRITTEN_OFF, "913000");
			type.put(Income.WRITTEN_PREMIUM, "410000");
			type.put(Income.CATASTROPHE_RECOVERY_CHARGE, "489999");

			carrier.put(Type.Income, type);

			type = new HashMap<String, String>();
			type.put(Liability.ACCRUED_PENSION, "260100");
			type.put(Liability.ACCRUED_POST_RETIREMENT, "260200");
			type.put(Liability.ACCRUED_PREMIUM_TAX, "260000");
			type.put(Liability.ACCRUED_RENT, "260300");
			type.put(Liability.A_P_GENERAL, "250000");
			type.put(Liability.A_P_INTERCOMPANY, "250100");
			type.put(Liability.CASE_LAE_RESERVE, "220000");
			type.put(Liability.CASE_LOSS_RESERVE, "210000");
			type.put(Liability.CLAIMS_PAYABLE_CLEARING, "252000");
			type.put(Liability.CLAIMS_PAYABLE_O_S, "253000");
			type.put(Liability.COMMISSION_PAYABLE, "250500");
			type.put(Liability.DEPENDENT_CARE_PAYABLE, "270500");
			type.put(Liability.DISTRIBUTION_PAYABLE, "250600");
			type.put(Liability.EMPLOYEE_SAVING_LOAN_PAYABLE, "270400");
			type.put(Liability.FEDERAL_TAX_PAYABLE, "270100");
			type.put(Liability.FICA_TAX_PAYABLE_EMPLOYEE, "270000");
			type.put(Liability.GARNISHMENT_PAYABLE, "270300");
			type.put(Liability.HEALTH_CARE_PAYABLE, "270600");
			type.put(Liability.IBNR_LAE_RESERVE, "225000");
			type.put(Liability.IBNR_RESERVE, "215000");
			type.put(Liability.ISO_REFUND, "250300");
			type.put(Liability.PAYABLE_FOR_INVESTMENTS, "251000");
			type.put(Liability.POLICYHOLDER_REFUND_NON_ISO, "250400");
			type.put(Liability.PREMIUM_SUSPENSE, "250200");
			type.put(Liability.PREPAID_PREMIUM, "240000");
			type.put(Liability.STATE_TAX_PAYABLE, "270200");
			type.put(Liability.UNCLAIMED_CHECKS_CLAIMS, "261100");
			type.put(Liability.UNCLAIMED_CHECKS_COMMISSION, "261200");
			type.put(Liability.UNCLAIMED_CHECKS_MEMBER_COMPANY, "261300");
			type.put(Liability.UNCLAIMED_CHECKS_REFUND, "261000");
			type.put(Liability.UNEARNED_PREMIUM, "230000");
			type.put(Liability.PREMIUM_CLEARING, "259000");
			type.put(Liability.CLAIMS_CLEARING, "259100");

			carrier.put(Type.Liability, type);

		map.put(GLCarrierCd.NORTH_CAROLINA_JOINT_UNDERWRITING_ASSOCIATION, carrier);
		// @formatter:on
	}
}
