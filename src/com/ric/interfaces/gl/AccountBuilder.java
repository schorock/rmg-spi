package com.ric.interfaces.gl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


public class AccountBuilder {

	public static List<Account> build(final Book book) {
		final List<Account> result = new ArrayList<Account>();
		final Distribution debits = book.debits();
		final Distribution credits = book.credits();
		final Set<String> keys = new TreeSet<String>();

		keys.addAll(debits.keySet());
		keys.addAll(credits.keySet());

		for (final String key : keys) {
			result.add(new Account(key, debits.get(key), credits.get(key)));
		}

		return result;
	}

}
