package com.ric.interfaces.gl;

import java.util.HashMap;
import java.util.Map;

/**
 * @author peter.hose
 *
 * Codes are taken from http://www.iso.com/dcs/downloads/CWAV_2010_UserGuide.pdf
 * http://www.naic.org/documents/industry_pcm_p_c.pdf
 */
public class GLAnnualStatement {
	public static class Line {
		// @formatter:off
		public static final String FIRE                                     = "010";
		public static final String ALLIED_LINES                             = "021";
		public static final String MULTIPLE_PERIL_CROP                      = "022";
		public static final String CRIME                                    = "026";
		public static final String FARMOWNERS_MULTIPLE_PERIL                = "030";
		/**
		 * 040
		 */
		public static final String HOMEOWNERS_MULTIPLE_PERIL                = "040";
		public static final String COMMERCIAL_MULTIPLE_PERIL_NON_LIABILITY  = "051";
		public static final String COMMERCIAL_MULTIPLE_PERIL_LIABILITY      = "052";
		public static final String INLAND_MARINE                            = "090";
		public static final String MEDICAL_MALPRACTICE                      = "110";
		public static final String EARTHQUAKE                               = "120";
		public static final String WORKERS_COMPENSATION                     = "160";
		public static final String OTHER_LIABILITY_OCCURANCE                = "171";
		public static final String OTHER_LIABILITY_CLAIMS_MADE              = "172";
		public static final String EXCESS_WORKERS_COMPENSATION              = "173";
		public static final String PRODUCT_LIABILITY                        = "180";
		public static final String PRIVATE_PASSENGER_AUTO_NO_FAULT          = "191";
		public static final String OTHER_PRIVATE_PASSENGER_AUTO_LIABILITY   = "192";
		public static final String COMMERCIAL_AUTO_NO_FAULT                 = "193";
		public static final String OTHER_COMMERCIAL_AUTO_LIABILITY          = "194";
		public static final String PRIVATE_PASSENGER_AUTO_PHYSICAL_DAMAGE   = "211";
		public static final String COMMERCIAL_AUTO_PHYSICAL_DAMAGE          = "212";
		public static final String BURGLARY_AND_THEFT                       = "260";
		public static final String BOILER_AND_MACHINERY                     = "270";
		public static final String WARRANTY                                 = "300";
		public static final String AGGREGATE_WRITE_INS_FOR_OTHER_LINES      = "340";
		public static final String UNKNOWN                                  = "999";
		// @formatter:on

		public static class Index {
			// @formatter:off
			static final int _UNKNOWN                                 =   0;
			static final int FIRE                                     =  10;
			static final int ALLIED_LINES                             =  21;
			static final int MULTIPLE_PERIL_CROP                      =  22;
			static final int CRIME                                    =  26;
			static final int FARMOWNERS_MULTIPLE_PERIL                =  30;
			static final int HOMEOWNERS_MULTIPLE_PERIL                =  40;
			static final int COMMERCIAL_MULTIPLE_PERIL_NON_LIABILITY  =  51;
			static final int COMMERCIAL_MULTIPLE_PERIL_LIABILITY      =  52;
			static final int INLAND_MARINE                            =  90;
			static final int MEDICAL_MALPRACTICE                      = 110;
			static final int EARTHQUAKE                               = 120;
			static final int WORKERS_COMPENSATION                     = 160;
			static final int OTHER_LIABILITY_OCCURANCE                = 171;
			static final int OTHER_LIABILITY_CLAIMS_MADE              = 172;
			static final int EXCESS_WORKERS_COMPENSATION              = 173;
			static final int PRODUCT_LIABILITY                        = 180;
			static final int PRIVATE_PASSENGER_AUTO_NO_FAULT          = 191;
			static final int OTHER_PRIVATE_PASSENGER_AUTO_LIABILITY   = 192;
			static final int COMMERCIAL_AUTO_NO_FAULT                 = 193;
			static final int OTHER_COMMERCIAL_AUTO_LIABILITY          = 194;
			static final int PRIVATE_PASSENGER_AUTO_PHYSICAL_DAMAGE   = 211;
			static final int COMMERCIAL_AUTO_PHYSICAL_DAMAGE          = 212;
			static final int BURGLARY_AND_THEFT                       = 260;
			static final int BOILER_AND_MACHINERY                     = 270;
			static final int WARRANTY                                 = 300;
			static final int AGGREGATE_WRITE_INS_FOR_OTHER_LINES      = 340;
			static final int UNKNOWN                                  = 999;
			// @formatter:on

			public static int get(final String text) {
				return Integer.parseInt(text);

				//				Integer result = index.get(text);
				//
				//				return (result == null) ? _UNKNOWN : result.intValue();
			}

			private static final Map<String, Integer> index = new HashMap<String, Integer>();

			static {
				index.put(Line.FIRE, FIRE);
				index.put(Line.ALLIED_LINES, ALLIED_LINES);
				index.put(Line.MULTIPLE_PERIL_CROP, MULTIPLE_PERIL_CROP);
				index.put(Line.CRIME, CRIME);
				index.put(Line.FARMOWNERS_MULTIPLE_PERIL, FARMOWNERS_MULTIPLE_PERIL);
				index.put(Line.HOMEOWNERS_MULTIPLE_PERIL, HOMEOWNERS_MULTIPLE_PERIL);
				index.put(Line.COMMERCIAL_MULTIPLE_PERIL_NON_LIABILITY, COMMERCIAL_MULTIPLE_PERIL_NON_LIABILITY);
				index.put(Line.COMMERCIAL_MULTIPLE_PERIL_LIABILITY, COMMERCIAL_MULTIPLE_PERIL_LIABILITY);
				index.put(Line.INLAND_MARINE, INLAND_MARINE);
				index.put(Line.MEDICAL_MALPRACTICE, MEDICAL_MALPRACTICE);
				index.put(Line.EARTHQUAKE, EARTHQUAKE);
				index.put(Line.WORKERS_COMPENSATION, WORKERS_COMPENSATION);
				index.put(Line.OTHER_LIABILITY_OCCURANCE, OTHER_LIABILITY_OCCURANCE);
				index.put(Line.OTHER_LIABILITY_CLAIMS_MADE, OTHER_LIABILITY_CLAIMS_MADE);
				index.put(Line.EXCESS_WORKERS_COMPENSATION, EXCESS_WORKERS_COMPENSATION);
				index.put(Line.PRODUCT_LIABILITY, PRODUCT_LIABILITY);
				index.put(Line.PRIVATE_PASSENGER_AUTO_NO_FAULT, PRIVATE_PASSENGER_AUTO_NO_FAULT);
				index.put(Line.OTHER_PRIVATE_PASSENGER_AUTO_LIABILITY, OTHER_PRIVATE_PASSENGER_AUTO_LIABILITY);
				index.put(Line.COMMERCIAL_AUTO_NO_FAULT, COMMERCIAL_AUTO_NO_FAULT);
				index.put(Line.OTHER_COMMERCIAL_AUTO_LIABILITY, OTHER_COMMERCIAL_AUTO_LIABILITY);
				index.put(Line.PRIVATE_PASSENGER_AUTO_PHYSICAL_DAMAGE, PRIVATE_PASSENGER_AUTO_PHYSICAL_DAMAGE);
				index.put(Line.COMMERCIAL_AUTO_PHYSICAL_DAMAGE, COMMERCIAL_AUTO_PHYSICAL_DAMAGE);
				index.put(Line.BURGLARY_AND_THEFT, BURGLARY_AND_THEFT);
				index.put(Line.BOILER_AND_MACHINERY, BOILER_AND_MACHINERY);
				index.put(Line.WARRANTY, WARRANTY);
				index.put(Line.AGGREGATE_WRITE_INS_FOR_OTHER_LINES, AGGREGATE_WRITE_INS_FOR_OTHER_LINES);
			}
		}
	}
}
