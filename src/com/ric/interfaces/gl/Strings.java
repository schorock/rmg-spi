package com.ric.interfaces.gl;


import com.ric.interfaces.gl.Perl;

public final class Strings {

	public static class Traits {

		public static boolean isNull(final String value) {
			return (value == null);
		}

		public static boolean isNullOrEmpty(final String value) {
			return (value == null || value.isEmpty());
		}
	}

	/**
	 * Convert null to empty
	 *
	 * @param value is a string or null value
	 * @return the original value or an empty string
	 */
	public static String unnulled(final String value) {
		return (value != null) ? value : "";
	}

	public static String rpad(final String s, final int n) {
		return String.format("%1$-" + n + "s", s);
	}

	public static String lpad(final String s, final int n) {
		return String.format("%1$" + n + "s", s);
	}

	public static String rpad(final String s, final char c, final int n) {
		return s + repeat(c, n - s.length());
	}
	
	public static String lpad(final String s, final char c, final int n) {
		return repeat(c, n - s.length()) + s;
	}

	public static String repeat(final char c, final int n) {
		final StringBuilder sb = new StringBuilder(n);

		for (int index = 0, count = n; index < count; ++index)
			sb.append(c);

		return sb.toString();
	}

	public static String repeat(final String s, final int n) {
		final StringBuilder sb = new StringBuilder(n);

		for (int index = 0, count = n; index < count; ++index)
			sb.append(s);

		return sb.toString();
	}

	public static String truncate(final String value, final int length) {
		String result = value;

		if (value.length() > length)
			result = result.substring(0, length);

		return result;
	}

	/**
	 * PERL like join method
	 *
	 * @param separator
	 * @param strings
	 * @return
	 */
	public static String join(final String separator, final String... strings) {
		return Perl.join(separator, strings);
	}

	public static String join(final char separator, final String... strings) {
		return Perl.join(separator, strings);
	}
}
