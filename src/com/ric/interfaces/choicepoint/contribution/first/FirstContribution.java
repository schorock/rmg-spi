package com.ric.interfaces.choicepoint.contribution.first;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import net.inov.biz.server.ServiceContext;
import net.inov.mda.MDAOption;
import net.inov.mda.MDATable;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.math.Money;
import net.inov.tec.obj.ClassHelper;

import com.innovextechnology.service.Log;
import com.iscs.common.business.company.Company;
import com.iscs.common.business.provider.Provider;
import com.iscs.common.mda.Store;
import com.iscs.common.render.DateRenderer;
import com.iscs.common.shared.address.AddressTools;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.ChangeInfo;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.interfaces.choicepoint.contribution.ChoicePointContributionHelper;
import com.ric.interfaces.choicepoint.contribution.UWPolicyInformation;
import com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.CurrentCarrierContribution;
import com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.entity.CurrentCarrierInfo;
import com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.entity.CurrentCarrierPolicyInfo;
import com.iscs.uw.interfaces.choicepoint.contribution.first.FirstContributionMapHandler;
import com.iscs.uw.interfaces.choicepoint.contribution.first.entity.FirstFinanceInfo;
import com.iscs.uw.interfaces.choicepoint.contribution.first.entity.FirstPolicyInfo;
import com.iscs.uw.interfaces.choicepoint.contribution.first.entity.FirstPropertyInfo;
import com.iscs.uw.policy.PM;

/** The main class that will process Policy data into CurrentCarrier FIRSt information.  This
 * information can then be saved to the database.  
 * 
 * @author allend
 *
 */
public class FirstContribution extends com.iscs.uw.interfaces.choicepoint.contribution.first.FirstContribution {

	private FirstContributionMapHandler mapper = null;
	private boolean isAutoPolicy = false;
	 protected ModelBean errors = null;
	    public void setErrors(ModelBean errors) {
	    	this.errors = errors;
	    }
	public FirstContribution() throws Exception {		
		// Obtain the Contribution Class which will map Policy data into Contribution data
        String repositoryName = "::first-elios-codes::Mapper::all";
        MDATable table = (MDATable) Store.getModelObject(repositoryName);
        MDAOption option = table.getOption("handler");
        String handler = option.getLabel();
        Object obj = ClassHelper.loadObject(handler);
        mapper = (FirstContributionMapHandler) obj;
	}
	
	/** Append the FIRSt information to the information on the Current Carrier contribution.
	 * @param currentCarrierInfo The Current Carrier Information that will be appended with FIRSt information
	 * @param policy The Policy ModelBean
	 * @param account The Account ModelBean
	 * @param todayDt The current date
	 * @param isInitialContributionInd Indicator if this is an Initial Contribution
	 * @throws Exception when an error occurs
	 */
	public void createContribution(CurrentCarrierInfo currentCarrierInfo, UWPolicyInformation policyInfo, StringDate todayDt, boolean isInitialContributionInd,String payoffAmt,String transactionCd, String paymentDueDate) throws Exception 
	 {
	try{
		Money premiumInstallAmt = policyInfo.getPremiumInstallAmt();
		Money premiumIncreaseAmt = policyInfo.getPremiumIncreaseAmt();
		
		ModelBean policy = policyInfo.getPolicy();
		ModelBean basicPolicy = policy.getBean("BasicPolicy");
		String payPlanCd = basicPolicy.gets("PayPlanCd");
		
		CurrentCarrierPolicyInfo currentCarrierPolicyInfo = currentCarrierInfo.getPolicy();
		
		if (currentCarrierPolicyInfo.getInsuranceType().equals("PA") || currentCarrierPolicyInfo.getInsuranceType().equals("CA") ) {
			isAutoPolicy = true;
			premiumInstallAmt= new Money("0.00");
			premiumIncreaseAmt= new Money("0.00");
		}else {
			isAutoPolicy = false;
		}
			
		currentCarrierPolicyInfo.setFirstPolicyInfo(createPolicyInfo(policy, premiumInstallAmt, premiumIncreaseAmt, currentCarrierInfo.getLine()));
		
		// Create a property record for each policy risk or vehicle in the Current Carrier list
		HashMap<String, String> riskMap = policyInfo.getRiskList();

		// Sort the keys (Property Id's)
		Vector ids = new Vector(riskMap.keySet());
		Collections.sort(ids);		
		
		Iterator propertyIds = ids.iterator();		
		ArrayList<FirstPropertyInfo> properties = new ArrayList<FirstPropertyInfo>();
		boolean hasMultipleFirstMortgagees = false;
		int hasMultipleFirstMortgageesCount = 0;
		while (propertyIds.hasNext()) {
			String key = (String) propertyIds.next();
			int propertyId = 0;
			if (isAutoPolicy) {
				if (!key.startsWith("Auto-")) {
					continue;					
				}
				String riskNo = key.substring("Auto-".length());
				propertyId = Integer.parseInt(riskNo);
			} else {
				if (!key.startsWith("Property-")) {
					continue;
				}
				String riskNo = key.substring("Property-".length());
				propertyId = Integer.parseInt(riskNo);
			}
			String beanId = riskMap.get(key);
			ModelBean risk = policy.getBeanById("Risk", beanId);
			ArrayList<ModelBean> ais = new ArrayList<ModelBean>();
			//Only first of all the first mortgagees added across multiple risks should get the bill (NBB) rest all mortgagees should not get the bill (NBS)
			ArrayList<ModelBean> firstMortgageeAis = getAllFirstMortgageeAIS(policy);
			ArrayList<ModelBean> firstMortgageeAisSorted = new ArrayList<ModelBean>();
			if(firstMortgageeAis.size() > 1 && (payPlanCd.contains("Mortgagee"))){
				hasMultipleFirstMortgagees = true;
				firstMortgageeAisSorted =  getFirstMortgageeAISorted(policy,risk);
				ArrayList<ModelBean> otherMortgageeAis = getMortgageeAIS(policy, risk,payPlanCd);
				ais.addAll(firstMortgageeAisSorted);
				ais.addAll(otherMortgageeAis);
				
			}else{
				ais = getMortgageeAIS(policy, risk,"");
			}
			int aiCount = 0;
				if(ais.size() == 0)	{
					FirstPropertyInfo property = createPropertyInfo(policyInfo, currentCarrierPolicyInfo, risk, propertyId,payoffAmt,transactionCd,paymentDueDate,aiCount,null,false,0);
		    		properties.add(property);
		    	}else{
		    		
		    		for (ModelBean additionalInt : ais) {
		    			if(firstMortgageeAisSorted.size() > 0){
			    			hasMultipleFirstMortgageesCount++;
			    		}
		    			aiCount++;
		    			FirstPropertyInfo property = createPropertyInfo(policyInfo, currentCarrierPolicyInfo, risk, propertyId,payoffAmt,transactionCd,paymentDueDate,aiCount,additionalInt,hasMultipleFirstMortgagees,hasMultipleFirstMortgageesCount);
						properties.add(property);
					}
		    	}
		}
		currentCarrierPolicyInfo.setFirstProperties(properties);
		
		// Notification Action Code & Date - In Initial extract, all Notification Action Codes are to be '999' and the Notification Action Date is to be submitted with all blank
		if(isInitialContributionInd){
			List<FirstPropertyInfo> firstProperties = currentCarrierPolicyInfo.getFirstProperties();
			int j=0;
			while ( j < firstProperties.size()) {
			
				FirstPropertyInfo propertyInfo = firstProperties.get(j);
				propertyInfo.setNotificationActionCd("999");
				propertyInfo.setNotificationActionDt(null);
				j++;
			}
			currentCarrierPolicyInfo.setFirstProperties(firstProperties);
		}
	}catch(Exception e){
		Log.error("Error processing policy with policy number "+policyInfo.getPolicy().getBean("BasicPolicy").gets("PolicyNumber"));
		e.printStackTrace();
		ModelBean error = ErrorTools.createError("ERROR", "Unable to process Policy with PolicyNmber with "+ policyInfo.getPolicy().getBean("BasicPolicy").gets("PolicyNumber")  +" because of exception: " + e, ErrorTools.GENERIC_BUSINESS_ERROR,ErrorTools.SEVERITY_ERROR);
		errors.addValue(error);
	}
}
	
	protected FirstPolicyInfo createPolicyInfo(ModelBean policy, Money premiumInstallAmt, Money premiumIncreaseAmt, ModelBean line) throws Exception 
	{

		FirstPolicyInfo policyInfo = new FirstPolicyInfo();
		ModelBean basicPolicy = policy.getBean("BasicPolicy");
		JDBCData data = ServiceContext.getServiceContext().getData();
		ModelBean provider = Provider.getProviderBySystemId(data, basicPolicy.getValueInt("ProviderRef"));
		ModelBean partyInfo = provider.getBean("ProviderAcct").getBean("PartyInfo");
		String fullName = partyInfo.getBean("NameInfo").gets("CommercialName");
		String subTypeCd = policy.getBean("BasicPolicy").gets("SubTypeCd");
		String[] nameParts = fullName.split(" ");
		if (nameParts.length > 1) {
			policyInfo.setAgentGivenName(nameParts[0].trim());
			policyInfo.setAgentSurname(fullName.substring(nameParts[0].length()).trim());
		} else {
			policyInfo.setAgentGivenName(fullName);
		}

		ModelBean mailingLookupAddr = partyInfo.getBean("Addr", "AddrTypeCd", "AcctMailingLookupAddr");
		if (mailingLookupAddr == null || !AddressTools.hasParsedData(mailingLookupAddr)) {
			ModelBean mailingAddr = partyInfo.getBean("Addr", "AddrTypeCd", "AcctMailingAddr");
			mailingLookupAddr = ChoicePointContributionHelper.validateAddress(mailingAddr);
		}
		policyInfo.setAgentMailingPrimaryNumber(mailingLookupAddr.gets("PrimaryNumber"));
		policyInfo.setAgentMailingStreetName(StringTools.concat(mailingLookupAddr.gets("StreetName"), mailingLookupAddr.gets("Suffix"), " ").trim());
		policyInfo.setAgentMailingSecondaryNumber(mailingLookupAddr.gets("SecondaryNumber"));
		policyInfo.setAgentMailingCity(mailingLookupAddr.gets("City"));
		policyInfo.setAgentMailingStateProvCd(mailingLookupAddr.gets("StateProvCd"));
		policyInfo.setAgentMailingPostalCode(AddressTools.getZip5(mailingLookupAddr.gets("PostalCode")));
		policyInfo.setAgentMailingPostalCodeExt(AddressTools.getZip4(mailingLookupAddr.gets("PostalCode"), null));
		// P.O Box edit 
		if (ChoicePointContributionHelper.isPOBox(mailingLookupAddr)) {
			policyInfo.setAgentMailingPrimaryNumber("");
			policyInfo.setAgentMailingStreetName(mailingLookupAddr.gets("Addr1"));
		}
		String parsedPhone = CurrentCarrierContribution.getParsedPhoneNumber(partyInfo);
		if (parsedPhone.length() >= 10) {
			policyInfo.setAgentTelephoneAreaCode(parsedPhone.substring(0, 3));
			policyInfo.setAgentTelephoneNumber(parsedPhone.substring(3, 10));
			policyInfo.setAgentTelephoneExt(parsedPhone.substring(10));
		} else {
			// Get phone from ProviderParty if the phone from the AcctInfo is empty
			ModelBean providerPartyInfo = provider.getBean("PartyInfo");
			if( providerPartyInfo != null ) {
				parsedPhone = CurrentCarrierContribution.getParsedPhoneNumber(providerPartyInfo);
				if (parsedPhone.length() >= 10) {
					policyInfo.setAgentTelephoneAreaCode(parsedPhone.substring(0, 3));
					policyInfo.setAgentTelephoneNumber(parsedPhone.substring(3, 10));
					policyInfo.setAgentTelephoneExt(parsedPhone.substring(10));
				}
			}
		}

		ModelBean remitInfo = getRemittanceInfo();
		ModelBean payToNameInfo = remitInfo.findBeanByFieldValue("NameInfo", "NameTypeCd", "PayToName");
		if(payToNameInfo != null){
			policyInfo.setPayableTo(payToNameInfo.gets("CommercialName"));
		}


		ModelBean remitLookupAddr = remitInfo.getBean("Addr", "AddrTypeCd", "CompanyMailingLookupAddr");
		if (remitLookupAddr == null || !AddressTools.hasParsedData(remitLookupAddr)) {
			ModelBean mailingAddr = remitInfo.getBean("Addr", "AddrTypeCd", "CompanyMailingAddr");
			remitLookupAddr = ChoicePointContributionHelper.validateAddress(mailingAddr);
		}
		policyInfo.setRemitMailingPrimaryNumber(remitLookupAddr.gets("PrimaryNumber"));
		policyInfo.setRemitMailingStreetName(StringTools.concat(remitLookupAddr.gets("StreetName"), remitLookupAddr.gets("Suffix"), " ").trim());
		policyInfo.setRemitMailingSecondaryNumber(remitLookupAddr.gets("SecondaryNumber"));
		policyInfo.setRemitMailingCity(remitLookupAddr.gets("City"));
		policyInfo.setRemitMailingStateProvCd(remitLookupAddr.gets("StateProvCd"));
		policyInfo.setRemitMailingPostalCode(AddressTools.getZip5(remitLookupAddr.gets("PostalCode")));
		policyInfo.setRemitMailingPostalCodeExt(AddressTools.getZip4(remitLookupAddr.gets("PostalCode"), null));
		// P.O Box edit 
		if (ChoicePointContributionHelper.isPOBox(remitLookupAddr)) {
			policyInfo.setRemitMailingPrimaryNumber("");
			policyInfo.setRemitMailingStreetName(remitLookupAddr.gets("Addr1"));
		}
		String remitParsedPhone = CurrentCarrierContribution.getParsedPhoneNumber(remitInfo);
		if (remitParsedPhone.length() >= 10) {
			policyInfo.setRemitTelephoneAreaCode(remitParsedPhone.substring(0, 3));
			policyInfo.setRemitTelephoneNumber(remitParsedPhone.substring(3, 10));
			policyInfo.setRemitTelephoneExt(remitParsedPhone.substring(10));
		}

		policyInfo.setPremiumInstallAmt(premiumInstallAmt.toString());
		policyInfo.setPremiumIncreaseAmt(premiumIncreaseAmt.toString());

		if(StringTools.contains(subTypeCd, "WD") || StringTools.contains(subTypeCd, "HW"))
			policyInfo.setNotificationPolicyType("WIND");	
		else
			policyInfo.setNotificationPolicyType(mapper.mapPropertyNotificationType(policy, line));

		return policyInfo;

	}	
	/** Create the FIRSt property information.  This contains the information needed to 
	 * notify a Mortgagee or Loss Payee on the risk.  Certain information are required for pure
	 * information notification, and others are for escrow billing.
	 * @param policyInfo The Policy Information that is shared with Current Carrier and FIRSt about the current policy
	 * @param currentCarrierInfo The Current Carrier Information used to locate previous Policy information.
	 * @param risk The Risk ModelBean 
	 * @param propertyId The property Id used to group Current Carrier / FIRSt records
	 * @return FIRSt property information
	 * @throws Exception when an error occurs
	 */
	protected FirstPropertyInfo createPropertyInfo(UWPolicyInformation policyInfo, CurrentCarrierPolicyInfo currentCarrierInfo, ModelBean risk, int propertyId,String payoffAmt,String transactionCd,String paymentDueDate,int aiCount,ModelBean ai,boolean hasMultipleFirstMortgagees,int hasMultipleFirstMortgageesCount) throws Exception 
			 {
		
		FirstPropertyInfo propertyInfo = new FirstPropertyInfo();
		
		ModelBean policy = policyInfo.getPolicy();
		ModelBean basicPolicy = policy.getBean("BasicPolicy");
		String finalPremiumAmt = basicPolicy.gets("FinalPremiumAmt");
		String payPlanCd = basicPolicy.gets("PayPlanCd");
		ModelBean carrier = Company.getCompany().getBean().getBeanById("Carrier", basicPolicy.gets("CarrierCd"));
		StringDate policyEffectiveDt = basicPolicy.getDate("EffectiveDt", null);
		if (isAutoPolicy) {
			ModelBean vehicle = risk.getBean("Vehicle");
	    	if (vehicle == null) 
	    		return null;
	    	propertyInfo.setVehIdentificationNumber(vehicle.gets("VehIdentificationNumber"));		
		} 
		
		// Always set propertyId
		propertyInfo.setPropertyId(Integer.toString(propertyId));

    	// Mortgagee or LienHolder check, else set to 999
    	if(aiCount == 0) {
    		propertyInfo.setNotificationActionCd("999");
    	} else {
    		propertyInfo.setNotificationActionCd(getNotificationAction(policyInfo, risk,aiCount,ai,hasMultipleFirstMortgagees,hasMultipleFirstMortgageesCount));
    	}
    	// add notes
    	if(StringTools.in(propertyInfo.getNotificationActionCd(), "RWL,RWB")){
    		propertyInfo.setNotes("This policy is not effective until premium payment is received by the "+carrier.gets("id") + " on or before "+policyEffectiveDt);
    	}
		// Set the Notification Action Date to null, if no notification is required
    	if(propertyInfo.getNotificationActionCd().equals("999")) {
    		propertyInfo.setNotificationActionDt(null);
    	} else {
    		if(propertyInfo.getNotificationActionCd().endsWith("B")){
    			if(paymentDueDate!=null){
    				propertyInfo.setNotificationActionDt(DateRenderer.getStringDate(paymentDueDate));
    			}else{
    				propertyInfo.setNotificationActionDt(null);
    			}
    		}else
    			propertyInfo.setNotificationActionDt(getCurrentTransactionDt(policyInfo, policy));
    	}
		propertyInfo.setEndorsementStateProvCd(basicPolicy.gets("ControllingStateCd"));
    	// Set all of the additional interest information except the first one, which is taken care of in Current Carrier
		if(ai!=null){
    		ArrayList<FirstFinanceInfo> financeList = new ArrayList<FirstFinanceInfo>();
    			FirstFinanceInfo financeInfo = new FirstFinanceInfo();
        		String typeCd = mapper.mapFinanceType(ai.gets("InterestTypeCd"), isAutoPolicy);
        		financeInfo.setFinanceCompanyName(ai.gets("InterestName"));
        		financeInfo.setFinanceCompanyType(typeCd);
        		
        		ModelBean lookupAddr = ai.getBean("Addr", "AddrTypeCd", "AIMailingLookupAddr");
        		if (lookupAddr == null || !AddressTools.hasParsedData(lookupAddr)) {
        			lookupAddr = ai.getBean("Addr", "AddrTypeCd", "AIMailingAddr");
        			lookupAddr = ChoicePointContributionHelper.validateAddress(lookupAddr);
        		}    		
        		financeInfo.setFinanceCompanyPrimaryNumber(lookupAddr.gets("PrimaryNumber"));
        		financeInfo.setFinanceCompanyStreetName(StringTools.concat(lookupAddr.gets("StreetName"), lookupAddr.gets("Suffix"), " ").trim());
        		financeInfo.setFinanceCompanySecondaryNumber(lookupAddr.gets("SecondaryNumber"));
        		financeInfo.setFinanceCompanyCity(lookupAddr.gets("City"));
        		financeInfo.setFinanceCompanyStateProvCd(lookupAddr.gets("StateProvCd"));
        		financeInfo.setFinanceCompanyPostalCode(AddressTools.getZip5(lookupAddr.gets("PostalCode")));
        		financeInfo.setFinanceCompanyPostalCodeExt(AddressTools.getZip4(lookupAddr.gets("PostalCode"), null));
        		// P.O Box edit 
        		if (ChoicePointContributionHelper.isPOBox(lookupAddr)) {
        			financeInfo.setFinanceCompanyPrimaryNumber("");
        			financeInfo.setFinanceCompanyStreetName(lookupAddr.gets("Addr1"));
        		}
        		financeInfo.setFinanceCompanyLoanNumber(ai.gets("AccountNumber"));
        		
        		financeInfo.setNotificationActionCd(getNotificationAction(policyInfo, risk,aiCount,ai,hasMultipleFirstMortgagees,hasMultipleFirstMortgageesCount));
        		financeInfo.setNotificationActionDt(getCurrentTransactionDt(policyInfo, policy));
        		// Set Premium Amount
        		if(payPlanCd.matches(".*Mortgagee.*") && !transactionCd.equalsIgnoreCase("Reinstatement") && !transactionCd.equalsIgnoreCase("Reinstatement With Lapse")){
	        		if(payoffAmt!="" && payoffAmt!=null)
	        			financeInfo.setFinanceCompanyPremiumAmtDue(payoffAmt);
	        		else
	        			financeInfo.setFinanceCompanyPremiumAmtDue(finalPremiumAmt);
        		}else{
        			financeInfo.setFinanceCompanyPremiumAmtDue("");
        		}
        		ModelBean[] billingEntities = policy.findBeansByFieldValue("BillingEntity", "StatusCd", "Active");   
        		for(ModelBean billingEntity:billingEntities){
        			String aiRef= billingEntity.gets("SourceIdRef");
        			if(!aiRef.equals("")){
        				if(ai.getId().equals(aiRef)){ // Check to see if Billing Entity is created from This Additional Interest
        					if(policyInfo.getAccount()!=null && policyInfo.getAccount().getSystemId().equalsIgnoreCase(billingEntity.gets("AccountRef"))){
        						financeInfo.setFinanceCompanyPremiumAmtDue(policyInfo.getPremiumInstallAmt().toString());        						
        					}
        				}
        			}
        		}
        		financeList.add(financeInfo);
        	propertyInfo.setFinancers(financeList);
    	}    	
		
		return propertyInfo;
		
	}
	
	
    /** Get the current transaction date.
     * @param policy The Policy ModelBean
     * @return The current transaction date
     * @throws Exception when an error occurs
     */
    protected StringDate getCurrentTransactionDt(UWPolicyInformation policyInfo, ModelBean policy)
    throws Exception {
		// Obtain the correct transaction effective date
    	ModelBean transaction = null;
    	if (policyInfo.getTransactionNumber() != null && policyInfo.getTransactionNumber().length() > 0)
    		transaction = getTransaction(policy, policyInfo.getTransactionNumber());
    	else
    		transaction = getCurrentTransaction(policy);
		return transaction.getDate("TransactionEffectiveDt");
    }
    
    protected StringDate getTransactionDt(UWPolicyInformation policyInfo, ModelBean policy)
	    throws Exception {
			// Obtain the correct transaction effective date
	    	ModelBean transaction = null;
	    	if (policyInfo.getTransactionNumber() != null && policyInfo.getTransactionNumber().length() > 0)
	    		transaction = getTransaction(policy, policyInfo.getTransactionNumber());
	    	else
	    		transaction = getCurrentTransaction(policy);
			return transaction.getDate("TransactionDt");
	    }
     
	
    
    /** Obtain the notification code for the current Policy risk.  The notification code
     * determines the action that ChoicePoint will take with this FIRSt information.  The
     * two main types are informational and escrow billing
     * @param policyInfo The Policy state and other information
     * @param risk The Risk ModelBean
     * @return The notification code
     * @throws Exception when an error occurs
     */
    private String getNotificationAction(UWPolicyInformation policyInfo, ModelBean risk,int aiCount,ModelBean ai,boolean hasMultipleFirstMortgagees,int hasMultipleFirstMortgageesCount) throws Exception 
    {
    	
    	// Determine Policy Level status
    	ModelBean policy = policyInfo.getPolicy();
    	ModelBean basicPolicy = policy.getBean("BasicPolicy");
    	String payplanCd = 	basicPolicy.gets("PayPlanCd");
    	ModelBean transaction = null;
    	if (policyInfo.getTransactionNumber() != null && policyInfo.getTransactionNumber().length() > 0)
    		transaction = getTransaction(policy, policyInfo.getTransactionNumber());
    	else
    		transaction = getCurrentTransaction(policy);
    	String transactionCd = transaction.gets("TransactionCd");
    	String policyNotification = "999";
    	if (transactionCd.equals(PM.TX_NEW_BUSINESS)) {
    		if(hasMultipleFirstMortgagees){
    			if(hasMultipleFirstMortgageesCount == 1)
    				policyNotification = "NBB";
    			else
    				policyNotification = "NBS";
    		}else{
    			if (policyInfo.hasEscrowBilling(payplanCd) && aiCount == 1)
        			policyNotification = "NBB";
        		else
        			policyNotification = "NBS";
    		}
    		 
    	} else if (transactionCd.equals(PM.TX_RENEWAL) || transactionCd.equals(PM.TX_RENEWAL_START)) {
    		if(hasMultipleFirstMortgagees){
    			if(hasMultipleFirstMortgageesCount == 1)
    				policyNotification = "RWB";
    			else
    				policyNotification = "RWL";
    		}else{
	    		if (policyInfo.hasEscrowBilling(payplanCd)  && aiCount == 1)
	    			policyNotification = "RWB";
	    		else
	    			policyNotification = "RWL";
    		}
    	} else if (transactionCd.equals(PM.TX_NON_RENEWAL)) {
    		policyNotification = "RWX";
    	} else if (transactionCd.equals(PM.TX_REINSTATEMENT) || transactionCd.equals(PM.TX_REINSTATEMENT_WITH_LAPSE)) {
    		if (policyInfo.getPolicyStatus().equals(UWPolicyInformation.REINSTATED_STATUS))
    			policyNotification = "REI";
    	} else if (transactionCd.equals(PM.TX_NON_RENEWAL_RESCIND)) {
    		policyNotification = "RNR";
    	} else if (transactionCd.equals(PM.TX_UNAPPLY)) {
    		policyNotification = "999";
    	} else if (transactionCd.equals(PM.TX_EXPIRE)) {
    		policyNotification = "XLO";
    	} else if (transactionCd.equals(PM.TX_CANCELLATION_NOTICE)) {
    		// Per LexisNexis, Use ITC (Intent to Cancel) for Cancellation Notice transaction
    		policyNotification = "ITC";
    	} else if (transactionCd.equals(PM.TX_CANCELLATION)) {
    		if (policyInfo.getCancellationReason().equals(UWPolicyInformation.CX_REASON_COMPANY)) {    			
        		if (policyInfo.hasEscrowBilling(payplanCd))
        			policyNotification = "XOB";
        		else
        			policyNotification = "XLO";
    		} else if (policyInfo.getCancellationReason().equals(UWPolicyInformation.CX_REASON_INSURED)) {
        		if (policyInfo.hasEscrowBilling(payplanCd))
        			policyNotification = "XQB";
        		else
        			policyNotification = "XLQ";
    		} else { 
        			policyNotification = "XLN";
    		}
    	} else if (transactionCd.equals(PM.TX_REWRITE_NEW) || transactionCd.equals(PM.TX_REWRITE_RENEWAL)) {
    		policyNotification = "REW";
    	} else if(transactionCd.equals(PM.TX_ENDORSEMENT) ){
    		if(hasMultipleFirstMortgagees){
    			if(hasMultipleFirstMortgageesCount == 1)
    				policyNotification = "PCB";
    			else
    				policyNotification = "PCH";
    		}else{
	    		if (policyInfo.hasEscrowBilling(payplanCd) && aiCount==1) {
	    			policyNotification = "PCB";
	    		} else {
	    			policyNotification = "PCH";
	    			// For autos, expand and determine detail level changes
	    			if (isAutoPolicy) {
	        			ModelBean changeInfo = policy.findBeanById("ChangeInfo", transaction.gets("ChangeInfoRef"));        			
	        			if( changeInfo != null ){        			
		        			ModelBean modInfo = ChangeInfo.findModInfoByIdRefAndField(changeInfo, risk.getBean("Vehicle").getBean("DriverLink").getId(), "DriverRef");
		        			if (modInfo != null) {
		        				policyNotification = "DRC";
		        			}
		        			ModelBean[] modInfos = changeInfo.findBeansByFieldValue("ModInfo", "PreviousValue", risk.getId());
		        			for (ModelBean mod : modInfos) {
		        				if (mod.gets("IdRef").matches("LinkReference.*")) {
		        					policyNotification = "LNC";
		        					break;
		        				}
		        			}
	        			}
	    			}    			
	    		}
    		}
    	}
    	
    	return policyNotification; 
    }
    public ArrayList<ModelBean> getMortgageeAIS(ModelBean policy, ModelBean risk,String payPlanCd) throws Exception {
    	ModelBean[] ais = com.iscs.uw.common.shared.AI.getAllAI(policy, risk);
    	ArrayList<ModelBean> otherMortgageeAIs = new ArrayList<ModelBean>();
    	for (ModelBean ai : ais) {
    		if(payPlanCd.contains("Mortgagee")){
	    		if(ai.gets("InterestTypeCd").matches(".*Mortgagee.*") && !ai.gets("InterestTypeCd").equalsIgnoreCase("First Mortgagee") ){
	    			otherMortgageeAIs.add(ai);
	    		}
    		}else{
    			if(ai.gets("InterestTypeCd").matches(".*Mortgagee.*")){
    				otherMortgageeAIs.add(ai);
        		}
    		}
    	}
    	return otherMortgageeAIs;
    }
    public ArrayList<ModelBean> getAllFirstMortgageeAIS(ModelBean policy) throws Exception {
    	ModelBean[] ais = com.iscs.uw.common.shared.AI.getAllActiveAI(policy);
    	ArrayList<ModelBean> firstMortgageeAIs = new ArrayList<ModelBean>();
    	for (ModelBean ai : ais) {
    		if(ai.gets("InterestTypeCd").equalsIgnoreCase("First Mortgagee")){
    			firstMortgageeAIs.add(ai);
    		}
    	}
    	return firstMortgageeAIs;
    }
    public ArrayList<ModelBean> getFirstMortgageeAISorted(ModelBean policy,ModelBean risk) throws Exception {
    	ModelBean[] ais = com.iscs.uw.common.shared.AI.getAllAI(policy, risk);
    	ArrayList<ModelBean> firstMortgageeAIs = new ArrayList<ModelBean>();
    	ArrayList<ModelBean> sortedFirstMortgageeList  = new ArrayList<ModelBean>();
    	for (ModelBean ai : ais) {
    		if(ai.gets("InterestTypeCd").equalsIgnoreCase("First Mortgagee")){
    				firstMortgageeAIs.add(ai);
    		}
    	}
    	if(firstMortgageeAIs.size() > 0){
    		sortedFirstMortgageeList = sortFirstMortgageeList(firstMortgageeAIs);
    	}
    	return sortedFirstMortgageeList;
    }
    public static ArrayList<ModelBean> sortFirstMortgageeList(ArrayList sortFirstMortgageeList) 
    	    throws Exception {
    	        try {        	
    	        	
    	        	// Sort List 
    				Collections.sort(sortFirstMortgageeList, new Comparator() {
    					public int compare(Object o1, Object o2) throws ClassCastException {
    						try {
    							ModelBean bean1 = (ModelBean) o1;
    							ModelBean bean2 = (ModelBean) o2;
    							
    							// Sort By SequenceNumber
    							String account1 = bean1.gets("SequenceNumber");
    							String account2 = bean2.gets("SequenceNumber");
    							
    							return account1.compareTo(account2);
    							
    						} catch (Exception e) {
    							throw new ClassCastException("Error occurred trying to compare fields for sorting." + e.toString() );
    						}
    					} 	
    				});	
    				
    				return sortFirstMortgageeList;
    				
    	        } catch(Exception e) {
    	            throw new Exception(e);
    	     }     
    	} 
    
    
    
    /** Obtain the Company remittance information.  Looks up the information in a coderef first.  
	 * If the coderef has not been defined, it will default to the information stored in the 
	 * Company record.
	 * @return Address ModelBean with the remittance address
	 * @throws Exception when an error occurs
	 */
	protected ModelBean getRemittanceInfo() throws Exception {
		ModelBean partyInfo = null;
        MDATable table = (MDATable) Store.getModelObject("::first-elios-codes::RemittanceInfo::all");
		MDAOption option = table.getOption("UseCompanyInfo");		
		if (StringTools.isFalse(option.getLabel())) {
			// Use the information within to build the party info
			partyInfo = new ModelBean("PartyInfo");
			
			// <NameInfo id="NameInfo-32370023-24981302" NameTypeCd="PayToName" CommercialName="Smith, Joe" />
			ModelBean nameInfo = new ModelBean("NameInfo");
			nameInfo.setValue("NameTypeCd", "PayToName");
			nameInfo.setValue("CommercialName", table.getOption("PayTo").getLabel());
			partyInfo.addValue(nameInfo);
			
			
			ModelBean addr = new ModelBean("Addr");
			addr.setValue("AddrTypeCd", "CompanyMailingLookupAddr");
			addr.setValue("PrimaryNumber", table.getOption("MailingPrimaryNumber").getLabel());
			addr.setValue("StreetName", table.getOption("MailingStreetName").getLabel());
			addr.setValue("SecondaryNumber", table.getOption("MaillingSecondaryNumber").getLabel());
			addr.setValue("City", table.getOption("MailingCity").getLabel());
			addr.setValue("StateProvCd", table.getOption("MailingStateProvCd").getLabel());
			addr.setValue("PostalCode", table.getOption("MailingPostalCode").getLabel());
			partyInfo.addValue(addr);
			ModelBean phone = new ModelBean("PhoneInfo");
			phone.setValue("PhoneTypeCd", "CompanyPhonePrimary");
			String phoneNo = table.getOption("TelephoneAreaCode").getLabel() + table.getOption("TelephoneNumber").getLabel() + table.getOption("TelephoneExt").getLabel();
			phone.setValue("PhoneNumber", phoneNo.trim());
			phone.setValue("PhoneName", "Business");
			partyInfo.addValue(phone);			
		} else {
			partyInfo = Company.getCompany().getBean().getBean("PartyInfo");
		}
		return partyInfo;
	}
}