package com.ric.interfaces.choicepoint.contribution.first;

import net.inov.tec.date.StringDate;

import com.ric.interfaces.choicepoint.contribution.UWPolicyInformation;
import com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.entity.CurrentCarrierInfo;

public interface FirstContributionHandler {

	/** Append the FIRSt information to the information on the Current Carrier contribution.
	 * @param currentCarrierInfo The Current Carrier Information that will be appended with FIRSt information
	 * @param policy The Policy ModelBean
	 * @param account The Account ModelBean
	 * @param todayDt The current date
	 * @param isInitialContributionInd Indicator if this is an Initial Contribution
	 * @throws Exception when an error occurs
	 */
	public void createContribution(CurrentCarrierInfo currentCarrierInfo, UWPolicyInformation policyInfo, StringDate todayDt, boolean isInitialContributionInd,String payoffAmt,String transactionCd) throws Exception;

}
