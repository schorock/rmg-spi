package com.ric.interfaces.choicepoint.contribution;

import java.io.File;
import java.util.Map;

import net.inov.tec.beans.ModelBean;

import com.iscs.common.render.DynamicString;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.InnovationUtils;
import com.iscs.common.utility.io.FileTools;
import com.iscs.interfaces.utility.record.Record;

public class ContributionFileGenerator {
	
	private String parsedFilename;
	private File contribFile;
	private String errorMsg;

	public ContributionFileGenerator() {
		errorMsg = "";
	}
	
	/** Set the file generator with the filename name derived from the env-settings file.
	 * @param name The environment settings name
	 * @param key The environment settings key
	 * @param contextObjects The Map of Velocity context objects used to derive the filename from
	 * @return true/false indicator that the file was set up correctly
	 * @throws Exception when an error occurs
	 */
	public boolean setFile(String name, String key, Map<String, Object> contextObjects) 
	throws Exception {
		String filename = InnovationUtils.getEnvSettingsLabel(name, key, "filename", null);
		if (filename == null) {
			errorMsg = "The contribution filename has not been set up in the environment settings file.";
			return false;
		}
		
		return setFile(filename, contextObjects);
	}
	
	/** Set the file generator with the filename name derived from the env-settings file.
	 * @param name The environment settings name
	 * @param contextObjects The Map of Velocity context objects used to derive the filename from
	 * @return true/false indicator that the file was set up correctly
	 * @throws Exception when an error occurs
	 */
	public boolean setFile(String filename, Map<String, Object> contextObjects)
	throws Exception {
		ModelBean party = new ModelBean("PartyInfo");
		parsedFilename = DynamicString.render(new ModelBean[] {party}, contextObjects, filename);		
		contribFile = new java.io.File(parsedFilename);
		String path = parsedFilename.substring(0, parsedFilename.lastIndexOf(File.separator) + 1);
		if (!FileTools.createPath(path)) {
			errorMsg = "Unable to create the filename path for the contribution file.";
			return false;
		}
		if (!contribFile.createNewFile()) {
			errorMsg = "The contribution file " + getFilename() + " currently exists.  There may have been an error processing that file to ChoicePoint.";
			return false;			
		}
		return true;		
	}
	
	public void writeRecord(Record rec)
	throws Exception {
		FileTools.appendText(contribFile, rec.toString(), true);
	}
	
	public void writeAll(Record[] records)
	throws Exception {
		for (Record rec : records) {
			FileTools.appendText(contribFile, rec.toString(), true);			
		}
	}
	
	/** Remove the file in the case of processing errors
	 * @throws Exception 
	 */
	public void cleanFile() {
		if (contribFile != null && !contribFile.delete()) {
			Log.error("Unable to remove ChoicePoint Contribution file due to processing errors");
		}
	}
	
	public String getFilename() {
		if (contribFile.exists())			
			return contribFile.getAbsolutePath();
		else
			return parsedFilename;
	}
	
	public String getErrorMsg() {
		return errorMsg;
	}
	
	public void copy(String folderName)
		throws Exception {
			FileTools.copyFile(this.contribFile.toURI().toURL(), folderName);			
		}

}
