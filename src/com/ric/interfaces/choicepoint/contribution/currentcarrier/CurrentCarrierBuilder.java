package com.ric.interfaces.choicepoint.contribution.currentcarrier;

import net.inov.mda.MDAOption;
import net.inov.mda.MDATable;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.date.StringDate;
import net.inov.tec.obj.ClassHelper;

import com.iscs.common.mda.Store;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanDiff;
import com.iscs.common.utility.bean.render.BeanRenderer;
import com.iscs.uw.interfaces.choicepoint.contribution.UWChoicePointContribution;
import com.ric.interfaces.choicepoint.contribution.UWPolicyInformation;
import com.ric.interfaces.choicepoint.contribution.currentcarrier.CurrentCarrierContributionHandler;
import com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.entity.CurrentCarrierInfo;
import com.iscs.uw.policy.PM;
import com.ric.interfaces.choicepoint.contribution.first.FirstContribution;
import com.ric.interfaces.choicepoint.contribution.first.FirstContributionHandler;

public class CurrentCarrierBuilder extends com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.CurrentCarrierBuilder{

	public String STATUS_NEW = "New";
	public String STATUS_PROCESSED = "Processed";

	/** Process a Periodic Current Carrier Contribution
	 * @param policy The Policy ModelBean
	 * @param account The Account ModelBean
	 * @param todayDt The date of the contribution report
	 * @return The CurrentCarrierInfo POJO
	 * @throws Exception
	 */
	public CurrentCarrierInfo[] processPeriod(ModelBean policy, ModelBean account, StringDate todayDt,String payoffAmt,String paymentDueDate, ModelBean errors,String requestName,ModelBean acct)
	throws Exception {
		return process(policy, account, null, todayDt, false, payoffAmt,paymentDueDate, errors, requestName, acct);
	}
	
	/** Process a Periodic Current Carrier Contribution
	 * @param policy The Policy ModelBean
	 * @param account The Account ModelBean
	 * @param todayDt The date of the contribution report
	 * @return The CurrentCarrierInfo POJO
	 * @throws Exception
	 */
	public CurrentCarrierInfo[] processPeriod(ModelBean policy, ModelBean account, String transactionNumber, StringDate todayDt,String payoffAmt,String paymentDueDate, ModelBean errors,String requestName, ModelBean acct)
	throws Exception {
		return process(policy, account, transactionNumber, todayDt, false,payoffAmt,paymentDueDate, errors,requestName, acct);
	}
	
	/** Process a Periodic Current Carrier Contribution
	 * @param policy The Policy ModelBean
	 * @param todayDt The date of the contribution report
	 * @return The CurrentCarrierInfo POJO
	 * @throws Exception
	 */
	public CurrentCarrierInfo[] processInitial(ModelBean policy, StringDate todayDt,String payoffAmt,String paymentDueDate, ModelBean errors,String requestName, ModelBean acct)
	throws Exception {
		return process(policy, null, null, todayDt, true,payoffAmt,paymentDueDate, errors,requestName, acct);
	}

	/** Process a Current Carrier Contribution
	 * @param policy The Policy ModelBean
	 * @param account The Account ModelBean if available
	 * @param todayDt The date of the contribution report
	 * @return The CurrentCarrierInfo POJO
	 * @throws Exception
	 */
	public CurrentCarrierInfo[] process(ModelBean policy, ModelBean account, String transactionNumber, StringDate todayDt, boolean isInitial,String payoffAmt,String paymentDueDate, ModelBean errors,String requestName, ModelBean acct) throws Exception{
		
		// Set up the Policy information
		UWPolicyInformation policyInfo = new UWPolicyInformation(policy, account);
		policyInfo.setTransactionNumber(transactionNumber);
		
		// Obtain the Contribution Class which will map Policy data into Contribution data
        String repositoryName = "::current-carrier-codes::Contribution::all";
        MDATable table = (MDATable) Store.getModelObject(repositoryName);
        MDAOption option = table.getOption("handler");
        Object obj = ClassHelper.loadObject(option.getLabel());
        CurrentCarrierContribution contrib = (CurrentCarrierContribution) obj;
        CurrentCarrierInfo[] infos = null ;
		        
        // General validation of mapping source
        contrib.validateContributionSetup(policy);
        ModelBean basicPolicy = policy.getBean("BasicPolicy");
    	ModelBean transaction = null;
    	
    	if (policyInfo.getTransactionNumber() != null && policyInfo.getTransactionNumber().length() > 0)
    		transaction = getTransaction(policy, policyInfo.getTransactionNumber());
    	else
    		transaction = getCurrentTransaction(policy);
    	String transactionCd = transaction.gets("TransactionCd");
    	String payplanCd = 	basicPolicy.gets("PayPlanCd");
    	
    	boolean mortgageeAddedDuringEndorsement = false ;
    	if(transactionCd.equalsIgnoreCase("Endorsement")){
	    	 ModelBean priorPolicy = policy.cloneBean(); 
	    	 PM.getPolicyAsOfTransaction(priorPolicy, (basicPolicy.getValueInt("TransactionNumber") - 1));
	    	 BeanDiff beanDiff = BeanRenderer.getBeanDiff(priorPolicy, policy, true, "ChangeInfo,Output");
	    	 BeanDiff[] aiDiffArray = beanDiff.getBeans("AI", BeanDiff.operations("A"));
	    	 for(BeanDiff aiDiff : aiDiffArray){
	    		 if(aiDiff.gets("InterestTypeCd").matches(".*Mortgagee.*")){
	    			 mortgageeAddedDuringEndorsement = true;
	    			 break;
	    		 }
	    	 }
    	}
        
    	if(!StringTools.in(transactionCd, "Non-Renewal,Non-Renewal Request,Non-Renewal Request Stop,Non-Renewal Rescind,Rewrite-New,Rewrite-Renewal,Renewal Activate")){
    		if(!payplanCd.matches(".*Mortgagee.*") || (payplanCd.matches(".*Mortgagee.*") && ((StringTools.in(transactionCd, "Cancellation Notice,Cancellation,Reinstatement,Reinstatement With Lapse"))
    				|| (transactionCd.equalsIgnoreCase("Endorsement") && mortgageeAddedDuringEndorsement) 
    				|| (requestName.equalsIgnoreCase("UWFirstEliosEscrowRq"))))){
    		
	    		infos = contrib.createContribution(policy, account, policyInfo, todayDt, isInitial,errors, acct);
				
				// Check to see if First Elios information should be included with Current Carrier
				if (UWChoicePointContribution.contributionRequired(policy, "CPFirstEnabled")) {
			        repositoryName = "::first-elios-codes::Contribution::all";
			        table = (MDATable) Store.getModelObject(repositoryName);
			        option = table.getOption("handler");
			        obj = ClassHelper.loadObject(option.getLabel());
			        if(infos!=null){
				        for (CurrentCarrierInfo info : infos) {
				        	FirstContribution firstContrib = (FirstContribution) obj;
							firstContrib.createContribution(info, policyInfo, todayDt, isInitial,payoffAmt,transactionCd, paymentDueDate);
							
							// Set the key fields and other non-contribution fields needed for System processing
							info.setPolicyRef(policy.getSystemId());
							
							info.setTransactionNumber(policy.getBean("BasicPolicy").getValueInt("TransactionNumber"));
							info.setProductVersionIdRef(policy.getBean("BasicPolicy").gets("ProductVersionIdRef"));
							
							info.setStatusCd(STATUS_NEW);
				        }
			        }
				} else {
					 if(infos!=null){
						for (CurrentCarrierInfo info : infos) {
							// Key fields still need to be set for non First Elios contributions
							
							// Set the key fields and other non-contribution fields needed for System processing
							info.setPolicyRef(policy.getSystemId());
							info.setTransactionNumber(policy.getBean("BasicPolicy").getValueInt("TransactionNumber"));
							info.setProductVersionIdRef(policy.getBean("BasicPolicy").gets("ProductVersionIdRef"));
							
							info.setStatusCd(STATUS_NEW);
				        }
					 }
				}
    		}	
		}
				
		return infos;
		
	}
	protected ModelBean getCurrentTransaction(ModelBean policy)
		    throws Exception {
		    	ModelBean basicPolicy = policy.getBean("BasicPolicy");
				String transactionNo = basicPolicy.gets("TransactionNumber");
				return basicPolicy.getBean("TransactionHistory", "TransactionNumber", transactionNo);    	
	 }
	
	protected ModelBean getTransaction(ModelBean policy, String transactionNo)
		    throws Exception {
		    	ModelBean basicPolicy = policy.getBean("BasicPolicy");
				return basicPolicy.getBean("TransactionHistory", "TransactionNumber", transactionNo);    	
		}
	
	
}
