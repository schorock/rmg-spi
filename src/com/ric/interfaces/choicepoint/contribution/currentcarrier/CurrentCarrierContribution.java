package com.ric.interfaces.choicepoint.contribution.currentcarrier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

import net.inov.biz.server.ServiceContext;
import net.inov.mda.MDAOption;
import net.inov.mda.MDATable;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.date.StringDate;
import net.inov.tec.math.Money;
import net.inov.tec.obj.ClassHelper;

import com.iscs.common.business.company.Company;
import com.iscs.common.mda.Store;
import com.iscs.common.shared.address.AddressRenderer;
import com.iscs.common.shared.address.AddressTools;
import com.iscs.common.tech.form.field.validate.FieldValidatePhone;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.NameValuePair;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.insurance.product.ProductMaster;
import com.iscs.interfaces.choicepoint.contribution.ChoicePointContributionException;
import com.iscs.interfaces.choicepoint.contribution.ChoicePointContributionHelper;
import com.ric.interfaces.choicepoint.contribution.UWPolicyInformation;
import com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.CurrentCarrierContributionMapHandler;
import com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.entity.CurrentCarrierCoverage;
import com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.entity.CurrentCarrierInfo;
import com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.entity.CurrentCarrierMisc;
import com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.entity.CurrentCarrierPolicyInfo;
import com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.entity.CurrentCarrierPropertyDetails;
import com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.entity.CurrentCarrierPropertyInfo;
import com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.entity.CurrentCarrierSubjectInfo;
import com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.entity.CurrentCarrierVehicle;
import com.iscs.uw.policy.PM;
import com.iscs.uw.policy.PMException;

/** The main class that will process Policy data into CurrentCarrier information.  This
 * information can then be saved to the database.  
 * 
 * @author allend
 *
 */
public class CurrentCarrierContribution extends com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.CurrentCarrierContribution {

	private CurrentCarrierContributionMapHandler mapper = null;
	private boolean isAutoPolicy = false;
	private boolean isInitialLoad = false;
	private static String CANCEL_STATUS = "Cancel";
	private static String ACTIVE_STATUS = "Active";
	private static String REINSTATED_STATUS = "Reinstated";
	private static String EXPIRED_STATUS = "Expired";
	private static String YES = "Y";
	private String policyStatusCd;
	private StringDate cancellationDt;
	private StringDate reinstatementDt;
	private String cancelReasonCd;
	
	public CurrentCarrierContribution() throws Exception {
		// Obtain the Contribution Class which will map Policy data into Contribution data
        String repositoryName = "::current-carrier-codes::Mapper::all";
        MDATable table = (MDATable) Store.getModelObject(repositoryName);
        MDAOption option = table.getOption("handler");
        String handler = option.getLabel();
        Object obj = ClassHelper.loadObject(handler);
        mapper = (CurrentCarrierContributionMapHandler) obj;
        
	}
	
	/** Process the Policy information and save it into the pojo data model
	 * @param policy The Policy ModelBean
	 * @param account The Account ModelBean
	 * @param policyInfo Policy information needed but not stored in Current Carrier 
	 * @param todayDt The date this contribution record was processed
	 * @param isInitialContributionInd Indicator if this is to be considered an initial contribution
	 * @return The Current Carrier information
	 * @throws Exception when an error occurs
	 */
	public CurrentCarrierInfo[] createContribution(ModelBean policy, ModelBean account, UWPolicyInformation policyInfo, StringDate todayDt, boolean isInitialContributionInd, ModelBean errors, ModelBean acct)
	throws Exception {
		
		HashMap<String, ArrayList> lineTypes = new HashMap();
		// Group the types of risks together into its Insurance Types. 
		ModelBean[] lines = policy.getBeans("Line");
		for (ModelBean line : lines) {
			String insuranceType = mapper.mapInsuranceType(policy, line);
			if (insuranceType.equals("")) 
				continue;  // The line is not set up for ChoicePoint contribution
			ArrayList<ModelBean> lineRisks = lineTypes.get(insuranceType);
			if (lineRisks == null) {
				lineRisks = new ArrayList<ModelBean>();
				lineTypes.put(insuranceType, lineRisks);
			}
			ModelBean[] risks = line.getBeans("Risk");
			lineRisks.addAll(Arrays.asList(risks));
		}
		
		ArrayList<CurrentCarrierInfo> carrierInfoList = new ArrayList<CurrentCarrierInfo>(); 
		ArrayList<ModelBean>[] riskLists = lineTypes.values().toArray(new ArrayList[lineTypes.size()]);
		HashMap<String, String> riskMap = new HashMap<String, String>();
		for (ArrayList<ModelBean> riskList : riskLists) {
			if (riskList.size() == 0)
				continue; // The line is empty and does not contain any reportable risks
			CurrentCarrierInfo carrier = new CurrentCarrierInfo();				
	        isInitialLoad = isInitialContributionInd;
			ModelBean[] risks = riskList.toArray(new ModelBean[riskList.size()]);
			carrier.setLine(BeanTools.getParentBean(risks[0], "Line"));
			carrier.setPolicy(createPolicyRecord(policy, account, risks, riskMap, isInitialContributionInd, acct));  // Set the Policy Information
			carrierInfoList.add(carrier);
		}
		
		policyInfo.setRiskList(riskMap);
		return carrierInfoList.toArray(new CurrentCarrierInfo[carrierInfoList.size()]);
	}
	
	/** Create the Policy Information record.  This contains the summary policy information
	 * @param policy The Policy ModelBean
	 * @param isInitial Indicator that the contribution is the initial one
	 * @return The Current Carrier Policy Information
	 * @throws Exception when an error occurs
	 */
	protected CurrentCarrierPolicyInfo createPolicyRecord(ModelBean policy, ModelBean account, ModelBean[] riskList, HashMap<String, String> riskMap, boolean isInitial, ModelBean acct)
	throws Exception {
		
		CurrentCarrierPolicyInfo ccPolicy = new CurrentCarrierPolicyInfo();
		ModelBean basicPolicy = policy.getBean("BasicPolicy");
		ModelBean carrier = Company.getCompany().getBean().getBeanById("Carrier", basicPolicy.gets("CarrierCd"));
		ccPolicy.setAMBestNumber(carrier.gets("AMBestNumber"));
		ccPolicy.setNAICNumber(carrier.gets("NAICNumber"));
		ccPolicy.setCompanyName(carrier.gets("Name"));
		
		ccPolicy.setPolicyNumber(basicPolicy.gets("PolicyNumber"));
		ccPolicy.setInsuranceType(mapper.mapInsuranceType(policy, riskList[0]));
		isAutoPolicy = false;		
		if (ccPolicy.getInsuranceType().equals("PA") || ccPolicy.getInsuranceType().equals("CA"))
			isAutoPolicy = true;
		
		ccPolicy.setSubTypeCd(mapper.mapRiskType(policy));
		ccPolicy.setPolicyTypeCd(mapper.mapPolicyType(policy, riskList[0]));
		// Set Policy State
		ccPolicy.setPolicyState(basicPolicy.gets("ControllingStateCd"));		
		ccPolicy.setInceptionDt(basicPolicy.getDate("InceptionDt", null));
		ccPolicy.setExpirationDt(basicPolicy.getDate("ExpirationDt", null));
		ccPolicy.setEffectiveDt(basicPolicy.getDate("EffectiveDt", null));
		if (basicPolicy.gets("TransactionCd").equals(PM.TX_CANCELLATION))
			ccPolicy.setCancelDt(basicPolicy.getDate("CancelDt", null));
		if (basicPolicy.gets("TransactionCd").equals(PM.TX_FUTURE_EXPIRE))
			ccPolicy.setCancelDt(basicPolicy.getDate("ExpirationDt", null));
		// Change Effective Dates - In an Initial extract, the Change Effective Dates are to be the same valid dates and are to be the date the file is created 
		if (!isInitial){
			ccPolicy.setChangeEffDt(getCurrentTransactionDt(policy));
		}else {
			StringDate effectiveDate = basicPolicy.getDate("EffectiveDt", null);
			StringDate bookDate = ServiceContext.getServiceContext().getBookDt();
			if (effectiveDate.compareTo(bookDate) > 0) {
				ccPolicy.setChangeEffDt(effectiveDate);
			}else{
				ccPolicy.setChangeEffDt(bookDate);
			}
		}
		
		//ccPolicy.setWrittenPremiumAmt(ChoicePointContributionHelper.getAccumulatedPremium(policy));
		String basePremium = ChoicePointContributionHelper.getAccumulatedPremium(policy);
		String cancelTypeCd = "";
		String canceRequestedBy = "";
		if(StringTools.in(basicPolicy.gets("TransactionCd"), "Cancellation,Cancellation Notice")){
			String transNo = basicPolicy.gets("TransactionNumber");
			ModelBean transHistory = basicPolicy.findBeanByFieldValue("TransactionHistory", "TransactionNumber", transNo);
		    cancelTypeCd = transHistory.gets("CancelTypeCd");
		    canceRequestedBy = transHistory.gets("CancelRequestedByCd");
		}
		BigDecimal totalPremium = new BigDecimal(basePremium);
		BigDecimal lateFeeAmt = new BigDecimal("0.00");
		if(StringTools.in(basicPolicy.gets("TransactionCd"), PM.TX_CANCELLATION)&& basePremium.equals("0.00")){
			ModelBean nbTransaction = basicPolicy.findBeanByFieldValue("TransactionHistory", "TransactionCd", "New Business");
			if(nbTransaction!=null) {
				String writtenPremiumAmt = nbTransaction.gets("WrittenPremiumAmt");
				if( writtenPremiumAmt!=null && !writtenPremiumAmt.equals("")) {
					BigDecimal transactionWrittenPremium = new BigDecimal(writtenPremiumAmt);
					totalPremium = totalPremium.add(transactionWrittenPremium);

				}
			}
		}
		
		if(acct!=null){
			ModelBean[] arTrans = acct.getBeans("ARTrans");
			String lateFee ;
			for(ModelBean arTran : arTrans){
				if(arTran.gets("AdjustmentCategoryCd").equalsIgnoreCase("LateFee")){
					lateFee = arTran.gets("Amount");
					BigDecimal translateFee = new BigDecimal(lateFee);
					if(!cancelTypeCd.equalsIgnoreCase("Flat"))
						lateFeeAmt = lateFeeAmt.add(translateFee);

				}
			}
			if(basicPolicy.gets("TransactionCd").equalsIgnoreCase("Cancellation Notice")){
				if(canceRequestedBy.equalsIgnoreCase("NonPay")){
					ModelBean arpayPlan = acct.getBean("ARPayPlan");
					ModelBean[] arCategorys = arpayPlan.getBeans("ARCategory");

					for(ModelBean arCategory : arCategorys){
						if(arCategory.gets("CategoryCd").equalsIgnoreCase("LateFee")){
							lateFee = arCategory.gets("Amount");
							BigDecimal translateFee = new BigDecimal(lateFee);
							lateFeeAmt = lateFeeAmt.add(translateFee);
						}
					}
				}
			}
		}
		
		ccPolicy.setWrittenPremiumAmt(totalPremium.add(lateFeeAmt).toString());
		ccPolicy.setPayPlanCd(mapper.mapPayPlan(policy, account));
		ccPolicy.setPaymentMethodCd(mapper.mapPaymentMethod(policy));
		// The insured address breakdown may have already been done an applied in InsuredMailingLookupAddr
		ModelBean insuredParty = policy.getBean("Insured").getBean("PartyInfo", "PartyTypeCd", "InsuredParty");
		ModelBean mailingLookupAddr = insuredParty.getBean("Addr", "AddrTypeCd", "InsuredLookupAddr");
		if (mailingLookupAddr == null) {
			ModelBean mailingAddr = insuredParty.getBean("Addr", "AddrTypeCd", "InsuredMailingAddr");
			mailingLookupAddr = ChoicePointContributionHelper.validateAddress(mailingAddr);
		}
		mailingLookupAddr = ChoicePointContributionHelper.convertToInternationalAddress(mailingLookupAddr);
		ccPolicy.setMailingAddressPrimaryNumber(mailingLookupAddr.gets("PrimaryNumber"));
		ccPolicy.setMailingAddressStreetName(StringTools.concat(mailingLookupAddr.gets("StreetName"), mailingLookupAddr.gets("Suffix"), " ").trim());
		ccPolicy.setMailingAddressSecondaryNumber(mailingLookupAddr.gets("SecondaryNumber"));
		ccPolicy.setMailingAddressCity(mailingLookupAddr.gets("City"));
		ccPolicy.setMailingAddressStateProvCd(mailingLookupAddr.gets("StateProvCd"));
		ccPolicy.setMailingAddressPostalCode(AddressTools.getZip5(mailingLookupAddr.gets("PostalCode")));
		ccPolicy.setMailingAddressPostalCodeExt(AddressTools.getZip4(mailingLookupAddr.gets("PostalCode"), null));
		// P.O Box edit 
		if (ChoicePointContributionHelper.isPOBox(mailingLookupAddr)) {
			ccPolicy.setMailingAddressPrimaryNumber("");
			ccPolicy.setMailingAddressStreetName(mailingLookupAddr.gets("Addr1"));
		}
		
		String parsedPhone = getParsedPhoneNumber(insuredParty);
		if (parsedPhone.length() >= 10) {
			ccPolicy.setTelephoneAreaCode(parsedPhone.substring(0, 3));
			ccPolicy.setTelephoneNumber(parsedPhone.substring(3, 10));
			ccPolicy.setTelephoneExt(parsedPhone.substring(10));
		}
		
		boolean insuredAdded = false;
		// Create a subject record for each policy insured (property) or driver (auto)
		ModelBean[] drivers = policy.findBeansByFieldValue("PartyInfo", "PartyTypeCd", "DriverParty");
		ArrayList<CurrentCarrierSubjectInfo> subjects = new ArrayList<CurrentCarrierSubjectInfo>();
		for (ModelBean driver : drivers) {
			CurrentCarrierSubjectInfo subject = createSubjectRecord(driver);
			subjects.add(subject);
			ModelBean driverInfo = driver.getBean("DriverInfo", "RelationshipToInsuredCd", "Self");
			if (driverInfo != null)
				insuredAdded = true;
		}
		
		if (!insuredAdded) {
			ModelBean partyInfos[] = policy.getBean("Insured").getBeans("PartyInfo");
			for (ModelBean partyInfo : partyInfos) {
				boolean isBlank = true;
				// Check to see if the child beans are blank
				ModelBean childBean[] = partyInfo.getBeans();
				for (int i = 0; i < childBean.length; i++) {
					if(!BeanTools.isBlank(childBean[i])) {
						isBlank = false;
					}
				}
				// If Child beans are not blank, create the SJ01 record
				if(!isBlank) {
					if(StringTools.in(partyInfo.gets("PartyTypeCd"), "InsuredParty,InsuredPartyJoint")){
						CurrentCarrierSubjectInfo subject = createPropertySubjectRecord(partyInfo,insuredParty);
						subjects.add(subject);
					}
				}
			}
		}
		
		ccPolicy.setSubjects(subjects);
		
		// Create a property record for each policy risk or vehicle
		ArrayList<CurrentCarrierPropertyInfo> properties = new ArrayList<CurrentCarrierPropertyInfo>();
		ArrayList details = new ArrayList();
		int propertyId = 1;
		int autoId = 1;
		for (ModelBean risk : riskList) {
			CurrentCarrierPropertyInfo property = createPropertyRecord(policy, risk, 1);
			if(property != null){
				if (isAutoPolicy ) {
					// Create the vehicle detail record if auto policy per risk/vehicle
					CurrentCarrierVehicle vehicle = createVehicleDetail(policy, risk, autoId);
					if( vehicle != null ){
						details.add(vehicle);
					}
			    	property.setPropertyId(autoId);
					riskMap.put("Auto-" + StringTools.leftPad(autoId, "0", 3), risk.getId());
					autoId++;
				} else {
					// Create the property detail record for each of the policy risk/vehicle
					CurrentCarrierPropertyDetails propDetail = createPropertyDetail(policy, risk, propertyId);
					details.add(propDetail);				
			    	property.setPropertyId(propertyId);
					riskMap.put("Property-" + StringTools.leftPad(propertyId, "0", 3), risk.getId());
					propertyId++;
				}
				
				properties.add(property);
			}
		}
		
		ccPolicy.setProperties(properties);
		ccPolicy.setPropertyDetails(details);
					
		// Initial file should not include MR01 records
		if( !isInitial ) {
			// Create the miscellaneous record
			CurrentCarrierMisc misc = createMiscellaneousRecord(policy);
			if (misc != null)
				ccPolicy.setMiscDetails(misc);
		}
		
		return ccPolicy;
	}
	    
    /** Find the current transaction effective date.  Determine what the current
     * transaction is, and then locate the effective date for it
     * @param basicPolicy The BasicPolicy ModelBean
     * @return date of the effective transaction 
     * @throws Exception when an error occurs
     */
    protected StringDate getChangeEffectiveDate(ModelBean basicPolicy) 
    throws Exception {
    	String transNo = basicPolicy.gets("TransactionNumber");
    	ModelBean trans = basicPolicy.getBean("TransactionHistory", "TransactionNumber", transNo);
    	return trans.getDate("TransactionEffectiveDt");
    }
    
    /** From the current PartyInfo bean, find the preferred phone number and parse it into its numeric string.
     * If no preferred phone, then obtain the first valid available.
     * @param bean The PartyInfo ModelBean containing the phone numbers
     * @return The parsed phone number
     * @throws Exception when an error occurs
     */
    public static String getParsedPhoneNumber(ModelBean partyInfo)
    throws Exception {
    	// Set up the phone validation
    	NameValuePair[] params = NameValuePair.createParams("MinLength=10|MaxLength=14", "|");
		FieldValidatePhone validPhone = new FieldValidatePhone();
		
    	ModelBean preferred = partyInfo.getBean("PhoneInfo", "PreferredInd", "Yes");
    	if (preferred == null) {    		
    		ModelBean[] phones = partyInfo.getBeans("PhoneInfo");
    		for (ModelBean phone : phones) {
    			if (validPhone.isValid(phone.gets("PhoneNumber"), params)) {
    				preferred = phone;
    				break;
    			}
    		}
    	}
    	if (preferred != null) {
    		return (String) validPhone.parse(preferred.gets("PhoneNumber"));
    	}
    	return "";
    }
    
    /** Create the subject Pojo record. This should be called for each subject 
     * within the Policy.  The subject(s) for a Property policy would consists of 
     * the insured and other co-insureds.  The subjects for an Auto policy includes
     * the drivers.
     * @param driverParty The ModelBean containing subject information
     * @return the Pojo for the subject information
     * @throws Exception when an error occurs
     */
    protected CurrentCarrierSubjectInfo createSubjectRecord(ModelBean driverParty)
    throws Exception {
    	CurrentCarrierSubjectInfo subject = new CurrentCarrierSubjectInfo();
    	subject.setRelationshipToInsuredCd(mapper.mapRelationshipTo(driverParty));
    	// Logic added to populate SSN for the Insured Party
    	ModelBean driverInfo = driverParty.getBean("DriverInfo");
    	String relationshipToInsuredCd = driverInfo.gets("RelationshipToInsuredCd");
    	ModelBean policy = BeanTools.getParentBean(driverParty, "Policy");
    	if(relationshipToInsuredCd.equalsIgnoreCase("Self")) {
    		ModelBean taxInfo = policy.getBeanByAlias("InsuredTaxInfo");
    		if (taxInfo != null && !taxInfo.gets("SSN").equals("") && taxInfo.gets("SSN") != null) {
    			String ssn = taxInfo.gets("SSN").replaceAll("-", "");
    			subject.setSsn(ssn);
    		}	
    	}
    	ModelBean nameInfo = driverParty.getBean("NameInfo");
    	subject.setSurname(nameInfo.gets("Surname"));
    	subject.setGivenName(nameInfo.gets("GivenName"));
    	subject.setOtherGivenName(nameInfo.gets("OtherGivenName"));
    	subject.setSuffixCd(nameInfo.gets("SuffixCd"));
    	ModelBean personInfo = driverParty.getBean("PersonInfo");
    	subject.setBirthDt(personInfo.getDate("BirthDt"));
    	if (personInfo.gets("GenderCd").equals("Male") || personInfo.gets("GenderCd").equals("Female")) {
    		subject.setGenderCd(personInfo.gets("GenderCd").substring(0, 1));    	
    	}
    	if (driverInfo != null) {
        	subject.setLicenseNumber(driverInfo.gets("LicenseNumber"));
        	subject.setLicensedStateProvCd(driverInfo.gets("LicensedStateProvCd"));    		
    	}
    	
    	return subject;
    }

    /** Create the subject Pojo record. This should be called for each subject 
     * within the Policy.  The subject(s) for a Property policy would consists of 
     * the insured and other co-insureds.  The subjects for an Auto policy includes
     * the drivers.
     * @param driverParty The ModelBean containing subject information
     * @return the Pojo for the subject information
     * @throws Exception when an error occurs
     */
    protected CurrentCarrierSubjectInfo createPropertySubjectRecord(ModelBean primaryPartyBean, ModelBean secondaryPartyBean)
    throws Exception {
		CurrentCarrierSubjectInfo subject = new CurrentCarrierSubjectInfo();
		subject.setRelationshipToInsuredCd(mapper.mapRelationshipTo(primaryPartyBean));
		ModelBean nameInfo = primaryPartyBean.getBean("NameInfo");
		if (BeanTools.isBlank(nameInfo)) {
			nameInfo = secondaryPartyBean.getBean("NameInfo");
		}

		String surName = nameInfo.gets("Surname");
		String givenName = nameInfo.gets("GivenName");
		String otherGivenName = nameInfo.gets("OtherGivenName");

		if ((StringUtils.isEmpty(surName) && StringUtils.isEmpty(givenName)) || StringUtils.isEmpty(surName) || StringUtils.isEmpty(givenName)) {
			otherGivenName = "";
			String commercialName = nameInfo.gets("CommercialName");
			String[] names = commercialName.split("\\s+");
			if (names != null && names.length > 0) {
				surName = names[0].replaceAll("[\\d]+", "");
			}
			if (names != null && names.length > 1) {
				givenName = names[1];
			}

			if (names.length > 2) {
				for (int i = 2; i < names.length; i++) {
					otherGivenName = otherGivenName + names[i] + " ";
				}
			}

		}

		subject.setSurname(surName);
		subject.setGivenName(givenName);
		subject.setOtherGivenName(otherGivenName);
		subject.setSuffixCd(nameInfo.gets("SuffixCd"));
		ModelBean personInfo = primaryPartyBean.getBean("PersonInfo");
		if (BeanTools.isBlank(personInfo)) {
			personInfo = secondaryPartyBean.getBean("PersonInfo");
		}
		subject.setBirthDt(personInfo.getDate("BirthDt"));
		if (personInfo.gets("GenderCd").equals("Male") || personInfo.gets("GenderCd").equals("Female"))
			subject.setGenderCd(personInfo.gets("GenderCd").substring(0, 1));

		return subject;
	}
        
    /** Create the property pojo record.  For each risk within the policy,
     * a corresponding Property record needs to be created for contribution.
     * @param policy The Policy ModelBean
     * @param risk The Risk ModelBean
     * @param propertyId A unique id to tag each property record
     * @return The property record Pojo
     * @throws Exception when an error occurs
     */
    protected CurrentCarrierPropertyInfo createPropertyRecord(ModelBean policy, ModelBean risk, int propertyId)
    throws Exception {
    	CurrentCarrierPropertyInfo property = new CurrentCarrierPropertyInfo();
    	if (risk.gets("Status").equals("Deleted") ) {
        	// Find when it was set to Deleted
    		ModelBean changedTrans = fieldChangedInTransaction(policy, risk, "Status", "Deleted");
    		ModelBean riskAddedTrans = fieldFirstAddedInTransaction(policy, risk, "Risk", "A");
    		ModelBean currTransaction = getCurrentTransaction(policy);
    		// If the changedTrans is null the risk is deleted in the NB transaction
    		if (changedTrans != null && !currTransaction.gets("TransactionCd").equals(PM.TX_NEW_BUSINESS) ){
    			String changedTransId = changedTrans.getId();
    			String currTransId = currTransaction.getId();
        		// Check if the risk is added and deleted in the same endorsement.
    			if(riskAddedTrans != null && changedTransId.equals(riskAddedTrans.getId())){
        			return null;
        		}
    			StringDate currentTransactionDt = currTransaction.getDate("TransactionEffectiveDt");
    			// Send the deleted risk only if the risk is added in previous transaction and deleted in the current transaction. 
    			if (changedTransId.equals(currTransId)){
		        	if(!isAutoPolicy){
		        		// Initial file should not set cancel date
		        		if( isInitialLoad ) {
		        			property.setPropertyCancellationDt(null);
		        			property.setPropertyCancellationInd("");
		        		} else {
		        			property.setPropertyCancellationDt(currentTransactionDt);
		        			property.setPropertyCancellationInd(YES);
		        		}
		        	}
    			}else{
    				return null;
    			}
        	}else{
        		return null;
        	}
    	}else{
    		String statusCd = getPolicyStatus(policy);
    		if (!isAutoPolicy && (statusCd.equals(CANCEL_STATUS) || statusCd.equals(EXPIRED_STATUS) )) {
    			// Initial file should not set cancel date
    			if( isInitialLoad ) {
    				property.setPropertyCancellationDt(null);
    				property.setPropertyCancellationInd("");
    			} else {
    				property.setPropertyCancellationDt(cancellationDt);
    				property.setPropertyCancellationInd(YES);
    			}
    		} else if ( !isAutoPolicy && statusCd.equals(REINSTATED_STATUS)) {
	        	property.setPropertyCancellationInd("");
    		}
    	
    	}
    	if (isAutoPolicy) {
        	ModelBean vehicle = risk.getBean("Vehicle");
        	if(vehicle == null){
        		return null;
        	}
    		property.setVehIdentificationNumber(vehicle.gets("VehIdentificationNumber"));
    		property.setModelYr(vehicle.gets("ModelYr"));
    		property.setManufacturer(vehicle.gets("Manufacturer"));
    		property.setModel(vehicle.gets("Model"));    		
    		
    		if (vehicle.gets("VehUseCd").equals("Commute"))
    			property.setBusinessUseInd("Y");
    		else if (vehicle.gets("VehUseCd").equals("Work"))
    			property.setBusinessUseInd("Y");
    		else if (vehicle.gets("VehUseCd").equals("Pleasure"))
    			property.setBusinessUseInd("N");
    		else 
    			property.setBusinessUseInd("U");    			    		
    	} else {
    		ModelBean building = risk.getBean("Building");
    		ModelBean lookupAddr = building.getBean("Addr", "AddrTypeCd", "RiskLookupAddr");
    		if (lookupAddr == null) {
    			lookupAddr = building.getBean("Addr", "AddrTypeCd", "RiskAddr");
    			if (lookupAddr == null) {
    				// Look in commercial types just in case
    				lookupAddr = risk.findBeanByFieldValue("Addr", "AddrTypeCd", "ExposureAddr", true);
    			}
    			lookupAddr = ChoicePointContributionHelper.validateAddress(lookupAddr);
    		}
    		lookupAddr = ChoicePointContributionHelper.convertToInternationalAddress(lookupAddr);
    		property.setLocationAddressPrimaryNumber(lookupAddr.gets("PrimaryNumber"));
    		property.setLocationAddressStreetName(StringTools.concat(lookupAddr.gets("StreetName"), lookupAddr.gets("Suffix"), " ").trim());
    		property.setLocationAddressSecondaryNumber(lookupAddr.gets("SecondaryNumber"));
    		property.setLocationAddressCity(lookupAddr.gets("City"));
    		property.setLocationAddressStateProvCd(lookupAddr.gets("StateProvCd"));
    		property.setLocationAddressPostalCode(AddressTools.getZip5(lookupAddr.gets("PostalCode")));
    		property.setLocationAddressPostalCodeExt(AddressTools.getZip4(lookupAddr.gets("PostalCode"), null));
    		// P.O Box edit 
    		if (ChoicePointContributionHelper.isPOBox(lookupAddr)) {
    			property.setLocationAddressPrimaryNumber("");
    			property.setLocationAddressStreetName(lookupAddr.gets("Addr1"));
    		}
    		property.setPropertyType(mapper.mapPropertyType(policy, risk));
    	}

    	property.setPropertyId(propertyId);
    	
    	// Obtain the policy deductibles if available
		property.setDeductible1(getCoverageDeductible1(policy, risk));
		property.setDeductible2(getCoverageDeductible2(policy, risk));
    	
    	// Obtain all the coverages that apply to this risk
    	ArrayList<CurrentCarrierCoverage> coverages = createPropertyCoverages(policy, risk);
    	
    	if(coverages != null){
    		for (CurrentCarrierCoverage coverage : coverages) {
    			String coverageCd = coverage.getCoverageCd();
    			if (!"OTHR".equalsIgnoreCase(coverageCd)
    					&& StringUtils.isEmpty(coverage.getCombinedSingleLimit())) {
    				coverage.setCombinedSingleLimit(property.getDeductible1());    				
    			}
				
			}
    	}
       
    	property.setCoverages(coverages);
    	
    	return property;
    }
    
    
    /** Create the property detail pojo record.  For each risk within the policy,
     * a corresponding Property Detail record needs to be created for contribution.
     * @param policy The Policy ModelBean
     * @param risk The Risk ModelBean
     * @param propertyId A unique id to tag each property record
     * @return The property record Pojo
     * @throws Exception when an error occurs
     */
    protected CurrentCarrierPropertyDetails createPropertyDetail(ModelBean policy, ModelBean risk, int propertyId)
    throws Exception {
    	CurrentCarrierPropertyDetails detail = new CurrentCarrierPropertyDetails();
    	ModelBean building = risk.getBean("Building");
    	if (building == null) 
    		return null;
    	detail.setPropertyId(propertyId);
    	detail.setSqFt(building.getValueInt("SqFt"));
    	detail.setStories(building.getValueInt("Stories"));
    	detail.setYearBuilt(building.getValueInt("YearBuilt"));
    	
    	return detail;
    }
    
    /** Create the vehicle detail pojo record.  For each risk within the policy,
     * a corresponding Vehicle Detail record needs to be created for contribution.
     * @param policy The Policy ModelBean
     * @param risk The Risk ModelBean
     * @param propertyId A unique id to tag each property record
     * @return The property record Pojo
     * @throws Exception when an error occurs
     */
    protected CurrentCarrierVehicle createVehicleDetail(ModelBean policy, ModelBean risk, int propertyId)
    throws Exception {
    	CurrentCarrierVehicle detail = new CurrentCarrierVehicle();
    	ModelBean vehicle = risk.getBean("Vehicle");
    	if (vehicle == null) 
    		return null;
    	detail.setVehIdentificationNumber(vehicle.gets("VehIdentificationNumber"));
    	detail.setVehicleStateProvCd(vehicle.gets("RegistrationStateProvCd"));    	
    	detail.setVehicleType(mapper.mapVehicleType(risk, vehicle));
    	if (risk.gets("Status").equals("Deleted")) {
        	detail.setVehicleStatus("C");
        	// Find when it has was set to Deleted
        	ModelBean trans = fieldChangedInTransaction(policy, risk, "Status", "Deleted");        	
        	if( trans != null ){        	
	        	detail.setVehicleStatusDt(trans.getDate("TransactionEffectiveDt"));
	        	detail.setVehicleCancelReason("CUST");
        	} else {
        		return null;
        	}	        
    	} else {
    		String statusCd = getPolicyStatus(policy);
    		if (statusCd.equals(ACTIVE_STATUS)) {
    			detail.setVehicleStatus("A");
    		} else if (statusCd.equals(CANCEL_STATUS) || statusCd.equals(EXPIRED_STATUS)) {
    			detail.setVehicleStatus("C");
    			detail.setVehicleStatusDt(cancellationDt);
        		if (statusCd.equals(CANCEL_STATUS)) {
        			detail.setVehicleCancelReason(cancelReasonCd);
        		}    			
    		} else if (statusCd.equals(REINSTATED_STATUS)) {
    			detail.setVehicleStatus("R");
    			detail.setVehicleStatusDt(reinstatementDt);
    		}
    	}
    	if (isInitialLoad) {
    		// Vehicle Add Date is required on Initial Load, Date vehicle added, after the inception date without a coverage gap
        	ModelBean trans = fieldFirstChangedInTransaction(policy, risk, "Status", "Active");
        	if (trans != null)
        		detail.setVehicleAddDt(trans.getDate("TransactionEffectiveDt"));
    	}
    	
    	return detail;
    }
    
    protected CurrentCarrierMisc createMiscellaneousRecord(ModelBean policy) 
    throws Exception {
    	CurrentCarrierMisc misc = new CurrentCarrierMisc();
    	if (cancelReasonCd != null || reinstatementDt != null) {
    		misc.setCancelReasonCd(cancelReasonCd);
    		misc.setReinstatementDt(reinstatementDt);
    		return misc;
    	}
    	return null;
    }
    
	/** Create the list of Property Coverages that will be added.  To conform to ChoicePoint requirements,
	 * each Coverage must be unique per property.  If a Coverage exists multiple times in the property,
	 * the additional copies must be added to the "Other" type of coverage.  Any coverage that cannot be 
	 * mapped to a defined code should also be summed to the Other coverage type also.
	 * @param policy The Policy ModelBean
	 * @param risk The auto risk or property that the coverages should be taken from.  Please note that if 
	 * common coverages such as liabilities that affect multiple risks will have to be obtained by starting from
	 * the Policy bean or traversed up the parent(s) of the current risk. 
	 * @return The list of Pojo Coverages this property has
	 * @throws Exception when an error occurs
	 */
    protected ArrayList<CurrentCarrierCoverage> createPropertyCoverages(ModelBean policy, ModelBean risk) 
    throws Exception, ChoicePointContributionException {
    	ModelBean[] coverages = risk.findBeansByFieldValue("Coverage","Status","Active");
    	HashMap<String, CurrentCarrierCoverage> covHash = new HashMap<String, CurrentCarrierCoverage>();
    	for (ModelBean coverage : coverages) {
        	String ccCode = mapper.mapCoverageCode(policy, coverage);
        	if (covHash.containsKey(ccCode)) {
        		// Coverage has been created already, sum this coverage in the All Other type coverage
        		String otherKey = null;
        		if (isAutoCoverage(ccCode)) {
        			otherKey = "OT";
        		} else {
        			otherKey = "OTHR";
        		}
        		CurrentCarrierCoverage other = covHash.get(otherKey);
        		if (other == null) {
        			other = new CurrentCarrierCoverage();
        			other.setCoverageCd(otherKey);
        			covHash.put(otherKey, other);
        		}
        		CurrentCarrierCoverage propCov = setPropertyCoverage(policy, coverage, ccCode);
        		
        		other.setIndividualLimit(addMoney(other.getIndividualLimit(), propCov.getIndividualLimit()));
        		other.setOccurrenceLimit(addMoney(other.getOccurrenceLimit(), propCov.getOccurrenceLimit()));
        		other.setCombinedSingleLimit(addMoney(other.getCombinedSingleLimit(), propCov.getCombinedSingleLimit()));
        	} else {
        		CurrentCarrierCoverage propCov = setPropertyCoverage(policy, coverage, ccCode);
        		propCov.setCoverageCd(ccCode);
        		covHash.put(ccCode, propCov);
        	}
    	}
    	
    	ArrayList<CurrentCarrierCoverage> coverageList = new ArrayList<CurrentCarrierCoverage>();
    	Iterator<String> iter = covHash.keySet().iterator();
    	while (iter.hasNext()) {
    		CurrentCarrierCoverage covItem = covHash.get(iter.next());
    		coverageList.add(covItem);
    	}
    	
    	return coverageList;
    }
    
	/** Create the list of Property Coverages that will be added.  To conform to ChoicePoint requirements,
	 * each Coverage must be unique per property.  If a Coverage exists multiple times in the property,
	 * the additional copies must be added to the "Other" type of coverage.  Any coverage that cannot be 
	 * mapped to a defined code should also be summed to the Other coverage type also.
	 * @param policy The Policy ModelBean
	 * @param coverage The auto risk or property coverage.  Please note that if 
	 * common coverages such as liabilities that affect multiple risks will have to be obtained by starting from
	 * the Policy bean or traversed up the parent(s) of the current risk. 
	 * @param ccCode 
	 * @return The list of Pojo Coverages this property has
	 * @throws Exception when an error occurs
	 */
    protected CurrentCarrierCoverage setPropertyCoverage(ModelBean policy, ModelBean coverage, String ccCode)
    throws Exception {
    	CurrentCarrierCoverage propCov = new CurrentCarrierCoverage();
    	// Changes done to populate the correct limit fields as per the LexisNexis 6.6 Specifications
    	ModelBean indLimit = null;
    	ModelBean occLimit = null;
    	ModelBean combLimit = null;
    	String inOccurCodes = "PD,PI,MP,ME,TL,NP,UP,OT";
    	if (ccCode.equals("BI") || ccCode.equals("RR") || ccCode.equals("NB") || ccCode.equals("UB")) {
    		indLimit = coverage.getBean("Limit", "LimitCd", "Limit1");
    		occLimit = coverage.getBean("Limit", "LimitCd", "Limit2");
    	} else if (StringTools.in(ccCode, inOccurCodes, false)) {
    		//occLimit = getOccurrenceLimit(coverage);
    	} else if (ccCode.equals("UM") || ccCode.equals("UN") || ccCode.equals("CS")) {
    		combLimit = coverage.getBean("Limit", "LimitCd", "Limit1");
    	} else if (ccCode.length() == 4) {
    		indLimit = coverage.getBean("Limit", "LimitCd", "Limit1");
    	} else if (ccCode.equals("CO") || ccCode.equals("CP")) {
    		// Do not set any limits
    	}
    	
    	if (indLimit != null) {
    		String value = indLimit.gets("Value");
    		if (StringTools.isNumeric(value)) {
            	propCov.setIndividualLimit(indLimit.gets("Value"));    		
    		}
    	}
    	if (occLimit != null) {
    		String value = occLimit.gets("Value");
    		if (StringTools.isNumeric(value)) {
            	propCov.setOccurrenceLimit(occLimit.gets("Value"));    		
    		}
    	}
    	if (combLimit != null) {
    		String value = combLimit.gets("Value");
    		if (StringTools.isNumeric(value)) {
            	propCov.setCombinedSingleLimit(combLimit.gets("Value"));    		
    		}    		
    	}
    	return propCov;
    }    
       
    /** Try to obtain the Occurrence Limit.  First look by TypeCd.  Then look by 
     * the order of the limits, which would generally be Limit2.  Else return Limit1.
     * @param coverage The Coverage ModelBean
     * @return The Limit ModelBean holding the Occurrence Limit
     * @throws Exception when an error occurs
     */
    protected ModelBean getOccurrenceLimit(ModelBean coverage)
    throws Exception {
    	ModelBean limit = coverage.findBeanByFieldValue("Limit", "TypeCd", "Occurrence");
    	if (limit == null) {
    		limit = coverage.findBeanByFieldValue("Limit", "LimitCd", "Limit2");
    		if (limit == null) {
        		limit = coverage.findBeanByFieldValue("Limit", "LimitCd", "Limit1");
    		}
    	}
    	return limit;
    }
    
    protected boolean isAutoCoverage(String coverageCd) {
    	return (coverageCd.length() == 2 ? true : false);
    }
    
    /** From the list of AI's linked to this Risk, make sure it is a Finance type AI.
     * @param policy The Policy ModelBean
     * @param risk The Risk ModelBean
     * @return The list of AI's attached to this risk
     * @throws Exception when an error occurs
     */
    protected ModelBean[] getAllFinanceAI(ModelBean policy, ModelBean risk) throws Exception {
    	ModelBean[] ais = com.iscs.uw.common.shared.AI.getAllAI(policy, risk);
    	ArrayList<ModelBean> validAIs = new ArrayList<ModelBean>();
    	for (ModelBean ai : ais) {
    		boolean isValid = false;
    		if (ai.gets("InterestTypeCd").equals("Loss Payee")) {
    			isValid = true;
    		} else if (ai.gets("InterestTypeCd").matches(".*Mortgagee")) {
    			isValid = true;    			
    		} else if (ai.gets("InterestTypeCd").equals("LossPayee")) {
    			isValid = true;
    		} else if (ai.gets("InterestTypeCd").equals("Lienholder")) {
    			isValid = true;
    		}
    		if (isValid)
    			validAIs.add(ai);
    	}
    	return (ModelBean[]) validAIs.toArray(new ModelBean[validAIs.size()]);
    }

    /** Obtain the list of AI's linked to this Risk.  Make sure the first AI is a financial
     * type AI.  All other AI's 
     * @param policy The Policy ModelBean
     * @param risk The Risk ModelBean
     * @return The list of AI's attached to this risk
     * @throws Exception when an error occurs
     */
    protected ModelBean[] getAllAI(ModelBean policy, ModelBean risk) throws Exception {
    	ModelBean[] ais = com.iscs.uw.common.shared.AI.getAllAI(policy, risk);
    	ArrayList<ModelBean> validAIs = new ArrayList<ModelBean>();
    	ArrayList<ModelBean> otherAIs = new ArrayList<ModelBean>();
    	
    	for (ModelBean ai : ais) {
    		boolean isValid = false;
    		if (ai.gets("InterestTypeCd").equals("Loss Payee")) {
    			isValid = true;
    		} else if (ai.gets("InterestTypeCd").matches("First Mortgagee")) {
    			isValid = true;    			
    		} else if (ai.gets("InterestTypeCd").equals("LossPayee")) {
    			isValid = true;
    		} else if (ai.gets("InterestTypeCd").equals("Lienholder")) {
    			isValid = true;
    		}
    		if (isValid && validAIs.size() == 0) {
    			validAIs.add(ai);
    		} else {
    			otherAIs.add(ai);
    		}
    	}
    	validAIs.addAll(otherAIs);
    	return (ModelBean[]) validAIs.toArray(new ModelBean[validAIs.size()]);
    }
    
    /** Add two money amounts together. The amounts should not have any non-numeric characters.
     * It should also not be negative
     * @param value1 The first amount value
     * @param value2 The second amount value
     * @return The total of the two amounts added together
     */
    protected String addMoney(String value1, String value2) {
    	if (value1 == null & value2 == null)
    		return null;
		Money moneyValue1 = StringTools.stringToMoney(value1);
		Money moneyValue2 = StringTools.stringToMoney(value2);
		Money totalAmt = null;
		if (moneyValue1 == null)
			totalAmt = moneyValue2;
		else if (moneyValue2 == null)
			totalAmt = moneyValue1;
		else			
			totalAmt = moneyValue1.add(moneyValue2);
		Money zeroAmt = new Money("0.00");
		if (totalAmt.compareTo(zeroAmt) < 0) {
			totalAmt = zeroAmt;
		}

		return StringTools.formatMoney(totalAmt.toString(), false, false, false);
    }
    
    /** Get the current transaction date.
     * @param policy The Policy ModelBean
     * @return The current transaction date
     * @throws Exception when an error occurs
     */
    protected StringDate getCurrentTransactionDt(ModelBean policy)
    throws Exception {
		// Obtain the correct transaction effective date
		ModelBean transaction = getCurrentTransaction(policy);
		return transaction.getDate("TransactionEffectiveDt");
    }
    
    protected ModelBean getCurrentTransaction(ModelBean policy)
    throws Exception {
    	ModelBean basicPolicy = policy.getBean("BasicPolicy");
		String transactionNo = basicPolicy.gets("TransactionNumber");
		return basicPolicy.getBean("TransactionHistory", "TransactionNumber", transactionNo);    	
    }
    
    /** Determine the current ChoicePoint Policy status. The three statuses are Active, Cancelled, or Reinstated.
     * Also determine the date if it is a cancellation or reinstatement.  And if it is a cancellation, obtain
     * the cancel reason.  Store these into the class variables for usage throughout.
     * @param policy The Policy ModelBean
     * @return The current ChoicePoint policy status
     * @throws Exception when an error occurs
     */
    protected String getPolicyStatus(ModelBean policy)
    throws Exception {
    	if (policyStatusCd != null)
    		return policyStatusCd;
    	ModelBean basicPolicy = policy.getBean("BasicPolicy");
    	String statusCd = null;
    	// Determine if the current status is Reinstatement.  Rescind does not count
    	ModelBean transaction = getCurrentTransaction(policy);
    	if (transaction.gets("TransactionCd").equals(PM.TX_REINSTATEMENT) || transaction.gets("TransactionCd").equals(PM.TX_REINSTATEMENT_WITH_LAPSE)) {
            statusCd = getTransactionStatus(policy, basicPolicy.gets("TransactionNumber"), new String[] {});
            if (statusCd == PM.ST_CX_NOTICE)
            	statusCd = ACTIVE_STATUS;
            else
            	statusCd = REINSTATED_STATUS;
    	} else {
    		if (basicPolicy.gets("StatusCd").equals(PM.ST_CX)) 
    			statusCd = CANCEL_STATUS;
    		else if (basicPolicy.gets("StatusCd").equals(PM.ST_EXPIRED))
    			statusCd = EXPIRED_STATUS;
    		else 
    			statusCd = ACTIVE_STATUS;
    	}
    	
    	if (statusCd == null)
    		statusCd = ACTIVE_STATUS;
    	
    	// Obtain the Cancellation Date, Cancellation Reason, or Reinstatement Date if needed. 
		if (statusCd.equals(CANCEL_STATUS) || statusCd.equals(EXPIRED_STATUS)) {
			cancellationDt = getCurrentTransactionDt(policy);
    		if (statusCd.equals(CANCEL_STATUS)) {
    			ModelBean trans = getCurrentTransaction(policy);    			
    			String cancelRequestedByCd = trans.gets("CancelRequestedByCd");
    			if (cancelRequestedByCd.equals("Insured"))
    				cancelReasonCd = "CUST";
    			else if (cancelRequestedByCd.equals("Company"))
    				cancelReasonCd = "COMP";
    			else if (cancelRequestedByCd.equals("NonPay"))
    				cancelReasonCd = "NONP";
    		} else {
    			cancelReasonCd = "LAPS";
    		}    			
		} else if (statusCd.equals(REINSTATED_STATUS)) {
			reinstatementDt = getCurrentTransactionDt(policy);
		}
    	
    	policyStatusCd = statusCd;
    	return statusCd;
    }
    
    /** Get the Policy Transaction Status Given the Transaction Number
     * Read Transactions Backwards, Skipping Over Any Excluded Statuses
     * @return Transaction Status
     * @param excludeStatus String Array of Statuses to Exclude
     * @param policy Policy ModelBean
     * @param transactionNumber Transaction Number
     * @throws PMException if an Unexpected Error Occurs
     */
    protected String getTransactionStatus( ModelBean policy, String transactionNumber, String[] excludeStatus )
    throws PMException {
        try {
            // If a Transaction Status is Not Found, Return a Status of Active to Prevent an Infinite Recursive Loop
            if( transactionNumber.equals("0") )
                return PM.ST_ACTIVE;
            
            String statusCd = "";
            ModelBean basicPolicy = policy.getBean("BasicPolicy");
            ModelBean chgInfo = policy.getBean("ChangeInfo","TransactionNumber",transactionNumber);
            ModelBean[] modInfo = chgInfo.findBeansByFieldValue("ModInfo", "IdRef", basicPolicy.getId());
            for( int i = 0; i < modInfo.length; i++ ) {
                if( modInfo[i].valueEquals("Field","StatusCd") )
                    statusCd = modInfo[i].gets("NewValue");
            }
            
            // If Status is Blank, Recurse Until Status is Found
            if( statusCd.equals("") ) {
                transactionNumber = Integer.toString( Integer.parseInt(transactionNumber) - 1 );
                statusCd = getTransactionStatus( policy, transactionNumber, excludeStatus );
            }
            
            // Check for Excluded Statuses
            if( !statusCd.equals("") ) {
                boolean excludeInd = false;
                for( int i = 0; i < excludeStatus.length; i++ ) {
                    if( statusCd.equals(excludeStatus[i]) ) {
                        excludeInd = true;
                        break;
                    }
                }
                if( excludeInd ) {
                    transactionNumber = Integer.toString( Integer.parseInt(transactionNumber) - 1 );
                    statusCd = getTransactionStatus( policy, transactionNumber, excludeStatus );
                }
            }
            return statusCd;
        }
        catch( Exception e ) {
            throw new PMException(e);
        }
    }    
    
    /** Determine from the ChangeInfo's when this field last changed, and return the transaction information
     * @param policy The Policy ModelBean
     * @param bean The ModelBean containing the field
     * @param field The field name
     * @param value The field value
     * @return The transaction information
     * @throws Exception when an error occurs
     */
    protected ModelBean fieldChangedInTransaction(ModelBean policy, ModelBean bean, String field, String value)
    throws Exception {
    	if (policy.getBean("BasicPolicy").gets("TransactionNumber").equals("1"))
    		return policy.getBean("BasicPolicy").getBean("TransactionHistory");
    	ModelBean[] changeInfos = PM.getChangeInfoTransactionOrdered(policy, 0, "Descending");
    	for (ModelBean changeInfo : changeInfos) {
        	ModelBean[] beanChanges = changeInfo.findBeansByFieldValue("ModInfo", "IdRef", bean.getId()); 
    		for (ModelBean beanChange : beanChanges) {
    			if (beanChange.gets("Field").equals(field)) {
    				if (beanChange.gets("NewValue").equals(value)) {
    					return policy.getBean("BasicPolicy").findBeanByFieldValue("TransactionHistory", "ChangeInfoRef", changeInfo.getId());
    				}
    			}
    		}
    	}
    	return null;
    	// There should have been a change detected.  Data error or other, so return failure
    	//throw new Exception("Unable to determine when the field " + field + " in ModelBean " + bean.getBeanName() + " changes.");
    }
    
    /** Determine from the ChangeInfo's when this field first changed, and return the transaction information
     * @param policy The Policy ModelBean
     * @param bean The ModelBean containing the field
     * @param field The field name
     * @param value The field value
     * @return The transaction information
     * @throws Exception when an error occurs
     */
    protected ModelBean fieldFirstChangedInTransaction(ModelBean policy, ModelBean bean, String field, String value)
    throws Exception {
    	if (policy.getBean("BasicPolicy").gets("TransactionNumber").equals("1"))
    		return policy.getBean("BasicPolicy").getBean("TransactionHistory");
    	ModelBean[] changeInfos = PM.getChangeInfoTransactionOrdered(policy, 0, "Ascending");
    	for (ModelBean changeInfo : changeInfos) {
        	ModelBean[] beanChanges = changeInfo.findBeansByFieldValue("ModInfo", "IdRef", bean.getId()); 
    		for (ModelBean beanChange : beanChanges) {
    			if (beanChange.gets("Field").equals(field)) {
    				if (beanChange.gets("NewValue").equals(value)) {
    					return policy.getBean("BasicPolicy").findBeanByFieldValue("TransactionHistory", "ChangeInfoRef", changeInfo.getId());
    				}
    			}
    		}
    	}
    	// There should have been a change detected.  Data error or other, so return failure
    	//throw new Exception("Unable to determine when the field " + field + " in ModelBean " + bean.getBeanName() + " changes.");
    	return null;
    }
        
    
    /** Determine from the ChangeInfo's when this field first Added, and return the transaction information
     * @param policy The Policy ModelBean
     * @param bean The ModelBean containing the field
     * @param field The field name
     * @param value The field value
     * @return The transaction information
     * @throws Exception when an error occurs
     */
    protected ModelBean fieldFirstAddedInTransaction(ModelBean policy, ModelBean bean, String field, String actionCd)
    throws Exception {
    	if (policy.getBean("BasicPolicy").gets("TransactionNumber").equals("1"))
    		return policy.getBean("BasicPolicy").getBean("TransactionHistory");
    	ModelBean[] changeInfos = PM.getChangeInfoTransactionOrdered(policy, 0, "Ascending");
    	for (ModelBean changeInfo : changeInfos) {
        	ModelBean[] beanChanges = changeInfo.findBeansByFieldValue("ModInfo", "IdRef", bean.getId()); 
    		for (ModelBean beanChange : beanChanges) {
    			if (beanChange.gets("Field").equals(field)) {
    				if (beanChange.gets("ActionCd").equals(actionCd)) {
    					return policy.getBean("BasicPolicy").findBeanByFieldValue("TransactionHistory", "ChangeInfoRef", changeInfo.getId());
    				}
    			}
    		}
    	}
    	// There should have been a change detected.  Data error or other, so return failure
    	//throw new Exception("Unable to determine when the field " + field + " in ModelBean " + bean.getBeanName() + " changes.");
    	return null;
    }    
    /** Obtain the first optional deductible.  For property it is the All Perils deductible. 
     * For auto it is the collision deductible.
     * @param policy The Policy ModelBean
     * @param risk The risk ModelBean
     * @return The deductible amount
     * @throws Exception when an error occurs
     */ 
    protected String getCoverageDeductible1(ModelBean policy, ModelBean risk)
    throws Exception {
    	if (isAutoPolicy) {
    		ModelBean collision = risk.findBeanByFieldValue("Coverage", "CoverageCd", "Collision");
    		if (collision != null) {
    			ModelBean deductible = collision.getBean("Deductible");
    			if (deductible != null)
    				return deductible.gets("Value");
    		}
    	} else {
    		ModelBean covA = risk.findBeanByFieldValue("Coverage", "CoverageCd", "CovA");
    		if (covA != null) {
    			ModelBean deductible = covA.getBean("Deductible");
    			if (deductible != null)
    				return deductible.gets("Value");
    		}    		
    	}
    	return "";
    }
    
    /** Obtain the second optional deductible.  For property it is the Theft deductible. 
     * For auto it is the comprehensive deductible.
     * @param policy The Policy ModelBean
     * @param risk The risk ModelBean
     * @return The deductible amount
     * @throws Exception when an error occurs
     */ 
    protected String getCoverageDeductible2(ModelBean policy, ModelBean risk)
    throws Exception {
    	if (isAutoPolicy) {
    		ModelBean collision = risk.findBeanByFieldValue("Coverage", "CoverageCd", "Comprehensive");
    		if (collision != null) {
    			ModelBean deductible = collision.getBean("Deductible");
    			if (deductible != null)
    				return deductible.gets("Value");
    		}
    	} else {
    		ModelBean covA = risk.findBeanByFieldValue("Coverage", "CoverageCd", "CovA");
    		if (covA != null) {
    			ModelBean deductible = covA.getBean("Deductible");
    			if (deductible != null)
    				return deductible.gets("Value");
    		}    		    		
    	}
    	return "";
    }
    
	/** Validate if needed any Current Carrier Contribution setup information
	 * @param The Policy ModelBean
	 * @throws Exception when an error occurs or a validation error is found
	 */
    public void validateContributionSetup(ModelBean policy) throws Exception {
    	// Validate all Products
    	if (policy == null) {
    		ModelBean productMaster = ProductMaster.getProductMaster("UW").getBean();
    		
			policy = new ModelBean("Policy");
			ModelBean basicPolicy = new ModelBean("BasicPolicy");
    		ModelBean[] products = productMaster.findBeansByFieldValue("Product", "CPCurrentCarrierEnabled", "Yes");
    		for (ModelBean product : products) {
				ModelBean[] versions = product.getBeans("ProductVersion");
				for (ModelBean version : versions) {
					basicPolicy.setValue("ProductVersionIdRef", version.getId());
		        	mapper.validateMappingSource(policy);
				}				
    		}    		
    	} else {
        	mapper.validateMappingSource(policy);	
    	}
    }
    
    /** Determine if the insured address has been verified
     * @param policy The Policy or Application ModelBean
     * @return true/false if the insured mailing address has been verified
     * @throws Exception when an error occurs
     */
    public boolean isInsuredAddressVerified(ModelBean policy) throws Exception {
    	boolean isVerified = false;
		ModelBean insuredParty = policy.getBean("Insured").getBean("PartyInfo", "PartyTypeCd", "InsuredParty");
		ModelBean mailingLookupAddr = insuredParty.getBean("Addr", "AddrTypeCd", "InsuredLookupAddr");
		if (mailingLookupAddr == null) {
			ModelBean mailingAddr = insuredParty.getBean("Addr", "AddrTypeCd", "InsuredMailingAddr");
			if (new AddressRenderer().isAddressVerified(mailingAddr) || AddressTools.hasParsedData(mailingAddr)) {
				isVerified = true;
			}
		} else {
			isVerified = true;
		}
    	return isVerified;
    }
    
    /** Check to see if the insured address has been verified, if so return the address with the broken down parts needed.
     *  Else call address verification and return an address with the broken down parts needed
     * @param policy The Policy ModelBean
     * @return The verified and parsed parts of the address
     * @throws Exception when an error occurs
     */
    public ModelBean getInsuredAddressVerified(ModelBean policy) throws Exception {
		ModelBean insuredParty = policy.getBean("Insured").getBean("PartyInfo", "PartyTypeCd", "InsuredParty");
		ModelBean mailingLookupAddr = insuredParty.getBean("Addr", "AddrTypeCd", "InsuredLookupAddr");
		if (mailingLookupAddr == null) {
			ModelBean mailingAddr = insuredParty.getBean("Addr", "AddrTypeCd", "InsuredMailingAddr");
			mailingLookupAddr = ChoicePointContributionHelper.validateAddress(mailingAddr);
		}
    	return mailingLookupAddr;
    }



}
