package com.ric.interfaces.choicepoint.contribution.currentcarrier;

import net.inov.tec.beans.ModelBean;
import net.inov.tec.date.StringDate;

import com.ric.interfaces.choicepoint.contribution.UWPolicyInformation;
import com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.entity.CurrentCarrierInfo;

/** Interface to call the Current Carrier Contribution processor.  This will process the 
 * Innovation Policy ModelBean information into the POJO data structure.
 * 
 * @author allend
 *
 */
public interface CurrentCarrierContributionHandler {
	
	/** Process the Policy information and save it into the pojo data model
	 * @param policy The Policy ModelBean
	 * @param account The Account ModelBean
	 * @param policyInfo Policy information needed but not stored in Current Carrier 
	 * @param todayDt The date this contribution record was processed
	 * @param isInitialContributionInd Indicator if this is to be considered an initial contribution
	 * @return The list of Current Carrier information records
	 * @throws Exception when an error occurs
	 */
	public CurrentCarrierInfo[] createContribution(ModelBean policy, ModelBean account, UWPolicyInformation policyInfo, StringDate todayDt, boolean isInitialContributionInd,ModelBean errors) throws Exception;	
	
	/** Validate if needed any Current Carrier Contribution setup information
	 * @param policy The Policy ModelBean
	 * @throws Exception when an error occurs or a validation error is found
	 */
	public void validateContributionSetup(ModelBean policy) throws Exception;
	
}
