package com.ric.interfaces.choicepoint.contribution.currentcarrier.handler;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.thread.ManagedTask;
import net.inov.tec.thread.MasterThreadManager;
import net.inov.tec.web.cmm.AdditionalParams;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.iscs.common.render.StringRenderer;
import com.iscs.common.shared.SystemData;
import com.iscs.common.tech.bean.pojomapper.BeanPojoMapper;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.interfaces.choicepoint.contribution.ChoicePointContributionException;
import com.ric.interfaces.choicepoint.contribution.currentcarrier.CurrentCarrierBuilder;
import com.iscs.uw.interfaces.choicepoint.contribution.currentcarrier.entity.CurrentCarrierInfo;

/** Process the current policy transaction into a Current Carrier record.
 *  
 * @author allend
 *
 */
public class CurrentCarrierProcess extends InnovationIBIZHandler {

    /** Creates a new instance of CurrentCarrierProcess
     * @throws Exception never
     */
    public CurrentCarrierProcess() throws Exception {
    }
    
    /** Processes a generic service request.
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
    public ModelBean process() throws IBIZException, ServiceHandlerException {
        try {
            // Log a Greeting
            Log.debug("Processing CurrentCarrierProcess...");
            ModelBean rs = this.getHandlerData().getResponse();
            JDBCData data = this.getHandlerData().getConnection();
            ModelBean responseParams = rs.getBean("ResponseParams");
            ModelBean request = this.getHandlerData().getRequest();
            String requestName=  request.getBeanName();
            StringDate todayDt = DateTools.getStringDate(responseParams);
            AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest());                                
            ModelBean hasPaymentAmt = ap.findBeanByFieldValue("Param", "Name", "TransactionInfo.PaymentAmt");
            String paidAmt = "";
            ModelBean errors = new ModelBean("Errors");
            if(hasPaymentAmt!=null){
            	paidAmt = hasPaymentAmt.gets("Value");
            }
            String payoffAmt = "";
            ModelBean policy = rs.getBean("Policy");
            ModelBean basicPolicy = policy.getBean("BasicPolicy");
            
            String finalPremiumAmt = basicPolicy.gets("FinalPremiumAmt");
    		String payplanCd = basicPolicy.gets("PayPlanCd");
    		if(payplanCd.matches(".*Mortgagee.*")){
	    		if((paidAmt!="" && paidAmt!=null) && (finalPremiumAmt!="" && finalPremiumAmt!=null) )
	    			payoffAmt = StringRenderer.subtractMoney(finalPremiumAmt, paidAmt).replace("$", "");
    		}
    		ModelBean account = null;
    		ModelBean accountInfo = null;
            if (rs.hasBeanField("DTOAccount"))
            	account = rs.getBean("DTOAccount");
            
            // Create the Current Carrier Contribution record if needed for this policy
            CurrentCarrierBuilder builder = new CurrentCarrierBuilder();
            if (builder.contributionRequired(policy)) {
            	 String managedTaskId = ap.gets("ManagedTaskId","");   
            	if( !managedTaskId.equals("")){
                	ManagedTask managedTask = MasterThreadManager.getInstance().getTask(managedTaskId);
                	managedTask.setProgress(ap.gets("ManagedTaskPrefix") + " " + "Processing Current Carrier Contributions");
                }
            	if(payplanCd.matches(".*Mortgagee.*") && payoffAmt.equals("") && !policy.gets("AccountRef").equals("")){
	            	if(accountInfo==null){
	            		accountInfo = data.selectModelBean( new ModelBean("Account"),policy.gets("AccountRef"));
	            		payoffAmt= accountInfo.gets("TotalAmt");
	            	}
            	}
            	
            	
            	String paymentDueDate=null;
            	if(policy.gets("AccountRef")!=null && !policy.gets("AccountRef").equals("")){
	            	ModelBean arInvoice = null;
	            	if(accountInfo!=null){
						arInvoice = accountInfo.getBean("ARInvoice", "StatusCd", "Rendered");
						if(arInvoice!=null){
							paymentDueDate=formatDate(arInvoice.gets("DueDt"),"MM/dd/yyyy");
						}else if(arInvoice==null){
							arInvoice = accountInfo.getBean("ARInvoice", "StatusCd", "Open");
							if(arInvoice!=null){
								paymentDueDate=formatDate(arInvoice.gets("DueDt"),"MM/dd/yyyy");
							}
						}
					}
            	}
                CurrentCarrierInfo[] ccInfos = builder.processPeriod(policy, account, todayDt,payoffAmt,paymentDueDate,errors,requestName, accountInfo);
                
                // Save the contribution record to the database for future reporting
                if(ccInfos!=null){
                	for (CurrentCarrierInfo ccInfo : ccInfos) {
                		ModelBean ccInfoBean = BeanPojoMapper.toModelBean(ccInfo);
                		/** Change addDt to postingDt instead of todayDt to get accurate current carrier reporting */
                		String postingDt = SystemData.getValue(data, "BookDt");                   
                		ccInfoBean.setValue("BookDt", postingDt);
                		ccInfoBean.setValue("AddDt", DateTools.getDate(rs.getBean("ResponseParams")));
                		ccInfoBean.setValue("AddTm", DateTools.getTime(rs.getBean("ResponseParams")));
                		data.saveModelBean(ccInfoBean);            	                	
                	}
                }
            }

            // Return the Response ModelBean
            return rs;
        }
        catch( ChoicePointContributionException ce ) {
        	try {
        		addErrorMsg("General", ce.getMessage(), ErrorTools.GENERIC_BUSINESS_ERROR, ErrorTools.SEVERITY_ERROR);
        	} catch (Exception e) {
                e.printStackTrace();
                throw new ServiceHandlerException(e);        		
        	}        	
            throw new IBIZException(ce);
        }
        catch( Exception e ) {
            e.printStackTrace();
            throw new ServiceHandlerException(e);
        }
    }
    public static String formatDate(String date,String patternformat){
		String dateformat = "yyyyMMdd";
		SimpleDateFormat sf = new SimpleDateFormat(dateformat);
		SimpleDateFormat pf = new SimpleDateFormat(patternformat);
		String formattedDate = null;
		try {
			 formattedDate =  sf.format(pf.parse(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formattedDate;
	}
}

