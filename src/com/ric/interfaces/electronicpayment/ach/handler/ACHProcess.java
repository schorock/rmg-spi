package com.ric.interfaces.electronicpayment.ach.handler;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.data.JDBCData.QueryResult;
import net.inov.tec.date.StringDate;

import com.iscs.common.render.DynamicString;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.InnovationUtils;
import com.iscs.common.utility.SessionInfo;
import com.iscs.common.utility.io.FileTools;
import com.ric.interfaces.electronicpayment.ach.ACH;
import com.iscs.interfaces.utility.record.Record;

/**
 * Builds the Ach request file
 *
 * @author patriciat
 */

public class ACHProcess extends InnovationIBIZHandler {

	public ACHProcess() {
	}

	public ModelBean process() throws IBIZException, ServiceHandlerException {
		JDBCData data = null;
		try {
			String sweepInd = null;

			// Log a Greeting
			Log.debug("Processing ACHProcess...New ACH");
			ModelBean rq = this.getHandlerData().getRequest();
			ModelBean rs = this.getHandlerData().getResponse();
			ModelBean ap = rq.getBean("RequestParams").getBean("AdditionalParams");
			data = this.getHandlerData().getConnection();
			StringDate todayDt = DateTools.getStringDate();
			String todayTm = DateTools.getTime(rs);
			SessionInfo info = new SessionInfo(rq.getBean("RequestParams"), rs.getBean("ResponseParams"), todayDt);

			// If the ACH process file override is specified, use it (it is
			// suffixed below with the destination because multiple files might
			// actually be created)
			String overrideUrl = null;
			ModelBean param = ap.findBeanByFieldValue("Param", "Name", "AchProcessFile");
			if (param != null) {
				if (!param.gets("Value").equals(""))
					overrideUrl = param.gets("Value");
			}

			ModelBean[] sortedList = getPaymentsSorted(data);

			if (sortedList.length > 0) {

				ModelBean errors = new ModelBean("Errors");

				// Each ImmediateDestination gets its own ach file
				String destination = "";
				String agentTrustInd = "";
				URL fileUrl = null;
				URL sweepfileUrl = null;
				Boolean agent = null;
				Boolean customer = null;
				String agentFile = "";
				String customerFile = "";
				ArrayList<ModelBean> al = new ArrayList<ModelBean>();
				ArrayList<ModelBean> ai = new ArrayList<ModelBean>();
				for (ModelBean payment : sortedList) {

					ModelBean agentTrustBean = payment.getBean("ElectronicPaymentSource");
					ModelBean contractBean = payment.getBean("ElectronicPaymentContract");

					agentTrustInd = agentTrustBean.gets("AgentTrustInd");
					if (!agentTrustInd.equals("")) {
						sweepInd = "Yes";
						agent = new Boolean(true);
						agentFile = "Yes";
						if (overrideUrl != null) {
							fileUrl = new URL(overrideUrl + "." + destination);
						} else {
							sweepfileUrl = getFileURL(contractBean, sweepInd);
						}
						ai.add(payment);
					}
					if (agentTrustInd.equals("")) {
						//ModelBean contractBean = payment.getBean("ElectronicPaymentContract");
						sweepInd = "No";
						customer = new Boolean(false);
						customerFile = "No";
						if (destination.equals("")) {

							destination = contractBean.gets("ACHImmediateDestination");
							if (overrideUrl != null) {
								fileUrl = new URL(overrideUrl + "." + destination);
							} else {
								fileUrl = getFileURL(contractBean, sweepInd);
							}
							al.add(payment);

						} else if (destination.equals(contractBean.gets("ACHImmediateDestination"))) {

							al.add(payment);

						} else {

							// new destination - create an ach file for the
							// previous destination
							if(!ai.isEmpty()){
							createACHFile(data, ai, sweepfileUrl, info, todayDt, todayTm, errors, true,agentFile);
							}
							if (!al.isEmpty()){
							createACHFile(data, al, fileUrl, info, todayDt, todayTm, errors, false,customerFile);
							}

							al = new ArrayList<ModelBean>();
							if (overrideUrl != null) {
								fileUrl = new URL(overrideUrl + "." + destination);
							} else {
								fileUrl = getFileURL(contractBean, sweepInd);
							}
							al.add(payment);
							destination = contractBean.gets("ACHImmediateDestination");

						}
					}
				}
				// create the last file
				
				if (!ai.isEmpty()){
					createACHFile(data, ai, sweepfileUrl, info, todayDt, todayTm, errors, true,agentFile);
					}
				if(!al.isEmpty()){
					createACHFile(data, al, fileUrl, info, todayDt, todayTm, errors, false,customerFile);
					}
					

				if (!rs.hasErrors()) {
					data.commit();
				} else {
					throw new IBIZException("Validation");
				}

			}

			return rs;

		} catch (IBIZException e) {
			try {
				data.rollback();
			} catch (Exception ex) {
				Log.error(ex);
			}
			;
			throw e;
		} catch (Exception e) {
			try {
				data.rollback();
			} catch (Exception ex) {
				Log.error(ex);
			}
			;
			throw new ServiceHandlerException(e);
		}

	}

	/**
	 * Create an ACH file
	 * 
	 * @param data
	 *            JDBCData The data connection
	 * @param al
	 *            ArrayList The array list containing all the records for the
	 *            file
	 * @param fileUrl
	 *            URL The URL
	 * @param info
	 *            SessionInfo The SessionInfo
	 * @param todayDt
	 *            StringDate Today's date
	 * @param todayTm
	 *            String Current time
	 * @param errors
	 *            ModelBean The errors model bean
	 * @param agent 
	 * @throws Exception
	 */
	private void createACHFile(JDBCData data, ArrayList<ModelBean> al, URL fileUrl, SessionInfo info,
			StringDate todayDt, String todayTm, ModelBean errors, Boolean agent, String sweepInd) throws Exception {

		try {
			ACH ach = new ACH();
			ModelBean[] recordSet = (ModelBean[]) al.toArray(new ModelBean[al.size()]);
			Record[] achRequests = ach.generateACHRecord(data, recordSet, info, todayDt, todayTm, errors, agent,sweepInd);
			File file = new java.io.File(fileUrl.getFile());

			for (Record achRequest : achRequests) {
				FileTools.appendText(file, achRequest.toString(), true);
			}

		} catch (Exception e) {
			Log.error(e);
			throw e;
		}

	}

	/**
	 * Retrieve and sort the electronic payment beans It sorts the beans by the
	 * ImmediateDestination first (all the records going to the same bank) then
	 * it will sort by the ACHName (used to group records belonging to different
	 * carriers) then it will sort by StandardEntryClassCode then it will sort
	 * by StatementAccountRef(used to group records on the same statement
	 * account) finally it will sort by Payment Source Details(used to group
	 * records on the same payment source so that 1 ACH request is generated for
	 * each statement and per each payment source)
	 * 
	 * @param data
	 *            JDBCData The data connection
	 * @return The sorted electronic payment beans
	 * @throws Exception
	 */

	public static ModelBean[] getPaymentsSorted(JDBCData data) throws Exception {
		try {

			// Create a List Object to Pass to the Collections Class
			List<ModelBean> sortList = new ArrayList<ModelBean>();

			// Get All the Unsorted electronic payment ModelBeans
			QueryResult[] results = data.doQueryFromLookup("ElectronicPayment", new String[] { "StatusCd", "MethodCd" },
					new String[] { "=", "=" }, new String[] { "Added", "ACH" }, 0);
			ModelBean[] electronicPayment = new ModelBean[results.length];

			for (int i = 0; i < results.length; i++) {
				electronicPayment[i] = data.selectModelBean(new ModelBean("ElectronicPayment"),
						Integer.parseInt(results[i].getSystemId()));
				sortList.add(electronicPayment[i]);
			}

			// Sort List
			Collections.sort(sortList, new Comparator() {
				public int compare(Object o1, Object o2) throws ClassCastException {
					try {
						ModelBean bean1 = (ModelBean) o1;
						ModelBean bean2 = (ModelBean) o2;

						// Sort By Immediate Destination
						String destination1 = bean1.getBean("ElectronicPaymentContract")
								.gets("ACHImmediateDestination");
						String destination2 = bean2.getBean("ElectronicPaymentContract")
								.gets("ACHImmediateDestination");
						if (destination2.equals(destination1)) {

							String carrier1 = "";
							String carrier2 = "";
							// If same destination, sort by carrier name
							if (bean1.gets("TypeCd").equalsIgnoreCase("DirectPayment")) {
								carrier1 = bean1.getBean("ElectronicPaymentDestination").gets("ACHName");
							} else if (bean1.gets("TypeCd").equalsIgnoreCase("DirectDeposit")) {
								carrier1 = bean1.getBean("ElectronicPaymentSource").gets("ACHName");
							}
							if (bean2.gets("TypeCd").equalsIgnoreCase("DirectPayment")) {
								carrier2 = bean2.getBean("ElectronicPaymentDestination").gets("ACHName");
							} else if (bean2.gets("TypeCd").equalsIgnoreCase("DirectDeposit")) {
								carrier2 = bean2.getBean("ElectronicPaymentSource").gets("ACHName");
							}

							// if same carrier then, sort by
							// StandardEntryClassCd
							if (carrier2.equals(carrier1)) {

								String standardEntryClassCd1 = "";
								String standardEntryClassCd2 = "";
								if (bean1.gets("TypeCd").equalsIgnoreCase("DirectPayment")) {
									standardEntryClassCd1 = bean1.getBean("ElectronicPaymentSource")
											.gets("ACHStandardEntryClassCd");
								} else if (bean1.gets("TypeCd").equalsIgnoreCase("DirectDeposit")) {
									standardEntryClassCd1 = bean1.getBean("ElectronicPaymentDestination")
											.gets("ACHStandardEntryClassCd");
								}
								if (bean2.gets("TypeCd").equalsIgnoreCase("DirectPayment")) {
									standardEntryClassCd2 = bean2.getBean("ElectronicPaymentSource")
											.gets("ACHStandardEntryClassCd");
								} else if (bean2.gets("TypeCd").equalsIgnoreCase("DirectDeposit")) {
									standardEntryClassCd2 = bean2.getBean("ElectronicPaymentDestination")
											.gets("ACHStandardEntryClassCd");
								}

								// if same StandardEntryClassCd, then sort by
								// statement account ref
								if (standardEntryClassCd1.equals(standardEntryClassCd2)) {
									String statementAccountRef1 = bean1.gets("StatementAccountRef");
									String statementAccountRef2 = bean2.gets("StatementAccountRef");

									String paymentSourceKey1 = "";
									String paymentSourceKey2 = "";

									// If same Statement Account Ref, sort by
									// payment source
									if (statementAccountRef1.equals(statementAccountRef2)
											&& !statementAccountRef1.equals("")) {
										paymentSourceKey1 = ACH.getPaymentSourceKey(bean1);
										paymentSourceKey2 = ACH.getPaymentSourceKey(bean2);

										return paymentSourceKey1.compareTo(paymentSourceKey2);

									} else {
										return statementAccountRef1.compareTo(statementAccountRef2);
									}

								} else {
									return standardEntryClassCd1.compareTo(standardEntryClassCd2);
								}

							} else {
								return carrier1.compareTo(carrier2);
							}
						} else {

							// Return the ModelBean that had the Earlier
							// Destination
							return destination1.compareTo(destination2);
						}
					} catch (Exception e) {
						throw new ClassCastException(
								"Error occurred trying to compare fields for sorting." + e.toString());
					}
				}
			});

			// Return All the Sorted Note ModelBeans
			return (ModelBean[]) sortList.toArray(new ModelBean[sortList.size()]);

		} catch (Exception e) {
			e.printStackTrace();
			Log.error(e);
			throw new Exception(e);
		}
	}

	/**
	 * Returns the FileURL from the contract bean
	 * 
	 * @param contractBean
	 *            ModelBean The ElectronicPaymentContract bean
	 * @return the fileURL
	 * @throws Exception
	 *             when an error occurs
	 */

	private URL getFileURL(ModelBean contractBean, String sweepInd) throws Exception {

		URL fileUrl = null;

		if (contractBean.gets("ACHProcessUrl").equals(""))
			throw new IBIZException("ACHProcessUrl not set up");
		if (sweepInd.equalsIgnoreCase("Yes")) {
			String directoryPath = DynamicString.render(new ModelBean("Company"),
					InnovationUtils.getEnvironmentVariable("CompanyACH", "processSweepURL", ""));
			fileUrl = new URL(directoryPath);
		} else {
			fileUrl = new URL(DynamicString.render(contractBean, contractBean.gets("ACHProcessUrl")));
		}

		if (!fileUrl.getProtocol().equalsIgnoreCase("File"))
			throw new IBIZException("URL protocol is not of type File");

		return fileUrl;
	}

}
