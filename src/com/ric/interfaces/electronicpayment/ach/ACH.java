package com.ric.interfaces.electronicpayment.ach;

import java.io.BufferedReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import net.inov.biz.server.ServiceContext;
import net.inov.mda.MDAOption;
import net.inov.mda.MDATable;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.data.JDBCData.QueryResult;
import net.inov.tec.data.JDBCLookup;
import net.inov.tec.date.StringDate;
import net.inov.tec.file.FileIO;
import net.inov.tec.obj.ClassHelper;
import net.inov.tec.xml.XmlDoc;

import com.ibm.math.BigDecimal;
import com.iscs.common.admin.calendar.Calendar;
import com.iscs.common.mda.Store;
import com.iscs.common.mda.object.MDATemplate;
import com.iscs.common.render.DynamicString;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.SessionInfo;
import com.iscs.common.utility.StringTools;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.counter.Counter;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.common.utility.table.CodeRefTools;
import com.iscs.interfaces.electronicpayment.AccountContractGet;
import com.iscs.interfaces.electronicpayment.ElectronicPaymentException;
import com.iscs.interfaces.electronicpayment.ElectronicPaymentExceptionHandler;
import com.iscs.interfaces.utility.record.Record;
import com.iscs.interfaces.utility.record.RecordGenerator;


public class ACH {

	/**
	 * Worker class for the ACH subsystem
	 * @author patriciat
	 */

	protected XmlDoc achRecord;
	protected XmlDoc fieldTypes;	
	protected int fileTotalCredit =0;
	protected int fileTotalDebit = 0;
	protected int batchTotalCredit = 0;
	protected int batchTotalDebit = 0;
	protected int lineCt = 0;
	protected int batchEntryCt = 0;
	protected int fileEntryCt = 0;	
	long fileEntryHash = 0;	
	long batchEntryHash = 0;	
	int batchCt = 0;
	protected static BigDecimal zeroAmt = new BigDecimal("0.00");
	
	public static String FINANCIAL_TYPE = "Financial Institution";

	/** Generates all the records needed to build an ACH file     
	 * @param data JDBCData The date connection
	 * @param electronicPayment ModelBean[] The Electronic Payment list
	 * @param sessionInfo SessionInfo
	 * @param todayDt StringDate The system date
	 * @param todayTm String The system time
	 * @param errors ModelBean The errors modelbean
	 * @return ACH records Record[] The record array
	 */

	public Record[] generateACHRecord(JDBCData data,ModelBean[] electronicPayment, SessionInfo sessionInfo, StringDate todayDt, String todayTm,
			ModelBean errors, Boolean agent,String sweepInd) throws ElectronicPaymentException{

		try {
			ArrayList<Record> array = new ArrayList<Record>();
			String carrierName = "";
			String carrierRoutingNumber = "";
			String carrierAccountNumber = "";
			String companyId = "";
			String nextCarrierName = "";
			String nextAccountNumber = "";
			String standardEntryClassCd = "";
			String nextStandardEntryClassCd = "";
			String includeBalanceRecordInd = "";
			String statementAccountRef = "";	
			String electronicPaymentStatementId = "";		
			String paymentSourceKey = "";

			int batchAmt = 0;					
			String batchNum = "";

			String runTm = sessionInfo.getTime();
			runTm = runTm.substring(0,2)+runTm.substring(3,5);

			init();
			RecordGenerator generator = new RecordGenerator();
			generator.setRecordLayoutDoc(achRecord);
			generator.setFieldTypesDoc(fieldTypes);
			
			StringDate settlementDt = getSettlementDt(data,todayDt, errors, agent);
			
			if (settlementDt != null){				
				
				// Only look at the first indicator to include the balance record
				includeBalanceRecordInd = electronicPayment[0].getBean("ElectronicPaymentContract").gets("ACHIncludeBalanceRecordInd");
				
				for (int i =0;i<electronicPayment.length; i++){
					if (electronicPayment[i].gets("TypeCd").equalsIgnoreCase("DirectPayment")){
						nextCarrierName = electronicPayment[i].getBean("ElectronicPaymentDestination").gets("ACHName");
						nextAccountNumber = electronicPayment[i].getBean("ElectronicPaymentDestination").gets("ACHBankAccountNumber");
						nextStandardEntryClassCd = electronicPayment[i].getBean("ElectronicPaymentSource").gets("ACHStandardEntryClassCd");
					} else {
						nextCarrierName = electronicPayment[i].getBean("ElectronicPaymentSource").gets("ACHName");
						nextAccountNumber = electronicPayment[i].getBean("ElectronicPaymentSource").gets("ACHBankAccountNumber");
						nextStandardEntryClassCd = electronicPayment[i].getBean("ElectronicPaymentDestination").gets("ACHStandardEntryClassCd");
					}
					if (!carrierName.equals(nextCarrierName) || !carrierAccountNumber.equals(nextAccountNumber) || !standardEntryClassCd.equals(nextStandardEntryClassCd)){
						// close the last batch
						if (batchCt > 0){
							batchAmt = batchTotalCredit - batchTotalDebit;
							if( !includeBalanceRecordInd.equalsIgnoreCase("No") )
								array.add(createBalanceRecord(generator, batchAmt, carrierRoutingNumber, carrierAccountNumber));		
							array.add(createBatchControlRecord(generator,batchNum, companyId, carrierRoutingNumber,sweepInd));
						}
						if (fileEntryCt == 0){
							array.add(createFileHeaderRecord(generator,electronicPayment[i],todayDt, runTm ));		
						}
						batchNum = StringTools.leftPad(Counter.nextNumber(data, "achbatch"), "0",7);
						array.add(createBatchHeaderRecord(generator,electronicPayment[i],batchNum,todayDt, settlementDt,sweepInd));
						// reset all batch variables
						batchEntryCt = 0;
						batchAmt =  0;
						batchTotalCredit = 0;
						batchTotalDebit = 0;
						batchEntryHash = 0;
						batchCt++;
	
					}
					
					if( !electronicPayment[i].gets("StatementAccountRef").equals("") ){									
						BigDecimal totalAmt = new BigDecimal("0.00");
						boolean firstInd = false;
						
						//Check if the Payments belong to same statement and have the same payment details
						if( statementAccountRef.equals("") || !statementAccountRef.equals(electronicPayment[i].gets("StatementAccountRef")) || !getPaymentSourceKey(electronicPayment[i]).equals(paymentSourceKey)){				
							paymentSourceKey = getPaymentSourceKey(electronicPayment[i]);
							firstInd = true;
							statementAccountRef = electronicPayment[i].gets("StatementAccountRef");
							electronicPaymentStatementId = "EP" + StringTools.leftPad(Counter.nextNumber(data, "electronicpaymentstatementid"), "0", 10);
							totalAmt = updateElectronicPayment(data, electronicPayment, statementAccountRef, electronicPaymentStatementId, paymentSourceKey);
						}	
						Record record = createPPDRecord(generator,data,electronicPayment[i], batchNum, todayDt, todayTm, settlementDt, electronicPaymentStatementId, totalAmt, firstInd);
						if( record != null){
							array.add(record);
						}
					} else {
						array.add(createPPDRecord(generator,data,electronicPayment[i], batchNum, todayDt, todayTm, settlementDt));
					}
				
					if (electronicPayment[i].gets("TypeCd").equalsIgnoreCase("DirectPayment")){
						carrierName = electronicPayment[i].getBean("ElectronicPaymentDestination").gets("ACHName");
						// Obtain the current ACH Routing Number
						carrierRoutingNumber = getProcessedRoutingNumber(data, electronicPayment[i].getBean("ElectronicPaymentDestination").gets("ACHRoutingNumber"));
						carrierAccountNumber = electronicPayment[i].getBean("ElectronicPaymentDestination").gets("ACHBankAccountNumber");
						standardEntryClassCd = electronicPayment[i].getBean("ElectronicPaymentSource").gets("ACHStandardEntryClassCd");
					} else {
						carrierName = electronicPayment[i].getBean("ElectronicPaymentSource").gets("ACHName");
						// Obtain the current ACH Routing Number
						carrierRoutingNumber = getProcessedRoutingNumber(data, electronicPayment[i].getBean("ElectronicPaymentSource").gets("ACHRoutingNumber"));
						carrierAccountNumber = electronicPayment[i].getBean("ElectronicPaymentSource").gets("ACHBankAccountNumber");
						standardEntryClassCd = electronicPayment[i].getBean("ElectronicPaymentDestination").gets("ACHStandardEntryClassCd");
					}
					companyId = electronicPayment[i].getBean("ElectronicPaymentContract").gets("ACHCompanyId");
					
				}		
	
				batchAmt = batchTotalCredit - batchTotalDebit;
				if( !includeBalanceRecordInd.equalsIgnoreCase("No") )
					array.add(createBalanceRecord(generator, batchAmt, carrierRoutingNumber, carrierAccountNumber));		
				array.add(createBatchControlRecord(generator,batchNum, companyId, carrierRoutingNumber,sweepInd));
				array.add(createFileControlRecord(generator,batchCt));
				Record[] blockFiller = createBlockFillerRecord(generator);
				
				if (blockFiller.length > 0){
					for (int i=0;i<blockFiller.length;i++)
						array.add(blockFiller[i]);
				}
			}
			
			return (Record[]) array.toArray(new Record[array.size()]);

		} catch (Exception e){
			e.printStackTrace();
			Log.error(e);
			throw new ElectronicPaymentException(e.getMessage());			
		}

	}

	/** Generates the file header record  
	 * @param generator RecordGenerator The Record Generator class
	 * @param electronicPayment ModelBean The Electronic Payment bean
	 * @param runDt StringDate The Run date
	 * @param runTm String The Run Time
	 * @return record Record The file header record
	 */

	public Record createFileHeaderRecord(RecordGenerator generator,ModelBean electronicPayment,StringDate runDt, String runTm)throws Exception{
		Record record = generator.createRecord("FileHeader");
		record.setFieldValue("RecordTypeCd","1");
		record.setFieldValue("PriorityCd", "01"); //Default		
		record.setFieldValue("ImmediateDestination",StringTools.leftPad(electronicPayment.getBean("ElectronicPaymentContract").gets("ACHImmediateDestination"), " " ,10));
		record.setFieldValue("ImmediateOrigin",StringTools.leftPad(electronicPayment.getBean("ElectronicPaymentContract").gets("ACHImmediateOrigin")," ",10));
		record.setFieldValue("CreationDt", new SimpleDateFormat("yyMMdd").format(runDt.toDate()));
		record.setFieldValue("CreationTm", runTm);
		record.setFieldValue("ModifierId", "A"); //Default
		record.setFieldValue("RecordSize", "094"); //Default
		record.setFieldValue("BlockingFactor", "10"); //Default
		record.setFieldValue("FormatCd", "1"); //Default
		record.setFieldValue("ImmediateDestinationId", electronicPayment.getBean("ElectronicPaymentContract").gets("ACHImmediateDestinationName"));
		record.setFieldValue("ImmediateOriginId", electronicPayment.getBean("ElectronicPaymentContract").gets("ACHImmediateOriginName"));	
		//record.setFieldValue("ReferenceCd", StringTools.leftPad("0","0",8));
		 
		//override specific fields
		String umol = "COInterface::achfield::field::FileHeader";
		MDATable table = (MDATable) Store.getModelObject(umol);
		if( table !=null) {
			MDAOption[] options = table.getOptions();
			for( MDAOption option:options){
				record.setFieldValue(option.getLabel(), option.getValue());
			}
		}
		lineCt++;
		return record;

	}	

	public Record createPPDRecord(RecordGenerator generator,JDBCData data,ModelBean electronicPayment, String batchNum, StringDate todayDt, String todayTm, StringDate settlementDt) throws Exception{
		return createPPDRecord(generator, data, electronicPayment, batchNum, todayDt, todayTm, settlementDt, "", new BigDecimal("0.00"), true);
	}
	/** Generates the file detail record of type PPD (Prearranged Payments or Deposits)
	 * @param generator RecordGenerator The Record Generator class
	 * @param data JDBCData The data connection
	 * @param electronicPayment ModelBean The Electronic Payment bean      
	 * @param batchNum String The Batch number  
	 * @param todayDt StringDate Today's date
	 * @param runTm String Run Time
	 * @param settlementDt StringDate The settlement date
	 * @param electronicPaymentStatementId String The unique ElectronicPaymentStatementId used to tie the electronic payments on the same statement together
	 * @param totalAmt BigDecimal The total amount to ACH for this statement
	 * @param firstInd Boolean Indicator if this is the first ACH record for the statement, or true if non-statement ACH payment
	 * @return record Record The file detail record, null if this is a subsequent ACH record on the same statement account
	 */

	public Record createPPDRecord(RecordGenerator generator,JDBCData data,ModelBean electronicPayment, String batchNum, StringDate todayDt, String todayTm, StringDate settlementDt, String electronicPaymentStatementId, BigDecimal totalAmt, boolean firstInd) throws Exception{

		Record record = null;
		String traceNumber = "";
		String formattedAmount = "";		
		int amountNoDecimal = 0;
		BigDecimal amount = new BigDecimal("0.00");
		
		// update the amount to be the total amount of the statement
		if( electronicPayment.gets("StatementAccountRef").equals("")){
			amount = electronicPayment.getDecimal("Amount");
		} else {
			amount = totalAmt;
		}
		
		
		if( firstInd){
			batchEntryCt++;
			fileEntryCt++;
			record = generator.createRecord("PPDDetail");
			record.setFieldValue("RecordTypeCd","6");

			record.setFieldValue("TransactionId", electronicPayment.getSystemId());		
			record.setFieldValue("AddendaInd", "0"); // no Addenda record(s) follow
			record.setFieldValue("DiscData", "  "); // Discretionary data
			long num = (batchCt * 000000) + batchEntryCt;
			String routingNumber = "";
			long bankRoutingNumber = 0;
			String accountType = "";
			if (electronicPayment.gets("TypeCd").equalsIgnoreCase("DirectPayment")){
				// Obtain the current ACH Routing Number(s)
				String currentSourceRoutingNumber = getProcessedRoutingNumber(data, electronicPayment.getBean("ElectronicPaymentSource").gets("ACHRoutingNumber"));
				String currentDestinationRoutingNumber = getProcessedRoutingNumber(data, electronicPayment.getBean("ElectronicPaymentDestination").gets("ACHRoutingNumber"));
				
				accountType = electronicPayment.getBean("ElectronicPaymentSource").gets("ACHBankAccountTypeCd");
				record.setFieldValue("Name", electronicPayment.getBean("ElectronicPaymentSource").gets("ACHName"));			
				record.setFieldValue("ReceivingDFIId", currentSourceRoutingNumber.substring(0,8));
				record.setFieldValue("ReceivingDFICheckDigit", currentSourceRoutingNumber.substring(8));
				record.setFieldValue("ReceivingDFIAccount", StringTools.rightPad(electronicPayment.getBean("ElectronicPaymentSource").gets("ACHBankAccountNumber"), " ", 17));
				// carrier routing number for routing return entries
				routingNumber = currentDestinationRoutingNumber.substring(0,8);
				bankRoutingNumber = Long.valueOf(currentSourceRoutingNumber.substring(0,8)).longValue();				
			} else {			
				// Obtain the current ACH Routing Number(s)
				String currentSourceRoutingNumber = getProcessedRoutingNumber(data, electronicPayment.getBean("ElectronicPaymentSource").gets("ACHRoutingNumber"));			
				String currentDestinationRoutingNumber = getProcessedRoutingNumber(data, electronicPayment.getBean("ElectronicPaymentDestination").gets("ACHRoutingNumber"));
				
				accountType = electronicPayment.getBean("ElectronicPaymentDestination").gets("ACHBankAccountTypeCd");
				record.setFieldValue("Name", electronicPayment.getBean("ElectronicPaymentDestination").gets("ACHName"));			
				record.setFieldValue("ReceivingDFIId", currentDestinationRoutingNumber.substring(0,8));	
				record.setFieldValue("ReceivingDFICheckDigit", currentDestinationRoutingNumber.substring(8));
				record.setFieldValue("ReceivingDFIAccount", StringTools.rightPad(electronicPayment.getBean("ElectronicPaymentDestination").gets("ACHBankAccountNumber"), " ", 17));			
				// carrier routing number for routing return entries
				routingNumber = currentSourceRoutingNumber.substring(0,8);
				bankRoutingNumber = Long.valueOf(currentDestinationRoutingNumber.substring(0,8)).longValue();
				// deposit is a credit, switch the sign
				amount = amount.negate();
			}				
			
			amountNoDecimal = amount.multiply(new BigDecimal("100.00")).intValue();				
			if(amount.compareTo(zeroAmt) < 0){
				amountNoDecimal = amountNoDecimal * (-1);
				formattedAmount = StringTools.leftPad(amountNoDecimal, "0", 10 );
				batchTotalCredit += amountNoDecimal;
				fileTotalCredit  += amountNoDecimal;
			} else {
				formattedAmount = StringTools.leftPad(amountNoDecimal, "0",10 );
				batchTotalDebit += amountNoDecimal;
				fileTotalDebit  += amountNoDecimal;
			}							
			
			if (accountType.equalsIgnoreCase("Savings")){
				if(amount.compareTo(zeroAmt) < 0){
					record.setFieldValue("TransactionCd", "32");
					record.setFieldValue("TransactionAmount", formattedAmount);
					
				} else {
					record.setFieldValue("TransactionCd", "37");
					record.setFieldValue("TransactionAmount", formattedAmount);				
				}
			} else {
				if(amount.compareTo(zeroAmt) < 0){
					record.setFieldValue("TransactionCd", "22");
					record.setFieldValue("TransactionAmount", formattedAmount);				
				} else {
					record.setFieldValue("TransactionCd", "27");
					record.setFieldValue("TransactionAmount", formattedAmount);				
				}			
			}
			
			traceNumber = routingNumber + StringTools.leftPad(num, "0",7);
			record.setFieldValue("TraceNum", traceNumber);		
			batchEntryHash += bankRoutingNumber ;
			fileEntryHash  +=  bankRoutingNumber ;
			
			//override specific fields
			String umol = "COInterface::achfield::field::PPDDetail" + electronicPayment.gets("TypeCd");
			MDATable table = (MDATable) Store.getModelObject(umol);
			if( table !=null) {
				MDAOption[] options = table.getOptions();
				for( MDAOption option:options){
					if( option.getValue().startsWith("$")) {
						//lookup any additional data
						String otherData = "";
						try {
							ModelBean bean = new ModelBean(electronicPayment.gets("SourceModelName"));
							data.selectModelBean(bean, electronicPayment.gets("SourceRef"));
							otherData = DynamicString.render(bean, option.getValue());
						} catch (Exception e) {
							Log.error(electronicPayment.gets("SourceModelName") + " with system id " + electronicPayment.gets("SourceRef") + " not found!" );
						}
						int len = record.getFieldEnd(option.getLabel()) - record.getFieldStart(option.getLabel());
						record.setFieldValue(option.getLabel(), StringTools.rightPad(otherData, " ", len+1) );
					} else {
						record.setFieldValue(option.getLabel(), option.getValue());
					}
				}
			}
			this.lineCt++;
		}
		
		ModelBean status = new ModelBean("ElectronicPaymentStatus");
		status.setValue("ACHBatchId", batchNum);
		if( firstInd)
			status.setValue("ACHTraceNumber",traceNumber);
		else
			status.setValue("ACHTraceNumber", getTraceNumber(data, electronicPaymentStatementId));
		electronicPayment.addValue(status);
		electronicPayment.setValue("StatusCd", "Sent");
		electronicPayment.setValue("SentDt",todayDt);
		electronicPayment.setValue("SentTm", todayTm);
		electronicPayment.setValue("SettlementDt", settlementDt);
		data.saveModelBean(electronicPayment);
		
		if( firstInd)
			return record;
		else
			return null;

	}
	
	/** Generates the batch balance record 
	 * @param generator RecordGenerator The Record Generator class
	 * @param batchAmt int The amount for this batch
	 * @param routingNumber String The routing number of the carrier
	 * @param accountNumber String The account number of the carrier       
	 * @return record Record The batch balance record 
	 */

	public Record createBalanceRecord(RecordGenerator generator,int batchAmt, String routingNumber, String accountNumber)throws Exception{
		Record record = generator.createRecord("PPDDetail");		
		batchEntryCt++;
		fileEntryCt++;
		record.setFieldValue("RecordTypeCd","6");
		long bankRoutingNumber = Long.valueOf(routingNumber.substring(0,8)).longValue();		
		batchEntryHash = batchEntryHash  + bankRoutingNumber ;
		fileEntryHash = fileEntryHash  + bankRoutingNumber ;
		String formattedAmount = "";
		if (batchAmt < 0){
			batchAmt = batchAmt * (-1);
			formattedAmount = StringTools.leftPad(batchAmt, "0", 10 );
			record.setFieldValue("TransactionCd", "22");
			record.setFieldValue("TransactionAmount", formattedAmount);
		} else {
			formattedAmount = StringTools.leftPad(batchAmt, "0", 10 );
			record.setFieldValue("TransactionCd", "27");
			record.setFieldValue("TransactionAmount", formattedAmount);
		}
		record.setFieldValue("ReceivingDFIId",routingNumber.substring(0,8));
		record.setFieldValue("ReceivingDFICheckDigit",routingNumber.substring(8));
		record.setFieldValue("ReceivingDFIAccount",accountNumber);
		record.setFieldValue("TransactionId","999");
		record.setFieldValue("Name", "BALANCE RECORD");
		record.setFieldValue("AddendaInd", "0");
		long traceNumber = (batchCt * 100000) + batchEntryCt;
		// ? cast trace Number 
		record.setFieldValue("TraceNum", routingNumber.substring(0,8) + StringTools.leftPad(traceNumber, "0",7));	
		this.lineCt++;
		return record;		

	}

	/** Generates the batch header record 
	 * @param generator RecordGenerator The Record Generator class
	 * @param electronicPayment ModelBean The Electronic Payment bean    
	 * @param batchNum String The batch number
	 * @param todayDt StringDate Today's date
	 * @param runDt StringDate The run date       
	 * @return record Record The batch header record 
	 */
	public Record createBatchHeaderRecord(RecordGenerator generator,ModelBean electronicPayment,String batchNum, StringDate todayDt,StringDate settlementDt,String sweepInd)throws Exception{
		Record record = generator.createRecord("BatchHeader");
		record.setFieldValue("RecordTypeCd","5");
		record.setFieldValue("ServiceClassCd","200");//200 mixed db cr, 220 cr only, 225 db only
		if (electronicPayment.gets("TypeCd").equalsIgnoreCase("DirectPayment")){
			// Obtain the current ACH Routing Number
			String currentDestinationRoutingNumber = getProcessedRoutingNumber(ServiceContext.getServiceContext().getData(), electronicPayment.getBean("ElectronicPaymentDestination").gets("ACHRoutingNumber"));			
			record.setFieldValue("CompanyName", electronicPayment.getBean("ElectronicPaymentDestination").gets("ACHName"));									
			record.setFieldValue("OriginatingDFIId",currentDestinationRoutingNumber.substring(0,8));
			record.setFieldValue("StandardEntryClassCd", electronicPayment.getBean("ElectronicPaymentSource").gets("ACHStandardEntryClassCd"));
		} else {
			// Obtain the current ACH Routing Number
			String currentSourceRoutingNumber = getProcessedRoutingNumber(ServiceContext.getServiceContext().getData(), electronicPayment.getBean("ElectronicPaymentSource").gets("ACHRoutingNumber"));			
			record.setFieldValue("CompanyName", electronicPayment.getBean("ElectronicPaymentSource").gets("ACHName"));							
			record.setFieldValue("OriginatingDFIId",currentSourceRoutingNumber.substring(0,8));
			record.setFieldValue("StandardEntryClassCd", electronicPayment.getBean("ElectronicPaymentDestination").gets("ACHStandardEntryClassCd"));
		}
		record.setFieldValue("CompanyEntryDesc", "INS PREM");
		if(!sweepInd.equals("") && sweepInd.equalsIgnoreCase("Yes")){
			record.setFieldValue("CompanyId", "A".concat(electronicPayment.getBean("ElectronicPaymentContract").gets("ACHCompanyId")));
		}else if(!sweepInd.equals("") && sweepInd.equalsIgnoreCase("No")){
			record.setFieldValue("CompanyId", "D".concat(electronicPayment.getBean("ElectronicPaymentContract").gets("ACHCompanyId")));	
		}
		// To make it work for the existing policies.
		if("".equals(record.getFieldValue("StandardEntryClassCd"))){
			record.setFieldValue("StandardEntryClassCd", "PPD");
		}
		
		record.setFieldValue("CompanyDescriptiveDt", new SimpleDateFormat("yyMMdd").format(todayDt.toDate()));
		record.setFieldValue("EffectiveEntryDt", new SimpleDateFormat("yyMMdd").format(settlementDt.toDate()));
		//record.setFieldValue("SettlementDt", "000");
		record.setFieldValue("OriginatorStatusCd","1");
		record.setFieldValue("BatchNum", batchNum);
		//override specific fields
		String umol = "COInterface::achfield::field::BatchHeader";
		MDATable table = (MDATable) Store.getModelObject(umol);
		if( table !=null) {
			MDAOption[] options = table.getOptions();
			for( MDAOption option:options){
				record.setFieldValue(option.getLabel(), option.getValue());
			}
		}
		lineCt++;
		return record;

	}

	/** Generates the batch control record 
	 * @param generator RecordGenerator The Record Generator class   
	 * @param batchNum String The batch number
	 * @param carrierId String The carrier id   
	 * @param routing String The company routing number       
	 * @return record Record The batch control record 
	 */
	public Record createBatchControlRecord(RecordGenerator generator, String batchNum, String carrierId, String routingNumber,String sweepInd)throws Exception{
		Record record = generator.createRecord("BatchControl");
		record.setFieldValue("RecordTypeCd","8");
		record.setFieldValue("ServiceClassCd","200");//200 mixed db cr, 220 cr only, 225 db only
		record.setFieldValue("EntryCt", StringTools.leftPad(batchEntryCt, "0", 6));
		record.setFieldValue("BatchEntryHash", StringTools.leftPad(batchEntryHash,"0",10));
		record.setFieldValue("TotalDebitAmt",  StringTools.leftPad(String.valueOf(batchTotalDebit), "0", 12));
		record.setFieldValue("TotalCreditAmt", StringTools.leftPad(String.valueOf(batchTotalCredit), "0",12));
		if(!sweepInd.equals("") && sweepInd.equalsIgnoreCase("Yes")){
			record.setFieldValue("CompanyId", "A".concat(carrierId));
		}else if(!sweepInd.equals("") && sweepInd.equalsIgnoreCase("No")){
			record.setFieldValue("CompanyId", "D".concat(carrierId));
		}
		record.setFieldValue("OriginatingDFIId",routingNumber.substring(0,8));
		record.setFieldValue("BatchNum", StringTools.leftPad(batchNum,"0",7));
		lineCt++;
		return record;

	}

	/** Generates the file control record 
	 * @param generator RecordGenerator The Record Generator class   
	 * @param batchCt int The batch count          
	 * @return record Record The  file control record 
	 */
	public Record createFileControlRecord(RecordGenerator generator,int batchCt)throws Exception{
		Record record = generator.createRecord("FileControl");	
		record.setFieldValue("RecordTypeCd","9");
		record.setFieldValue("BatchCt",StringTools.leftPad(batchCt,"0",6));
		// Blocks must be in multiple of 10
		if (lineCt < 10){
			record.setFieldValue("BlockCt", StringTools.leftPad("1", "0",6));
		}else{
			if( lineCt/10 % 10 == 0){
				record.setFieldValue("BlockCt", StringTools.leftPad(lineCt/10, "0",6));
			} else {
				int blockCt = lineCt/10 + 1;
				record.setFieldValue("BlockCt", StringTools.leftPad(blockCt, "0",6));
			}
		}
		record.setFieldValue("EntryCt",StringTools.leftPad(fileEntryCt, "0",8));
		record.setFieldValue("FileEntryHash",StringTools.leftPad(String.valueOf(fileEntryHash), "0",10));
		record.setFieldValue("TotalDebitAmt", StringTools.leftPad(String.valueOf(fileTotalDebit), "0", 12));
		record.setFieldValue("TotalCreditAmt", StringTools.leftPad(String.valueOf(fileTotalCredit), "0", 12));
		record.setFieldValue("Spaces", StringTools.leftPad(" "," ",39));
		this.lineCt++;

		return record;

	}

	/** Generates the block filler record(s) 
	 * @param generator RecordGenerator The Record Generator class              
	 * @return record Record[] The  block filler record(s)
	 */
	public Record[] createBlockFillerRecord(RecordGenerator generator)throws Exception{		
		int remainder = lineCt % 10;		
		ArrayList<Record> array = new ArrayList<Record>();		

		if (remainder > 0){			
			for (int i = remainder; i<10;i++){
				Record record = generator.createRecord("BlockFiller");
				record.setFieldValue("Filler", StringTools.leftPad("9","9",94));
				array.add(record);
			}
		}
		return (Record[]) array.toArray(new Record[array.size()]);
	}	

	/**Add an electronic payment request to the database
	 * @param data JDBCData The data connection
	 * @param sessionInfo SessionInfo             
	 * @return errors ModelBean The errors model bean
	 */

	public ModelBean addRequest(JDBCData data, ModelBean request, StringDate todayDt)
	throws Exception, ElectronicPaymentException {
		ModelBean errors = new ModelBean("Errors");
		Boolean agent = null;
		// Validate the request
		boolean valid = validateRequest(request, errors);
		if (valid) {	
			StringDate settlementDt = getSettlementDt(data,todayDt, errors, agent);
			if (settlementDt !=null){
				request.setValue("SettlementDt",settlementDt);
				BeanTools.saveBean(request, data);
			}
		}
		return errors;
	}

	/** Validate the request	 
	 * @param request ModelBean The Electronic Payment Request ModelBean
	 * @param error ModelBean
	 * @return true/false if the validation was successful
	 * @throws ElectronicPaymentException when an error occurs
	 */
	public boolean validateRequest(ModelBean request, ModelBean errors)
	throws ElectronicPaymentException {
		try {
			boolean valid = true;
			// validate carrier info
			if (request.getBean("ElectronicPaymentContract").gets("ACHImmediateOrigin").equals("") || request.getBean("ElectronicPaymentContract").gets("ACHImmediateOriginName").equals("")){
				ModelBean error = ErrorTools.createError("ACHValidation", "The carrier immediate origin information is missing", ErrorTools.GENERIC_BUSINESS_ERROR);
				errors.addValue(error);
				valid = false;
			}
			if (request.getBean("ElectronicPaymentContract").gets("ACHImmediateDestination").equals("") || request.getBean("ElectronicPaymentContract").gets("ACHImmediateDestinationName").equals("")){
				ModelBean error = ErrorTools.createError("ACHValidation", "The carrier immediate destination information is missing", ErrorTools.GENERIC_BUSINESS_ERROR);
				errors.addValue(error);
				valid = false;
			}
			if (request.getBean("ElectronicPaymentContract").gets("ACHCompanyId").equals("") || request.getBean("ElectronicPaymentContract").gets("MethodCd").equals("")){
				ModelBean error = ErrorTools.createError("ACHValidation", "The carrier information is missing", ErrorTools.GENERIC_BUSINESS_ERROR);
				errors.addValue(error);
				valid = false;
			}
			
			//validate source account info
			if (request.getBean("ElectronicPaymentSource").gets("ACHRoutingNumber").equals("") || request.getBean("ElectronicPaymentSource").gets("ACHBankAccountTypeCd").equals("") ||
					request.getBean("ElectronicPaymentSource").gets("ACHBankAccountNumber").equals("")){
				ModelBean error = ErrorTools.createError("ACHValidation", "The required source information is missing", ErrorTools.GENERIC_BUSINESS_ERROR);
				errors.addValue(error);
				valid = false;
			}
			//validate destination account info
			if (request.getBean("ElectronicPaymentDestination").gets("ACHRoutingNumber").equals("") || request.getBean("ElectronicPaymentDestination").gets("ACHBankAccountTypeCd").equals("") ||
					request.getBean("ElectronicPaymentDestination").gets("ACHBankAccountNumber").equals("")){
				ModelBean error = ErrorTools.createError("ACHValidation", "The required destination information is missing", ErrorTools.GENERIC_BUSINESS_ERROR);
				errors.addValue(error);
				valid = false;
			}
			//validate amount
			if (request.getDecimal("Amount").compareTo(zeroAmt) == 0 && !StringTools.isTrue(request.gets("ZeroAllowedInd")) ){			
				ModelBean error = ErrorTools.createError("ACHValidation", "The amount is zero", ErrorTools.GENERIC_BUSINESS_ERROR);
				errors.addValue(error);
				valid = false;
			}
			//validate routing number
			if (request.getBean("ElectronicPaymentDestination").gets("ACHRoutingNumber").length() != 9) {
				ModelBean error = ErrorTools.createError("ACHValidation", "Invalid routing number", ErrorTools.GENERIC_BUSINESS_ERROR);
				errors.addValue(error);
				valid = false;
			}

			return valid;
		} catch (Exception e) {
			Log.error("ElectronicPaymentException", e);
			throw new ElectronicPaymentException(e);
		}

	}
	/** Set up the ach record templates   
	 */

	protected void init() throws Exception{		
		achRecord = new XmlDoc( getClass().getResource("model/template/ach-record-layouts.xml") );
		fieldTypes = new XmlDoc( getClass().getResource("model/template/field-types.xml") );
	}

	/**
	 * Process the ACH Exception file and call the appropriate handler to process the exceptions
	 * @param data JDBC data The data connection
	 * @param filename String The exception filename
	 * @param bookDt StringDate The book date 	
	 * @param runTm String The run time
	 * @param info SessionInfo The session info
	 * @param errors ModelBean The errors bean
	 * @throws Exception
	 */
	public void processException(JDBCData data,String filename, StringDate bookDt, String runTm, SessionInfo info, ModelBean errors) throws Exception {

		BufferedReader reader = FileIO.openFile(filename);
		String thisLine;
		ModelBean ep = null;				
		ModelBean[] payments = null;
		boolean combinedACHPaymentInd = false;

		while( (thisLine = reader.readLine()) != null ) {
			// 6 & 7 are PPD and addenda records for Insured Bill
			if (thisLine.substring(0,1).equals("6")) {							

				int id = Integer.parseInt(thisLine.substring(39,54).trim());
				ep = new ModelBean("ElectronicPayment");
				data.selectModelBean(ep,id);
				
				if( ep.gets("ElectronicPaymentStatementId").equals("")) {
					combinedACHPaymentInd = false;
					payments = null;
					setAcknowledgement(ep, bookDt,runTm);										
				} else {
					// this is a combined ACH payment, find all the individual records that make up this payment
					combinedACHPaymentInd = true;
					payments = getPaymentRecords(data, ep.gets("ElectronicPaymentStatementId"));
					for( ModelBean payment:payments){
						setAcknowledgement(payment, bookDt,runTm);
					}					
				}

			} else if (thisLine.substring(0,1).equals("7")){
				String errorCd = thisLine.substring(3,6);
				if( !combinedACHPaymentInd){
					boolean successInd = processException(data, info, new ModelBean[] {ep}, thisLine, errorCd, errors);
					
					if( successInd){
						Log.debug("Committing transaction for Electronic Payment id " + ep.getSystemId());
						data.commit();
					}else{ 
						Log.debug("Rolling back transaction for Electronic Payment id " + ep.getSystemId());	
						data.rollback();
					}
				} else {
					// reverse all the ACH payments that are part of the rejection, commit only when all of them are reversed off successfully
																							
					boolean successInd = processException(data, info, payments, thisLine, errorCd, errors);
						
					String paymentList = "";
					for( ModelBean payment : payments ) {
						if( paymentList.equals("") )
							paymentList = payment.gets("ElectronicPaymentStatementId");
						else
							paymentList = paymentList + ", " + payment.gets("ElectronicPaymentStatementId");
					}
					
					if( successInd ) {
						Log.debug("Committing transaction for combined payments containing: " + paymentList);
						data.commit();
					}
					
					if( !successInd){	
						Log.debug("Rolling back transaction for combined payments containing: " + paymentList);
						data.rollback();
					}
					
				}
				
			}

		}	

	}

	/**
	 * Verify the ACH Exception file is valid
	 * It's currently checking for the existence of 6/7 pair and the trailer record 9
	 * @param filename String The exception filename	 
	 * @return true if valid, false if not
	 * @throws Exception
	 */
	public boolean validateExceptionFile(String filename) throws Exception{
		boolean valid = false;
		boolean detailInd = false;
		boolean addendaInd = false;	
		BufferedReader reader = FileIO.openFile(filename);
		String thisLine;
		while( (thisLine = reader.readLine()) != null ) { 
			if (thisLine.substring(0,1).equals("6")) {
				if (detailInd){
					valid = false;
					break;
				} else {
					detailInd = true;
					addendaInd = false;
				}
			}else if (thisLine.substring(0,1).equals("7")){
				if (addendaInd){
					valid = false;
					break;
				} else {
					addendaInd = true;
					detailInd = false;
				}				
			}else if (thisLine.substring(0,1).equals("9")){
				valid = true;
				break;
			}
		}
		reader.close();
		return valid;
	}
	
	/** Get the ACH settlement date
	 *  For credit entry the settlement date = today + 1 or 2 days
	 *	For debit entry the settlement date = today + 1
	 *	Since the job is typically run at night, we need to add 1 more day to the run date
	 *  @param data JDBCData The data connection
	 *  @param todayDt StringDate The date to work from
	 *  @return The settlement date
	 */
	
	public StringDate getSettlementDt(JDBCData data,StringDate todayDt, ModelBean errors, Boolean agent) throws Exception {
		StringDate settlementDt = null;
		MDATemplate templateMDA = null;
		if (agent == true){ 
			 templateMDA = (MDATemplate) Store.getModelObject("calendar-template", "CalendarTemplate", "AGENTACH");
		}
		else{
		 templateMDA = (MDATemplate) Store.getModelObject("calendar-template", "CalendarTemplate", "ACH");
		}
		if( templateMDA != null ){	
			ModelBean template = templateMDA.getBean();			
			settlementDt = Calendar.getBusinessDate(data, todayDt, template);
		} else {
			ModelBean error = ErrorTools.createError("ERROR", "Calendar template for ACH not found", ErrorTools.GENERIC_BUSINESS_ERROR);
			errors.addValue(error);
		}
		return settlementDt;
		
	}	
	
	
	/** Get all the bank account contracts from Payables
	 * @param data JDBCData The data connection
	 * @param contractList ArrayList The list of all the contracts
	 * @param urlList ArrayList The list of all the exception URLS
	 * @return List of all the bank account contract
	 * @throws Exception
	 */
	
	public static ArrayList<ModelBean> getBankAccountContracts(JDBCData data, ArrayList<ModelBean> contractList, ArrayList<String> urlList) throws Exception {
		
		String repositoryName =  "PYInterfaces::bankaccount::list::all";
		
		MDATable table = (MDATable) Store.getModelObject(repositoryName);
		MDAOption option = table.getOption("handler");
		String handler = option.getLabel();
		Object obj = ClassHelper.loadObject(handler);
		
		AccountContractGet aHandler = (AccountContractGet) obj;
		return aHandler.process(data, contractList, urlList);
	}
	
	/** Determine if the routing number is valid, and that the financial institution with that 
	 * routing number exists 
	 * @param data The data connection
	 * @param routingNumber The routing number to validate
	 * @return true/false indicator if routing number exists
	 * @throws Exception when an error occurs
	 */
	public static boolean isValidRoutingNumber(JDBCData data, String routingNumber) throws Exception {

		String fiType = FINANCIAL_TYPE;
        MDATable table = (MDATable) Store.getModelObject("COProvider::provider::type::financialinstitution");
        MDAOption[] options = table.getOptions();
        if (options.length > 0)
        	fiType = options[0].getLabel();
        
		JDBCLookup lookup = new JDBCLookup("ProviderLookup");
		lookup.addLookupKey("ProviderNumber", routingNumber, JDBCLookup.LOOKUP_EQUALS);
		lookup.addLookupKey("ProviderType", fiType, JDBCLookup.LOOKUP_EQUALS);
        
		JDBCData.QueryResult[] results = lookup.doLookup(data, 1);
		if (results.length > 0)
			return true;
		else
			return false;		
	}
	
	/** Obtain the current Financial Institution record with the routing number
	 * @param data The data connection
	 * @param routingNumber The routing number
	 * @return The Financial Institution information
	 * @throws Exception when an error occurs
	 */
	public static ModelBean getFinancialInstitution(JDBCData data, String routingNumber) throws Exception {
        try {
    		String fiType = FINANCIAL_TYPE;
            MDATable table = (MDATable) Store.getModelObject("COProvider::provider::type::financialinstitution");
            MDAOption[] options = table.getOptions();
            if (options.length > 0)
            	fiType = options[0].getLabel();
            
    		JDBCLookup lookup = new JDBCLookup("ProviderLookup");
    		lookup.addLookupKey("ProviderNumber", routingNumber, JDBCLookup.LOOKUP_EQUALS);
    		lookup.addLookupKey("ProviderType", fiType, JDBCLookup.LOOKUP_EQUALS);
    		ModelBean[] fi = lookup.selectBeans(data, 1);
    		if (fi.length > 0)
    			return fi[0];
    		else 
    			return null;
        } catch( Exception e ) {
            Log.error(e.getMessage(), e);
            e.printStackTrace();
            return null;
        }
	}
	
	/** Obtain the current routing number.  The previous routing number if valid will point to
	 * the current valid routing number to use for a Financial Institution
	 * @param data The data connection
	 * @param routingNumber The routing number
	 * @return A new routing number or the current number if no new one found
	 * @throws Exception when an error occurs
	 */
	public static String getProcessedRoutingNumber(JDBCData data, String routingNumber) {
		try {
			ModelBean fi = getFinancialInstitution(data, routingNumber);
			if (fi != null) {
				String curRouteNo = fi.getBean("FinancialInstitutionInfo").gets("ACHNewRoutingNumber", "");
				if (!curRouteNo.equals("")) {
					return curRouteNo;
				}
			}
			return routingNumber;			
		} catch (Exception e) {
			return routingNumber;
		}
	}
	
	protected String getCorrectionText(String line, String msg, String errorCd) throws Exception{
		StringBuffer correctionText = new StringBuffer();
		correctionText.append(msg);
		correctionText.append(" : ");
		
		String label = CodeRefTools.getOptionLabel("COInterface::exceptioncodes::ach-codes::corrections", errorCd);			
		String[] text = label.split(",");
		for( int i =0; i<text.length;i++){
			if( i> 0)
				correctionText.append(",");
			
			String[] pos = text[i].split(":");
			correctionText.append(line.substring(Integer.parseInt(pos[0]), Integer.parseInt(pos[1])).trim());
		}
		
		return correctionText.toString();
			
	}
	
	/** Tie the ElectronicPayment records that are on the same statement together and sum them up
	 * @param data JDBCData The data connection
	 * @param electronicPayments ModelBean[] The list of electronic payment records
	 * @param statementAccountRef String The Statement Account Ref being processed
	 * @param electronicPaymentStatementId String The unique id to tie the electronic payment records that are on the same statement together
	 * @return the total amount to ACH for this statement
	 * @throws Exception whenever an unexpected error occurs
	 */
	public BigDecimal updateElectronicPayment(JDBCData data, ModelBean[] electronicPayments, String statementAccountRef, String electronicPaymentStatementId) throws Exception{
		BigDecimal totalAmt = new BigDecimal("0.00");		
		for(ModelBean electronicPayment:electronicPayments){
			if( electronicPayment.gets("StatementAccountRef").equals(statementAccountRef)){				
				totalAmt = totalAmt.add(electronicPayment.getDecimal("Amount"));
				electronicPayment.setValue("ElectronicPaymentStatementId", electronicPaymentStatementId);
				data.saveModelBean(electronicPayment);
			}
		}				
		return totalAmt;
	}
	
	/** Tie the ElectronicPayment records together that are on the same statement and are from same payment source
	 * @param data JDBCData The data connection
	 * @param electronicPayments ModelBean[] The list of electronic payment records
	 * @param statementAccountRef String The Statement Account Ref being processed
	 * @param electronicPaymentStatementId String The unique id to tie the electronic payment records that are on the same statement together
	 * @param paymentKey String The unique Key to distinguish an ACH Payment( StandardEntryClassCd-ACHRoutingNumber-ACHBankAccountNumber)
	 * @return the total amount to ACH for this statement and for this payment account
	 * @throws Exception whenever an unexpected error occurs
	 */
	public BigDecimal updateElectronicPayment(JDBCData data, ModelBean[] electronicPayments, String statementAccountRef, String electronicPaymentStatementId, String paymentKey) throws Exception{
		BigDecimal totalAmt = new BigDecimal("0.00");		
		for(ModelBean electronicPayment:electronicPayments){
			if( electronicPayment.gets("StatementAccountRef").equals(statementAccountRef) && getPaymentSourceKey(electronicPayment).equals(paymentKey)){				
				totalAmt = totalAmt.add(electronicPayment.getDecimal("Amount"));
				electronicPayment.setValue("ElectronicPaymentStatementId", electronicPaymentStatementId);
				data.saveModelBean(electronicPayment);
			}
		}				
		return totalAmt;
	}
	
	/* Get the trace number which is the same for ACH payments on the same statement account
	 * @param data JDBCData The data connection
	 * @param electronicPaymentStatementId String The unique identifier that links the electronic payments on the same statement together
	 * @return The trace number, empty string if not found
	 */
	public String getTraceNumber(JDBCData data, String electronicPaymentStatementId) throws Exception {
		String traceNumber = "";
		QueryResult[] results = data.doQueryFromLookup("ElectronicPayment",new String[] {"ElectronicPaymentStatementId"},new String[] {"="}, new String[] {electronicPaymentStatementId},0);
		
		for( int i=0;i<results.length;i++){
			ModelBean payment = new ModelBean("ElectronicPayment");
			data.selectModelBean(payment, results[i].getSystemId());
			ModelBean status = payment.getBean("ElectronicPaymentStatus");
			if( status != null && !status.gets("ACHTraceNumber").equals("")){
				traceNumber = status.gets("ACHTraceNumber");
				break;
			}
				
		}		
		return traceNumber;
	}
	
	public void setAcknowledgement(ModelBean electronicPayment, StringDate bookDt, String time) throws Exception {
		// check the status to make sure this payment has not already been rejected/corrected
		if (!electronicPayment.gets("StatusCd").equals("Rejected") && !electronicPayment.gets("StatusCd").equals("Corrected")){					
			electronicPayment.setValue("AcknowledgeDt", bookDt);
			electronicPayment.setValue("AcknowledgeTm", time);										
		}
	}
	
	/* Get all the ACH payments that are tied to this rejection
	 * @param data JDBCData The data connection
	 * @param electronicPaymentStatementId String The unique identifier that links the electronic payments on the same statement together
	 * @return The array of ACH payments
	 */
	public ModelBean[] getPaymentRecords(JDBCData data, String electronicPaymentStatementId) throws Exception {
		ArrayList<ModelBean> list = new ArrayList<ModelBean>();
		QueryResult[] results = data.doQueryFromLookup("ElectronicPayment",new String[] {"ElectronicPaymentStatementId"},new String[] {"="}, new String[] {electronicPaymentStatementId},0);
		
		for( int i=0;i<results.length;i++){
			ModelBean payment = new ModelBean("ElectronicPayment");
			data.selectModelBean(payment, results[i].getSystemId());
			list.add(payment);

		}
		return (ModelBean[]) list.toArray(new ModelBean[list.size()]);
	}		
	
	public boolean processException(JDBCData data, SessionInfo info, ModelBean[] electronicPayments, String thisLine, String errorCd, ModelBean errors) throws Exception {
		ModelBean processingErrors = null;	
		boolean successInd = false;
		String repositoryName = "";
		
		for( ModelBean electronicPayment : electronicPayments ) {
			// check the status to make sure this payment has not already been rejected/corrected
			if( !electronicPayment.gets("StatusCd").equals("Rejected") && !electronicPayment.gets("StatusCd").equals("Corrected") ){													
				ModelBean status = electronicPayment.getBean("ElectronicPaymentStatus");		
				String msg = CodeRefTools.getOptionLabel("COInterface::exceptioncodes::ach-codes::codes", errorCd);				
				status.setValue("ACHExceptionCd", errorCd);
				status.setValue("ACHExceptionMsg", msg);					           							            
	
				String sourceModule = electronicPayment.gets("SourceModule");
				if( sourceModule.equals("Billing") ){
					// handler for incoming payment exceptions
					repositoryName =  "ARInterfaces::electronic-payment-exception::exception::" + sourceModule;
				} else if( sourceModule.equals("Payables") ){
					// handler for outgoing payment exceptions
					repositoryName =  "PYInterfaces::electronic-payment-exception::exception::" + sourceModule;						
					
				}
				if( repositoryName.equals("") ) {
					ModelBean error = new ModelBean("Error");
					msg = "No exception handler for electronic payment id " + electronicPayment.getSystemId();
					error = ErrorTools.createError("ElectronicPaymentException", msg, ErrorTools.GENERIC_BUSINESS_ERROR);
					errors.addValue(error);
				}
				
				// update the error message with any correction text 
				if( errorCd.startsWith("C") ){
					status.setValue("ACHExceptionMsg", getCorrectionText(thisLine, msg, errorCd) );
				}
			}
			else {
				String msg = "One of the ElectronicPayments has a status of " + electronicPayment.gets("StatusCd");
				ModelBean error = ErrorTools.createError("ElectronicPaymentException", msg, ErrorTools.GENERIC_BUSINESS_ERROR);
				errors.addValue(error);
				return successInd;
			}
		}
		
		MDATable table = (MDATable) Store.getModelObject(repositoryName);
		MDAOption option = table.getOption("handler");
		String handler = option.getLabel();
		Object obj = ClassHelper.loadObject(handler);
		ElectronicPaymentExceptionHandler aHandler = (ElectronicPaymentExceptionHandler) obj;
		
		processingErrors = aHandler.processException(data,electronicPayments, new String[] {}, info);
		
		if( !processingErrors.hasErrors() ) {	
			
			// Update each of the EPs with 
			for( ModelBean electronicPayment : electronicPayments ) {
				if( errorCd.startsWith("C"))
					electronicPayment.setValue("StatusCd","Corrected");
				else
					electronicPayment.setValue("StatusCd","Rejected");
				
				data.saveModelBean(electronicPayment);
			}

			successInd = true;	 
		} else {							
			ModelBean[] error = processingErrors.getBeans("Error");
			for (int j=0;j<error.length;j++){
				errors.addValue(error[j]);		
			}
		}			
			
		return successInd;
	}
	
	/** Obtain the unique identifier that represents an ACH payment(StandardEntryClassCd-ACHRoutingNumber-ACHBankAccountNumber)
	 * @param electronicPayment The ElectronicPayment bean of an ACH payment
	 * @return String Returns an unique identifier that represents an ACH payment(StandardEntryClassCd-ACHRoutingNumber-ACHBankAccountNumber)
	 * @throws Exception
	 */
	public static String getPaymentSourceKey(ModelBean electronicPayment) throws Exception{
		try{
			
			String paymentKey = "";
			ModelBean electronicPaymentSource = null;
			
			if( electronicPayment.gets("TypeCd").equalsIgnoreCase("DirectPayment") ) {
				electronicPaymentSource = electronicPayment.getBean("ElectronicPaymentSource");
			} else if ( electronicPayment.gets("TypeCd").equalsIgnoreCase("DirectDeposit") ) {				
				electronicPaymentSource = electronicPayment.getBean("ElectronicPaymentDestination");
			}
			
			if(electronicPaymentSource!=null){
				paymentKey = electronicPaymentSource.gets("ACHStandardEntryClassCd") + "-" + electronicPaymentSource.gets("ACHRoutingNumber") + "-" + electronicPaymentSource.gets("ACHBankAccountNumber");
			}
			
			return paymentKey;
			
		}catch(Exception ex){
			Log.error(ex);	
			return "";
		}
		
	}

}
