/*
 * CommissionLoadPlan.java * 
 */

package com.ric.commission.handler;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.jdom.Element;

import com.iscs.commission.CommissionPlanTools;
import com.iscs.common.tech.bean.data.SQLRelationDelegate;
import com.iscs.common.tech.bean.data.SQLRelationHelper;
import com.iscs.common.tech.biz.InnovationIBIZHandler;
import com.iscs.common.tech.log.Log;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.common.utility.excel.ExcelTools;
import com.iscs.common.utility.io.FileTools;

import net.inov.biz.server.IBIZException;
import net.inov.biz.server.ServiceHandlerException;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.web.cmm.AdditionalParams;
import net.inov.tec.xml.XmlDoc;


/** Loads a commission plan from an Excel file  
 *
 * @author  cataling
 */
public class CommissionLoadPlan extends InnovationIBIZHandler {
        
    /** Loads a commission plan from an Excel file
     * @throws Exception never
     */
    public CommissionLoadPlan() throws Exception {
    }
    
    /** Loads a commission plan from an Excel file
     * @return the current response bean
     * @throws IBIZException when an error requiring user attention occurs
     * @throws ServiceHandlerException when a critical error occurs
     */
	public ModelBean process() throws IBIZException, ServiceHandlerException {
		try {
			Log.debug("...Load CommissionPlan beans from an Excel file");

			ModelBean rs = getHandlerData().getResponse();
			JDBCData data = this.getHandlerData().getConnection();
			AdditionalParams ap = new AdditionalParams(this.getHandlerData().getRequest());
			String uploadFile = "";

			String commissionEffectiveDateString = ap.getString("CommissionEffectiveDate");
			if (commissionEffectiveDateString == null || commissionEffectiveDateString.isEmpty()) {
				addErrorMsg("CommissionEffectiveDate",	"CommissionEffectiveDate Required",	ErrorTools.MISSING_FIELD_ERROR);
			}
			
            if (!data.tableExists("CommissionPlan", false)){
            	addErrorMsg("MissingTable", "CommissionPlan table does not exist.", ErrorTools.GENERIC_BUSINESS_ERROR);
            }

			StringDate commissionEffectiveDate = new StringDate(StringDate.formatEntryDate(commissionEffectiveDateString));

			// Get Imported File
			String modelFile = ap.getString("ImportFile");
			if (modelFile.equals("")) {
				addErrorMsg("ImportFile", "Import Commission Plan File Required", ErrorTools.MISSING_FIELD_ERROR);
			} else {
				try {
					// Extract the File Name from the Path
					String fileName = "";

					// Determine which file separator the client uses
					if (modelFile.indexOf("/") > 0) {
						fileName = modelFile.substring(modelFile.lastIndexOf("/") + 1);
					} else {
						fileName = modelFile.substring(modelFile.lastIndexOf("\\") + 1);
					}
					Log.debug("fileName: " + fileName);

					// Build the path to the uploaded file
					uploadFile = FileTools.cleanPath(net.inov.tec.prefs.PrefsDirectory.getLocation())	+ "upload" + File.separator + fileName;
					Log.debug("uploadFile: " + uploadFile);

					ExcelTools excelTools = new ExcelTools();
					
					CommissionPlanTools tools = new CommissionPlanTools();
					XmlDoc importedDoc = read(uploadFile);
					
					HashMap tagToFieldMap = tools.getColumnToTagMap("CM::commission::excelimport::columnmapping");
					
					ModelBean[] commissionPlans = excelTools.loadToBeans( importedDoc, "CommissionPlan", tagToFieldMap);
					
					// ModelBean[] existingCommissionPlans = tools.getAllCommissionPlansOrdered(data, "EffectiveDt", "Ascending");
					ModelBean[] existingCommissionPlans = tools.getAllCommissionPlans(data);
					
					// Get already verified Imported File
					String verifiedFileName = ap.getString("VerifiedFileName");
					
					if (verifiedFileName == null || verifiedFileName.isEmpty() || !verifiedFileName.equals(fileName)){
	                    String exception = tools.validatePlansEffectiveDate(commissionPlans, existingCommissionPlans, commissionEffectiveDate);
	
	                    if (!exception.isEmpty()){
	                    	addErrorMsg("DateValidation", exception, ErrorTools.GENERIC_BUSINESS_ERROR);
	                    }
					}
					
					if (!this.hasErrors()){
						// Set up the sql helpers
						SQLRelationDelegate delegate = SQLRelationHelper.getDelegate(data);
						SQLRelationHelper sqlHelp = new SQLRelationHelper(data,	delegate);
						if (existingCommissionPlans.length >0){
							deleteAllPlansByEffectiveDt(data, delegate, commissionEffectiveDate, false);
						}
						for (ModelBean plan : commissionPlans) {
							plan.setValue("EffectiveDt", commissionEffectiveDate);
							plan.setValue("Status", "Active");
							sqlHelp.saveBean(plan);
						}
					}	
					ModelBean responseAP = rs.getBean("ResponseParams").getBean("AdditionalParams");
					ModelBean param = new ModelBean("Param");
					param.setValue("Name", "ImportedFileName");
					param.setValue("Value", fileName);
					responseAP.addValue(param);

				} catch (Exception e) {
					addErrorMsg("General", "Invalid Excel File", ErrorTools.GENERIC_BUSINESS_ERROR);
					e.printStackTrace();
					Log.error(e);
				} finally {

					// Delete File
					File file = new File(uploadFile);
					file.delete();
				}
			}

			// If Response has Errors, Throw IBIZException
			if (hasErrors())
				throw new IBIZException();

			// Return Response ModelBean
			return rs;
		} catch (IBIZException e) {
			throw e;
		} catch (Exception e) {
			throw new ServiceHandlerException(e);
		}
	}
	
	/** Delete or just mark status as deleted for all CommissionPlan beans with a specific EffectiveDt 
	 * @param data The JDBCData
	 * @param effectiveDate The effective date
	 * @param delegate The SQL delegate
	 * @param markAsDeleted Flag to indicate if the deletion means only marking of Status as "Deleted" or real removal from DB
	 * @throws Exception 
	 */
	protected void deleteAllPlansByEffectiveDt(JDBCData data, SQLRelationDelegate delegate, StringDate effectiveDate, boolean markAsDeleted) throws Exception{
		String dbTableName = SQLRelationHelper.getDelegate(data).tableName("CommissionPlan");
		if (markAsDeleted){
			String query = "UPDATE " + dbTableName;
			query += " SET " + delegate.columnName("Status") + " = 'Deleted'";
			query += " WHERE " + delegate.columnName("EffectiveDt") + " = " + data.getDelegate().formatForSQL(effectiveDate);
			
			data.updateStatement(query);
		} else {
			String query = "DELETE FROM " + dbTableName;
			query += " WHERE " + delegate.columnName("EffectiveDt") + " = " + data.getDelegate().formatForSQL(effectiveDate);
			data.deleteStatement(query);
		}
	}

	
	/**
	 * Reads an Excel file and creates an XmlDoc object out of it
	 * 
	 * @param excelFile Excel file url
	 * @return XmlDoc object
	 * @throws Exception for errors
	 */
	public XmlDoc read(String excelFile) throws Exception {

		// WorkbookFactory create the appropriate kind of Workbook (be it
		// HSSFWorkbook or XSSFWorkbook), by auto-detecting from the supplied input.
		Workbook wb = WorkbookFactory.create(new FileInputStream(excelFile));

		XmlDoc doc = new XmlDoc();

		Element root = new Element("ExcelImport");
		doc.setRootElement(root);

		HashMap headersMap = new HashMap();

		for (int k = 0; k < wb.getNumberOfSheets(); k++) {
			Sheet sheet = wb.getSheetAt(k);
			int firstRowIndex = sheet.getFirstRowNum();
			int lastRowNumber = sheet.getLastRowNum();
			Log.info("Sheet " + k + " \"" + wb.getSheetName(k) + "\" has " + lastRowNumber + " row(s).");
			for (int r = firstRowIndex; r <= lastRowNumber; r++) {
				Row row = sheet.getRow(r);
				if (row == null) {
					continue;
				}

				if (headersMap.isEmpty()) {
					// It must be the header of the table, so read it
					for (int c = 0; c < row.getPhysicalNumberOfCells(); c++) {
						Cell cell = row.getCell(c);
						String cellValue = readExcelCell(cell);
						if (!cellValue.isEmpty() && cellValue.contains(" ")) {
							// Replace space string with empty string
							cellValue = cellValue.replace(" ", "");
						}
						headersMap.put(c, cellValue);
					}
					continue;
				}

				Element el = new Element("ExcelRow");
				int firstCellNumber = row.getFirstCellNum();
				int lastCellNumber = row.getLastCellNum();
				Log.info("\nROW " + row.getRowNum() + " has " + lastCellNumber + " cell(s).");
				int nonEmptyCellsNumber = 0;
				for (int c = firstCellNumber; c < lastCellNumber; c++) {
					Cell cell = row.getCell(c);
					String cellValue = readExcelCell(cell);
					if (!cellValue.isEmpty()){
						nonEmptyCellsNumber++;
					}
					el.setAttribute((String) headersMap.get(c), cellValue);
				}
				if (nonEmptyCellsNumber >0){
					root.addContent(el);
				}	
			}
		}
		return doc;
	}

	/** Read an Excel cell
	 * @param cell
	 * @return the value from the cell 
	 */
	protected String readExcelCell(Cell cell) {
		String value = "";
		int columnIndex = 0;
		if (cell != null) {
			switch (cell.getCellType()) {
			case HSSFCell.CELL_TYPE_STRING:
				value = cell.getStringCellValue();
		        break;
		    default:
		    	value = cell.toString().trim();
		    	break;
			}
			columnIndex = cell.getColumnIndex();
		}
		Log.info("CELL col=" + columnIndex + " VALUE=" + value);

		return value;
	}
}
