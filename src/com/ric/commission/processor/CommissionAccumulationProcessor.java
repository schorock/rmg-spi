/*
 * CommissionAccumulationProcessor.java
 *
 */

package com.ric.commission.processor;

import java.sql.SQLException;

import net.inov.tec.batch.MBPException;
import net.inov.tec.batch.MBPLookupSpec;
import net.inov.tec.beans.ModelBean;
import net.inov.tec.beans.ModelBeanException;
import net.inov.tec.beans.ModelSpecification;
import net.inov.tec.data.JDBCData;
import net.inov.tec.date.StringDate;
import net.inov.tec.math.Money;
import net.inov.tec.web.webpath.FrontController;

import com.iscs.common.admin.operation.BatchJobProcessing;
import com.iscs.common.business.company.Company;
import com.iscs.common.business.provider.Provider;
import com.iscs.common.tech.bean.data.SQLRelationDelegate;
import com.iscs.common.tech.bean.data.SQLRelationHelper;
import com.iscs.common.tech.log.Log;
import com.iscs.common.tech.service.ModelBeanProcessorBaseSPI;
import com.iscs.common.utility.DateTools;
import com.iscs.common.utility.InnovationUtils;
import com.iscs.common.utility.bean.BeanTools;
import com.iscs.common.utility.counter.Counter;
import com.iscs.common.utility.error.ErrorTools;
import com.iscs.insurance.producer.Producer;

/** Process all Producers - Sum CommissionDetail records and Create Payment Requests as necessary
 *
 * @author moniquef
 */
public class CommissionAccumulationProcessor extends ModelBeanProcessorBaseSPI {


	private String myName;
	private static String ERROR_DELIM = "||";
	private FrontController controller = new FrontController();
	private StringDate runDt = getRunDate();  

	/** Creates a new instance of CommissionAccumulationProcessor */
	public CommissionAccumulationProcessor() 
			throws Exception {
		super ();

		this.spec = ModelSpecification.getSharedModel();
		this.jdbcData = InnovationUtils.createConnection();
	}

	/** Bean processor main routine.  Each bean in the data repository table will be presented to this processor.  
	 * @param provider Each Provider bean in the data repository that is passed for evaluation
	 * @param data JDBC data repository connection
	 * @return boolean of whether this bean has been processed or not
	 * @throws MBPException when an error has occurred.
	 */
	public boolean processBean(ModelBean provider, JDBCData data) 
			throws MBPException {
		try {
			Log.debug ("Enter processBean() for "+provider.getBeanName()+" bean. SystemId = " + provider.getSystemId());

			// Validate ProducerAgency and ProducerGroup
			validateAgencyAndGroup(provider, data);

			// Get user entered job parameters
			StringDate runDt = getRunDate();
			String paymentDetailLevel = this.getParam("PaymentDetailLevel","");
			StringDate todayDt = DateTools.getStringDate();

			// Initialize
			String source = "";
			ModelBean producerInfo = provider.getBean("ProducerInfo");

			// Set up the sql helpers
			SQLRelationDelegate delegate = SQLRelationHelper.getDelegate(data);
			SQLRelationHelper sqlHelp = new SQLRelationHelper(data, delegate);      

			// Create the Payable request
			ModelBean requestBean =	createPayableRequest(data);            

			//Get ProducerTypeCd and PayToCd
			String producerTypeCd = producerInfo.gets("ProducerTypeCd");
			String payToCd = producerInfo.gets("PayToCd");

			// Accumulate data at AgencyOrGroup/Producer/Policy/Detail Level
			if( (producerTypeCd.equals("Group") && payToCd.equals("Group") ) 
					|| ( producerTypeCd.equals("Agency") && payToCd.equals("Agency") )
					|| ( Producer.isValidProducerType(provider) && payToCd.equals("Producer") ) ){

				// Build the query parameters
				String[] jdbckey1 = new String[] {"PayToTypeCd","PayToRef","PaidStatusCd","PayThroughDt"};
				String[] jdbcop1 = new String[] {"=","=","=","<="};
				String[] jdbcval1 = new String[] {payToCd,provider.getSystemId(),"Unpaid",data.getDelegate().formatDate(runDt.getCalendar(),false,false)};

				// Get all the commission detail records for the provider 
				ModelBean[] details = sqlHelp.selectModelBean("CommissionDetail", jdbckey1, jdbcop1, jdbcval1, 0 );  
   	        	        	        
				// Accumulate Data at Agency/Group level
				if( paymentDetailLevel.equals("AgencyOrGroup") ) {

					// Initialize Variables
					Money commissionAmt = new Money("0.00");
					String lastCarrierCd = "";

					// Sort Commission Detail by Carrier
					ModelBean[] commissionDetailArray = BeanTools.sortBeansBy(details, new String[] {"CarrierCd"}, new String[] {"Ascending"});
					for( ModelBean commissionDetail : commissionDetailArray ) {

						// If Carrier Has Changed, Then Create Payable Request and Reset Commission Amount
						String carrierCd = commissionDetail.gets("CarrierCd");
						if( !carrierCd.equals(lastCarrierCd) ) {

							// If Positive or Negative Commission Amount
		            		if( commissionAmt.compareTo("0.00") != 0 ) {

								// Add Payable Request
								String sourceCd = "CommissionDetail - " + "Provider::" + provider.getSystemId() + ":: PayThroughDt: " +  producerInfo.gets("CommissionPayThroughDt") + " - CarrierCd:: " + lastCarrierCd + "-Counter::" + Counter.nextNumber(data, "commissionCheckCounter");
								ModelBean request = createPayableDTO(provider, lastCarrierCd);
								request.setValue("SourceRef", sourceCd);
								request.setValue("ItemAmt", commissionAmt);
								requestBean.addValue(request);

								// Mark Commission Detail as Requested only when Commission amount is > 0
								for( ModelBean detail : commissionDetailArray ) {
									if( detail.valueEquals("CarrierCd", lastCarrierCd) ) {
										detail.setValue("PaidStatusCd", "Payment Requested");
										detail.setValue("PaymentRequestedDt", todayDt);
										detail.setValue("PaymentSourceRef", sourceCd);
										detail.setValue("PayThroughDt", runDt);										    	
								    	detail.setValue("OriginalCommissionDetailRef", "");								    								    									    	
										sqlHelp.saveBean(detail);
									}
								}
							}

							// Clear Commission Amount
							commissionAmt = new Money("0.00");
						}

						// Sum Commission Amount
						commissionAmt = commissionAmt.add(new Money(commissionDetail.gets("CommissionAmt")));

						// Track Last Carrier
						lastCarrierCd = carrierCd;
					}

					// If Positive or Negative Commission Amount
            		if( commissionAmt.compareTo("0.00") != 0 ) {

						// Add Payable Request
						String sourceCd = "CommissionDetail - " + "Provider::" + provider.getSystemId() + ":: PayThroughDt: " +  producerInfo.gets("CommissionPayThroughDt") + " - CarrierCd:: " + lastCarrierCd + "-Counter::" + Counter.nextNumber(data, "commissionCheckCounter");
						ModelBean request = createPayableDTO(provider, lastCarrierCd);
						request.setValue("SourceRef", sourceCd);
						request.setValue("ItemAmt", commissionAmt);
						requestBean.addValue(request); 
						
						// Mark Commission Detail as Requested only when Commission amount is > 0
						for( ModelBean detail : commissionDetailArray ) {
							if( detail.valueEquals("CarrierCd", lastCarrierCd) ) {
								detail.setValue("PaidStatusCd", "Payment Requested");
								detail.setValue("PaymentRequestedDt", todayDt);
								detail.setValue("PaymentSourceRef", sourceCd);
								detail.setValue("PayThroughDt", runDt);										
								detail.setValue("OriginalCommissionDetailRef", "");						    	
								sqlHelp.saveBean(detail);
							}
						}
					}
				}
				
				// Accumulate Data at Producer Level
				else if( paymentDetailLevel.equals("Producer") ) {		
					
					// Initialize Variables
					Money commissionAmt = new Money("0.00");
					String lastCarrierCd = "";
					String lastProducerRef = "";

					// Sort Commission Detail by Carrier and Producer
					ModelBean[] commissionDetailArray = BeanTools.sortBeansBy(details, new String[] {"CarrierCd","ProviderRef"}, new String[] {"Ascending","Ascending"});
					for( ModelBean commissionDetail : commissionDetailArray ) {

						// If Carrier has changed or Producer has Changed( this happens in case when Producer is paid to Agency/Group), 
						// then Create Payable Request and Reset Commission Amount
						String carrierCd = commissionDetail.gets("CarrierCd");
						String producerRef = commissionDetail.gets("ProviderRef");
						if( !carrierCd.equals(lastCarrierCd) || !producerRef.equals(lastProducerRef) ) {						
							
							// If Positive or Negative Commission Amount
		            		if( commissionAmt.compareTo("0.00") != 0 ) {						
									
									String sourceCd = "";									
									
									// If Producer is paid to Agency/Group, then add the Producer and Paid Agency/Group to the Source Code
								    if(!lastProducerRef.equals(provider.getSystemId())){
								    	sourceCd = "CommissionDetail - " + "Provider::" + lastProducerRef + ":: Paid Provider::" + provider.getSystemId() + ":: PayThroughDt: " +  producerInfo.gets("CommissionPayThroughDt") + " - CarrierCd:: " + lastCarrierCd + "-Counter::" + Counter.nextNumber(data, "commissionCheckCounter");
								    }else{
								    	sourceCd = "CommissionDetail - " + "Provider::" + lastProducerRef + ":: PayThroughDt: " +  producerInfo.gets("CommissionPayThroughDt") + " - CarrierCd:: " + lastCarrierCd + "-Counter::" + Counter.nextNumber(data, "commissionCheckCounter");								    	
								    }
									
								    // Add Payable Request
								   	ModelBean request = createPayableDTO(provider, lastCarrierCd);
									request.setValue("SourceRef", sourceCd);
									request.setValue("ItemAmt", commissionAmt);
									requestBean.addValue(request);
								    
									// Mark Commission Detail as Requested only when Commission amount is > 0
									for( ModelBean detail : commissionDetailArray ) {
										if( detail.valueEquals("CarrierCd", lastCarrierCd) && detail.valueEquals("ProviderRef", lastProducerRef)) {
											detail.setValue("PaidStatusCd", "Payment Requested");
											detail.setValue("PaymentRequestedDt", todayDt);
											detail.setValue("PaymentSourceRef", sourceCd);
											detail.setValue("PayThroughDt", runDt);													
											detail.setValue("OriginalCommissionDetailRef", "");									    
											sqlHelp.saveBean(detail);
										}
									}															
							}

							// Clear Commission Amount
							commissionAmt = new Money("0.00");
						}

						// Sum Commission Amount
						commissionAmt = commissionAmt.add(new Money(commissionDetail.gets("CommissionAmt")));

						// Track Last Carrier and Last Producer
						lastCarrierCd = carrierCd;
						lastProducerRef = producerRef;
					}

					// If Positive or Negative Commission Amount
            		if( commissionAmt.compareTo("0.00") != 0 ) {
						
						String sourceCd = "";
						
						// If Producer is paid to Agency/Group, then add the Producer and Paid Agency/Group to the Source Code
					    if(!lastProducerRef.equals(provider.getSystemId())){
					    	sourceCd = "CommissionDetail - " + "Provider::" + lastProducerRef + ":: Paid Provider::" + provider.getSystemId() + ":: PayThroughDt: " +  producerInfo.gets("CommissionPayThroughDt") + " - CarrierCd:: " + lastCarrierCd + "-Counter::" + Counter.nextNumber(data, "commissionCheckCounter");
					    }else{
					    	sourceCd = "CommissionDetail - " + "Provider::" + lastProducerRef + ":: PayThroughDt: " +  producerInfo.gets("CommissionPayThroughDt") + " - CarrierCd:: " + lastCarrierCd + "-Counter::" + Counter.nextNumber(data, "commissionCheckCounter");								    	
					    }
					    
				    	ModelBean request = createPayableDTO(provider, lastCarrierCd);
						request.setValue("SourceRef", sourceCd);
						request.setValue("ItemAmt", commissionAmt);
						requestBean.addValue(request); 			    
						
						// Mark Commission Detail as Requested only when Commission amount is > 0
						for( ModelBean detail : commissionDetailArray ) {
							if( detail.valueEquals("CarrierCd", lastCarrierCd) && detail.valueEquals("ProviderRef", lastProducerRef)) {
								detail.setValue("PaidStatusCd", "Payment Requested");
								detail.setValue("PaymentRequestedDt", todayDt);
								detail.setValue("PaymentSourceRef", sourceCd);
								detail.setValue("PayThroughDt", runDt);											
								detail.setValue("OriginalCommissionDetailRef", "");						    	
								sqlHelp.saveBean(detail);
							}
						}
					}
				}
				else if( paymentDetailLevel.equals("Policy") ) {            	

					details = BeanTools.sortBeansBy(details, new String[] {"SourceNumber"}, new String[] {"Ascending"});
					Money commissionAmt = new Money("0.00");

					for( int i=0; i<details.length; i++ ) {
						ModelBean detail = details[i];            		

						// If the policy number has changed 
						if( i != 0 && hasKeyChanged(details[i],details[i - 1], "SourceNumber") ) {

							// Create a PayableDTO bean
							ModelBean request = createPayableDTO (provider, details[i-1].gets("CarrierCd"));
							request.setValue("SourceRef",source);
							request.setValue("ItemAmt", commissionAmt);
							requestBean.addValue(request); 

							// Clear the summary fields
							commissionAmt = new Money("0.00");
						}

						// If end of file
						if( i == details.length - 1 ) {

							// Create the Payment Source Ref
							source = "CommissionDetail - " + "Provider::" + provider.getSystemId() + ":: PayThroughDt: " +  producerInfo.gets("CommissionPayThroughDt") + " - PolicyNumber:: " + detail.gets("SourceNumber") + "-Counter::" + Counter.nextNumber(data, "commissionCheckCounter");

							// Sum up the detail data
							commissionAmt = commissionAmt.add(new Money(detail.gets("CommissionAmt")));

							// Create a PayableDTO bean
							ModelBean request = createPayableDTO (provider, details[i].gets("CarrierCd"));
							request.setValue("SourceRef",source);
							request.setValue("ItemAmt", commissionAmt);
							requestBean.addValue(request); 
						}

						// Create the Payment Source Ref
						source = "CommissionDetail - " + "Provider::" + provider.getSystemId() + ":: PayThroughDt: " +  producerInfo.gets("CommissionPayThroughDt") + " - PolicyNumber:: " + detail.gets("SourceNumber") + "-Counter::" + Counter.nextNumber(data, "commissionCheckCounter");

						// Sum up the detail data
						commissionAmt = commissionAmt.add(new Money(detail.gets("CommissionAmt")));

						// Update the CommissionDetail bean
						detail.setValue("PaidStatusCd","Payment Requested");
						detail.setValue("PaymentRequestedDt",todayDt);
						detail.setValue("PaymentSourceRef",source);
						detail.setValue("PayThroughDt", runDt);								
						detail.setValue("OriginalCommissionDetailRef", "");				    	
						sqlHelp.saveBean(detail);
					}

				}
				else if( paymentDetailLevel.equals("Detail") ) {

					for( int i=0; i<details.length; i++ ) {
						ModelBean detail = details[i];            		

						// Create the Payment Source Ref
						source = "CommissionDetail - " + "Provider::" + provider.getSystemId() + ":: PayThroughDt: " +  producerInfo.gets("CommissionPayThroughDt") + " - PolicyNumber:: " + detail.gets("SourceNumber") + " - Detail:: " + detail.getSystemId() + "-Counter::" + Counter.nextNumber(data, "commissionCheckCounter");

						// Create a PayableDTO bean
						ModelBean request = createPayableDTO (provider, detail.gets("CarrierCd"));
						request.setValue("SourceRef",source);
						request.setValue("ItemAmt", detail.gets("CommissionAmt"));
						requestBean.addValue(request);

						// Update the CommissionDetail bean
						detail.setValue("PaidStatusCd","Payment Requested");
						detail.setValue("PaymentRequestedDt",todayDt);
						detail.setValue("PaymentSourceRef",source);
						detail.setValue("PayThroughDt", runDt);								
						detail.setValue("OriginalCommissionDetailRef", "");				    
						sqlHelp.saveBean(detail);

					}
				}
			}

			// Send the Payable request
			return sendPayableRequest(data, provider, requestBean);

		} catch (Exception e) {
			if (this.processControlNumber > 0) {      
				try {
					Log.error(e);
					String msg = getErrorMsg( provider, e.getMessage(), data);
					BatchJobProcessing.changeStatus(this.jdbcData, this.processControlNumber, this.myName, "Error", msg);
				} catch (Exception ec) {
					throw new MBPException(ec);
				}
			}
			return false;
		}
	}


	public ModelBean createPayableRequest (JDBCData data)
			throws ModelBeanException, Exception {    	

		ModelBean requestBean = new ModelBean("CMCommissionPaymentRq");

		// Set the UserId and SessionId in the RequestParams
		ModelBean requestParams = new ModelBean("RequestParams");
		requestParams.setValue("SecurityId",this.getParam("SecurityId"));
		requestParams.setValue("SessionId", this.getParam("SessionId"));
		requestParams.setValue("UserId", this.getParam("UserId"));
		requestParams.setValue("ConversationId", getParam("ConversationId"));
		requestBean.addValue(requestParams);

		return requestBean;
	}

	public boolean sendPayableRequest (JDBCData data, ModelBean provider, ModelBean requestBean)
			throws ModelBeanException, Exception {    	

		// Turn Off Debug Messages
		Log.setLoggingLevelError();

		// Send Webpath Request
		ModelBean response = controller.sendRequest(requestBean, data);

		// Turn On Debug Messages
		Log.setLoggingLevelDebug();        

		// Look to see if the response returned any errors besides just warnings
		ModelBean errors = response.getBean("ResponseParams").getBean("Errors");
		if( errors != null ) {
			ModelBean[] info = errors.findBeansByFieldValue("Error", "Severity", ErrorTools.SEVERITY_INFO.getSeverity());
			ModelBean[] warning = errors.findBeansByFieldValue("Error", "Severity", ErrorTools.SEVERITY_WARN.getSeverity());            
			ModelBean[] error = errors.getBeans("Error");

			if (error.length > (warning.length + info.length)) {
				String errorMsg = "Provider: " + provider.getSystemId() + " - " + error[0].gets("Msg");
				BatchJobProcessing.changeStatus(this.jdbcData, this.processControlNumber, this.myName, "Running", errorMsg, error[0].gets("Severity"));
				return false;
			}
		}

		return true;
	}

	public ModelBean createPayableDTO (ModelBean provider, String carrierCd)
			throws Exception {   	

		ModelBean request = null; 		

		// Create the DTO Payable Request ModelBean
		request = new ModelBean("DTOPayableRequest");

		// Fill the information in the claim into the DTO request
		request.setValue("TransactionCd", "Payment");
		if (provider.gets("PaymentPreferenceCd").equalsIgnoreCase("Check")) {				
			request.setValue("PaymentMethodCd", "Check - Batch");
		} else if (provider.gets("PaymentPreferenceCd").equalsIgnoreCase("ACH")) {	
			request.setValue("PaymentMethodCd", "ACH");
			ModelBean contract = provider.getBean("ElectronicPaymentContract", "ContractTypeCd", "Commission");
			if( contract!=null){
				ModelBean destination = contract.getBean("ElectronicPaymentDestination").cloneBean();			
				request.addValue(destination);
			} else {
				throw new Exception("Missing Provider ACH information");
			}
		} else{
			throw new Exception("Provider Payment Method not supported");
		}

		// Get the Company Bank Account Info 
		ModelBean company = Company.getCompany().getBean();		
		ModelBean carrier = company.findBeanByFieldValue("Carrier", "id", carrierCd);

		request.setValue("PaymentAccountCd", carrier.gets("CommissionBankAccount"));
		request.setValue("PayToName", provider.findBeanByFieldValue("NameInfo","NameTypeCd","AcctName").gets("CommercialName"));
		request.setValue("ProviderRef", provider.getSystemId());
		request.setValue("StatusCd", "Open");
		request.setValue("StatusDt", runDt);
		request.setValue("Memo", "");
		request.setValue("MemoCd", "");
		request.setValue("RequestDt", "");
		request.setValue("ClassificationCd", "");
		request.setValue("ItemNumber", "");
		request.setValue("ItemDt", "");
		request.setValue("TransactionDt", "");
		request.setValue("TransactionTm", "");
		request.setValue("TransactionUser", "");
		request.setValue("BookDt", "");
		request.setValue("SourceCd", "Commission");		

		// Set the indicator to specify that ACH requests be tracked by Payables for check detail updating
		request.setValue("CheckUpdateRequestInd", "Yes");
		
		// Override the alias of the name/address bean for Payables
		ModelBean partyInfo = provider.findBeanByFieldValue("PartyInfo","PartyTypeCd","ProviderParty").cloneBean();

		ModelBean nameInfo = partyInfo.findBeanByFieldValue("NameInfo","NameTypeCd","ProviderName");
		nameInfo.setValue("NameTypeCd","PayToName");

		ModelBean billAddr = partyInfo.findBeanByFieldValue("Addr","AddrTypeCd","ProviderBillingAddr");
		billAddr.setValue("AddrTypeCd","PayToMailingAddr");
		request.addValue(partyInfo);

		// Additional detail information
		ModelBean payableDetail = new ModelBean("DTOPayableDetail");	
		payableDetail.setValue("CarrierCd", carrierCd);
		
		request.addValue(payableDetail);

		return request;
	}

	/** Data repository selection criteria.  Only pass through tasks that have status of Open
	 * @return MBPLookupSpec The lookup specification object containing the fields to be used for data selection
	 * @throws MBPException when an error occurs
	 */
	public MBPLookupSpec getLookupSpec () 
			throws MBPException {
		return new MBPLookupSpec("ProviderType", "=", "Producer");
	}

	/** Find out the run date of the current batch process
	 * @return StringDate The date of the current batch run is set to.
	 * @throws Exception when an error occurs
	 */
	public StringDate getRunDate() 
			throws Exception {
		// Find if the run date has been passed in
		if (this.getParam("RunDt", "").equals("")) {
			return DateTools.getStringDate();
		} else {
			return new StringDate(this.getParam("RunDt"));
		}
	}

	/** Set up the processor before the batch job is executed.
	 * @throws Exception when an error occurs
	 */
	public void initialCall() 
			throws MBPException {
		try {        	
			//  Make an entry in the processor table to show that we have started. 
			//  This is for information purposes only.
			String[] myNames = this.getClass().getName().split("[.]");
			this.myName = myNames[myNames.length-1];
			this.processControlNumber = BatchJobProcessing.markRunning(jdbcData, myName);


		} catch (Exception e) {
			// If the job has been entered into the process table, go ahead and mark it as an error
			if (this.processControlNumber > 0) {      
				try {
					BatchJobProcessing.changeStatus(this.jdbcData, this.processControlNumber, this.myName, "Error", e.getMessage());
				} catch (Exception ec) {
					throw new MBPException(ec);
				}
			}
			throw new MBPException(e);
		}
	}


	/** Update the process control manager that this task is now complete.
	 * @param beanData The data repository connection
	 * @throws MBPException when an error occurs
	 */
	public void finalCall (JDBCData beanData) 
			throws MBPException {
		try {
			//  Make an entry in the processor table to show that we have finished. 
			//  This is for information purposes only.
			BatchJobProcessing.changeStatus(jdbcData, processControlNumber, myName, "Complete", "");
		} catch (SQLException sqle) {
			// Do not throw an exception if its because the result of the search is empty
			if (sqle.getErrorCode() != 0) 
				throw new MBPException(sqle);
		} catch (Exception e) {
			throw new MBPException(e);
		}
	}

	/* Determines if a change in the key values has occured between the current and last commission record   
	 *  @param currentCommission the current CommissionDetail bean
	 *  @param lastCommission the prior CommissionDetail bean
	 *  @param key String 
	 *  @return true if any key values are different between the two beans 
	 */
	private boolean hasKeyChanged(ModelBean currentCommission, ModelBean lastCommission, String key) throws Exception {
		// Check if there is a change in the key values	
		if( !currentCommission.gets(key).equals(lastCommission.gets(key)) )
			return true;
		else
			return false;	
	}

	/** Validate the Producer Agency and Producer Group
	 * @param provider Each Provider bean in the data repository that is passed for evaluation
	 * @param data JDBC data repository connection
	 * @throws Exception when an error occurs
	 */
	public void validateAgencyAndGroup(ModelBean provider, JDBCData data)
			throws Exception {   	

		// Get Producer Agency and Producer Group
		ModelBean producerInfo = provider.getBean("ProducerInfo");
		String producerAgency = producerInfo.gets("ProducerAgency");
		String producerGroup = producerInfo.gets("ProducerGroup");

		// Initialize error message and status
		String errorMsg = "";
		boolean errorInd = false;			

		/* Standard Conditions */
		// If Producer Agency is provided, validate that it exists and is truly an agency 
		if( !producerAgency.equals("") ) {
			// Find the Provider based on Producer Agency
			ModelBean agencyProvider = Provider.getProvider(data, producerAgency, "Producer");
			if( agencyProvider.getSystemId().equals("") ) {
				errorInd = true;
				errorMsg = errorMsg + " Producer Agency " +  producerAgency + " does not exist.";
			}
			else if( !agencyProvider.getBean("ProducerInfo").gets("ProducerTypeCd").equals("Agency") ) {
				errorInd = true;
				errorMsg = errorMsg + " Producer Agency " +  producerAgency + " is not an Agency.";
			}
		} 

		// If Producer Group is provided, validate that it exists and is truly a Group
		if( !producerGroup.equals("") ) {
			// Find the Provider based on Producer Group
			ModelBean groupProvider = Provider.getProvider(data, producerGroup, "Producer");
			if( groupProvider.getSystemId().equals("") ) {
				errorInd = true;
				errorMsg = errorMsg + " Producer Group " + producerGroup + " does not exist."; 
			}
			else if( !groupProvider.getBean("ProducerInfo").gets("ProducerTypeCd").equals("Group") ) {
				errorInd = true;
				errorMsg = errorMsg + " Producer Group " +  producerGroup + " is not a Group.";
			}
		} 

		// If error status is true, throw exception
		if( errorInd ) {
			throw new Exception(errorMsg);
		} 

		/* Special Conditions */
		// If producer is a valid Producer Type(ProducerTypeCd = Producer/Agent/Broker and PayToCd = Agency), Producer Agency is required 
		if( Producer.isValidProducerType(provider)){
			if(producerInfo.gets("PayToCd").equals("Agency") ) {
				if( producerAgency.equals("") ) {
					errorInd = true;
					errorMsg = errorMsg + " Producer Agency is not set up.";
				}
				else {
					// Find the Provider based on Producer Agency
					ModelBean agencyProvider = Provider.getProvider(data, producerAgency);
					String agencyProducerGroup = agencyProvider.getBean("ProducerInfo").gets("ProducerGroup");
					// If Producer Agency's PayToCd = Group, Producer Group is required 
					if(  agencyProvider.getBean("ProducerInfo").gets("PayToCd").equals("Group") ) {
						// Producer Group must match the agency's Producer Group 
						if( !producerGroup.equals("") ) {
							if( !producerGroup.equalsIgnoreCase(agencyProducerGroup) ) {
									errorInd = true;
									errorMsg = errorMsg + " Producer Group does not match the agency's Producer Group.";
							}
							} else {
								errorInd = true;
								errorMsg = errorMsg + " Producer Group is not set up.";
							}
						}
				} 
			} else if(producerInfo.gets("PayToCd").equals("Group") ){
				if( producerGroup.equals("") ) {
					errorInd = true;
					errorMsg = errorMsg + " Producer Group is not set up.";
				}
			}
		}

		// If ProducerTypeCd = Agency and PayToCd = Group, Producer Group is required
		else if( producerInfo.gets("ProducerTypeCd").equals("Agency") && producerInfo.gets("PayToCd").equals("Group") ) {
			if( producerGroup.equals("") ) {
				errorInd = true;
				errorMsg = errorMsg + " Producer Group is not set up.";
			}	
		}

		// If error status is true, throw exception
		if( errorInd ) {
			throw new Exception(errorMsg);
		} 
	}

	private String getErrorMsg(ModelBean provider, String msg, JDBCData data) {
		try {
			StringBuffer msgBuf = new StringBuffer();

			// Task information
			msgBuf = msgBuf.append("Provider ")
					.append(provider.getSystemId())
					.append(" - Producer Code ")
					.append(provider.gets("ProviderNumber"))
					.append(" - Unable to process this producer.");


			return msgBuf.append(ERROR_DELIM).append(msg).append(ERROR_DELIM).toString();

		} catch (Exception e) {
			return "Unknown error";
		}
	}

}
